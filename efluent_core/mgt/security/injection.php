<?

set_time_limit(99999999);

REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

function validateVar($value)
{
if(stristr($value, "concat") || 
		stristr($value, "select") || 
		stristr($value, "from") || 
		stristr($value, "etc") ||
		stristr($value, "passwd") ||
		stristr($value, "DROP") ||
		stristr($value, "DR/**/OP/*") ||
		stristr($value, "--") ||
		stristr($value, "OR") || 
		stristr($value, "0x50") || 
		stristr($value, "login") ||
		stristr($value, "ascii") ||
		stristr($value, "having") ||
		stristr($value, "sum") ||
		stristr($value, "cast") ||
		stristr($value, "convert") ||
		stristr($value, "group") ||
		stristr($value, "where") ||
		stristr($value, "shell") ||
		stristr($value, "exec") ||
		stristr($value, "sp_") ||
		stristr($value, "declare") ||
		stristr($value, "wait") ||
		stristr($value, "exists") ||
		stristr($value, "sha1") ||
		stristr($value, "encode") ||
		stristr($value, "row_count") ||
		stristr($value, "schema") ||
		stristr($value, "version") ||
		stristr($value, "char") )
	{	
		return false;
	}
	
	return true;
}

foreach($_POST as $key => $value)
{
	if( !validateVar($key) || !validateVar($value))
	{
		echo "SQL Injection _POST<BR>$value<BR>";
	}
	else 
	{
		echo "Valid<BR>";
	}
}


foreach($_GET as $key => $value)
{
	if( !validateVar($key) || !validateVar($value))
	{
		echo "SQL Injection _GET<BR>$value<BR>";
	}
	else 
	{
		echo "Valid<BR>";
	}
}
?>