<HTML>
<HEAD>
<TITLE>eFluent Core Monitor</TITLE>
<meta http-equiv="Content-Style-Type" content="text/css">
<LINK HREF="/mgt/style.css" TYPE="text/css" REL="stylesheet">
<style type="text/css">
input.sleeping
{
   font-size:12px;
   font-family:Courier New,monospace;
   color:#330033;
   background-color:red;
   filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr='red',EndColorStr='red');
   border-style:double;
   border-color:#000000;
   border-width:3px;
}
input.awake
{
   font-size:12px;
   font-family:Courier New,monospace;
   color:#000000;
   background-color:green;
   filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr='green',EndColorStr='green');
   border-style:double;
   border-color:#99FF99;
   border-width:3px;
}
input.sleeping2
{
   font-size:12px;
   font-family:Courier New,monospace;
   color:#330033;
   background-color:red;
   filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr='red',EndColorStr='red');
   border-style:double;
   border-color:#000000;
   border-width:3px;
}
input.awake2
{
   font-size:12px;
   font-family:Courier New,monospace;
   color:#000000;
   background-color:green;
   filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr='green',EndColorStr='green');
   border-style:double;
   border-color:#99FF99;
   border-width:3px;
}
</style>
</HEAD>
<script>
function GetXmlHttpObject()
{
	var xmlHttp=null;

	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}

	return xmlHttp;
}

function ajax()
{
	if(xmlHttp.readyState==4)
	{
		document.getElementById("stat").innerHTML = xmlHttp.responseText;
		setTimeout("loadPage()",5000);
	}
}

function StartUp()
{
	setTimeout("loadPage()",3000);
}

function loadPage()
{
	xmlHttp=GetXmlHttpObject();


	var url="ajax_stat.php";

	xmlHttp.onreadystatechange=ajax;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);


	return false;
}
</script>
<body onload="StartUp()" bgcolor="white">
<div id="stat">&nbsp;</div>
</body>
</html>
