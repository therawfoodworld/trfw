

<style>
table.contacts
{ width: 240px;
background-color: #fafafa;
border: 1px #000000 solid;
border-collapse: collapse;
border-spacing: 0px; }

table.contacts2
{ width: 300px;
background-color: #fafafa;
border: 1px #000000 solid;
border-collapse: collapse;
border-spacing: 0px; }


td.contactDept
{ background-color: #99CCCC;
border: 1px #000000 solid;
font-family: Verdana;
font-weight: bold;
font-size: 10px;
color: #404040; }


td.contact
{ border-bottom: 1px #6699CC dotted;
text-align: left;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: 10px;
color: #404040;
background-color: #fafafa;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }

table.alerts
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 260px;
background-color: #fafafa;
border: 1px #d79900 solid;
border-collapse: collapse;
border-spacing: 0px;}


.alertHd
{ border-bottom: 2px solid #d79900;
background-color: #fff2ba;
text-align: center;
font-family: Verdana;
font-weight: bold;
font-size: 11px;
color: #404040;}


.alertBod p
{ text-align: left;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: 11px;
line-height: 12px;
color: #404040;
background-color: #fafafa;
text-indent: -8px;
margin-left: 10px;
margin-right: 10px;
margin-top: 5px;
margin-bottom: 5px;} 

table.stats
{text-align: center;
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif ;
font-weight: normal;
font-size: 11px;
color: #fff;
width: 280px;
background-color: #666;
border: 0px;
border-collapse: collapse;
border-spacing: 0px;}

table.stats td
{background-color: #CCC;
color: #000;
padding: 4px;
text-align: left;
border: 1px #fff solid;}

table.stats td.hed
{background-color: #666;
color: #fff;
padding: 4px;
text-align: left;
border-bottom: 2px #fff solid;
font-size: 12px;
font-weight: bold;} 

table.helpT
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 500px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px; }

td.helpHed
{ border-bottom: 2px solid #6699CC;
border-left: 1px solid #6699CC;
background-color: #BEC8D1;
text-align: left;
text-indent: 5px;
font-family: Verdana;
font-weight: bold;
font-size: 11px;
color: #404040; }

td.helpBod
{ border-bottom: 1px solid #9CF;
border-top: 0px;
border-left: 1px solid #9CF;
border-right: 0px;
text-align: left;
text-indent: 10px;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: #fafafa; }


table.sofT4
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 380px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px; } 


table.sofT
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 600px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px; } 

table.sofT2
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 180px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px; } 


table.sofT3
{ text-align: center;
font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 500px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px; } 



table.servicesT
{ font-family: Verdana;
font-weight: normal;
font-size: 11px;
color: #404040;
width: 320px;
background-color: #fafafa;
border: 1px #6699CC solid;
border-collapse: collapse;
border-spacing: 0px;
margin-top: 0px;}


table.servicesT td.servHd
{ border-bottom: 2px solid #6699CC;
background-color: #BEC8D1;
text-align: center;
font-family: Verdana;
font-weight: bold;
font-size: 11px;
color: #404040;}


table.servicesT td
{ border-bottom: 1px dotted #6699CC;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: 11px;
color: #404040;
background-color: white;
text-align: left;
padding-left: 3px;}

.servBodL { border-left: 1px dotted #CEDCEA; } 
</style>



<?
//exit;

	$swap = join( '', file( '/proc/meminfo' ) );
	$swap = explode("\n",$swap);
	$swap = $swap[12];
	$swap = str_replace("SwapFree:","",trim($swap));
	$swap = str_replace("kB","",trim($swap));
	$swap = str_replace(" ","",trim($swap));
	$swap = $swap."kB";
	

	//memory
	$mem = join( '', file( '/proc/meminfo' ) );
	$mem = explode("\n",$mem);
	$memtotal = $mem[0];
	$memfree = $mem[1];
	$memcache = $mem[3];
	
	$memfree = str_replace("MemFree:","",trim($memfree));
	$memfree = str_replace("kB","",trim($memfree));

	$memcache = str_replace("MemCache:","",trim($memcache));
	$memcache = str_replace("kB","",trim($memcache));

	$memtotal = str_replace("MemTotal:","",trim($memtotal));

	$memfree = str_replace(" ","",trim($memfree));
	$memcache = str_replace(" ","",trim($memcache));
	
	$memtotal = str_replace(" ","",trim($memtotal));
	
	$memfree = $memfree + $memtotal . "kB";   

	//disk space
	$df = `/bin/df`;
	foreach( split( "\n", $df ) as $line )
	{
		if( preg_match( "/(\d+%)\s+(\S+)$/", $line, $matches ) )
		{
			$fs = $matches[ 2 ];
			if($usage == "") {
				$usage = $matches[ 1 ];
				// $fs = name space
			}
		}
	}
	
	$la = sys_getloadavg();
	$la = "$la[0], $la[1], $la[2]";

	$procs = `/bin/ps -e|wc -l`;
	$procs = trim($procs);
	
	//display uniq (count:ip)

	/*$connections = `netstat -plan | grep :80 | awk '{print $5}' | awk -F: '{print $1}' | sort uniq -c`;
	$connections = explode("\n",$connections);

	unset($connections[0]);

	foreach($connections as $k => $s)
	{
		if($s !="")
		{
			$s = trim($s);
			$carray[] = $s;
		}

	}

	foreach($carray as $k => $v)
	{
		unset($tmp);
		$tmp = explode(" ",$v);
		$numbers[] = $tmp[0];
	}

	$count = 0;
	foreach($numbers as $number) {  $count = $count + $number; $counts[] = $count; }
	$counts = array_reverse($counts);*/

	//display all established (count)
	$non_uniq = trim(`netstat -an | grep ':80' | grep ESTAB | wc -l`) - 1;


	/////////////////////////////////////////////////////////////////////////////////////////////////////


	$uptime = `uptime`;
	$uptime = trim($uptime);

	$uptime = explode(" ",$uptime);
	foreach($uptime as $k => $v) { $uptime[$k] = str_replace(",","",$v); }

	
	//$y2staff = $uptime[6];
      $y2staff = str_replace("\n","",`who | wc -l`);
//      $y2staff = '0';

	$uptime = "$uptime[2], $uptime[3]";


	$server_array[] = $y2staff;
	$server_array[] = $la;
	$server_array[] = $count;
	$server_array[] = $procs;


	$data = '
<table summary="Stater" class="contacts" cellspacing="0">
<tr>
<td class="contactDept">server</td>
<td class="contactDept">&nbsp;value</td>
</tr>

<tr>
<td class=contact>Server Uptime</td>
<td class=contact>&nbsp;'.$uptime.'</td>
</tr>

<tr>
<td class=contact>Y2Tech Staff</td>
<td class=contact>&nbsp;'.$y2staff.'</td>
</tr>

<tr>
<td class=contact>Swap Free</td>
<td class=contact>&nbsp;'.$swap.'</td>
</tr>

<tr>
<td class=contact>Memory Free</td>
<td class=contact>&nbsp;'.$memfree.'</td>
</tr>


<tr>
<td class=contact>Memory Total</td>
<td class=contact>&nbsp;'.$memtotal.'</td>
</tr>

<tr>
<td class=contact>Drive Usage</td>
<td class=contact>&nbsp;'.$usage.'</td>
</tr>

<tr>
<td class=contact>Load Average</td>
<td class=contact>&nbsp;'.$la.'</td>
</tr>

<tr>
<td class=contact>Connection Count</td>
<td class=contact>&nbsp;'.$non_uniq.'</td>
</tr>

<tr>
<td class=contact>Active Processes</td>
<td class=contact>&nbsp;'.$procs.'</td>
</tr>

</table>
<!-- </tr> -->';

	echo $data;




/*
<table align=left><tr><td></td></tr></table>
<table align=left border="1" cellpadding="1" cellspacing="1" width="22%">
<tr>
<th>routine</th>
<th>timestamp</th>
</tr>
<?
$query = "select data, cur_timestamp from monitor_timestamp";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
while(list($data,$timestamp) = mysql_fetch_array($qok))
{?>
<tr>
<td width="30%"><?=str_replace("_"," ",$data);?></td>
<td>&nbsp;<?=$timestamp;?></td>
</tr>
<?}?>
</table>
*/

?>

