<?
class htmlStats {
	
	function __construct() {
		
	}
	function getHTML() {
		?>
			<table cellpadding="3" cellspacing="3" border="0">
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="rowTitle" colspan="7">Servers Stats</td>
								<td width="30px">&nbsp;</td>
								<td class="rowTitle">Buy.com</td>
								<td width="30px">&nbsp;</td>
								<td class="rowTitle">Flow</td>
							</tr>
							<tr>
								<td style="padding-top:4px;font-size:4px;height:4px;" colspan="9">&nbsp;</td>
							</tr>
							<tr>
								<td><?$this->getScriptServerStats()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getDatabaseServerStats()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getServerStats()?></td>
								<td width="30px">&nbsp;</td>
								<td><?$this->getBuyScripts()?></td>
								<td width="30px">&nbsp;</td>
								<td><?$this->getFlowScripts()?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="rowSep">&nbsp;</td>
				</tr>
				<tr>
					<td height="6px">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="rowTitle" colspan="13">Scripts</td>
							</tr>
							<tr>
								<td style="padding-top:4px;font-size:4px;height:4px;" colspan="13">&nbsp;</td>
							</tr>
							<tr>
								<td><?$this->getCustomScriptsLeft()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getCustomScriptsLeft2()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getCustomScriptsMiddle()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getCustomScriptsRight()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getCustomScriptsRight2()?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="rowSep">&nbsp;</td>
				</tr>
				<tr>
					<td height="6px">&nbsp;</td>
				</tr>
				<tr>
					<td class="rowTitle">Amazon Scripts</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td><?$this->getAmazonScripts()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getAmazonCanadaScripts()?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getAmazonMaxScripts()?></td>
							</tr>
							<tr>
								<td colspan="7" height="6px">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="7" class="rowTitle">FBA Scripts</td>
							</tr>
							<tr>
								<td colspan="7" height="6px">&nbsp;</td>
							</tr>
							<tr>
								<td><?$this->getFBAScripts();?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getFBACanadaScripts();?></td>
								<td class="rightBorder colSep">&nbsp;</td>
								<td class="colSep">&nbsp;</td>
								<td><?$this->getFBAMaxScripts()?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="rowSep">&nbsp;</td>
				</tr>
				<tr>
					<td height="6px">&nbsp;</td>
				</tr>
				<tr>
					<td class="rowTitle">Vendors</td>
				</tr>
				<tr>
					<td>
					<?=$this->getVendorHTML();?>
					</td>
				</tr>
			</table>
		<?
	}
	function getScriptServerStats() {
		$swap = join( '', file( '/proc/meminfo' ) );
		$swap = explode("\n",$swap);
		$swap = $swap[12];
		$swap = str_replace("SwapFree:","",trim($swap));
		$swap = str_replace("kB","",trim($swap));
		$swap = str_replace(" ","",trim($swap));
		$swap = $swap."kB";
	
		//memory
		$mem = join( '', file( '/proc/meminfo' ) );
		$mem = explode("\n",$mem);
		$memtotal = $mem[0];
		$memfree = $mem[1];
		$memcache = $mem[3];
		
		$memfree = str_replace("MemFree:","",trim($memfree));
		$memfree = str_replace("kB","",trim($memfree));
	
		$memcache = str_replace("MemCache:","",trim($memcache));
		$memcache = str_replace("kB","",trim($memcache));
	
		$memtotal = str_replace("MemTotal:","",trim($memtotal));
	
		$memfree = str_replace(" ","",trim($memfree));
		$memcache = str_replace(" ","",trim($memcache));
		
		$memtotal = str_replace(" ","",trim($memtotal));
		
		$memfree = $memfree + $memtotal . "kB";   
	
		//disk space
		$df = `/bin/df`;
		foreach( split( "\n", $df ) as $line )
		{
			if( preg_match( "/(\d+%)\s+(\S+)$/", $line, $matches ) )
			{
				$fs = $matches[ 2 ];
				if($usage == "") {
					$usage = $matches[ 1 ];
					// $fs = name space
				}
			}
		}
		
		$la = sys_getloadavg();
		$la = "$la[0], $la[1], $la[2]";
	
		$procs = `/bin/ps -e|wc -l`;
		$procs = trim($procs);
		
		//display all established (count)
		$non_uniq = trim(`netstat -an | grep ':80' | grep ESTAB | wc -l`) - 1;
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		$uptime = `uptime`;
		$uptime = trim($uptime);
	
		$uptime = explode(" ",$uptime);
		foreach($uptime as $k => $v) { $uptime[$k] = str_replace(",","",$v); }

       $y2staff = str_replace("\n","",`who | wc -l`);
	
	    if(file_exists("/tmp/.staff.lock")) { $y2staff = 0; } // <---  Hide and watch :-D
	
		$uptime = "$uptime[2], $uptime[3]";

		$server_array[] = $y2staff;
		$server_array[] = $la;
		$server_array[] = $count;
		$server_array[] = $procs;
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td><b>Scripts</b></td>
				<td class="colSep">&nbsp;</td>
				<td><b>&nbsp;Value</b></td>
			</tr>
			<tr>
				<td>Server Uptime</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$uptime?></td>
			</tr>
			<tr>
				<td>Y2Tech Staff</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$y2staff?></td>
			</tr>
			<tr>
				<td>Swap Free</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$swap?></td>
			</tr>
			<tr>
				<td>Memory Free</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$memfree?></td>
			</tr>
			<tr>
				<td>Memory Total</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$memtotal?></td>
			</tr>
			<tr>
				<td>Drive Usage</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$usage?></td>
			</tr>
			<tr>
				<td>Load Average</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$la?></td>
			</tr>
			<tr>
				<td>Connection Count</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$non_uniq?></td>
			</tr>
			<tr>
				<td>Active Processes</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$procs?></td>
			</tr>
		</table>
		<?
	}
	function getDatabaseServerStats() {
		$query = "show processlist";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while($row = mysql_fetch_array($qok, MYSQL_ASSOC)) {	$mysql_array[$row['Id']] = $row['Command']; }
		foreach($mysql_array as $k => $v)
		{
			$c_count++;
			if($v !="Sleep"){ $q_count++; }
		}
		
		
		$la = `curl http://192.168.100.88/dbstat.php`;
		$la = base64_decode($la);
		$la = explode("\n",$la);
		$san = trim($la[8]);
		$swap = trim($la[7]);
		$uptime = trim($la[6]);
		$drive = trim($la[5]);
		$memfree = trim($la[4]);
		$memtotal = trim($la[3]);
		$who = trim($la[1]);
		$la = trim($la[0]);
		?>
		<table summary="Oooo" class="contacts" cellspacing="0">
			<tr>
				<td><b>Database</b></td>
				<td class="colSep">&nbsp;</td>
				<td><b>&nbsp;Value</b></td>
			</tr>
			<tr>
				<td>Server Uptime</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$uptime?></td>
			</tr>
			<tr>
				<td>Y2Tech Staff</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$who?></td>
			</tr>
			<tr>
				<td>Swap Free</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$swap?></td>
			</tr>
			
			<tr>
				<td>Memory Free</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$memfree?></td>
			</tr>
			<tr>
				<td>Memory Total</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$memtotal?></td>
			</tr>
			<tr>
				<td>Drive Usage</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$drive?></td>
			</tr>
			<tr>
				<td>SAN Usage</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$san?></td>
			</tr>
			<tr>
				<td>Load Average</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$la?></td>
			</tr>
			<tr>
				<td>Connection Count</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$c_count?></td>
			</tr>
			<tr>
				<td>Active Queries</td>
				<td class="colSep">&nbsp;</td>
				<td>&nbsp;<?=$q_count?></td>
			</tr>
		
		</table>
		<?		
	}
	function getServerStats() {
		$a = "http://ecomelectronics.com/mgt/security/monitor/monitor-stat2.php"; 
		$cmd = "curl -s $a"; 
		passthru($cmd);
	}
	
	function getCustomScriptsLeft(){
		$data_array = array();
		$query = "select data, cur_timestamp from monitor_timestamp where data != 'eBay_MSCNTR'";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while(list($data,$timestamp) = mysql_fetch_array($qok))
		{
			$data = str_replace("_"," ",$data);
//			$data_array[$data] = $timestamp;
			array_push($data_array,array("key"=>$data,"value"=>$timestamp));
		}

		$arr = array_chunk($data_array,round(count($data_array)/2));
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="contactDept">Routine</td>
				<td class="colSep">&nbsp;</td>
				<td class="contactDept">&nbsp;Timestamp</td>
			</tr>
			<?foreach($arr[0] as $key => $string){
					
					if ((time()-strtotime($string['value'])) > 172800) {
						$color = "font-weight:bold;color:red";
					}
					else {
						$color = "";
					}
				?>
				<tr style="<?=$color?>">
					<td><?=$string['key'];?></td>
					<td class="colSep">&nbsp;</td>
					<td><?=str_replace("2013-","",$string['value']);?></td>
				</tr>
			<?}?>
		</table>
		<?
	}
	function getCustomScriptsLeft2(){
		$data_array = array();
		$query = "select data, cur_timestamp from monitor_timestamp where data != 'eBay_MSCNTR'";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while(list($data,$timestamp) = mysql_fetch_array($qok))
		{
			$data = str_replace("_"," ",$data);
//			$data_array[$data] = $timestamp;
			array_push($data_array,array("key"=>$data,"value"=>$timestamp));
		}
		$arr = array_chunk($data_array,round(count($data_array)/2));
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="contactDept">Routine</td>
				<td class="colSep">&nbsp;</td>
				<td class="contactDept">&nbsp;Timestamp</td>
			</tr>
			<?foreach($arr[1] as $key => $string){
					
//					if (date("m-d",strtotime($string['value'])) != date("m-d")) {
					if ((time()-strtotime($string['value'])) > 172800) {
						$color = "font-weight:bold;color:red";
					}
					else {
						$color = "";
					}
				?>
				<tr style="<?=$color?>">
					<td><?=$string['key'];?></td>
					<td class="colSep">&nbsp;</td>
					<td><?=str_replace("2013-","",$string['value']);?></td>
				</tr>
			<?}?>
		</table>
		<?
	}
	function getCustomScriptsMiddle() {
		$res = array();
/*
		$query = "select id from bulkship order by id desc limit 0, 1";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($idvalue) = mysql_fetch_array($qok);
		array_push($res,array("name"=>"Last Bulk Ship ID","values"=>$idvalue));
//		------------------------------------//
		$query = "select title, status, tstamp from background_jobs";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while(list($title,$status) = mysql_fetch_array($qok))
		{ $yamama++; }
		array_push($res,array("name"=>"eBay R/C/A","values"=>$yamama));
*/
//		------------------------------------//		
		$autokit = str_replace("EST 2013","",file_get_contents("/tmp/.akit.monitor"));
		array_push($res,array("name"=>"Autokit","values"=>$autokit));
//		------------------------------------//		
		$shop503 = str_replace("EST 2013","",file_get_contents("/tmp/.shop.503"));
		array_push($res,array("name"=>"SHOP orders","values"=>$shop503));
//		------------------------------------//		
		$query = "select value from nxoi_monitor";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($nxoi) = mysql_fetch_array($qok);
		array_push($res,array("name"=>"NX orders","values"=>$nxoi));
//		------------------------------------//		
		$query = "select count(*) from product_list where kit = '1' and visible = '1'";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($viskits) = mysql_fetch_array($qok);
		array_push($res,array("name"=>"Vis Kits","values"=>$viskits));
//		------------------------------------//		
/*
		$buyzip = str_replace("EST 2013","",file_get_contents("/tmp/.buyzip")); 
		if($buyzip =="") { $buyzip = '...'; }
		array_push($res,array("name"=>"Buy.zip","values"=>$buyzip));
*/		
//		------------------------------------//	
		$lalala = str_replace("EST 2013","",file_get_contents("/tmp/.recache_queue"));
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") { $lalala = '...'; }
		array_push($res,array("name"=>"Onhold/Comm","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",file_get_contents("/tmp/feedback_routine.amz"));
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") { $lalala = '...'; }
		array_push($res,array("name"=>"AMZ FDBCK","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",file_get_contents("/tmp/.update_orders_stock_status"));  	
		$lalala = str_replace("EST 2012","",$lalala);
		array_push($res,array("name"=>"N Ord Stock Filter","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ebay_test`);//str_replace("2013","",file_get_contents("/tmp/ebay_test_ace"));
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {	$lalala = '...'; }
		array_push($res,array("name"=>"eBay RCntr","values"=>$lalala));
//		------------------------------------//
		$estimated_fees = file_get_contents('/tmp/estimated_fba_fees');
		array_push($res,array('name'=>'FBA Preview','values'=>$estimated_fees));
//		------------------------------------//
		$bt_kit_100_qty = file_get_contents('/tmp/amz_kit_qty_updater');
		array_push($res,array('name'=>'BT Kit 100 Qty','values'=>$bt_kit_100_qty));
//		------------------------------------//
		$sellery = file_get_contents('/tmp/sellery_result');
		array_push($res,array('name'=>'Sellery','values'=>$sellery));
//		------------------------------------//
		$reorder = file_get_contents('/tmp/ecom_reorder');
		array_push($res,array('name'=>'Reord Email','values'=>$reorder));
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="contactDept">Identification</td>
				<td class="colSep">&nbsp;</td>
				<td class="contactDept">&nbsp;Value</td>
			</tr>
			<?
				foreach ($res as $k=>$v) {
					if ($v['name'] != 'NX orders' && $v['name'] != 'Vis Kits') {
//						if (date("m-d",strtotime($v['values'])) != date("m-d")) {
						if ((time()-strtotime($v['values'])) > 172800) {
							$color = "font-weight:bold;color:red";
						}
						else {
							$color = "";
						}
					}
				?>
					<tr style="<?=$color?>">
						<td><?=$v['name']?></td>
						<td class="colSep">&nbsp;</td>
						<td>&nbsp;<?=$v['values'];?></td>
					</tr>
				<?
				}
			?>
		</table>		
		<?
	}
	function getCustomScriptsRight() {
		$res = array();
/*
		$lalala = str_replace("EST 2013","",`cat /tmp/ebay_audit_reverse`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"eBay reverse audit","values"=>$lalala));
*/		
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/multi_vis_ecom`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Multi-Vis-ecom","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/amzwhatever`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"ASIN Compettition grab","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/amazon_price_matcher`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Amz/Web $ matcher","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/abe_hotlist`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Abes eb/amz hot list","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/auto_bt_feeds`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Auto BT - Feeds","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/duplicated_sku`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"BT Dupe SKU Fix","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/amz_max_dave_ignore_price`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Max/Dave AMZ price ignore","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/sp_managed_price`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"SP $ Match","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ecom_clean_order`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Pship coup,gr,buy","values"=>$lalala));
//		------------------------------------//
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="contactDept">Identification</td>
				<td class="colSep">&nbsp;</td>
				<td class="contactDept">&nbsp;Value</td>
			</tr>
			<?
				foreach ($res as $k=>$v) {
//					if (date("m-d",strtotime($v['values'])) != date("m-d")) {
					if ((time()-strtotime($v['values'])) > 172800) {
						$color = "font-weight:bold;color:red";
					}
					else {
						$color = "";
					}
				?>
					<tr style="<?=$color?>">
						<td><?=$v['name']?></td>
						<td class="colSep">&nbsp;</td>
						<td>&nbsp;<?=$v['values'];?></td>
					</tr>
				<?
				}
			?>
		</table>		
		<?
	}
	function getCustomScriptsRight2() {
		$res = array();
/*
		$lalala = str_replace("EST 2013","",`cat /tmp/ebay_audit_reverse`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"eBay reverse audit","values"=>$lalala));
*/		
		$lalala = str_replace("EST 2013","",`cat /tmp/ebay_test_ace`);//str_replace("2013","",file_get_contents("/tmp/ebay_test_ace"));
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") { 	$lalala = '...'; }
		array_push($res,array("name"=>"ebay_test_ace","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/related_skus`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Related","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/amz_can_note`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"AMZ CAN Note","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ebay_audit`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"eBay Audit","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ecom_competition_crawl_amz`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"BT Comp list","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ingram_cleaner`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Ing Non Auth Cln","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ecom_fba_missing_report`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"FBA no list","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ecom_mass_price_update_cron`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Stan Child UPDT","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/ecom_credit_request`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"UPG Credit","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/amazon_cf_ignore`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"SP CF Ignore","values"=>$lalala));
//		------------------------------------//
		$lalala = str_replace("EST 2013","",`cat /tmp/_amazonca_feedback`);
		$lalala = str_replace("EST 2012","",$lalala);
		if($lalala =="") {$lalala = '...'; }
		array_push($res,array("name"=>"Can Fdbck","values"=>$lalala));
//		------------------------------------//
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="contactDept">Identification</td>
				<td class="colSep">&nbsp;</td>
				<td class="contactDept">&nbsp;Value</td>
			</tr>
			<?
				foreach ($res as $k=>$v) {
//					if (date("m-d",strtotime($v['values'])) != date("m-d")) {
					if ((time()-strtotime($v['values'])) > 172800) {
						$color = "font-weight:bold;color:red";
					}
					else {
						$color = "";
					}
				?>
					<tr style="<?=$color?>">
						<td><?=$v['name']?></td>
						<td class="colSep">&nbsp;</td>
						<td>&nbsp;<?=$v['values'];?></td>
					</tr>
				<?
				}
			?>
		</table>		
		<?
	}
	function getAmazonScripts() {
		$feedList = array();
		$feedList['name'][] = "Base";
		$feedList['enum'][] = "BASE";
		
		$feedList['name'][] = "Inventory";
		$feedList['enum'][] = "INVENTORY";
		
		$feedList['name'][] = "Pricing";
		$feedList['enum'][] = "PRICE";
		
		$feedList['name'][] = "Image";
		$feedList['enum'][] = "IMAGE";
		
		$feedList['name'][] = "Fulfillment";
		$feedList['enum'][] = "FULFILLMENT";
		
		$feedList['name'][] = "Shipping Override";
		$feedList['enum'][] = "SHIPPING";
		
		$feedList['name'][] = "Orders Report";
		$feedList['enum'][] = "ORDERS";
		
		$feedList['name'][] = "Convert to AFN";
		$feedList['enum'][] = "CONVERTSKUSAFN";
		
		$feedList['name'][] = "Convert to MFN";
		$feedList['enum'][] = "CONVERTSKUSMFN";
		
		$feedList['name'][] = "Listings Report";
		$feedList['enum'][] = "REPORT";
		
		$feedList['name'][] = "Unshipped Orders Report";
		$feedList['enum'][] = "UNSHIPPED";
		
		$feedList['name'][] = "Checkout by Amazon";
		$feedList['enum'][] = "CBA";
		
		$feedList['name'][] = "Order Adjustment";
		$feedList['enum'][] = "ORDERADJUSTMENT";
		?>
		
		  <table cellpadding="" cellspacing="0" border="0">
				<tr>
					<td colspan="7"><B><i>Amazon</i></B></td>
				</tr>		
		  		<tr>
					<td><B>ID</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Name</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Last Ran</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Complete</B></td>
				</tr>
				<?
				for($xx = 0; $xx < count($feedList['name']); $xx++)
				{
					$query = "select id, tstamp, reportid, complete from amazon_feed_report where type = '".$feedList['enum'][$xx]."' order by tstamp desc limit 0, 1";
					$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
					if(mysql_num_rows($qok) <= 0)
					{
		
					}
					else
					{
						list($id, $tstamp, $reportid, $complete) = mysql_fetch_array($qok);
						$feedage = (time() - strtotime($tstamp)) / 60;
						?>
						<tr>
							<td><?=$id;?></td>
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=$feedList['name'][$xx];?></td>	
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=date("m/d/Y H:i:s", strtotime($tstamp));?> 
								<?
								if($complete == 0)
								if($feedage > 15) echo "(<font color='#ff0000'><B>".number_format($feedage, 2)." min ago</B></font>)";else echo "(".number_format($feedage, 2)." min ago)";
								?>
							</td>
							<td class="colSep">&nbsp;</td>
							<td <?if($complete == 0) echo "bgcolor='#ffffee'";?>><?if($complete == 1) echo "YES"; else echo "Pending";?></td>
						</tr>
						<?
					}
				}
				?>
				
		</table>
		<?
	}
	function getAmazonCanadaScripts() {
		$feedList = array();
		$feedList['name'][] = "Base";
		$feedList['enum'][] = "BASE";
		
		$feedList['name'][] = "Inventory";
		$feedList['enum'][] = "INVENTORY";
		
		$feedList['name'][] = "Pricing";
		$feedList['enum'][] = "PRICE";
		
		$feedList['name'][] = "Image";
		$feedList['enum'][] = "IMAGE";
		
		$feedList['name'][] = "Fulfillment";
		$feedList['enum'][] = "FULFILLMENT";
		
		$feedList['name'][] = "Shipping Override";
		$feedList['enum'][] = "SHIPPING";
		
		$feedList['name'][] = "Orders Report";
		$feedList['enum'][] = "ORDERS";
		
		$feedList['name'][] = "Convert to AFN";
		$feedList['enum'][] = "CONVERTSKUSAFN";
		
		$feedList['name'][] = "Convert to MFN";
		$feedList['enum'][] = "CONVERTSKUSMFN";
		
		$feedList['name'][] = "Listings Report";
		$feedList['enum'][] = "REPORT";
		
		$feedList['name'][] = "Unshipped Orders Report";
		$feedList['enum'][] = "UNSHIPPED";
		
		$feedList['name'][] = "Checkout by Amazon";
		$feedList['enum'][] = "CBA";
		
		$feedList['name'][] = "Order Adjustment";
		$feedList['enum'][] = "ORDERADJUSTMENT";
		?>
		
		  <table cellpadding="" cellspacing="0" border="0">
				<tr>
					<td colspan="7"><B><i>Amazon Canada</i></B></td>
				</tr>		
		  		<tr>
					<td><B>ID</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Name</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Last Ran</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Complete</B></td>
				</tr>
				<?
				for($xx = 0; $xx < count($feedList['name']); $xx++)
				{
					$query = "select id, tstamp, reportid, complete from amazon_ca_feed_report where type = '".$feedList['enum'][$xx]."' order by tstamp desc limit 0, 1";
					$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
					if(mysql_num_rows($qok) <= 0)
					{
		
					}
					else
					{
						list($id, $tstamp, $reportid, $complete) = mysql_fetch_array($qok);
						$feedage = (time() - strtotime($tstamp)) / 60;
						?>
						<tr>
							<td><?=$id;?></td>
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=$feedList['name'][$xx];?></td>	
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=date("m/d/Y H:i:s", strtotime($tstamp));?> 
								<?
								if($complete == 0)
								if($feedage > 15) echo "(<font color='#ff0000'><B>".number_format($feedage, 2)." min ago</B></font>)";else echo "(".number_format($feedage, 2)." min ago)";
								?>
							</td>
							<td class="colSep">&nbsp;</td>
							<td <?if($complete == 0) echo "bgcolor='#ffffee'";?>><?if($complete == 1) echo "YES"; else echo "Pending";?></td>
						</tr>
						<?
					}
				}
				?>
				
		</table>
		<?
	}
	function getAmazonMaxScripts() {
		$feedList = array();
		$feedList['name'][] = "Base";
		$feedList['enum'][] = "BASE";
		
		$feedList['name'][] = "Inventory";
		$feedList['enum'][] = "INVENTORY";
		
		$feedList['name'][] = "Pricing";
		$feedList['enum'][] = "PRICE";
		
		$feedList['name'][] = "Image";
		$feedList['enum'][] = "IMAGE";
		
		$feedList['name'][] = "Fulfillment";
		$feedList['enum'][] = "FULFILLMENT";
		
		$feedList['name'][] = "Shipping Override";
		$feedList['enum'][] = "SHIPPING";
		
		$feedList['name'][] = "Orders Report";
		$feedList['enum'][] = "ORDERS";
		
		$feedList['name'][] = "Convert to AFN";
		$feedList['enum'][] = "CONVERTSKUSAFN";
		
		$feedList['name'][] = "Convert to MFN";
		$feedList['enum'][] = "CONVERTSKUSMFN";
		
		$feedList['name'][] = "Listings Report";
		$feedList['enum'][] = "REPORT";
		
		$feedList['name'][] = "Unshipped Orders Report";
		$feedList['enum'][] = "UNSHIPPED";
		
		$feedList['name'][] = "Checkout by Amazon";
		$feedList['enum'][] = "CBA";
		
		$feedList['name'][] = "Order Adjustment";
		$feedList['enum'][] = "ORDERADJUSTMENT";
		?>
		
		  <table cellpadding="" cellspacing="0" border="0">
				<tr>
					<td colspan="7"><B><i>Amazon MAX</i></B></td>
				</tr>		
		  		<tr>
					<td><B>ID</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Name</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Last Ran</B></td>
					<td class="colSep">&nbsp;</td>
					<td><B>Complete</B></td>
				</tr>
				<?
				for($xx = 0; $xx < count($feedList['name']); $xx++)
				{
					$query = "select id, tstamp, reportid, complete from amazonmax_feed_report where type = '".$feedList['enum'][$xx]."' order by tstamp desc limit 0, 1";
					$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
					if(mysql_num_rows($qok) <= 0)
					{
		
					}
					else
					{
						list($id, $tstamp, $reportid, $complete) = mysql_fetch_array($qok);
						$feedage = (time() - strtotime($tstamp)) / 60;
						?>
						<tr>
							<td><?=$id;?></td>
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=$feedList['name'][$xx];?></td>	
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=date("m/d/Y H:i:s", strtotime($tstamp));?> 
								<?
								if($complete == 0)
								if($feedage > 15) echo "(<font color='#ff0000'><B>".number_format($feedage, 2)." min ago</B></font>)";else echo "(".number_format($feedage, 2)." min ago)";
								?>
							</td>
							<td class="colSep">&nbsp;</td>
							<td <?if($complete == 0) echo "bgcolor='#ffffee'";?>><?if($complete == 1) echo "YES"; else echo "Pending";?></td>
						</tr>
						<?
					}
				}
				?>
				
		</table>
		<?
	}
	function getFBAScripts() {
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="7"><B><i>FBA</i></B></td>
			</tr>
			<tr>
				<td class="helpHed"><B>ID</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Type</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>TStamp</B></td>
			<!--	<td class="helpHed"><B>Report ID</B></td>
				<td class="helpHed"><B>Request ID</B></td>-->
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Complete</B></td>
				<!-- <td class="helpHed"><B>Filename</B></td> -->
			</tr>
				<?			
				$query = "select id, type, tstamp, reportid, requestid, complete, filename from fba_feed_report order by tstamp";
				$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
				if(mysql_num_rows($qok) <= 0)
				{}else
				{
					//while(list($id, $type, $tstamp, $reportid, $requestid, $complete, $filename) = mysql_fetch_array($qok))
					while(list($id, $type, $tstamp, $reportid, $requestid, $complete) = mysql_fetch_array($qok))
					{
						if($complete == "1") { $complete = "YES"; } else { $complete = "NO"; }
//						$fba[] = "<tr><td class=sup>$id</td><td class=sup>$type</td><td class=sup>$tstamp</td><td class=sup>$reportid</td><td class=sup>$requestid</td><td class=sup>$complete</td>";
						$fba[] = "<tr><td class=sup>$id</td><td class=colSep>&nbsp;</td><td class=sup>$type</td><td class=colSep>&nbsp;</td><td class=sup>$tstamp</td><td class=colSep>&nbsp;</td><td class=sup>$complete</td>";
					}
	
					$fba = array_reverse($fba);
	
					foreach($fba as $string)
					{
						if($fba_c < 10)
						{
							$fba_c++; $fresh[] = $string;
						}
					}
	
					$fresh = array_reverse($fresh);
	
					foreach($fresh as $string) { echo $string; }
	
	
				}
				?>		
			</table>
		<?
	}
	function getFBACanadaScripts() {
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="7"><B><i>FBA Canada</i></B></td>
			</tr>
			<tr>
				<td class="helpHed"><B>ID</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Type</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>TStamp</B></td>
			<!--	<td class="helpHed"><B>Report ID</B></td>
				<td class="helpHed"><B>Request ID</B></td>-->
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Complete</B></td>
				<!-- <td class="helpHed"><B>Filename</B></td> -->
			</tr>
				<?			
				$query = "select id, type, tstamp, reportid, requestid, complete, filename from fba_ca_feed_report order by tstamp";
				$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
				if(mysql_num_rows($qok) <= 0)
				{}else
				{
					//while(list($id, $type, $tstamp, $reportid, $requestid, $complete, $filename) = mysql_fetch_array($qok))
					while(list($id, $type, $tstamp, $reportid, $requestid, $complete) = mysql_fetch_array($qok))
					{
						if($complete == "1") { $complete = "YES"; } else { $complete = "NO"; }
						//$fba[] = "<tr><td class=sup>$id</td><td class=sup>$type</td><td class=sup>$tstamp</td><td class=sup>$reportid</td><td class=sup>$requestid</td><td class=sup>$complete</td><td class=sup>$filename</td>";
						$fba[] = "<tr><td class=sup>$id</td><td class=colSep>&nbsp;</td><td class=sup>$type</td><td class=colSep>&nbsp;</td><td class=sup>$tstamp</td><td class=colSep>&nbsp;</td><td class=sup>$complete</td>";
					}
	
					$fba = array_reverse($fba);
	
					foreach($fba as $string)
					{
						if($fba_c < 10)
						{
							$fba_c++; $fresh[] = $string;
						}
					}
	
					$fresh = array_reverse($fresh);
	
					foreach($fresh as $string) { echo $string; }
	
	
				}
				?>		
			</table>
		<?
	}
	function getFBAMaxScripts() {
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="7"><B><i>FBA Max</i></B></td>
			</tr>
			<tr>
				<td class="helpHed"><B>ID</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Type</B></td>
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>TStamp</B></td>
				<!--<td class="helpHed"><B>Report ID</B></td>
				<td class="helpHed"><B>Request ID</B></td>-->
				<td class="colSep">&nbsp;</td>
				<td class="helpHed"><B>Complete</B></td>
				<!-- <td class="helpHed"><B>Filename</B></td> -->
			</tr>
				<?			
				$query = "select id, type, tstamp, reportid, requestid, complete, filename from fba_max_feed_report order by tstamp";
				$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
				if(mysql_num_rows($qok) <= 0)
				{}else
				{
					//while(list($id, $type, $tstamp, $reportid, $requestid, $complete, $filename) = mysql_fetch_array($qok))
					while(list($id, $type, $tstamp, $reportid, $requestid, $complete) = mysql_fetch_array($qok))
					{
						if($complete == "1") { $complete = "YES"; } else { $complete = "NO"; }
						//$fba[] = "<tr><td class=sup>$id</td><td class=sup>$type</td><td class=sup>$tstamp</td><td class=sup>$reportid</td><td class=sup>$requestid</td><td class=sup>$complete</td><td class=sup>$filename</td>";
						$fba[] = "<tr><td class=sup>$id</td><td class=colSep>&nbsp;</td><td class=sup>$type</td><td class=colSep>&nbsp;</td><td class=sup>$tstamp</td><td class=colSep>&nbsp;</td><td class=sup>$complete</td>";
					}
	
					$fba = array_reverse($fba);
	
					foreach($fba as $string)
					{
						if($fba_c < 10)
						{
							$fba_c++; $fresh[] = $string;
						}
					}
	
					$fresh = array_reverse($fresh);
	
					foreach($fresh as $string) { echo $string; }
	
	
				}
				?>		
			</table>
		<?
	}
	function getFlowScripts() {
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td><b>Routine</b></td>
				<td><b>Status</b></td>
			</tr>
			<tr>
				<td>ebay revise</td>
				<td><?=$this->statusScripts('revise');?></td>
			</tr>
			<tr>
				<td>ebay feedback</td>
				<td><?=$this->statusScripts('ebay_feedback_import');?></td>
			</tr>
			<tr>
				<td>ebay listings</td>
				<td><?=$this->statusScripts('ebay_listings_upload');?></td>
			</tr>
			<tr>
				<td>ebay multilistings</td>
				<td><?=$this->statusScripts('ebay_manage_multi');?></td>
			</tr>
			<tr>
				<td>ebay order</td>
				<td><?=$this->statusScripts('ebay_order_import');?></td>
			</tr>
		</table>
		<?
	}
	
	function statusScripts($code)
	{
		$green = '<input type="button" name="groovybtn1" class="groovybutton1" value="Syncing&nbsp;" title=""   onMouseOver="goLite1(this.form.name,this.name)"   onMouseOut="goDim1(this.form.name,this.name)">';
		$red = '<input   type="button"   name="groovybtn1"   class="groovybutton"   value="Sleeping"   title=""   onMouseOver="goLite(this.form.name,this.name)"   onMouseOut="goDim(this.form.name,this.name)">';
		$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
		if($ROOT_DIR == "") { exit("ROOT_DIR"); }

		if($code == "") { exit("ERR: func() code"); }
	
		if($code == "who") 	{	echo "<pre>\n"; echo passthru("w") ; echo "</pre>";	}
	
		if($code == "pslist") 	{	echo "<pre>\n"; echo passthru("ps auxwww") ; echo "</pre>";	}
	
		if($code == "orderflow") 
		{
			
			$cmdaaa = "ps auxwww | grep order_flow_pump | grep -v grep";
			$orderflowdata = `$cmdaaa`;
			$od = explode(" ",$orderflowdata);
			foreach($od as $key => $string_v)
			{
				if($string_v == "/home/httpd/ecomelectronics.com/mgt/scripts/order_flow_pump.phtml") { $green_is_go = 1; }
			}
			
			if($green_is_go == "1")
			{
			   //echo "GREEN";
			   echo '<input type="button" name="groovybtn1" class="groovybutton1" value="Syncing&nbsp;" title=""   onMouseOver="goLite1(this.form.name,this.name)"   onMouseOut="goDim1(this.form.name,this.name)">';
			}
			else
			{
				//echo "RED: '$ordeflowdata'";
				echo  '<input   type="button"   name="groovybtn1"   class="groovybutton"   value="Sleeping"   title=""   onMouseOver="goLite(this.form.name,this.name)"   onMouseOut="goDim(this.form.name,this.name)">';
			}
		}
	
		if($code == "genthumbs") { passthru($ROOT_DIR."/mgt/security/monitor/sh/genthumbs.sh"); }
	
		if($code == "buildfeed") { passthru($ROOT_DIR."/mgt/security/monitor/sh/buildfeed.sh"); }
	
		if($code == "sfeed") { passthru($ROOT_DIR."/mgt/security/monitor/sh/sfeed.sh"); }
	
		if($code == "etil") { passthru($ROOT_DIR."/mgt/security/monitor/sh/etilize.sh"); }
	
		if($code == "who") 	{	echo "<pre>\n"; echo passthru("w") ; echo "</pre>";	}
	
		if($code == "pslist") 	{	echo "<pre>\n"; echo passthru("ps auxwww") ; echo "</pre>";	}
	
	
		if($code == "revise")
		{
			if(file_exists("/tmp/revise_0.lock"))
			{
				echo $green;
	
			}
			else
			{
				echo $red;
			}
	
		}
		if($code == "ebay_feedback_import")
		{
			if(file_exists("/tmp/ebay_feedback_import.lock"))
			{
				echo $green;
			}
			else
			{
				echo $red;
			}
		}
		if($code == "ebay_listings_upload")
		{
			if(file_exists("/tmp/ebay_listings_upload.lock"))
			{
				echo $green;
			}
			else
			{
				echo $red;
			}
		}
		if($code == "ebay_manage_multi")
		{
			if(file_exists("/tmp/ebay_manage_multi.lock"))
			{
				echo $green;
			}
			else
			{
				echo $red;
			}
		}
		if($code == "ebay_order_import")
		{
			if(file_exists("/tmp/ebay_order_import.lock"))
			{
				echo $green;
			}
			else
			{
				echo $red;
			}
	
		}
	
	}
	function getBuyScripts() {
		$feedList = array();
		
		$feedList['name'][] = "New";
		$feedList['enum'][] = "NEW";
		
		$feedList['name'][] = "Base";
		$feedList['enum'][] = "BASE";
		
		$feedList['name'][] = "Inventory";
		$feedList['enum'][] = "INVENTORY";
		
		$feedList['name'][] = "Pricing";
		$feedList['enum'][] = "PRICE";
		
		$feedList['name'][] = "Image";
		$feedList['enum'][] = "IMAGE";
		
		$feedList['name'][] = "Fulfillment";
		$feedList['enum'][] = "FULFILLMENT";
		
		$feedList['name'][] = "Shipping Override";
		$feedList['enum'][] = "SHIPPING";
		
		$feedList['name'][] = "Orders Report";
		$feedList['enum'][] = "ORDERS";
		
		
		$feedList['name'][] = "Listings Report";
		$feedList['enum'][] = "REPORT";
		
		$feedList['name'][] = "Unshipped Orders Report";
		$feedList['enum'][] = "UNSHIPPED";
		
		$feedList['name'][] = "Listing Export";
		$feedList['enum'][] = "LISTINGEXPORT";
		?>
		
		  <table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class=helpHed><B>ID</B></td>
					<td class="colSep">&nbsp;</td>
					<td class=helpHed><B>Name</B></td>
					<td class="colSep">&nbsp;</td>
					<!--<td class=helpHed><B>Last ReportID</B></td>
					<td class="colSep">&nbsp;</td>-->
					<td class=helpHed><B>Last Ran</B></td>
					<td class="colSep">&nbsp;</td>
					<td class=helpHed><B>Complete</B></td>
				</tr>
		
				<?
				for($xx = 0; $xx < count($feedList['name']); $xx++)
				{
					$query = "select id, tstamp, reportid, complete from buy_feed_report where type = '".$feedList['enum'][$xx]."' order by tstamp desc limit 0, 1";
		
					$qok = mysql_db_query($this->DB, $query, $this->mysqlid);
		
					if(mysql_num_rows($qok) <= 0)
					{
		
					}
					else
					{
						list($id, $tstamp, $reportid, $complete) = mysql_fetch_array($qok);
		
						$feedage = (time() - strtotime($tstamp)) / 60;
		
						?>
						<tr>
							<td class=sup height=21><?=$id;?></td>
							<td class="colSep">&nbsp;</td>
							<td nowrap><?=$feedList['name'][$xx];?></td>	
							<td class="colSep">&nbsp;</td>
<!--							<td><?=$reportid;?></td>-->
							<td nowrap><?=date("m/d/Y H:i:s", strtotime($tstamp));?> 
								<?
								if($complete == 0)
								if($feedage > 15) echo "(<font color='#ff0000'><B>".number_format($feedage, 2)." min ago</B></font>)";else echo "(".number_format($feedage, 2)." min ago)";
								?>
							</td>
							<td class="colSep">&nbsp;</td>
							<td <?if($complete == 0) echo "bgcolor='#ffffee'";?>><?if($complete == 1) echo "YES"; else echo "Pending";?></td>
						</tr>
						<?
					}
				}
				?>
				
		</table>
		<?
	}
	
	function getVendorHTML() {
		?>
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td colspan="7" class="helpHed"><?=$header_date;?> </td> </tr>
			<tr>
			    <td class="helpHed"><?if($my_head_rs['VID'] == '1') { ?><a href="/mgt/security/monitor/svendrs.php">PEEK</a><?}?></td>
			    <td class=\"colSep\">&nbsp;</td>
				<td class="helpHed">Vendor</td>
				<td class=\"colSep\">&nbsp;</td>
				<td class="helpHed">Feed</td>
				<td class=\"colSep\">&nbsp;</td>
				<td class="helpHed">Import</td>
				<td class=\"colSep\">&nbsp;</td>
				<td class="helpHed">Updated</td>
				<td class=\"colSep\">&nbsp;</td>
				<td class="helpHed">Created</td>
				<td class="helpHed">Status</td>
			</tr>
		<?
		$no_green_light = 0;
		$query = "select filedate,lastsync,vendorname,updated,created from vendor_sync order by lastsync";
		if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while($row = mysql_fetch_array($qok, MYSQL_ASSOC))
		{
			$filedate = "$test"."{$row['filedate']}";
			$lastsync = "$test"."{$row['lastsync']}";
			$vendorname = "{$row['vendorname']}";
			$updated  = "$test"."{$row['updated']}";
			$created  = "$test"."{$row['created']}";
		
			$query = "select vendorid from vendors where vendorname = '$vendorname'";
			if(!$qok_vn = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($vn_patch) = mysql_fetch_array($qok_vn);
		
			$status = vendor_status2($vn_patch);
		
		
		
			$status = str_replace("\n","",$status);
			$status = str_replace("\r","",$status);
			$status = trim($status);
			if(preg_match("/sleeping/",$status)) { $status_bug = '<td><input   type="button"   name="groovybtn1"   class="groovybutton"   value="Sleeping"   title=""   onMouseOver="goLite(this.form.name,this.name)"   onMouseOut="goDim(this.form.name,this.name)"></td>'; }
			if(preg_match("/awake/",$status)) { $status_bug = '<td><input type="button" name="groovybtn1" class="groovybutton1" value="Syncing&nbsp;" title=""   onMouseOver="goLite1(this.form.name,this.name)"   onMouseOut="goDim1(this.form.name,this.name)"></td>'; $no_green_light = 1; }
		
			$lastsync = str_replace("2013-","",$lastsync);
		
			$today_lastsync_date = date("m-d");
		
			$lastsync_day = explode(" ",$lastsync);
			$lastsync_day = $lastsync_day[0];
		
		
			if($lastsync_day == $today_lastsync_date) { $lastsync = '<b>'.$lastsync.'</b>'; }
		
			if(file_exists("/tmp/vendor_ts/$vn_patch.dat"))
			{
		
				$isrc_url = "<img src=/mgt/security/monitor/happy.png height=12>";
		
				$vn_display_line = " [<b><font color=green>$vn_patch</font></b>]";
		
				$vn_display_line = "";
		
				$vn_patch_show = "$isrc_url"."$vn_display_line";
		
				$vn_patch_show_data = file_get_contents("/tmp/vendor_ts/$vn_patch.dat");
		
			}
			else
			{
				$isrc_url = "<img src=/mgt/security/monitor/sad.gif height=13>";
		
				$vn_display_line = " [$vn_patch]";
		
				$vn_display_line = "";
		
				$vn_patch_show = "$isrc_url"."$vn_display_line";
		
			}
		
		
			if($vn_patch == "145") { $vn_patch_show = "<img src=/mgt/security/monitor/loop.png height=13>"; }
		
		
		
			//($vn_patch)
			$hoho ='';
			if($my_head_rs['VID'] == '1') { $harhar = "($vn_patch)"; $hoho = $vn_patch_show_data; }
		
			echo '<tr>';
			echo "<td class=\"sup\" alt=\"trottle_status\" nowrap>$vn_patch_show $hoho</td><td class=\"colSep\">&nbsp;</td>";
			echo "<td class=\"sup\" alt=\"$vn_patch\" nowrap>$vendorname $harhar</td><td class=\"colSep\">&nbsp;</td>";
			echo "<td nowrap>$filedate</td><td class=\"colSep\">&nbsp;</td>";
			echo "<td nowrap>$lastsync</td><td class=\"colSep\">&nbsp;</td>";
			echo "<td nowrap>$updated</td><td class=\"colSep\">&nbsp;</td>";
			echo "<td nowrap>$created</td>";
			echo "$status_bug";
			echo '</tr>';
		}
		if(file_exists('/tmp/vendor_debug/VENDOR_HEALTH')) { $vstat_show = file_get_contents("/tmp/vendor_debug/VENDOR_HEALTH"); } else { $vstat_show = ''; }
		if(file_exists('/tmp/vendor_locks/.VENDORS.lock'))
		{ $vstat_lock = "<img src='http://66.132.174.237/mgt/security/stock_lock.png' height='20' ALT=\"LOCKED\">";
		$no_lock = 1;
		$vstat_bar = "<img src='http://66.132.174.237/mgt/security/sync.gif' ALT='' height='9'>";
		}
		else
		{
			$vstat_lock = "<img src='http://66.132.174.237/mgt/security/stock_lock_open.png' height='20' ALT=\"UNLOCKED\">";
			$vstat_bar = "";
			$no_lock = 0;
		
		}
		
		if($vstat_show == "Sourcing\n")
		{
			$vstat_show_s = "<td></td><td><img src='http://66.132.174.237/mgt/security/data.png' height='12' ALT=\"Sourcing\"> <b>inventory</b></td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Data\"> product data</td>";
			$status = "awake";
		}
		
		
		if($vstat_show == "Data\n")
		{
			//$vstat_show = "<img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Data\"> product data";
			$vstat_show_s = "<td></td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Sourcing\"> inventory</td><td><img src='http://66.132.174.237/mgt/security/data.png' height='12' ALT=\"Data\"> <b>product data</b></td>";
			$status = "awake";
		}
		
		if($vstat_show == "")
		{
			$vstat_show_s = "<td></td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Sourcing\"> inventory</td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Data\"> product data</td>";
			$status = "sleeping";
		}
		
		if($vstat_show == "Success\n")
		{
			$vstat_show_s = "<td></td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Sourcing\"> inventory</td><td><img src='http://66.132.174.237/mgt/security/data2.png' height='12' ALT=\"Data\"> product data</td>";
			$status = "sleeping";
		}
		
		if(preg_match("/sleeping/",$status)) { $status_bug = '<td><input   type="button"   name="groovybtn1"   class="groovybutton"   value="Sleeping"   title=""   onMouseOver="goLite(this.form.name,this.name)"   onMouseOut="goDim(this.form.name,this.name)"></td>'; }
		if(preg_match("/awake/",$status)) { $status_bug = '<td><input type="button" name="groovybtn1" class="groovybutton1" value="Syncing&nbsp;" title=""   onMouseOver="goLite1(this.form.name,this.name)"   onMouseOut="goDim1(this.form.name,this.name)"></td>'; }
		$status_bug = '';
		
		$no_c = $no_green_light + $no_lock;
		if($no_c !="0" && $no_c !="2")
		{
		
			$status = vendor_status2('17');
			$status = str_replace("\n","",$status);
			$status = str_replace("\r","",$status);
			$status = trim($status);
			if(!preg_match("/awake/",$status)) {
				if(!file_exists("/tmp/vendor_locks/.ING-TD.lock")) {
					$warning = "<td></td><td><img src='http://66.132.174.237/mgt/security/r.gif' ALT=\"INSPECTION MAY BE NECESSARY TO PREVENT FAILURE!\"></td>";
				}
			}
		}

		
		?>
			<tr>
				<td><?=$vstat_lock;?></td>
				<?=$vstat_show_s;?>
				<?=$warning;?>
				<?=$status_bug;?>
			</tr>
		</table>
		<?
	}
}

?>
