CREATE TABLE monitor_timestamp (
         id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
         data VARCHAR(100),
         cur_timestamp VARCHAR(100)
       );
Query OK, 0 rows affected (0.00 sec)

mysql> INSERT INTO example_timestamp (data)
            VALUES ('The time of creation is:');
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM example_timestamp;
+----+--------------------------+---------------------+
| id | data                     | cur_timestamp       |
+----+--------------------------+---------------------+
|  1 | The time of creation is: | 2004-12-01 20:37:22 |
+----+--------------------------+---------------------+
1 row in set (0.00 sec)

mysql> UPDATE example_timestamp 
          SET data='The current timestamp is: ' 
        WHERE id=1;
Query OK, 1 row affected (0.03 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM example_timestamp;
+----+---------------------------+---------------------+
| id | data                      | cur_timestamp       |
+----+---------------------------+---------------------+
|  1 | The current timestamp is: | 2004-12-01 20:38:55 |
+----+---------------------------+---------------------+
1 row in set (0.01 sec)


CREATE TABLE monitor_timestamp (
         id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
         data VARCHAR(100),
         cur_timestamp VARCHAR(100)
       );


CREATE TABLE vendor_status (
         id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
         vendorname VARCHAR(100),
         status VARCHAR(100)
       );



CREATE TABLE doba_monitor_timestamp (
         id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
         data VARCHAR(100),
         cur_timestamp VARCHAR(100)
       );
