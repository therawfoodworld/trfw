create table ebay_templates
(
id bigint NOT NULL auto_increment,
title varchar(255),
tstamp datetime,
body longblob,
index(id)
);

drop table ebay_campaigns;
create table ebay_campaigns
(
id bigint NOT NULL auto_increment,
campaign_name varchar(255),
active int NOT NULL default 0,
begindate datetime, #bdate
enddate datetime,	#edate
auto_relist int NOT NULL default 0,
listing_type varchar(255),
item_title varchar(255) default '#producttitle# #sku#',
item_subtitle varchar(255),
ebay_template bigint,
listing_duration varchar(10),
starting_price double(16,2) NOT NULL default 0.00,
buyitnow_price double(16,2) NOT NULL default 0.00,
quantity int NOT NULL default -1,
qty_control int NOT NULL default 100,
campaign_type varchar(25),
pid bigint NOT NULL default 0,
listid bigint NOT NULL default 0,
pricelevel int NOT NULL default 0,
index(id),
index(active, begindate, enddate)
);
alter table ebay_campaigns add hitcounter varchar(30) NOT NULL default '';
alter table ebay_campaigns add bold int NOT NULL default 0;
alter table ebay_campaigns add featured int NOT NULL default 0;
alter table ebay_campaigns add min_profit_pct double(16,2) NOT NULL default 0.00;
alter table ebay_campaigns add min_profit_cur double(16,2) NOT NULL default 0.00;
alter table ebay_campaigns add immediate_pay int NOT NULL default 0;
alter table ebay_campaigns add lastupdate bigint NOT NULL default 0;

# the table to tell us when we need to update all the listings, cuz the campaign changed
create table ebay_campaigns_lastmod
(
campid bigint NOT NULL,
lastupdate bigint NOT NULL,
index(campid)
);

drop table ebay_listings;
create table ebay_listings
(
id bigint NOT NULL auto_increment,
ecampid bigint, #ebay campaign id
pid bigint NOT NULL default 0,
active int NOT NULL default 0, #whether or not its active
ebaylistingid varchar(255),
starttime datetime,
endtime datetime,
pid_lastupdate bigint NOT NULL default 0,
index(id),
index(ecampid, pid, active),
index(endtime),
index(pid_lastupdate)
);


drop table ebay_store_categories;
create table ebay_store_categories
(
catid bigint NOT NULL,
parentid bigint NOT NULL,
catname varchar(255),
ebaycatid bigint NOT NULL,
ebayparentid bigint NOT NULL,
depth int NOT NULL default 0,
index(catid),
index(ebaycatid),
index(parentid),
index(ebayparentid)
);

drop table ebay_errors;
create table ebay_errors
(
id bigint NOT NULL auto_increment,
ebaycampid bigint NOT NULL default 0, #ebay campaign id
pid bigint NOT NULL,
tstamp datetime,
resolved int NOT NULL default 0,
index(id),
index(pid, resolved),
index(tstamp, resolved),
index(ebaycampid)
);

drop table ebay_errors_extended;
create table ebay_errors_extended
(
id bigint NOT NULL auto_increment,
eid bigint NOT NULL,
message longblob,
index(id),
index(eid)
);


create table ebay_orders_uploaded
(
orderid bigint NOT NULL,
tstamp datetime,
index(orderid),
index(tstamp)
);

create table ebay_orders
(
orderid bigint NOT NULL,
listingid bigint NOT NULL,
index(orderid),
index(listingid)
);


drop table ebay_log;
create table ebay_log
(
id bigint NOT NULL auto_increment,
ebcampid bigint NOT NULL,
title varchar(255),
listid bigint NOT NULL default 0,
pid bigint NOT NULL default 0,
tstamp datetime,
userid bigint NOT NULL default -1,
index(id),
index(tstamp),
index(title),
index(listid),
index(pid)
);

drop table ebay_log_extended;
create table ebay_log_extended
(
id bigint NOT NULL,
lid bigint NOT NULL,
type enum('request', 'response'),
data longblob,
index(id),
index(lid),
index(type)
);
alter table ebay_log_extended add index(lid, type);
alter table ebay_log_extended add index(id);
alter table ebay_log_extended change id id bigint NOT NULL auto_increment;

drop table ebay_feedback;
create table ebay_feedback
(
orderid bigint NOT NULL,
ebayacctid bigint NOT NULL,
feedbackid bigint NOT NULL,
txid varchar(50),
score int NOT NULL default 0,
comment varchar(256),
tstamp datetime,
type varchar(50),
username varchar(128),
index(orderid),
index(ebayacctid),
index(feedbackid),
index(txid)
);

alter table customer_accounts add ebayacctid bigint NOT NULL;
alter table customer_accounts change ebayacctid ebayacctid varchar(26);
alter table customer_accounts add index(ebayacctid);

alter table autopackages add listid bigint NOT NULL default 0;


alter table user_permissions add perm_edit_ebay int NOT NULL default 0;
alter table user_permissions add perm_order_editprice int NOT NULL default 0;

alter table autopackages add regenerate int NOT NULL default 0;
alter table lists add exclusive int NOT NULL default 0;
alter table ebay_orders add ebaytxid varchar(255);

################# DATABASE ebay ####################################

create table ebay_categories
(
catid bigint NOT NULL default 0,
catname varchar(255),
parentid bigint NOT NULL default 0,
bestofferenabled int NOT NULL default 0,
autopayenabled int NOT NULL default 0,
categorylevel int NOT NULL default 0,
leafcategory int NOT NULL default 0,
index(catid),
index(parentid)
);


create table ebay_catdetail
(
id bigint NOT NULL auto_increment,
ebcatid bigint NOT NULL default 0,
catalogenabled int NOT NULL default 0, # if its 0 there will be an exclude list in ebay_catdetail_exclude and the rest of the table will be populated
attributesetname varchar(255),
attributesetid bigint NOT NULL default 0,
attributesetversion bigint NOT NULL default 0,
swattributesetname varchar(255),
swattributesetid bigint NOT NULL default 0,
swattributesetversion bigint NOT NULL default 0,
index(id),
index(ebcatid)
);

create table ebay_catdetail_exclude
(
cdid bigint NOT NULL,
excatid bigint,
index(cdid)
);


drop table ebay_item_specifics;
create table ebay_item_specifics
(
id bigint NOT NULL auto_increment,
attributesetid bigint NOT NULL default 0,
attributesetversion bigint NOT NULL default 0,
labelvisible int NOT NULL default 0,
labelid bigint NOT NULL default 0,
edittype int NOT NULL default 0,
label varchar(255) NOT NULL default 0,
type int NOT NULL default 0,
message varchar(255) NOT NULL default '',
index(id),
index(attributesetid, attributesetversion)
);

drop table ebay_item_specifics_values;
create table ebay_item_specifics_values
(
id bigint NOT NULL auto_increment,
attributesetid bigint NOT NULL default 0,
attributesetversion bigint NOT NULL default 0,
isid bigint NOT NULL default 0,
vname varchar(255),
vid bigint,
index(id),
index(isid),
index(isid, vname),
index(attributesetid, attributesetversion)
);
#####################################################################################


select ebay_listings.pid, ebay_listings.ebaylistingid, ebay_listings.ecampid from
ebay_listings, product_list where
ebay_listings.active = 1 AND
ebay_listings.pid = product_list.id AND
ebay_listings.pid_lastupdate != product_list.lastupdate;