-- phpMyAdmin SQL Dump
-- version 2.11.9.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2012 at 03:00 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_buyer_requirement_details`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_buyer_requirement_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `shiptoregistrationcountry` enum('0','1') NOT NULL,
  `minimumfeedbackscore` int(11) NOT NULL,
  `maximumitemrequirements_maximumitemcount` int(11) NOT NULL,
  `maximumitemrequirements_minimumfeedbackscore` int(11) NOT NULL,
  `linkedpaypalaccount` enum('0','1') NOT NULL,
  `maximumunpaiditemstrikesinfo_count` int(11) NOT NULL,
  `maximumunpaiditemstrikesinfo_period` varchar(250) NOT NULL,
  `maximumbuyerpolicyviolations_count` int(11) NOT NULL,
  `maximumbuyerpolicyviolations_period` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_buyer_requirement_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_info`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `autopay` enum('0','1') NOT NULL,
  `buyerprotection` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `currency` varchar(250) NOT NULL,
  `gifticon` enum('0','1') NOT NULL,
  `hitcounter` varchar(250) NOT NULL,
  `Itemid` int(11) NOT NULL,
  `listingduration` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `quantity` int(11) NOT NULL,
  `revisestatus` enum('0','1') NOT NULL,
  `shiptolocations` varchar(250) NOT NULL,
  `site` varchar(250) NOT NULL,
  `subtitle` varchar(250) NOT NULL,
  `timeleft` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `watchcount` int(11) NOT NULL,
  `hitcount` int(11) NOT NULL,
  `sku` bigint(20) NOT NULL,
  `proxyitem` enum('0','1') NOT NULL,
  `buyerguaranteeprice` double(9,2) NOT NULL,
  `buyerguaranteeprice_curencyid` varchar(250) NOT NULL,
  `conditionid` int(11) NOT NULL,
  `conditiondisplayname` varchar(250) NOT NULL,
  `postcheckoutexperienceenabled` enum('0','1') NOT NULL,
  `relistparentid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_info`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_listing_details`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_listing_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  `viewitemurl` varchar(250) NOT NULL,
  `hasunansweredquestions` enum('0','1') NOT NULL,
  `haspublicmessages` enum('0','1') NOT NULL,
  `viewitemurlfornaturalsearch` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_listing_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_picture_details`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_picture_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `galleryurl` varchar(250) NOT NULL,
  `photodisplay` varchar(250) NOT NULL,
  `pictureurl` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_picture_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_primary_category`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_primary_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `categoryname` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_primary_category`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_return_policy`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_return_policy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `refundoption` varchar(250) NOT NULL,
  `refund` varchar(250) NOT NULL,
  `returnswithinoption` varchar(250) NOT NULL,
  `returnswithin` varchar(250) NOT NULL,
  `returnsacceptedoption` varchar(250) NOT NULL,
  `returnsaccepted` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `shippingcostpaidbyoption` varchar(250) NOT NULL,
  `shippingcostpaidby` varchar(250) NOT NULL,
  `restockingfeevalue` varchar(250) NOT NULL,
  `restockingfeevalueoption` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_return_policy`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_selling_status`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_selling_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `bidcount` int(11) NOT NULL,
  `bidincrement` double(9,2) NOT NULL,
  `bidincrement_curencyid` varchar(250) NOT NULL,
  `convertedcurrentprice` double(9,2) NOT NULL,
  `convertedcurrentprice_curencyid` varchar(250) NOT NULL,
  `currentprice` double(9,2) NOT NULL,
  `currentprice_curencyid` varchar(250) NOT NULL,
  `minimumtobid` double(9,2) NOT NULL,
  `minimumtobid_curencyid` varchar(250) NOT NULL,
  `quantitysold` int(11) NOT NULL,
  `secondchanceeligible` enum('0','1') NOT NULL,
  `listingstatus` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_selling_status`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_shipping_details`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_shipping_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `shippingdiscountprofileid` int(11) NOT NULL,
  `internationalshippingdiscountprofileid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_shipping_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_sku_compare`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_sku_compare` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `sku` bigint(20) NOT NULL,
  `listing_details_id` bigint(20) NOT NULL,
  `sku_exist` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_sku_compare`
--


-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_store_front`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_store_front` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_id` bigint(20) NOT NULL,
  `storecategoryid` int(11) NOT NULL,
  `storecategory2id` int(11) NOT NULL,
  `storeurl` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ebay_audit_store_front`
--

