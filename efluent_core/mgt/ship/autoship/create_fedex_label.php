<?
REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
ini_set('display_errors',1);
error_reporting(E_ALL^E_NOTICE);
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************************************************/

$im_w = 1200;
$im_h = 880;
$im_border = 1;
$font = 'arial.ttf';
$im = imagecreatetruecolor($im_w, $im_h);
$color_white = imagecolorallocate($im, 255, 255, 255);
$color_black = imagecolorallocate($im, 0, 0, 0);
$textcolor = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, $im_w, $im_h, $color_white);
imagesetthickness($im, $im_border);
imagerectangle($im, 0, 0, ($im_w - $im_border), ($im_h - $im_border), $color_black);

// ship to
imagettftext($im, 30, 0, 30, 70, $textcolor, $font, 'SHIP TO:');
imagettftext($im, 25, 0, 220, 70, $textcolor, $font, 'Chende Cristian');
imagettftext($im, 25, 0, 220, 110, $textcolor, $font, 'Cluj-Napoca, Romania');
imagettftext($im, 25, 0, 220, 150, $textcolor, $font, 'Cluj 900547');

// order number
imagettftext($im, 40, 0, 30, 270, $textcolor, $font, 'Order Number: #45345345');

foreach ($pidlist as $k=>$v) {
    list($pid, $whid, $serial,$qty_item) = explode(":", $pidlist[$k]);
    $plist['pid'][] = $pid;
    $plist['whid'][] = $whid;
    $plist['serial'][] = $serial;
    $plist['qty'][] = $qty_item;
}
$qty_tmp = 0;
$cat_tmp = false;
$pids_count = 0;
$pos = 650;
foreach ($plist['pid'] as $k=>$v) 
{
    $pid = $v;
    if (intval($pid) > 0)
    {
    	$query = "SELECT pid FROM product_category_map WHERE pid='".$pid."' AND catid='508'";
		if(!$q2ok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
    	if (mysql_num_rows($q2ok)>0) {
    		$cat_tmp = true;
    		$qty_tmp = $plist['qty'][$k];
    	}
		$queryx = "SELECT pid FROM auto_shipping_ignore WHERE pid='".$pid."'";
		if(!$q2okx = mysql_db_query($this->DB, $queryx, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if (mysql_num_rows($q2okx)==0) {
            $query = "select product_manufacturers.manname,product_list.manpart from product_list,product_manufacturers where product_list.id = '$pid' AND product_list.manid=product_manufacturers.manid";
            if(!$q2ok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
            list($manname,$manpart) = mysql_fetch_array($q2ok);

            $query = "select qty_ordered, descr from customer_order_lines where orderid = '$orderid' and prodid = '$pid'";
            if(!$q2ok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
            list($qty,$descr) = mysql_fetch_array($q2ok);

            $query = "select text_short from custom_values where cfid = '42' and pid = '$pid'";
            if(!$qok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
            list($text_short) = mysql_fetch_array($qok);
            if($text_short == '') {
//                            $text_short = $manpart."-".$descr;
            }
            $pos += ($pids_count*50);

            $text = $plist['qty'][$k]." ".$manname." ".$manpart." ".$text_short;
			imagettftext($im, 20, 0, 300, 650, $textcolor, $font, $text);
            $pids_count++;
		}
    }
}

// shipping
imagettftext($im, 40, 0, 30, 830, $textcolor, $font, 'Shipping: '.$shipmethod);
// imagegif($im,$ROOT_DIR."/mgt/ship/labels/".$uida);
imagegif($im,$ROOT_DIR."/mgt/ship/labels/fedex-label-test.gif");
imagedestroy($im);