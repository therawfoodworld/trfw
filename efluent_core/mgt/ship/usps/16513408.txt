labelRequestXML=<?xml version="1.0"?>
    	<LabelRequest  LabelType="Default" LabelSize="4X6" ImageFormat="ZPLII" ImageRotation="Rotate180">
			<RequesterID>lecm</RequesterID>
			<AccountID>844091</AccountID>
			<PassPhrase>patches1</PassPhrase>
			<MailClass>Priority</MailClass>
			<DateAdvance>0</DateAdvance>
			<WeightOz>19</WeightOz><MailpieceShape>FlatRateLegalEnvelope</MailpieceShape><Services SignatureConfirmation="OFF" DeliveryConfirmation="ON" CertifiedMail="OFF"/>
<Value>80.41</Value>
<Stealth>TRUE</Stealth><Description>Flat Rate Legal Envelope</Description>
		<PartnerCustomerID>The Raw Food World</PartnerCustomerID>
		<PartnerTransactionID>16513408</PartnerTransactionID>
		<ToName>Teresa Krueger</ToName>
		<ToCompany></ToCompany>
		<ToAddress1>11923 West Trail</ToAddress1>
		<ToAddress2></ToAddress2>
		<ToCity>Kagel Canyon</ToCity>
		<ToState>CA</ToState><ToPostalCode>91342</ToPostalCode>
		<ToDeliveryPoint>00</ToDeliveryPoint>
		<ToPhone>818 8979195</ToPhone>
		<FromName>The Raw Food World</FromName>
		<ReturnAddress1>406 Bryant Cir Ste H</ReturnAddress1>
		<ReturnAddress1></ReturnAddress1>
		<FromCity>Ojai</FromCity>
		<FromState>CA</FromState>
		<FromPostalCode>93023</FromPostalCode>
		<FromZIP4>0000</FromZIP4>
		<FromPhone>8667293438</FromPhone>
		<ResponseOptions PostagePrice="TRUE"/></LabelRequest>