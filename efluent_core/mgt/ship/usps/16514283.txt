labelRequestXML=<?xml version="1.0"?>
    	<LabelRequest  LabelType="Default" LabelSize="4X6" ImageFormat="ZPLII" ImageRotation="Rotate180">
			<RequesterID>lecm</RequesterID>
			<AccountID>844091</AccountID>
			<PassPhrase>patches1</PassPhrase>
			<MailClass>First</MailClass>
			<DateAdvance>0</DateAdvance>
			<WeightOz>9</WeightOz>
			<MailpieceShape>Parcel</MailpieceShape><Services InsuredMail="OFF" SignatureConfirmation="OFF" DeliveryConfirmation="ON" CertifiedMail="OFF"/>
<Value>0</Value>
<Stealth>TRUE</Stealth><Description>First Class Parcel</Description>
		<PartnerCustomerID>The Raw Food World</PartnerCustomerID>
		<PartnerTransactionID>16514283</PartnerTransactionID>
		<ToName>linda  hunt</ToName>
		<ToCompany></ToCompany>
		<ToAddress1>12703 ne 37th st</ToAddress1>
		<ToAddress2></ToAddress2>
		<ToCity>Vancouver</ToCity>
		<ToState>WA</ToState><ToPostalCode>98682</ToPostalCode>
		<ToDeliveryPoint>00</ToDeliveryPoint>
		<ToPhone>3606096606</ToPhone>
		<FromName>The Raw Food World</FromName>
		<ReturnAddress1>406 Bryant Cir Ste H</ReturnAddress1>
		<ReturnAddress1></ReturnAddress1>
		<FromCity>Ojai</FromCity>
		<FromState>CA</FromState>
		<FromPostalCode>93023</FromPostalCode>
		<FromZIP4>0000</FromZIP4>
		<FromPhone>8667293438</FromPhone>
		<ResponseOptions PostagePrice="TRUE"/></LabelRequest>