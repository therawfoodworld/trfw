labelRequestXML=<?xml version="1.0"?>
    	<LabelRequest  LabelType="Default" LabelSize="4X6" ImageFormat="ZPLII" ImageRotation="Rotate180">
			<RequesterID>lecm</RequesterID>
			<AccountID>844091</AccountID>
			<PassPhrase>patches1</PassPhrase>
			<MailClass>Priority</MailClass>
			<DateAdvance>0</DateAdvance>
			<WeightOz>256</WeightOz><MailpieceShape>Parcel</MailpieceShape><Services InsuredMail="OFF" SignatureConfirmation="OFF" DeliveryConfirmation="ON" CertifiedMail="OFF"/>
<Value>0</Value>
<Stealth>TRUE</Stealth><Description>Priority</Description>
		<PartnerCustomerID>The Raw Food World</PartnerCustomerID>
		<PartnerTransactionID>16514977</PartnerTransactionID>
		<ToName>Erin Campbell</ToName>
		<ToCompany></ToCompany>
		<ToAddress1>4131 Grape Place</ToAddress1>
		<ToAddress2>Unit 4</ToAddress2>
		<ToCity>Anchorage</ToCity>
		<ToState>AK</ToState><ToPostalCode>99508</ToPostalCode>
		<ToDeliveryPoint>00</ToDeliveryPoint>
		<ToPhone>9074408328</ToPhone>
		<FromName>The Raw Food World</FromName>
		<ReturnAddress1>406 Bryant Cir Ste H</ReturnAddress1>
		<ReturnAddress1></ReturnAddress1>
		<FromCity>Ojai</FromCity>
		<FromState>CA</FromState>
		<FromPostalCode>93023</FromPostalCode>
		<FromZIP4>0000</FromZIP4>
		<FromPhone>8667293438</FromPhone>
		<ResponseOptions PostagePrice="TRUE"/></LabelRequest>