labelRequestXML=<?xml version="1.0"?>
    	<LabelRequest  LabelType="Default" LabelSize="4X6" ImageFormat="ZPLII" ImageRotation="Rotate180">
			<RequesterID>lecm</RequesterID>
			<AccountID>844091</AccountID>
			<PassPhrase>patches1</PassPhrase>
			<MailClass>Priority</MailClass>
			<DateAdvance>0</DateAdvance>
			<WeightOz>15</WeightOz><MailpieceShape>FlatRateEnvelope</MailpieceShape><Services InsuredMail="OFF" SignatureConfirmation="OFF" DeliveryConfirmation="ON" CertifiedMail="OFF"/>
<Value>0</Value>
<Stealth>TRUE</Stealth><Description>Flat Rate Envelope</Description>
		<PartnerCustomerID>The Raw Food World</PartnerCustomerID>
		<PartnerTransactionID>16514174</PartnerTransactionID>
		<ToName>Randi Collins</ToName>
		<ToCompany></ToCompany>
		<ToAddress1>7401 S. 111th E. Ave</ToAddress1>
		<ToAddress2></ToAddress2>
		<ToCity>Tulsa</ToCity>
		<ToState>OK</ToState><ToPostalCode>74133</ToPostalCode>
		<ToDeliveryPoint>00</ToDeliveryPoint>
		<ToPhone>9182540967</ToPhone>
		<FromName>The Raw Food World</FromName>
		<ReturnAddress1>406 Bryant Cir Ste H</ReturnAddress1>
		<ReturnAddress1></ReturnAddress1>
		<FromCity>Ojai</FromCity>
		<FromState>CA</FromState>
		<FromPostalCode>93023</FromPostalCode>
		<FromZIP4>0000</FromZIP4>
		<FromPhone>8667293438</FromPhone>
		<ResponseOptions PostagePrice="TRUE"/></LabelRequest>