<?php
/*ini_set('display_errors',1);
error_reporting(E_ALL);*/

REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
REQUIRE($ROOT_DIR."/mgt/auth.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************************************************/

// Copyright 2008, FedEx Corporation. All rights reserved.
// Version 4.0.0
if(floatval($weight) <= 0)
{
	echo "OK\nFEDEX_GROUND\t0.00\nFEDEX_EXPRESS_SAVER\t0.00\nFEDEX_2_DAY\t0.00\nSTANDARD_OVERNIGHT\t0.00\nPRIORITY_OVERNIGHT\t0.00\nFIRST_OVERNIGHT\t0.00\nSMART_POST\t0.00\n";
	exit;
}

require_once('fedex-common.php');

if(intval($residential) == 1)
	$doresidential = true;
else
	$doresidential = false;
	

$newline = "<br />";
//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = "RateService_v4.wsdl";

ini_set("soap.wsdl_cache_enabled", "0");
 
$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

$request['WebAuthenticationDetail'] = array('UserCredential' =>
                                      array('Key' => $efc->getConfigValue("FedEx Settings", "Developer Key"), 'Password' => $efc->getConfigValue("FedEx Settings", "Password"))); // Replace 'XXX' and 'YYY' with FedEx provided credentials 
$request['ClientDetail'] = array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 'MeterNumber' => $efc->getConfigValue("FedEx Settings", "MeterNumber"));// Replace 'XXX' with your account and meter number
$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Available Services Request v4 using PHP ***');
$request['Version'] = array('ServiceId' => 'crs', 'Major' => '4', 'Intermediate' => '0', Minor => '0');
$request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
$request['RequestedShipment']['ShipTimestamp'] = date('c');

// Service Type and Packaging Type are not passed in the request
$request['RequestedShipment']['Shipper'] = array('Address' => array(
                                          'StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), $efc->getConfigValue("Company Information", "CompanyAddress2")), // Origin details
                                          'City' => $efc->getConfigValue("Company Information", "CompanyCity"),
                                          'StateOrProvinceCode' => $efc->getConfigValue("Company Information", "CompanyState"),
                                          'PostalCode' => $efc->getConfigValue("Company Information", "CompanyZip"),
                                          'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry")));

$request['RequestedShipment']['Recipient'] = array('Address' => array (
                                               'StreetLines' => array($ship2street1, $ship2street2), // Destination details
                                               'City' => $ship2city,
                                               'StateOrProvinceCode' => $ship2state,
                                               'PostalCode' => $ship2zip,
                                               'CountryCode' => $ship2country,
											   'Residential' => $doresidential));
$request['RequestedShipment']['ShippingChargesPayment'] = array('PaymentType' => 'SENDER',
                                                        'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), // Replace "XXX" with payor's account number
                                                                     'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry")));

                                                                     $request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
$request['RequestedShipment']['PackageCount'] = '1';
$request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';

$request['RequestedShipment']['RequestedPackages'] = array('0' => array('SequenceNumber' => '1',
                                                                  'InsuredValue' => array('Amount' => $insured,
                                                                                          'Currency' => 'USD'),
                                                                  'ItemDescription' => $invoiceid,
                                                                  'Weight' => array('Value' => $weight,
                                                                                    'Units' => 'LB'),
                                                                  'Dimensions' => array('Length' => 0,
                                                                                        'Width' => 0,
                                                                                        'Height' => 0,
                                                                                        'Units' => 'IN'),
                                                                  'CustomerReferences' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                                                                                                 'Value' => $invoiceid)));
//print_r($request);
try
{
    $response = $client->getRates($request);
//    print_r($response);    
    if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR')
    {
    	$rateArray = array();
        //echo 'Rates for following service type(s) were returned.'. $newline. $newline; 
        foreach ($response -> RateReplyDetails as $rateReply)
        {           
           $rateArray['type'][] = $rateReply->ServiceType;
          
           for($xx = 0; $xx < count($rateReply->RatedShipmentDetails[0]); $xx++)
           {
				if($rateReply->RatedShipmentDetails[$xx]->ShipmentRateDetail->RateType == $efc->getConfigValue("FedEx Settings", "ReturnedRateType"))
				{
					
					$rateArray['price'][] = $rateReply->RatedShipmentDetails[$xx]->ShipmentRateDetail->TotalNetCharge->Amount;	
				}
           }
           
           //echo $rateReply -> Message . $newline;
        }
        
        array_multisort($rateArray['price'], SORT_ASC, SORT_NUMERIC, $rateArray['type']);
        
        echo "OK\n";
        
        for($xx = 0; $xx < count($rateArray['type']); $xx++)
        {
        	echo $rateArray['type'][$xx]."\t".number_format($rateArray['price'][$xx], 2)."\n";
        }
        
        echo "SMART_POST\t0.00\n";
		/* 
		echo "<pre>";
		print_r($response->RateReplyDetails[0]->RatedShipmentDetails);
		echo "</pre>";
		*/
        //printRequestResponse($client);
    }
    else
    {
        echo 'Error\n\n'; 
        foreach ($response -> Notifications as $notification)
        {           
            if(is_array($response -> Notifications))
            {              
               $errormsg .= $notification -> Severity;
               $errormsg .= ': ';           
               $errormsg .= $notification -> Message. " | ";
            }
            else
            {
                $errormsg .= $notification;
            }
        } 
        
        echo $errormsg;
    } 

}catch (SoapFault $exception) 
{
   printFault($exception, $client);        
}

?>