<?php
REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

if($keycode == "9782ga87t174gadf7g134b-asdf2834y")
{
	// bulkshipping
}
else 
{
	REQUIRE($ROOT_DIR."/mgt/auth.phtml");
}

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

if(intval($residential) == 1)
	$doresidential = true;
else
	$doresidential = false;
	
if(($_SESSION["LABEL_FORMAT"] == "") && ($lf == ""))
	$label_format = "EPL2";
else if($lf != "")
	$label_format = $lf;
else
	$label_format = strtoupper($_SESSION["LABEL_FORMAT"]);
	
/*ini_set('display_errors',1); 
error_reporting(E_ALL);	
*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$on = $efc->getOrderNumber($invoiceid);
$pidlist = explode(",", $productlist);
		
for($xx = 0; $xx < count($pidlist); $xx++)
{
	list($pid, $whid, $serial) = explode(":", $pidlist[$xx]);
	
	if(intval($pid) > 0)
	{
		$query = "select lineid from customer_order_lines where qty_ordered > qty_shipped AND orderid = '$on' and prodid = '$pid'";
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($q2ok) <= 0)
		{
			echo "This would create a duplicate shipment!";
			exit;
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************************************************/
// Copyright 2008, FedEx Corporation. All rights reserved.
// Version 4.0.0
$invoiceid = trim($invoiceid);

require_once("fedex-common.php");

if($ship2phone == "")
	$ship2phone = "888-555-1212";

$newline = "<br />";
//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = "ShipService_v7.wsdl";
//define('SHIP_LABEL', 'shipgroundlabel.png');  // PDF label file. Change to file-extension .png for creating a PNG label (e.g. shiplabel.png)
//define('SHIP_CODLABEL', 'CODgroundreturnlabel.png');  // PDF label file. Change to file-extension ..png for creating a PNG label (e.g. CODgroundreturnlabel.png)

ini_set("soap.wsdl_cache_enabled", "0");


$sigtype = $efc->getConfigValue("FedEx Settings", "SIGNATURE_TYPE");

if($sigtype == "")
	$sigtype = "ADULT";

if(stristr($shipmethod, "home"))
{
	$sigtype = $efc->getConfigValue("FedEx Settings", "HOME_SIGNATURE_TYPE");
}


$shiptimestamp = date("c", strtotime($shipdate));

$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

$request['WebAuthenticationDetail'] = array('UserCredential' =>
                                      array('Key' => $efc->getConfigValue("FedEx Settings", "Developer Key"), 'Password' => $efc->getConfigValue("FedEx Settings", "Password"))); // Replace 'XXX' and 'YYY' with FedEx provided credentials 
$request['ClientDetail'] = array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 'MeterNumber' => $efc->getConfigValue("FedEx Settings", "MeterNumber"));// Replace 'XXX' with your account and meter number
$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request v7 using PHP ***');
$request['Version'] = array('ServiceId' => 'ship', 'Major' => 7, 'Intermediate' => 0, 'Minor' => 0);


if(strtolower($shipmethod) != "smart_post")
{
	if(intval($saturday == 1))
	{
		if(intval($signature) == 1) // saturday with a sig
		{
			$request['RequestedShipment'] = array('ShipTimestamp' => $shiptimestamp,
	                                        'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	                                        'ServiceType' => $shipmethod, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	                                        'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	                                        'Shipper' => array('Contact' => array('CompanyName' => $efc->getConfigValue("Company Information", "CompanyName"),
	                                                                              'PhoneNumber' => $efc->getConfigValue("Company Information", "CompanyPhone")),
	                                                           'Address' => array('StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), 
	                                                           						$efc->getConfigValue("Company Information", "CompanyAddress2")),
	                                                                              'City' =>  $efc->getConfigValue("Company Information", "CompanyCity"),
	                                                                              'StateOrProvinceCode' =>  $efc->getConfigValue("Company Information", "CompanyState"),
	                                                                              'PostalCode' =>  $efc->getConfigValue("Company Information", "CompanyZip"),
	                                                                              'CountryCode' =>  $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'Recipient' => array('Contact' => array('CompanyName' => $ship2name,
	                                                                                'PhoneNumber' => $ship2phone),
	                                                             'Address' => array('StreetLines' => array($ship2street1, $ship2street2),
	                                                                                'City' => $ship2city,
	                                                                                'StateOrProvinceCode' => $ship2state,
	                                                                                'PostalCode' => $ship2zip,
	                                                                                'CountryCode' => $ship2country),
																					'Residential' => $doresidential),
	                                        'ShippingChargesPayment' => array('PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
	                                                                          'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
	                                                                                           'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry"))),
											
											'SpecialServicesRequested' => array('SpecialServiceTypes' => array('SATURDAY_DELIVERY')),	                                                                                           
                        
	                                        'LabelSpecification' => array('LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
	                                                                      'ImageType' => $label_format,
	                                                                      'LabelStockType' => 'STOCK_4X6', // STOCK_4X6.75_LEADING_DOC_TAB
	                                                                      'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'),
	                                         'RateRequestTypes' => array('ACCOUNT'), 
	                                        'PackageCount' => 1,
	                                        'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
	                                        'RequestedPackageLineItems' => array('0' => array('SequenceNumber' => '1',   
	                                        							 'InsuredValue' => array('Amount' => floatval($insured), 'Currency' => 'USD'),
	                                                                     'Weight' => array('Value' => $weight, 'Units' => 'LB'), // valid values LB or KG
	                                                                     'CustomerReferences' => array('0' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE', 'Value' => $invoiceid), 
	                                                                                                   '1' => array('CustomerReferenceType' => 'INVOICE_NUMBER', 'Value' => $invoiceid),
	                                                                                                   '2' => array('CustomerReferenceType' => 'P_O_NUMBER', 'Value' => $invoiceid)),
											'SpecialServicesRequested' => array( 'SpecialServiceTypes' => array('SIGNATURE_OPTION'), 'SignatureOptionDetail' => array('OptionType' => $sigtype) )		                                                                                                   
	                                                                     )));				
		}
		else  // saturday without a sig
		{ // 'SpecialServicesRequested' => ),
			$request['RequestedShipment'] = array('ShipTimestamp' => $shiptimestamp,
	                                        'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	                                        'ServiceType' => $shipmethod, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	                                        'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	                                        'Shipper' => array('Contact' => array('CompanyName' => $efc->getConfigValue("Company Information", "CompanyName"),
	                                                                              'PhoneNumber' => $efc->getConfigValue("Company Information", "CompanyPhone")),
	                                                           'Address' => array('StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), 
	                                                           						$efc->getConfigValue("Company Information", "CompanyAddress2")),
	                                                                              'City' =>  $efc->getConfigValue("Company Information", "CompanyCity"),
	                                                                              'StateOrProvinceCode' =>  $efc->getConfigValue("Company Information", "CompanyState"),
	                                                                              'PostalCode' =>  $efc->getConfigValue("Company Information", "CompanyZip"),
	                                                                              'CountryCode' =>  $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'Recipient' => array('Contact' => array('CompanyName' => $ship2name,
	                                                                                'PhoneNumber' => $ship2phone),
	                                                             'Address' => array('StreetLines' => array($ship2street1, $ship2street2),
	                                                                                'City' => $ship2city,
	                                                                                'StateOrProvinceCode' => $ship2state,
	                                                                                'PostalCode' => $ship2zip,
	                                                                                'CountryCode' => $ship2country,
																					'Residential' => $doresidential)),
	                                        'ShippingChargesPayment' => array('PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
	                                                                          'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
	                                                                                           'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry"))),
											
											'SpecialServicesRequested' => array('SpecialServiceTypes' => array('SATURDAY_DELIVERY')),	                                                                                           
                        
	                                        'LabelSpecification' => array('LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
	                                                                      'ImageType' => $label_format,
	                                                                      'LabelStockType' => 'STOCK_4X6', // STOCK_4X6.75_LEADING_DOC_TAB
	                                                                      'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'),
	                                         'RateRequestTypes' => array('ACCOUNT'), 
	                                        'PackageCount' => 1,
	                                        'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
	                                        'RequestedPackageLineItems' => array('0' => array('SequenceNumber' => '1',  
	                                        							 'InsuredValue' => array('Amount' => floatval($insured), 'Currency' => 'USD'), 
	                                                                     'Weight' => array('Value' => $weight, 'Units' => 'LB'), // valid values LB or KG
	                                                                     'CustomerReferences' => array('0' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE', 'Value' => $invoiceid), 
	                                                                                                   '1' => array('CustomerReferenceType' => 'INVOICE_NUMBER', 'Value' => $invoiceid),
	                                                                                                   '2' => array('CustomerReferenceType' => 'P_O_NUMBER', 'Value' => $invoiceid)),
	                                                                     )));	
		}
	                                                         
	                                                                     
		
	}
	else 
	{
		if(intval($signature) == 1)// regular with a sig
		{
			$request['RequestedShipment'] = array('ShipTimestamp' => $shiptimestamp,
	                                        'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	                                        'ServiceType' => $shipmethod, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	                                        'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	                                        'Shipper' => array('Contact' => array('CompanyName' => $efc->getConfigValue("Company Information", "CompanyName"),
	                                                                              'PhoneNumber' => $efc->getConfigValue("Company Information", "CompanyPhone")),
	                                                           'Address' => array('StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), 
	                                                           						$efc->getConfigValue("Company Information", "CompanyAddress2")),
	                                                                              'City' =>  $efc->getConfigValue("Company Information", "CompanyCity"),
	                                                                              'StateOrProvinceCode' =>  $efc->getConfigValue("Company Information", "CompanyState"),
	                                                                              'PostalCode' =>  $efc->getConfigValue("Company Information", "CompanyZip"),
	                                                                              'CountryCode' =>  $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'Recipient' => array('Contact' => array('CompanyName' => $ship2name,
	                                                                                'PhoneNumber' => $ship2phone),
	                                                             'Address' => array('StreetLines' => array($ship2street1, $ship2street2),
	                                                                                'City' => $ship2city,
	                                                                                'StateOrProvinceCode' => $ship2state,
	                                                                                'PostalCode' => $ship2zip,
	                                                                                'CountryCode' => $ship2country,
																					'Residential' => $doresidential)),
	                                        'ShippingChargesPayment' => array('PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
	                                                                          'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
	                                                                                           'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry"))),
											
	                                        'LabelSpecification' => array('LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
	                                                                      'ImageType' => $label_format,
	                                                                      'LabelStockType' => 'STOCK_4X6', // STOCK_4X6.75_LEADING_DOC_TAB
	                                                                      'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'),
	                                         'RateRequestTypes' => array('ACCOUNT'), 
	                                        'PackageCount' => 1,
	                                        'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
	                                        'RequestedPackageLineItems' => array('0' => array('SequenceNumber' => '1',  
	                                        							 'InsuredValue' => array('Amount' => floatval($insured), 'Currency' => 'USD'), 
	                                                                     'Weight' => array('Value' => $weight, 'Units' => 'LB'), // valid values LB or KG
	                                                                     'CustomerReferences' => array('0' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE', 'Value' => $invoiceid), 
	                                                                                                   '1' => array('CustomerReferenceType' => 'INVOICE_NUMBER', 'Value' => $invoiceid),
	                                                                                                   '2' => array('CustomerReferenceType' => 'P_O_NUMBER', 'Value' => $invoiceid)),
																		 'SpecialServicesRequested' => array( 'SpecialServiceTypes' => array('SIGNATURE_OPTION'), 'SignatureOptionDetail' => array('OptionType' => $sigtype) )	                                                                                                   
	                                                                     )));			
		}
		else // regular without a sig (works great)
		{
			$request['RequestedShipment'] = array('ShipTimestamp' => $shiptimestamp,
	                                        'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	                                        'ServiceType' => $shipmethod, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	                                        'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	                                        'Shipper' => array('Contact' => array('CompanyName' => $efc->getConfigValue("Company Information", "CompanyName"),
	                                                                              'PhoneNumber' => $efc->getConfigValue("Company Information", "CompanyPhone")),
	                                                           'Address' => array('StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), 
	                                                           						$efc->getConfigValue("Company Information", "CompanyAddress2")),
	                                                                              'City' =>  $efc->getConfigValue("Company Information", "CompanyCity"),
	                                                                              'StateOrProvinceCode' =>  $efc->getConfigValue("Company Information", "CompanyState"),
	                                                                              'PostalCode' =>  $efc->getConfigValue("Company Information", "CompanyZip"),
	                                                                              'CountryCode' =>  $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'Recipient' => array('Contact' => array('CompanyName' => $ship2name,
	                                                                                'PhoneNumber' => $ship2phone),
	                                                             'Address' => array('StreetLines' => array($ship2street1, $ship2street2),
	                                                                                'City' => $ship2city,
	                                                                                'StateOrProvinceCode' => $ship2state,
	                                                                                'PostalCode' => $ship2zip,
	                                                                                'CountryCode' => $ship2country,
																					'Residential' => $doresidential)),
	                                        'ShippingChargesPayment' => array('PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
	                                                                          'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
	                                                                                           'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'LabelSpecification' => array('LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
	                                                                      'ImageType' => $label_format,
	                                                                      'LabelStockType' => 'STOCK_4X6', // STOCK_4X6.75_LEADING_DOC_TAB
	                                                                      'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'),
	                                         'RateRequestTypes' => array('ACCOUNT'), 
	                                        'PackageCount' => 1,
	                                        'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
	                                        'RequestedPackageLineItems' => array('0' => array('SequenceNumber' => '1',  
	                                        							 'InsuredValue' => array('Amount' => floatval($insured), 'Currency' => 'USD'), 
	                                                                     'Weight' => array('Value' => $weight, 'Units' => 'LB'), // valid values LB or KG
	                                                                     'CustomerReferences' => array('0' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE', 'Value' => $invoiceid), 
	                                                                                                   '1' => array('CustomerReferenceType' => 'INVOICE_NUMBER', 'Value' => $invoiceid),
	                                                                                                   '2' => array('CustomerReferenceType' => 'P_O_NUMBER', 'Value' => $invoiceid)),
	                                                                     )));			
		}
	                                                                     
	
	                                                                     
	}
}
else  // SHIPPING SMARTPOST
{//5015
			if($DB == "sndkids")
				$hubid = "5185";
			else if($DB == "butterfly")
				$hubid = "5185";
			else
				$hubid = "5087";
				
			$request['RequestedShipment'] = array('ShipTimestamp' => $shiptimestamp,
	                                        'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	                                        'ServiceType' => $shipmethod, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	                                        'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	                                        'Shipper' => array('Contact' => array('CompanyName' => $efc->getConfigValue("Company Information", "CompanyName"),
	                                                                              'PhoneNumber' => $efc->getConfigValue("Company Information", "CompanyPhone")),
	                                                           'Address' => array('StreetLines' => array($efc->getConfigValue("Company Information", "CompanyAddress1"), 
	                                                           						$efc->getConfigValue("Company Information", "CompanyAddress2")),
	                                                                              'City' =>  $efc->getConfigValue("Company Information", "CompanyCity"),
	                                                                              'StateOrProvinceCode' =>  $efc->getConfigValue("Company Information", "CompanyState"),
	                                                                              'PostalCode' =>  $efc->getConfigValue("Company Information", "CompanyZip"),
	                                                                              'CountryCode' =>  $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                        'Recipient' => array('Contact' => array('PersonName' => $ship2name,
	                                                                                'PhoneNumber' => $ship2phone),
	                                                             'Address' => array('StreetLines' => array($ship2street1, $ship2street2),
	                                                                                'City' => $ship2city,
	                                                                                'StateOrProvinceCode' => $ship2state,
	                                                                                'PostalCode' => $ship2zip,
	                                                                                'CountryCode' => $ship2country,
																					'Residential' => $doresidential)),
	                                        'ShippingChargesPayment' => array('PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
	                                                                          'Payor' => array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
	                                                                                           'CountryCode' => $efc->getConfigValue("Company Information", "CompanyCountry"))),
	                                                                                           
	                                        'SmartPostDetail' => array('Indicia' => 'PARCEL_SELECT', 
	                                        							'AncillaryEndorsement' => 'ADDRESS_CORRECTION',
	                                        							'SpecialServices' => 'USPS_DELIVERY_CONFIRMATION',
	                                        							'HubId' => $hubid), //LIVE => 5087,  DEV => 5531
	                                        
	                                        'ProcessShipmentRequest' => array('RequestedShipment', array('ServiceType' => 'SMART_POST')),
	                                        
	                                        'LabelSpecification' => array('LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
	                                                                      'ImageType' => $label_format,
	                                                                      'LabelStockType' => 'STOCK_4X6', // STOCK_4X6.75_LEADING_DOC_TAB
	                                                                      'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'),
	                                         'RateRequestTypes' => array('ACCOUNT'), 
	                                        'PackageCount' => 1,
	                                        'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
	                                        'RequestedPackageLineItems' => array('0' => array('SequenceNumber' => '1',   
	                                                                     'Weight' => array('Value' => $weight, 'Units' => 'LB'), // valid values LB or KG
	                                                                     'CustomerReferences' => array('0' => array('CustomerReferenceType' => 'CUSTOMER_REFERENCE', 'Value' => $invoiceid), 
	                                                                                                   '1' => array('CustomerReferenceType' => 'INVOICE_NUMBER', 'Value' => $invoiceid),
	                                                                                                   '2' => array('CustomerReferenceType' => 'P_O_NUMBER', 'Value' => $invoiceid)),
	                                                                     )));
}


/*ob_start();
print_r($request);
$xmlrequest = ob_get_clean();

$fp = fopen("/tmp/fedex.log", "a");
fputs($fp, $xmlrequest);
fclose($fp);*/

try 
{
    $response = $client->processShipment($request); // FedEx web service invocation  

	ob_start();
	
	echo "SEVERIFTY: ".$response->HighestSeverity;
	$xmlrequest = ob_get_clean();
	
		
/*	$fp = fopen("/tmp/fedex.log", "a");
	fputs($fp, "REQUEST -------------------------------------------------------\n");
	fputs($fp, $xmlrequest);
	fclose($fp);	*/

	if ( ($response->HighestSeverity != 'FAILURE') && ($response->HighestSeverity != 'ERROR') )
    {
    	
			    
		ob_start();
		print_r($response);
		$xmlrequest = ob_get_clean();
	
		
/*		$fp = fopen("/tmp/fedex.log", "a");
		fputs($fp, "SUCCESS -------------------------------------------------------\n");
		fputs($fp, $xmlrequest);
		fclose($fp);*/
    	
    	
        //printRequestResponse($client);

        /*$fp = fopen(SHIP_CODLABEL, 'wb');   
        fwrite($fp, $response->CompletedShipmentDetail->CompletedPackageDetails->CodReturnDetail->Label->Parts->Image); //Create COD Return PNG or PDF file
        fclose($fp);
        echo SHIP_CODLABEL.' was generated.'.$newline;*/
        
        // Create PNG or PDF label
        // Set LabelSpecification.ImageType to 'PNG' for generating a PNG label
        
        $uid = $invoiceid . date("m-d-Y.H.i.s", time()).".".strtolower($label_format);
        
        $fp = fopen($ROOT_DIR."/mgt/ship/labels/$uid", 'wb');   
        fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
        fclose($fp);

        
        //echo $response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
       
	   	$shipcost = $response->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails->TotalNetCharge->Amount;
		
		if(floatval($shipcost) <= 0.00)
        	$shipcost = $response->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails[0]->TotalNetCharge->Amount;
        
        if(strtolower($shipmethod) != "smart_post")
        {
        	$tracking = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
        }
        else 
        {// TrackingIdType
		
			if(is_array($response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds))
			{
	        	$tracking = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds[0]->TrackingIdType . "~" . 
	        				$response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds[0]->TrackingNumber . ":" . 
	        				$response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds[1]->TrackingIdType . "~" .
	        				$response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds[1]->TrackingNumber;
			}
			else
			{
				$tracking = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
			}
     	
        }
        
                
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		$tstamp = date("Y-m-d H:i:s", strtotime($shipdate));
		$xshipdate = strtotime($shipdate);
		
		$query = "insert into customer_order_tracking(invoiceid, trackno, type, carrier, shipdate, cost, pickerid, shipperid) ";
		$query .= "values('$invoiceid', '$tracking', '$shipmethod', 'fedex', '$xshipdate', '$shipcost', '$pickerid', '".$_SESSION["ID"]."') ";
		
		ob_start();
		echo "QUERY: $query\n";
		$xmlrequest = ob_get_clean();
	
		
/*		$fp = fopen("/tmp/fedex.log", "a");
		fputs($fp, $xmlrequest);
		fclose($fp);
*/		
				
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$tid = mysql_insert_id();
		
		//add id of user who shipped the order
		/*if ($DB == 'butterfly') {
			$query = "UPDATE customer_order_tracking SET shipped_user_id='".$_SESSION["ID"]."' WHERE tid='".$tid."'";
            if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}*/
		
		$on = $efc->getOrderNumber($invoiceid);
		$pidlist = explode(",", $productlist);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// update customer_order_tracking_extended
		for($xx = 0; $xx < count($pidlist); $xx++)
		{//update the qty shipped for each item
			list($pid, $whid, $serial) = explode(":", $pidlist[$xx]);
			
			if(intval($pid) > 0)
			{
				$serial = addslashes($serial);
				// save what was in the box
				$query = "insert into customer_order_tracking_extended(orderid, tid, trackno, prodid, serial) values('$on', '$tid', '$tracking', '$pid', '$serial')";
				if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ship all coupons
		$query = "update customer_order_lines set qty_shipped = qty_ordered where orderid = '$on' and prodid < 0";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		
		// ship all kits
		$query = "select customer_order_lines.prodid from customer_order_lines, product_list where customer_order_lines.prodid = product_list.id and product_list.kit = '1' and customer_order_lines.orderid = '$on'";
		if(!$q1723b = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		while(list($xkitpid) = mysql_fetch_array($q1723b))
		{
			$query = "update customer_order_lines set qty_shipped = qty_ordered where orderid = '$on' and prodid = '$xkitpid'";
		    if(!$q1732x = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}	
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

		for($xx = 0; $xx < count($pidlist); $xx++)
		{//update the qty shipped for each item
			list($pid, $whid) = explode(":", $pidlist[$xx]);
			
			if(intval($pid) > 0)
			{
				$now = time();
				$cost = $efc->getActualCost($pid);
				$scost = $efc->getSalesmanCost($pid);
				
				if($efc->getOrderType($on) == "EXCHANGE")
				{
					$query = "update customer_order_lines set qty_shipped = qty_shipped + 1, shipdate = '$now', cost = '$cost', scost = '$scost' where orderid = '$on' and prodid = '$pid' and ex_return = 0";
					if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }					
				}
				else 
				{
					$query = "update customer_order_lines set qty_shipped = qty_shipped + 1, shipdate = '$now', cost = '$cost', scost = '$scost' where orderid = '$on' and prodid = '$pid'";
					if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
				
				$efc->updateInventory(0, 1, $shipcost, $pid, $whid, "", $on);
			}
		}
         
		$efc->getStartStatus($on);
		if($efc->getOrderType($on) == "EXCHANGE")
		{
			$query = "select count(*) from customer_order_lines where orderid = '$on' and ex_return = 0 and qty_ordered > qty_shipped";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($numleft) = mysql_fetch_array($qok);
				
			if($numleft <= 0)
			{
				$query = "update customer_orders set status = '50' where orderid = '$on'";	
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
		}
		else 
		{		
			// see if everything on the order has shipped
			$query = "select lineid, prodid from customer_order_lines where orderid = '$on' AND qty_ordered > qty_shipped";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) <= 0)
			{
				// success
				$query = "select customer_orders.status, company_orderflow.success from customer_orders, company_orderflow where
							customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				list($this_step, $success_step) = mysql_fetch_array($qok);
				
				$query = "update customer_orders set status = '$success_step' where orderid = '$on'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
			else 
			{
				// failure .... partially shipped
				$query = "select customer_orders.status, company_orderflow.failure from customer_orders, company_orderflow where
							customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				list($this_step, $failure_step) = mysql_fetch_array($qok);
				
				$query = "update customer_orders set status = '$failure_step' where orderid = '$on'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
		}
		$efc->getLogStopStatus($on);
		$efc->recalculateInventory($on);
		
		if($kitpid = $efc->orderWasKit($on))
			$efc->internal_repriceKits($kitpid, 0, 0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$efc->UpdatePool(2, $on);
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        echo "OK\t$tracking\t$shipcost\t$uid\t\n";
        
        //echo SHIP_LABEL.' was generated.'; 
    }
    else
    {
		ob_start();
		print_r($response);
		$xmlrequest = ob_get_clean();
	
		
/*		$fp = fopen("/tmp/fedex.log", "a");
		fputs($fp, "FAILURE -------------------------------------------------------\n");
		fputs($fp, $xmlrequest);
		fclose($fp);
*/

        foreach ($response->Notifications as $notification)
        {
            if(is_array($response->Notifications))
            {              
               //echo $notification -> Severity;
               //echo ': ';           
               echo $notification->Message.", ";
            }
            else
            {
                echo $notification.", ";
            }
        }
    }
    


//writeToLog($client);    // Write to log file

} catch (SoapFault $exception) 
{
	printFault($exception, $client);
}
?>