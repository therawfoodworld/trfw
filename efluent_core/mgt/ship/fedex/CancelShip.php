<?php

// Copyright 2008, FedEx Corporation. All rights reserved.
// Version 6.0.0

require_once('fedex-common.php');

$newline = "\n";
//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = $ROOT_DIR."/mgt/ship/fedex/ShipService_v6.wsdl";

ini_set("soap.wsdl_cache_enabled", "0");

$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

$request['WebAuthenticationDetail'] = array('UserCredential' => array('Key' => $efc->getConfigValue("FedEx Settings", "Developer Key"), 
					'Password' => $efc->getConfigValue("FedEx Settings", "Password")));
$request['ClientDetail'] = array('AccountNumber' => $efc->getConfigValue("FedEx Settings", "AccountNumber"), 
									'MeterNumber' => $efc->getConfigValue("FedEx Settings", "MeterNumber")); 
$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Cancel Shipment Request v6 using PHP ***');
$request['Version'] = array('ServiceId' => 'ship', 'Major' => '6', 'Intermediate' => '0', 'Minor' => '0');
$request['CarrierCode'] = 'FDXG'; // valid values FDXE-Express, FDXG-Ground, etc
$request['TrackingNumber'] = $trackno; // Replace 'XXX' with tracking number to cancel/delete
$request['DeletionControl'] = 'DELETE_ONE_PACKAGE'; // Package/Shipment

try 
{
    $response = $client ->deleteShipment($request);
    
    if($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR')
    {
        printRequestResponse($client);
        $success = 1;
    }
    else
    {
    	$success = 0;
        echo 'Error in processing transaction.'. $newline. $newline; 
        foreach ($response -> Notifications as $notification)
        {
            if(is_array($response -> Notifications))
            {              
               echo $notification -> Severity;
               echo ': ';           
               echo $notification -> Message . $newline;
            }
            else
            {
                echo $notification . $newline;
            }
        }
    } 
    
    writeToLog($client);    // Write to log file   

} catch (SoapFault $exception) {
    printFault($exception, $client);
}

?>