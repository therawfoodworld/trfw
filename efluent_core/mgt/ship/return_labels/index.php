<?php
 require_once("EwsLabelService.php");
// *************** This code will request a shipping label in PDF format *********************

// This will ensure that their isn't a 30 second timeout on this script so you can run the script as many cycles as you wish.
set_time_limit(0);
// User Inputs
$customerZip = "";
$packageWeight = "";
$referenceID = "";
$customerName = "";
$password = "";

$customerZip = $_POST["customerZip"];
$packageWeight = $_POST["packageWeight"];
$referenceID = $_POST["referenceID"];
$customerName = $_POST["customerName"];
$password = $_POST["password"];

// Client account values
$account="843778";
$passphrase="passphrasegoeshere"; //enter your account passphrase here
if ($customerZip != "" && $packageWeight != ""){

	if ( $password != "ecom" ){
	$htmlPassword = "<font color=\"red\">Incorrect Password</font>";
	} else {
		//Create Request XML
		$labelServer = new EwsLabelService();
		$labelRequest = "<?xml version=\"1.0\"?>";
		$labelRequest .= "\n<LabelRequest Test=\"NO\" LabelSize=\"4x6\" ImageFormat=\"PDF\" ImageRotation=\"Rotate90\">";
		$labelRequest .= "\n\t<RequesterID>OD</RequesterID>";
		$labelRequest .= "\n\t<AccountID>" . $account . "</AccountID>";
		$labelRequest .= "\n\t<PassPhrase>" . $passphrase . "</PassPhrase>";
		$labelRequest .= "\n\t<PartnerTransactionID>123</PartnerTransactionID>";
		$labelRequest .= "\n\t<MailClass>PRIORITY</MailClass>";
		$labelRequest .= "\n\t<WeightOz>" . $packageWeight ."</WeightOz>";
		$labelRequest .= "\n\t<ReferenceID>" . $referenceID ."</ReferenceID>";
		$labelRequest .= "\n\t<CostCenter>6901</CostCenter>";
		$labelRequest .= "\n\t<RubberStamp1>" . $customerName ."</RubberStamp1>";
		$labelRequest .= "\n\t<RubberStamp2>" . $referenceID  ."</RubberStamp2>";
		$labelRequest .= "\n\t<MailpieceShape>PARCEL</MailpieceShape>";
		$labelRequest .= "\n\t<ReplyPostage>TRUE</ReplyPostage>";	
		$labelRequest .= "\n\t<MailDimensions><Length>1</Length><Width>1</Width><Height>1</Height></MailDimensions>";
		$labelRequest .= "\n\t<Stealth>TRUE</Stealth>";
		$labelRequest .= "\n\t<FromCompany>FromCompany</FromCompany>";
		$labelRequest .= "\n\t<ReturnAddress1>100 METRO WAY</ReturnAddress1>";
		$labelRequest .= "\n\t<FromCity>SECAUCUS</FromCity>";
		$labelRequest .= "\n\t<FromState>NJ</FromState>";
		$labelRequest .= "\n\t<FromPostalCode>07094</FromPostalCode>";
		$labelRequest .= "\n\t<ToCompany>ToCompany</ToCompany>";
		$labelRequest .= "\n\t<ToAddress1>100 METRO WAY</ToAddress1>";
		$labelRequest .= "\n\t<ToCity>SECAUCUS</ToCity>";
		$labelRequest .= "\n\t<ToState>NJ</ToState>";
		$labelRequest .= "\n\t<ToPostalCode>07094</ToPostalCode>";
		$labelRequest .= "\n\t<POZipCode>" . $customerZip . "</POZipCode>";
		$labelRequest .= "\n</LabelRequest>";
		
		//echo $labelRequest;
		
		// Used to test the timing on the script.
		$timeStart = microtime_float();
		
		// Make Label Request to ELS
		$getPostageLabelXML = new GetPostageLabelXML();
		$getPostageLabelXML->LabelRequestXML = $labelRequest;
		//$labelResult = $labelServer->GetPostageLabelXML($getPostageLabelXML);
		$labelResult = $labelServer->GetPostageLabelXML($getPostageLabelXML);

	//print_r($labelResult);

		// Used to test the timing on the script.
		$timeEnd = microtime_float();
		$statusRequestDuration = $timeEnd - $timeStart;


		$errorMessage = $labelResult->LabelRequestResponse->ErrorMessage;
		if (!$errorMessage){
			$labelImage = $labelResult->LabelRequestResponse->Base64LabelImage;
			$trackingNumber = $labelResult->LabelRequestResponse->PIC;
			$statusCode = $labelResult->LabelRequestResponse->Status;
			$finalPostage = $labelResult->LabelRequestResponse->FinalPostage;	

		$fileContents = base64_decode($labelImage);
		//echo $fileContents;
		$file = trim($referenceID) . "_" . $trackingNumber . ".pdf";
		$fileName = "./pdf/" . $file;
		$fh = fopen($fileName, "w+");
		fwrite($fh, $fileContents);
		fclose($fh);
		$htmlMessage = "<h3><font color=\"red\">Link to the file: </font><a href=\"pdf/".$file."\">" .$file. "</a><h3>";
		
		//log the printing
		$trackingLogDetails = "\n" . $_SERVER['REMOTE_ADDR'] . ", ". date("r") . ", " .  $customerName . ", " . $referenceID . ", " . $packageWeight . ", \" " . $trackingNumber . "\", " . "http://www.postalsupport.com/ashleystewart/pdf/" . $file;
		$textFile = "./pdf/tracking.csv";
		$fht = fopen($textFile, "a");
		fwrite($fht, $trackingLogDetails);
		fclose($fht);

		
		} else {
		echo "Label request failed: ".$errorMessage;
		}
	} 
	} else {
	$htmlMessage = "Please enter the Customer's 5 digit Zip Code and select the weight of the package they will be returning.";

}



// Timer function to let you know how long the script ran for.
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
?>

<html>
<body>
<?php
echo $htmlMessage;
?>
<form action="index.php" method="post">


Enter Customer's Name: <input type="text" name="customerName" value="<?php echo $customerName;?>"><br>
Enter an Invoice Number <i>(ex.73600802)</i>: <input type="text" name="referenceID" value="<?php echo $referenceID;?>"><br>
Enter the Customer's Zip Code: <input type="text" name="customerZip" value="<?php echo $customerZip;?>"><br>
Select the weight of the package: <select name="packageWeight" style="font-size:18">
	<option value="16">1 lb</option>
	<option value="32">2 lbs</option>
	<option value="48">3 lbs</option>
	<option value="64">4 lbs</option>
	<option value="80">5 lbs</option>
	<option value="96">6 lbs</option>
	<option value="112">7 lbs</option>
	<option value="128">8 lbs</option>
	<option value="144">9 lbs</option>
	<option value="160">10 lbs</option>
	<option value="176">11 lbs</option>
	<option value="192">12 lbs</option>
	<option value="208">13 lbs</option>
	<option value="224">14 lbs</option>
	<option value="240">15 lbs</option>
	<option value="256">16 lbs</option>
	<option value="272">17 lbs</option>
	<option value="288">18 lbs</option>
	<option value="304">19 lbs</option>
	<option value="320">20 lbs</option>
	<option value="336">21 lbs</option>
	<option value="352">22 lbs</option>
	<option value="368">23 lbs</option>
	<option value="384">24 lbs</option>
	<option value="400">25 lbs</option>
	<option value="416">26 lbs</option>
	<option value="432">27 lbs</option>
	<option value="448">28 lbs</option>
	<option value="464">29 lbs</option>
	<option value="480">30 lbs</option>
	<option value="496">31 lbs</option>
	<option value="512">32 lbs</option>
	<option value="528">33 lbs</option>
	<option value="544">34 lbs</option>
	<option value="560">35 lbs</option>
	<option value="576">36 lbs</option>
	<option value="592">37 lbs</option>
	<option value="608">38 lbs</option>
	<option value="624">39 lbs</option>
	<option value="640">40 lbs</option>
	<option value="656">41 lbs</option>
	<option value="672">42 lbs</option>
	<option value="688">43 lbs</option>
	<option value="704">44 lbs</option>
	<option value="720">45 lbs</option>
	<option value="736">46 lbs</option>
	<option value="752">47 lbs</option>
	<option value="768">48 lbs</option>
	<option value="784">49 lbs</option>
	<option value="800">50 lbs</option>
	<option value="816">51 lbs</option>
	<option value="832">52 lbs</option>
	<option value="848">53 lbs</option>
	<option value="864">54 lbs</option>
	<option value="880">55 lbs</option>
	<option value="896">56 lbs</option>
	<option value="912">57 lbs</option>
	<option value="928">58 lbs</option>
	<option value="944">59 lbs</option>
	<option value="960">60 lbs</option>
	<option value="976">61 lbs</option>
	<option value="992">62 lbs</option>
	<option value="1008">63 lbs</option>
	<option value="1024">64 lbs</option>
	<option value="1040">65 lbs</option>
	<option value="1056">66 lbs</option>
	<option value="1072">67 lbs</option>
	<option value="1088">68 lbs</option>
	<option value="1104">69 lbs</option>
	<option value="1120">70 lbs</option>
	</select><br><br>
ENTER PASSWORD: <input type="password" name="password"><?php
echo $htmlPassword;
?><br>	
<input type="submit" value="PRINT">
</form>
</body>
</html>
