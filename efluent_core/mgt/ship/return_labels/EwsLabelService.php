<?php
class GetVersion {
}

class GetVersionResponse {
  public $GetVersionResults; // GetVersionResults
}

class GetVersionResults {
  public $ErrorMessage; // string
  public $Mode; // string
  public $PostalSystem; // string
  public $ServerName; // string
  public $Status; // int
  public $Version; // string
}

class GetVersionXML {
}

class GetVersionXMLResponse {
  public $GetVersionResults; // GetVersionResults
}

class GetAccountStatus {
  public $AccountStatusRequest; // AccountStatusRequest
}

class AccountStatusRequest {
  public $RequesterID; // string
  public $RequestID; // string
  public $CertifiedIntermediary; // CertifiedIntermediary
}

class DataValidator {
}

class CertifiedIntermediary {
  public $AccountID; // string
  public $PassPhrase; // string
}

class GetAccountStatusResponse {
  public $AccountStatusResponse; // AccountStatusResponse
}

class AccountStatusResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $RequesterID; // string
  public $RequestID; // string
  public $CertifiedIntermediary; // CertifiedIntermediaryStatus
}

class CertifiedIntermediaryStatus {
  public $AccountID; // string
  public $SerialNumber; // int
  public $PostageBalance; // decimal
  public $AscendingBalance; // decimal
  public $AccountStatus; // string
  public $DeviceID; // string
}

class GetAccountStatusXML {
  public $AccountStatusRequestXML; // string
}

class GetAccountStatusXMLResponse {
  public $AccountStatusResponse; // AccountStatusResponse
}

class GetChallengeQuestion {
  public $ChallengeQuestionRequest; // ChallengeQuestionRequest
}

class ChallengeQuestionRequest {
  public $RequesterID; // string
  public $RequestID; // string
  public $AccountID; // string
  public $EMail; // string
}

class GetChallengeQuestionResponse {
  public $ChallengeQuestionResponse; // ChallengeQuestionResponse
}

class ChallengeQuestionResponse {
  public $Question; // string
  public $ErrorMessage; // string
  public $Status; // int
  public $RequesterID; // string
  public $RequestID; // string
}

class GetChallengeQuestionXML {
  public $ChallengeQuestionRequestXML; // string
}

class GetChallengeQuestionXMLResponse {
  public $ChallengeQuestionResponse; // ChallengeQuestionResponse
}

class CalculateStampRate {
  public $StampsRateRequest; // StampsRateRequest
}

class StampsRateRequest {
  public $AccountID; // string
  public $PassPhrase; // string
  public $ShipDate; // string
  public $ShipTime; // string
  public $Extension; // string
  public $DateAdvance; // int
  public $StampRequests; // ArrayOfStampRequest
}

class StampRequest {
  public $MailpieceShape; // string
  public $MailClass; // string
  public $ToCountryCode; // string
  public $WeightOz; // double
  public $UseUserRate; // string
  public $UserRate; // decimal
  public $Count; // int
}

class CalculateStampRateResponse {
  public $StampsRateResponse; // StampsRateResponse
}

class StampsRateResponse {
  public $StampsRequested; // int
  public $StatusPostage; // int
  public $ErrorMessagePostage; // string
  public $SuccessCountPostage; // int
  public $TotalRate; // decimal
  public $StampRateResponses; // ArrayOfStampRateResponse
}

class StampRateResponse {
  public $StatusPostage; // int
  public $ErrorMessagePostage; // string
  public $PerItemRate; // decimal
}

class ValidationRequestHeader {
  public $ValidClientCode; // string
  public $ValidClientName; // string
}

class CalculateStampRateXML {
  public $StampsRateRequestXML; // string
}

class CalculateStampRateXMLResponse {
  public $StampsRateResponse; // StampsRateResponse
}

class CalculatePostageRate {
  public $PostageRateRequest; // PostageRateRequest
}

class PostageRateRequest {
  public $RequesterID; // string
  public $CertifiedIntermediary; // CertifiedIntermediary
  public $AutomationRate; // string
  public $Machinable; // string
  public $ServiceLevel; // string
  public $SortType; // string
  public $Services; // SpecialServices
  public $Value; // float
  public $CODAmount; // double
  public $InsuredValue; // string
  public $RegisteredMailValue; // double
  public $EntryFacility; // string
  public $FromPostalCode; // string
  public $ToPostalCode; // string
  public $ToCountry; // string
  public $ToCountryCode; // string
  public $ShipDate; // string
  public $ShipTime; // string
  public $ResponseOptions; // ResponseOptions
}

class CommonLabelRequestAndPostageRateRequest {
  public $MailpieceShape; // string
  public $MailClass; // string
  public $WeightOz; // double
  public $PackageTypeIndicator; // string
  public $MailpieceDimensions; // Dimensions
  public $DateAdvance; // int
  public $Pricing; // string
  public $SundayHolidayDelivery; // string
  //public $LiveAnimalSurcharge; // string
  public $Extension; // string
}

class Dimensions {
  public $Length; // double
  public $Width; // double
  public $Height; // double
}

class SpecialServices {
  public $DeliveryConfirmation; // string
  public $MailClassOnly; // string
  public $CertifiedMail; // string
  public $COD; // string
  public $ElectronicReturnReceipt; // string
  public $InsuredMail; // string
  public $RegisteredMail; // string
  public $RestrictedDelivery; // string
  public $ReturnReceipt; // string
  public $SignatureConfirmation; // string
  public $HoldForPickup; // string
  public $MerchandiseReturnService; // string
  public $OpenAndDistribute; // string
  public $AdultSignature; // string
  public $AdultSignatureRestrictedDelivery; // string
}

class ResponseOptions {
  public $PostagePrice; // string
}

class CalculatePostageRateResponse {
  public $PostageRateResponse; // PostageRateResponse
}

class PostageRateResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $Zone; // string
  public $Postage; // PostageRate
  public $PostagePrice; // PostagePrice
}

class PostageRate {
  public $MailService; // string
  public $Rate; // decimal
}

class PostagePrice {
  public $MailClass; // string
  public $Postage; // Postage
  public $Fees; // Fees
  public $TotalAmount; // decimal
}

class Postage {
  public $MailService; // string
  public $Zone; // string
  public $IntraBMC; // string
  public $Pricing; // string
  public $TotalAmount; // decimal
}

class Fees {
  public $CertificateOfMailing; // decimal
  public $CertifiedMail; // decimal
  public $CollectOnDelivery; // decimal
  public $DeliveryConfirmation; // decimal
  public $ElectronicReturnReceipt; // decimal
  public $InsuredMail; // decimal
  public $RegisteredMail; // decimal
  public $RestrictedDelivery; // decimal
  public $ReturnReceipt; // decimal
  public $ReturnReceiptForMerchandise; // decimal
  public $SignatureConfirmation; // decimal
  public $SpecialHandling; // decimal
  public $CriticalMail; // decimal
  public $MerchandiseReturn; // decimal
  public $OpenAndDistribute; // decimal
  public $AdultSignature; // decimal
  public $AdultSignatureRestrictedDelivery; // decimal
  //public $LiveAnimalSurcharge; // decimal
  public $TotalAmount; // decimal
}

class CalculatePostageRateXML {
  public $PostageRateRequestXML; // string
}

class CalculatePostageRateXMLResponse {
  public $PostageRateResponse; // PostageRateResponse
}

class BuyPostage {
  public $RecreditRequest; // RecreditRequest
}

class RecreditRequest {
  public $RequesterID; // string
  public $RequestID; // string
  public $CertifiedIntermediary; // CertifiedIntermediary
  public $RecreditAmount; // string
}

class BuyPostageResponse {
  public $RecreditRequestResponse; // RecreditRequestResponse
}

class RecreditRequestResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $RequesterID; // string
  public $RequestID; // string
  public $CertifiedIntermediary; // CertifiedIntermediaryStatus
}

class BuyPostageXML {
  public $RecreditRequestXML; // string
}

class BuyPostageXMLResponse {
  public $RecreditRequestResponse; // RecreditRequestResponse
}

class CalculatePostageRates {
  public $PostageRatesRequest; // PostageRatesRequest
}

class PostageRatesRequest {
  public $RequesterID; // string
  public $CertifiedIntermediary; // CertifiedIntermediary
  public $MailClass; // string
  public $WeightOz; // double
  public $MailpieceShape; // string
  public $MailpieceDimensions; // Dimensions
  public $Machinable; // string
  public $Services; // SpecialServices
  public $CODAmount; // double
  public $InsuredValue; // double
  public $RegisteredMailValue; // double
  public $FromPostalCode; // string
  public $ToPostalCode; // string
  public $ToCountry; // string
  public $ToCountryCode; // string
  public $DateAdvance; // int
  public $Extension; // string
}

class CalculatePostageRatesResponse {
  public $PostageRatesResponse; // PostageRatesResponse
}

class PostageRatesResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $PostagePrice; // PostagePrice
}

class CalculatePostageRatesXML {
  public $PostageRatesRequestXML; // string
}

class CalculatePostageRatesXMLResponse {
  public $PostageRatesResponse; // PostageRatesResponse
}

class ResetSuspendedAccount {
  public $ResetSuspendedAccountRequest; // ResetSuspendedAccountRequest
}

class ResetSuspendedAccountRequest {
  public $RequesterID; // string
  public $RequestID; // string
  public $AccountID; // string
  public $ChallengeAnswer; // string
  public $NewPassPhrase; // string
}

class ResetSuspendedAccountResponse {
  public $ResetSuspendedAccountRequestResponse; // ResetSuspendedAccountRequestResponse
}

class ResetSuspendedAccountRequestResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $RequesterID; // string
  public $RequestID; // string
}

class ResetSuspendedAccountXML {
  public $ResetSuspendedAccountRequestXML; // string
}

class ResetSuspendedAccountXMLResponse {
  public $ResetSuspendedAccountRequestResponse; // ResetSuspendedAccountRequestResponse
}

class GetStamps {
  public $StampsRequest; // StampsRequest
}

class StampsRequest {
  public $RequesterID; // string
  public $MediaType; // string
  public $XOffset; // decimal
  public $YOffset; // decimal
  public $XStartLabel; // int
  public $YStartLabel; // int
  public $ActivationCode; // string
  public $Test; // string
  public $ImageFormat; // string
}

class GetStampsResponse {
  public $StampsResponse; // StampsResponse
}

class StampsResponse {
  public $StampsRequested; // int
  public $StatusPostage; // int
  public $ErrorMessagePostage; // string
  public $SuccessCountPostage; // int
  public $TotalRate; // decimal
  public $StampResponses; // ArrayOfStampResponse
  public $Base64LabelImage; // string
  public $StatusPrinting; // int
  public $ErrorMessagePrinting; // string
  public $SuccessCountPrinting; // int
}

class StampResponse {
  public $SuccessCountPostage; // int
  public $StatusPrinting; // int
  public $ErrorMessagePrinting; // string
  public $SuccessCountPrinting; // int
  public $TotalRates; // decimal
  public $StampsRequested; // int
}

class GetStampsXML {
  public $StampsRequestXML; // string
}

class GetStampsXMLResponse {
  public $StampsResponse; // StampsResponse
}

class ChangePassPhrase {
  public $ChangePassPhraseRequest; // ChangePassPhraseRequest
}

class ChangePassPhraseRequest {
  public $RequesterID; // string
  public $RequestID; // string
  public $CertifiedIntermediary; // CertifiedIntermediary
  public $NewPassPhrase; // string
}

class ChangePassPhraseResponse {
  public $ChangePassPhraseRequestResponse; // ChangePassPhraseRequestResponse
}

class ChangePassPhraseRequestResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $RequesterID; // string
  public $RequestID; // string
}

class ChangePassPhraseXML {
  public $ChangePassPhraseRequestXML; // string
}

class ChangePassPhraseXMLResponse {
  public $ChangePassPhraseRequestResponse; // ChangePassPhraseRequestResponse
}

class GetPostageLabel {
  public $LabelRequest; // LabelRequest
}

class LabelRequest {
  public $RequesterID; // string
  public $AccountID; // string
  public $PassPhrase; // string
  public $AutomationRate; // string
  public $Machinable; // string
  public $ServiceLevel; // string
  public $SortType; // string
  public $IncludePostage; // string
  public $ReplyPostage; // string
  public $ShowReturnAddress; // string
  public $Stealth; // string
  public $ValidateAddress; // string
  public $SignatureWaiver; // string
  public $NoWeekendDelivery; // string
  public $Services; // SpecialServices
  public $CostCenter; // int
  public $Value; // float
  public $CODAmount; // double
  public $InsuredValue; // string
  public $RegisteredMailValue; // double
  public $Description; // string
  public $IntegratedFormType; // string
  public $CustomsFormType; // string
  public $CustomsFormImageFormat; // string
  public $CustomsFormImageResolution; // string
  public $OriginCountry; // string
  public $ContentsType; // string
  public $ContentsExplanation; // string
  public $NonDeliveryOption; // string
  public $ReferenceID; // string
  public $PartnerCustomerID; // string
  public $PartnerTransactionID; // string
  public $BpodClientDunsNumber; // string
  public $RubberStamp1; // string
  public $RubberStamp2; // string
  public $RubberStamp3; // string
  public $EntryFacility; // string
  public $POZipCode; // string
  public $ShipDate; // string
  public $ShipTime; // string
  public $CustomsInfo; // CustomsInfo
  public $CustomsCertify; // string
  public $CustomsSigner; // string
  public $HfpEmailAddress; // string
  public $HfpSMS; // string
  public $HfpFacilityID; // string
  public $MRSPermitNo; // string
  public $MRSPermitCityStateZIP; // string
  public $MRSPermitFirm; // string
  public $MRSPermitStreet; // string
  public $MRSRMANumber; // string
  public $OpenAndDistributeFacilityType; // string
  public $OpenAndDistributeFacilityName; // string
  public $OpenAndDistributeTray; // string
  public $OpenAndDistributeMailClassEnclosed; // string
  public $OpenAndDistributeMailClassOther; // string
  public $GXGFedexTrackingNumber; // string
  public $GXGUSPSTrackingNumber; // string
  public $PrintConsolidatorLabel; // string
  public $ResponseOptions; // ResponseOptions
  public $FromName; // string
  public $FromCompany; // string
  public $ReturnAddress1; // string
  public $ReturnAddress2; // string
  public $ReturnAddress3; // string
  public $ReturnAddress4; // string
  public $FromCity; // string
  public $FromState; // string
  public $FromPostalCode; // string
  public $FromZIP4; // string
  public $FromCountry; // string
  public $FromPhone; // string
  public $FromEMail; // string
  public $ToName; // string
  public $ToCompany; // string
  public $ToAddress1; // string
  public $ToAddress2; // string
  public $ToAddress3; // string
  public $ToAddress4; // string
  public $ToCity; // string
  public $ToState; // string
  public $ToPostalCode; // string
  public $ToZIP4; // string
  public $ToDeliveryPoint; // string
  public $ToCountry; // string
  public $ToCountryCode; // string
  public $ToPhone; // string
  public $ToEMail; // string
  public $CustomsCountry1; // string
  public $CustomsDescription1; // string
  public $CustomsQuantity1; // unsignedInt
  public $CustomsValue1; // float
  public $CustomsWeight1; // unsignedInt
  public $CustomsCountry2; // string
  public $CustomsDescription2; // string
  public $CustomsQuantity2; // unsignedInt
  public $CustomsValue2; // float
  public $CustomsWeight2; // unsignedInt
  public $CustomsCountry3; // string
  public $CustomsDescription3; // string
  public $CustomsQuantity3; // unsignedInt
  public $CustomsValue3; // float
  public $CustomsWeight3; // unsignedInt
  public $CustomsCountry4; // string
  public $CustomsDescription4; // string
  public $CustomsQuantity4; // unsignedInt
  public $CustomsValue4; // float
  public $CustomsWeight4; // unsignedInt
  public $CustomsCountry5; // string
  public $CustomsDescription5; // string
  public $CustomsQuantity5; // unsignedInt
  public $CustomsValue5; // float
  public $CustomsWeight5; // unsignedInt
  public $EelPfc; // string
  public $HfpFacilityName; // string
  public $HfpFacilityAddress1; // string
  public $HfpFacilityCity; // string
  public $HfpFacilityState; // string
  public $HfpFacilityPostalCode; // string
  public $HfpFacilityZIP4; // string
  public $Test; // string
  public $LabelType; // string
  public $LabelSubtype; // string
  public $LabelSize; // string
  public $ImageFormat; // string
  public $ImageResolution; // string
  public $ImageRotation; // string
}

class CustomsInfo {
  public $ContentsType; // string
  public $ContentsExplanation; // string
  public $RestrictionType; // string
  public $RestrictionComments; // string
  public $SendersCustomsReference; // string
  public $ImportersCustomsReference; // string
  public $LicenseNumber; // string
  public $CertificateNumber; // string
  public $InvoiceNumber; // string
  public $NonDeliveryOption; // string
  public $InsuredNumber; // string
  public $EelPfc; // string
  public $CustomsItems; // ArrayOfCustomsItem
}

class CustomsItem {
  public $Description; // string
  public $Quantity; // int
  public $Weight; // decimal
  public $Value; // decimal
  public $HSTariffNumber; // string
  public $CountryOfOrigin; // string
}

class GetPostageLabelResponse {
  public $LabelRequestResponse; // LabelRequestResponse
}

class LabelRequestResponse {
  public $Status; // int
  public $ErrorMessage; // string
  public $Base64LabelImage; // string
  public $Label; // ImageSet
  public $CustomsForm; // ImageSet
  public $PIC; // string
  public $CustomsNumber; // string
  public $TrackingNumber; // string
  public $FinalPostage; // decimal
  public $TransactionID; // int
  public $TransactionDateTime; // string
  public $PostmarkDate; // string
  public $PostageBalance; // decimal
  public $ReferenceID; // string
  public $CostCenter; // int
  public $HfpFacilityID; // string
  public $HfpFacilityName; // string
  public $HfpFacilityAddress1; // string
  public $HfpFacilityCity; // string
  public $HfpFacilityState; // string
  public $HfpFacilityPostalCode; // string
  public $HfpFacilityZIP4; // string
  public $PostagePrice; // PostagePrice
}

class ImageSet {
  public $Image; // ImageData
  public $Name; // string
}

class ImageData {
  public $_; // string
  public $PartNumber; // int
}

class GetPostageLabelXML {
  public $LabelRequestXML; // string
}

class GetPostageLabelXMLResponse {
  public $LabelRequestResponse; // LabelRequestResponse
}

class GetPostageIndicium {
  public $IndiciumRequest; // IndiciumRequest
}

class IndiciumRequest {
  public $RequestID; // string
  public $SoftwareID; // string
  public $SoftwareVersion; // string
  public $AccountID; // string
  public $PassPhrase; // string
  public $PicMailerID; // string
  public $ClientMailerID; // string
  public $DateAdvance; // int
  public $MailClass; // string
  public $ServiceType; // string
  public $ServiceFee; // decimal
  public $Weight; // decimal
  public $Zone; // int
  public $MailpieceShape; // string
  public $Dimensions; // Dimensions
  public $Machinable; // boolean
  public $SortType; // string
  public $Postage; // decimal
  public $Fees; // decimal
  public $Pricing; // string
  public $PriceTier; // string
  public $CertifiedMail; // boolean
  public $CertifiedMailFee; // decimal
  public $COD; // boolean
  public $CODFee; // decimal
  public $CODAmount; // decimal
  public $InsuredMail; // string
  public $InsuredMailFee; // decimal
  public $InsuredValue; // decimal
  public $RegisteredMail; // boolean
  public $RegisteredMailFee; // decimal
  public $RegisteredMailValue; // decimal
  public $RestrictedDelivery; // boolean
  public $RestrictedDeliveryFee; // decimal
  public $ReturnReceipt; // string
  public $ReturnReceiptFee; // decimal
  public $ServiceLevel; // string
  public $HoldForPickup; // boolean
  public $HfpEmailAddress; // string
  public $HfpSMS; // string
  public $HfpFacilityID; // string
  public $HfpFacilityAddress; // AddressInfo
  public $HfpFacilityName; // string
  public $HfpFacilityAddress1; // string
  public $HfpFacilityCity; // string
  public $HfpFacilityState; // string
  public $HfpFacilityPostalCode; // string
  public $HfpFacilityZIP4; // string
  public $SundayHolidayDelivery; // string
  public $NoDeliverySaturday; // boolean
  public $WaiverOfSignature; // boolean
  public $ServiceClassV2; // string
  public $DialAZipOptions; // DialAZipOptions
  public $FromName; // string
  public $FromCompany; // string
  public $FromAddress1; // string
  public $FromAddress2; // string
  public $FromCity; // string
  public $FromState; // string
  public $FromZIP5; // string
  public $FromZIP4; // string
  public $FromPhone; // string
  public $FromEMail; // string
  public $EntryFacility; // string
  public $POZipCode; // string
  public $ToName; // string
  public $ToCompany; // string
  public $ToAddress1; // string
  public $ToAddress2; // string
  public $ToAddress3; // string
  public $ToCity; // string
  public $ToState; // string
  public $ToPostalCode; // string
  public $ToDeliveryPoint; // string
  public $ToCountry; // string
  public $ToCountryCode; // string
  public $ToPhone; // string
  public $ToFax; // string
  public $ToEMail; // string
  public $ToPOBox; // boolean
  public $DestinationFacility; // string
  public $Description; // string
  public $originCountryField; // string
  public $contentsTypeField; // string
  public $contentsExplanationField; // string
  public $nonDeliveryOptionField; // string
  public $PackageReference1; // string
  public $PackageReference2; // string
  public $PackageReference3; // string
  public $CostCenter; // int
  public $SurchargeType; // string
  public $SurchargeAmount; // decimal
  public $SurchargeDescription; // string
  public $ShipDate; // string
  public $ShipTime; // string
  public $CustomsInfo; // CustomsInfo
  public $customsCertify; // string
  public $customsSigner; // string
  public $ComputeRDC; // boolean
  public $validationLevel; // string
  public $domesticMail; // boolean
  public $PackageTypeIndicator; // string
  public $ValidateAddress; // boolean
  public $PrintConsolidatorLabel; // boolean
  public $DHLMailClass; // string
  public $DHLFacilityCode; // string
  public $IncludePostage; // boolean
  public $consoliatorAccountNumber; // string
  public $Test; // boolean
}

class AddressInfo {
  public $AddressLines; // ArrayOfString
  public $City; // string
  public $Company; // string
  public $Country; // string
  public $CountryCode; // string
  public $DeliveryPoint; // string
  public $EMail; // string
  public $Name; // string
  public $Phone; // string
  public $PostalCode; // string
  public $State; // string
  public $ZIP4; // string
}

class DialAZipOptions {
  public $ValidateSenderAddress; // boolean
  public $ValidateRecipientAddress; // boolean
  public $AllowAPOFPOAddressNotFound; // boolean
  public $AllowStreetAddressNotMatch; // boolean
  public $AllowDefaultMatch; // boolean
}

class GetPostageIndiciumResponse {
  public $IndiciumResponse; // IndiciumResponse
}

class IndiciumResponse {
  public $BarcodeDisplayNumber; // string
  public $BarcodeNumber; // string
  public $HfpFacilityID; // string
  public $HfpFacilityName; // string
  public $HfpFacilityAddress1; // string
  public $HfpFacilityCity; // string
  public $HfpFacilityState; // string
  public $HfpFacilityPostalCode; // string
  public $HfpFacilityZIP4; // string
  public $DHLInboundSortCode; // string
  public $DHLOutboundSortCode; // string
  public $DHLDestMailTerminal; // string
  public $DHLMailType; // string
  public $DHLITVersion; // string
  public $RequestID; // string
  public $Status; // int
  public $ErrorMessage; // string
  public $Indicium; // string
  public $TransactionID; // int
  public $TransactionAmount; // decimal
  public $TransactionDateTime; // string
  public $PostageBalance; // decimal
  public $LabelNumber; // string
  public $LabelNumberType; // string
  public $LabelDate; // string
  public $RecipientAddress; // ValidatedAddress
  public $SDRValue; // decimal
  public $RDC; // string
  public $ServerName; // string
  public $ServerDateTime; // string
  public $Banner; // string
}

class ValidatedAddress {
  public $ReturnCode; // int
  public $Name; // string
  public $Company; // string
  public $Address1; // string
  public $Address2; // string
  public $City; // string
  public $State; // string
  public $ZIP5; // string
  public $Plus4; // string
  public $DeliveryPoint; // string
  public $CRT; // string
  public $RDI; // string
}

class GetPostageIndiciumXML {
  public $IndiciumRequestXML; // string
}

class GetPostageIndiciumXMLResponse {
  public $IndiciumResponse; // IndiciumResponse
}


/**
 * EwsLabelService class
 * 
 * You have reached the Help page for the Label Server Web Service. 
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class EwsLabelService extends SoapClient {

  private static $classmap = array(
                                    'GetVersion' => 'GetVersion',
                                    'GetVersionResponse' => 'GetVersionResponse',
                                    'GetVersionResults' => 'GetVersionResults',
                                    'GetVersionXML' => 'GetVersionXML',
                                    'GetVersionXMLResponse' => 'GetVersionXMLResponse',
                                    'GetAccountStatus' => 'GetAccountStatus',
                                    'AccountStatusRequest' => 'AccountStatusRequest',
                                    'DataValidator' => 'DataValidator',
                                    'CertifiedIntermediary' => 'CertifiedIntermediary',
                                    'GetAccountStatusResponse' => 'GetAccountStatusResponse',
                                    'AccountStatusResponse' => 'AccountStatusResponse',
                                    'CertifiedIntermediaryStatus' => 'CertifiedIntermediaryStatus',
                                    'GetAccountStatusXML' => 'GetAccountStatusXML',
                                    'GetAccountStatusXMLResponse' => 'GetAccountStatusXMLResponse',
                                    'GetChallengeQuestion' => 'GetChallengeQuestion',
                                    'ChallengeQuestionRequest' => 'ChallengeQuestionRequest',
                                    'GetChallengeQuestionResponse' => 'GetChallengeQuestionResponse',
                                    'ChallengeQuestionResponse' => 'ChallengeQuestionResponse',
                                    'GetChallengeQuestionXML' => 'GetChallengeQuestionXML',
                                    'GetChallengeQuestionXMLResponse' => 'GetChallengeQuestionXMLResponse',
                                    'CalculateStampRate' => 'CalculateStampRate',
                                    'StampsRateRequest' => 'StampsRateRequest',
                                    'StampRequest' => 'StampRequest',
                                    'CalculateStampRateResponse' => 'CalculateStampRateResponse',
                                    'StampsRateResponse' => 'StampsRateResponse',
                                    'StampRateResponse' => 'StampRateResponse',
                                    'ValidationRequestHeader' => 'ValidationRequestHeader',
                                    'CalculateStampRateXML' => 'CalculateStampRateXML',
                                    'CalculateStampRateXMLResponse' => 'CalculateStampRateXMLResponse',
                                    'CalculatePostageRate' => 'CalculatePostageRate',
                                    'PostageRateRequest' => 'PostageRateRequest',
                                    'CommonLabelRequestAndPostageRateRequest' => 'CommonLabelRequestAndPostageRateRequest',
                                    'Dimensions' => 'Dimensions',
                                    'SpecialServices' => 'SpecialServices',
                                    'ResponseOptions' => 'ResponseOptions',
                                    'CalculatePostageRateResponse' => 'CalculatePostageRateResponse',
                                    'PostageRateResponse' => 'PostageRateResponse',
                                    'PostageRate' => 'PostageRate',
                                    'PostagePrice' => 'PostagePrice',
                                    'Postage' => 'Postage',
                                    'Fees' => 'Fees',
                                    'CalculatePostageRateXML' => 'CalculatePostageRateXML',
                                    'CalculatePostageRateXMLResponse' => 'CalculatePostageRateXMLResponse',
                                    'BuyPostage' => 'BuyPostage',
                                    'RecreditRequest' => 'RecreditRequest',
                                    'BuyPostageResponse' => 'BuyPostageResponse',
                                    'RecreditRequestResponse' => 'RecreditRequestResponse',
                                    'BuyPostageXML' => 'BuyPostageXML',
                                    'BuyPostageXMLResponse' => 'BuyPostageXMLResponse',
                                    'CalculatePostageRates' => 'CalculatePostageRates',
                                    'PostageRatesRequest' => 'PostageRatesRequest',
                                    'CalculatePostageRatesResponse' => 'CalculatePostageRatesResponse',
                                    'PostageRatesResponse' => 'PostageRatesResponse',
                                    'CalculatePostageRatesXML' => 'CalculatePostageRatesXML',
                                    'CalculatePostageRatesXMLResponse' => 'CalculatePostageRatesXMLResponse',
                                    'ResetSuspendedAccount' => 'ResetSuspendedAccount',
                                    'ResetSuspendedAccountRequest' => 'ResetSuspendedAccountRequest',
                                    'ResetSuspendedAccountResponse' => 'ResetSuspendedAccountResponse',
                                    'ResetSuspendedAccountRequestResponse' => 'ResetSuspendedAccountRequestResponse',
                                    'ResetSuspendedAccountXML' => 'ResetSuspendedAccountXML',
                                    'ResetSuspendedAccountXMLResponse' => 'ResetSuspendedAccountXMLResponse',
                                    'GetStamps' => 'GetStamps',
                                    'StampsRequest' => 'StampsRequest',
                                    'GetStampsResponse' => 'GetStampsResponse',
                                    'StampsResponse' => 'StampsResponse',
                                    'StampResponse' => 'StampResponse',
                                    'GetStampsXML' => 'GetStampsXML',
                                    'GetStampsXMLResponse' => 'GetStampsXMLResponse',
                                    'ChangePassPhrase' => 'ChangePassPhrase',
                                    'ChangePassPhraseRequest' => 'ChangePassPhraseRequest',
                                    'ChangePassPhraseResponse' => 'ChangePassPhraseResponse',
                                    'ChangePassPhraseRequestResponse' => 'ChangePassPhraseRequestResponse',
                                    'ChangePassPhraseXML' => 'ChangePassPhraseXML',
                                    'ChangePassPhraseXMLResponse' => 'ChangePassPhraseXMLResponse',
                                    'GetPostageLabel' => 'GetPostageLabel',
                                    'LabelRequest' => 'LabelRequest',
                                    'CustomsInfo' => 'CustomsInfo',
                                    'CustomsItem' => 'CustomsItem',
                                    'GetPostageLabelResponse' => 'GetPostageLabelResponse',
                                    'LabelRequestResponse' => 'LabelRequestResponse',
                                    'ImageSet' => 'ImageSet',
                                    'ImageData' => 'ImageData',
                                    'GetPostageLabelXML' => 'GetPostageLabelXML',
                                    'GetPostageLabelXMLResponse' => 'GetPostageLabelXMLResponse',
                                    'GetPostageIndicium' => 'GetPostageIndicium',
                                    'IndiciumRequest' => 'IndiciumRequest',
                                    'AddressInfo' => 'AddressInfo',
                                    'DialAZipOptions' => 'DialAZipOptions',
                                    'GetPostageIndiciumResponse' => 'GetPostageIndiciumResponse',
                                    'IndiciumResponse' => 'IndiciumResponse',
                                    'ValidatedAddress' => 'ValidatedAddress',
                                    'GetPostageIndiciumXML' => 'GetPostageIndiciumXML',
                                    'GetPostageIndiciumXMLResponse' => 'GetPostageIndiciumXMLResponse',
                                   );
//http://NAPALDEVAPP04.dev.psisystems.local/LabelService3/EwsLabelService.asmx?wsdl
//https://labelserver.endicia.com/LabelService/EwsLabelService.asmx?wsdl


  public function EwsLabelService($wsdl = "https://labelserver.endicia.com/LabelService/EwsLabelService.asmx?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GetVersion $parameters
   * @return GetVersionResponse
   */
  public function GetVersion(GetVersion $parameters) {
    return $this->__soapCall('GetVersion', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetVersionXML $parameters
   * @return GetVersionXMLResponse
   */
  public function GetVersionXML(GetVersionXML $parameters) {
    return $this->__soapCall('GetVersionXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetAccountStatus $parameters
   * @return GetAccountStatusResponse
   */
  public function GetAccountStatus(GetAccountStatus $parameters) {
    return $this->__soapCall('GetAccountStatus', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetAccountStatusXML $parameters
   * @return GetAccountStatusXMLResponse
   */
  public function GetAccountStatusXML(GetAccountStatusXML $parameters) {
    return $this->__soapCall('GetAccountStatusXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetChallengeQuestion $parameters
   * @return GetChallengeQuestionResponse
   */
  public function GetChallengeQuestion(GetChallengeQuestion $parameters) {
    return $this->__soapCall('GetChallengeQuestion', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetChallengeQuestionXML $parameters
   * @return GetChallengeQuestionXMLResponse
   */
  public function GetChallengeQuestionXML(GetChallengeQuestionXML $parameters) {
    return $this->__soapCall('GetChallengeQuestionXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculateStampRate $parameters
   * @return CalculateStampRateResponse
   */
  public function CalculateStampRate(CalculateStampRate $parameters) {
    return $this->__soapCall('CalculateStampRate', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculateStampRateXML $parameters
   * @return CalculateStampRateXMLResponse
   */
  public function CalculateStampRateXML(CalculateStampRateXML $parameters) {
    return $this->__soapCall('CalculateStampRateXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculatePostageRate $parameters
   * @return CalculatePostageRateResponse
   */
  public function CalculatePostageRate(CalculatePostageRate $parameters) {
    return $this->__soapCall('CalculatePostageRate', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculatePostageRateXML $parameters
   * @return CalculatePostageRateXMLResponse
   */
  public function CalculatePostageRateXML(CalculatePostageRateXML $parameters) {
    return $this->__soapCall('CalculatePostageRateXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param BuyPostage $parameters
   * @return BuyPostageResponse
   */
  public function BuyPostage(BuyPostage $parameters) {
    return $this->__soapCall('BuyPostage', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param BuyPostageXML $parameters
   * @return BuyPostageXMLResponse
   */
  public function BuyPostageXML(BuyPostageXML $parameters) {
    return $this->__soapCall('BuyPostageXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculatePostageRates $parameters
   * @return CalculatePostageRatesResponse
   */
  public function CalculatePostageRates(CalculatePostageRates $parameters) {
    return $this->__soapCall('CalculatePostageRates', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CalculatePostageRatesXML $parameters
   * @return CalculatePostageRatesXMLResponse
   */
  public function CalculatePostageRatesXML(CalculatePostageRatesXML $parameters) {
    return $this->__soapCall('CalculatePostageRatesXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ResetSuspendedAccount $parameters
   * @return ResetSuspendedAccountResponse
   */
  public function ResetSuspendedAccount(ResetSuspendedAccount $parameters) {
    return $this->__soapCall('ResetSuspendedAccount', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ResetSuspendedAccountXML $parameters
   * @return ResetSuspendedAccountXMLResponse
   */
  public function ResetSuspendedAccountXML(ResetSuspendedAccountXML $parameters) {
    return $this->__soapCall('ResetSuspendedAccountXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetStamps $parameters
   * @return GetStampsResponse
   */
  public function GetStamps(GetStamps $parameters) {
    return $this->__soapCall('GetStamps', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetStampsXML $parameters
   * @return GetStampsXMLResponse
   */
  public function GetStampsXML(GetStampsXML $parameters) {
    return $this->__soapCall('GetStampsXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ChangePassPhrase $parameters
   * @return ChangePassPhraseResponse
   */
  public function ChangePassPhrase(ChangePassPhrase $parameters) {
    return $this->__soapCall('ChangePassPhrase', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ChangePassPhraseXML $parameters
   * @return ChangePassPhraseXMLResponse
   */
  public function ChangePassPhraseXML(ChangePassPhraseXML $parameters) {
    return $this->__soapCall('ChangePassPhraseXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetPostageLabel $parameters
   * @return GetPostageLabelResponse
   */
  public function GetPostageLabel(GetPostageLabel $parameters) {
    return $this->__soapCall('GetPostageLabel', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetPostageLabelXML $parameters
   * @return GetPostageLabelXMLResponse
   */
  public function GetPostageLabelXML(GetPostageLabelXML $parameters) {
    return $this->__soapCall('GetPostageLabelXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetPostageIndicium $parameters
   * @return GetPostageIndiciumResponse
   */
  public function GetPostageIndicium(GetPostageIndicium $parameters) {
    return $this->__soapCall('GetPostageIndicium', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetPostageIndiciumXML $parameters
   * @return GetPostageIndiciumXMLResponse
   */
  public function GetPostageIndiciumXML(GetPostageIndiciumXML $parameters) {
    return $this->__soapCall('GetPostageIndiciumXML', array($parameters),       array(
            'uri' => 'www.envmgr.com/LabelService',
            'soapaction' => ''
           )
      );
  }

}

?>
