<?php
ini_set('memory_limit', '-1');
set_time_limit(9999999999);

REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

//ini_set('display_errors',1);
//error_reporting(E_ALL^E_NOTICE);
require_once("EwsLabelService.php");
// *************** This code will request a shipping label in PDF format *********************

// This will ensure that their isn't a 30 second timeout on this script so you can run the script as many cycles as you wish.



// User Inputs
REQUIRE($ROOT_DIR."/mgt/ship/return_labels/generateXML.phtml");
$shl= new generateXML($efc);
$shl->DB = $DB;
$shl->mysqlid = $mysqlid;
$shl->ROOT_DIR = $ROOT_DIR;



// Timer function to let you know how long the script ran for.
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


if ($orderid != '') {
$tmp_orderid = $orderid;
	
	$qorder = "SELECT customer_orders.orderid,
	                  customer_orders.invoiceid,
	                  customer_orders.divid,
	                  customer_orders.phone,
	                  customer_orders.ship2zip,
	                  customer_orders.ship2name,
	                  customer_orders.ship2street1,
	                  customer_orders.ship2street2,
	                  customer_orders.ship2city,
	                  customer_orders.ship2state,
	                  customer_orders.ship2country,
	                  customer_orders.shipping,
	                  customer_order_lines.qty_ordered,
	                  customer_order_lines.qty_shipped,
	                  customer_order_lines.prodid
	                    FROM customer_orders,customer_order_lines 
	                    WHERE customer_orders.orderid=customer_order_lines.orderid AND 
	                          customer_orders.orderid='".$orderid."'";
	   
	if(!$qok = mysql_db_query($DB, $qorder, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error";echo $q; exit; }  }
	
	while (list($orderid,$invoiceid,$divid,$ship2phone,$ship2zip,$ship2name,$ship2street1,$ship2street2,$ship2city,$ship2state,$ship2country,$shipping,$qty_ordered,$qty_shipped,$original_pid) = mysql_fetch_array($qok)) {
	    $tmpzip = split("-",$ship2zip);
		
		$o['orderid'] = $orderid;
	    $o['invoiceid'] = $invoiceid;
	    $o['divid'] = $divid;
	    $o['ship2zip'] = $tmpzip[0];
	    $o['ship2name'] = $ship2name;
	    $o['ship2street1'] = $ship2street1;
	    $o['ship2street2'] = $ship2street2;
	    $o['ship2city'] = $ship2city;
	    $o['ship2state'] = $ship2state;
	    $o['ship2country'] = $ship2country;
	    if($ship2phone == "") {
	        $ship2phone = "888-555-1212";
	    }
	    $o['ship2phone'] = $ship2phone;
	    $o['shipdate'] = date("m/d/Y", time());;
	    $o['zone'] = $zone;
	    $o['qty_ordered'] = $qty_ordered;
	    $o['qty_shipped'] = $qty_shipped;
	    $o['shipping'] = $shipping;
	    
	    $querya = "select descr, shipcode, sku, shipweight,refurb from product_list where id = '$original_pid'";
	    if(!$qoka = mysql_db_query($DB, $querya, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error";echo $q; exit; }  }
	    list($descr, $shipcode,$sku,$shipweight,$refurb) = mysql_fetch_array($qoka);
	    $o['weight'] += $qty_ordered*$shipweight;
		
	}
	
	$shl->endicia_method = $method;
	$referenceID = $o['invoiceid'];
	$data = $shl->createRequest($o);
	//print_r($data);
	//exit();
	
	$labelRequest = $data;
	$labelServer = new EwsLabelService();
	
	// Used to test the timing on the script.
	$timeStart = microtime_float();
	
	// Make Label Request to ELS
	$getPostageLabelXML = new GetPostageLabelXML();
	$getPostageLabelXML->LabelRequestXML = $labelRequest;
	$labelResult = $labelServer->GetPostageLabelXML($getPostageLabelXML);
	
	// Used to test the timing on the script.
	$timeEnd = microtime_float();
	$statusRequestDuration = $timeEnd - $timeStart;
	
	
	$errorMessage = $labelResult->LabelRequestResponse->ErrorMessage;
	if (!$errorMessage){
		$labelImage = $labelResult->LabelRequestResponse->Base64LabelImage;
		$trackingNumber = $labelResult->LabelRequestResponse->PIC;
		$statusCode = $labelResult->LabelRequestResponse->Status;
		$finalPostage = $labelResult->LabelRequestResponse->FinalPostage;	
	
		$fileContents = base64_decode($labelImage);
		//echo $fileContents;
		$file = trim($referenceID) . "_" . $trackingNumber . ".pdf";
		$fileName = "./pdf/" . $file;
		$fh = fopen($fileName, "w+");
		fwrite($fh, $fileContents);
		fclose($fh);
	//	$htmlMessage = "<h3><font color=\"red\">Link to the file: </font><a href=\"pdf/".$file."\">" .$file. "</a><h3>";
		
		//log the printing
		$trackingLogDetails = "\n TEST - " . $_SERVER['REMOTE_ADDR'] . ", ". date("r") . ", " .  $customerName . ", " . $referenceID . ", " . $packageWeight . ", \" " . $trackingNumber . "\", " . "http://www.postalsupport.com/ashleystewart/pdf/" . $file;
		$textFile = $ROOT_DIR."/mgt/ship/return_labels/pdf/tracking.csv";
		$fht = fopen($textFile, "a");
		fwrite($fht, $trackingLogDetails);
		fclose($fht);
		
		$querya = "INSERT INTO labels_return_log(orderid,method,label,trackno) VALUES('".$tmp_orderid."','".$method."','".$file."','".$trackingNumber."')";
		if(!$qoka = mysql_db_query($DB, $querya, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error";echo $q; exit; }  }
		echo "OK~".$file."~".$trackingNumber;
	} else {
		echo "ERROR~Label request failed: ".$errorMessage;
	}
	
}
?>