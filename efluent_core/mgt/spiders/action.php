<?
function buildImage($currentfile, $targetwidth, $targetheight, $newfile)
{
	$im = imagecreatefromjpeg($currentfile);

	if(!$im)
	{
		$cmd = "/usr/bin/convert $currentfile $currentfile";
		exec($cmd, $exec_output, $exec_retval);
	}

	$im = imagecreatefromjpeg($currentfile);

	if(!$im)
	{
		return false;
	}

	$image_width = imagesx($im);
	$image_height = imagesy($im);

	//$new_h = $oh;
	//$new_w = $ow;
	$sizefactor = 1;

	if (($image_height >= $targetheight) || ($image_width >= $targetwidth))
	{
		if ($image_height >= $image_width) {$sizefactor = (double) ($targetheight / $image_height);}
		else {$sizefactor = (double) ($targetwidth / $image_width);}
	}

	$newwidth = (int) ($image_width * $sizefactor);
	$newheight = (int) ($image_height * $sizefactor);
	$newsize = $newwidth . "x" . $newheight;

	//create dst image
	$dst_img = imagecreatetruecolor($targetwidth,$targetheight);

	// bug fix for the black border
	$bug_img = imagecreate($targetwidth, $targetheight);
	$color_white = ImageColorAllocate( $bug_img, 255, 255, 255 );
	imagefill($bug_img, 0, 0, $color_white);
	imagecopy ($dst_img, $bug_img,0, 0, 0, 0, $targetwidth, $targetheight);

	// see if it'll fit
	$dest_x = ($targetwidth / 2) - ($image_width / 2);
	$dest_y = ($targetheight / 2) - ($image_height / 2);

	if($dest_x < 0) $dest_x = 0;
	if($dest_y < 0) $dest_y = 0;

	imagecopyresampled($dst_img, $im,
	$dest_x, $dest_y,  // centered
	0,0,  // copy from the topleft,
	$newwidth, $newheight,  // the target size (keep it the same width)
	$image_width, $image_height // the source size
	);

	imagejpeg($dst_img, $newfile);

	// REMOVE ANY IPICT CODING
	$cmd = "/usr/bin/mogrify +profile iptc $newfile";
	exec($cmd, $exec_output, $exec_retval);

	return true;
}


$a = $_GET['a']; //action
if($a == "image") { $action = "Use Image"; }
if($a == "title") { $action = "Use Title"; }
if($a == "descr") { $action = "Use Description"; }
if($a == "specs") { $action = "Use Tech Specs"; }
if($a == "panic") { $action = "PANIC!!@!@$#%"; }
if($a == "del") { $action = "Delete Cache Data"; }


$dataid = $_GET['dataid'];
$oursku = $_GET['oursku'];
$manpart = $_GET['manpart'];
$engine = $_GET['engine'];

$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;


if($a == "del")
{
	$query = "delete from spidercache where manpart = '$manpart'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$msg = "Deleted collected data for $manpart.";
}

if($a == "panic")
{
	$msg = "Never fear! Product Info RESET!";

	// Copy back the image...
	$query = "select image from spidercache where type = 'RESET' and oursku = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($idata) = mysql_fetch_array($qok);
	$idata = addslashes($idata);
	$query = "delete from product_images where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into product_images(id, idata, itype) values('$oursku', '$idata', '0')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$id = $oursku;
	$query = "select id, imageid, itype from product_images where id = '$id'";
	if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	while(list($id, $imageid, $itype) = mysql_fetch_array($qok))
	{
		// see if a thumb already exists for this image

		$query = "select id from product_images where id = '$id' and itype = '$itype' and thumb = '1'";
		if(!$q4ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($q4ok) < 1)
		{
			//		echo "DOING PID: $id<br>";

			$query = "select idata from product_images where id = '$id' and itype = '$itype' ";
			$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
			list($idata) = mysql_fetch_array($q2ok);

			$sourceFile = "$ROOT_DIR/mgt/temp/$id.jpg";
			$destFile = "$ROOT_DIR/mgt/temp/$id.thumb.jpg";

			//		echo "...DUMPING IMAGE: $sourceFile<br>";
			$fp = fopen($sourceFile, "w");
			fputs($fp, $idata);
			fclose($fp);

			//	echo "...BUILDING THUMB: $destFile<br>";
			if(buildImage($sourceFile, 200, 200, $destFile))
			{
				////////////////////////////////////////////////////////////////

				$imageinfo = getimagesize($destFile);
				$width = $imageinfo[0];
				$height = $imageinfo[1];

				//			echo "...LOADING NEW THUMB: $width x $height<br>";


				$fp = fopen($destFile, "r");
				if($fp == false){echo "ERROR OPENING FILE<BR>";return;}
				$data = "";
				while(!feof($fp))
				{
					$data .= fgets($fp, 4096);
				}
				fclose($fp);
				$data = addslashes($data);

				$query = "insert into product_images(id, idata, itype, thumb, width, height) values('$id', '$data', '$itype', '2', '$width', '$height')";
				if(!$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				//			echo "...CLEANING HOUSE<br>";


				unlink($destFile);
			}

			unlink($sourceFile);
			//		echo "...DONE<br>";
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//	echo "DOING PID: $id<br>";

			$query = "select idata from product_images where id = '$id' and itype = '$itype' ";
			$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
			list($idata) = mysql_fetch_array($q2ok);

			$sourceFile = "$ROOT_DIR/mgt/temp//$id.jpg";
			$destFile = "$ROOT_DIR/mgt/temp//$id.thumb.jpg";

			//		echo "...DUMPING IMAGE: $sourceFile<br>";
			$fp = fopen($sourceFile, "w");
			fputs($fp, $idata);
			fclose($fp);

			//		echo "...BUILDING THUMB: $destFile<br>";
			if(buildImage($sourceFile, 75, 75, $destFile))
			{
				////////////////////////////////////////////////////////////////

				$imageinfo = getimagesize($destFile);
				$width = $imageinfo[0];
				$height = $imageinfo[1];

				//		echo "...LOADING NEW THUMB: $width x $height<BR>";


				$fp = fopen($destFile, "r");
				if($fp == false){echo "ERROR OPENING FILE<BR>";return;}
				$data = "";
				while(!feof($fp))
				{
					$data .= fgets($fp, 4096);
				}
				fclose($fp);
				$data = addslashes($data);

				$query = "insert into product_images(id, idata, itype, thumb, width, height) values('$id', '$data', '$itype', '1', '$width', '$height')";
				if(!$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				//		echo "...CLEANING HOUSE\n";


				unlink($destFile);
			}

			unlink($sourceFile);
			//		echo "...DONE<br>";;


		}
		else
		{
			//		echo "Already Got PID: $id<br>";
		}
	}
	// We copyed over image and made a thumb, now we need to copy over rest of prod info.

	$query = "select title from spidercache where type = 'RESET' and oursku = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($title) = mysql_fetch_array($qok);
	$title = addslashes($title);
	$query = "update product_list set descr='$title' where id='$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }



	$query = "select descr from spidercache where type = 'RESET' and oursku = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($descr) = mysql_fetch_array($qok);
	$descr = addslashes($descr);
	$query = "select data from product_descr where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		$query = "update product_descr set data='$descr' where id='$oursku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}
	else
	{
		$query = "insert into product_descr(id, data) values('$oursku','$descr')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}
	// Ignore techspecs for now, we only write borrowed HTML to product_html, not specs.... Ooops!


	$query = "select specs from spidercache where type = 'RESET' and oursku = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($prodhtml) = mysql_fetch_array($qok);
	$prodhtml = addslashes($prodhtml);
	$query = "select data from product_html where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		$query = "update product_html set data='$prodhtml' where id='$oursku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}
	else
	{
		$query = "insert into product_html(id, data) values('$oursku','$prodhtml')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}


}//end of panic scope.





if($a == "image")
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	$query = "select image from spidercache where type = 'image' and dataid = '$dataid'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($idata) = mysql_fetch_array($qok);
	$idata = addslashes($idata);
	//$query = "delete from product_images where id = '$oursku'";
	//if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "select itype from product_images where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($itype) = mysql_fetch_array($qok);
	if($itype !="") { $itype++; } else { $itype = 0; } 
	$query = "insert into product_images(id, idata, itype) values('$oursku', '$idata', '$itype')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$msg = "Image $dataid => OurSku $oursku, Success!";

	$efc->cache_watermark_image($oursku, $ROOT_DIR); 
	/*
	//$query = "delete from product_images where thumb = 1 and id = '$oursku'";
	//if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$id = $oursku;
	$query = "select id, imageid, itype from product_images where id = '$id'";
	if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	while(list($id, $imageid, $itype) = mysql_fetch_array($qok))
	{
		// see if a thumb already exists for this image

		$query = "select id from product_images where id = '$id' and itype = '$itype' and thumb = '1'";
		if(!$q4ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($q4ok) < 1)
		{
			echo "DOING PID: $id\n";

			$query = "select idata from product_images where id = '$id' and itype = '$itype' ";
			$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
			list($idata) = mysql_fetch_array($q2ok);

			$sourceFile = "$ROOT_DIR/mgt/temp/$id.jpg";
			$destFile = "$ROOT_DIR/mgt/temp/$id.thumb.jpg";

			echo "...DUMPING IMAGE: $sourceFile\n";
			$fp = fopen($sourceFile, "w");
			fputs($fp, $idata);
			fclose($fp);

			echo "...BUILDING THUMB: $destFile\n";
			if(buildImage($sourceFile, 200, 200, $destFile))
			{
				////////////////////////////////////////////////////////////////

				$imageinfo = getimagesize($destFile);
				$width = $imageinfo[0];
				$height = $imageinfo[1];

				echo "...LOADING NEW THUMB: $width x $height\n";


				$fp = fopen($destFile, "r");
				if($fp == false){echo "ERROR OPENING FILE<BR>";return;}
				$data = "";
				while(!feof($fp))
				{
					$data .= fgets($fp, 4096);
				}
				fclose($fp);
				$data = addslashes($data);

				$query = "insert into product_images(id, idata, itype, thumb, width, height) values('$id', '$data', '$itype', '2', '$width', '$height')";
				if(!$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				echo "...CLEANING HOUSE\n";


				unlink($destFile);
			}

			unlink($sourceFile);
			echo "...DONE\n";
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			echo "DOING PID: $id\n";

			$query = "select idata from product_images where id = '$id' and itype = '$itype' ";
			$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
			list($idata) = mysql_fetch_array($q2ok);

			$sourceFile = "$ROOT_DIR/mgt/temp//$id.jpg";
			$destFile = "$ROOT_DIR/mgt/temp//$id.thumb.jpg";

			echo "...DUMPING IMAGE: $sourceFile\n";
			$fp = fopen($sourceFile, "w");
			fputs($fp, $idata);
			fclose($fp);

			echo "...BUILDING THUMB: $destFile\n";
			if(buildImage($sourceFile, 75, 75, $destFile))
			{
				////////////////////////////////////////////////////////////////

				$imageinfo = getimagesize($destFile);
				$width = $imageinfo[0];
				$height = $imageinfo[1];

				echo "...LOADING NEW THUMB: $width x $height\n";


				$fp = fopen($destFile, "r");
				if($fp == false){echo "ERROR OPENING FILE<BR>";return;}
				$data = "";
				while(!feof($fp))
				{
					$data .= fgets($fp, 4096);
				}
				fclose($fp);
				$data = addslashes($data);

				$query = "insert into product_images(id, idata, itype, thumb, width, height) values('$id', '$data', '$itype', '1', '$width', '$height')";
				if(!$q2ok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				echo "...CLEANING HOUSE\n";


				unlink($destFile);
			}

			unlink($sourceFile);
			echo "...DONE<br>";;


		}
		else
		{
			echo "Already Got PID: $id\n";
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

*/

}


if($a == "title")
{
	$query = "select title from spidercache where type = 'title' and manpart = '$manpart' and engine = '$engine'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($title) = mysql_fetch_array($qok);
	$title = addslashes($title);
	$query = "update product_list set descr='$title' where id='$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$msg = "Title $manpart/$engine => OurSku $oursku, Success!";
}

if($a == "descr")
{
	$query = "select descr from spidercache where type = 'descr' and manpart = '$manpart' and engine = '$engine'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($descr) = mysql_fetch_array($qok);
	$descr = addslashes($descr);

	$query = "select data from product_descr where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		$query = "update product_descr set data='$descr' where id='$oursku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}
	else
	{
		$query = "insert into product_descr(id, data) values('$oursku','$descr')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}

	$msg = "Description $manpart/$engine => OurSku $oursku, Success!";
}



if($a == "specs")
{
	$query = "select specs from spidercache where type = 'specs' and manpart = '$manpart' and engine = '$engine'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($specs) = mysql_fetch_array($qok);
	$specs = addslashes($specs);

	$query = "select data from product_html where id = '$oursku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		$query = "update product_html set data='$specs' where id='$oursku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}
	else
	{
		$query = "insert into product_html(id, data) values('$oursku','$specs')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	}

	$msg = "Tech Specs $manpart/$engine => OurSku $oursku, Success!";
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html>
<head>
   <title>Action Confirmation</title>
   <link href="style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="main">
    <div class="caption">Action: <?=$action;?></div>
    <p>&nbsp;&nbsp;<?=$msg;?></p>
    <div id="icon">&nbsp;</div>
</td>
</tr>
<a href="javascript:window.close();" ALT="Close this Window">Close Window</a> 
</body>
</html>