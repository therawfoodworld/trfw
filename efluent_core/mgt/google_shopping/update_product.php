<?php
/**
 * Example to update a product.
 *
 * Copyright 2011 Google, Inc
 *
 *   Licensed under the Apache License, Version 2.0 (the "License"); you may not
 *   use this file except in compliance with the License.  You may obtain a copy
 *   of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 *   License for the specific language governing permissions and limitations
 *   under the License.
 *
 * @version 1.3
 * @author dhermes@google.com
 * @copyright Google Inc, 2011
 * @package GShoppingContent
 */
$ROOT_DIR = $argv[1];
$price = $argv[2];
$pid = $argv[3];

if($ROOT_DIR == "")
{
	echo "usage: ack_orders.phtml ROOT_DIR REQUESTID\n\n";
	exit;
}

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/amazon/amazonclass.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

// import our library
require_once('classes/GShoppingContent.php');
require_once('classes/BuildMockProduct.php');

// Get the user credentials
$creds = Credentials::get();

// Create a client for our merchant and log in
$client = new GSC_Client($creds["merchantId"]);
$client->login($creds["email"], $creds["password"]);

// Now enter some product data
//$product = buildMockProduct("446", "US", "en");

echo date("Y-m-d H:i:s")."\n";
// Finally send the data to the API
$entry = $client->getProduct($pid, "US", "en");
echo 'Item: ' . $entry->getTitle() . " - $pid\n";
echo 'Old Price: ' . $entry->getPrice() . "\n";

// Update the price and send updated product
$entry->setPrice($price, "usd");
$updatedEntry = $client->updateProduct($entry);

echo 'Item: ' . $updatedEntry->getTitle() . "- $pid\n";
echo 'New Price: ' . $updatedEntry->getPrice() . "\n\n";

/**
 * Credentials - Enter your own values
 *
 * @author afshar@google.com
**/
class Credentials {
    public static function get() {
        return array(
            "merchantId" => "100472106",
            "email" => "victor@niceelectronics.com",
            "password" => "rolex1826",
        );
    }
}

?>
