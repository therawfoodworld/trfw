<?
class neweggXMLClass {

	function __construct() {
		
	}
	function getNewItemFeed($items) {
		$xml = $this->getNewItemHeader();	
			
		foreach ($items as $item) {
//			$xml .= $this->getNewItemCategory($item['catid']);
			$xml .= $this->getNewItemBody($item);	
		}
		$xml .= $this->getNewItemFooter();	
		return $xml;
	}
	private function getNewItemHeader() {
		$xml = '﻿<?xml version="1.0" encoding="utf-8"?>
					<NeweggEnvelope xsi:noNamespaceSchemaLocation="ExistingFeed.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					  <Header>
					    <DocumentVersion>2.0</DocumentVersion>
					    <!--The identifier for the very of current feed; assigned by Newegg-->
					  </Header>
					  <MessageType>BatchItemCreation</MessageType>
					  
					  <Message>
    					<Itemfeed>
					';
		return $xml;
	}
	private function getNewItemFooter() {
		$xml = '    </Itemfeed>
				  </Message>
				</NeweggEnvelope>
					';
		return $xml;
	}
	private function getNewItemCategory($catid) {
		$xml = '<SummaryInfo>
			        <SubCategoryID>'.$catid.'</SubCategoryID>
			        <!--Indentify the category specification. This example is based on category: Ski, category ID: 1704, under Sports Goods-->
			      </SummaryInfo>
					';
//			$xml = '<SummaryInfo/>';
		return $xml;
	}
	private function getNewItemBody($item) {
		if($item['shipweight'] == "0.00") {
			$shipweight = "1.00";
		}
		if ($item['refurb'] == '1') {
			$refurb = "Refurbished";
		}
		else {
			$refurb = "New";
		}
		
//		".$item['manpart']."
		$xml = "<Item>
					<Action>Create Item</Action>
					<!--Specify the action you would like to perform. -->
			        <BasicInfo>
			          <SellerPartNumber>".$item['pid']."</SellerPartNumber>
			          <!--A seller-defined unique identifier for an item. An alphanumeric string, max 40 characters including space-->
			          <Manufacturer>".$item['manname']."</Manufacturer>
			          <!--The manufacturer name for the item. This value must be entered into our database before the data feed is uploaded.  You can check the list of live manufacturers at http://www.newegg.com/ProductSort/BrandList.aspx?Depa=0&name=All-Brands. Send new manufacturer names to datafeeds@newegg.com.-->
			          <ManufacturerPartNumberOrISBN></ManufacturerPartNumberOrISBN>
			          <!--Manufacturer defined unique identifier for an item. -->
			          <UPC>".$item['upccode']."</UPC>
			          <WebsiteShortTitle><![CDATA[".$item['title']."]]></WebsiteShortTitle>
			          <!--The short, easily identifiable item name for this item. It will be used as the main name for your product on all store pages, Sales Orders and receipts, so should not contain variables. Maximum characters: 200. -->
			          <ProductDescription>This is a testing item with full description. Please use your own description</ProductDescription>
			          <!--The detailed description to be featured on the item page. This description should state the features and functions of the product. Should be the same for all items in product family. 4000 character limit. -->
			          <ItemDimension>
			            <ItemLength>2</ItemLength>
			            <!--Individual unit shipping length in inches-->
			            <ItemWidth>2</ItemWidth>
			            <!--Individual unit shipping width in inches-->
			            <ItemHeight>2</ItemHeight>
			            <!--Individual unit shipping height in inches-->
			          </ItemDimension>
			          <ItemWeight>".$item['shipweight']."</ItemWeight>
			          <ItemCondition>".$refurb."</ItemCondition>
			          <!--Individual unit shipping height in inches-->
			          <ShippingRestriction>No</ShippingRestriction>
			          <!--Available value to identify the package of product-->
			          <SellingPrice>".$item['price']."</SellingPrice>
			          <!--The selling price of item. -->
			          <MAP>5.00</MAP>
			          <!--The MAP of item. -->
			          <CheckoutMAP>False</CheckoutMAP>
			          <!--The CheckoutMAP must be True or False -->
			          <Shipping>Default</Shipping>
			          <!--The shipping charge setting for this item. -->
			          <Inventory>".$item['qty']."</Inventory>
			          <!--The inventory quantity available for sale for this item. -->
			          <ActivationMark>False</ActivationMark>
			          <!--The ActivationMark's default setting is True on all uploaded products because all products must be activated to be visible on Newegg websites. -->
			          ".$this->getImagesXML($item['imagelist'],$pid)."
			        </BasicInfo>
			        <SubCategoryProperty>
			          <!--Section containing category-specific information such as properties. Please refer to the attached XSDs to complete the ProductData section -->
			          <Skis>
			            <SkisBrand>YourBrand</SkisBrand>
			            <SkisModel>YourModel</SkisModel>
			            <SkisSize>88 cm</SkisSize>
			            <SkisType>Alpine</SkisType>
			            <SkisColor>Black</SkisColor>
			            <!--The color value for a item. This value must be entered into our database before the data feed is uploaded. Send new manufacturer names to datafeeds@newegg.com.-->
			            <SkisColorMapping>Black</SkisColorMapping>
			            <SkisAge>Adult</SkisAge>
			            <SkisGender>Male</SkisGender>
			          </Skis>
			        </SubCategoryProperty>
			      </Item>";
		$xml = "<Item>
					<BasicInfo> 
						<SellerPartNumber>".$item['pid']."</SellerPartNumber> 
						<!--A seller-defined unique identifier for an item. An alphanumeric string, max 40 characters including space-->
					 	<Manufacturer>".$item['manname']."</Manufacturer> 
						<!--The manufacturer name for the item. This value must be entered into our database before the data feed is uploaded. You can check the list of live manufacturers at http://www.newegg.com/ProductSort/BrandList.aspx?Depa=0&name=All-Brands. Send new manufacturer names to datafeeds@newegg.com.-->
					 	<ManufacturerPartNumberOrISBN>".$item['manpart']."</ManufacturerPartNumberOrISBN> 
						<!--Manufacturer defined unique identifier for an item. -->
					 	<UPC>".$item['upccode']."</UPC>
						<!--Enter the item's UPC code-->
					 	<Currency>USD</Currency> 
						<!--The currency of the item's selling price and instant rebate. Currently, only USD is available as option. If left blank, system will default currency to USD. -->
					 	<SellingPrice>".$item['price']."</SellingPrice> 
						<!--The selling price of item. -->
						 <Inventory>".$item['qty']."</Inventory> 
						<!--The inventory quantity available for sale for this item. -->
						<Shipping>Default</Shipping>
			            <!--The shipping charge setting for this item. -->
						 <ActivationMark>False</ActivationMark> 
						<!--The ActivationMark's default setting is True on all uploaded products because all products must be activated to be visible on Newegg websites. -->
					 </BasicInfo> 
				</Item>";
		
		
//		<MAP>5.00</MAP> 
//						<!--The MAP of item. -->
		return $xml;
	}
	private function getImagesXML($imageList,$pid) {
		$xml = "";
		if (count($imageList)>0) {
			$xml .="<ItemImages>";
			foreach ($imageList as $k => $img) {
				$xml .="<Image>
			              <ImageUrl>".$this->BASE_URL."/getimage.phtml?id=".$pid."&itype=".$img."&thumb=0</ImageUrl>
			              <!--Enter the URL for this item's product images.  All URLs must be a direct link to the actual image. -->";
				if ($k == 0) {
					$xml .="<IsPrimary>1</IsPrimary>
			              <!--Used to identify if the image is the default image. -->";
				}
	            $xml .="</Image>";
			}
			$xml .="</ItemImages>";
		}
	}
}
?>