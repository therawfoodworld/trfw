<?
class neweggClass {
	
	private $ftpURL = "ftp03.newegg.com";
	private $ftpInboundDir = "/Inbound/";
	private $ftpOutboundDir = "/Outbound/";
	private $localPath = "";
	
	function __construct() {
		
	}
	function setFtpParams($ftp_usr,$ftp_pass) {
		$this->ftpUsr = $ftp_usr;
		$this->ftpPw = $ftp_pass;
	}
	function executeQuery($query) {
		if(!$q2ok = mysql_db_query($this->DB, $query, $this->mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error";echo $query; exit; }  }
		return $q2ok;
	}
	function getNewProductFeedItems($logid,$filename) {
		$numout = 0;
		$maxout = 10000;
		$items = array();
		$reportid = date("Ymd-His", time());
		$requestid = $this->genRandomString().time();
		
		
		$campaigns = $this->getCampaignList();
		
		foreach ($campaigns as $campaign) {
			$campaignid = $campaign['campaignid'];
			$newegg_campaign_name = $campaign['newegg_campaign_name'];
			$newegg_listid = $campaign['newegg_listid'];
			$list_title = $campaign['list_title'];
			
			
			$tstamp = date("Y-m-d H:i:s", time());
			$query = "insert into newegg_base_requests(tstamp, campid) values('$tstamp', '$campaignid')";
			$q2ok = $this->executeQuery($query);
			
			$brid = mysql_insert_id();
					
			$newegg_campaign_name = stripslashes($newegg_campaign_name);
			echo "Campaign $newegg_campaign_name [LIST => $list_title]\n";
		
			// first list new items
			$query = "select product_list.id, product_manufacturers.manname, product_list.manpart, product_list.descr, product_list.sku, product_list.upccode, product_list.modelno, 
						product_list.use_asin, product_list.asin, product_list.lastupdate, product_list.shipweight,product_list.refurb
						from product_list, product_manufacturers, list_products
						where 
						product_list.manid = product_manufacturers.manid AND
						product_list.id = list_products.pid AND
						list_products.lid = '$newegg_listid' AND 
						product_list.id NOT IN (select pid from newegg_base_messages) AND
						product_list.id NOT IN (select pid from newegg_errors)";
			$q2ok = $this->executeQuery($query);
			while( (list($pid, $manname,$manpart, $descr, $sku, $upccode, $modelno, $use_asin, $asin, $lastupdate, $shipweight,$refurb) = mysql_fetch_array($q2ok))  && ($numout < $maxout) )
			{
				$query = "select mid from newegg_base_messages where pid = '$pid'"; // the query above supposedly filters them.... but they could have been added since
				$qok = $this->executeQuery($query);
				if(mysql_num_rows($qok) <= 0)
				{
					if($upccode == '')
					{
						$this->logError($pid,'SKU Excluded - NO UPC',$campaignid,$requestid);
					}
					else 
					{
						echo "Adding: $pid [$numout of $maxout]\n";
						$upc_check = $this->validate_UPCABarcode($upccode);
						
						if( (strlen($upccode) < 12) && ($use_asin != 1) )
						{
							$this->logError($pid,'SKU Excluded - NO UPC NO ASIN',$campaignid,$requestid);
						}
						elseif($upc_check === false) {
							$this->logError($pid,'UPC Invalid',$campaignid,$requestid);
						}
						else 
						{
							$query = "select pricelevel from newegg_campaigns where id = '$campaignid'";
							$qoka = $this->executeQuery($query);
							list($plevel) = mysql_fetch_array($qoka);
							
							$query = "select price_level".$plevel." from product_prices where id = '$pid'";
							$qoka = $this->executeQuery($query);
					//		list($prod_price) = mysql_fetch_array($q2ok);
								
							if (mysql_num_rows($qoka) == 0) {
								$prod_price = number_format(9999.99,2,'.','');
							}
							else {
								list($prod_price) = mysql_fetch_array($qoka);
							}
							$query = "select price_msrp from product_prices where id = '$pid'";
							$qoka = $this->executeQuery($query);
							list($price_msrp) = mysql_fetch_array($qoka);
							
							$imageList = $this->efc->getImageList($pid);
							$qty = $this->getNeweggQty($pid,$campaignid,$this->efc);
							$cat = $this->getNeweggAssignedCat($pid);
							$item = array(
									"pid"=>$pid,
									"manname"=>$manname,
									"manpart"=>$manpart,
									"title"=>$descr,
									"sku"=>$sku,
									"price"=>$prod_price,
									"price_msrp"=>$price_msrp,
									"upccode"=>$upccode,
									"modelno"=>$modelno,
									"refurb"=>$refurb,
									"imagelist"=>$imageList,
									"shipweight"=>$shipweight,
									"qty"=>$qty,
									"use_asin"=>$use_asin,
									"asin"=>$asin,
									"catid"=>$cat
								);
							array_push($items,$item);
							$numout++;
							
							$query_bm = "insert into newegg_base_messages(pid, tstamp, brid, campid, asin,pid_lastupdate,qty,price) values('$pid', '$tstamp', '$brid', '$campaignid', '$asin', '$lastupdate','$qty','$prod_price')";
							$qok_bm = $this->executeQuery($query_bm);
						}
					}
				}
			}	
		}
		
		
		if(count($items) <= 0)
		{
			echo "Empty Feed\n";
			
			$query = "delete from newegg_feed_report where id = '$feedreportid'";
			$qoka = $this->executeQuery($query);
			return array();
		}
		
		
		
		if($reportid != "" || $requestid != "")
		{
			$query = "update newegg_base_requests set newegg_submissionid = '$reportid', newegg_requestid = '$requestid' where brid = '$brid'";
			$q2ok = $this->executeQuery($query);
			
			$tstamp = date("Y-m-d H:i:s", time());
			
			$query = "insert into newegg_feed_report(type, tstamp, reportid, requestid, filename, complete) ";
			$query .= " values('NEW', '$tstamp', '$reportid', '$requestid', '$filename', '0')";
			$q2ok = $this->executeQuery($query);
			
			$fid = mysql_insert_id();	
			
			$query = "update newegg_log set requestid = '$requestid' where id = '$logid'";
			$q2ok = $this->executeQuery($query);
		}	
		
		
		return $items;
	}
	
	function getCampaignList() {
		$campaigns = array();
		$query = "select newegg_campaigns.id, newegg_campaigns.campaign_name, newegg_campaign_lists.listid, lists.list_title from newegg_campaigns, newegg_campaign_lists, lists where 
						newegg_campaigns.id = newegg_campaign_lists.campid AND newegg_campaign_lists.listid = lists.lid";
		$q9ok = $this->executeQuery($query);
		while(list($campaignid, $newegg_campaign_name, $newegg_listid, $list_title) = mysql_fetch_array($q9ok))
		{
			array_push($campaigns,array("campaignid"=>$campaignid,
										"newegg_campaign_name"=> $newegg_campaign_name, 
										"newegg_listid"=>$newegg_listid, 
										"list_title"=>$list_title));
		}
		return $campaigns;
	}
	
	function getFeedStatus() {
		$query_status = "select reportid,requestid,filename,type from newegg_feed_report where complete = 0"; // type != 'SHIPPING'
		$q2ok_status = $this->executeQuery($query_status);
		while(list($reportid,$requestid,$filename,$reporttype) = mysql_fetch_array($q2ok_status))
		{
			if ($reporttype == 'NEW') {
				$type = 'item';
				$this->parseCreateItemFeedResponse($reportid,$requestid,$filename,$reporttype);
			}
			if ($reporttype == 'ORDERLIST') {
				$type = 'orders';
			}
			
		}
	}
	
	function parseCreateItemFeedResponse($reportid,$requestid,$filename,$reporttype)
	{
		if ($reporttype == 'NEW')
		{
			$feedData = $this->getFeedFile($filename,'item');
		}	
			
		$tstamp = date("Y-m-d H:i:s", time());
		$query2 = "insert into newegg_log(title, tstamp, requestid) values('$reporttype', '$tstamp', '$requestid')";
		$q2ok = $this->executeQuery($query2);
		$logid = mysql_insert_id();
		
		$query3 = "SELECT @@max_allowed_packet";
		$qok3 = $this->executeQuery($query3);
		list($maxsize) = mysql_fetch_array($qok3);
			
		$filesize = filesize($this->ROOT_DIR."/mgt/newegg/feeds/".$filename);
		
		if($filesize > $maxsize)
		{
			$query4 = "insert into newegg_log_extended(lid, type, data) values('$logid', 'response', 'Filesize was too large')";
			$qok4 = $this->executeQuery($query4);
		}
		else 
		{
			/*$query4 = "insert into newegg_log_extended(lid, type, data) values('$logid', 'response', '".addslashes($data)."')";
			if(!$qok4 = mysql_db_query($DB, $query4, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; logDBError(mysql_error()); exit; }  }*/
		}

		$results = $feedData['NeweggEnvelope']['Message']['ProcessingReport'];
		if ($results['ProcessingSummary']['ProcessedCount']['value'] > 0)
		{
			$query5 = "update newegg_base_messages set deleteflag = 1";
			$qok5 = $this->executeQuery($query5);

			// some pids with error
			if ($results['ProcessingSummary']['WithErrorCount']['value'] > 0)
			{
				foreach ($results['Result'] AS $result_item_errors)
				{
					if (isset($result_item_errors['ErrorList']))
					{
						$pid = $result_item_errors['AdditionalInfo']['SellerPartNumber']['value'];
	
						$errors = $result_item_errors['ErrorList']['ErrorDescription'];
						$err_msg = trim($errors['1']['value']);
						
						if ($err_msg == 'Error:[Item does not exist. Please create a new list.]')
						{
							// update table with pid
							$query7 = "insert into newegg_feed_item_not_exist (requestid, pid) values ('$requestid', '$pid')";
							$qok7 = $this->executeQuery($query7);
						}
						else 
						{
							$query8 = "select brid,campid from newegg_base_requests where newegg_requestid = '$requestid'";
							$q2ok8 = $this->executeQuery($query8);
							list($brid,$campaignid) = mysql_fetch_array($q2ok8);
							$this->logError($pid,$err_msg,$campaignid,$requestid);
						}
					}
				}
			}
			
			// some pid where added
			if ($results['ProcessingSummary']['SuccessCount']['value'] > 0)
			{
				//$query6 = "update newegg_base_messages set deleteflag = '0' where pid = '$pid'";				
				//$qok6 = $this->executeQuery($query6);
			}
			
			$query9 = "delete from newegg_base_messages where deleteflag = 1";
			$qok9 = $this->executeQuery($query9);
		}
		else 
		{
			if (isset($results['Result']['ErrorList']))
			{
				$err_msg = $results['Result']['ErrorList']['ErrorDescription']['value'];

				$query4 = "insert into newegg_log_extended (lid, type, data) values('$logid', 'response', '".mysql_real_escape_string(trim($err_msg))."')";
				$qok4 = $this->executeQuery($query4);

				$query9 = "delete from newegg_base_messages";
				$qok9 = $this->executeQuery($query9);
			}
		}
		$query10 = "update newegg_feed_report set complete = '1' where reportid = '$reportid'";
		$qok10 = $this->executeQuery($query10);
	}
	
	function logError($pid,$msg,$campaignid,$requestid) {
//		SKU Excluded - NO UPC NO ASIN
		$tstamp = date("Y-m-d H:i:s", time());
		$query = "select id from newegg_errors where pid = '$pid' and message = '$msg'";
		$qok = $this->executeQuery($query);
		if(mysql_num_rows($qok) <= 0)
		{
			$tstamp = date("Y-m-d H:i:s", time());
			$query10 = "insert into newegg_errors(pid, tstamp, message, campid,requestid) values('$pid', '$tstamp', '".$msg."', '$campaignid','$requestid')";
			$qok10 = $this->executeQuery($query10);
		}
	}
	function uploadFile($local_file,$filename,$type) {
		
		if ($type == 'item') {
			$ftp_path = $this->ftpInboundDir."CreateItem/".$filename;
		}elseif ($type == 'inventory') {
			$ftp_path = $this->ftpInboundDir."Inventory/".$filename;
		}elseif ($type == 'shipping') {
			$ftp_path = $this->ftpInboundDir."Shipping/".$filename;
		}
		
		$conn_id = ftp_connect($this->ftpURL, 21) or die ("Cannot connect to host");
		 
		// send access parameters
		ftp_login($conn_id, $this->ftpUsr, $this->ftpPw) or die("Cannot login");
		
		$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_BINARY);
		if (!$upload) {
			echo "Error uploading file ".$filename;
		}
		ftp_close($conn_id);
	} 
	function getFeedFile($filename,$type) {
		$read_tmp = false;
		$count = 0;
		$message = '';
		$conn_id = ftp_connect($this->ftpURL, 21) or die ("Cannot connect to host");
		ftp_login($conn_id, $this->ftpUsr, $this->ftpPw) or die("Cannot login");
		
		if ($type == 'orders') {
			$ftp_path = $this->ftpOutboundDir."OrderList";
		}elseif ($type == 'item') {
			$ftp_path = $this->ftpOutboundDir."CreateItem";
		}elseif ($type == 'inventory') {
			$ftp_path = $this->ftpOutboundDir."Inventory";
		}elseif ($type == 'shipping') {
			$ftp_path = $this->ftpOutboundDir."Shipping";
		}
		
		$filename_tmp = substr($filename,0,-4);
		
		$files = ftp_nlist($conn_id,$ftp_path);
		if (count($files)>0) {
			foreach ($files as $k=>$v) {
				
				if (strpos($v,$filename_tmp)) {
					
					$res_file_arr = explode("/",$v);
					$res_file = $res_file_arr[3];
					
					if ($type == 'orders') {
						$target = fopen($this->ROOT_DIR."mgt/newegg/feeds/".$res_file, "w+");
						$local_file = $this->ROOT_DIR."mgt/newegg/feeds/".$res_file;
					}elseif ($type == 'item') {
						$target = fopen($this->ROOT_DIR."mgt/newegg/feeds/".$res_file, "w+");
						$local_file = $this->ROOT_DIR."mgt/newegg/feeds/".$res_file;
					}elseif ($type == 'inventory') {
						$target = fopen($this->ROOT_DIR."mgt/newegg/feeds/".$res_file, "w+");
						$local_file = $this->ROOT_DIR."mgt/newegg/feeds/".$res_file;
					}elseif ($type == 'shipping') {
						$target = fopen($this->ROOT_DIR."mgt/newegg/feeds/shipping/".$res_file, "w+");
						$local_file = $this->ROOT_DIR."mgt/newegg/feeds/".$res_file;
					}

					if (ftp_fget($conn_id, $target, $v, FTP_BINARY, 0))
					{
						echo "Successfully written to $target";
					} else {
						echo "There was a problem while downloading $v to $target";
					}
	
					fclose($target);
					$read_tmp = true;
				}
			}
		}
		ftp_close($conn_id);
		
		$fp = fopen($local_file, "r");
		if(!$fp){echo "NO FILE!<br />$local_file<br />";exit;}
		$data = "";
		while(!feof($fp))$data .= fgets($fp, 4096);
		fclose($fp);
		
		return $this->xml2array($data);
	}
	
	function validate_UPCABarcode($barcode)
	{
	  // check to see if barcode is 12 digits long
	  if(!preg_match("/^[0-9]{12}$/",$barcode)) {
	  	if (strlen($barcode) == 13 && substr($barcode,0,1) == '0') {
	  		return validate_UPCABarcode(substr($barcode,1));
	  	}
	  	else{
	  		return false;	
	  	}
	    
	  }
	  $digits = $barcode;
	  // 1. sum each of the odd numbered digits
	  $odd_sum = $digits[0] + $digits[2] + $digits[4] + $digits[6] + $digits[8] + $digits[10];  
	  // 2. multiply result by three
	  $odd_sum_three = $odd_sum * 3;
	  // 3. add the result to the sum of each of the even numbered digits
	  $even_sum = $digits[1] + $digits[3] + $digits[5] + $digits[7] + $digits[9];
	  $total_sum = $odd_sum_three + $even_sum;
	  // 4. subtract the result from the next highest power of 10
	  $next_ten = (ceil($total_sum/10))*10;
	  $check_digit = $next_ten - $total_sum;
	
	
	
	  // if the check digit and the last digit of the barcode are OK return true;
	  if($check_digit == $digits[11]) {
	     return true;
	  } 
	  return false;
	}	
	function xml2array($contents, $get_attributes=1) 
	{ 
	    if(!$contents) return array(); 
	
	    if(!function_exists('xml_parser_create')) { 
	        //print "'xml_parser_create()' function not found!"; 
	        return array(); 
	    } 
	    //Get the XML parser of PHP - PHP must have this module for the parser to work 
	    $parser = xml_parser_create(); 
	    xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 ); 
	    xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 ); 
	    xml_parse_into_struct( $parser, $contents, $xml_values ); 
	    xml_parser_free( $parser ); 
	
	    if(!$xml_values) return;//Hmm... 
	
	    //Initializations 
	    $xml_array = array(); 
	    $parents = array(); 
	    $opened_tags = array(); 
	    $arr = array(); 
	
	    $current = &$xml_array; 
	
	    //Go through the tags. 
	    foreach($xml_values as $data) { 
	        unset($attributes,$value);//Remove existing values, or there will be trouble 
	
	        //This command will extract these variables into the foreach scope 
	        // tag(string), type(string), level(int), attributes(array). 
	        extract($data);//We could use the array by itself, but this cooler. 
	
	        $result = ''; 
	        if($get_attributes) {//The second argument of the function decides this. 
	            $result = array(); 
	            if(isset($value)) $result['value'] = $value; 
	
	            //Set the attributes too. 
	            if(isset($attributes)) { 
	                foreach($attributes as $attr => $val) { 
	                    if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
	                    /**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */ 
	                } 
	            } 
	        } elseif(isset($value)) { 
	            $result = $value; 
	        } 
	
	        //See tag status and do the needed. 
	        if($type == "open") {//The starting of the tag '<tag>' 
	            $parent[$level-1] = &$current; 
	
	            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
	                $current[$tag] = $result; 
	                $current = &$current[$tag]; 
	
	            } else { //There was another element with the same tag name 
	                if(isset($current[$tag][0])) { 
	                    array_push($current[$tag], $result); 
	                } else { 
	                    $current[$tag] = array($current[$tag],$result); 
	                } 
	                $last = count($current[$tag]) - 1; 
	                $current = &$current[$tag][$last]; 
	            } 
	
	        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
	            //See if the key is already taken. 
	            if(!isset($current[$tag])) { //New Key 
	                $current[$tag] = $result; 
	
	            } else { //If taken, put all things inside a list(array) 
	                if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array... 
	                        or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) { 
	                    array_push($current[$tag],$result); // ...push the new element into that array. 
	                } else { //If it is not an array... 
	                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value 
	                } 
	            } 
	
	        } elseif($type == 'close') { //End of tag '</tag>' 
	            $current = &$parent[$level-1]; 
	        } 
	    } 
	
	    return($xml_array); 
	}
	function genRandomString() {
	    $length = 10;
	    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
	    $string = "";    
	
	    for ($p = 0; $p < $length; $p++) {
	        $string .= $characters[mt_rand(0, strlen($characters))];
	    }
	
	    return $string;
	}
	function getNeweggQty ($pid,$campid,$efc) {
		if ($campid == '') {
			return '0';
		}
		$query = "select 
			newegg_campaigns.quantity, 
			newegg_campaigns.qty_control,
			newegg_campaigns.min_qty, 
			product_list.kit, 
			product_list.am_qty
			from product_list, newegg_campaigns
			where 
			newegg_campaigns.id='$campid' AND 
			product_list.id = '".$pid."'";
		$q2ok = $this->executeQuery($query);
		list($usequantity, $qty_control, $min_qty, $kit,$am_qty) = mysql_fetch_array($q2ok);
		if(intval($kit) == 0)
		{
			if($usequantity == "-1")  // -1 => physical + dropship - onhold  - committed a.k.a. instock + dropship
			{
		       	$invinfo = $efc->getInventoryInfo2($pid);
		        $query = "select dropship from product_list where id = '$pid'";
		        $qok = $this->executeQuery($query);
		        list($dropship) = mysql_fetch_array($qok);
		        	
		       	if($dropship == 1) // then we count sourcing
		       	  	$sourcing_stock = $efc->getInventorySourcing($pid);
		       	else 
		       	 	$sourcing_stock = 0;
		        	
		       	$available = $invinfo['instock'] - $invinfo['onhold'] - $invinfo['committed'] + $sourcing_stock;
		       	if($qty_control > 0){$available = $available * ($qty_control / 100);}
		        	
		       	$quantity = $available;
			}
		    else if($usequantity == "-2") // -2 => physical
		    {
		    	$invinfo = $efc->getInventoryInfo2($pid);
		        $available = $invinfo['instock'];
		        if($qty_control > 0){$available = $available * ($qty_control / 100);}
		        	
		        $quantity = $available;
			}
		    else if($usequantity == "-3") // -3 => physical - onhold - committed a.k.a. instock
		    {
		    	$invinfo = $efc->getInventoryInfo2($pid);
		        $available = $invinfo['instock'] - $invinfo['onhold'] - $invinfo['committed'];
		        if($qty_control > 0){$available = $available * ($qty_control / 100);}
		        	
		        $quantity = $available;
			}
		    else if($usequantity == "-4")// -4 => physical + dropship 
		    {
		    	$invinfo = $efc->getInventoryInfo2($pid);
		        	
		        $query = "select dropship from product_list where id = '$pid'";
		        $qok = $this->executeQuery($query);
		        list($dropship) = mysql_fetch_array($qok);
		        	
		        if($dropship == 1) // then we count sourcing
		        	$sourcing_stock = $efc->getInventorySourcing($pid);
				else 
		        	$sourcing_stock = 0;
		        	
				$available = $invinfo['instock'] + $sourcing_stock;
		        if($qty_control > 0){$available = $available * ($qty_control / 100);}
		        	
		        $quantity = $available;
			}
			else if($usequantity == "-5")// -5 => amazon quantity
			{
				$query = "select am_qty, rtdsinv, amazon_realtime from product_list where id = '$pid'";
				$q4ok = $this->executeQuery($query);
				list($am_qty, $rtdsinv, $amazon_realtime) = mysql_fetch_array($q4ok);
				
				if(intval($amazon_realtime) == 1)
				{
					$invinfo = $efc->getInventoryInfo2($pid);
		        	$available = $invinfo['instock'] - $invinfo['onhold'] - $invinfo['committed'];
				}
				else if($rtdsinv == 1)
				{
					$query = "select sum(qty) from product_sourcing where pid = '$pid' and useinv = '1'";
					$q4ok = $this->executeQuery($query);
					list($dsinv) = mysql_fetch_array($q4ok);
					
					if($dsinv == "")
						$available = "0";
					else 
						$available = intval($dsinv);
				}
				else
				{
					$available = $am_qty;
				}			
				//////////////////////////////////////////////////////////////////////////			
			
				if($qty_control > 0){$quantity = $available * ($qty_control / 100);}
				else $quantity = $available;
			}
			else if($usequantity == "-6")// SKU Level Inventory (if available) or available + dropship
			{
				$query = "select am_qty, rtdsinv, amazon_realtime from product_list where id = '$pid'";
				$q4ok = $this->executeQuery($query);
				list($am_qty, $rtdsinv, $amazon_realtime) = mysql_fetch_array($q4ok);
				
				if(intval($amazon_realtime) == 1)
				{
					$invinfo = $efc->getInventoryInfo2($pid);
		        	$available = $invinfo['instock'] - $invinfo['onhold'] - $invinfo['committed'];
				}
				else if($rtdsinv == 1)
				{
					$query = "select sum(qty) from product_sourcing where pid = '$pid' and useinv = '1'";
					$q4ok = $this->executeQuery($query);
					list($dsinv) = mysql_fetch_array($q4ok);
					
					if($dsinv == "")
						$available = "0";
					else 
						$available = intval($dsinv);
				}
				else if(intval($am_qty) > 0) 
				{
					$available = $am_qty;
				}
				else if(intval($am_qty) == -1)
				{
					$available = -1;
				}
				else
				{
					$invinfo = $efc->getInventoryInfo2($pid);
			        $query = "select dropship from product_list where id = '$pid'";
			        $qok = $this->executeQuery($query);
			        list($dropship) = mysql_fetch_array($qok);
			        	
			       	if($dropship == 1) // then we count sourcing
			       	  	$sourcing_stock = $efc->getInventorySourcing($pid);
			       	else 
			       	 	$sourcing_stock = 0;
			        	
			       	$available = $invinfo['instock'] - $invinfo['onhold'] - $invinfo['committed'] + $sourcing_stock;
			       	
			       	//echo "AVAILABLE: $available SOURCING: $sourcing_stock\n";
			       	
			       	if($qty_control > 0){$available = $available * ($qty_control / 100);}
			        	
			       	$quantity = $available;
				}			
				//////////////////////////////////////////////////////////////////////////			
			
				if($qty_control > 0){$quantity = $available * ($qty_control / 100);}
				else $quantity = $available;
			}
		    else 
		    {
		    	$quantity = $quantity;
			}
		}
		else 
		{
			$quantity = $am_qty;
		}
		if(intval($min_qty) > 0)
		{
			echo "... min qty set to => $min_qty (current qty: $quantity\n";
			
			if(intval($quantity) > intval($min_qty))
			{
				$quantity = intval($quantity);
			}
			else 
			{
				$quantity = 0;
				
				echo "... zeroed out qty because it was too low\n";
			}
		}
		else 
		{
			if(intval($quantity) < 0)
				$quantity = 0;
			else 
				$quantity = intval($quantity);
		}
		
//		echo "PID=".$pid."--------".$quantity."\n";
		return $quantity;
	}
	function getNeweggAssignedCat($pid) {
		$query3 = "SELECT neweggcat from newegg_categories_pid where pid = '".$pid."'";
		$q4ok = $this->executeQuery($query3);
		if (mysql_num_rows($q4ok)>0) {
			list($buycat) = mysql_fetch_array($q4ok);	
		}
		else {
			$cattree = $this->efc->getCatTree($pid);
			foreach ($cattree['catid'] as $k=>$v) {
				$query3 = "SELECT neweggcat from newegg_categories where catid = '".$v."'";
				$q4ok = $this->executeQuery($query3);
				if (mysql_num_rows($q4ok)>0) {
					list($buycat) = mysql_fetch_array($q4ok);	
					break;
				}
			}
		}
		return $buycat;
	}
}
?>