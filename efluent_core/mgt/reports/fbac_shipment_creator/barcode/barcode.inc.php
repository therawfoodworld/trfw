<?php

	//########################//
	// 
	// Author :Harish Chauhan
	// Created : 7July 2005
	// 
	//########################//

	/*
	* This class is for generating barcodes in diffrenct encoding symbologies.
	* It supports EAN-13,EAN-8,UPC-A,UPC-E,ISBN ,2 of 5 Symbologies(std,ind,interleaved),postnet,
	* codabar,code128,code39,code93 symbologies.
	* 
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	* 
	* Requirements : PHP with GD library support. 
	* 
	* Reference : http://www.barcodeisland.com/symbolgy.phtml
	*/
	
	class BARCODE
	{
		var $_encode;
		var $_error;
		var $_width;
		var $_height;
		var $_scale;
		var $_color;
		var $_font;
		var $_bgcolor;
		var $_format;
		var $_n2w;
		var $_file_path;
		var $_font_path;
		var $_prod_title;
		var $_prod_cond;
		var $_from_addr;
		var $_to_addr;
		function BARCODE($encoding="EAN-13")
		{
			
			if(!function_exists("imagecreate"))
			{
				die("This class needs GD library support.");
				return false;
			}

			$this->_error="";
			$this->_scale=2;
			$this->_width=0;
			$this->_height=0;
			$this->_n2w=2;
			$this->_height=60;
			$this->_format='png';
			/*$this->_font=dirname($_SERVER["PATH_TRANSLATED"])."/"."arialbd.ttf";
			if (isset($_SERVER['WINDIR']) && file_exists($_SERVER['WINDIR']))
			  $this->_font=$_SERVER['WINDIR']."\Fonts\arialbd.ttf";*/
		    $this->_font = $this->_font_path.'consola.ttf';

			$this->setSymblogy($encoding);
			$this->setHexColor("#000000","#FFFFFF");
		}
		function setProd($title,$cond) {
			$this->_prod_title = $title;
			$this->_prod_cond = $cond;
		}
		function setFromAddr($addr) {
			$this->_from_addr = $addr;
		}
		function setToAddr($addr) {
			$this->_to_addr = $addr;
		}
		function setFont($font,$autolocate=false)
		{
			$this->_font=$font;
			if($autolocate)
			{  
				$this->_font=dirname($_SERVER["PATH_TRANSLATED"])."/".$font.".ttf";
			
				if (isset($_SERVER['WINDIR']) && file_exists($_SERVER['WINDIR']))
				   $this->_font=$_SERVER['WINDIR']."\Fonts\\".$font.".ttf";
			}
		}

		function setSymblogy($encoding="EAN-13")
		{
			$this->_encode=strtoupper($encoding);
		}
		
		function setHexColor($color,$bgcolor)
		{
			$this->setColor(hexdec(substr($color,1,2)),hexdec(substr($color,3,2)),hexdec(substr($color,5,2)));
			$this->setBGColor(hexdec(substr($bgcolor,1,2)),hexdec(substr($bgcolor,3,2)),hexdec(substr($bgcolor,5,2)));
		}

		function setColor($red,$green,$blue)
		{
			$this->_color=array($red,$green,$blue);
		}

		function setBGColor($red,$green,$blue)
		{
			$this->_bgcolor=array($red,$green,$blue);
		}
		
		function setScale($scale)
		{
			$this->_scale=$scale;
		}
		
		function setFormat($format)
		{
			$this->_format=strtolower($format);
		}

		function setHeight($height)
		{
			$this->_height=$height;
		}

		function setNarrow2Wide($n2w)
		{
			if($n2w<2)
				$n2w=3;
			$this->_n2w=$n2w;
		}
		
		function error($asimg=false)
		{
			if(empty($this->_error))
				return "";
			if(!$asimg)
				return $this->_error;
			

			@header("Content-type: image/png");
			$im=@imagecreate(250,100);
			$color = @imagecolorallocate($im,255,255,255);
			$color = @imagecolorallocate($im,0,0,0);
			@imagettftext($im,10,0,5,50,$color,$this->_font , wordwrap($this->_error, 40, "\n"));
			@imagepng($im);
			@imagedestroy($im);
		}

		function genBarCode($barnumber,$format="gif",$file="")
		{
			$this->setFormat($format);
			
			if($this->_encode=="CODE128" )
			{ 
				$this->_c128Barcode($barnumber,$this->_scale,$file);
			}
			if($this->_encode=="CODE128-2" )
			{ 
				$this->_c128Barcode2($barnumber,$this->_scale,$file);
			}
		}
		
		
		/// End functions for code39
		
		///Start function for code128
		function _c128Encode($barnumber,$useKeys)
		{
			$encTable=array("11011001100","11001101100","11001100110","10010011000","10010001100","10001001100","10011001000","10011000100","10001100100","11001001000","11001000100","11000100100","10110011100","10011011100","10011001110","10111001100","10011101100","10011100110","11001110010","11001011100","11001001110","11011100100","11001110100","11101101110","11101001100","11100101100","11100100110","11101100100","11100110100","11100110010","11011011000","11011000110","11000110110","10100011000","10001011000","10001000110","10110001000","10001101000","10001100010","11010001000","11000101000","11000100010","10110111000","10110001110","10001101110","10111011000","10111000110","10001110110","11101110110","11010001110","11000101110","11011101000","11011100010","11011101110","11101011000","11101000110","11100010110","11101101000","11101100010","11100011010","11101111010","11001000010","11110001010","10100110000","10100001100","10010110000","10010000110","10000101100","10000100110","10110010000","10110000100","10011010000","10011000010","10000110100","10000110010","11000010010","11001010000","11110111010","11000010100","10001111010","10100111100","10010111100","10010011110","10111100100","10011110100","10011110010","11110100100","11110010100","11110010010","11011011110","11011110110","11110110110","10101111000","10100011110","10001011110","10111101000","10111100010","11110101000","11110100010","10111011110","10111101110","11101011110","11110101110","11010000100","11010010000","11010011100","11000111010");

			$start=array("A"=>"11010000100","B"=>"11010010000","C"=>"11010011100");
			$stop="11000111010";

			$sum=0;
			$mfcStr="";
			if($useKeys=='C')
			{
				for($i=0;$i<strlen($barnumber);$i+=2)
				{ 
					$val=substr($barnumber,$i,2);
					if(is_int($val))
						$sum+=($i+1)*(int)($val);
					elseif($barnumber==chr(129))
						$sum+=($i+1)*100;
					elseif($barnumber==chr(130))
						$sum+=($i+1)*101;
					$mfcStr.=$encTable[$val];
				}
			}
			else
			{
				for($i=0;$i<strlen($barnumber);$i++)
				{ 
					$num=ord($barnumber[$i]);
					if($num>=32 && $num<=126)
						$num=ord($barnumber[$i])-32;
					elseif($num==128)
						$num=99;
					elseif($num==129)
						$num=100;
					elseif($num==130)
						$num=101;
					elseif($num<32 && $useKeys=='A')
						$num=$num+64;
					$sum+=($i+1)*$num;
					$mfcStr.=$encTable[$num];
				}
			}

			if($useKeys=='A')
				$check=($sum+103)%103;
			if($useKeys=='B')
				$check=($sum+104)%103;
			if($useKeys=='C')
				$check=($sum+105)%103;

			return $start[$useKeys].$mfcStr.$encTable[$check].$stop."11";
		}

		function _c128Barcode($barnumber,$scale=1,$file="")
		{
			$scale = 1;
			$useKeys="B";
			if(preg_match("/^[0-9".chr(128).chr(129).chr(130)."]+$/",$barnumber))
			{
				$useKeys='C';
				if(strlen($barnumber)%2 != 0)
					$barnumber='0'.$barnumber;
			}
			
			for($i=0;$i<32;$i++)
				$chr=chr($i);
			if(preg_match("/[".$chr."]+/",$barnumber))
				$useKeys='A';

			$bars=$this->_c128Encode($barnumber,$useKeys);
			if(empty($file))
				header("Content-type: image/".$this->_format);

			if ($scale<1) $scale=2;
			$total_y=(double)$scale * $this->_height+10*$scale;
			if (!$space)
			  $space=array('top'=>10*$scale,'bottom'=>10*$scale,'left'=>9*$scale,'right'=>9*$scale);
			
			/* count total width */
			$xpos=0;
			
			$xpos=$scale*strlen($bars)+2*$scale*10; 

			/* allocate the image */
			$total_x= $xpos +$space['left']+$space['right'];
			$xpos=$space['left']+$scale*10;
	
//		    $height=floor($total_y-($scale*20));
			$height = 30;
//		    $height2=floor($total_y-$space['bottom']);
		    $height2=42;
			
		    $total_y = $total_y+13;
		    $total_x = $total_x;
			
			$im=@imagecreatetruecolor($total_x, $total_y);
			$bg_color = @imagecolorallocate($im, $this->_bgcolor[0], $this->_bgcolor[1],$this->_bgcolor[2]);
			$black_color = @imagecolorallocate($im, 0, 0,0);
			
			
			
			@imagefilledrectangle($im,0,0,$total_x,$total_y,$bg_color); 
			
			$bar_color = @imagecolorallocate($im, $this->_color[0], $this->_color[1],$this->_color[2]);
	
			for($i=0;$i<strlen($bars);$i++)
			{
				$h=$height;
				
				$val=strtoupper($bars[$i]);

				if($val==1)
					@imagefilledrectangle($im,$xpos, $space['top'],$xpos+$scale-1, $h,$bar_color);
				$xpos+=$scale;
			}
			
			$font_arr=@imagettfbbox ( $scale*10, 0, $this->_font, $barnumber);
//			$x= floor($total_x-(int)$font_arr[0]-(int)$font_arr[2]+$scale*10)/2;	
			$x= 18;	

			@imagettftext($im,$scale*8,0,$x, $height2, $black_color,$this->_font , $barnumber);
			@imagettftext($im,$scale*8,0,$x, $height2+10, $black_color,$this->_font , $this->_prod_title);
			@imagettftext($im,$scale*8,0,$x, $height2+20, $black_color,$this->_font , $this->_prod_cond);

//			@imagerectangle($im, 0, 0, $total_x-1, $height2+30, $black_color);
			if($this->_format=="png")
			{
				if(!empty($file))
					@imagepng($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagepng($im);
			}

			if($this->_format=="gif")
			{
				if(!empty($file))
					@imagegif($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagegif($im);
			}

			if($this->_format=="jpg" || $this->_format=="jpeg" )
			{
				if(!empty($file))
					@imagejpeg($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagejpeg($im);
			}

			@imagedestroy($im);
		}
		
		function _c128Barcode2($barnumber,$scale=1,$file="")
		{
			$scale = 1;
			$useKeys="B";
			if(preg_match("/^[0-9".chr(128).chr(129).chr(130)."]+$/",$barnumber))
			{
				$useKeys='C';
				if(strlen($barnumber)%2 != 0)
					$barnumber='0'.$barnumber;
			}
			
			for($i=0;$i<32;$i++)
				$chr=chr($i);
			if(preg_match("/[".$chr."]+/",$barnumber))
				$useKeys='A';

			$bars=$this->_c128Encode($barnumber,$useKeys);
			if(empty($file))
				header("Content-type: image/".$this->_format);

			$total_y = 358;
			$total_x = 564;
			$space=array('top'=>10,'bottom'=>10,'left'=>15,'right'=>15);
			
			/* count total width */
			$xpos=0;
			
			$xpos=strlen($bars)+2*10; 

			/* allocate the image */
			$xpos=$space['left']+10;
	
//		    $height=floor($total_y-($scale*20));
			$height = 83;
//		    $height2=floor($total_y-$space['bottom']);
		    $height2=42;
			
		   /* $total_y = $total_y+13;
		    $total_x = $total_x;*/
			
			$im=@imagecreatetruecolor($total_x, $total_y);
			$bg_color = @imagecolorallocate($im, $this->_bgcolor[0], $this->_bgcolor[1],$this->_bgcolor[2]);
			$black_color = @imagecolorallocate($im, 0, 0,0);
			$white_color = @imagecolorallocate($im, 255, 255,255);
			
			
			
			@imagefilledrectangle($im,5,5,$total_x-5,$total_y-5,$bg_color); 
			@imagefilledrectangle($im,20,66,544,70,$black_color); 
			
			/*-------ADDRESS------*/
			@imagettftext($im,11,0,25, 86, $black_color,$this->_font , "SHIP FROM:");
			@imagettftext($im,10,0,25, 100, $black_color,$this->_font , $this->_from_addr['name']);
			@imagettftext($im,10,0,25, 116, $black_color,$this->_font , $this->_from_addr['address']);
			@imagettftext($im,10,0,25, 130, $black_color,$this->_font , $this->_from_addr['city'].", ".$this->_from_addr['state']." ".$this->_from_addr['zip']);
			@imagettftext($im,10,0,25, 144, $black_color,$this->_font , $this->_from_addr['country']);
			
						
			@imagettftext($im,12,0,290, 86, $black_color,$this->_font_path."consolab.ttf" , "SHIP TO:");
			@imagettftext($im,12,0,290, 102, $black_color,$this->_font_path."consolab.ttf" , $this->_to_addr['name']);
			@imagettftext($im,12,0,290, 118, $black_color,$this->_font_path."consolab.ttf" , $this->_to_addr['address']);
			@imagettftext($im,12,0,290, 134, $black_color,$this->_font_path."consolab.ttf" , $this->_to_addr['city'].", ".$this->_to_addr['state']." ".$this->_to_addr['zip']);
			@imagettftext($im,12,0,290, 150, $black_color,$this->_font_path."consolab.ttf" , $this->_to_addr['country']);
			
			
			@imagefilledrectangle($im,20,153,544,167,$black_color); 
			@imagettftext($im,9,0,22, 164, $white_color,$this->_font_path."consolab.ttf" , "Shipping Label");
			
			@imagefilledrectangle($im,20,300,544,314,$black_color); 
			@imagettftext($im,9,0,22, 311, $white_color,$this->_font_path."consolab.ttf" , "Internal Use Only");
			
			@imagettftext($im,14,0,414, 340, $black_color,$this->_font_path."consolab.ttf" , "Assorted SKUs");
			
			@imagettftext($im,11,0,156, 29, $black_color,$this->_font , "PLEASE LEAVE THIS LABEL UNCOVERED");
			@imagettftext($im,28,0,25, 61, $black_color,$this->_font_path."consolab.ttf" , "FBA");
			@imagettftext($im,12,0,165, 184, $black_color,$this->_font , "Purchase Order/ Shipment ID");
			
			
			
			$bar_length = strlen($bars);
			$bar_start = ($total_x-$bar_length)/2;
			
			$bar_color = @imagecolorallocate($im, $this->_color[0], $this->_color[1],$this->_color[2]);
			for($i=0;$i<strlen($bars);$i++)
			{
				$h=$height;
				
				$val=strtoupper($bars[$i]);

				if($val==1) {
//					@imagefilledrectangle($im,$xpos, $space['top'],$xpos+$scale-1, $h,$bar_color);
					@imagefilledrectangle($im,$bar_start, 195,$bar_start, 278,$bar_color);
				}
				$bar_start+=$scale;
			}
			$font_arr=@imagettfbbox ( 12, 0, $this->_font, $barnumber);
			$x= floor($total_x-(int)$font_arr[0]-(int)$font_arr[2])/2;
			@imagettftext($im,12,0,$x, 294, $black_color,$this->_font , $barnumber);
			
//			$font_arr=@imagettfbbox ( $scale*10, 0, $this->_font, $barnumber);
//			$x= floor($total_x-(int)$font_arr[0]-(int)$font_arr[2]+$scale*10)/2;	
//			$x= 18;	

			/*@imagettftext($im,$scale*8,0,$x, $height2, $black_color,$this->_font , $barnumber);
			@imagettftext($im,$scale*8,0,$x, $height2+10, $black_color,$this->_font , $this->_prod_title);
			@imagettftext($im,$scale*8,0,$x, $height2+20, $black_color,$this->_font , $this->_prod_cond);*/

			if($this->_format=="png")
			{
				if(!empty($file))
					@imagepng($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagepng($im);
			}

			if($this->_format=="gif")
			{
				if(!empty($file))
					@imagegif($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagegif($im);
			}

			if($this->_format=="jpg" || $this->_format=="jpeg" )
			{
				if(!empty($file))
					@imagejpeg($im,$this->_file_path.$file.".".$this->_format);
				else
					@imagejpeg($im);
			}

			@imagedestroy($im);
		}
		///End function for codabar
	}
?>