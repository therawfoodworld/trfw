<?
REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
REQUIRE($ROOT_DIR."/mgt/auth.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************************************************/


?>
<HTML>
<HEAD>
<TITLE>Inventory Valuation</TITLE>
<meta http-equiv="Content-Style-Type" content="text/css">
<LINK HREF="/mgt/style.css" TYPE="text/css" REL="stylesheet">
<SCRIPT LANGUAGE="JavaScript" SRC="/mgt/js/calendar.js"></SCRIPT>
<script src="/mgt/e2win/sortable.js" type="text/javascript"></script>
<script src="/mgt/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="/mgt/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript">
//var cal = new CalendarPopup();
var cal1x = new CalendarPopup("testdiv1");
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>
<STYLE>
.dialog {
display:none;
width:125px;
height:65px;
position:absolute;
z-index:100;
background:white;
padding:2px;
font:9pt tahoma;
border:1px solid gray
}

.cover {
display:none;
position:absolute;
left:0px;
top:0px;
width:100%;
height:100%;
z-index:99;
background:#eeeeee;
filter:alpha(Opacity=50);
opacity:0.7;
-moz-opacity:0.7;
-khtml-opacity:0.7
}

</STYLE>
<script>
var g_level = 0;

function GetXmlHttpObject()
{
	var xmlHttp=null;

	try
    {
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
    catch (e)
    {
    	// Internet Explorer
        try
        {
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
        {
        	try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
            catch (e)
            {
            	alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	
	return xmlHttp;
}

function stateChanged() 
{ 
	if(xmlHttp.readyState==4)
	{ 
		//document.getElementById("txtHint").innerHTML=
		var lines=xmlHttp.responseText.split("\n"); 
		
		
		
		if(lines.length <= 2)
		{
			return;
		}
			
		document.getElementById("cat"+g_level+"d").style.display = '';
		
		for(xx = g_level+1; xx < 6; xx++)
		{
			document.getElementById("cat"+xx+"d").style.display = 'none';
			
			var theSelect = document.getElementById("cat"+g_level);
			for (i = theSelect.length - 1; i>=0; i--) 
			{
				theSelect.remove(i);
		    }		
		}
		
		var theSelect = document.getElementById("cat"+g_level);
		var i;
		
			
		for (i = theSelect.length - 1; i>=0; i--) 
		{
			theSelect.remove(i);
	    }		
		
		for(xx = 0; xx < lines.length; xx++)
		{
			var data = lines[xx].split(",");
			
			if(data[0] != "")appendOption(data);
		}
		
		getBrands();
	}
}

var g_cat = 0;

function getCategories(level)
{
	g_level = level;
	
	xmlHttp=GetXmlHttpObject();
	
	if (xmlHttp==null)
 	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 
  	
  	var thislevel = level - 1;
  	var varname = "cat"+thislevel;

  	if(thislevel == 0) 	cat = document.ivaluation.cat0.options[ document.ivaluation.cat0.selectedIndex ].value;
  	if(thislevel == 1) 	cat = document.ivaluation.cat1.options[ document.ivaluation.cat1.selectedIndex ].value;
  	if(thislevel == 2) 	cat = document.ivaluation.cat2.options[ document.ivaluation.cat2.selectedIndex ].value;
  	if(thislevel == 3) 	cat = document.ivaluation.cat3.options[ document.ivaluation.cat3.selectedIndex ].value;
  	if(thislevel == 4) 	cat = document.ivaluation.cat4.options[ document.ivaluation.cat4.selectedIndex ].value;
  	
  	if(parseInt(thislevel) > 0 && parseInt(cat) == 0)
  	{
  		getBrands();
  		return;
  	}
  	
  	g_cat = cat;
  	
	var url="/mgt/em/ajax_getcats.phtml?catid="+cat+"&sid="+Math.random();
	
	
	
	xmlHttp.onreadystatechange=stateChanged;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}

function appendOption(data)
{
	var theSelect = document.getElementById("cat"+g_level);
	
	nOption = document.createElement('option'); 
	isData = document.createTextNode(data[1]); 
	nOption.setAttribute('value',data[0]); 
	nOption.appendChild(isData); 
	
	//isList = document.advform.cat1;
	theSelect.appendChild(nOption); 
}

function getBrands()
{
	xmlCHttp=GetXmlHttpObject();
	var url="/mgt/sm/ajax_getbrands.phtml?catid="+g_cat+"&sid="+Math.random();
	
	//window.open( url );
	
	xmlCHttp.onreadystatechange=gotBrands;
	xmlCHttp.open("GET",url,true);
	xmlCHttp.send(null);	
	
}

function gotBrands()
{
	if(xmlCHttp.readyState==4)
	{
		//alert(xmlCHttp.responseText);
		var theSelect = document.getElementById("brand");
			
		for (i = theSelect.length - 1; i>=0; i--) 
		{
			theSelect.remove(i);
	    }		
	    
	    var lines=xmlCHttp.responseText.split("\n"); 
		
	    for(xx = 0; xx < lines.length; xx++)
		{
			var data = lines[xx].split("\t");
			
			if(data[0] != "")
			{
				nOption = document.createElement('option'); 
				isData = document.createTextNode(data[1]); 
				nOption.setAttribute('value',data[0]); 
				nOption.appendChild(isData); 
				
				//isList = document.advform.cat1;
				theSelect.appendChild(nOption);	
			}
		}
	    
	}
}

function pullReport()
{
	var cat0 = "", cat1 = "", cat2 = "", cat3 = "", cat4 = "", assignedto = "", fbclass = "";
	var cvr = document.getElementById("savecover");
	cvr.style.display = "block";
	
	cvr.style.top = 0;
	cvr.style.left = 0;
	
	cvr.style.width = "100%";
	cvr.style.height = "100%";

	if(document.ivaluation.cat0.selectedIndex > 0)
		cat0 = document.ivaluation.cat0.options[ document.ivaluation.cat0.selectedIndex ].value;
	
	if(document.ivaluation.cat1.selectedIndex > 0)
		cat1 = document.ivaluation.cat1.options[ document.ivaluation.cat1.selectedIndex ].value;
	
	if(document.ivaluation.cat2.selectedIndex > 0)		
		cat2 = document.ivaluation.cat2.options[ document.ivaluation.cat2.selectedIndex ].value;
	
	if(document.ivaluation.cat3.selectedIndex > 0)		
		cat3 = document.ivaluation.cat3.options[ document.ivaluation.cat3.selectedIndex ].value;
	
	if(document.ivaluation.cat4.selectedIndex > 0)		
		cat4 = document.ivaluation.cat4.options[ document.ivaluation.cat4.selectedIndex ].value;
		
	brand = document.ivaluation.brand.options[ document.ivaluation.brand.selectedIndex ].value;
	warehouse = document.ivaluation.warehouse.options[ document.ivaluation.warehouse.selectedIndex ].value;
	vendorid = document.ivaluation.vendorid.options[ document.ivaluation.vendorid.selectedIndex ].value;
	
	keywords = escape(document.ivaluation.keywords.value);
	
	assignedto = document.ivaluation.assignedto.options[ document.ivaluation.assignedto.selectedIndex ].value;
	fbclass = document.ivaluation.fbclass.options[ document.ivaluation.fbclass.selectedIndex ].value;
		
	var url="/mgt/reports/ajax_ivaluation.phtml";
 	url += "?cat0="+cat0;
 	url += "&cat1="+cat1;
 	url += "&cat2="+cat2;
 	url += "&cat3="+cat3;
 	url += "&cat4="+cat4;
 	url += "&brand="+brand;
 	url += "&warehouse="+warehouse;
 	url += "&vendorid="+vendorid;
 	url += "&keywords="+keywords;
 	url += "&fbclass="+fbclass;
 	url += "&assignedto="+assignedto;
	<?
	$query = "select divid, sitename from site_list order by sitename";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	while(list($divid, $sitename) = mysql_fetch_array($qok))
	{
	?>
		if(document.ivaluation.displayon_<?=$divid;?>.checked)
		url += "&displayon[]=<?=$divid;?>";
	<?
	}
	?>
	xmlHttp=GetXmlHttpObject();
	
	if (xmlHttp==null)
 	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 	
  	
 	xmlHttp.onreadystatechange=gotReport;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);

}

function gotReport()
{
	if(xmlHttp.readyState==4)
	{ 

		document.getElementById("reportdata").innerHTML = xmlHttp.responseText;
	
		var cvr = document.getElementById("savecover");
		cvr.style.display = "none";

	}	
}

var isOpera = navigator.userAgent.indexOf("Opera") > -1; 
var isIE = navigator.userAgent.indexOf("MSIE") > 1 && !isOpera; 
var isMoz = navigator.userAgent.indexOf("Mozilla/5.") == 0 && !isOpera;

function cancelReturn(oTextbox, oEvent) 
{ 
	var keyCode;
	
	if (isIE) 
   	{
   		keyCode = oEvent.keyCode;	
   	}
   	else if (isMoz) 
   	{
		keyCode = oEvent.charCode;
   	}
   	
	
	switch (oEvent.keyCode) 
	{ 
 		case 13: //enter  
 		{
 			pullReport();
 			return false; 
 		}
	}  
} 

function skuDetail(detailrow, rowid, pid)
{
	if( document.getElementById(detailrow).style.display=='none' )
	{
  		document.getElementById(detailrow).style.display = '';
  		document.getElementById("image"+rowid).src = "/mgt/images/minus.gif";
  		
  		loadSKUDetail(rowid, pid);
 	}
 	else
 	{
  		document.getElementById(detailrow).style.display = 'none';
  		document.getElementById("image"+rowid).src = "/mgt/images/plus.gif";
 	}

}

var g_rowid = 0;

function loadSKUDetail(rowid, pid)
{
	var url="/mgt/reports/ajax_skudetail.phtml";
 	url += "?pid="+pid;
 	url += "&rowid="+rowid;
	
	xml4Http=GetXmlHttpObject();
	
	if (xml4Http==null)
 	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 	
  	
  	g_rowid = rowid;
  	
 	xml4Http.onreadystatechange=gotSKUDetail;
	xml4Http.open("GET",url,true);
	xml4Http.send(null);	
}

function gotSKUDetail()
{
	if(xml4Http.readyState==4)
	{ 
		document.getElementById("detail"+g_rowid).innerHTML = xml4Http.responseText;
	}	
}

function clearReport()
{
	document.getElementById("cat1d").style.display = 'none';
	document.getElementById("cat2d").style.display = 'none';
	document.getElementById("cat3d").style.display = 'none';
	document.getElementById("cat4d").style.display = 'none';
	document.ivaluation.brand.selectedIndex = 0;
	document.ivaluation.warehouse.selectedIndex = 0;
	document.ivaluation.vendorid.selectedIndex = 0;
	document.ivaluation.keywords.value = "";
	document.ivaluation.vendorid.selectedIndex = 0;
	
	document.getElementById("reportdata").innerHTML = "";
	
}
</script>
<?REQUIRE($ROOT_DIR."/mgt/e2win/e2win_javascript.phtml");?>
</HEAD>

<body>
<div id="savecover" class="cover"></div>
<table width=100% height=100% cellpadding=0 cellspacing=0 border=0>
<tr>
	<td width=13 background="/mgt/images/l.jpg"> </td>
	<td valign=top>
		<?REQUIRE($ROOT_DIR."/mgt/include/menu.phtml");?>
		<div style="padding-left:5px;padding-top:10px">

<!----------------------------------------------------------------------------------------------------------------------------------------------------------->
		<table width=99%>
		<tr>
			<td width=250>
			<!------------------------------------------------------------------------------------------>
				<div id="TabbedPanels1" class="TabbedPanels" style="width:270px">
		    	<ul class="TabbedPanelsTabGroup">
		        <li class="TabbedPanelsTab" tabindex="0"><strong>Advanced</strong></li>
				</ul>
				<div class="TabbedPanelsContentGroup">
					<div class="TabbedPanelsContent">  
						<?INCLUDE("iv_form.phtml");?>
					</div>
				</div>
				</div>
				<script type="text/javascript">
				<!--
				var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
				//-->
				</script>			
			</td>			
			<!------------------------------------------------------------------------------------------>
			
			</td>		
			
			<td>
			<!------------------------------------------------------------------------------------------>
			
			<div id="reportdata">&nbsp;</div>
				
			<!------------------------------------------------------------------------------------------>
			</td>
		</tr>
		</table>

		<!--- PID (:)), SKU, current stock, amount to order, last purchase price, last vendor --->
		

		
<!----------------------------------------------------------------------------------------------------------------------------------------------------------->
	<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>
	</td>
	<td width=13 background="/mgt/images/r.jpg"> </td>
</tr>
</table>
<?REQUIRE($ROOT_DIR."/mgt/e2win/e2win_tables.phtml");?>
</body>
</html>