<html>
<head>
<title>FBA Calculator</title>
<style>
body, html {
	font-family:verdana, helvetica, arial !important;
	font-size:12px !important;
}
input.disabled {
	border:none;
	background:#fff;
}
table#fees input {
	width:60px;
}

table {
	font-size:11px !important;
}

span#asin_date_add {
	color:#009900;
}
</style>
<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var $ = jQuery;

	function submitASIN() {
		var asin = $('#asin').val();
		if (asin == '') {
	  		alert ("You forgot to write ASIN number.");
	  		return;
		}
		$('#loading').append('<img src="images/loading.gif">');
		getItem();
	}

	function getItem() {
		var asin = $('#asin').val();
		$.post('product_matches.php',{ 'method' : 'GET', 'model' : '{"searchString":"'+ asin +'","lang":"en_US","marketPlace":"ATVPDKIKX0DER"}' }, function(data) {
			var obj_item = $.parseJSON($.trim(data));
			if (obj_item.error == null)
			{
				$('input#link').val(obj_item.data[0].link);
				$('#image').html('');
				$('#title').html('');
				$('td#link').html('');
				$('span#dim_units').html('');
				$('td#fees_new').html('');
				$('span#weight_units').html('');
				$('#title').append(obj_item.data[0].title);
				$('#image').append('<img src="'+ obj_item.data[0].image +'" />');
				$('td#link').append('<a href="'+ obj_item.data[0].link +'" target="_blank">See Product Details</a>');
				$('#thumbnail').val(obj_item.data[0].thumbnail);
				$('input#length').val(obj_item.data[0].dimensions.length);
				$('input#width').val(obj_item.data[0].dimensions.width);
				$('input#height').val(obj_item.data[0].dimensions.height);
				$('span#dim_units').append(obj_item.data[0].dimUnits);
				$('input#shipping_weight').val(obj_item.data[0].weight);
				$('span#weight_units').append(obj_item.data[0].weightUnits);
				$('input#gl').val(obj_item.data[0].gl);
				$('input#product_group').val(obj_item.data[0].productGroup);
				$('input#sub_category').val(obj_item.data[0].subCategory);
				$('input#white_gloves_required').val(obj_item.data[0].whiteGlovesRequired);
				
				getFeeMFN();
			}
			else
			{
				alert('Amazon error: ' + obj_item.error);
				return;
			}
	  	});
	}
	
	function getFeeMFN()
	{
		var item_title = $('td#title').text();
		var item_asin = $('input#asin').val();
		var item_link = $('input#link').val();
		var item_image = $('#image img').attr('src');
		var item_thumbnail = $('input#thumbnail').val();
		var item_length = $('input#length').val();
		var item_width = $('input#width').val();
		var item_height = $('input#height').val();
		var item_dim_units = $('span#dim_units').text();
		var item_shipping_weight = $('input#shipping_weight').val();
		var item_weight_units = $('span#weight_units').text();
		var item_gl = $('input#gl').val();
		var item_product_group = $('input#product_group').val();
		var item_sub_category = $('input#sub_category').val();
		var item_white_gloves_required = $('input#white_gloves_required').val();
			
		$.post('mfn_fees.php',{ 'method' : 'GET', 'model' : '{"selected":true,"link":"'+ item_link +'","dimUnits":"'+ item_dim_units +'","thumbnail":"'+ item_thumbnail +'","subCategory":"'+ item_sub_category +'","dimensions":{"width":'+ item_width +',"length":'+ item_length +',"height":'+ item_height +'},"gl":"'+ item_gl +'","image":"'+ item_image +'","weightUnits":"'+ item_weight_units +'","productGroup":"'+ item_product_group +'","weight":'+ item_shipping_weight +',"asin":"'+ item_asin +'","whiteGlovesRequired":"'+ item_white_gloves_required +'","title":"'+ item_title +'","language":"en_US","price":0,"shipping":0,"revenueTotal":0,"order-handling":0,"pick-pack":0,"outbound-delivery":0,"storage":0,"inbound-delivery":0,"customer-service":0,"prep-service":0,"fulfillmentTotal":0}' }, function(data) {
			var obj_fee = $.parseJSON($.trim(data));
			if (obj_fee.error == null)
			{
				$('input#commission_fee').val(obj_fee.data.commissionFee);
				$('input#variable_closing_fee').val(obj_fee.data.variableClosingFee);
				
				getFeeAFN();
			}
			else
			{
				alert('Amazon error: ' + obj_item.error);
				return;
			}
		});
	}
	
	function getFeeAFN()
	{
		var item_title = $('td#title').text();
		var item_asin = $('input#asin').val();
		var item_link = $('input#link').val();
		var item_image = $('#image img').attr('src');
		var item_thumbnail = $('input#thumbnail').val();
		var item_length = $('input#length').val();
		var item_width = $('input#width').val();
		var item_height = $('input#height').val();
		var item_dim_units = $('span#dim_units').text();
		var item_shipping_weight = $('input#shipping_weight').val();
		var item_weight_units = $('span#weight_units').text();
		var item_gl = $('input#gl').val();
		var item_product_group = $('input#product_group').val();
		var item_sub_category = $('input#sub_category').val();
		var item_white_gloves_required = $('input#white_gloves_required').val();
		var item_price = $('input#afn-fees-price').val();	
		var item_inbound_delivery = $('input#afn-fees-inbound-delivery').val();
		var item_prep_service = $('input#afn-fees-prep-service').val();
		var item_fulfillment_total = parseFloat(item_inbound_delivery) + parseFloat(item_prep_service);
		var item_fulfillment_total = parseFloat(item_fulfillment_total).toFixed(2);
		
		$.post('afn_fees.php',{ 'method' : 'GET', 'model' : '{"selected":true,"link":"'+ item_link +'","dimUnits":"'+ item_dim_units +'","thumbnail":"'+ item_thumbnail +'","subCategory":"'+ item_sub_category +'","dimensions":{"width":'+ item_width +',"length":'+ item_length +',"height":'+ item_height +'},"gl":"'+ item_gl +'","image":"'+ item_image +'","weightUnits":"'+ item_weight_units +'","productGroup":"'+ item_product_group +'","weight":'+ item_shipping_weight +',"asin":"'+ item_asin +'","whiteGlovesRequired":"'+ item_white_gloves_required +'","title":"'+ item_title +'","language":"en_US","price":'+ item_price +',"revenueTotal":'+ item_price +',"inbound-delivery":'+ item_inbound_delivery +',"prep-service":'+ item_prep_service +',"fulfillmentTotal":'+ item_fulfillment_total +'}' }, function(data) {
			var obj_fee = $.parseJSON($.trim(data));
			if (obj_fee.error == null)
			{
				$('input#weight_handling_fee').val(obj_fee.data.weightHandlingFee);
				$('input#order_handling_fee').val(obj_fee.data.orderHandlingFee);
				$('input#fba_delivery_services_fee').val(obj_fee.data.fbaDeliveryServicesFee);
				$('input#commission_fee').val(obj_fee.data.commissionFee);
				$('input#pick_and_pack_fee').val(obj_fee.data.pickAndPackFee);
				$('input#storage_fee').val(obj_fee.data.storageFee);
				$('input#variable_closing_fee').val(obj_fee.data.variableClosingFee);
				$('input#revenue_subtotal').val($('input#afn-fees-price').val());
	
				item_fulfillment_total = parseFloat(item_fulfillment_total) + parseFloat(obj_fee.data.orderHandlingFee) + parseFloat(obj_fee.data.pickAndPackFee) + parseFloat(obj_fee.data.weightHandlingFee) + parseFloat(obj_fee.data.storageFee);
				var cost_total = parseFloat($('input#commission_fee').val()) + parseFloat($('input#variable_closing_fee').val());
				var item_cost_total = parseFloat(item_fulfillment_total) + parseFloat(cost_total);
				var item_margin_impact = parseFloat($('input#afn-fees-price').val()) - (parseFloat(item_fulfillment_total) + parseFloat(cost_total));

				var cost_total = parseFloat(cost_total).toFixed(2);
				var item_cost_total = parseFloat(item_cost_total).toFixed(2);
				var item_margin_impact = parseFloat(item_margin_impact).toFixed(2);
				
				$('input#fulfillment_cost_subtotal').val(item_fulfillment_total);
				$('input#cost_subtotal').val('- ' + item_cost_total);
				$('input#margin_impact').val(item_margin_impact);

				// new afn fee
				$.post('usfee.php',{ 'nfo' : '{"length":"'+ item_length +'","width":"'+ item_width +'","height":"'+ item_height +'","weight":"'+ item_shipping_weight +'","category":"'+ item_gl +'","sub_category":"'+ item_sub_category +'","is_white_gloves_required":"'+ item_white_gloves_required +'"}' }, function(data) {
					var obj_fee = $.parseJSON($.trim(data));
					$('input#fee_new_pick_and_pack').val(obj_fee.pick_and_pack);
					$('input#fee_new_weight_handling').val(obj_fee.weight_handling);
					$('input#fee_new_order_handling').val(obj_fee.order_handling);
					$('input#fee_new_category_percent').val(obj_fee.category_percent);
					
					saveItem();
				});
			}
			else
			{
				alert('Amazon error: ' + obj_item.error);
				return;
			}
		});
	}
	
	function saveItem() {
		var item_asin = $('input#asin').val();
		if (item_asin == '') {
	  		alert ("You forgot to write ASIN number.");
	  		return;
		}
		var item_length = $('input#length').val();
		var item_width = $('input#width').val();
		var item_height = $('input#height').val();
		var item_dim_units = $('span#dim_units').text();
		var item_shipping_weight = $('input#shipping_weight').val();
		var item_weight_units = $('span#weight_units').text();
		var item_gl = $('input#gl').val();
		var item_white_gloves_required = $('input#white_gloves_required').val();
		var item_price = $('input#afn-fees-price').val();	
		var item_prep_service = $('input#afn-fees-prep-service').val();

		var item_weight_handling = $('input#weight_handling_fee').val();
		var item_order_handling = $('input#order_handling_fee').val();
		var item_fba_delivery_services = $('input#fba_delivery_services_fee').val();
		var item_commission = $('input#commission_fee').val();
		var item_pick_and_pack = $('input#pick_and_pack_fee').val();
		var item_storage = $('input#storage_fee').val();
		var item_variable_closing = $('input#variable_closing_fee').val();
		
		var item_new_pick_and_pack = $('input#fee_new_pick_and_pack').val();
		var item_new_weight_handling = $('input#fee_new_weight_handling').val();	
		var item_new_order_handling = $('input#fee_new_order_handling').val();	
		var item_category_percent = $('input#fee_new_category_percent').val();
		
		$.post('ajax_save_fee.php',{ 'nfo' : '{"asin":"'+ item_asin +'","length":"'+ item_length +'","width":"'+ item_width +'","height":"'+ item_height +'","dim_units":"'+ item_dim_units +'","weight":"'+ item_shipping_weight +'","weight_units":"'+ item_weight_units +'","category":"'+ item_gl +'","is_white_gloves_required":"'+ item_white_gloves_required +'","price":"'+ item_price +'","prep_service":"'+ item_prep_service +'","weight_handling":"'+ item_weight_handling +'","order_handling":"'+ item_order_handling +'","fba_delivery_services":"'+ item_fba_delivery_services +'","commission":"'+ item_commission +'","pick_and_pack":"'+ item_pick_and_pack +'","storage":"'+ item_storage +'","variable_closing":"'+ item_variable_closing +'","new_pick_and_pack":"'+ item_new_pick_and_pack +'","new_weight_handling":"'+ item_new_weight_handling +'","new_order_hanling":"'+ item_new_order_handling +'","category_percent":"'+ item_category_percent +'"}' }, function(data) {
			$('#loading').html('');
		});
	}
</script>	
</head>
<body>

<input type="hidden" name="AFNBuyerSubtotal" id="AFNBuyerSubtotal" value="">

<table width="1200">
<tr>
	<td valign="top" width="200" style="border-right:1px solid #ccc;">
		<table width="100%">
		<tr>
			<td colspan="2" align="center"><b>Item inputs</b> <div id="loading" style="display:inline;"></div></td>
		</tr>
		<tr>
			<td>ASIN</td>
			<td><input type="text" name="asin" style="width:100px" id="asin" value=""></td>
		</tr>
		<tr>
			<td>Item price</td>
			<td><input type="text" name="afn-fees-price" style="width:100px" id="afn-fees-price" value="0"></td>
		</tr>
		<tr>
			<td>Inbound Shipping</td>
			<td><input type="text" name="afn-fees-inbound-delivery" style="width:100px" id="afn-fees-inbound-delivery" value="0"></td>
		</tr>
		<tr>
			<td>Prep Service</td>
			<td><input type="text" name="afn-fees-prep-service" style="width:100px" id="afn-fees-prep-service" value="0"></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="link" id="link" value="">
				<input type="hidden" name="thumbnail" id="thumbnail" value="">
				<input type="hidden" name="commission_fee" id="commission_fee" value="">
				<input type="hidden" name="variable_closing_fee" id="variable_closing_fee" value="">
			</td>
		</tr>
		</table>
	</td>
	<td valign="top" width="250" style="border-right:1px solid #ccc;">
		<table width="100%">
		<tr>
			<td colspan="2" align="center"><b>Item details</b></td>
		</tr>
		<tr>
			<td id="title" align="center" colspan="2"></td>
		</tr>
		<tr>
			<td id="image" align="center" colspan="2"></td>
		</tr>
		<tr>
			<td>Length:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="length" id="length" value="" style="text-align:center;width:50px">&nbsp;<span id="dim_units"></span></td>
		</tr>
		<tr>
			<td>Width:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="width" id="width" value="" style="text-align:center;width:50px"></td>
		</tr>
		<tr>
			<td>Height:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="height" id="height" value="" style="text-align:center;width:50px"></td>
		</tr>
		<tr>
			<td>Shipping Weight:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="shipping_weight" id="shipping_weight" value="" style="text-align:center;width:50px">&nbsp;<span id="weight_units"></span></td>
		</tr>
		<tr>
			<td id="link" align="left" colspan="2"></td>
		</tr>
		<tr>
			<td>Category:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="gl" id="gl" value="" style="overflow:visible;"></td>
		</tr>
		<tr>
			<td>Subcategory:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="sub_category" id="sub_category" value="" style="width:70px"></td>
		</tr>
		<tr>
			<td>Group:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="product_group" id="product_group" value="" style="width:70px"></td>
		</tr>
		<tr>
			<td>White gloves required:</td>
			<td><input type="text" disabled="disabled" class="disabled" name="white_gloves_required" id="white_gloves_required" value="" style="width:50px"></td>
		</tr>

		</table>
	</td>
	<td valign="top" width="200" style="border-right:1px solid #ccc;">
		<table width="100%">
		<tr>
			<td colspan="2" align="center"><b>AFN Fees</b></td>
		</tr>
		<tr>
			<td>Weight Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="weight_handling_fee" style="width:50px" id="weight_handling_fee" value=""></td>
		</tr>
		<tr>
			<td>Order Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="order_handling_fee" style="width:50px" id="order_handling_fee" value=""></td>
		</tr>
		<tr>
			<td>FBA Delivery Services:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="fba_delivery_services_fee" style="width:50px" id="fba_delivery_services_fee" value=""></td>
		</tr>
		<tr>
			<td>Commission:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="commission_fee" style="width:50px" id="commission_fee" value=""></td>
		</tr>
		<tr>
			<td>Pick &amp; Pack</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="pick_and_pack_fee" style="width:50px" id="pick_and_pack_fee" value=""></td>
		</tr>
		<tr>
			<td>Storage:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="storage_fee" style="width:50px" id="storage_fee" value=""></td>
		</tr>
		<tr>
			<td>Variable closing:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="variable_closing_fee" style="width:50px" id="variable_closing_fee" value=""></td>
		</tr>
		</table>
	</td>	
	<td valign="top" width="250" style="border-right:1px solid #ccc;">
		<table width="100%" id="fees">
		<tr>
			<td colspan="2" align="center"><b>Item fees</b></td>
		</tr>
		<!--
		<tr>
			<td>Revenue Subtotal:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="revenue_subtotal" width="50" id="revenue_subtotal" value=""></td>
		</tr>
		<tr>
			<td>Amazon Commission:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="commission_fee" width="50" id="commission_fee" value=""></td>
		</tr>
		<tr>
			<td>Variable Closing Fee:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="variable_closing_fee" width="50" id="variable_closing_fee" value=""></td>
		</tr>
		-->
		<tr>
			<td>Pick &amp; Pack:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="pick_and_pack_fee" width="50" id="pick_and_pack_fee" value=""></td>
		</tr>
		<tr>
			<td>Weight Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="weight_handling_fee" width="50" id="weight_handling_fee" value=""></td>
		</tr>
		<tr>
			<td>Order Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="order_handling_fee" width="50" id="order_handling_fee" value=""></td>
		</tr>
		<!--
		<tr>
			<td>Outbound Shipping:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="outbound_shipping" width="50" id="outbound_shipping" value="N/A"></td>
		</tr>
		<tr>
			<td>30 Day Storage:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="storage_fee" width="50" id="storage_fee" value=""></td>
		</tr>
		<tr>
			<td><b>Fulfillment Cost Subtotal:</b></td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="fulfillment_cost_subtotal" width="50" id="fulfillment_cost_subtotal" value=""></td>
		</tr>
		<tr>
			<td><b>Cost Subtotal:</b></td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="cost_subtotal" width="50" id="cost_subtotal" value=""></td>
		</tr>
		<tr>
			<td><b>Margin Impact:</b></td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="margin_impact" width="50" id="margin_impact" value=""></td>
		</tr>
		-->
		</table>
	</td>
	<td valign="top" width="250" style="border-right:1px solid #ccc;">
		<table width="100%" id="fees">
		<tr>
			<td colspan="2" align="center"><b>Item fees (New)</b></td>
		</tr>
		<tr>
			<td>Pick and Pack:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="fee_new_pick_and_pack" width="50" id="fee_new_pick_and_pack" value=""></td>
		</tr>
		<tr>
			<td>Weight Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="fee_new_weight_handling" width="50" id="fee_new_weight_handling" value=""></td>
		</tr>
		<tr>
			<td>Order Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="fee_new_order_handling" width="50" id="fee_new_order_handling" value=""></td>
		</tr>
		<tr>
			<td>Category percent:</td>
			<td>%<input type="text" disabled="disabled" class="disabled" name="fee_new_category_percent" width="50" id="fee_new_category_percent" value=""></td>
		</tr>
		</table>
	</td>
</tr>
</tr>
	<td colspan="4"><input type="button" name="submit_asin" id="submit_asin" onclick="submitASIN();" value="Get item info"></td>
<tr>
</table>
</body>
</html>

