<html>
<head>
<title>FBA Calculator</title>
<style>
body, html {
	font-family:verdana, helvetica, arial !important;
	font-size:12px !important;
}
input.disabled {
	border:none;
	background:#fff;
}
table#fees input {
	width:60px;
}

table {
	font-size:11px !important;
}

span#asin_date_add {
	color:#009900;
}
</style>
<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var $ = jQuery;
	function getItem() {
		var asin = $('#asin').val();
		if (asin == '') {
	  		alert ("You forgot to write ASIN number.");
	  		return;
		}
		$('#submit_asin').attr('disabled','disabled');
		$('#loading').append('<img src="images/loading.gif">');
		$.post('product_matches.php',{ 'method' : 'GET', 'model' : '{"searchString":"'+ asin +'","lang":"en_US","marketPlace":"ATVPDKIKX0DER"}' }, function(data) {
			var obj_item = $.parseJSON($.trim(data));
			if (obj_item.error == null) {
				getFeeMFN(asin,data);
			} else {
				alert('Amazon error: ' + obj_item.error);
				return;
			}
	  	});
	}
	
	function getFeeMFN(asin,itemData) {
		var obj_item = $.parseJSON($.trim(itemData));
		$.post('mfn_fees.php',{ 'method' : 'GET', 'model' : '{"selected":true,"link":"'+ obj_item.data[0].link +'","dimUnits":"'+ obj_item.data[0].dimUnits +'","thumbnail":"'+ obj_item.data[0].thumbnail +'","subCategory":"'+ obj_item.data[0].subCategory +'","dimensions":{"width":'+ obj_item.data[0].dimensions.width +',"length":'+ obj_item.data[0].dimensions.length +',"height":'+ obj_item.data[0].dimensions.height +'},"gl":"'+ obj_item.data[0].gl +'","image":"'+ obj_item.data[0].image +'","weightUnits":"'+ obj_item.data[0].weightUnits +'","productGroup":"'+ obj_item.data[0].productGroup +'","weight":'+ obj_item.data[0].weight +',"asin":"'+ asin +'","whiteGlovesRequired":"'+ obj_item.data[0].whiteGlovesRequired +'","title":"'+ obj_item.data[0].title +'","language":"en_US","price":0,"shipping":0,"revenueTotal":0,"order-handling":0,"pick-pack":0,"outbound-delivery":0,"storage":0,"inbound-delivery":0,"customer-service":0,"prep-service":0,"fulfillmentTotal":0}' }, function(data) {
			var obj_mfn = $.parseJSON($.trim(data));
			if (obj_mfn.error == null) {
				getFeeAFN(asin,itemData,data);
			} else {
				alert('Amazon error: ' + obj_mfn.error);
				return;
			}
		});
	}
	
	function getFeeAFN(asin,itemData,mfnData) {
		var obj_item = $.parseJSON($.trim(itemData));
		var item_price = $('input#price').val();	
		var item_inbound_delivery = $('input#inbound-delivery').val();
		var item_prep_service = $('input#prep-service').val();
		var item_fulfillment_total = parseFloat(item_inbound_delivery) + parseFloat(item_prep_service);
		var item_fulfillment_total = parseFloat(item_fulfillment_total).toFixed(2);
		$.post('afn_fees.php',{ 'method' : 'GET', 'model' : '{"selected":true,"link":"'+ obj_item.data[0].link +'","dimUnits":"'+ obj_item.data[0].dimUnits +'","thumbnail":"'+ obj_item.data[0].thumbnail +'","subCategory":"'+ obj_item.data[0].subCategory +'","dimensions":{"width":'+ obj_item.data[0].dimensions.width +',"length":'+ obj_item.data[0].dimensions.length +',"height":'+ obj_item.data[0].dimensions.height +'},"gl":"'+ obj_item.data[0].gl +'","image":"'+ obj_item.data[0].image +'","weightUnits":"'+ obj_item.data[0].weightUnits +'","productGroup":"'+ obj_item.data[0].productGroup +'","weight":'+ obj_item.data[0].weight +',"asin":"'+ asin +'","whiteGlovesRequired":"'+ obj_item.data[0].whiteGlovesRequired +'","title":"'+ obj_item.data[0].title +'","language":"en_US","price":'+ item_price +',"revenueTotal":'+ item_price +',"inbound-delivery":'+ item_inbound_delivery +',"prep-service":'+ item_prep_service +',"fulfillmentTotal":'+ item_fulfillment_total +'}' }, function(data) {
			var obj_afn = $.parseJSON($.trim(data));
			if (obj_afn.error == null) {
				var obj_mfn = $.parseJSON($.trim(mfnData));
				var item_fulfillment_total = parseFloat(item_fulfillment_total) + parseFloat(obj_afn.data.orderHandlingFee) + parseFloat(obj_afn.data.pickAndPackFee) + parseFloat(obj_afn.data.weightHandlingFee) + parseFloat(obj_afn.data.storageFee);
				var cost_total = parseFloat(obj_mfn.data.commissionFee) + parseFloat(obj_mfn.data.variableClosingFee);
				var item_cost_total = parseFloat(item_fulfillment_total) + parseFloat(cost_total);
				var item_margin_impact = parseFloat(item_price) - (parseFloat(item_fulfillment_total) + parseFloat(cost_total));
				var cost_total = parseFloat(cost_total).toFixed(2);
				var item_cost_total = parseFloat(item_cost_total).toFixed(2);
				var item_margin_impact = parseFloat(item_margin_impact).toFixed(2);
				// new afn fee
				$.post('usfee.php',{ 'nfo' : '{"length":"'+ obj_item.data[0].dimensions.length +'","width":"'+ obj_item.data[0].dimensions.width +'","height":"'+ obj_item.data[0].dimensions.height +'","weight":"'+ obj_item.data[0].weight +'","category":"'+ obj_item.data[0].gl +'","sub_category":"'+ obj_item.data[0].subCategory +'","is_white_gloves_required":"'+ obj_item.data[0].whiteGlovesRequired +'"}' }, function(data_new) {
					var obj_mfn = $.parseJSON($.trim(mfnData));
					var obj_afn_new = $.parseJSON($.trim(data_new));
					$('input#commission').val(obj_afn.data.commissionFee);
					$('input#variable_closing').val(obj_mfn.data.variableClosingFee);
					$('input#pick_and_pack').val(obj_afn_new.pick_and_pack);
					$('input#weight_handling').val(obj_afn_new.weight_handling);
					$('input#order_handling').val(obj_afn_new.order_handling);
					$('input#category_percent').val(obj_afn_new.category_percent);
					saveItem(asin,itemData,mfnData,data,data_new);
				});
			} else {
				alert('Amazon error: ' + obj_afn.error);
				return;
			}
		});
	}
	
	function saveItem(asin,itemData,mfnData,afnData,afnDataNew) {
		var obj_item = $.parseJSON($.trim(itemData));
		var obj_afn = $.parseJSON($.trim(afnData));
		var obj_afn_new = $.parseJSON($.trim(afnDataNew));
		var item_price = $('input#price').val();	
		var item_prep_service = $('input#prep-service').val();
		$.post('ajax_save_fee.php',{ 'nfo' : '{"asin":"'+ asin +'","length":"'+ obj_item.data[0].dimensions.length +'","width":"'+ obj_item.data[0].dimensions.width +'","height":"'+ obj_item.data[0].dimensions.height +'","dim_units":"'+ obj_item.data[0].dimUnits +'","weight":"'+ obj_item.data[0].weight +'","weight_units":"'+ obj_item.data[0].weightUnits +'","category":"'+ obj_item.data[0].gl +'","is_white_gloves_required":"'+ obj_item.data[0].whiteGlovesRequired +'","price":"'+ item_price +'","prep_service":"'+ item_prep_service +'","weight_handling":"'+ obj_afn.data.weightHandlingFee +'","order_handling":"'+ obj_afn.data.orderHandlingFee +'","fba_delivery_services":"'+ obj_afn.data.fbaDeliveryServicesFee +'","commission":"'+ obj_afn.data.commissionFee +'","pick_and_pack":"'+ obj_afn.data.pickAndPackFee +'","storage":"'+ obj_afn.data.storageFee +'","variable_closing":"'+ obj_afn.data.variableClosingFee +'","new_pick_and_pack":"'+ obj_afn_new.pick_and_pack +'","new_weight_handling":"'+ obj_afn_new.weight_handling +'","new_order_hanling":"'+ obj_afn_new.order_handling +'","category_percent":"'+ obj_afn_new.category_percent +'"}' }, function(data) {
			$('#loading').html('');
			$('#submit_asin').removeAttr('disabled');
		});
	}
</script>	
</head>
<body>
<table width="550">
<tr>
	<td valign="top" width="250" style="border-right:1px solid #ccc;">
		<table width="100%">
		<tr>
			<td colspan="2" align="center"><b>Item inputs</b> <div id="loading" style="display:inline;"></div></td>
		</tr>
		<tr>
			<td>ASIN</td>
			<td><input type="text" name="asin" style="width:100px" id="asin" value=""></td>
		</tr>
		<tr>
			<td>Item price</td>
			<td><input type="text" name="price" style="width:100px" id="price" value="0"></td>
		</tr>
		<tr>
			<td>Inbound Shipping</td>
			<td><input type="text" name="inbound-delivery" style="width:100px" id="inbound-delivery" value="0"></td>
		</tr>
		<tr>
			<td>Prep Service</td>
			<td><input type="text" name="prep-service" style="width:100px" id="prep-service" value="0"></td>
		</tr>
		</table>
	</td>
	<td valign="top" width="300">
		<table width="100%">
		<tr>
			<td colspan="2" align="center"><b>Item fees (New)</b></td>
		</tr>
		<tr>
			<td>Commission:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="commission" width="50" id="commission" value=""></td>
		</tr>
		<tr>
			<td>Variable Closing:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="variable_closing" width="50" id="variable_closing" value=""></td>
		</tr>
		<tr>
			<td>Pick and Pack:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="pick_and_pack" width="50" id="pick_and_pack" value=""></td>
		</tr>
		<tr>
			<td>Weight Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="weight_handling" width="50" id="weight_handling" value=""></td>
		</tr>
		<tr>
			<td>Order Handling:</td>
			<td>$<input type="text" disabled="disabled" class="disabled" name="order_handling" width="50" id="order_handling" value=""></td>
		</tr>
		<tr>
			<td>Category percent:</td>
			<td>%<input type="text" disabled="disabled" class="disabled" name="category_percent" width="50" id="category_percent" value=""></td>
		</tr>
		</table>
	</td>
</tr>
</tr>
	<td colspan="4"><input type="button" name="submit_asin" id="submit_asin" onclick="getItem();" value="Get item info"></td>
<tr>
</table>
</body>
</html>

