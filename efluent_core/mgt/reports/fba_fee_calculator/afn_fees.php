<?php
error_reporting(E_ALL);
header("Content-Type: text/html");

//set POST variables
if (isset($_POST['model']) && !empty($_POST['model']))
{
	$model = $_POST['model'];
//	$model_decoded = json_decode($model, true);
//	unset($model_decoded['undefined']);
//	$model = json_encode($model_decoded);
}
else
{
	die('{"error":null,"data":null}');
}	

$url = 'https://sellercentral.amazon.com/gp/fba/revenue-calculator/data/afn-fees.html';

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_REFERER, 'http://sellercentral.amazon.com/gp/fba/revenue-calculator/index.html/ref=xx_xx_cont_xx?ie=UTF8&lang=en_US');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, 'method=GET&model='.$model);

//execute post
$result = curl_exec($ch);

echo $result;

if(curl_exec($ch) === false)
{
	// echo 'Curl error: ' . curl_error($ch);
	die('{"error":null,"data":null}');
}
else
{
	//echo 'Operation completed without any errors.';
	$result_decoded = json_decode($result, true);
	if (is_null($result_decoded['error']) && is_null($result_decoded['data']))
	{	
		die('{"error":null,"data":null}');
	}
	elseif(is_array($result_decoded['data']))
	{
		/*
		foreach ($result_decoded['data']['0'] AS $k => $v)
		{
			[weightHandlingFee] => 0.74
			[orderHandlingFee] => 1
			[fbaDeliveryServicesFee] => 0
			[commissionFee] => 0
			[pickAndPackFee] => 0.75
			[storageFee] => 0.02
			[variableClosingFee] => 0
		}
		*/
	}
	else 
	{
		// echo 'Amazon error: '.$result_decoded['error'];
		die('{"error":null,"data":null}');
	}
}


//close connection
curl_close($ch);