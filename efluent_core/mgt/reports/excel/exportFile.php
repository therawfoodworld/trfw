<?
require_once "class.writeexcel_workbook.inc.php";
require_once "class.writeexcel_worksheet.inc.php";
REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
class excelExport {
	var $workbook;
	var $worksheet;
	var $format;
	var $dbx;
	function excelExport($name,$MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD,$DB) {
		$this->workbook = new writeexcel_workbook($name);
		$this->dbx = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
		mysql_select_db($DB, $this->dbx);
	}
	/**
	 * Enter description here...
	 *
	 */
	function newWorksheetShipped () {
		$this->worksheet =& $this->workbook->addworksheet();
		$this->worksheet->set_column (0,0,10);
		$this->worksheet->set_column (1,1,20); 
		$this->worksheet->set_column (2,2,15);
		$this->worksheet->set_column (3,3,15);
		$this->worksheet->set_column (4,4,10);
		$this->worksheet->set_column (5,5,10);
		$this->worksheet->set_column (6,6,10);
		$this->worksheet->set_column (7,7,10);
	}
	/**
	 * Enter description here...
	 *
	 */
	function newWorksheetShipper () {
		$this->worksheet =& $this->workbook->addworksheet();
		$this->worksheet->set_column (0,0,5);
		$this->worksheet->set_column (1,1,20); 
		$this->worksheet->set_column (2,2,15);
		$this->worksheet->set_column (3,3,100);
		$this->worksheet->set_column (4,4,5);
		$this->worksheet->set_column (5,5,10);
		$this->worksheet->set_column (6,6,10);
		$this->worksheet->set_column (7,7,10);
	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $style
	 */
	function cellFormat($style) {
		$this->format = $this->workbook->addformat($style);
	}
	/**
	 * Enter description here...
	 *
	 */
	function worksheetHeader () {

	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $language
	 */
	function worksheetTableHeaderShipped () {
		$this->cellFormat(array('Size' => 10,
								'Align' => "center",
                                'Bold' => 1));
		$this->worksheet->write(0, 0, "Type", $this->format);
		$this->worksheet->write(0, 1, "Order", $this->format);
		$this->worksheet->write(0, 2, "Order date", $this->format);
		$this->worksheet->write(0, 3, "Ship date", $this->format);
		$this->worksheet->write(0, 4, "Total", $this->format);
		$this->worksheet->write(0, 5, "Cost", $this->format);
		$this->worksheet->write(0, 6, "Profit", $this->format);
		$this->worksheet->write(0, 7, "Margin", $this->format);
	}
	/**
	 * Enter description here...
	 *
	 */
	function worksheetTableHeaderShipper () {
		$this->cellFormat(array('Size' => 10,
								'Align' => "center",
                                'Bold' => 1));
		$this->worksheet->write(0, 0, "PID", $this->format);
		$this->worksheet->write(0, 1, "SKU", $this->format);
		$this->worksheet->write(0, 2, "Man", $this->format);
		$this->worksheet->write(0, 3, "Descr", $this->format);
		$this->worksheet->write(0, 4, "Qty.", $this->format);
		$this->worksheet->write(0, 5, "Total", $this->format);
		$this->worksheet->write(0, 6, "Cost", $this->format);
		$this->worksheet->write(0, 7, "Profit", $this->format);
		$this->worksheet->write(0, 8, "%", $this->format);
	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $status
	 * @param unknown_type $bdate
	 * @param unknown_type $edate
	 * @param unknown_type $repid
	 * @param unknown_type $house
	 * @param unknown_type $shipping
	 */
	function getShippedLines($status,$bdate,$edate,$repid,$house,$shipping) {
		$xbdate = strtotime("$bdate 00:00:00");
		$xedate = strtotime("$edate 23:59:59");
		
		/*
			url += "&repid="+repid;
			url += "&house="+house;
			url += "&status="+status;	
		*/
		if(intval($repid) > 0)
			$addon .= " and customer_orders.arepid = '$repid' ";
		
		if(intval($house) == 1)
		{
			// include house
		}
		else if(intval($house) == 2)
			$addon .= " and customer_orders.houseorder = '0' "; // exclude
	
		$completed = 0;
		if(intval($status) > 0)
		{
			$query = mysql_query("select complete from company_orderflow where step = '$status'",$this->dbx);
			$co = mysql_fetch_assoc($query);
			$completed = $co['complete'];
		}
		
		if($completed == 1)
		{
			$query = "select customer_orders.otype,customer_orders.shipping, customer_orders.invoiceid, customer_orders.orderdate, customer_order_lines.shipdate,
					customer_order_lines.price, customer_order_lines.cost, customer_order_lines.qty_shipped, customer_order_lines.prodid,
					customer_order_lines.manname, customer_order_lines.manpart, customer_order_lines.descr,  customer_order_lines.ex_return
					from
					customer_orders, customer_order_lines
					where
					customer_orders.otype = 'INVOICE' AND
					customer_orders.status = '$status' AND
					customer_orders.orderid = customer_order_lines.orderid AND
					customer_orders.completedate >= '$xbdate' and
					customer_orders.completedate <= '$xedate' 
					AND customer_orders.clicktype = '1' 
					AND customer_orders.clickid = 'AM' 
					$addon 
		
					UNION 
					
					select customer_orders.otype,customer_orders.shipping, customer_orders.invoiceid, customer_orders.orderdate, customer_order_lines.shipdate,
					customer_order_lines.price, customer_order_lines.cost, customer_order_lines.qty_shipped, customer_order_lines.prodid,
					customer_order_lines.manname, customer_order_lines.manpart, customer_order_lines.descr,  customer_order_lines.ex_return
					from
					customer_orders, customer_order_lines
					where
					customer_orders.otype = 'RETURN' AND
					customer_orders.status = '5' AND
					customer_orders.orderid = customer_order_lines.orderid AND
					customer_orders.completedate >= '$xbdate' and
					customer_orders.completedate <= '$xedate' 
					AND customer_orders.clicktype = '1' 
					AND customer_orders.clickid = 'AM' 
					$addon 			
					
					UNION
					
					select customer_orders.otype,customer_orders.shipping, customer_orders.invoiceid, customer_orders.orderdate, customer_order_lines.shipdate,
					customer_order_lines.price, customer_order_lines.cost, customer_order_lines.qty_shipped, customer_order_lines.prodid,
					customer_order_lines.manname, customer_order_lines.manpart, customer_order_lines.descr,  customer_order_lines.ex_return
					from
					customer_orders, customer_order_lines
					where
					customer_orders.otype = 'EXCHANGE' AND
					customer_orders.status = '50' AND
					customer_orders.orderid = customer_order_lines.orderid AND
					customer_orders.completedate >= '$xbdate' and
					customer_orders.completedate <= '$xedate' 
					AND customer_orders.clicktype = '1' 
					AND customer_orders.clickid = 'AM' 
					$addon 
					
					UNION
					
					select customer_orders.otype,customer_orders.shipping, customer_orders.invoiceid, customer_orders.orderdate, customer_order_lines.shipdate,
					customer_order_lines.price, customer_order_lines.cost, customer_order_lines.qty_shipped, customer_order_lines.prodid,
					customer_order_lines.manname, customer_order_lines.manpart, customer_order_lines.descr,  customer_order_lines.ex_return
					from
					customer_orders, customer_order_lines
					where
					customer_orders.otype = 'CREDIT' AND
					customer_orders.orderid = customer_order_lines.orderid AND
					customer_orders.completedate >= '$xbdate' and
					customer_orders.completedate <= '$xedate' 
					AND customer_orders.clicktype = '1' 
					AND customer_orders.clickid = 'AM' 
					$addon			
								
					order by invoiceid, price desc";
		}
		else 
		{
			$query = "select customer_orders.otype,customer_orders.shipping, customer_orders.invoiceid, customer_orders.orderdate, customer_order_lines.shipdate,
					customer_order_lines.price, customer_order_lines.cost, customer_order_lines.qty_shipped, customer_order_lines.prodid,
					customer_order_lines.manname, customer_order_lines.manpart, customer_order_lines.descr, customer_order_lines.ex_return
					from
					customer_orders, customer_order_lines
					where
					customer_orders.orderid = customer_order_lines.orderid AND 
					customer_orders.orderdate >= '$xbdate' and
					customer_orders.orderdate <= '$xedate' 
					AND customer_orders.clicktype = '1' 
					AND customer_orders.clickid = 'AM' 
					$addon order by customer_orders.invoiceid, customer_order_lines.price desc";	
			
		}
		$q = mysql_query($query,$this->dbx);
		$rtotal = 0;
		$rcost = 0;
		$row = 2;
		while ($result = mysql_fetch_assoc($q)) {
			$odate = date("m/d/Y", $result['orderdate']);
			$sdate = date("m/d/Y", $result['shipdate']);
			
			$ttotal = 	$result['price'] * $result['qty_shipped'];
			$tcost = 	$result['cost'] * $result['qty_shipped'];
			
			$rtotal += 	$result['price'] * $result['qty_shipped'];
			$rcost += 	$result['cost'] * $result['qty_shipped'];
			if ($shipping == '1') {
				$rcost += $result['shipping'];
				$rtotal += $result['shipping'];
				$ttotal += 	$result['shipping'];
				$tcost += 	$result['shipping'];
			}
			$tprofit = $ttotal - $tcost;
			$tmargin = ($tprofit / $ttotal) * 100;
			$this->cellFormat(array('Size' => 10,
								'Align' => "left",
                                'Bold' => 0));
			$this->worksheet->write($row, 0, $result['otype'], $this->format);
			$this->worksheet->write($row, 1, $result['invoiceid'], $this->format);
			$this->worksheet->write($row, 2, $odate, $this->format);
			$this->worksheet->write($row, 3, $sdate, $this->format);
			$this->cellFormat(array('Size' => 10,
								'Align' => "right",
                                'Bold' => 0));
			$this->worksheet->write($row, 4, number_format($ttotal,2), $this->format);
			$this->worksheet->write($row, 5, number_format($tcost,2), $this->format);
			$this->worksheet->write($row, 6, number_format($ttotal-$tcost,2), $this->format);
			$this->worksheet->write($row, 7, number_format($tmargin,2)."%", $this->format);
			$row++;
		}
		$this->cellFormat(array('Size' => 10,
								'Align' => "right",
                                'Bold' => 0));
		$this->worksheet->write($row+1, 4, number_format($rtotal,2), $this->format);
		$this->worksheet->write($row+1, 5, number_format($rcost,2), $this->format);
		$this->worksheet->write($row+1, 6, number_format($rtotal-$rcost,2), $this->format);
		$this->worksheet->write($row+1, 7, number_format((($rtotal-$rcost)/$rtotal*100),2)."%", $this->format);
	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $cat0
	 * @param unknown_type $cat1
	 * @param unknown_type $cat2
	 * @param unknown_type $cat3
	 * @param unknown_type $cat4
	 * @param unknown_type $brand
	 * @param unknown_type $bdate
	 * @param unknown_type $edate
	 * @param unknown_type $pid
	 * @param unknown_type $useship
	 * @param unknown_type $status
	 */
	function getShipperLines($cat0,$cat1,$cat2,$cat3,$cat4,$brand,$bdate,$edate,$pid,$useship,$status) {
		$xbdate = strtotime("$bdate 00:00:00");
		$xedate = strtotime("$edate 23:59:59");
		
		if(intval($status) > 0)
		{
			$addon .= " AND customer_orders.status = '$status'";
			
			$query = mysql_query("select complete from company_orderflow where step = '$status'",$this->dbx);
			$co = mysql_fetch_assoc($query);
			$$complete = $co['complete'];
			
			if($complete == 1)
				$addon .= " and customer_orders.completedate >= '$xbdate' AND customer_orders.completedate <= '$xedate' ";
			else 
				$addon .= " and customer_orders.orderdate >= '$xbdate' AND customer_orders.orderdate <= '$xedate' ";
		}
		else 
		{
			$addon .= " and customer_orders.orderdate >= '$xbdate' AND customer_orders.orderdate <= '$xedate' ";
		}
		
		if(intval($brand) > 0)
			$addon .= " and customer_order_lines.manid = '$brand' ";
		if(intval($pid) > 0)
			$addon .= " and customer_order_lines.prodid = '$pid' ";
		
		
		if($cat4 != "" && $cat0 != "" && $cat1 != "" && $cat2 != "" && $cat3 != "")
		{ $cattable = ", product_category_map "; $addon .= " AND product_category_map.catid = '$cat4' AND customer_order_lines.prodid = product_category_map.pid";}
		else if($cat3 != "" && $cat0 != "" && $cat1 != "" && $cat2 != "")
		{ $cattable = ", product_category_map "; $addon .= " AND product_category_map.catid = '$cat3' AND customer_order_lines.prodid = product_category_map.pid";}
		else if($cat2 != "" && $cat0 != "" && $cat1 != "")
		{ $cattable = ", product_category_map "; $addon .= " AND product_category_map.catid = '$cat2' AND customer_order_lines.prodid = product_category_map.pid";}
		else if($cat1 != "" && $cat0 != "")
		{ $cattable = ", product_category_map "; $addon .= " AND product_category_map.catid = '$cat1' AND customer_order_lines.prodid = product_category_map.pid";}
		else if($cat0 != "")
		{ $cattable = ", product_category_map "; $addon .= " AND product_category_map.catid = '$cat0' AND customer_order_lines.prodid = product_category_map.pid";}
		
		$query = "select distinct(customer_order_lines.prodid), 
						sum(customer_order_lines.qty_shipped),
						sum(customer_order_lines.cost * customer_order_lines.qty_shipped),
						sum(customer_order_lines.price * customer_order_lines.qty_shipped),
						sum(customer_orders.subtotal),
						sum(customer_orders.cost),
						sum(customer_orders.shipping),
						customer_order_lines.manpart, 
						customer_order_lines.manname,
						customer_order_lines.descr
						from customer_orders, customer_order_lines $cattable
						where 
						customer_orders.orderid = customer_order_lines.orderid
						AND customer_orders.clicktype = '1' 
						AND customer_orders.clickid = 'AM' 
						$addon
						group by customer_order_lines.prodid
						";
		$q = mysql_query($query,$this->dbx);
		$row = 2;
		while ($result = mysql_fetch_assoc($q)) {
			
			$subtotal = $result['sum(customer_orders.subtotal)'];
			$price = $result['sum(customer_order_lines.price * customer_order_lines.qty_shipped)'];
			$cost = $result['sum(customer_order_lines.cost * customer_order_lines.qty_shipped)'];
			$qty = $result['sum(customer_order_lines.qty_shipped)'];
			$shipping = $result['sum(customer_orders.shipping)'];
			if($useship == 1) {// include shipping 
				$subtotal += $shipping;
				$price += $shipping;
				$cost += $shipping;
			}
			
			
			$tqty += $qty;
			$tcost += $cost;
			$tprice += $price;
			$this->cellFormat(array('Size' => 10,
								'Align' => "left",
                                'Bold' => 0));
			$this->worksheet->write($row, 0, $result['prodid'], $this->format);
			$this->cellFormat(array('Size' => 10,
								'Align' => "center",
                                'Bold' => 0));
			$this->worksheet->write($row, 1, $result['manpart'], $this->format);
			$this->worksheet->write($row, 2, $result['manname'], $this->format);
			$this->worksheet->write($row, 3, $result['descr'], $this->format);
			$this->worksheet->write($row, 4, $qty, $this->format);
			$this->cellFormat(array('Size' => 10,
								'Align' => "right",
                                'Bold' => 0));
			$this->worksheet->write($row, 5, number_format($price, 2), $this->format);
			$this->worksheet->write($row, 6, number_format($cost , 2), $this->format);
			$this->worksheet->write($row, 7, number_format($price - $cost, 2), $this->format);
			$this->worksheet->write($row, 8, number_format(100 * ($price - $cost) / $price, 2)."%", $this->format);
			$row++;
		}
		$this->cellFormat(array('Size' => 10,
								'Align' => "right",
                                'Bold' => 0));
		$this->worksheet->write($row+1, 4, $tqty, $this->format);
		$this->worksheet->write($row+1, 5, number_format($tprice, 2), $this->format);
		$this->worksheet->write($row+1, 6, number_format($tcost,2), $this->format);
		$this->worksheet->write($row+1, 7, number_format($tprice-$tcost,2), $this->format);
		$this->worksheet->write($row+1, 8, number_format(100 * ($tprice - $tcost) / $tprice, 2)."%", $this->format);
	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $data
	 * @param unknown_type $language
	 */
	function createWorksheet($get_var,$action) {
		
		if ($action == 'shipped') {
			$status = $get_var['status'];
			$bdate = $get_var['bdate'];
			$edate = $get_var['edate'];
			$repid = $get_var['repid'];
			$house = $get_var['house'];
			$shipping = $get_var['shipping'];
			$this->newWorksheetShipped();
			$this->worksheetTableHeaderShipped();
     	    $this->getShippedLines($status,$bdate,$edate,$repid,$house,$shipping);
		}		
	    if ($action == 'shipper') {
	    	$cat0 = $get_var['cat0'];
			$cat1 = $get_var['cat1'];
			$cat2 = $get_var['cat2'];
			$cat3 = $get_var['cat3'];
			$cat4 = $get_var['cat4'];
			$brand = $get_var['brand'];
			$bdate = $get_var['bdate'];
			$edate = $get_var['edate'];
			$pid = $get_var['pid'];
			$useship = $get_var['useship'];
			$status = $get_var['status'];
			$this->newWorksheetShipper();
			$this->worksheetTableHeaderShipper();
     	    $this->getShipperLines($cat0,$cat1,$cat2,$cat3,$cat4,$brand,$bdate,$edate,$pid,$useship,$status);
		}	
		$this->workbook->close();
	}
}
$action = $_GET['action'];
$name = "report_".time().".xls";
$ex = new excelExport($name,$MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD,$DB);
if ($action == "shipped") {
	

	$ex->createWorksheet($_GET,$action);
	
	header("Content-Type: application/x-msexcel; name=\"report.xls\"");
	header("Content-Disposition: inline; filename=\"report.xls\"");
	$fh=fopen($name, "rb");
	fpassthru($fh);
	unlink($name);
}
if ($action == "shipper") {
	
	
	$ex->createWorksheet($_GET,$action);
	
	header("Content-Type: application/x-msexcel; name=\"report.xls\"");
	header("Content-Disposition: inline; filename=\"report.xls\"");
	$fh=fopen($name, "rb");
	fpassthru($fh);
	unlink($name);
}
?>