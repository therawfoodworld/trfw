<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
<DocumentVersion>1.01</DocumentVersion>
<MerchantIdentifier>M_BUTTERFLYP_388783</MerchantIdentifier>
</Header>
<MessageType>OrderReport</MessageType>
<Message>
<MessageID>1</MessageID>
<OrderReport>
<AmazonOrderID>102-1203018-2258616</AmazonOrderID>
<AmazonSessionID>102-1203018-2258616</AmazonSessionID>
<OrderDate>2011-05-22T22:08:30-07:00</OrderDate>
<OrderPostedDate>2011-05-22T22:08:30-07:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>1prpr34d8ts7ngx@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>Scott Richard</BuyerName>
<BuyerPhoneNumber>2854859005</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>Scott Richard</Name>
<AddressFieldOne>3314 Castlebay Ct</AddressFieldOne>
<City>Pearland</City>
<StateOrRegion>TX</StateOrRegion>
<PostalCode>77584-7918</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>2854859005</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>10618975206962</AmazonOrderItemCode>
<SKU>41575</SKU>
<Title>Essential 37mm Lens Kit Bundle For Sony HDR-CX130, HDR-CX160, HDR-CX360V, HDR-CX560V, HDR-CX700V, HDR-XR160 HD Handycam Camcorder Includes .21x HD Sup</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1306126456</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>176-9410053-4985018</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiY&gt;TSj88</Type>
<Data>V:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-8849699-4215464:8d82e298-1daf-396d-bd3c-3a6da4a3</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>8d82e298-1daf-396d-bd3c-3a6da4a31f02</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestAddressId</Type>
<Data>1272605861</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4DD9E878FFAD0000000164E37284</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>1f02#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0408IS8APE7</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>Essential 37mm Lens Kit Bundle For Sony HDR-CX130, HDR-CX160, HDR-CX360V, HDR...</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">59.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">4.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-5.20</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>440395020</TaxLocationCode>
<City>PEARLAND</City>
<County>BRAZORIA</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">59.95</City>
<County currency="USD">59.95</County>
<State currency="USD">59.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">59.95</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>440395020</TaxLocationCode>
<City>PEARLAND</City>
<County>BRAZORIA</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">4.95</City>
<County currency="USD">4.95</County>
<State currency="USD">4.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">4.95</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
<Item>
<AmazonOrderItemCode>42662817659034</AmazonOrderItemCode>
<SKU>41542</SKU>
<Title>Must Have Accessory Kit For Sony HDR-CX700V High Definition Camcorder Includes Replacement (2300Mah) NP-FV70 Battery + Ac / DC Charger + Deluxe Case +</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1306126621</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>176-9410053-4985018</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiY&gt;TSj88</Type>
<Data>V:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-8849699-4215464:5133082e-7931-3050-9c51-7a502d71</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>5133082e-7931-3050-9c51-7a502d717dd9</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestAddressId</Type>
<Data>1272605861</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4DD9E91DFFAD0000000164E3F104</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>7dd9#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0408IS7Q0RJ</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>Must Have Accessory Kit For Sony HDR-CX700V High Definition Camcorder Includes</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">59.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">5.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-5.28</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>440395020</TaxLocationCode>
<City>PEARLAND</City>
<County>BRAZORIA</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">59.95</City>
<County currency="USD">59.95</County>
<State currency="USD">59.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">59.95</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>440395020</TaxLocationCode>
<City>PEARLAND</City>
<County>BRAZORIA</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">5.95</City>
<County currency="USD">5.95</County>
<State currency="USD">5.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">5.95</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
</AmazonEnvelope>
