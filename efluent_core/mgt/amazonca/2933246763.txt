<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
<DocumentVersion>1.01</DocumentVersion>
<MerchantIdentifier>M_BUTTERFLYP_388783</MerchantIdentifier>
</Header>
<MessageType>OrderReport</MessageType>
<Message>
<MessageID>1</MessageID>
<OrderReport>
<AmazonOrderID>002-6834727-1641801</AmazonOrderID>
<AmazonSessionID>002-6834727-1641801</AmazonSessionID>
<OrderDate>2010-10-14T07:09:31-07:00</OrderDate>
<OrderPostedDate>2010-10-14T07:09:31-07:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>3y31slnd6hn3nrd@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>LUIS VELOSA (CM CARGO)</BuyerName>
<BuyerPhoneNumber>3055009234</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>LUIS VELOSA (CM CARGO)</Name>
<AddressFieldOne>8400 NW 17TH ST</AddressFieldOne>
<City>MIAMI</City>
<StateOrRegion>FLORIDA</StateOrRegion>
<PostalCode>33126-1010</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>3055009234</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>41411854701978</AmazonOrderItemCode>
<SKU>32648</SKU>
<Title>Canon Powershot G12 Accessory Kit Includes USB 2.0 High Speed Card Reader + Extended Replacement NB-7l (1300 mAH) Battery + Ac/Dc Rapid Travel Charger</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1287064781</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>178-3897527-5666060</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>UPC</Type>
<Data>628586193161</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>1007GF3BBLJ</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>Canon Powershot G12 Accessory Kit Includes USB 2.0 High Speed Card Reader...</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">29.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">2.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-2.64</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>100250000</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>MIAMI-DADE</County>
<State>FL</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">29.95</County>
<State currency="USD">29.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">29.95</District>
<City currency="USD">29.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>100250000</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>MIAMI-DADE</County>
<State>FL</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">2.95</County>
<State currency="USD">2.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">2.95</District>
<City currency="USD">2.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
<Message>
<MessageID>2</MessageID>
<OrderReport>
<AmazonOrderID>002-2494292-4229833</AmazonOrderID>
<AmazonSessionID>002-2494292-4229833</AmazonSessionID>
<OrderDate>2010-10-14T07:08:11-07:00</OrderDate>
<OrderPostedDate>2010-10-14T07:08:11-07:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>t0xqwjgxjqv5f8w@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>ACCOUNTS PAYABLE</BuyerName>
<BuyerPhoneNumber>2077785332</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>School Administrative District #9</Name>
<AddressFieldOne>115 Learning Lane</AddressFieldOne>
<City>Farmington</City>
<StateOrRegion>ME</StateOrRegion>
<PostalCode>04938</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>207-778-6571</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>24767784959890</AmazonOrderItemCode>
<SKU>19081</SKU>
<Title>Tripod &amp; Carrying Case Kit Exclusive For The Canon FS300 FS31 HF M300 HF M30 HF M31 HF S200 H S20 HF S21 Flash Memory Camcorder Includes 57Inch Tripod</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1287065078</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>186-1858192-9694255</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>UPC</Type>
<Data>628586116740</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0114G1G9LO1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>Tripod &amp; Carrying Case Kit Exclusive For The Canon FS300 FS31 HF M300 HF M30 HF</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00+00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">34.89</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-2.79</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>200070160</TaxLocationCode>
<City>FARMINGTON</City>
<County>FRANKLIN</County>
<State>ME</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">34.89</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">34.89</District>
<City currency="USD">34.89</City>
<County currency="USD">34.89</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
</Item>
</OrderReport>
</Message>
</AmazonEnvelope>
