<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     FBAInboundServiceMWS
 *  @copyright   Copyright 2008-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2010-10-01
 */
 
/******************************************************************************* 
 * 
 *  FBA Inbound Service MWS PHP5 Library
 *  Generated: Fri Sep 13 02:43:24 GMT 2013
 * 
 */

/**
 *  @see FBAInboundServiceMWS_Model
 */

require_once (dirname(__FILE__) . '/../Model.php');


/**
 * FBAInboundServiceMWS_Model_GetTransportContentResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>TransportContent: FBAInboundServiceMWS_Model_TransportContent</li>
 *
 * </ul>
 */

 class FBAInboundServiceMWS_Model_GetTransportContentResult extends FBAInboundServiceMWS_Model {

    public function __construct($data = null)
    {
        $this->_fields = array (
            'TransportContent' => array('FieldValue' => null, 'FieldType' => 'FBAInboundServiceMWS_Model_TransportContent'),
        );
	    parent::__construct($data);
    }

    /**
     * Get the value of the TransportContent property.
     *
     * @return TransportContent TransportContent.
     */
    public function getTransportContent()
	{
	    return $this->_fields['TransportContent']['FieldValue'];
    }

    /**
     * Set the value of the TransportContent property.
     *
     * @param FBAInboundServiceMWS_Model_TransportContent transportContent
     * @return this instance
     */
    public function setTransportContent($value)
	{
	    $this->_fields['TransportContent']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if TransportContent is set.
     *
     * @return true if TransportContent is set.
     */
    public function isSetTransportContent()
	{
	            return !is_null($this->_fields['TransportContent']['FieldValue']);
		    }

    /**
     * Set the value of TransportContent, return this.
     *
     * @param transportContent
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withTransportContent($value)
	{
        $this->setTransportContent($value);
        return $this;
    }

}
