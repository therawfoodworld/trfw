<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     FBAInboundServiceMWS
 *  @copyright   Copyright 2008-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2010-10-01
 */
 
/******************************************************************************* 
 * 
 *  FBA Inbound Service MWS PHP5 Library
 *  Generated: Fri Sep 13 02:43:24 GMT 2013
 * 
 */

/**
 *  @see FBAInboundServiceMWS_Model
 */

require_once (dirname(__FILE__) . '/../Model.php');


/**
 * FBAInboundServiceMWS_Model_CreateInboundShipmentPlanResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>InboundShipmentPlans: FBAInboundServiceMWS_Model_InboundShipmentPlanList</li>
 *
 * </ul>
 */

 class FBAInboundServiceMWS_Model_CreateInboundShipmentPlanResult extends FBAInboundServiceMWS_Model {

    public function __construct($data = null)
    {
        $this->_fields = array (
            'InboundShipmentPlans' => array('FieldValue' => null, 'FieldType' => 'FBAInboundServiceMWS_Model_InboundShipmentPlanList'),
        );
	    parent::__construct($data);
    }

    /**
     * Get the value of the InboundShipmentPlans property.
     *
     * @return InboundShipmentPlanList InboundShipmentPlans.
     */
    public function getInboundShipmentPlans()
	{
	    return $this->_fields['InboundShipmentPlans']['FieldValue'];
    }

    /**
     * Set the value of the InboundShipmentPlans property.
     *
     * @param FBAInboundServiceMWS_Model_InboundShipmentPlanList inboundShipmentPlans
     * @return this instance
     */
    public function setInboundShipmentPlans($value)
	{
	    $this->_fields['InboundShipmentPlans']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if InboundShipmentPlans is set.
     *
     * @return true if InboundShipmentPlans is set.
     */
    public function isSetInboundShipmentPlans()
	{
	            return !is_null($this->_fields['InboundShipmentPlans']['FieldValue']);
		    }

    /**
     * Set the value of InboundShipmentPlans, return this.
     *
     * @param inboundShipmentPlans
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withInboundShipmentPlans($value)
	{
        $this->setInboundShipmentPlans($value);
        return $this;
    }

}
