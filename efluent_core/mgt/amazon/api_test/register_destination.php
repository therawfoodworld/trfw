<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
$ROOT_DIR = $argv[1];

if($ROOT_DIR == "")
{
	/*echo "usage: create_inventory_feed.phtml ROOT_DIR REQUESTID\n\n";
	exit;*/
	$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
}
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/amazon/amazonclass.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

/********************************************************************************************/
set_include_path(get_include_path() . PATH_SEPARATOR . '../');    

function __autoload($className)
{
	$filePath = str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
	$includePaths = explode(PATH_SEPARATOR, get_include_path());
    foreach($includePaths as $includePath)
    {
    	if(file_exists($includePath . DIRECTORY_SEPARATOR . $filePath))
    	{
        	require_once $filePath;
            return;
		}
	}
}
/********************************************************************************************/
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

/********************************************************************************************/
define ('DATE_FORMAT', 'Y-m-d\TH:i:s\Z');
 
define('AWS_ACCESS_KEY_ID', 	$efc->getConfigValue("AmazonFBA", "AWS_ACCESS_KEY_ID"));
define('AWS_SECRET_ACCESS_KEY', $efc->getConfigValue("AmazonFBA", "AWS_SECRET_ACCESS_KEY"));  

define('APPLICATION_NAME', 'eFluent');
define('APPLICATION_VERSION', '2.0');

define ('MERCHANT_ID',  	$efc->getConfigValue("AmazonFBA", "MERCHANT_ID"));
define ('MARKETPLACE_ID', 	$efc->getConfigValue("AmazonFBA", "MARKETPLACE_ID"));
/********************************************************************************************/
$serviceUrl = "https://mws.amazonservices.com/Subscriptions/2013-07-01";

/************************************************************************
 * Instantiate Implementation of MWSSubscriptionsService
 *
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants
 * are defined in the .config.inc.php located in the same
 * directory as this sample
 ***********************************************************************/
// More endpoints are listed in the MWS Developer Guide
// North America:
//$serviceUrl = "https://mws.amazonservices.com/Subscriptions/2013-07-01";
// Europe
//$serviceUrl = "https://mws-eu.amazonservices.com/Subscriptions/2013-07-01";
// Japan
//$serviceUrl = "https://mws.amazonservices.jp/Subscriptions/2013-07-01";
// China
//$serviceUrl = "https://mws.amazonservices.com.cn/Subscriptions/2013-07-01";


 $config = array (
   'ServiceURL' => $serviceUrl,
   'ProxyHost' => null,
   'ProxyPort' => -1,
   'MaxErrorRetry' => 3,
 );

 $service = new MWSSubscriptionsService_Client(
        AWS_ACCESS_KEY_ID,
        AWS_SECRET_ACCESS_KEY,
        APPLICATION_NAME,
        APPLICATION_VERSION,
        $config);

/************************************************************************
 * Uncomment to try out Mock Service that simulates MWSSubscriptionsService
 * responses without calling MWSSubscriptionsService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MWSSubscriptionsService/Mock tree
 *
 ***********************************************************************/
 // $service = new MWSSubscriptionsService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out
 * sample for Register Destination Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MWSSubscriptionsService_Model_RegisterDestination
 $request = new MWSSubscriptionsService_Model_RegisterDestinationInput();
 $request->setSellerId(MERCHANT_ID);
 $request->setMarketplaceId(MARKETPLACE_ID);
 $destination = new MWSSubscriptionsService_Model_Destination();
 $destination->setDeliveryChannel('SQS');
 
 $attribute = new MWSSubscriptionsService_Model_AttributeKeyValue();
 $attribute->setKey('sqsQueueUrl');
 $attribute->setValue('https://sqs.us-west-2.amazonaws.com/572783313015/Pricer');
 
 $attributeList = new MWSSubscriptionsService_Model_AttributeKeyValueList();
 $attributeList->withmember($attribute);
 
 $destination->setAttributeList($attributeList);
 // object or array of parameters
 print_r($request);
 $request->setDestination($destination);
 invokeRegisterDestination($service, $request);

/**
  * Get Register Destination Action Sample
  * Gets competitive pricing and related information for a product identified by
  * the MarketplaceId and ASIN.
  *
  * @param MWSSubscriptionsService_Interface $service instance of MWSSubscriptionsService_Interface
  * @param mixed $request MWSSubscriptionsService_Model_RegisterDestination or array of parameters
  */

  function invokeRegisterDestination(MWSSubscriptionsService_Interface $service, $request)
  {
      try {
        $response = $service->RegisterDestination($request);

        echo ("Service Response\n");
        echo ("=============================================================================\n");

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        echo $dom->saveXML();
        echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");

     } catch (MWSSubscriptionsService_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

