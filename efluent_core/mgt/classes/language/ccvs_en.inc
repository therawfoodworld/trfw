<?php

/**
 * Credit Card Validation Solution, PHP Edition,
 * English language error messages.
 *
 * @package    CreditCardValidationSolution
 * @author     Daniel Convissor <danielc@analysisandsolutions.com>
 * @copyright  The Analysis and Solutions Company, 2002-2006
 * @version    $Name: rel-5-13 $ $Id: ccvs_en.inc,v 1.3 2006/03/18 17:31:31 danielc Exp $
 * @link       http://www.analysisandsolutions.com/software/ccvs/ccvs.htm
 */

/** */
$CCVSErrNumberString = 'Not a string';
$CCVSErrVisa14       = '14 Digits';
$CCVSErrUnknown      = 'Unknown card type';
$CCVSErrAccepted     = 'Programmer improperly used the Accepted argument';
$CCVSErrNoAccept     = 'We don\'t accept %s cards';
$CCVSErrShort        = 'Missing %s digit(s)';
$CCVSErrLong         = '%s too many digit(s)';
$CCVSErrChecksum     = 'Failed checksum test';
$CCVSErrMonthString  = 'Month isn\'t a string';
$CCVSErrMonthFormat  = 'Month has invalid format';
$CCVSErrYearString   = 'Year isn\'t a string';
$CCVSErrYearFormat   = 'Year has invalid format';
$CCVSErrExpired      = 'Card has expired';
