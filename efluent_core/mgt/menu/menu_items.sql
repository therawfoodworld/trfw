-- MySQL dump 10.9
--
-- Host: localhost    Database: beachtr
-- ------------------------------------------------------
-- Server version	4.1.8-standard

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `mid` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  `parent` bigint(20) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--


/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
LOCK TABLES `menu_items` WRITE;
INSERT INTO `menu_items` VALUES (1,'Home','/i/default.phtml',0),(2,'Users','/i/users.phtml',0),(3,'Admin Log','/i/log.phtml',0),(4,'Marketing','/i/marketing.phtml',0),(5,'Tools','/i/tools.phtml',0),(6,'Logout','/login.phtml',0),(7,'Banner Manager','http://192.168.1.251/bmanager/default.phtml',4),(19,'Configuration','/config/default.phtml',4),(20,'Datafeed Manager','/datafeed/default.phtml',4),(21,'Database Manager','http://intranet.beachtr.com/phpMyAdmin/index.php',4),(15,'asdf','',14),(22,'Froogle Map','/froogle/default.phtml',4),(17,'','',15),(23,'Google','/google/default.phtml',4),(24,'Dashboard','/google/default.phtml',23),(25,'Categories','/google/category_list.phtml',23),(26,'SKU Perf','/google/category_list.phtml',23),(27,'Log','/google/action_log.phtml',23),(28,'Queue','/google/view_queue.phtml',23),(29,'PE Reports','/roilogic/default.phtml',4),(30,'Google','/google/default.phtml',5),(31,'Rebates','/i/rebates.phtml',5),(32,'Images','/i/images.phtml',5),(33,'Inventory Alerts','/i/inventoryalerts.phtml',5),(34,'Advanced Search','/i/search.phtml',5);
UNLOCK TABLES;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

