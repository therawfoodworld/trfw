create table user_permissions
(
userid int NOT NULL default 0,
perm_category_manager int NOT NULL default 0,
perm_edit_menu int NOT NULL default 0,
perm_email_templates int NOT NULL default 0,
perm_ingram_markup int NOT NULL default 0,
perm_order_flow int NOT NULL default 0,
perm_shipping int NOT NULL default 0,
perm_site_config int NOT NULL default 0,
perm_featured_products int NOT NULL default 0,
perm_pegrid int NOT NULL default 0,
perm_categorize int NOT NULL default 0,
perm_visible int NOT NULL default 0,
perm_datafeed int NOT NULL default 0,
perm_scolumns int NOT NULL default 0,
perm_createskus int NOT NULL default 0,
perm_accbycat int NOT NULL default 0,
perm_scost int NOT NULL default 0,
perm_editsku int NOT NULL default 0,
perm_datafeed_manager int NOT NULL default 0,
perm_email_marketing int NOT NULL default 0,
perm_traffic_reports int NOT NULL default 0,
perm_po_search int NOT NULL default 0,
perm_vendor_search int NOT NULL default 0,
perm_po_edit int NOT NULL default 0,
perm_po_create int NOT NULL default 0,
perm_vendor_edit int NOT NULL default 0,
perm_vendor_create int NOT NULL default 0,
perm_user_manager int NOT NULL default 0,
perm_online_help int NOT NULL default 0,
index(userid)
);

create table user_ip_restriction
(
userid int NOT NULL default 0,
ipaddr varchar(16),
index(userid),
index(userid, ipaddr)
);

alter table user_permissions add column perm_skutabs_price int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_descr int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_hspecs int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_image int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_specs int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_access int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_sourcing int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_shipping int NOT NULL default 0;
alter table user_permissions add column perm_skutabs_perf int NOT NULL default 0;

alter table user_permissions add column perm_manual_billing int NOT NULL default 0;
alter table user_permissions add column perm_add_cards int NOT NULL default 0;
alter table user_permissions add column perm_decrypt_cards int NOT NULL default 0;
alter table user_permissions add column perm_marketing_campaigns int NOT NULL default 0;