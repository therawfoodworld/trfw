create table shipping_codes
(
id bigint NOT NULL auto_increment,
active int NOT NULL default 0,
catid bigint NOT NULL default 0,
flatrate int NOT NULL default 0,
method_name varchar(255), #displayed to customers
method_descr varchar(255), # displayed to customers
method_note text, 
formula longblob,
index(id),
index(catid),
index(active)
);


create table shipping_assignment
(
id bigint NOT NULL auto_increment,
scid bigint NOT NULL, # shipping_codes.id
pid bigint NOT NULL, # product_id
index(id),
index(scid),
index(pid)
);

alter table product_list add shipping_flag int NOT NULL default 0;


product_list.shipping_flag can be
  0 - use standard and assigned
  1 - use standard assignement (not assigned)
  2 - use assigned shipping methods only

//////////////////////////////////
shipping_codes.catid can be
  = 0 - Not automatically applied to anything [must be assigned at the sku level]
  > 0 - automatically assigned to all products in that category and all categories below in tree
  = -1 - Globally assigned to all products


//////////////////////////////////////////////////////////////////////////////

Shipping category assignement for an order is determined by the main item* on the order.  Shipping should be assigned and flagged as flatrate for items where a flat shipping rate would be required on a per item basis (i.e. televisions, furniture).

*The main item on the order is determined by price.


////////////////////////////////////
// variables exposed

For each item on the order, e2 will check to see if a flatrate shipping method is available.  If so, the flatrate amount will be applied to the order and that item will be excluded from the future shipping calculation.

For example:

The following order is placed:

1 Television $1500 (flatrate shipping method is assigned to this sku with a price of $200)
1 Camera $300 (no assigned shipping method set by merchant)
1 carrying case $20 (no assigned shipping method set by merchant)

Since the television has been assigned a flatrate shipping method at $200.  The shipping for the order would be $200 + the calculation of the rest of the order.  In this case, the TV will be removed from the continuing calculation leaving the camera as the most expensive item (category assigned shipping would be for "Cameras" on this order).  The additional shipping charges would be computed on the camera and the carrying case as if they were on a order without the television.

