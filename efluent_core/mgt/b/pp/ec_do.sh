#!/bin/bash
# ec_do.sh
# Do the Express Checkout payment

if test $# -ne 5
then
   echo Bad argument count.  Usage:
   echo "   " $0 "[API username] [API password] [token] [payer ID] [amount]"
   echo example:
   echo "   " $0 paypaltech_api1.ersatz.biz CRz7pawd EC-29879344RH8987624 12345678901234567 1.15
   echo "   " $0 dave_seller_api1.ersatz.biz chumphauler EC-56049627RH6486701 RRJPLTMFCREJQ 1.15
   exit 1
fi


# Set the temp file name to a string based on the process ID
tempfile=/usr/tmp/do_ec_tempfile_$$
apiusername=$1
apipw=$2
token=$3
payerid=$4
amount=$5
certpath=/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt

cat << EOF > $tempfile

<SOAP-ENV:Envelope
   xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
   xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
   xmlns:xsd="http://www.w3.org/1999/XMLSchema"
   SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
   <SOAP-ENV:Header>
      <RequesterCredentials
        xmlns="urn:ebay:api:PayPalAPI"
        SOAP-ENV:mustUnderstand="1"
      >
         <Credentials xmlns="urn:ebay:apis:eBLBaseComponents">
            <Username>$apiusername</Username>
            <Password>$apipw</Password>
            <Subject/>
         </Credentials>
      </RequesterCredentials>
   </SOAP-ENV:Header>
   <SOAP-ENV:Body>
      <DoExpressCheckoutPaymentReq xmlns="urn:ebay:api:PayPalAPI">
         <DoExpressCheckoutPaymentRequest>
            <Version
              xmlns="urn:ebay:apis:eBLBaseComponents"
            >1.0</Version>
            <DoExpressCheckoutPaymentRequestDetails
              xmlns="urn:ebay:apis:eBLBaseComponents"
            >
               <Token>$token</Token>
               <PaymentAction>Sale</PaymentAction>
               <PayerID>$payerid</PayerID>
               <PaymentDetails>
                  <OrderTotal
                    xmlns="urn:ebay:apis:eBLBaseComponents"
                    currencyID="USD"
                  >$amount</OrderTotal>
               </PaymentDetails>
            </DoExpressCheckoutPaymentRequestDetails>
         </DoExpressCheckoutPaymentRequest>
      </DoExpressCheckoutPaymentReq>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>

EOF

# Let's try getting the Express Checkout details
curl -v -E $certpath -F "content=@$tempfile;type=text/xml" https://api.paypal.com/2.0/
rm $tempfile
echo

