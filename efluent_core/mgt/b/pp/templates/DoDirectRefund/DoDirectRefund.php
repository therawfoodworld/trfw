<?php

$values = $this->_values;

$SOAPrequest .= <<< End_of_quote
   <SOAP-ENV:Body>
      <RefundTransactionReq xmlns="urn:ebay:api:PayPalAPI">
         <RefundTransactionRequest xsi:type="ns:RefundTransactionRequestType">
            <Version xmlns="urn:ebay:apis:eBLBaseComponents" xsi:type="xsd:string">1.0</Version>
            <TransactionID xsi:type="ebl:TransactionId">{$values['txid']}</TransactionID>
            <RefundType>Partial</RefundType>
            <Amount currencyID="USD" xsi:type="cc:BasicAmountType">{$values['amount']}</Amount>
            <Memo>{$values['memo']}</Memo>
         </RefundTransactionRequest>
      </RefundTransactionReq>
   </SOAP-ENV:Body>

End_of_quote;



?>