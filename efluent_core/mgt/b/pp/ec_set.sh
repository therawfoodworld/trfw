#!/bin/bash
# ec_set.sh
# Set an Express Checkout amount

if test $# -ne 5
then
   echo Bad argument count.  Usage:
   echo "   " $0 "[API username] [API password] [amount] [ReturnURL] [CancelURL]"
   echo example:
   echo "   " $0 paypaltech_api1.ersatz.biz CRz7pawd 3.00 http://www.asite.com/return.asp http://www.asite.com/cancel.asp
   echo "   " $0 dave_seller_api1.ersatz.biz chumphauler 1.00 http://www.asite.com/return.asp http://www.asite.com/cancel.asp
   exit 1
fi

# Set the temp file name to a string based on the process ID
tempfile=/usr/tmp/set_ec_tempfile_$$
apiusername=$1
apipw=$2
amount=$3
returnURL=$4
cancelURL=$5
certpath=/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt

cat << EOF > $tempfile

<SOAP-ENV:Envelope
   xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
   xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
   xmlns:xsd="http://www.w3.org/1999/XMLSchema"
   SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
   <SOAP-ENV:Header>
      <RequesterCredentials
        xmlns="urn:ebay:api:PayPalAPI"
        SOAP-ENV:mustUnderstand="1"
      >
         <Credentials xmlns="urn:ebay:apis:eBLBaseComponents">
            <Username>$apiusername</Username>
            <Password>$apipw</Password>
            <Subject/>
         </Credentials>
      </RequesterCredentials>
   </SOAP-ENV:Header>
   <SOAP-ENV:Body>
      <SetExpressCheckoutReq xmlns="urn:ebay:api:PayPalAPI">
         <SetExpressCheckoutRequest xsi:type="ns:SetExpressCheckoutRequestType">
            <Version
              xmlns="urn:ebay:apis:eBLBaseComponents"
              xsi:type="xsd:string"
            >1.0</Version>
            <SetExpressCheckoutRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">
               <OrderTotal
                 xmlns="urn:ebay:apis:eBLBaseComponents"
                 currencyID="USD"
                 xsi:type="cc:BasicAmountType"
               >$amount</OrderTotal>
               <ReturnURL xsi:type="xsd:string">$returnURL</ReturnURL>
               <CancelURL xsi:type="xsd:string">$cancelURL</CancelURL>
            </SetExpressCheckoutRequestDetails>
         </SetExpressCheckoutRequest>
      </SetExpressCheckoutReq>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>

EOF

# Let's try setting an Express Checkout amount
curl -v -E $certpath -F "content=@$tempfile;type=text/xml" https://api.paypal.com/2.0/
rm $tempfile
echo
