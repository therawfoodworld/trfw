#!/bin/bash
# ec_get.sh
# Get some Express Checkout details

if test $# -ne 3
then
   echo Bad argument count.  Usage:
   echo "   " $0 "[API username] [API password] [token]"
   echo example:
   echo "   " $0 paypaltech_api1.ersatz.biz CRz7pawd EC-29879344RH8987624
   echo "   " $0 dave_seller_api1.ersatz.biz chumphauler EC-56049627RH6486701
   exit 1
fi

# Set the temp file name to a string based on the process ID
tempfile=/usr/tmp/get_ec_tempfile_$$
apiusername=$1
apipw=$2
token=$3
certpath=/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt

cat << EOF > $tempfile

<SOAP-ENV:Envelope
   xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
   xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
   xmlns:xsd="http://www.w3.org/1999/XMLSchema"
   SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
   <SOAP-ENV:Header>
      <RequesterCredentials
        xmlns="urn:ebay:api:PayPalAPI"
        SOAP-ENV:mustUnderstand="1"
      >
         <Credentials xmlns="urn:ebay:apis:eBLBaseComponents">
            <Username>$apiusername</Username>
            <Password>$apipw</Password>
            <Subject/>
         </Credentials>
      </RequesterCredentials>
   </SOAP-ENV:Header>
   <SOAP-ENV:Body>
      <GetExpressCheckoutDetailsReq xmlns="urn:ebay:api:PayPalAPI">
         <GetExpressCheckoutDetailsRequest xsi:type="ns:SetExpressCheckoutRequestType">
            <Version
              xmlns="urn:ebay:apis:eBLBaseComponents"
              xsi:type="xsd:string"
            >1.0</Version>
            <Token>$token</Token>
         </GetExpressCheckoutDetailsRequest>
      </GetExpressCheckoutDetailsReq>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>

EOF

# Let's try getting the Express Checkout details
curl -v -E $certpath -F "content=@$tempfile;type=text/xml" https://api.paypal.com/2.0/
rm $tempfile
echo
