<?php

/**
 * Copyright (C) 2007 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /* This is the response handler code that will be invoked every time
  * a notification or request is sent by the Google Server
  *
  * To allow this code to receive responses, the url for this file
  * must be set on the seller page under Settings->Integration as the
  * "API Callback URL'
  * Order processing commands can be sent automatically by placing these
  * commands appropriately
  *
  * To use this code for merchant-calculated feedback, this url must be
  * set also as the merchant-calculations-url when the cart is posted
  * Depending on your calculations for shipping, taxes, coupons and gift
  * certificates update parts of the code as required
  *
  */
require_once("../../config.phtml");
require_once($ROOT_DIR."/include/mysql.phtml");
require_once($ROOT_DIR."/classes/siteclass.phtml");
require_once($ROOT_DIR."/classes/ipinfoclass.phtml");
require_once($ROOT_DIR."/classes/customerclass.phtml");
require_once($ROOT_DIR."/classes/cartclass.phtml");
require_once($ROOT_DIR."/mgt/classes/efluentclass.phtml");	
 	
require_once($ROOT_DIR."/checkout/gc/googleresponse.php");
require_once($ROOT_DIR.'/checkout/gc/googlemerchantcalculations.php');
require_once($ROOT_DIR.'/checkout/gc/googleresult.php');
require_once($ROOT_DIR.'/checkout/gc/googlerequest.php');

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
	  
function gc_get_arr_result($child_node) 
{
	$result = array();
	if(isset($child_node)) 
	{
		if(gc_is_associative_array($child_node)) 
		{
			$result[] = $child_node;
		}
    	else 
    	{
      		foreach($child_node as $curr_node)
      		{
				$result[] = $curr_node;
			}
		}
	}

	return $result;
}
	
// Returns true if a given variable represents an associative array
function gc_is_associative_array($var) 
{
	return is_array($var) && !is_numeric(implode('', array_keys($var)));
} 

function sanitizeInput($data)
{
	$data = str_replace("/", "", $data);
	$data = str_replace("\"", "", $data);
	$data = str_replace("'", "", $data);
	$data = str_replace(")", "", $data);
	$data = str_replace("(", "", $data);
	$data = str_replace(";", "", $data);
	$data = str_replace("|", "", $data);
	$data = str_replace("<", "", $data);
	$data = str_replace(">", "", $data);
	$data = str_replace("#", "", $data);
	$data = str_replace(":", "", $data);
	
	return $data;
}
	
//define('RESPONSE_HANDLER_ERROR_LOG_FILE', 'googleerror.log');
//define('RESPONSE_HANDLER_LOG_FILE', 'googlemessage.log');

$merchant_id = $efc->getConfigValue("Google_Checkout", "MERCHANT_ID");  // Your Merchant ID
$merchant_key = $efc->getConfigValue("Google_Checkout", "MERCHANT_KEY");  // Your Merchant Key
$server_type = $efc->getConfigValue("Google_Checkout", "SERVER_TYPE");;  // change this to go live
$currency = 'USD';  // set to GBP if in the UK

$Gresponse = new GoogleResponse($merchant_id, $merchant_key);
$Grequest = new GoogleRequest($merchant_id, $merchant_key, $server_type, $currency);

// Retrieve the XML sent in the HTTP POST request to the ResponseHandler
$xml_response = isset($HTTP_RAW_POST_DATA)?$HTTP_RAW_POST_DATA:file_get_contents("php://input");
if (get_magic_quotes_gpc()) 
{
	$xml_response = stripslashes($xml_response);
}
 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$tstamp = date("Y-m-d H:i:s", time());
$query = "insert into google_debug(tstamp, data) values('$tstamp', '".addslashes($xml_response)."')";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

list($root, $data) = $Gresponse->GetParsedXML($xml_response);
$Gresponse->SetMerchantAuthentication($merchant_id, $merchant_key);

$status = $Gresponse->HttpAuthentication();

if(! $status) 
{
	die('authentication failed');
}

switch ($root) 
{
	case "request-received": 
	{
		break;
    }
    case "error": 
    {
		break;
    }
    case "diagnosis": 
    {
		break;
    }
    case "checkout-redirect": 
    {
    	break;
    }
    case "merchant-calculation-callback": 
    {
		break;
	}
    case "new-order-notification": 
    {
		list ($firstname, $lastname) = explode(' ', $data[$root]['buyer-billing-address']['contact-name']['VALUE'], 2);

		$acctid = 0;
		$phone = $data[$root]['buyer-billing-address']['phone']['VALUE'];
		$altphone = $data[$root]['buyer-billing-address']['fax']['VALUE'];
		$emailaddr = $data[$root]['buyer-billing-address']['email']['VALUE'];
		
		$fname = addslashes($firstname);
		$lname = addslashes($lastname);
		
		$bill2name = addslashes($data[$root]['buyer-billing-address']['contact-name']['VALUE']);
		$bill2street1 = addslashes($data[$root]['buyer-billing-address']['address1']['VALUE']);
		$bill2street2 = addslashes($data[$root]['buyer-billing-address']['address2']['VALUE']);
		$bill2city = addslashes($data[$root]['buyer-billing-address']['city']['VALUE']);
		$bill2state = addslashes($data[$root]['buyer-billing-address']['region']['VALUE']);
		$bill2zip = addslashes($data[$root]['buyer-billing-address']['postal-code']['VALUE']);
		
		$ship2name = addslashes($data[$root]['buyer-shipping-address']['contact-name']['VALUE']);
		$ship2street1 = addslashes($data[$root]['buyer-shipping-address']['address1']['VALUE']);
		$ship2street2 = addslashes($data[$root]['buyer-shipping-address']['address2']['VALUE']);
		$ship2city = addslashes($data[$root]['buyer-shipping-address']['city']['VALUE']);
		$ship2state = addslashes($data[$root]['buyer-shipping-address']['region']['VALUE']);
		$ship2zip = addslashes($data[$root]['buyer-shipping-address']['postal-code']['VALUE']);
		
		$csmid = 2; // $_SESSION["METHOD"];
		
		$shipping = addslashes($data[$root]['order-adjustment']['shipping']['flat-rate-shipping-adjustment']['shipping-name']['VALUE']);
		$ship_cost = addslashes($data[$root]['order-adjustment']['shipping']['flat-rate-shipping-adjustment']['shipping-cost']['VALUE']);
		$method = $shipping;	
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$tstamp = time();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$subtotal = $data[$root]['order-total']['VALUE'];
		$tax = $data[$root]['order-adjustment']['total-tax']['VALUE'];
		
		$total = $subtotal + $tax + $ship_cost;
		
		$ip_addr = "GOOGLE_CHECKOUT";
		
		$query = "insert into customer_orders(acctid, invoiceid, orderdate, status, 
			bill2name, bill2street1, bill2street2, bill2city, bill2state, bill2zip,
			ship2name, ship2street1, ship2street2, ship2city, ship2state, ship2zip,
			weight, subtotal, tax, shipping, total, ip_addr, 
			phone, altphone, emailaddr, csmid, csmethod, divid, otype) ";
		$query .= " values('$acctid', '', '$tstamp', '0',
			'$bill2name', '$bill2street1', '$bill2street2', '$bill2city', '$bill2state', '$bill2zip',
			'$ship2name', '$ship2street1', '$ship2street2', '$ship2city', '$ship2state', '$ship2zip',
			'$weight', '$subtotal', '$tax', '$shipping', '$total', '$ip_addr',
			'$phone', '$altphone', '$emailaddr', '$csmid', '$method', 0, 'INVOICE')";
			
		$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
		if($queryok == false){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		
		$orderid = mysql_insert_id();
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////
		$query = "select step from company_orderflow order by step limit 0, 1";// get the first step from the flow
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($step) = mysql_fetch_array($qok);
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$google_order_number = $data[$root]['google-order-number']['VALUE'];
		
		$prefix = $efc->getConfigValue("General Settings", "0_OrderPrefixWeb");

		if($prefix == "")
			$efc->getConfigValue("General Settings", "OrderPrefixWeb");

		$save_orderid = $prefix . date("y", time()) . $orderid;
		
		$query = "update customer_orders set status = '$step', invoiceid = '$save_orderid', ginvoiceid = '$google_order_number' where orderid = '$orderid' ";
		$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
		if($queryok == false){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$tstamp = date("Y-m-d H:i:s", time());
		$query = "insert into update_pool(type, updateid, tstamp) values('2', '$orderid', '$tstamp')";
		if(!$qxok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	  
	  	$items = $data[$root]['shopping-cart']['items'];
		

	  	$numitems = 0;
  		for($xx = 0; $xx < count($items['item']); $xx++)
  		{
			$query = "select product_manufacturers.manname, product_list.manid, 
						product_list.manpart, product_list.descr, product_list.shipweight 
						from
						product_list, product_manufacturers 
						where 
						product_list.manid = product_manufacturers.manid
						and product_list.id = '".$items['item'][$xx]['item-name']['VALUE']."'";
			if(!$qxok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($manname, $manid, $manpart, $descr, $weight) =mysql_fetch_array($qxok);
				     
			$id 	= $items['item'][$xx]['item-name']['VALUE'];
			$price 	= $items['item'][$xx]['unit-price']['VALUE'];
			$qty 	= $items['item'][$xx]['quantity']['VALUE'];
			
			$manname = addslashes($manname);
			$descr= addslashes($descr);
			$manpart = addslashes($manpart);
			
			if(intval($id) > 0)
			{
				$query = "insert into customer_order_lines(orderid, prodid, manid, manname, manpart, descr, price, qty_ordered, weight) ";
				$query .= "values('$orderid', '$id', '$manid', '$manname', '$manpart', '$descr', '$price', '$qty', '$weight')";
				$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
				if($queryok == false){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				
				$numitems++;
			}
	  	}

	  	// fail-safe for single item orders
	  	if($numitems <= 0)
	  	{
	 		foreach($items as $item) 
			{
				
				$query = "select product_manufacturers.manname, product_list.manid, 
							product_list.manpart, product_list.descr, product_list.shipweight 
							from
							product_list, product_manufacturers 
							where 
							product_list.manid = product_manufacturers.manid
							and product_list.id = '".$item['item-name']['VALUE']."'";
				if(!$qxok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				list($manname, $manid, $manpart, $descr, $weight) =mysql_fetch_array($qxok);
				          
				$id 	= $item['item-name']['VALUE'];
				$price 	= $item['unit-price']['VALUE'];
				$qty 	= $item['quantity']['VALUE'];
				$manname = addslashes($manname);
				$descr= addslashes($descr);
				$manpart = addslashes($manpart);
				
				$query = "insert into customer_order_lines(orderid, prodid, manid, manname, manpart, descr, price, qty_ordered, weight) ";
				$query .= "values('$orderid', '$id', '$manid', '$manname', '$manpart', '$descr', '$price', '$qty', '$weight')";
				$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid);
				if($queryok == false){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
	  	}
	  	
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Google Checkout..... mark it
		$tstamp = date("Y-m-d H:i:s", time());
		$query = "insert into customer_payments(orderid, tstamp, avscode, txid, approvalcode, reference, 
				amount, eci, status, cvvcode, pname, firstdigit, lastfour, txtype) ";
		$query .= "values('$orderid', '$tstamp', '-', '-', '-', '-',
				'$total', '', '0', '', 'Google Checkout', 'X', 'XXXX', 'REVIEWING')";	
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
     	$Gresponse->SendAck();
      break;
    }
    case "order-state-change-notification": 
    {
		
		$new_financial_state = $data[$root]['new-financial-order-state']['VALUE'];
		$new_fulfillment_order = $data[$root]['new-fulfillment-order-state']['VALUE'];

		$google_order_number = $data[$root]['google-order-number']['VALUE'];
		
		$query = "select orderid from customer_orders where ginvoiceid = '$google_order_number'";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($orderid) = mysql_fetch_array($qok);

		$query = "update customer_payments set txtype = '$new_financial_state' where orderid = '$orderid'";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		
		switch($new_financial_state) 
		{
			// first see if there's a link in customer payments
			//$query = "select orderid from customer_payments"
        	case 'REVIEWING': 
        	{
          		break;
        	}
        	case 'CHARGEABLE': 
        	{
          		$Grequest->SendProcessOrder($data[$root]['google-order-number']['VALUE']);
          		$Grequest->SendChargeOrder($data[$root]['google-order-number']['VALUE'],'');
          		
          		break;
        	}
        	case 'CHARGING': 
        	{
          		break;
			}
        	case 'CHARGED': 
        	{
        		$query = "update customer_payments set status = '1', txtype = 'CAPT' where orderid = '$orderid'";
				if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		
				$new_status = $efc->getConfigValue("Google_Checkout", "STATUS_CHARGED");  // Your Merchant ID
				
				$query = "update customer_orders set status = '$new_status' where orderid = '$orderid'";
				if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				
          		break;
        	}
        	case 'PAYMENT_DECLINED': 
        	{
				$new_status = $efc->getConfigValue("Google_Checkout", "STATUS_DECLINED");  // Your Merchant ID
				
				$query = "update customer_orders set status = '$new_status' where orderid = '$orderid'";
				if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
        		
				break;
			}
			case 'CANCELLED': 
			{
				break;
			}
			case 'CANCELLED_BY_GOOGLE': 
			{
          		//$Grequest->SendBuyerMessage($data[$root]['google-order-number']['VALUE'],
          		//    "Sorry, your order is cancelled by Google", true);
          		break;
        	}
        	default:
          		break;
      	}

      	$efc->UpdatePool("2", $orderid);
      	
      	switch($new_fulfillment_order) 
      	{
        	case 'NEW': 
        	{
				break;
			}	
			case 'PROCESSING': 
			{
				break;
			}
			case 'DELIVERED': 
			{
				break;
			}
        	case 'WILL_NOT_DELIVER': 
        	{
				break;
			}
			default:
          		break;
		}
		
		$Gresponse->SendAck();
		
		break;
    }
    case "charge-amount-notification": 
    {
		//$Grequest->SendDeliverOrder($data[$root]['google-order-number']['VALUE'],
		//    <carrier>, <tracking-number>, <send-email>);
		//$Grequest->SendArchiveOrder($data[$root]['google-order-number']['VALUE'] );
		$Gresponse->SendAck();
		break;
    }
    case "chargeback-amount-notification": 
    {
		$Gresponse->SendAck();
		break;
    }
    case "refund-amount-notification": 
    {
		$Gresponse->SendAck();
		break;
    }
    case "risk-information-notification": 
    {
    	$google_order_number = $data[$root]['google-order-number']['VALUE'];
    	$ip_addr = $data[$root]['risk-information']['ip-address']['VALUE'];
    	$avscode = $data[$root]['risk-information']['avs-response']['VALUE'];
    	$cvvcode = $data[$root]['risk-information']['cvn-response']['VALUE'];
    	$eligible = $data[$root]['risk-information']['eligible-for-protection']['VALUE'];

    	$query = "update customer_orders set ip_addr = '$ip_addr' where ginvoiceid = '$google_order_number'";
    	if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
    	
    	$query = "select orderid from customer_orders where ginvoiceid = '$google_order_number'";
    	if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
    	list($orderid) = mysql_fetch_array($qok);
    	
    	//echo "ORDERNUM: $orderid RISK_INFO: AVS: $avscode CVVCODE: $cvvcode LASTFOUR: $lastfour<BR>";
    	
    	$query = "update customer_payments set avscode = '$avscode', cvvcode = '$cvvcode', 
    				approvalcode = 'ELIGIBLE: $eligible', reference = 'ELIGIBLE: $eligible', lastfour = '$lastfour' where orderid = '$orderid'";
    	if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
    	
    	
    	$efc->UpdatePool("2", $orderid);
    	
		$Gresponse->SendAck();
		break;
	}
    default:
		$Gresponse->SendBadRequestStatus("Invalid or not supported Message");
		break;
}

 	
?>