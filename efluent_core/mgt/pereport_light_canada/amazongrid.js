function showPriceLevels(pid) {
    $('#all_pricelevels_'+pid).toggle();
}
function showMenu(productID, manpart)
{
    var pos = $('#item_'+productID).position();
    var dlg = $("#dialog");
    dlg.show();
    dlg.css('left',pos.left+20);
    dlg.css('top',pos.top);
   
    $("#menuOption1").html("<a href=\"javascript:e2window('/mgt/sm/viewsku.phtml?id="+productID+"&inwin=1', 'View SKU: "+productID+"','item_"+productID+"');\">View SKU</a>");
    $("#menuOption2").html("<a href=\"javascript:e2window('/mgt/sm/editsku.phtml?id="+productID+"&inwin=1', 'Edit SKU: "+productID+"','item_"+productID+"');\">Edit SKU</a>");
    $("#menuOption3").html("<a href=\"javascript:void(0);\" onclick=\"javascript:addItem2PO('"+productID+"')\">Add to PO</a>");
    $("#menuOption4").html("<a href=\"javascript:void(0);\" onclick=\"javascript:addItem2ISO('"+productID+"')\">Add to ISO</a>");
}
function hideMenu()
{
	var dlg = document.getElementById("dialog");
	dlg.style.display = "none";
}

function addItem2PO(productID)
{
    $.get('/mgt/sm/ajax_additem2po.phtml',
        { 
                'pid': productID, 
                'sid': Math.random()
        }, function(data) {
            var productimage = $("#productimage");
            productimage.html("<center><img src=\"/getimage.phtml?id="+productID+"&itype=0&thumb=1\" border=0 width=50></center>");
            productimage.show();
            productimage.css('left',posX);
            productimage.css('top',posY);
            paX = posX;
            paY = posY;
            posArray = findPos($("#orderdisplay"));
            dX = posArray[0];
            
            $("#podisplay").html(data);
            $("#podisplay").css('backgroundColor','#ffcccc');
            setTimeout("unColorPODisplay()", 2000);

            $("#poimage").hide();
            setTimeout("animateCart()", 0);
    });

    return false;
}

function unColorPODisplay()
{
	$("#podisplay").css('backgroundColor','#ffffff');
}
function addItem2ISO(productID)
{
	xml7Http=GetXmlHttpObject();

	var url="/mgt/sm/ajax_additem2iso.phtml?pid="+productID+"&sid="+Math.random();
	xml7Http.onreadystatechange=isoResult;
	xml7Http.open("GET",url,true);
	xml7Http.send(null);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// animation stuff
	var productimage = document.getElementById("productimage");

	productimage.innerHTML = "<center><img src=\"/getimage.phtml?id="+productID+"&itype=0&thumb=1\" border=0 width=50></center>";

	productimage.style.display = "block";

	productimage.style.left = posX;
	productimage.style.top = posY;

	paX = posX;
	paY = posY;
	posArray = findPos(document.getElementById("isodisplay"));
	dX = posArray[0];

	setTimeout("animateCart()", 0);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return false;
}

function isoResult()
{
	if(xml7Http.readyState==4)
	{
		document.getElementById("isodisplay").innerHTML = xml7Http.responseText;

		document.getElementById("isodisplay").style.backgroundColor = "#ffcccc";
		setTimeout("unColorISODisplay()", 2000);

		document.getElementById("isoimage").style.display = "";
	}
}
function unColorISODisplay()
{
	document.getElementById("isodisplay").style.backgroundColor = "#ffffff";
}

function findPos(obj)
{
    var curleft = curtop = 0;

    if (obj.offsetParent)
    {
            do
            {
                    curleft += obj.offsetLeft;
                    curtop += obj.offsetTop;
            }
            while (obj = obj.offsetParent);
    }

    return [curleft,curtop];
}
function animateCart()
{
    if(paX < dX)
    paX = paX + 20;

    paY = paY - 20;

    var productimage = $("#productimage");
    productimage.css('left',paX);
    productimage.css('top',paY);
    if(paY > -75){
        setTimeout("animateCart()", 0);
    }
    else{
        productimage.hide();
    }
}
function stockDetail(pid, type)
{
    e2window("/mgt/sm/stockdetail.phtml?pid="+pid+"&type="+type+"&inwin=1", "Stock Detail "+pid,"stock_"+type+"_"+pid);
}
function saveAmazonPct(pid) {
    $('#amzcalctable_'+pid).css('background-color','#ffcccc');
    $('#fbacalctable_'+pid).css('background-color','#ffcccc');
    $('#amz'+pid+'_amazonpct').html($('#amazonpct_perc_'+pid).val());
    $.get('ajax_savecustom_field.phtml',
        { action: "save_amazonpct", 
            'id': pid,
            'pct': $('#amazonpct_perc_'+pid).val()
        }
        , function(data) {
            updateAMZTable(data);
            updateFBATable(data);
    });
}

function updateAMZTable(pid)
{
    var newprice = parseFloat($('#amz'+pid+'_price').html()) ;
    var cost = parseFloat( $('#amz'+pid+'_cost').html()) ;
    var shipping = parseFloat( $('#amz'+pid+'_ship').html()) ;
    var pct1 = parseFloat($('#amz'+pid+'_amazonpct').html());
    var pct = pct1 / 100;
    
//    $('#amz'+pid+'_amazonpct').html($('#amazonpct_perc_'+pid).val());
    var amzfee = newprice * pct;
    $("#amz"+pid+"_amzfee").html(amzfee.toFixed(2));	
    /////////////////////////////////////////////////////////////////////////////////////////
    var profit = newprice - cost - shipping - amzfee;
    
    if(profit >= 0)
        $("#amz"+pid+"_profit").html("<B><font color='#00ff00'>$"+profit.toFixed(2)+"</font></B>");
    else
        $("#amz"+pid+"_profit").html("<B><font color='#ff0000'>$"+profit.toFixed(2)+"</font></B>");


    var margin = 100 * (profit / newprice);

    if(margin >= 0)
        $("#amz"+pid+"_margin").html("<B><font color='#00ff00'>"+margin.toFixed(2)+"%</font></B>");
    else
        $("#amz"+pid+"_margin").html("<B><font color='#ff0000'>"+margin.toFixed(2)+"%</font></B>");
    $('#amzcalctable_'+pid).css('background-color','#ffffff');
//    $("#amz"+pid+"_profit2").html("$"+profit.toFixed(2));
}

function updateFBATable(pid)
{
    var amzover = 0.00;
    if($("fba"+pid+"-amzoversize").length>0) {
        amzover = parseFloat($("fba"+pid+"-amzoversize").html());
    }
    var newprice = parseFloat($('#fba'+pid+'_price').html()) ;
    var cost = parseFloat( $('#fba'+pid+'_cost').html()) ;
    
    var weight = Math.ceil(parseFloat($('#weight-'+pid).val()));
    var fullfee = 0.00;					
    if(newprice <= 299.99)
    {
        fullfee += 1.00;
    }
    else if(newprice > 300)
    {
        fullfee = 0;
    }

    fullfee += parseFloat(weight * 0.37);
    fullfee += 1; // per order fee
    if( parseInt( $('#oversized_'+pid).val() ) > 0 )
    {
        fullfee += 5;
    }	
    fullfee = fullfee.toFixed(2);
    $("#fba"+pid+"-fullfilment").html(fullfee);
    ////////////////////////////////////////////////////////////////////////////////////////

    var pct1 = parseFloat($('#amz'+pid+'_amazonpct').html());
    var pct = pct1 / 100;

    amazonpct = newprice * pct;
    amazonpct = amazonpct.toFixed(2);

    $("#fba"+pid+"-amzfee").html(amazonpct);
    var amzoversize = 0;
    if($('#fba'+pid+'-amzoversize').length>0) {
        amzoversize = parseFloat($('#fba'+pid+'-amzoversize').html());
    }
    /////////////////////////////////////////////////////////////////////////////////////////
    var profit = newprice - cost - fullfee - amazonpct-amzover-amzoversize;

    if(profit >= 0)
        $("#fba"+pid+"-profit").html("<B><font color='#00ff00'>$"+profit.toFixed(2)+"</font></B>");
    else
        $("#fba"+pid+"-profit").html("<B><font color='#ff0000'>$"+profit.toFixed(2)+"</font></B>");


    var margin = 100 * (profit / newprice);

    if(margin >= 0)
        $("#fba"+pid+"-margin").html("<B><font color='#00ff00'>"+margin.toFixed(2)+"%</font></B>");
    else
        $("#fba"+pid+"-margin").html("<B><font color='#ff0000'>"+margin.toFixed(2)+"%</font></B>");


//    document.getElementById("fba"+pid+"-profit2").innerHTML = "$"+profit.toFixed(2);
    $('#fbacalctable_'+pid).css('background-color','#ffffff');
}

function showFBAOrdersShipped(pid) {
	if($('#fbaordersshipped-'+pid).html() == '') {
		$.get('ajax_getfbaorders_shipped.phtml',{ 'pid': pid}, function(data) {
			$('#fbaordersshipped-'+pid).html(data);
			$('#fbaordersshipped-'+pid).toggle();
			if($('#fbaordersimgshipped-'+pid).attr('src') == '/mgt/images/minus.gif') {
				$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/plus.gif')
			}
			else {
				$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/minus.gif')
			}
		});
	}
	else {
		$('#fbaordersshipped-'+pid).toggle();
		if($('#fbaordersimgshipped-'+pid).attr('src') == '/mgt/images/minus.gif') {
			$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/plus.gif')
		}
		else {
			$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/minus.gif')
		}
	}
}
function showShippedOrders(pid,origin,kit) {
    if($('#shippedorders-'+pid+'-'+origin).html() == '') {
        $.get('ajax_getshippedorders.phtml',{ 'pid': pid,'origin':origin,'kit':kit}, function(data) {
            $('#shippedorders-'+pid+'-'+origin).html(data);
            $('#shippedorders-'+pid+'-'+origin).toggle();
            if($('#shippedordersimg-'+pid+'-'+origin).attr('src') == '/mgt/images/minus.gif') {
                $('#shippedordersimg-'+pid+'-'+origin).attr('src','/mgt/images/plus.gif')
            }
            else {
                $('#shippedordersimg-'+pid+'-'+origin).attr('src','/mgt/images/minus.gif')
            }
        });
    }
    else {
        $('#shippedorders-'+pid+'-'+origin).toggle();
        if($('#shippedordersimg-'+pid+'-'+origin).attr('src') == '/mgt/images/minus.gif') {
            $('#shippedordersimg-'+pid+'-'+origin).attr('src','/mgt/images/plus.gif')
        }
        else {
            $('#shippedordersimg-'+pid+'-'+origin).attr('src','/mgt/images/minus.gif')
        }
    }
}
function showFBAOrders(pid)
{
	if(parseInt(document.getElementById("fbaordersshowing-"+pid).value) == 0)
	{
		document.getElementById("fbaordersshowing-"+pid).value = "1"
		document.getElementById("fbaordersimg-"+pid).src = "/mgt/images/minus.gif";
		document.getElementById("fbaorders-"+pid).innerHTML = "<img src='/mgt/images/loading.gif'>";
		
		/////////////////////////////////////////////////
		xml9Http=GetXmlHttpObject();
			
		var url = "ajax_getfbaorders.phtml?pid="+pid+"&sid="+Math.random();
		
		xml9Http.onreadystatechange=gotFBAOrders; // throw away the result..... the pool will catch the price change
		xml9Http.open("GET",url,true);
		xml9Http.send(null);	
	}
	else
	{
		document.getElementById("fbaordersshowing-"+pid).value = "0"
		document.getElementById("fbaorders-"+pid).innerHTML = "&nbsp;";
	}
	
}

function gotFBAOrders()
{
    if(xml9Http.readyState==4)
    { 
        var data = xml9Http.responseText.split("~");
        document.getElementById("fbaorders-"+data[0]).innerHTML = data[1];
    }
}
function saveSuperPricerOld(pid) {
    
    $('#minprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#maxprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#mapprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#profile_amazon_'+pid).css('background-color','#ffeeee');
    $('#group_amazon_'+pid).css('background-color','#ffeeee');
    $('#managed_amazon_'+pid).css('background-color','#ffeeee');
    
    $('#minprice_ebay_'+pid).css('background-color','#ffeeee');
    $('#maxprice_ebay_'+pid).css('background-color','#ffeeee');
    $('#mapprice_ebay_'+pid).css('background-color','#ffeeee');
    $('#profile_ebay_'+pid).css('background-color','#ffeeee');
    $('#group_ebay_'+pid).css('background-color','#ffeeee');
    $('#managed_ebay_'+pid).css('background-color','#ffeeee');
    
    $('#minprice_buy_'+pid).css('background-color','#ffeeee');
    $('#maxprice_buy_'+pid).css('background-color','#ffeeee');
    $('#mapprice_buy_'+pid).css('background-color','#ffeeee');
    $('#profile_buy_'+pid).css('background-color','#ffeeee');
    $('#group_buy_'+pid).css('background-color','#ffeeee');
    $('#managed_buy_'+pid).css('background-color','#ffeeee');
    
    
    
    var minprice_amazon = $('#minprice_amazon_'+pid).val();
    var maxprice_amazon = $('#maxprice_amazon_'+pid).val();
    var mapprice_amazon = $('#mapprice_amazon_'+pid).val();
    var profile_amazon = $('#profile_amazon_'+pid).val();
    var group_amazon = $('#group_amazon_'+pid).val();
    if($('#managed_amazon_'+pid).is(':checked')) {
        managed_amazon = 1;
    }
    else {
        managed_amazon = 0;
    }
    var minprice_ebay = $('#minprice_ebay_'+pid).val();
    var maxprice_ebay = $('#maxprice_ebay_'+pid).val();
    var mapprice_ebay = $('#mapprice_ebay_'+pid).val();
    var profile_ebay = $('#profile_ebay_'+pid).val();
    var group_ebay = $('#group_ebay_'+pid).val();
    if($('#managed_ebay_'+pid).is(':checked')) {
        managed_ebay = 1;
    }
    else {
        managed_ebay = 0;
    }
    
    var minprice_buy = $('#minprice_buy_'+pid).val();
    var maxprice_buy = $('#maxprice_buy_'+pid).val();
    var mapprice_buy = $('#mapprice_buy_'+pid).val();
    var profile_buy = $('#profile_buy_'+pid).val();
    var group_buy = $('#group_buy_'+pid).val();
    if($('#managed_buy_'+pid).is(':checked')) {
        managed_buy = 1;
    }
    else {
        managed_buy = 0;
    }
    $.post('/mgt/sm/ajax_save_superpricer.phtml', {
                'skus': pid,
                'minprice_amazon': minprice_amazon,
                'maxprice_amazon': maxprice_amazon,
                'mapprice_amazon': mapprice_amazon,
                'profile_amazon': profile_amazon,
                'group_amazon': group_amazon,
                'managed_amazon': managed_amazon,
                'minprice_ebay': minprice_ebay,
                'maxprice_ebay': maxprice_ebay,
                'mapprice_ebay': mapprice_ebay,
                'profile_ebay': profile_ebay,
                'group_ebay': group_ebay,
                'managed_ebay': managed_ebay,
                'minprice_buy': minprice_buy,
                'maxprice_buy': maxprice_buy,
                'mapprice_buy': mapprice_buy,
                'profile_buy': profile_buy,
                'group_buy': group_buy,
                'managed_buy': managed_buy
            }, function(res) {
                $('#minprice_amazon_'+pid).css('background-color','#ffffff');
                $('#maxprice_amazon_'+pid).css('background-color','#ffffff');
                $('#mapprice_amazon_'+pid).css('background-color','#ffffff');
                $('#profile_amazon_'+pid).css('background-color','#ffffff');
                $('#group_amazon_'+pid).css('background-color','#ffffff');
                $('#managed_amazon_'+pid).css('background-color','#ffffff');

                $('#minprice_ebay_'+pid).css('background-color','#ffffff');
                $('#maxprice_ebay_'+pid).css('background-color','#ffffff');
                $('#mapprice_ebay_'+pid).css('background-color','#ffffff');
                $('#profile_ebay_'+pid).css('background-color','#ffffff');
                $('#group_ebay_'+pid).css('background-color','#ffffff');
                $('#managed_ebay_'+pid).css('background-color','#ffffff');

                $('#minprice_buy_'+pid).css('background-color','#ffffff');
                $('#maxprice_buy_'+pid).css('background-color','#ffffff');
                $('#mapprice_buy_'+pid).css('background-color','#ffffff');
                $('#profile_buy_'+pid).css('background-color','#ffffff');
                $('#group_buy_'+pid).css('background-color','#ffffff');
                $('#managed_buy_'+pid).css('background-color','#ffffff');
        });
}
function saveSuperPricer(pid) {
    
    $('#minprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#minprice_amazon_canada_'+pid).css('background-color','#ffeeee');
    $('#maxprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#maxprice_amazon_canada_'+pid).css('background-color','#ffeeee');
    $('#mapprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#mapprice_amazon_canada_'+pid).css('background-color','#ffeeee');
    $('#profile_amazon_'+pid).css('background-color','#ffeeee');
    $('#profile_amazon_canada_'+pid).css('background-color','#ffeeee');
    $('#group_amazon_'+pid).css('background-color','#ffeeee');
    $('#group_amazon_canada_'+pid).css('background-color','#ffeeee');
    $('#managed_amazon_'+pid).css('background-color','#ffeeee');
    $('#managed_amazon_canada_'+pid).css('background-color','#ffeeee');
    
    var minprice_amazon = $('#minprice_amazon_'+pid).val();
    var maxprice_amazon = $('#maxprice_amazon_'+pid).val();
    var mapprice_amazon = $('#mapprice_amazon_'+pid).val();
    var profile_amazon = $('#profile_amazon_'+pid).val();
    var group_amazon = $('#group_amazon_'+pid).val();
    if($('#managed_amazon_'+pid).is(':checked')) {
        managed_amazon = 1;
    }
    else {
        managed_amazon = 0;
    }
    
    var minprice_amazon_canada = $('#minprice_amazon_canada_'+pid).val();
    var maxprice_amazon_canada = $('#maxprice_amazon_canada_'+pid).val();
    var mapprice_amazon_canada = $('#mapprice_amazon_canada_'+pid).val();
    var profile_amazon_canada = $('#profile_amazon_canada_'+pid).val();
    var group_amazon_canada = $('#group_amazon_canada_'+pid).val();
    if($('#managed_amazon_canada_'+pid).is(':checked')) {
        managed_amazon_canada = 1;
    }
    else {
        managed_amazon_canada = 0;
    }
    
    $.post('/mgt/sm/ajax_save_superpricer.phtml', {
                'skus': pid,
                'minprice_amazon': minprice_amazon,
                'maxprice_amazon': maxprice_amazon,
                'mapprice_amazon': mapprice_amazon,
                'profile_amazon': profile_amazon,
                'group_amazon': group_amazon,
                'managed_amazon': managed_amazon,
                'minprice_amazon_canada': minprice_amazon_canada,
                'maxprice_amazon_canada': maxprice_amazon_canada,
                'mapprice_amazon_canada': mapprice_amazon_canada,
                'profile_amazon_canada': profile_amazon_canada,
                'group_amazon_canada': group_amazon_canada,
                'managed_amazon_canada': managed_amazon_canada
            }, function(res) {
                $('#minprice_amazon_'+pid).css('background-color','#ffffff');
                $('#maxprice_amazon_'+pid).css('background-color','#ffffff');
                $('#mapprice_amazon_'+pid).css('background-color','#ffffff');
                $('#profile_amazon_'+pid).css('background-color','#ffffff');
                $('#group_amazon_'+pid).css('background-color','#ffffff');
                $('#managed_amazon_'+pid).css('background-color','#ffffff');
                
                $('#minprice_amazon_canada_'+pid).css('background-color','#ffffff');
                $('#maxprice_amazon_canada_'+pid).css('background-color','#ffffff');
                $('#mapprice_amazon_canada_'+pid).css('background-color','#ffffff');
                $('#profile_amazon_canada_'+pid).css('background-color','#ffffff');
                $('#group_amazon_canada_'+pid).css('background-color','#ffffff');
                $('#managed_amazon_canada_'+pid).css('background-color','#ffffff');
                
        });
}
function saveSuperPricerAll(pid) {
    var password;
    var pass1="superpricer";
    password = prompt('Are you sure you want to update all!', ' ');
    if (password == pass1) {
        var prodCount = parseFloat($('#prodCount').html());
        var prodTotal = parseFloat($('#prodTotal').html());
        if(prodCount != prodTotal) {
            alert('Report not ready!');
        }
        else {
            saveSuperPricerAllCall(0,pid);
        }
    } else {
        alert('Wrong password!');
    }    
}
function saveSuperPricerAllCall(nr,productID) {
    
    if(nr<pidLength) {
        var pid = pidArr[nr];
        $('#minprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#maxprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#mapprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#profile_amazon_'+pid).css('background-color','#ffeeee');
        $('#group_amazon_'+pid).css('background-color','#ffeeee');
        $('#managed_amazon_'+pid).css('background-color','#ffeeee');
        
        $('#minprice_amazon_canada_'+pid).css('background-color','#ffeeee');
        $('#maxprice_amazon_canada_'+pid).css('background-color','#ffeeee');
        $('#mapprice_amazon_canada_'+pid).css('background-color','#ffeeee');
        $('#profile_amazon_canada_'+pid).css('background-color','#ffeeee');
        $('#group_amazon_canada_'+pid).css('background-color','#ffeeee');
        $('#managed_amazon_canada_'+pid).css('background-color','#ffeeee');
        
        
        var minprice_amazon = $('#minprice_amazon_'+productID).val();
        var maxprice_amazon = $('#maxprice_amazon_'+productID).val();
        var mapprice_amazon = $('#mapprice_amazon_'+productID).val();
        var profile_amazon = $('#profile_amazon_'+productID).val();
        var group_amazon = $('#group_amazon_'+productID).val();
        if($('#managed_amazon_'+productID).is(':checked')) {
            managed_amazon = 1;
        }
        else {
            managed_amazon = 0;
        }
        var minprice_amazon_canada = $('#minprice_amazon_canada_'+productID).val();
        var maxprice_amazon_canada = $('#maxprice_amazon_canada_'+productID).val();
        var mapprice_amazon_canada = $('#mapprice_amazon_canada_'+productID).val();
        var profile_amazon_canada = $('#profile_amazon_canada_'+productID).val();
        var group_amazon_canada = $('#group_amazon_canada_'+productID).val();
        if($('#managed_amazon_canada_'+productID).is(':checked')) {
            managed_amazon_canada = 1;
        }
        else {
            managed_amazon_canada = 0;
        }
       
        $.post('/mgt/sm/ajax_save_superpricer.phtml', {
                    'skus': pid,
                    'minprice_amazon': minprice_amazon,
                    'maxprice_amazon': maxprice_amazon,
                    'mapprice_amazon': mapprice_amazon,
                    'profile_amazon': profile_amazon,
                    'group_amazon': group_amazon,
                    'managed_amazon': managed_amazon,
                    'minprice_amazon_canada': minprice_amazon_canada,
                    'maxprice_amazon_canada': maxprice_amazon_canada,
                    'mapprice_amazon_canada': mapprice_amazon_canada,
                    'profile_amazon_canada': profile_amazon_canada,
                    'group_amazon_canada': group_amazon_canada,
                    'managed_amazon_canada': managed_amazon_canada,
                    
                }, function(res) {
                    
                    $('#minprice_amazon_'+pid).val(minprice_amazon);
                    $('#maxprice_amazon_'+pid).val(maxprice_amazon);
                    $('#mapprice_amazon_'+pid).val(mapprice_amazon);
                    $('#profile_amazon_'+pid+' option[value='+profile_amazon+'] ').attr("selected", true);
                    $('#group_amazon_'+pid+' option[value='+group_amazon+'] ').attr("selected", true);
                    if(managed_amazon == 1) {
                       $('#managed_amazon_'+pid).attr('checked',true);
                    }
                    else {
                       $('#managed_amazon_'+pid).attr('checked',false);
                    }  
                    
                    $('#minprice_amazon_canada_'+pid).val(minprice_amazon_canada);
                    $('#maxprice_amazon_canada_'+pid).val(maxprice_amazon_canada);
                    $('#mapprice_amazon_canada_'+pid).val(mapprice_amazon_canada);
                    $('#profile_amazon_canada_'+pid+' option[value='+profile_amazon_canada+'] ').attr("selected", true);
                    $('#group_amazon_canada_'+pid+' option[value='+group_amazon_canada+'] ').attr("selected", true);
                    if(managed_amazon_canada == 1) {
                       $('#managed_amazon_canada_'+pid).attr('checked',true);
                    }
                    else {
                       $('#managed_amazon_canada_'+pid).attr('checked',false);
                    }

                    
                    $('#minprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#maxprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#mapprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#profile_amazon_'+pid).css('background-color','#ffffff');
                    $('#group_amazon_'+pid).css('background-color','#ffffff');
                    $('#managed_amazon_'+pid).css('background-color','#ffffff');
                    
                    $('#minprice_amazon_canada_'+pid).css('background-color','#ffffff');
                    $('#maxprice_amazon_canada_'+pid).css('background-color','#ffffff');
                    $('#mapprice_amazon_canada_'+pid).css('background-color','#ffffff');
                    $('#profile_amazon_canada_'+pid).css('background-color','#ffffff');
                    $('#group_amazon_canada_'+pid).css('background-color','#ffffff');
                    $('#managed_amazon_canada_'+pid).css('background-color','#ffffff');
                    
                    saveSuperPricerAllCall(nr+1,productID);
            });
    }
}
function saveSuperPricerAllCallOld(nr,productID) {
    
    if(nr<pidLength) {
        var pid = pidArr[nr];
        $('#minprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#maxprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#mapprice_amazon_'+pid).css('background-color','#ffeeee');
        $('#profile_amazon_'+pid).css('background-color','#ffeeee');
        $('#group_amazon_'+pid).css('background-color','#ffeeee');
        $('#managed_amazon_'+pid).css('background-color','#ffeeee');

        $('#minprice_ebay_'+pid).css('background-color','#ffeeee');
        $('#maxprice_ebay_'+pid).css('background-color','#ffeeee');
        $('#mapprice_ebay_'+pid).css('background-color','#ffeeee');
        $('#profile_ebay_'+pid).css('background-color','#ffeeee');
        $('#group_ebay_'+pid).css('background-color','#ffeeee');
        $('#managed_ebay_'+pid).css('background-color','#ffeeee');

        $('#minprice_buy_'+pid).css('background-color','#ffeeee');
        $('#maxprice_buy_'+pid).css('background-color','#ffeeee');
        $('#mapprice_buy_'+pid).css('background-color','#ffeeee');
        $('#profile_buy_'+pid).css('background-color','#ffeeee');
        $('#group_buy_'+pid).css('background-color','#ffeeee');
        $('#managed_buy_'+pid).css('background-color','#ffeeee');
        
        
        var minprice_amazon = $('#minprice_amazon_'+productID).val();
        var maxprice_amazon = $('#maxprice_amazon_'+productID).val();
        var mapprice_amazon = $('#mapprice_amazon_'+productID).val();
        var profile_amazon = $('#profile_amazon_'+productID).val();
        var group_amazon = $('#group_amazon_'+productID).val();
        if($('#managed_amazon_'+productID).is(':checked')) {
            managed_amazon = 1;
        }
        else {
            managed_amazon = 0;
        }
        var minprice_ebay = $('#minprice_ebay_'+productID).val();
        var maxprice_ebay = $('#maxprice_ebay_'+productID).val();
        var mapprice_ebay = $('#mapprice_ebay_'+productID).val();
        var profile_ebay = $('#profile_ebay_'+productID).val();
        var group_ebay = $('#group_ebay_'+productID).val();
        if($('#managed_ebay_'+productID).is(':checked')) {
            managed_ebay = 1;
        }
        else {
            managed_ebay = 0;
        }

        var minprice_buy = $('#minprice_buy_'+productID).val();
        var maxprice_buy = $('#maxprice_buy_'+productID).val();
        var mapprice_buy = $('#mapprice_buy_'+productID).val();
        var profile_buy = $('#profile_buy_'+productID).val();
        var group_buy = $('#group_buy_'+productID).val();
        if($('#managed_buy_'+productID).is(':checked')) {
            managed_buy = 1;
        }
        else {
            managed_buy = 0;
        }
        $.post('/mgt/sm/ajax_save_superpricer.phtml', {
                    'skus': pid,
                    'minprice_amazon': minprice_amazon,
                    'maxprice_amazon': maxprice_amazon,
                    'mapprice_amazon': mapprice_amazon,
                    'profile_amazon': profile_amazon,
                    'group_amazon': group_amazon,
                    'managed_amazon': managed_amazon,
                    'minprice_ebay': minprice_ebay,
                    'maxprice_ebay': maxprice_ebay,
                    'mapprice_ebay': mapprice_ebay,
                    'profile_ebay': profile_ebay,
                    'group_ebay': group_ebay,
                    'managed_ebay': managed_ebay,
                    'minprice_buy': minprice_buy,
                    'maxprice_buy': maxprice_buy,
                    'mapprice_buy': mapprice_buy,
                    'profile_buy': profile_buy,
                    'group_buy': group_buy,
                    'managed_buy': managed_buy
                }, function(res) {
                    
                    $('#minprice_amazon_'+pid).val(minprice_amazon);
                    $('#maxprice_amazon_'+pid).val(maxprice_amazon);
                    $('#mapprice_amazon_'+pid).val(mapprice_amazon);
                    $('#profile_amazon_'+pid+' option[value='+profile_amazon+'] ').attr("selected", true);
                    $('#group_amazon_'+pid+' option[value='+group_amazon+'] ').attr("selected", true);
                    if(managed_amazon == 1) {
                       $('#managed_amazon_'+pid).attr('checked',true);
                    }
                    else {
                       $('#managed_amazon_'+pid).attr('checked',false);
                    }

                    $('#minprice_ebay_'+pid).val(minprice_ebay);
                    $('#maxprice_ebay_'+pid).val(maxprice_ebay);
                    $('#mapprice_ebay_'+pid).val(mapprice_ebay);
                    $('#profile_ebay_'+pid+' option[value='+profile_ebay+'] ').attr("selected", true);
                    $('#group_ebay_'+pid+' option[value='+group_ebay+'] ').attr("selected", true);
                    if(managed_ebay == 1) {
                       $('#managed_ebay_'+pid).attr('checked',true);
                    }
                    else {
                       $('#managed_ebay_'+pid).attr('checked',false);
                    }
                    
                    $('#minprice_buy_'+pid).val(minprice_buy);
                    $('#maxprice_buy_'+pid).val(maxprice_buy);
                    $('#mapprice_buy_'+pid).val(mapprice_buy);
                    $('#profile_buy_'+pid+' option[value='+profile_buy+'] ').attr("selected", true);
                    $('#group_buy_'+pid+' option[value='+group_buy+'] ').attr("selected", true);
                    if(managed_buy == 1) {
                       $('#managed_buy_'+pid).attr('checked',true);
                    }
                    else {
                       $('#managed_buy_'+pid).attr('checked',false);
                    }
                    
                    $('#minprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#maxprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#mapprice_amazon_'+pid).css('background-color','#ffffff');
                    $('#profile_amazon_'+pid).css('background-color','#ffffff');
                    $('#group_amazon_'+pid).css('background-color','#ffffff');
                    $('#managed_amazon_'+pid).css('background-color','#ffffff');

                    $('#minprice_ebay_'+pid).css('background-color','#ffffff');
                    $('#maxprice_ebay_'+pid).css('background-color','#ffffff');
                    $('#mapprice_ebay_'+pid).css('background-color','#ffffff');
                    $('#profile_ebay_'+pid).css('background-color','#ffffff');
                    $('#group_ebay_'+pid).css('background-color','#ffffff');
                    $('#managed_ebay_'+pid).css('background-color','#ffffff');

                    $('#minprice_buy_'+pid).css('background-color','#ffffff');
                    $('#maxprice_buy_'+pid).css('background-color','#ffffff');
                    $('#mapprice_buy_'+pid).css('background-color','#ffffff');
                    $('#profile_buy_'+pid).css('background-color','#ffffff');
                    $('#group_buy_'+pid).css('background-color','#ffffff');
                    $('#managed_buy_'+pid).css('background-color','#ffffff');
                    
                    saveSuperPricerAllCall(nr+1,productID);
            });
    }
}
function addItemToList(pid) {
    var listId = $('#lid_'+pid).val();
    $.get('ajax_get_lists.phtml',{ action: "add_item",'id': pid,'lid':listId}, function(data) {
            loadItemLists(pid);
    });
}
function loadItemLists(pid) {
    $.get('ajax_get_lists.phtml',{ action: "get_lists",'id': pid}, function(data) {
            $('#listDisplay_'+pid).html(data);
    });
}
function confirmDeleteList(msg,lid,id) {
    var answer = confirm(msg)
    if (answer)
    {
        $.get('ajax_get_lists.phtml',{ action: "DeleteList",'id': id,'lid':lid}, function(data) {
            loadItemLists(id);
        });
    }
}
function updateMinQty(pid) {
    var minqty = $('#minqty_'+pid).val();
    $.get('ajax_savecustom_field.phtml',{ 'action':'save_min_qty','id':pid,'minqty':minqty}, function(data) {

    });
}
function updateReorderQty(pid) {
    var targetqty = $('#reorderqty_'+pid).val();
    $.get('ajax_savecustom_field.phtml',{ 'action':'save_reorder_qty','id':pid,'targetqty':targetqty}, function(data) {

    });
}
function saveProdWeight(pid) {
    $('#fbacalctable_'+pid).css('background-color','#ffcccc');
    var weight = $('#weight-'+pid).val();
    $.get('ajax_savecustom_field.phtml',{ 'action':'save_weight','id':pid,'weight':weight}, function(data) {
        updateFBATable(pid);
    });
}





function updateFCO_all(pid) {
    if($('#all_over_c_pl-'+pid).is(':checked')) {
        var override = 1;
        $('.pl_checkbox'+pid).each(function(){
            if($(this).attr('id') != 'fprice-'+pid) {
                    $(this).attr('checked', true);
            }
        });
    }
    else{
        var override = 0;
        $('.pl_checkbox'+pid).each(function(){
            if($(this).attr('id') != 'fprice-'+pid) {
                    $(this).attr('checked', false);
            }
        });
    }
    $.get('ajax_updatefco_all.phtml',{ 'pid': pid,'override':override,'sid':Math.random()}, function(data) {

    });
}
function updateFCO(priceName, pid, pl) //'fprice'
{
    var checkbox = document.getElementById(priceName+"-"+pid);

    if(checkbox.checked)
    {
        var override = 1;			
    }
    else
    {
        var override = 0;		
    }
    $.get('/mgt/pereport/ajax_updatefco.phtml',{ 'pid': pid,'pl':pl,'override':override,'sid':Math.random()}, function(data) {

    });
}
function updateCost_all(costType, pid) {
    var pl_level_arr = costType.split('_');
    var pl_level = pl_level_arr[1];
    document.getElementById(costType+"-"+pid).style.background = '#ffeeee';

    $.get('ajax_saveprice.phtml',{ 'pid': pid,'price':$('#'+costType+"-"+pid).val(),'costtype':pl_level,'sid':Math.random()}, function(data) {
        document.getElementById(costType+"-"+pid).style.background = '#ffffff';
    });
}
function updateCost_allOver(pid) {
    $.get('ajax_saveprice_all.phtml',{ 'pid': pid,'price':$('#all_over_pl_'+pid).val(),'sid':Math.random()}, function(data) {
        $('.plBox'+pid).each(function(){
            if($(this).attr('id') != 'pl6-'+pid) {
                $(this).val($('#all_over_pl_'+pid).val());
            }
        });
    });
}
function updateCost(costType, pid)
{
    // scost, myprice, amprice
    var price = $('#'+costType+'-'+pid).val();
    
    if(costType == 'avgcost') {
        $('#amzcalctable_'+pid).css('background-color','#ffcccc');
        $('#fbacalctable_'+pid).css('background-color','#ffcccc');
        
        $('#amz'+pid+'_cost').html(price);
        $('#fba'+pid+'_cost').html(price);
    }
    if(costType == 'pl'+aprice) {
        $('#amzcalctable_'+pid).css('background-color','#ffcccc');
        $('#amz'+pid+'_price').html(price);
    }
    if(costType == 'pl'+fprice) {
        $('#fbacalctable_'+pid).css('background-color','#ffcccc');
        $('#fba'+pid+'_price').html(price);
    }
    $('#'+costType+'-'+pid).css('background-color','#ffeeee');
    $.get('ajax_saveprice.phtml',{ 'pid': pid,'price':price,'costtype':costType,'sid':Math.random()}, function(data) {
        var dataArr = data.split(":");
        var pid = dataArr[0];
        var costType = dataArr[1];
        if(costType == 'avgcost') {
            updateAMZTable(dataArr[0]);
            updateFBATable(dataArr[0]);
        }
        if(costType == 'pl'+aprice) {
            updateAMZTable(dataArr[0]);
        }
        if(costType == 'pl'+fprice) {
            updateFBATable(dataArr[0]);
        }
        $('#'+costType+'-'+pid).css('background-color','#ffffff');
    });
}
function addSourcing(pid)
{
	document.addSourcingForm.sid.value = "";
//	document.addSourcingForm.sourcing_pid.value = pid;
	$('#sourcing_pid').val(pid);
	var pos = $('#addsourcing_'+pid).offset();
	
	var dlg = document.getElementById("sourcingForm");
	dlg.style.display = "block";
	$("#sourcingForm").offset({ top: pos.top+20, left: pos.left+20});
}
function hideSourcingForm()
{
	$('#sourcing_pid').val('');
	var dlg = document.getElementById("sourcingForm");
	dlg.style.display = "none";
}
function addSourcingValues() {
	var vendorid = $('#sourcing_vendorid').val();
	var sourcing_manpart = $('#sourcing_manpart').val();
	var sourcing_cost = $('#sourcing_cost').val();
	var sourcing_override_price = $('#sourcing_override_price').val();
	var sourcing_distsku = $('#sourcing_distsku').val();
	var sourcing_distqty = $('#sourcing_distqty').val();
	var pid = $('#sourcing_pid').val();
	
	if(document.addSourcingForm.sourcing_stealth.checked)
		sourcing_stealth = 1;
	else
		sourcing_stealth = 0;
		
	$.get('ajax_add_sourcing.phtml',
			{ 
				action: "AddSourcing", 
				'vendorid': vendorid,
				'manpart': sourcing_manpart,
				'distsku': sourcing_distsku,
				'cost': sourcing_cost,
				'distqty': sourcing_distqty,
				'id': pid,
				'stealth': sourcing_stealth,
				'oprice': sourcing_override_price
			}
			, function(data) {
		$('#new_sourcing_table').show();
		$('#new_sourcing_table tr:last').after(data);
		hideSourcingForm();
	});
}
function resetCost(pid, newcost)
{
    $('#fbacalctable_'+pid).css('background-color','#ffcccc');
    $('#amzcalctable_'+pid).css('background-color','#ffcccc');
    $('#amz'+pid+'_cost').html(newcost);
    $('#fba'+pid+'_cost').html(newcost);
    updateFBATable(pid);
    updateAMZTable(pid);
}