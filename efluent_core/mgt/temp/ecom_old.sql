-- MySQL dump 10.9
--
-- Host: localhost    Database: ecom
-- ------------------------------------------------------
-- Server version	4.1.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners_manager`
--

DROP TABLE IF EXISTS `banners_manager`;
CREATE TABLE `banners_manager` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `shop` char(2) default NULL,
  `catid` int(11) default NULL,
  `position` int(11) default NULL,
  `rotation` int(11) default NULL,
  `img_data` blob,
  `img_rich` text,
  `tstamp` datetime default NULL,
  `link` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `position` (`position`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `company_orderflow`
--

DROP TABLE IF EXISTS `company_orderflow`;
CREATE TABLE `company_orderflow` (
  `step` int(11) NOT NULL default '0',
  `status` varchar(255) default NULL,
  `explanation` text,
  KEY `step` (`step`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `company_shipping_assigned`
--

DROP TABLE IF EXISTS `company_shipping_assigned`;
CREATE TABLE `company_shipping_assigned` (
  `id` bigint(20) default NULL,
  `csmid` bigint(20) default NULL,
  `pid` bigint(20) default NULL,
  `grpid` bigint(20) default NULL,
  `catid` bigint(20) default NULL,
  `subid` bigint(20) default NULL,
  `ppr` double(16,2) default NULL,
  `ppr_min` double(16,2) default NULL,
  `ppr_max` double(16,2) default NULL,
  `flatrate` double(16,2) default NULL,
  `flatrate_incremental` int(11) default NULL,
  `required` int(11) default NULL,
  KEY `id` (`id`),
  KEY `csmid` (`csmid`),
  KEY `pid` (`pid`),
  KEY `grpid` (`grpid`,`catid`,`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `company_shipping_methods`
--

DROP TABLE IF EXISTS `company_shipping_methods`;
CREATE TABLE `company_shipping_methods` (
  `id` bigint(20) default NULL,
  `method` varchar(128) default NULL,
  `descr` varchar(255) default NULL,
  `active` int(11) default NULL,
  `default_method` int(11) default NULL,
  `ppr` double(16,2) default NULL,
  `ppr_min` double(16,2) default NULL,
  `ppr_max` double(16,2) default NULL,
  `flatrate` double(16,2) default NULL,
  `flatrate_incremental` int(11) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_accounts`
--

DROP TABLE IF EXISTS `customer_accounts`;
CREATE TABLE `customer_accounts` (
  `acctid` bigint(20) NOT NULL auto_increment,
  `smacctid` bigint(20) NOT NULL default '0',
  `fname` varchar(50) default NULL,
  `lname` varchar(50) default NULL,
  `emailaddr` varchar(255) default NULL,
  `password` varchar(50) default NULL,
  `emailspecials` int(11) default NULL,
  `phone` varchar(50) default NULL,
  `altphone` varchar(50) default NULL,
  `faxnumber` varchar(50) default NULL,
  `website` varchar(255) default NULL,
  `bill2name` varchar(128) default NULL,
  `bill2street1` varchar(128) default NULL,
  `bill2street2` varchar(128) default NULL,
  `bill2city` varchar(128) default NULL,
  `bill2state` varchar(16) default NULL,
  `bill2zip` varchar(20) default NULL,
  `ship2name` varchar(128) default NULL,
  `ship2street1` varchar(128) default NULL,
  `ship2street2` varchar(128) default NULL,
  `ship2city` varchar(128) default NULL,
  `ship2state` varchar(16) default NULL,
  `ship2zip` varchar(20) default NULL,
  `default_pl` int(11) default NULL,
  `nontaxable` int(11) default NULL,
  `taxid` varchar(255) default NULL,
  `created` bigint(20) default NULL,
  `accttype` int(11) default NULL,
  `netterms` int(11) default NULL,
  `creditlimit` double(16,2) default NULL,
  `balance` double(16,2) default NULL,
  `bankacct` varchar(50) default NULL,
  `routingno` varchar(50) default NULL,
  `bankname` varchar(50) default NULL,
  `repid` bigint(20) default NULL,
  `arepid` bigint(20) default NULL,
  `bill2country` char(2) default 'US',
  `ship2country` char(2) default 'US',
  KEY `acctid` (`acctid`),
  KEY `smacctid` (`smacctid`),
  KEY `emailaddr` (`emailaddr`,`password`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_order_lines`
--

DROP TABLE IF EXISTS `customer_order_lines`;
CREATE TABLE `customer_order_lines` (
  `lineid` bigint(20) NOT NULL auto_increment,
  `orderid` bigint(20) NOT NULL default '0',
  `prodid` bigint(20) NOT NULL default '0',
  `manid` bigint(20) default NULL,
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `price` double(16,2) default NULL,
  `qty_ordered` int(11) default NULL,
  `qty_shipped` int(11) NOT NULL default '0',
  `weight` double(16,2) default NULL,
  KEY `lineid` (`lineid`),
  KEY `orderid` (`orderid`),
  KEY `prodid` (`prodid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_order_notes`
--

DROP TABLE IF EXISTS `customer_order_notes`;
CREATE TABLE `customer_order_notes` (
  `orderid` bigint(20) default NULL,
  `notes` text,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_order_tracking`
--

DROP TABLE IF EXISTS `customer_order_tracking`;
CREATE TABLE `customer_order_tracking` (
  `invoiceid` varchar(26) default NULL,
  `trackno` varchar(128) default NULL,
  `type` varchar(128) default NULL,
  `carrier` varchar(10) default NULL,
  `shipdate` bigint(20) default NULL,
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_orders`
--

DROP TABLE IF EXISTS `customer_orders`;
CREATE TABLE `customer_orders` (
  `orderid` bigint(20) NOT NULL auto_increment,
  `smid` bigint(20) NOT NULL default '0',
  `otype` varchar(26) default NULL,
  `acctid` bigint(20) default NULL,
  `smacctid` bigint(20) NOT NULL default '0',
  `invoiceid` varchar(26) default NULL,
  `orderdate` bigint(20) default NULL,
  `status` int(11) default NULL,
  `bill2name` varchar(128) default NULL,
  `bill2street1` varchar(128) default NULL,
  `bill2street2` varchar(128) default NULL,
  `bill2city` varchar(128) default NULL,
  `bill2state` varchar(16) default NULL,
  `bill2zip` varchar(20) default NULL,
  `ship2name` varchar(128) default NULL,
  `ship2street1` varchar(128) default NULL,
  `ship2street2` varchar(128) default NULL,
  `ship2city` varchar(128) default NULL,
  `ship2state` varchar(16) default NULL,
  `ship2zip` varchar(20) default NULL,
  `phone` varchar(50) default NULL,
  `altphone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `emailaddr` varchar(255) default NULL,
  `weight` double(16,2) default NULL,
  `subtotal` double(16,2) default NULL,
  `tax` double(16,2) default NULL,
  `shipping` double(16,2) default NULL,
  `total` double(16,2) default NULL,
  `ip_addr` varchar(26) default NULL,
  `repid` int(11) default NULL,
  `arepid` int(11) default NULL,
  `cardnum` text,
  `exp1a` varchar(4) default NULL,
  `exp1b` varchar(4) default NULL,
  `pmttype` char(2) default NULL,
  `cvv` varchar(10) default NULL,
  `bill2country` char(2) NOT NULL default 'US',
  `ship2country` char(2) NOT NULL default 'US',
  `package` int(11) NOT NULL default '0',
  `csmid` int(11) NOT NULL default '0',
  `csmethod` varchar(255) NOT NULL default '',
  `divid` int(11) NOT NULL default '0',
  `brand` bigint(20) default NULL,
  `clickid` varchar(50) default NULL,
  `clicktype` int(11) default NULL,
  `category` varchar(128) default NULL,
  KEY `orderid` (`orderid`),
  KEY `smid` (`smid`),
  KEY `otype` (`otype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `customer_payments`
--

DROP TABLE IF EXISTS `customer_payments`;
CREATE TABLE `customer_payments` (
  `orderid` bigint(20) default NULL,
  `cardnum` text,
  `exp1a` varchar(4) default NULL,
  `exp1b` varchar(4) default NULL,
  `pmttype` char(2) default NULL,
  `cvv` varchar(10) default NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `datafeed_columns`
--

DROP TABLE IF EXISTS `datafeed_columns`;
CREATE TABLE `datafeed_columns` (
  `dfcid` bigint(20) NOT NULL auto_increment,
  `dfid` bigint(20) NOT NULL default '0',
  `column_name` varchar(255) default NULL,
  `header_name` varchar(255) default NULL,
  `static_data` varchar(255) default NULL,
  KEY `dfcid` (`dfcid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `datafeed_columns_list`
--

DROP TABLE IF EXISTS `datafeed_columns_list`;
CREATE TABLE `datafeed_columns_list` (
  `id` bigint(20) NOT NULL auto_increment,
  `colid` varchar(255) default NULL,
  `colname` varchar(255) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `datafeed_columns_order`
--

DROP TABLE IF EXISTS `datafeed_columns_order`;
CREATE TABLE `datafeed_columns_order` (
  `dfcid` bigint(20) NOT NULL default '0',
  `dorder` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `datafeeds`
--

DROP TABLE IF EXISTS `datafeeds`;
CREATE TABLE `datafeeds` (
  `dfid` bigint(20) NOT NULL auto_increment,
  `dfname` varchar(255) default NULL,
  `delimiter` char(2) default NULL,
  `enclosedby` char(2) default NULL,
  `min` double NOT NULL default '0',
  `max` double NOT NULL default '0',
  `filename` text,
  `tracking_code` varchar(255) default NULL,
  `options_append` int(11) default NULL,
  `options_listall` int(11) default NULL,
  `options_instock` int(11) default NULL,
  `options_content` int(11) default NULL,
  `options_upc` int(11) default NULL,
  `options_visible` int(11) default NULL,
  KEY `dfid` (`dfid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `display_order`
--

DROP TABLE IF EXISTS `display_order`;
CREATE TABLE `display_order` (
  `grpid` int(11) NOT NULL default '0',
  `catid` int(11) NOT NULL default '0',
  `subid` int(11) NOT NULL default '0',
  `dorder` int(11) NOT NULL default '0',
  KEY `grpid` (`grpid`),
  KEY `grpid_2` (`grpid`,`catid`),
  KEY `grpid_3` (`grpid`,`catid`,`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_campaigns`
--

DROP TABLE IF EXISTS `email_campaigns`;
CREATE TABLE `email_campaigns` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `templateid` bigint(20) default NULL,
  `type` int(11) default NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `bdate` datetime default NULL,
  `edate` datetime default NULL,
  `ostatus` int(11) default NULL,
  `cat0` int(11) default NULL,
  `cat1` int(11) default NULL,
  `cat2` int(11) default NULL,
  `cat3` int(11) default NULL,
  `cat4` int(11) default NULL,
  `brand` int(11) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(50) default NULL,
  `zipcode` varchar(32) default NULL,
  `pl` int(11) NOT NULL default '1',
  `minskus` int(11) NOT NULL default '1',
  `maxskus` int(11) NOT NULL default '1',
  `em_clicktype` int(11) default NULL,
  `em_clickid` varchar(50) default NULL,
  `exclusive` int(11) NOT NULL default '0',
  `keywords` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_sent_log`
--

DROP TABLE IF EXISTS `email_sent_log`;
CREATE TABLE `email_sent_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `acctid` bigint(20) default NULL,
  `smacctid` bigint(20) default NULL,
  `invoiceid` bigint(20) NOT NULL default '0',
  `emailaddr` varchar(255) default NULL,
  `campaign` int(11) default NULL,
  `status` int(11) default NULL,
  `estatus` int(11) default NULL,
  `ostatus` int(11) default NULL,
  `odate` datetime default NULL,
  `sentdate` datetime default NULL,
  `md5invoice` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `acctid` (`acctid`,`smacctid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `emailaddr` (`emailaddr`),
  KEY `campaign` (`campaign`),
  KEY `md5invoice` (`md5invoice`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_sent_log_extended`
--

DROP TABLE IF EXISTS `email_sent_log_extended`;
CREATE TABLE `email_sent_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `esid` bigint(20) NOT NULL default '0',
  `errormsg` text,
  `subject` text,
  `body` longblob,
  KEY `id` (`id`),
  KEY `esid` (`esid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_skus`
--

DROP TABLE IF EXISTS `email_skus`;
CREATE TABLE `email_skus` (
  `id` bigint(20) NOT NULL auto_increment,
  `campid` bigint(20) default NULL,
  `pid` bigint(20) default NULL,
  KEY `id` (`id`),
  KEY `campid` (`campid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `subject` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `body` longblob,
  `tbody` longblob,
  KEY `id` (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `email_variables`
--

DROP TABLE IF EXISTS `email_variables`;
CREATE TABLE `email_variables` (
  `id` bigint(20) NOT NULL auto_increment,
  `varname` varchar(50) default NULL,
  `tabledata` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `skulevel` int(11) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `help_items`
--

DROP TABLE IF EXISTS `help_items`;
CREATE TABLE `help_items` (
  `hid` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  `parent` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `public` int(11) NOT NULL default '0',
  `data` text,
  KEY `hid` (`hid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `mid` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  `parent` bigint(20) NOT NULL default '0',
  `public` int(11) NOT NULL default '0',
  `dorder` int(11) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `order_category_map`
--

DROP TABLE IF EXISTS `order_category_map`;
CREATE TABLE `order_category_map` (
  `id` bigint(20) NOT NULL auto_increment,
  `orderid` bigint(20) default NULL,
  `catid` bigint(20) default NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `order_status_codes`
--

DROP TABLE IF EXISTS `order_status_codes`;
CREATE TABLE `order_status_codes` (
  `status` int(11) NOT NULL auto_increment,
  `internal_status` varchar(16) default NULL,
  `client_status` varchar(255) default NULL,
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_accessories`
--

DROP TABLE IF EXISTS `product_accessories`;
CREATE TABLE `product_accessories` (
  `id` bigint(20) NOT NULL default '0',
  `accid` bigint(20) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories` (
  `catid` bigint(20) default NULL,
  `catname` varchar(255) default NULL,
  `parentid` bigint(20) NOT NULL default '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_category_map`
--

DROP TABLE IF EXISTS `product_category_map`;
CREATE TABLE `product_category_map` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) default NULL,
  `catid` bigint(20) default NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_descr`
--

DROP TABLE IF EXISTS `product_descr`;
CREATE TABLE `product_descr` (
  `id` bigint(20) NOT NULL default '0',
  `data` text,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_featured`
--

DROP TABLE IF EXISTS `product_featured`;
CREATE TABLE `product_featured` (
  `id` bigint(20) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_features`
--

DROP TABLE IF EXISTS `product_features`;
CREATE TABLE `product_features` (
  `id` bigint(20) NOT NULL default '0',
  `feature` varchar(255) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_groups`
--

DROP TABLE IF EXISTS `product_groups`;
CREATE TABLE `product_groups` (
  `grpid` int(11) NOT NULL default '0',
  `catid` int(11) NOT NULL default '0',
  `subid` int(11) NOT NULL default '0',
  `grpdescr` varchar(255) default NULL,
  `catdescr` varchar(255) default NULL,
  `subdescr` varchar(255) default NULL,
  KEY `grpid` (`grpid`,`catid`,`subid`),
  KEY `grpdescr` (`grpdescr`,`catdescr`,`subdescr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_html`
--

DROP TABLE IF EXISTS `product_html`;
CREATE TABLE `product_html` (
  `id` bigint(20) NOT NULL default '0',
  `data` text,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `id` bigint(20) NOT NULL default '0',
  `idata` longblob,
  `itype` int(11) default NULL,
  `thumb` int(11) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_inventory`
--

DROP TABLE IF EXISTS `product_inventory`;
CREATE TABLE `product_inventory` (
  `id` bigint(20) NOT NULL default '0',
  `distname` varchar(25) NOT NULL default '',
  `warehouse` varchar(50) NOT NULL default '',
  `wh_state` char(2) NOT NULL default 'XX',
  `inv_available` bigint(20) NOT NULL default '0',
  `inv_backorder` bigint(20) NOT NULL default '0',
  `inv_onorder` bigint(20) NOT NULL default '0',
  `eta` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_inventory_cache`
--

DROP TABLE IF EXISTS `product_inventory_cache`;
CREATE TABLE `product_inventory_cache` (
  `id` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_list`
--

DROP TABLE IF EXISTS `product_list`;
CREATE TABLE `product_list` (
  `id` bigint(20) NOT NULL default '-1',
  `manid` bigint(20) NOT NULL default '-1',
  `manpart` varchar(50) NOT NULL default '',
  `descr` varchar(255) NOT NULL default 'NO DESCRIPTION',
  `added_dte` bigint(20) NOT NULL default '0',
  `status` varchar(4) default NULL,
  `returnable` int(11) NOT NULL default '0',
  `upccode` bigint(20) default NULL,
  `platform` varchar(7) NOT NULL default '',
  `grp` int(11) NOT NULL default '0',
  `cat` int(11) NOT NULL default '0',
  `sub` int(11) NOT NULL default '0',
  `roimast` bigint(20) NOT NULL default '-1',
  `dropship` int(11) NOT NULL default '0',
  `shipweight` double(16,2) NOT NULL default '0.00',
  `oem` int(11) NOT NULL default '0',
  `refurb` int(11) NOT NULL default '0',
  `sku` varchar(50) NOT NULL default '',
  `subsku` varchar(50) NOT NULL default '',
  `visible` int(11) NOT NULL default '0',
  `kit` int(11) NOT NULL default '0',
  `demo` int(11) NOT NULL default '0',
  `hidestock` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `manid` (`manid`,`manpart`),
  KEY `grp` (`grp`,`cat`,`sub`),
  KEY `descr` (`descr`),
  KEY `added_dte` (`added_dte`),
  KEY `status` (`status`,`returnable`),
  KEY `upccode` (`upccode`),
  KEY `manpart` (`manpart`),
  KEY `status_2` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_manufacturers`
--

DROP TABLE IF EXISTS `product_manufacturers`;
CREATE TABLE `product_manufacturers` (
  `manid` bigint(20) NOT NULL auto_increment,
  `manname` varchar(50) NOT NULL default '',
  KEY `manid` (`manid`),
  KEY `manname` (`manname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_prices`
--

DROP TABLE IF EXISTS `product_prices`;
CREATE TABLE `product_prices` (
  `id` bigint(20) NOT NULL default '0',
  `price_msrp` double(16,2) default NULL,
  `price_level1` double(16,2) default NULL,
  `price_level2` double(16,2) default NULL,
  `price_level3` double(16,2) default NULL,
  `price_level4` double(16,2) default NULL,
  `price_level5` double(16,2) default NULL,
  `avgcost` double(16,2) default NULL,
  `price_level6` double(16,2) default NULL,
  `price_level7` double(16,2) default NULL,
  `price_level8` double(16,2) default NULL,
  `price_level9` double(16,2) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_related`
--

DROP TABLE IF EXISTS `product_related`;
CREATE TABLE `product_related` (
  `id` bigint(20) NOT NULL default '0',
  `relid` bigint(20) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_sourcing`
--

DROP TABLE IF EXISTS `product_sourcing`;
CREATE TABLE `product_sourcing` (
  `sid` bigint(20) default NULL,
  `distid` char(2) default NULL,
  `manid` bigint(20) default NULL,
  `manpart` varchar(50) default NULL,
  `distsku` varchar(50) default NULL,
  `cost` double(16,2) default NULL,
  KEY `sid` (`sid`),
  KEY `distid` (`distid`,`distsku`),
  KEY `manid` (`manid`,`manpart`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_specs`
--

DROP TABLE IF EXISTS `product_specs`;
CREATE TABLE `product_specs` (
  `id` bigint(20) NOT NULL default '0',
  `section` varchar(255) default NULL,
  `dkey` varchar(255) default NULL,
  `dval` varchar(255) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_static_specs`
--

DROP TABLE IF EXISTS `product_static_specs`;
CREATE TABLE `product_static_specs` (
  `id` bigint(20) default NULL,
  `tid` bigint(20) default NULL,
  `sscid` bigint(20) default NULL,
  `dvalue` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `tid` (`tid`),
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product_translation`
--

DROP TABLE IF EXISTS `product_translation`;
CREATE TABLE `product_translation` (
  `divid` int(11) NOT NULL default '0',
  `productid` varchar(50) NOT NULL default '',
  `newproductid` bigint(20) default NULL,
  KEY `divid` (`divid`,`productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` bigint(20) NOT NULL auto_increment,
  `sessionid` varchar(128) default NULL,
  `trackid` varchar(128) default NULL,
  `tstamp` bigint(20) default NULL,
  `ip_addr` varchar(20) default NULL,
  `curpage` varchar(255) default NULL,
  `pagedescr` varchar(255) default NULL,
  `pid` bigint(20) default NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`),
  KEY `tstamp` (`tstamp`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions_campaigns`
--

DROP TABLE IF EXISTS `sessions_campaigns`;
CREATE TABLE `sessions_campaigns` (
  `id` bigint(20) NOT NULL auto_increment,
  `campname` varchar(128) default NULL,
  `reftag` varchar(128) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions_filter`
--

DROP TABLE IF EXISTS `sessions_filter`;
CREATE TABLE `sessions_filter` (
  `id` bigint(20) NOT NULL auto_increment,
  `ip_addr` varchar(20) default NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions_log`
--

DROP TABLE IF EXISTS `sessions_log`;
CREATE TABLE `sessions_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `trackid` varchar(128) NOT NULL default '',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `trackid` (`trackid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions_traffic`
--

DROP TABLE IF EXISTS `sessions_traffic`;
CREATE TABLE `sessions_traffic` (
  `id` bigint(20) NOT NULL default '0',
  `sessionid` varchar(128) default NULL,
  `trackid` varchar(128) default NULL,
  `tstamp` bigint(20) default NULL,
  `pageurl` varchar(255) default NULL,
  `referrer` text,
  `action` int(11) default NULL,
  `pagedescr` varchar(128) default NULL,
  `pid` bigint(20) default NULL,
  `cookie` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `static_specs_columns`
--

DROP TABLE IF EXISTS `static_specs_columns`;
CREATE TABLE `static_specs_columns` (
  `id` bigint(20) default NULL,
  `tid` bigint(20) default NULL,
  `dkey` varchar(255) default NULL,
  `dorder` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `subcat_order`
--

DROP TABLE IF EXISTS `subcat_order`;
CREATE TABLE `subcat_order` (
  `grpid` varchar(25) default NULL,
  `dorder` int(11) NOT NULL default '0',
  KEY `grpid` (`grpid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `synnex_cats`
--

DROP TABLE IF EXISTS `synnex_cats`;
CREATE TABLE `synnex_cats` (
  `grpid` int(11) default NULL,
  `grpdescr` varchar(255) default NULL,
  `catid` int(11) default NULL,
  `catdescr` varchar(255) default NULL,
  `subid` int(11) default NULL,
  `subdescr` varchar(255) default NULL,
  `synnex` int(11) NOT NULL default '0',
  `dummy` varchar(255) default NULL,
  KEY `synnex` (`synnex`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_campaign_types`
--

DROP TABLE IF EXISTS `traffic_campaign_types`;
CREATE TABLE `traffic_campaign_types` (
  `id` bigint(20) NOT NULL auto_increment,
  `reftag` varchar(50) default NULL,
  `clicktype` int(11) default NULL,
  `cpc` double(16,2) default NULL,
  `title` varchar(50) default NULL,
  `tid` varchar(16) default NULL,
  `abr` int(11) NOT NULL default '0',
  `filter` int(11) NOT NULL default '0',
  `price_level` int(11) NOT NULL default '1',
  KEY `id` (`id`),
  KEY `reftag` (`reftag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_path_pages`
--

DROP TABLE IF EXISTS `traffic_click_path_pages`;
CREATE TABLE `traffic_click_path_pages` (
  `id` bigint(20) NOT NULL auto_increment,
  `pagedescr` varchar(255) default NULL,
  KEY `id` (`id`),
  KEY `pagedescr` (`pagedescr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_paths`
--

DROP TABLE IF EXISTS `traffic_click_paths`;
CREATE TABLE `traffic_click_paths` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `path` varchar(255) default NULL,
  `followed` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`path`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_summary`
--

DROP TABLE IF EXISTS `traffic_click_summary`;
CREATE TABLE `traffic_click_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(255) NOT NULL default '',
  `clicks` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  `coa` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_summary_byip`
--

DROP TABLE IF EXISTS `traffic_click_summary_byip`;
CREATE TABLE `traffic_click_summary_byip` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(16) NOT NULL default '',
  `ip_addr` varchar(20) default NULL,
  `network` varchar(20) default NULL,
  `pid` bigint(20) default NULL,
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `clicks` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`pid`,`ip_addr`),
  KEY `tstamp_3` (`tstamp`,`clicktype`,`clickid`,`pid`,`network`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_summary_bysku`
--

DROP TABLE IF EXISTS `traffic_click_summary_bysku`;
CREATE TABLE `traffic_click_summary_bysku` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(255) default NULL,
  `pid` bigint(20) NOT NULL default '0',
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` text,
  `clicks` int(11) default NULL,
  `sales` int(11) default NULL,
  `salesvol` double(16,2) default NULL,
  `coa` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `clicktype` (`clicktype`,`clickid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_click_summary_extended`
--

DROP TABLE IF EXISTS `traffic_click_summary_extended`;
CREATE TABLE `traffic_click_summary_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(16) NOT NULL default '',
  `pid` bigint(20) default NULL,
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `clicks` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  `coa` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_exit_pages`
--

DROP TABLE IF EXISTS `traffic_exit_pages`;
CREATE TABLE `traffic_exit_pages` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `page` varchar(255) default NULL,
  `clicks` int(11) default NULL,
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_product_performance`
--

DROP TABLE IF EXISTS `traffic_product_performance`;
CREATE TABLE `traffic_product_performance` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `pid` bigint(20) NOT NULL default '-1',
  `views` int(11) default '0',
  `clicks` int(11) default '0',
  `sales` int(11) default '0',
  `salesvol` double(16,2) default '0.00',
  `views_uk` int(11) default '0',
  `clicks_uk` int(11) default '0',
  `sales_uk` int(11) default '0',
  `salesvol_uk` double(16,2) default '0.00',
  `views_pe` int(11) default '0',
  `clicks_pe` int(11) default '0',
  `sales_pe` int(11) default '0',
  `salesvol_pe` double(16,2) default '0.00',
  `views_ca` int(11) default '0',
  `clicks_ca` int(11) default '0',
  `sales_ca` int(11) default '0',
  `salesvol_ca` double(16,2) default '0.00',
  `views_se` int(11) default '0',
  `clicks_se` int(11) default '0',
  `sales_se` int(11) default '0',
  `salesvol_se` double(16,2) default '0.00',
  `views_dr` int(11) default '0',
  `clicks_dr` int(11) default '0',
  `sales_dr` int(11) default '0',
  `salesvol_dr` double(16,2) default '0.00',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `pid` (`pid`),
  KEY `tstamp_2` (`tstamp`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_referring_urls`
--

DROP TABLE IF EXISTS `traffic_referring_urls`;
CREATE TABLE `traffic_referring_urls` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `domain` varchar(255) default NULL,
  `path` varchar(255) default NULL,
  `clicks` int(11) default NULL,
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`domain`,`path`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_referring_urls_extended`
--

DROP TABLE IF EXISTS `traffic_referring_urls_extended`;
CREATE TABLE `traffic_referring_urls_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `domain` varchar(255) default NULL,
  `path` varchar(255) default NULL,
  `qstring` varchar(255) default NULL,
  `link` text,
  `clicks` int(11) default NULL,
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`domain`,`path`,`qstring`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_sales_summary`
--

DROP TABLE IF EXISTS `traffic_sales_summary`;
CREATE TABLE `traffic_sales_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(255) default NULL,
  `invoiceid` varchar(50) default NULL,
  `ip_addr` varchar(26) default NULL,
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `clicktype` (`clicktype`,`clickid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_search_terms_external`
--

DROP TABLE IF EXISTS `traffic_search_terms_external`;
CREATE TABLE `traffic_search_terms_external` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(16) NOT NULL default '',
  `clicks` int(11) default NULL,
  `phrase` varchar(128) default NULL,
  `pviews` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_search_terms_external_extended`
--

DROP TABLE IF EXISTS `traffic_search_terms_external_extended`;
CREATE TABLE `traffic_search_terms_external_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `clicktype` int(11) default NULL,
  `clickid` varchar(16) NOT NULL default '',
  `phrase` varchar(128) default NULL,
  `pid` bigint(20) default NULL,
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `views` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_search_terms_internal`
--

DROP TABLE IF EXISTS `traffic_search_terms_internal`;
CREATE TABLE `traffic_search_terms_internal` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `phrase` varchar(128) default NULL,
  `clicks` int(11) NOT NULL default '0',
  `pviews` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_search_terms_internal_extended`
--

DROP TABLE IF EXISTS `traffic_search_terms_internal_extended`;
CREATE TABLE `traffic_search_terms_internal_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `phrase` varchar(128) default NULL,
  `pid` bigint(20) default NULL,
  `manname` varchar(50) default NULL,
  `manpart` varchar(50) default NULL,
  `descr` varchar(255) default NULL,
  `views` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_summary_hourly_hits`
--

DROP TABLE IF EXISTS `traffic_summary_hourly_hits`;
CREATE TABLE `traffic_summary_hourly_hits` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `rtype` int(11) NOT NULL default '0',
  `hour_00` double NOT NULL default '0',
  `hour_01` double NOT NULL default '0',
  `hour_02` double NOT NULL default '0',
  `hour_03` double NOT NULL default '0',
  `hour_04` double NOT NULL default '0',
  `hour_05` double NOT NULL default '0',
  `hour_06` double NOT NULL default '0',
  `hour_07` double NOT NULL default '0',
  `hour_08` double NOT NULL default '0',
  `hour_09` double NOT NULL default '0',
  `hour_10` double NOT NULL default '0',
  `hour_11` double NOT NULL default '0',
  `hour_12` double NOT NULL default '0',
  `hour_13` double NOT NULL default '0',
  `hour_14` double NOT NULL default '0',
  `hour_15` double NOT NULL default '0',
  `hour_16` double NOT NULL default '0',
  `hour_17` double NOT NULL default '0',
  `hour_18` double NOT NULL default '0',
  `hour_19` double NOT NULL default '0',
  `hour_20` double NOT NULL default '0',
  `hour_21` double NOT NULL default '0',
  `hour_22` double NOT NULL default '0',
  `hour_23` double NOT NULL default '0',
  `hour_24` double NOT NULL default '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`rtype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `traffic_summary_location`
--

DROP TABLE IF EXISTS `traffic_summary_location`;
CREATE TABLE `traffic_summary_location` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` bigint(20) default NULL,
  `country` varchar(64) default NULL,
  `ccode` varchar(4) default NULL,
  `city` varchar(128) default NULL,
  `state` varchar(128) default NULL,
  `hits` int(11) NOT NULL default '0',
  `sales` int(11) NOT NULL default '0',
  `salesvol` double(16,2) NOT NULL default '0.00',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`country`),
  KEY `tstamp_2` (`tstamp`,`country`,`city`),
  KEY `tstamp_3` (`tstamp`,`country`,`city`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL auto_increment,
  `un` varchar(32) NOT NULL default '',
  `pw_md5` varchar(32) NOT NULL default '',
  `displayname` varchar(128) default NULL,
  `emailaddr` varchar(255) default NULL,
  `created` datetime default NULL,
  `tstamp` datetime default NULL,
  KEY `id` (`id`),
  KEY `un` (`un`,`pw_md5`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `verification_log`
--

DROP TABLE IF EXISTS `verification_log`;
CREATE TABLE `verification_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `orderid` varchar(26) default NULL,
  `ordertext` text,
  `sendtext` text,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `verification_results`
--

DROP TABLE IF EXISTS `verification_results`;
CREATE TABLE `verification_results` (
  `id` bigint(20) NOT NULL auto_increment,
  `tstamp` datetime default NULL,
  `orderid` varchar(26) NOT NULL default '',
  `pares` char(2) default NULL,
  `cavv` varchar(32) default NULL,
  `eci` varchar(5) default NULL,
  `xid` varchar(64) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `verification_results_pp`
--

DROP TABLE IF EXISTS `verification_results_pp`;
CREATE TABLE `verification_results_pp` (
  `id` bigint(20) NOT NULL auto_increment,
  `orderid` varchar(25) default NULL,
  `pmtstatus` varchar(25) default NULL,
  `txid` varchar(50) default NULL,
  `tstamp` datetime default NULL,
  `status` varchar(50) default NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

