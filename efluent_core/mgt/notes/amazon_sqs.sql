 CREATE TABLE `amazon_sqs` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`uniqueid` VARCHAR( 255 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `amazon_sqs` ADD `asin` VARCHAR( 100 ) NOT NULL AFTER `id` ;

 CREATE TABLE `amazon_sqs_summary` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sqs_id` INT( 10 ) NOT NULL ,
`asin` VARCHAR( 100 ) NOT NULL ,
`used_amazon` INT( 10 ) NOT NULL ,
`used_merchant` INT( 10 ) NOT NULL ,
`new_amazon` INT( 10 ) NOT NULL ,
`new_merchant` INT( 10 ) NOT NULL
) ENGINE = MYISAM;

 ALTER TABLE `amazon_sqs_summary` ADD INDEX ( `sqs_id` ) ;
 ALTER TABLE `amazon_sqs_summary` ADD INDEX ( `asin` ) ;

 CREATE TABLE `amazon_sqs_lowest_price` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sqs_id` INT( 10 ) NOT NULL ,
`asin` VARCHAR( 100 ) NOT NULL ,
`fulfillment_channel` VARCHAR( 100 ) NOT NULL ,
`item_condition` tinyint(2) NOT NULL ,
`price` double(16,2) NOT NULL default '0.00'
) ENGINE = MYISAM;

ALTER TABLE `amazon_sqs_lowest_price` ADD INDEX ( `sqs_id` ) ;
ALTER TABLE `amazon_sqs_lowest_price` ADD INDEX ( `asin` ) ;

 CREATE TABLE `amazon_sqs_buybox_price` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sqs_id` INT( 10 ) NOT NULL ,
`asin` VARCHAR( 100 ) NOT NULL ,
`item_condition` tinyint(2) NOT NULL ,
`price` double(16,2) NOT NULL default '0.00'
) ENGINE = MYISAM;

ALTER TABLE `amazon_sqs_buybox_price` ADD INDEX ( `sqs_id` ) ;
ALTER TABLE `amazon_sqs_buybox_price` ADD INDEX ( `asin` ) ;

 CREATE TABLE `amazon_sqs_sales_rank` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sqs_id` INT( 10 ) NOT NULL ,
`asin` VARCHAR( 100 ) NOT NULL ,
`category_id` VARCHAR( 100 ) NOT NULL ,
`rank` INT( 10 )
) ENGINE = MYISAM;

ALTER TABLE `amazon_sqs_sales_rank` ADD INDEX ( `sqs_id` ) ;
ALTER TABLE `amazon_sqs_sales_rank` ADD INDEX ( `asin` ) ;

 CREATE TABLE `amazon_sqs_offers` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sqs_id` INT( 10 ) NOT NULL ,
`asin` VARCHAR( 100 ) NOT NULL ,
`seller_id` VARCHAR( 100 ) NOT NULL ,
`item_condition` tinyint(2) NOT NULL ,
`seller_positive_feedback_reating` INT( 10 ) NOT NULL ,
`seller_feedback_count` INT( 10 ) NOT NULL ,
`seller_shipping_availability_type` VARCHAR( 100 ) NOT NULL ,
`seller_listing_price` double(16,2) NOT NULL default '0.00',
`seller_shipping` double(16,2) NOT NULL default '0.00',
`seller_is_fulfilled` tinyint(2) NOT NULL,
`seller_is_box_winner` tinyint(2) NOT NULL,
`seller_condition_notes` text
) ENGINE = MYISAM;

ALTER TABLE `amazon_sqs_offers` ADD INDEX ( `sqs_id` ) ;
ALTER TABLE `amazon_sqs_offers` ADD INDEX ( `asin` ) ;

 CREATE TABLE `amazon_sqs_sellers` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`seller_id` INT( 10 ) NOT NULL ,
`seller_name` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM;


 CREATE TABLE `amazon_sqs_nodes` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nodeid` VARCHAR( 255 ) NOT NULL ,
`nodename` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

 CREATE TABLE `ecom`.`amazon_sqs_sales_rank2` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`asin` VARCHAR( 255 ) NOT NULL ,
`rank` INT( 10 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
INDEX ( `asin` )
) ENGINE = MYISAM 