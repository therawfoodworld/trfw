truncate amazon_base_messages;
truncate amazon_base_requests;
truncate amazon_duplicates;
truncate amazon_duplicates_extended;
truncate amazon_errors;
truncate amazon_errors_other;
truncate amazon_feed_report;
truncate amazon_log;
truncate amazon_log_extended;
truncate amazon_log_extended_skus;
truncate amazon_order_reports;

