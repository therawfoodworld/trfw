CREATE TABLE `amazonmax_duplicates` (
  `did` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_duplicates_extended` (
  `did` bigint(20) NOT NULL default '0',
  `asin` varchar(50) default NULL,
  `salesrank` varchar(20) default NULL,
  `imageurl` varchar(255) default NULL,
  `brand` varchar(50) default NULL,
  `upc` varchar(50) default NULL,
  `features` text,
  `lowestprice` varchar(50) default NULL,
  `numsellers` int(11) default NULL,
  `title` varchar(255) default NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_errors` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) default NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `requestid_2` (`requestid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_base_messages` (
  `mid` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `brid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL default '',
  `pid_lastupdate` bigint(20) NOT NULL default '0',
  `qty` int(11) NOT NULL default '0',
  `price` double NOT NULL default '0',
  `listid` int(11) NOT NULL default '0',
  `deleteflag` int(11) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`),
  KEY `listid_2` (`listid`),
  KEY `listid_3` (`listid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `mid` bigint(20) NOT NULL default '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` bigint(20) NOT NULL default '-1',
  `requestid` varchar(50) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_categories` (
  `catid` bigint(20) NOT NULL default '0',
  `amazoncat` bigint(20) NOT NULL default '0',
  `pid` bigint(20) NOT NULL default '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL default '0',
  `tlid` bigint(20) NOT NULL default '0',
  `override_rid` int(11) NOT NULL default '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_tlid_map` (
  `tid` bigint(20) NOT NULL default '0',
  `tlid` bigint(20) NOT NULL default '0',
  `override_rid` int(11) NOT NULL default '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_ss_map` (
  `sscid` bigint(20) NOT NULL default '0',
  `amzkid` bigint(20) NOT NULL default '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `amazonmax_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_campaign_lists` (
  `campid` bigint(20) NOT NULL default '0',
  `listid` bigint(20) NOT NULL default '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_qty_scripts` (
  `campid` bigint(20) NOT NULL default '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_campaigns_categories` (
`campid` bigint NOT NULL,
`catid` bigint NOT NULL,
INDEX(campid)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_campaigns` (
  `id` bigint(20) NOT NULL auto_increment,
  `campaign_name` varchar(255) default NULL,
  `min_profit_pct` double(16,2) NOT NULL default '0.00',
  `min_profit_cur` double(16,2) NOT NULL default '0.00',
  `quantity` int(11) NOT NULL default '-1',
  `qty_control` int(11) NOT NULL default '80',
  `pricelevel` int(11) NOT NULL default '0',
  `lastupdate` bigint(20) default NULL,
  `min_qty` int(11) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_feed_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','ORDERADJUSTMENT') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_order_reports_by_last_update` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` int(11) NOT NULL default '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_unshipped_orders` (
  `invoiceid` varchar(26) default NULL,
  `paydate` datetime default NULL,
  `dayslate` int(11) NOT NULL default '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_errors_other` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) default NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') default NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_convert_mfn` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL,
  `date_created` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_base_requests` (
  `brid` bigint(20) NOT NULL auto_increment,
  `amazon_submissionid` varchar(50) default NULL,
  `amazon_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_convert_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_kit_quantity` (
  `orderid` bigint(20) default NULL,
  `pid` bigint(20) NOT NULL,
  `ampid` varchar(24) default NULL,
  `qty` int(11) default NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_order_reports_by_last_update_orders` (
  `id` bigint(20) NOT NULL auto_increment,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(20) NOT NULL,
  `purchase_timestamp_pst` int(20) NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(20) NOT NULL,
  `lastupdate_timestamp_pst` int(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` int(5) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM 

CREATE TABLE `amazonmax_order_reports_by_last_update_orders_items` (
  `id` bigint(20) NOT NULL auto_increment,
  `orderid` bigint(20) NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` int(5) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL default '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_relationships` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM 

CREATE TABLE `amazonmax_order_reports_by_date_items` (
  `id` bigint(20) NOT NULL auto_increment,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(5) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_order_reports_by_date` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` int(11) NOT NULL default '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_inbound_shipping` (
  `id` bigint(20) NOT NULL auto_increment,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_inbound_shipping_extra` (
  `id` bigint(20) NOT NULL auto_increment,
  `ais_id` bigint(20) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL default '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL default '0',
  `assigned` bigint(2) NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_template_att` (
  `att_id` bigint(20) NOT NULL auto_increment,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`att_id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_template_att_values` (
  `att_value_id` bigint(20) NOT NULL auto_increment,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY  (`att_value_id`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_template_category` (
  `catid` bigint(20) NOT NULL auto_increment,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY  (`catid`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_template_category_attributes` (
  `attid` bigint(20) NOT NULL auto_increment,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) default NULL,
  PRIMARY KEY  (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM

---------------------------------------------------------

CREATE TABLE IF NOT EXISTS `amazonmax_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email`  varchar(100) NOT NULL,
  `rater_role`  varchar(100) NOT NULL,
  `notes` TEXT NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` TINYINT( 3 ) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `amazonmax_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM

CREATE TABLE `amazonmax_returns` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`tstamp` VARCHAR( 255 ) NOT NULL ,
`tstamp_est` INT( 10 ) NOT NULL ,
`amazon_orderid` VARCHAR( 255 ) NOT NULL ,
`sku` VARCHAR( 255 ) NOT NULL ,
`asin` VARCHAR( 255 ) NOT NULL ,
`qty` INT( 10 ) NOT NULL ,
`title` VARCHAR( 255 ) NOT NULL ,
`fc` VARCHAR( 255 ) NOT NULL ,
`disposition` VARCHAR( 255 ) NOT NULL ,
INDEX ( `sku` )
) ENGINE = MYISAM ;