####################################################################
ALTER TABLE product_accessories ENGINE = InnoDB;
ALTER TABLE product_accessories2 ENGINE = InnoDB;
ALTER TABLE product_categories ENGINE = InnoDB;
ALTER TABLE product_categories_order ENGINE = InnoDB;
ALTER TABLE product_category_map ENGINE = InnoDB;
ALTER TABLE product_descr ENGINE = InnoDB;
ALTER TABLE product_docs ENGINE = InnoDB;
ALTER TABLE product_featured ENGINE = InnoDB;
ALTER TABLE product_features ENGINE = InnoDB;
ALTER TABLE product_groups ENGINE = InnoDB;
ALTER TABLE product_html ENGINE = InnoDB;
ALTER TABLE product_inthebox ENGINE = InnoDB;
ALTER TABLE product_inventory ENGINE = InnoDB;
ALTER TABLE product_inventory_b ENGINE = InnoDB;
ALTER TABLE product_inventory_cache ENGINE = InnoDB;
ALTER TABLE product_keywords ENGINE = InnoDB;
ALTER TABLE product_kitskus ENGINE = InnoDB;
ALTER TABLE product_list ENGINE = InnoDB;
ALTER TABLE product_manufacturers ENGINE = InnoDB;
ALTER TABLE product_prices ENGINE = InnoDB;
ALTER TABLE product_profit_cached ENGINE = InnoDB;
ALTER TABLE product_purchase_history ENGINE = InnoDB;
ALTER TABLE product_related ENGINE = InnoDB;
ALTER TABLE product_search_extended ENGINE = InnoDB;
ALTER TABLE product_sourcing ENGINE = InnoDB;
ALTER TABLE product_specs ENGINE = InnoDB;
ALTER TABLE product_static_specs ENGINE = InnoDB;
ALTER TABLE product_taxonomy ENGINE = InnoDB;
ALTER TABLE product_translation ENGINE = InnoDB;
ALTER TABLE product_warranty ENGINE = InnoDB;
####################################################################
ALTER TABLE customer_accounts ENGINE = InnoDB;
ALTER TABLE customer_billinginfo ENGINE = InnoDB;
ALTER TABLE customer_billinginfo_extended ENGINE = InnoDB;
ALTER TABLE customer_order_lines ENGINE = InnoDB;
ALTER TABLE customer_order_lines_log ENGINE = InnoDB;
ALTER TABLE customer_order_notes ENGINE = InnoDB;
ALTER TABLE customer_order_notes_internal ENGINE = InnoDB;
ALTER TABLE customer_order_tracking ENGINE = InnoDB;
ALTER TABLE customer_order_tracking_extended ENGINE = InnoDB;
ALTER TABLE customer_orders ENGINE = InnoDB;
ALTER TABLE customer_payments ENGINE = InnoDB;
ALTER TABLE customer_shippinginfo ENGINE = InnoDB;
####################################################################
ALTER TABLE vendor_cat_map ENGINE = InnoDB;
ALTER TABLE vendor_categories ENGINE = InnoDB;
ALTER TABLE vendor_category_map ENGINE = InnoDB;
ALTER TABLE vendor_features ENGINE = InnoDB;
ALTER TABLE vendor_invoice ENGINE = InnoDB;
ALTER TABLE vendor_invoice_lines ENGINE = InnoDB;
ALTER TABLE vendor_man_map ENGINE = InnoDB;
ALTER TABLE vendor_manufacturers ENGINE = InnoDB;
ALTER TABLE vendor_markup_rules ENGINE = InnoDB;
ALTER TABLE vendor_merge_profiles ENGINE = InnoDB;
ALTER TABLE vendor_merge_profiles_extended ENGINE = InnoDB;
ALTER TABLE vendor_metatag_data ENGINE = InnoDB;
ALTER TABLE vendor_notes ENGINE = InnoDB;
ALTER TABLE vendor_po_lines ENGINE = InnoDB;
ALTER TABLE vendor_po_notes ENGINE = InnoDB;
ALTER TABLE vendor_pos ENGINE = InnoDB;
ALTER TABLE vendor_pricing ENGINE = InnoDB;
ALTER TABLE vendor_product_accessories ENGINE = InnoDB;
ALTER TABLE vendor_product_docs ENGINE = InnoDB;
ALTER TABLE vendor_product_html ENGINE = InnoDB;
ALTER TABLE vendor_product_list ENGINE = InnoDB;
ALTER TABLE vendor_product_manufacturers ENGINE = InnoDB;
ALTER TABLE vendor_product_specs ENGINE = InnoDB;
ALTER TABLE vendor_receipt ENGINE = InnoDB;
ALTER TABLE vendor_receipt_lines ENGINE = InnoDB;
ALTER TABLE vendor_sales_descr ENGINE = InnoDB;
ALTER TABLE vendor_sync ENGINE = InnoDB;
ALTER TABLE vendors ENGINE = InnoDB;
####################################################################
ALTER TABLE static_specs_columns ENGINE = InnoDB;
ALTER TABLE static_specs_templates ENGINE = InnoDB;
####################################################################
ALTER TABLE action_log ENGINE = InnoDB;
ALTER TABLE action_log_extended ENGINE = InnoDB;



DO NOT DO

ALTER TABLE product_images ENGINE = InnoDB;
ALTER TABLE product_manufacturers_image ENGINE = InnoDB;
ALTER TABLE vendor_product_images ENGINE = InnoDB;