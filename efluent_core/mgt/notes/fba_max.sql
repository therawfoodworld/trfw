CREATE TABLE IF NOT EXISTS `fba_max_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_id` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_cost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repid` bigint(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_cost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_new` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_feed_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_age_report` (
  `id` bigint(20) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `fba_max_inventory_mismatch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_line_items_import` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_order_mismatch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `fba_max_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `updateprice` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_max_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


####################### FBA CANADA #######################


CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_network` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_respons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` bigint(20) NOT NULL DEFAULT '0',
  `order_created` bigint(20) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_respons_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sc_id` bigint(20) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;