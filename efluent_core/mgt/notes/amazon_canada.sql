
--
-- Table structure for table `amazon_ca_base_messages`
--

DROP TABLE IF EXISTS `amazon_ca_base_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_base_messages` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `qty` int(10) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` smallint(5) NOT NULL DEFAULT '0',
  `deleteflag` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_base_requests`
--

DROP TABLE IF EXISTS `amazon_ca_base_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_campaign_lists`
--

DROP TABLE IF EXISTS `amazon_ca_campaign_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_campaigns`
--

DROP TABLE IF EXISTS `amazon_ca_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` int(10) unsigned DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_categories`
--

DROP TABLE IF EXISTS `amazon_ca_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_categories` (
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `amazoncat` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_duplicates`
--

DROP TABLE IF EXISTS `amazon_ca_duplicates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_duplicates_extended`
--

DROP TABLE IF EXISTS `amazon_ca_duplicates_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_errors`
--

DROP TABLE IF EXISTS `amazon_ca_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_errors_other`
--

DROP TABLE IF EXISTS `amazon_ca_errors_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_feed_report`
--

DROP TABLE IF EXISTS `amazon_ca_feed_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_feedback`
--

DROP TABLE IF EXISTS `amazon_ca_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_inbound_shipping`
--

DROP TABLE IF EXISTS `amazon_ca_inbound_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_inbound_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_inbound_shipping_extra`
--

DROP TABLE IF EXISTS `amazon_ca_inbound_shipping_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_inbound_shipping_extra` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poid` (`poid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_inbound_shipping_files`
--

DROP TABLE IF EXISTS `amazon_ca_inbound_shipping_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_inbound_shipping_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inbound_id` int(10) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_log`
--

DROP TABLE IF EXISTS `amazon_ca_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_log_extended`
--

DROP TABLE IF EXISTS `amazon_ca_log_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_log_extended_skus`
--

DROP TABLE IF EXISTS `amazon_ca_log_extended_skus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_order_reports`
--

DROP TABLE IF EXISTS `amazon_ca_order_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_order_reports` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_orders_uploaded`
--

DROP TABLE IF EXISTS `amazon_ca_orders_uploaded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_orders_uploaded` (
  `orderid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_qty_scripts`
--

DROP TABLE IF EXISTS `amazon_ca_qty_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_qty_scripts` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_relationships`
--

DROP TABLE IF EXISTS `amazon_ca_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_shipping_map`
--

DROP TABLE IF EXISTS `amazon_ca_shipping_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `amazon_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `weight_min` double(16,2) NOT NULL DEFAULT '0.00',
  `weight_max` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_min` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_ss_map`
--

DROP TABLE IF EXISTS `amazon_ca_ss_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_att`
--

DROP TABLE IF EXISTS `amazon_ca_template_att`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `catid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_att_values`
--

DROP TABLE IF EXISTS `amazon_ca_template_att_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_category`
--

DROP TABLE IF EXISTS `amazon_ca_template_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_category_attributes`
--

DROP TABLE IF EXISTS `amazon_ca_template_category_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL COMMENT 'set to true if the attribute is generic to the category id',
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_value_cat`
--

DROP TABLE IF EXISTS `amazon_ca_template_value_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_value_cat` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_catid` bigint(20) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_template_value_pid`
--

DROP TABLE IF EXISTS `amazon_ca_template_value_pid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_template_value_pid` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_field` varchar(255) NOT NULL,
  `amz_value` varchar(255) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_tlid_map`
--

DROP TABLE IF EXISTS `amazon_ca_tlid_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `slid` bigint(20) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_tlid_map_secondary`
--

DROP TABLE IF EXISTS `amazon_ca_tlid_map_secondary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazon_ca_unshipped_orders`
--

DROP TABLE IF EXISTS `amazon_ca_unshipped_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazon_ca_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;




--
-- Table structure for table `fba_ca_age_report`
--

DROP TABLE IF EXISTS `fba_ca_age_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_feed_report`
--

DROP TABLE IF EXISTS `fba_ca_feed_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_inventory_mismatch`
--

DROP TABLE IF EXISTS `fba_ca_inventory_mismatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_line_items_import`
--

DROP TABLE IF EXISTS `fba_ca_line_items_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_order_mismatch`
--

DROP TABLE IF EXISTS `fba_ca_order_mismatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settelement`
--

DROP TABLE IF EXISTS `fba_ca_settelement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_items`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_items_cost`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_items_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_items_cost_newdata`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_items_cost_newdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_items_newdata`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_items_newdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_items_newdata` (
  `dataid` int(10) unsigned NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_items_promotion`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_items_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_adjustment_newdata`
--

DROP TABLE IF EXISTS `fba_ca_settlement_adjustment_newdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_adjustment_newdata` (
  `dataid` int(10) NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_order`
--

DROP TABLE IF EXISTS `fba_ca_settlement_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `repid` (`repid`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_order_items`
--

DROP TABLE IF EXISTS `fba_ca_settlement_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_order_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_order_items_cost`
--

DROP TABLE IF EXISTS `fba_ca_settlement_order_items_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_order_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_order_items_new`
--

DROP TABLE IF EXISTS `fba_ca_settlement_order_items_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_order_items_new` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_order_items_promotion`
--

DROP TABLE IF EXISTS `fba_ca_settlement_order_items_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_order_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_other`
--

DROP TABLE IF EXISTS `fba_ca_settlement_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_settlement_report`
--

DROP TABLE IF EXISTS `fba_ca_settlement_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator_items`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator_link`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator_network`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator_respons`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator_respons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_shipment_creator_respons_items`
--

DROP TABLE IF EXISTS `fba_ca_shipment_creator_respons_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_sku_map`
--

DROP TABLE IF EXISTS `fba_ca_sku_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fba_ca_skus`
--

DROP TABLE IF EXISTS `fba_ca_skus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fba_ca_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


ALTER TABLE `product_list` ADD `amazon_ca_asin` VARCHAR( 50 ) NOT NULL ,
ADD `amazon_ca_am_qty` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_std_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazon_ca_am_exp_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazon_ca_amazon_realtime` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_latency` INT( 11 ) NOT NULL ,
ADD `amazon_ca_use_asin` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_update_override` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_condition` ENUM( 'New', 'UsedLikeNew', 'VeryGood', 'Good', 'Acceptable', 'Refurbished' ) NOT NULL ;