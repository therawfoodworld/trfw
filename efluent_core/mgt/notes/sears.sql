CREATE TABLE IF NOT EXISTS `sears_base_messages` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `qty` int(10) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` smallint(5) NOT NULL DEFAULT '0',
  `deleteflag` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;


CREATE TABLE IF NOT EXISTS `sears_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sears_submissionid` varchar(50) DEFAULT NULL,
  `sears_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `brid` (`brid`),
  KEY `sears_submissionid` (`sears_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_campaigns_categories` (
  `campid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `sears_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sears_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','CBA','ORDERADJUSTMENT','CONVERTSKUSAFN','CONVERTSKUSMFN') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `sears_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sears_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `sears_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `divid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

 CREATE TABLE IF NOT EXISTS `sears_config` (
`id` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`sales_enabled` TINYINT( 3 ) NOT NULL ,
`sales_price` DOUBLE( 16, 2 ) NOT NULL ,
`sales_start` INT( 10 ) NOT NULL ,
`sales_end` INT( 10 ) NOT NULL ,
`sales_text` VARCHAR( 255 ) NOT NULL ,
`shipping_ground` TINYINT( 3 ) NOT NULL ,
`shipping_ground_cost` DOUBLE( 16, 2 ) NOT NULL ,
`shipping_expedited` TINYINT( 3 ) NOT NULL ,
`shipping_expedited_cost` DOUBLE( 16, 2 ) NOT NULL ,
`shipping_premium` TINYINT( 3 ) NOT NULL ,
`shipping_premium_cost` DOUBLE( 16, 2 ) NOT NULL ,
`free_shipping_start` INT( 10 ) NOT NULL ,
`free_shipping_end` INT( 10 ) NOT NULL ,
`free_shipping_text` VARCHAR( 255 ) NOT NULL ,
`condition_comments` VARCHAR( 255 ) NOT NULL ,
`price_in_cart` TINYINT( 3 ) NOT NULL ,
PRIMARY KEY ( `id` ) ,
INDEX ( `pid` )
) ENGINE = MYISAM ;

 ALTER TABLE `sears_config` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `sears_config` ADD `condition` VARCHAR( 255 ) NOT NULL AFTER `free_shipping_text` ;


 CREATE TABLE `sears_categories` (
`id` INT( 10 ) NOT NULL ,
`catid` INT( 10 ) NOT NULL ,
`parentid` INT( 10 ) NOT NULL ,
`catname` VARCHAR( 255 ) NOT NULL ,
`catfee` DOUBLE( 16, 2 ) NOT NULL ,
`fbm_elig_used` TINYINT( 1 ) NOT NULL ,
`fbm_elig_refurb` TINYINT( 1 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;

 CREATE TABLE `ecom`.`sears_categories_map` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`searscat` INT( 10 ) NOT NULL ,
`catid` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `sears_config` ADD `qty` INT( 10 ) NOT NULL ;


DROP TABLE IF EXISTS `sears_unshipped_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sears_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;