CREATE TABLE `newegg_campaign_lists` (
  `campid` bigint(20) NOT NULL default '0',
  `listid` bigint(20) NOT NULL default '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_campaigns` (
  `id` bigint(20) NOT NULL auto_increment,
  `campaign_name` varchar(255) default NULL,
  `min_profit_pct` double(16,2) NOT NULL default '0.00',
  `min_profit_cur` double(16,2) NOT NULL default '0.00',
  `quantity` int(11) NOT NULL default '-1',
  `qty_control` int(11) NOT NULL default '80',
  `lastupdate` bigint(20) default NULL,
  `pricelevel` int(11) NOT NULL default '0',
  `min_qty` int(11) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_duplicates` (
  `did` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_errors` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`,`campid`,`requestid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_errors_other` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) default NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') default NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_feed_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','LISTINGEXPORT') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_listing` (
  `id` bigint(20) NOT NULL auto_increment,
  `campid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `newegg_pid` bigint(20) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` bigint(20) NOT NULL default '-1',
  `requestid` varchar(50) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_base_requests` (
  `brid` bigint(20) NOT NULL auto_increment,
  `newegg_submissionid` varchar(50) default NULL,
  `newegg_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `newegg_submissionid` (`newegg_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `newegg_base_messages` (
  `mid` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `brid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `pid_lastupdate` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `deleteflag` int(11) NOT NULL,
  `listid` int(11) NOT NULL,
  KEY `mid` (`mid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;