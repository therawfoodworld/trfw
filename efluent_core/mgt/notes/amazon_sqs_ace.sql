CREATE TABLE `amazon_sqs_offers_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `sqs_id` int(10) NOT NULL,
    `asin` varchar(100) NOT NULL,
    `seller_id` varchar(100) NOT NULL,
    `item_condition` tinyint(2) NOT NULL,
    `seller_positive_feedback_reating` int(10) NOT NULL,
    `seller_feedback_count` int(10) NOT NULL,
    `seller_shipping_availability_type` varchar(100) NOT NULL,
    `seller_listing_price` double(16,2) NOT NULL DEFAULT '0.00',
    `seller_shipping` double(16,2) NOT NULL DEFAULT '0.00',
    `seller_is_fulfilled` tinyint(2) NOT NULL,
    `seller_is_box_winner` tinyint(2) NOT NULL,
    `seller_condition_notes` text,
    PRIMARY KEY (`id`),
    KEY `sqs_id` (`sqs_id`),
    KEY `asin` (`asin`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_sqs_sales_rank_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `sqs_id` int(10) NOT NULL,
    `asin` varchar(100) NOT NULL,
    `category_id` varchar(100) NOT NULL,
    `rank` int(10) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `sqs_id` (`sqs_id`),
    KEY `asin` (`asin`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_sqs_summary_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `sqs_id` int(10) NOT NULL,
    `asin` varchar(100) NOT NULL,
    `used_amazon` int(10) NOT NULL,
    `used_merchant` int(10) NOT NULL,
    `new_amazon` int(10) NOT NULL,
    `new_merchant` int(10) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `sqs_id` (`sqs_id`),
    KEY `asin` (`asin`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_sqs_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `asin` varchar(100) NOT NULL,
    `uniqueid` varchar(255) NOT NULL,
    `tstamp` int(10) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_sqs_buybox_price_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `sqs_id` int(10) NOT NULL,
    `asin` varchar(100) NOT NULL,
    `item_condition` tinyint(2) NOT NULL,
    `price` double(16,2) NOT NULL DEFAULT '0.00',
    PRIMARY KEY (`id`),
    KEY `sqs_id` (`sqs_id`),
    KEY `asin` (`asin`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_sqs_lowest_price_ace` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `sqs_id` int(10) NOT NULL,
    `asin` varchar(100) NOT NULL,
    `fulfillment_channel` varchar(100) NOT NULL,
    `item_condition` tinyint(2) NOT NULL,
    `price` double(16,2) NOT NULL DEFAULT '0.00',
    PRIMARY KEY (`id`),
    KEY `sqs_id` (`sqs_id`),
    KEY `asin` (`asin`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


 CREATE TABLE `ecom`.`amazon_sqs_sales_rank_ace2` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`asin` VARCHAR( 255 ) NOT NULL ,
`rank` INT( 10 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
INDEX ( `asin` )
) ENGINE = MYISAM 