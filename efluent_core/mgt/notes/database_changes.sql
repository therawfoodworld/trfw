create table discontinued_replacedby
(
pid bigint NOT NULL default 0,
replacedby bigint NOT NULL default 0,
index(pid)
);

alter table product_search_extended add inlist int NOT NULL default 0;
update product_search_extended, list_products set product_search_extended.inlist = 1 where product_search_extended.pid = list_products.pid;

alter table product_kitskus add qty int NOT NULL default 1;
alter table product_sourcing add oprice double(16,2) NOT NULL default 0.00;
alter table product_sourcing add basecost int NOT NULL default 0;

alter table vendors add dropshipvendor int NOT NULL default 0;
alter table customer_billinginfo add debitcard int NOT NULL default 0; 
alter table customer_orders add allocated int NOT NULL default 0;

CREATE TABLE `amazon_tlid_map` (
  `tid` bigint(20) NOT NULL default '0',
  `tlid` bigint(20) NOT NULL default '0',
  `override_rid` int(11) NOT NULL default '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# the table below is mapped from the amazon_taxonomy page (from category manager)
CREATE TABLE `amazon_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL default '0',
  `tlid` bigint(20) NOT NULL default '0',
  `override_rid` int(11) NOT NULL default '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

create table amazon_ss_map
(
sscid bigint NOT NULL default 0, # Static Specs Column ID
amzkid bigint NOT NULL default 0, # Amazon Key ID
index(sscid)
);

CREATE TABLE `amazon_order_reports` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,  `ack` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1


CREATE TABLE `vendor_markup_rules` (
  `vendorid` bigint(20) NOT NULL default '0',
  `data` longblob,
  KEY `vendorid` (`vendorid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
create table product_profit_cached
(
pid bigint NOT NULL,
pl1pct double(16,2) NOT NULL default 0,
pl1cur double(16,2) NOT NULL default 0,
pl2pct double(16,2) NOT NULL default 0,
pl2cur double(16,2) NOT NULL default 0,
pl3pct double(16,2) NOT NULL default 0,
pl3cur double(16,2) NOT NULL default 0,
pl4pct double(16,2) NOT NULL default 0,
pl4cur double(16,2) NOT NULL default 0,
pl5pct double(16,2) NOT NULL default 0,
pl5cur double(16,2) NOT NULL default 0,
pl6pct double(16,2) NOT NULL default 0,
pl6cur double(16,2) NOT NULL default 0,
pl7pct double(16,2) NOT NULL default 0,
pl7cur double(16,2) NOT NULL default 0,
pl8pct double(16,2) NOT NULL default 0,
pl8cur double(16,2) NOT NULL default 0,
pl9pct double(16,2) NOT NULL default 0,
pl9cur double(16,2) NOT NULL default 0,
index(pid)
);
CREATE TABLE `amazon_categories` (
  `catid` bigint(20) NOT NULL default '0',
  `amazoncat` bigint(20) NOT NULL default '0',
  `pid` bigint(20) NOT NULL default '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

create table amazon_campaigns
(
id bigint NOT NULL auto_increment,
campaign_name varchar(255),
min_profit_pct double(16,2) NOT NULL default 0,
min_profit_cur double(16,2) NOT NULL default 0,
quantity int NOT NULL default -1,
qty_control int NOT NULL default 80,
pricelevel int NOT NULL default 0,
lastupdate bigint,
index(id)
);

create table amazon_campaign_lists
(
campid bigint NOT NULL default 0,
listid bigint NOT NULL default 0,
index(campid)
);
CREATE TABLE `amazon_base_requests` (
  `brid` bigint(20) NOT NULL auto_increment,
  `amazon_submissionid` varchar(50) default NULL,
  `amazon_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_base_messages` (
  `mid` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `brid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL default '',
  `pid_lastupdate` bigint(20) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

alter table amazon_base_messages add qty int NOT NULL default 0;
alter table amazon_base_messages add price double NOT NULL default 0.00;
alter table amazon_base_messages add listid int NOT NULL default 0;

CREATE TABLE `amazon_errors` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

alter table amazon_errors add requestid varchar(50);
alter table amazon_errors add index(requestid);

CREATE TABLE `amazon_errors_other` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50),
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING'),
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

 CREATE TABLE `amazon_feed_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED', 'ORDERADJUSTMENT') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

alter table amazon_feed_report change type type enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED', 'ORDERADJUSTMENT');

alter table product_sourcing add index(pid); # huge speed increase 
alter table datafeeds add options_nokits int NOT NULL default 0;

CREATE TABLE `amazon_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` bigint(20) NOT NULL default '-1',
  `requestid` varchar(50),
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE amazon_log_extended_skus
(
lid bigint NOT NULL,
pid bigint NOT NULL,
index(lid),
index(pid)
);

alter table product_list add am_latency int NOT NULL default 1;

create table background_jobs
(
jobid bigint NOT NULL auto_increment,
title varchar(255),
status enum('PENDING', 'RUNNING', 'COMPLETE'),
tstamp datetime,
index(jobid),
index(tstamp)
);

create table background_job_skus
(
jobid bigint NOT NULL auto_increment,
pid bigint NOT NULL,
listid bigint NOT NULL,
complete int NOT NULL default 0,
index(jobid),
index(complete)
);

alter table product_list add index(kit);
alter table product_list add index(visible);

alter table list_products add lorder int NOT NULL default 0;

create table amazon_unshipped_orders
(
invoiceid varchar(26),
paydate datetime,
dayslate int NOT NULL default 0,
index(invoiceid),
index(dayslate)
);

alter table ebay_listings add index(pid);

alter table product_list add use_asin int NOT NULL default 0;

CREATE TABLE `shop_categories` (
  `catid` bigint(20) NOT NULL default '0',
  `shopcat` bigint(20) NOT NULL default '0',
  `pid` bigint(20) NOT NULL default '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`shopcat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table customer_order_lines_log add tstamp datetime;
alter table customer_order_lines_log add index(tstamp);


alter table lists add lcid int NOT NULL default 0;

CREATE TABLE `list_categories` (
  `lcid` int(10) NOT NULL auto_increment,
  `lcname` varchar(255) NOT NULL,
  PRIMARY KEY  (`lcid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

create table traffic_spiders
(
id bigint NOT NULL auto_increment,
ip_addr varchar(20),
tstamp bigint NOT NULL default 0,
message varchar(255),
repeated int NOT NULL default 0,
index(id),
index(ip_addr),
index(tstamp)
);

create table traffic_spider_notes
(
ip_addr varchar(20),
note varchar(255),
index(ip_addr)
);


create table amazon_orders_uploaded
(
orderid bigint NOT NULL,
tstamp datetime default NULL,
index(orderid),
index(tstamp)
);

create table amazon_duplicates
(
did bigint NOT NULL auto_increment,
pid bigint NOT NULL,
tstamp datetime,
index(did),
index(pid),
index(tstamp)
);

create table amazon_duplicates_extended
(
did bigint NOT NULL default 0,
asin varchar(50),
salesrank varchar(20),
imageurl varchar(255),
brand varchar(50),
upc varchar(50),
features text,
lowestprice varchar(50),
index(did)
);

alter table product_list add index(sku);
alter table amazon_campaigns add min_qty int NOT NULL default 0;
alter table customer_orders  add pallocated int NOT NULL default 0;
alter table product_list add series int NOT NULL default 0;

alter table product_descr add divid int NOT NULL default 0;
alter table product_html add divid int NOT NULL default 0;
alter table product_images add divid int NOT NULL default 0;
alter table product_specs add divid int NOT NULL default 0;

CREATE TABLE `category_images_store` (
  `catid` bigint(20) default NULL,
  `banner` longblob,
  `banner_filename` varchar(255) default NULL,
  `thumb` longblob,
  `thumb_filename` varchar(255) default NULL,
  `divid` int(10) NOT NULL,
  `manid` int(10) NOT NULL,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table amazon_base_messages add deleteflag int NOT NULL default 0;
alter table banners_manager add divid int NOT NULL default 0
alter table company_orderflow add sendinvoice int NOT NULL default 0;
alter table email_templates_addon add sendinvoice int NOT NULL default 0;
alter table email_templates add sendinvoice int NOT NULL default 0;
alter table product_inventory add index(id, whid);
alter table user_permissions add perm_scanqty int NOT NULL default 0;

alter table amazon_duplicates_extended add numsellers int;
alter table amazon_duplicates_extended add title varchar(255);
ALTER TABLE amazon_duplicates_extended ADD manpart varchar( 255 ) ;

alter table ebay_campaigns add std_shipping double(16,2) NOT NULL default 0;
alter table ebay_campaigns add exp_shipping double(16,2) NOT NULL default 0;

alter table ebay_campaigns add std_addon double(16,2) NOT NULL default -1;
alter table ebay_campaigns add exp_addon double(16,2) NOT NULL default -1;

alter table product_list add eb_std_ship double(16,2) NOT NULL default -1;
alter table product_list add eb_exp_ship double(16,2) NOT NULL default -1;

CREATE TABLE `ebay_pid` (
  `upccode` varchar(15) default NULL,
  `epid` varchar(50) default NULL,
  `itemid` varchar(255) default NULL,
  `category` varchar(255) default NULL,
  `onnum` int(11) default NULL,
  KEY `upccode` (`upccode`),
  KEY `epid` (`epid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

create table ebay_ship_methods
(
code varchar(64),
descr text,
index(code)
);

create table ebay_campaign_methods
(
id bigint NOT NULL auto_increment,
campid bigint NOT NULL,
code varchar(64),
shipping double(16,2),
addon double(16,2),
index(id),
index(campid)
);

alter table product_prices add fco1 int NOT NULL default 0;
alter table product_prices add fco2 int NOT NULL default 0;
alter table product_prices add fco3 int NOT NULL default 0;
alter table product_prices add fco4 int NOT NULL default 0;
alter table product_prices add fco5 int NOT NULL default 0;
alter table product_prices add fco6 int NOT NULL default 0;
alter table product_prices add fco7 int NOT NULL default 0;
alter table product_prices add fco8 int NOT NULL default 0;
alter table product_prices add fco9 int NOT NULL default 0;


alter table product_list add am_update_override int NOT NULL default 0;
alter table product_list add am_condition enum('New', 'LikeNew', 'VeryGood', 'Good', 'Acceptable', 'Refurbished');

alter table shipping_codes add method_html_descr longblob;

create table custom_tabs
(
tab1 int NOT NULL default 1,
tab2 int NOT NULL default 1,
tab3 int NOT NULL default 1,
tab4 int NOT NULL default 1,
tab5 int NOT NULL default 1,
tab6 int NOT NULL default 1,
tab7 int NOT NULL default 1,
tab8 int NOT NULL default 1,
tab9 int NOT NULL default 1,
tab10 int NOT NULL default 1,
tab11 int NOT NULL default 1,
tab12 int NOT NULL default 1,
tab13 int NOT NULL default 1,
tab14 int NOT NULL default 1,
tab15 int NOT NULL default 1,
tab16 int NOT NULL default 1,
tab17 int NOT NULL default 1,
tab18 int NOT NULL default 1,
tab19 int NOT NULL default 1
);


alter table datafeeds add utf8 int NOT NULL default 0;

################################################################################################

CREATE TABLE `bundles` (
  `apid` bigint(20) NOT NULL AUTO_INCREMENT,
  `packagename` varchar(255) DEFAULT NULL,
  `packagetitle` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `subtitlecfid` int(11) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `regenerate` int(11) NOT NULL DEFAULT '0',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  KEY `apid` (`apid`),
  KEY `packagename` (`packagename`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

alter table bundles add bdate datetime;
alter table bundles add edate datetime;

CREATE TABLE `bundles_criteria` (
  `apcid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `min` double(16,2) NOT NULL DEFAULT '0.00',
  `max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `apcid` (`apcid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `bundles_addskus` (
  `apsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `dollar` double(16,2) NOT NULL default 0.00,
  `percent` double(16,2) NOT NULL default 0.00,
  KEY `apsid` (`apsid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE `bundles_skus` (
  `pid` bigint(20) NOT NULL,
  `apid` bigint(20) NOT NULL,
  `mainid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `apid` (`apid`),
  KEY `pid` (`pid`),
  KEY `mainid` (`mainid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


alter table bundles_skus add bundleid varchar(128);

alter table datafeeds add mdollar double(16,2);
alter table datafeeds add mpercent double(16,2);

CREATE TABLE `historical_stas` (
  `promoid` bigint(20) default NULL,
  `promoname` varchar(255) default NULL,
  `amount` double(16,2) default NULL,
  `note` varchar(50) default NULL,
  `pid` bigint(20) default NULL,
  `begin_date` datetime default NULL,
  `end_date` datetime default NULL,
  KEY `promoid` (`promoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table site_list add domain varchar(255) NOT NULL default '';

###########################################################################################################
##  Static Specs / Category Drilldown Tables

mysql> select * from static_specs_columns where dkey = 'Dimensions~Capacity' and tid = '80';
+------+------+---------------------+--------+-------+
| id   | tid  | dkey                | dorder | dtype |
+------+------+---------------------+--------+-------+
| 1263 |   80 | Dimensions~Capacity |      0 |       |
+------+------+---------------------+--------+-------+


id => 1
catid => 2011
catname => "Washers"
ltype => ss (static specs / category / brand)  * listing type
datatype => 1 (1 = int / 2 = text)
tid => 80
colid => 1263
name => "Shop by Style"


create table drilldown_main
(
id bigint NOT NULL auto_increment,
catid bigint,
ltype enum('SS', 'CAT', 'BRND', 'PRICE'),
dtype int NOT NULL default 0, # (1 = int / 2 = text)
tid bigint NOT NULL,
colid bigint NOT NULL, # colid = product_specs
catname varchar(128),
title varchar(128),
index(id),
index(catid)
);

# select distinct(dvalue) from product_static_specs where tid = '80' and sscid = '1263';
# product_static_specs.id = pid

# select id, cast(dvalue as DECIMAL(16,2)) from product_static_specs where tid = '80' and sscid = '1263' and cast(dvalue as DECIMAL(16,2)) between '2' AND '2.9'

insert into drilldown_main(catid, ltype, dtype, tid, colid, catname, title) values('2011', 'SS', '1', '80', '1263', 'Washers', 'Shop by Capacity');
insert into drilldown_main(catid, ltype, dtype, tid, colid, catname, title) values('2011', 'CAT', '2', '80', '1263', 'Washers', 'Shop by Style');
insert into drilldown_main(catid, ltype, dtype, tid, colid, catname, title) values('2011', 'SS', '1', '80', '1260', 'Washers', 'Shop by Width');

create table drilldown_options
(
id bigint NOT NULL auto_increment,
dmid bigint NOT NULL,
minval double(16,2),
maxval double(16,2), 
title varchar(255),
index(id),
index(dmid)
);

insert into drilldown_options(dmid, minval, maxval, title) values('1', '0.00', '0.9', '0 - .9 (cu ft.)');
insert into drilldown_options(dmid, minval, maxval, title) values('1', '1.00', '1.9', '1 - 1.9 (cu ft.)');
insert into drilldown_options(dmid, minval, maxval, title) values('1', '2.00', '2.9', '2 - 2.9 (cu ft.)');
insert into drilldown_options(dmid, minval, maxval, title) values('1', '3.00', '3.9', '3 - 3.9 (cu ft.)');
insert into drilldown_options(dmid, minval, maxval, title) values('1', '4.00', '4.9', '4 - 4.9 (cu ft.)');


insert into drilldown_options(dmid, minval, maxval, title) values('4', '18.00', '20.9', '18" - 20.9"');
insert into drilldown_options(dmid, minval, maxval, title) values('4', '21.00', '23.9', '21" - 23.9"');
insert into drilldown_options(dmid, minval, maxval, title) values('4', '24.00', '26.9', '24" - 26.9"');
insert into drilldown_options(dmid, minval, maxval, title) values('4', '27.00', '29.9', '27" - 29.9"');

#  if drilldown_main.ltype 
# 'SS' => Static Specs => then its cached into drilldown_cache
# 'CAT' => Sub Category => not cached
# 'BRND' => Brand => not cached


create table drilldown_cache # for static specs only
(
id bigint NOT NULL auto_increment,
dmid bigint NOT NULL,
doid bigint NOT NULL,
pid bigint NOT NULL,
index(id),
index(dmid),
index(doid)
);

################################################################################################################################


ALTER TABLE `vendor_product_list` ADD `item_id` VARCHAR( 15 ) NOT NULL ;
alter table vendor_product_list add index(item_id);


################################################################################################################################

CREATE TABLE `shop_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


################################################################################################################################


create table fba_sku_map
(
fba_sku varchar(32),
pid bigint NOT NULL,
index(fba_sku),
index(pid)
);

create table fba_feed_report
(
id bigint NOT NULL auto_increment,
type varchar(32),
tstamp datetime,
reportid varchar(50),
requestid varchar(50),
filename varchar(255),
complete int NOT NULL default 0,
index(id),
index(type)
);

create table fba_inventory_mismatch
(
id bigint NOT NULL auto_increment,
sku varchar(32),
fcsku varchar(32),
asin varchar(32),
cond varchar(32),
cond_code varchar(32),
qty int NOT NULL default 0,
index(id),
index(sku)
);

create table fba_order_mismatch
(
id bigint NOT NULL auto_increment,
invoiceid varchar(32),
sku varchar(32),
index(id),
index(sku)
);

create table fba_skus
(
id bigint NOT NULL auto_increment,
sku varchar(32),
index(id),
index(sku)
);
alter table fba_skus add lastupdate bigint NOT NULL default 0;
alter table fba_skus add fcsku varchar(32) NOT NULL default '';
alter table fba_skus add updateprice int NOT NULL default 0;

################################################################################################################################

alter table ebay_campaigns add calculated_uspspmi int NOT NULL default 0;
alter table ebay_campaigns add calculated_uspsexi int NOT NULL default 0;
alter table ebay_campaigns add calculated_upswwsi int NOT NULL default 0;

alter table product_categories add amazonpct double(16,2) NOT NULL default 0.00;

create table cardinal_results
(
orderid bigint NOT NULL,
tstamp datetime,
xid varchar(128),
cavv varchar(128),
eci varchar(26),
message varchar(255),
index(orderid)
);

create table autoprice
(
pid bigint NOT NULL,
aggressive_factor double(16,2),
minimum_profit double(16,2),
profit_type enum('DOLLAR', 'PERCENT'),
use_avgcost enum('AVGCOST', 'WAREHOUSE', 'VENDOR'),
index(pid)
);
alter table autoprice add maximum_profit double(16,2) default 0.00;
alter table autoprice add max_profit_type enum('DOLLAR', 'PERCENT');
alter table autoprice add commission double(16,2) default 0.00;
alter table autoprice add ceiling_price double(16,2) default 0.00;
alter table autoprice add fight_amazon int NOT NULL default 0;
alter table autoprice add add_fba_cost int NOT NULL default 0;
alter table autoprice add minprice double(16,2) default 0.00;

create table autoprice_extended
(
pid bigint NOT NULL,
avgcost_id int,
index(pid)
);

create table additional_costs
(
id bigint NOT NULL auto_increment,
invoiceid varchar(26),
title varchar(255),
note varchar(255),
cost double(16,2),
index(id),
index(invoiceid)
);

############################################################################################################################
# AutoPrice Tables
create table autoprice_batches
(
id bigint NOT NULL auto_increment,
jobid bigint NOT NULL, # jobid from roilogic
tstamp datetime,
status varchar(255), # completed, running, no XXXX configured
numskus int NOT NULL default 0,
pctcomplete double NOT NULL default 0.00,
index(id),
index(tstamp)
);

create table autoprice_batches_skus
(
id bigint NOT NULL auto_increment,
apbid bigint NOT NULL,
pid bigint,
result text(255),
complete int NOT NULL default 0,
index(id),
index(apbid),
index(pid)
);


create table ebay_cancels
(
id bigint NOT NULL auto_increment,
orderid bigint,
disputeid varchar(255),
correlationid varchar(255),
tstamp datetime,
index(id),
index(orderid)
);
alter table ebay_cancels add complete int NOT NULL default 0;


create table autoprice_amazon
(
pid bigint NOT NULL default 0,
tstamp datetime,
price double(16,2),
index(pid)
);

############################################################################################################################
# Pricing Scheduler

create table pricing_schedule
(
id bigint NOT NULL auto_increment,
name varchar(255),
listid bigint NOT NULL default 0,
belowcost int NOT NULL default 0,
active int NOT NULL default 0,
pl1 int NOT NULL default 0,
pl2 int NOT NULL default  0,
pl3 int NOT NULL default  0,
pl4 int NOT NULL default  0,
pl5 int NOT NULL default  0,
pl6 int NOT NULL default  0,
pl7 int NOT NULL default  0,
pl8 int NOT NULL default  0,
pl9 int NOT NULL default  0,
index(id)
);

create table pricing_schedule_extended
(
id bigint NOT NULL auto_increment,
psid bigint NOT NULL,
startday bigint NOT NULL default -1,
starthour bigint NOT NULL default -1,
startmin bigint NOT NULL default -1,
endday bigint NOT NULL default -1,
endhour bigint NOT NULL default -1,
endmin bigint NOT NULL default -1,
index(id),
index(psid)
);

create table pricing_schedule_prices
(
id bigint NOT NULL auto_increment,
psid bigint NOT NULL,
pid bigint NOT NULL default 0,
pl1 double(16,2) default 0.00,
pl2 double(16,2) default 0.00,
pl3 double(16,2) default 0.00,
pl4 double(16,2) default 0.00,
pl5 double(16,2) default 0.00,
pl6 double(16,2) default 0.00,
pl7 double(16,2) default 0.00,
pl8 double(16,2) default 0.00,
pl9 double(16,2) default 0.00,
index(id),
index(psid, pid)
);

create table pricing_schedule_prior
(
id bigint NOT NULL auto_increment,
psid bigint NOT NULL,
pid bigint NOT NULL default 0,
pl1 double(16,2) default 0.00,
pl2 double(16,2) default 0.00,
pl3 double(16,2) default 0.00,
pl4 double(16,2) default 0.00,
pl5 double(16,2) default 0.00,
pl6 double(16,2) default 0.00,
pl7 double(16,2) default 0.00,
pl8 double(16,2) default 0.00,
pl9 double(16,2) default 0.00,
index(id),
index(psid, pid)
);


############################################################################################################################
# image_cache stuff

create table product_images_md5
(
id bigint NOT NULL default 0,
itype int,
thumb int,
md5 varchar(32),
index(id, itype, thumb)
);

############################################################################################################################
# 

alter table ebay_campaigns add mainimage int NOT NULL default 0;
alter table ebay_campaigns add useconfig char(1) default 'A';

alter table vendor_pos add paid int NOT NULL default 0;
alter table vendor_pos add paid_amount double(16,2);
alter table vendor_pos add mgr int NOT NULL default 0;

create table vendor_pos_extended
(
poid bigint NOT NULL,
invoiceid varchar(26),
index(poid),
index(invoiceid)
);

################################################################################################################
CREATE TABLE `buy_base_messages` (
  `mid` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `brid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `pid_lastupdate` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `deleteflag` int(11) NOT NULL,
  `listid` int(11) NOT NULL,
  KEY `mid` (`mid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=452290 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_base_requests` (
  `brid` bigint(20) NOT NULL auto_increment,
  `buy_submissionid` varchar(50) default NULL,
  `buy_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `buy_submissionid` (`buy_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=5135 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_campaign_lists` (
  `campid` bigint(20) NOT NULL default '0',
  `listid` bigint(20) NOT NULL default '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_campaigns` (
  `id` bigint(20) NOT NULL auto_increment,
  `campaign_name` varchar(255) default NULL,
  `min_profit_pct` double(16,2) NOT NULL default '0.00',
  `min_profit_cur` double(16,2) NOT NULL default '0.00',
  `quantity` int(11) NOT NULL default '-1',
  `qty_control` int(11) NOT NULL default '80',
  `lastupdate` bigint(20) default NULL,
  `pricelevel` int(11) NOT NULL default '0',
  `min_qty` int(11) NOT NULL default '0',
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_categories` (
  `catid` int(10) NOT NULL,
  `buycat` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_categories_api` (
  `id` bigint(20) NOT NULL auto_increment,
  `buycat` int(10) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3933 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_categories_pid` (
  `pid` bigint(20) NOT NULL,
  `buycat` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_duplicates` (
  `did` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_errors` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`,`campid`,`requestid`)
) ENGINE=MyISAM AUTO_INCREMENT=15684 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_errors_other` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` bigint(20) NOT NULL default '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) default NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') default NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_feed_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','LISTINGEXPORT') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=8193 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_feed_respons` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_listing` (
  `id` bigint(20) NOT NULL auto_increment,
  `campid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `buy_pid` bigint(20) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` bigint(20) NOT NULL default '-1',
  `requestid` varchar(50) default NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=15527 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=8187 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `buy_log_orders` (
  `id` bigint(20) NOT NULL auto_increment,
  `invoiceid` varchar(50) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `shipped` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;

CREATE TABLE `buy_log_orders_items` (
  `id` bigint(20) NOT NULL auto_increment,
  `invoiceid` varchar(50) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `receipt_item_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;


create table recalc_inventory_queue
(
pid bigint NOT NULL,
PRIMARY KEY (pid)
);

###########################################################################
ALTER TABLE `customer_orders` ADD `fbaid` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `customer_orders` ADD `assigned` BIGINT( 20 ) NOT NULL ;
ALTER TABLE `vendor_pos` ADD `assigned` BIGINT( 20 ) NOT NULL AFTER `mgr`, ADD `fbaid` VARCHAR( 50 ) NOT NULL AFTER `assigned`;

###########################################################################
create table shipping_defaults
(
id bigint NOT NULL auto_increment,
carrier varchar(255),
method varchar(255),
options varchar(255) NOT NULL default '',
index(id),
index(carrier, method)
);

ALTER TABLE `customer_orders` ADD `solicited` TINYINT( 1 ) NOT NULL DEFAULT '0';

#########################################################################################
# Web Services Log Tables

create table ws_inventory_import
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
index(id),
index(tstamp)
);

create table ws_inventory_import_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
sku varchar(255),
success int NOT NULL default 0,
index(id),
index(wsid),
index(sku),
index(success)
);

#----------------------------------------------------------------------------------

create table ws_flowstep
(
id bigint NOT NULL auto_increment,
tstamp datetime,
invoiceid varchar(26),
status int NOT NULL default 0,
index(id),
index(tstamp)
);

#----------------------------------------------------------------------------------

create table ws_products_import
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
pnew int NOT NULL default 0,
pupdated int NOT NULL default 0,
index(id),
index(tstamp)
);

create table ws_products_import_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
sku varchar(255),
success int NOT NULL default 0, # 0 = updated, 1 created
status varchar(255),
index(id),
index(wsid),
index(sku),
index(success)
);

#----------------------------------------------------------------------------------

create table ws_receive_order
(
id bigint NOT NULL auto_increment,
success int NOT NULL default 0,
tstamp datetime,
invoiceid varchar(255),
ip_addr varchar(25),
title varchar(255),
index(id),
index(success),
index(tstamp),
index(invoiceid)
);
#----------------------------------------------------------------------------------

create table ws_posted_orders
(
orderid bigint NOT NULL,
index(orderid)
);


create table ws_posted_orders_log
(
id bigint NOT NULL auto_increment,
success int NOT NULL default 0,
tstamp datetime,
title varchar(255),
orderid bigint NOT NULL,
invoiceid varchar(26),
result varchar(255),
index(id),
index(tstamp)
);
#----------------------------------------------------------------------------------

create table ws_picking_export
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
index(id),
index(tstamp)
);

create table ws_picking_export_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
invoiceid varchar(26),
lineid bigint,
location int,
sku varchar(50),
qty_picked int,
pickedtime datetime,
pickeduser varchar(50),
lastupdated datetime,
currentorderstep int,
index(id),
index(wsid)
);

#----------------------------------------------------------------------------------

create table ws_picking_import
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
index(id),
index(tstamp)
);

create table ws_picking_import_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
sku varchar(50),
qty_ordered int,
qty_picked int,
ordersource varchar(26),
qty_shipped int,
qty_cancelled int,
status int,
index(id),
index(wsid)
);

#----------------------------------------------------------------------------------

create table ws_orders_uploaded
(
orderid bigint NOT NULL,
tstamp datetime default NULL,
index(orderid),
index(tstamp)
);

#----------------------------------------------------------------------------------

create table ws_alias_import
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
index(id),
index(tstamp)
);

create table ws_alias_import_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
pid bigint NOT NULL,
parentid bigint NOT NULL,
channel varchar(26),
aliassku varchar(26),
asin varchar(26),
upccode varchar(26),
price double(16,2),
index(id),
index(wsid),
index(aliassku)
);

create table ws_ship_log
(
id bigint NOT NULL auto_increment,
tstamp datetime,
title varchar(255),
index(id),
index(tstamp)
);

create table ws_ship_log_extended
(
id bigint NOT NULL auto_increment,
wsid bigint NOT NULL,
pid bigint NOT NULL,
sku varchar(26),
linkedsku varchar(26),
descr varchar(255),
qty_shipped int,
price double(16,2),
orderid bigint,
invoiceid varchar(26),
snd_lineid bigint,
shipdate datetime,
cost double(16,2),
lastupdated datetime,
status int,
tid bigint,
qty_ordered int,
index(id),
index(wsid),
index(pid)	
);
#########################################################################################

create table IF NOT EXISTS login_log
(
id bigint NOT NULL auto_increment,
success int NOT NULL default '0', # 0 = failure, 1 = success
tstamp datetime,
un varchar(26),
ip_addr varchar(26),
descr varchar(255),
index(id),
index(ip_addr)
);


#########################################################################################

CREATE TABLE IF NOT EXISTS `amazon_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


#########################################################################################
ALTER TABLE `amazon_feedback` ADD `notes` TEXT NOT NULL ,
ADD `solicited` INT( 5 ) NOT NULL ,
ADD `removed` INT( 5 ) NOT NULL ;

#########################################################################################
CREATE TABLE `amazon_order_reports_cba` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
#########################################################################################
create table snd_orders_uploaded
(
orderid bigint NOT NULL,
tstamp datetime NOT NULL,
index(orderid),
index(tstamp)
);
#########################################################################################
CREATE TABLE IF NOT EXISTS `user_search_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `user_search_filters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

#########################################################################################

CREATE TABLE `ecom`.`multipack_upc_codes` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`code` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 

#########################################################################################

CREATE TABLE IF NOT EXISTS `user_search_order_filters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
#########################################################################################

CREATE TABLE IF NOT EXISTS `amazon_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazon_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `amazon_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazon_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  PRIMARY KEY (`attid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `amazon_template_value_pid` (
`amz_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`amz_pid` BIGINT( 20 ) NOT NULL ,
`amz_field` VARCHAR( 255 ) NOT NULL ,
`amz_value` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `amazon_template_value_cat` (
`amz_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`amz_pid` BIGINT( 20 ) NOT NULL ,
`amz_catid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM ;
#########################################################################################
CREATE TABLE IF NOT EXISTS `action_log_inventory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ltype` enum('SUCCESS','FAILURE') DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `updateid` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `ltype` (`ltype`,`tstamp`),
  KEY `type` (`type`,`updateid`,`tstamp`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1

CREATE TABLE IF NOT EXISTS `action_log_inventory_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) DEFAULT NULL,
  `details` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
#########################################################################################
CREATE TABLE IF NOT EXISTS `customer_orders_stock_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
#########################################################################################

CREATE TABLE `fba_settlement_adjustment_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM; 
CREATE TABLE `fba_settlement_adjustment_items_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM; 
CREATE TABLE `fba_settlement_adjustment_items_cost_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `fba_settlement_order_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM; 
CREATE TABLE `fba_settlement_order_items_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM ;
CREATE TABLE `fba_settlement_order_items_cost_newdata` (
`dataid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM ;


CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;
ALTER TABLE `fba_settlement_adjustment_items` ADD `adj_id` BIGINT( 20 ) NOT NULL AFTER `id` ;
ALTER TABLE `fba_settlement_adjustment_items_cost` ADD `adj_id` BIGINT( 20 ) NOT NULL AFTER `id`;

ALTER TABLE `buy_prelisting` ADD `tstamp` DATETIME NOT NULL ;
ALTER TABLE `buy_prelisting` ADD PRIMARY KEY ( `pid` ) 

alter table amazon_shipping_map add weight_min double(16,2) NOT NULL default 0.00;
alter table amazon_shipping_map add weight_max double(16,2) NOT NULL default 0.00;
alter table amazon_shipping_map add cost_min double(16,2) NOT NULL default 0.00;
alter table amazon_shipping_map add cost_max double(16,2) NOT NULL default 0.00;

alter table amazonzd_shipping_map add weight_min double(16,2) NOT NULL default 0.00;
alter table amazonzd_shipping_map add weight_max double(16,2) NOT NULL default 0.00;
alter table amazonzd_shipping_map add cost_min double(16,2) NOT NULL default 0.00;
alter table amazonzd_shipping_map add cost_max double(16,2) NOT NULL default 0.00;

weight min/max  and   cost min/max


CREATE TABLE IF NOT EXISTS `btg_categories` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  `nodeid` bigint(20) NOT NULL,
  `query` varchar(255) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btg_categories_map`
--

CREATE TABLE IF NOT EXISTS `btg_categories_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL,
  `btgcat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btg_categories_pid_map`
--

CREATE TABLE IF NOT EXISTS `btg_categories_pid_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `btgcat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `product_categories_description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL,
  `description` blob NOT NULL,
  `divid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE `order_shipped_globegistic` (
`id` BIGINT( 20 ) NOT NULL ,
`orderid` BIGINT( 20 ) NOT NULL ,
`in_report` TINYINT( 5 ) NOT NULL DEFAULT '0'
) ENGINE = MYISAM 

ALTER TABLE `order_shipped_globegistic` ADD PRIMARY KEY(`id`)
ALTER TABLE `order_shipped_globegistic` CHANGE `id` `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT 


-- AMAZON CANADA TABLES --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `amazon_ca_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` bigint(20) DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_campaign_lists` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_categories`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `amazoncat` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_duplicates` (
  `did` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_errors`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_errors_other` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feedback`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping_extra` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ais_id` bigint(20) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` bigint(2) NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping_files`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `amazon_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_qty_scripts` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_relationships`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_shipping_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `amazon_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazonzd_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `amazon_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------

--
-- Table structure for table `amazon_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_att`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `catid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_category`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL COMMENT 'set to true if the attribute is generic to the category id',
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1954 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_value_cat`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_value_cat` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_catid` bigint(20) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_value_pid`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_value_pid` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_field` varchar(255) NOT NULL,
  `amz_value` varchar(255) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `slid` bigint(20) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


ALTER TABLE `product_list` ADD `amazon_ca_asin` VARCHAR( 50 ) NOT NULL ,
ADD `amazon_ca_am_qty` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_std_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazon_ca_am_exp_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazon_ca_amazon_realtime` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_latency` INT( 11 ) NOT NULL ,
ADD `amazon_ca_use_asin` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_update_override` INT( 11 ) NOT NULL ,
ADD `amazon_ca_am_condition` ENUM( 'New', 'UsedLikeNew', 'VeryGood', 'Good', 'Acceptable', 'Refurbished' ) NOT NULL ;

alter table amazon_ca_shipping_map add weight_min double(16,2) NOT NULL default 0.00;
alter table amazon_ca_shipping_map add weight_max double(16,2) NOT NULL default 0.00;
alter table amazon_ca_shipping_map add cost_min double(16,2) NOT NULL default 0.00;
alter table amazon_ca_shipping_map add cost_max double(16,2) NOT NULL default 0.00;

create table amazon_ca_unshipped_orders
(
invoiceid varchar(26),
paydate datetime,
dayslate int NOT NULL default 0,
index(invoiceid),
index(dayslate)
);
CREATE TABLE `amazon_ca_base_requests` (
  `brid` bigint(20) NOT NULL auto_increment,
  `amazon_submissionid` varchar(50) default NULL,
  `amazon_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

create table amazon_ca_duplicates_extended
(
did bigint NOT NULL default 0,
asin varchar(50),
salesrank varchar(20),
imageurl varchar(255),
brand varchar(50),
upc varchar(50),
features text,
lowestprice varchar(50),
index(did)
);

 CREATE TABLE `amazon_ca_feed_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



CREATE TABLE `amazon_ca_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` bigint(20) NOT NULL default '-1',
  `requestid` varchar(50),
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_ca_log_extended` (
  `id` bigint(20) NOT NULL auto_increment,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE amazon_ca_log_extended_skus
(
lid bigint NOT NULL,
pid bigint NOT NULL,
index(lid),
index(pid)
);


CREATE TABLE `amazon_ca_order_reports` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

create table amazon_ca_orders_uploaded
(
orderid bigint NOT NULL,
tstamp datetime default NULL,
index(orderid),
index(tstamp)
);

CREATE TABLE `amazon_ca_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL default '0',
  `tlid` bigint(20) NOT NULL default '0',
  `override_rid` int(11) NOT NULL default '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1


CREATE TABLE `shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `order_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `weight_min` double(16,2) NOT NULL DEFAULT '0.00',
  `weight_max` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_min` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`order_method`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `ebay_cases` ADD `case_account` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `fba_shipment_creator` ADD `assigned` INT( 10 ) NOT NULL ;


# EBAY MESSAGE CENTER

 CREATE TABLE `ecom`.`ebay_message_center` (
`emc_id` BIGINT( 20 ) NOT NULL ,
`emc_status` VARCHAR( 50 ) NOT NULL ,
`emc_date_created` VARCHAR( 50 ) NOT NULL ,
`emc_last_modified` VARCHAR( 50 ) NOT NULL ,
`emc_item_id` BIGINT( 20 ) NOT NULL ,
`emc_question_id` BIGINT( 20 ) NOT NULL ,
PRIMARY KEY ( `emc_id` )
) ENGINE = MYISAM ;
 ALTER TABLE `ebay_message_center` CHANGE `emc_id` `emc_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT 
ALTER TABLE `ebay_message_center` ADD `emc_assignedto` INT( 5 ) NOT NULL ,
ADD `emc_followup` INT( 5 ) NOT NULL ;
ALTER TABLE `ebay_message_center` ADD `emc_notes` BLOB NOT NULL ;
ALTER TABLE `ebay_message_center` ADD `emc_site` VARCHAR( 10 ) NOT NULL AFTER `emc_id` ;

 CREATE TABLE `ecom`.`ebay_message_center_item` (
`emci_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`emci_ebayid` BIGINT( 20 ) NOT NULL ,
`emci_listing_start_time` VARCHAR( 50 ) NOT NULL ,
`emci_listing_end_time` VARCHAR( 50 ) NOT NULL ,
`emci_listing_url` VARCHAR( 255 ) NOT NULL ,
`emci_sellerid` VARCHAR( 50 ) NOT NULL ,
`emci_price` DOUBLE( 16, 2 ) NOT NULL ,
`emci_timeleft` VARCHAR( 50 ) NOT NULL ,
`emci_title` VARCHAR( 255 ) NOT NULL ,
`emci_condition_id` INT( 10 ) NOT NULL ,
`emci_condition_name` VARCHAR( 50 ) NOT NULL
) ENGINE = MYISAM ;

 CREATE TABLE `ecom`.`ebay_message_center_question` (
`emcq_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`emcq_message_type` VARCHAR( 50 ) NOT NULL ,
`emcq_question_type` VARCHAR( 50 ) NOT NULL ,
`emcq_display_public` VARCHAR( 10 ) NOT NULL ,
`emcq_sender_id` VARCHAR( 50 ) NOT NULL ,
`emcq_sender_email` VARCHAR( 255 ) NOT NULL ,
`emcq_recipient_id` VARCHAR( 255 ) NOT NULL ,
`emcq_subject` TEXT NOT NULL ,
`emcq_body` TEXT NOT NULL ,
`emcq_message_id` VARCHAR( 50 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `ebay_message_center_question` ADD `emcq_item_id` VARCHAR( 255 ) NOT NULL ;

CREATE TABLE `ecom`.`ebay_message_center_question_response` (
`emcqr_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`emcqr_qid` BIGINT( 20 ) NOT NULL ,
`emcqr_public` INT( 5 ) NOT NULL ,
`emcqr_copy` INT( 5 ) NOT NULL ,
`emcqr_response` TEXT NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `ebay_message_center_question_response` ADD `emcqr_userid` INT( 10 ) NOT NULL ;

create table amazon_refunds
(
invoiceid varchar(26) NOT NULL,
orderid bigint NOT NULL,
ampid varchar(24) NOT NULL,
creditid varchar(26) NOT NULL,
reportid varchar(50) NOT NULL,
tstamp datetime NOT NULL,
index(orderid, ampid),
index(invoiceid),
index(creditid),
index(tstamp)
);
alter table amazon_refunds add status varchar(255) NOT NULL default '';


create table amazon_campaigns_categories
(
campid bigint NOT NULL,
catid bigint NOT NULL,
index(campid)
);

 CREATE TABLE `ecom`.`superpricer` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
`min_price` DOUBLE( 16, 2 ) NOT NULL ,
`max_price` DOUBLE( 16, 2 ) NOT NULL ,
`map_price` DOUBLE( 16, 2 ) NOT NULL ,
`profile` VARCHAR( 255 ) NOT NULL ,
`group` VARCHAR( 255 ) NOT NULL ,
`managed` INT( 5 ) NOT NULL ,
`type` VARCHAR( 50 ) NOT NULL
) ENGINE = MYISAM 

 CREATE TABLE `ecom`.`superpricer_log` (
`log_id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`log_pid` BIGINT( 20 ) NOT NULL ,
`log_type` VARCHAR( 50 ) NOT NULL ,
`log_date` DATETIME NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `superpricer_log` ADD `log_sent` INT( 5 ) NOT NULL ;
 ALTER TABLE `superpricer` CHANGE `profile` `sp_profile` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `group` `sp_group` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `managed` `sp_managed` INT( 5 ) NOT NULL ,
CHANGE `type` `sp_type` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL 


CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;
 CREATE TABLE `amazon_order_reports_by_date_items` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`amazon_order_id` VARCHAR( 50 ) NOT NULL ,
`amazon_order_purchase_date` DATETIME NOT NULL ,
`amazon_order_status` VARCHAR( 50 ) NOT NULL ,
`amazon_sales_chanel` VARCHAR( 50 ) NOT NULL ,
`amazon_item_asin` VARCHAR( 50 ) NOT NULL ,
`amazon_item_sku` VARCHAR( 50 ) NOT NULL ,
`amazon_item_status` VARCHAR( 50 ) NOT NULL ,
`amazon_item_qty` SMALLINT( 5 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `amazon_order_reports_by_date_items` CHANGE `amazon_order_purchase_date` `amazon_order_purchase_date` INT( 10 ) NOT NULL 
ALTER TABLE `amazon_order_reports_by_date_items` ADD `amazon_order_purchase_date_string` VARCHAR( 100 ) NOT NULL ;
ALTER TABLE `amazon_order_reports_by_date` ADD `requestid` VARCHAR( 100 ) NOT NULL AFTER `reportid` ;
ALTER TABLE `amazon_order_reports_by_date` ADD `type` TINYINT( 5 ) NOT NULL ;
ALTER TABLE `amazon_order_reports_by_date_items` ADD `type` TINYINT( 5 ) NOT NULL ;

 CREATE TABLE `ebay_message_center_account_orders` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`account_id` VARCHAR( 100 ) NOT NULL ,
`nr_orders` TINYINT( 5 ) NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `ebay_message_center_account_orders` ADD `duplicates` TINYINT( 5 ) NOT NULL ;

create table sa_asin
(
pid bigint NOT NULL,
brand varchar(50),
title varchar(255),
manpart varchar(50),
asin varchar(50),
index(pid),
index(asin)
);

create table sa_crawled
(
pid bigint NOT NULL,
index(pid)
);

 CREATE TABLE IF NOT EXISTS `products_liveon` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
INDEX ( `pid` )
) ENGINE = MYISAM 

 CREATE TABLE IF NOT EXISTS `products_liveon_skus` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
INDEX ( `pid` )
) ENGINE = MYISAM 

CREATE TABLE IF NOT EXISTS `user_search_tabs` (
`id` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`userid` bigint( 20 ) DEFAULT NULL ,
`colname` varchar( 32 ) DEFAULT NULL ,
`dorder` int( 11 ) DEFAULT NULL ,
`coltitle` varchar( 128 ) NOT NULL DEFAULT 'N/A',
KEY `id` ( `id` ) ,
KEY `userid` ( `userid` )
) ENGINE = MYISAM DEFAULT CHARSET = latin1;

ALTER TABLE `user_search_tabs` ADD `permission` VARCHAR( 50 ) NOT NULL ,
ADD `db` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `amazon_order_reports_by_date_items` ADD `pid` BIGINT( 20 ) NOT NULL AFTER `amazon_item_sku` ;

 CREATE TABLE `products_sourced_details` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
`last_tstamp` DATETIME NOT NULL ,
`total_qty` INT( 10 ) NOT NULL
) ENGINE = MYISAM 
 ALTER TABLE `products_sourced_details` ADD INDEX ( `pid` ) 

ALTER TABLE `customer_order_tracking` ADD `istracked` TINYINT( 5 ) NOT NULL ;


CREATE TABLE IF NOT EXISTS `listing_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `belowcost` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `listing_schedule_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psid` bigint(20) NOT NULL,
  `startday` bigint(20) NOT NULL DEFAULT '-1',
  `starthour` bigint(20) NOT NULL DEFAULT '-1',
  `startmin` bigint(20) NOT NULL DEFAULT '-1',
  `endday` bigint(20) NOT NULL DEFAULT '-1',
  `endhour` bigint(20) NOT NULL DEFAULT '-1',
  `endmin` bigint(20) NOT NULL DEFAULT '-1',
  KEY `id` (`id`),
  KEY `psid` (`psid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `listing_schedule_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `list_add` int(10) NOT NULL,
  `list_remove` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `psid` (`psid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ebay_audit_sku_compare` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `sku` bigint(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `sku_exist` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ebay_audit_sku_compare_max` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `sku` bigint(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `sku_exist` enum('0','1') NOT NULL,
  `date_added` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `ss_bb_full_amazon` (
  `upc` varchar(24) NOT NULL default '',
  `asin` varchar(50) default NULL,
  `salesrank` varchar(20) default NULL,
  `imageurl` varchar(255) default NULL,
  `brand` varchar(50) default NULL,
  `features` text,
  `lowestprice` varchar(50) default NULL,
  `numsellers` int(11) default NULL,
  `title` varchar(255) default NULL,
  `manpart` varchar(50) NOT NULL default '',
  KEY `upc` (`upc`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ebay_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `ebay_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `weight_min` double(16,2) NOT NULL DEFAULT '0.00',
  `weight_max` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_min` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`ebay_method`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `amazon_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `auto_shipping_ignore` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `auto_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `auto_shipping_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asid` bigint(20) NOT NULL COMMENT 'uniq ID from auto_shipping table',
  `zone` tinyint(4) NOT NULL,
  `carrier` varchar(250) NOT NULL,
  `method` varchar(250) NOT NULL,
  `boxes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `asid` (`asid`,`zone`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

 ALTER TABLE `ebay_audit_sku_compare` ADD INDEX ( `item_id` ) 

 CREATE TABLE `ecom`.`customer_order_label_history` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`orderid` BIGINT( 20 ) NOT NULL ,
`pid` BIGINT( 20 ) NOT NULL ,
`label` VARCHAR( 255 ) NOT NULL ,
`user` VARCHAR( 50 ) NOT NULL ,
`date_created` DATETIME NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `customer_order_label_history` ADD `tracking` VARCHAR( 255 ) NOT NULL AFTER `label` ;

CREATE TABLE IF NOT EXISTS `ebay_audit_reverse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `prod_name` varchar(250) NOT NULL,
  `is_not_active` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

alter table datafeeds add options_dsstock int NOT NULL default 0;

ALTER TABLE cache_channel_sales ADD fba double(16,2) DEFAULT NULL
ALTER TABLE cache_channel_sales ADD amazonca double(16,2) DEFAULT NULL
ALTER TABLE cache_channel_sales ADD ebayace double(16,2) DEFAULT NULL

ALTER TABLE `customer_order_label_history` ADD `user_id` INT( 10 ) NOT NULL ;



CREATE TABLE IF NOT EXISTS `auto_shipping_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bulkship_results`
--

CREATE TABLE IF NOT EXISTS `auto_shipping_items_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bsid` bigint(20) NOT NULL,
  `invoiceid` varchar(255) DEFAULT NULL,
  `resultline` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `bsid` (`bsid`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

ALTER TABLE `bulkship` ADD `type` INT( 5 ) NOT NULL DEFAULT '0';
ALTER TABLE `bulkship_results` ADD `pid` BIGINT( 20 ) NOT NULL ;

ALTER TABLE `ebay_message_center` ADD INDEX ( `emc_question_id` , `emc_site` , `emc_status` ) 
ALTER TABLE `ebay_message_center_question_response` ADD INDEX ( `emcqr_qid` ) 
ALTER TABLE `ebay_message_center_account_orders` ADD INDEX ( `account_id` ) 
ALTER TABLE `ebay_message_center_question` ADD INDEX ( `emcq_sender_id` ) 

CREATE TABLE IF NOT EXISTS `fba_ca_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `updateprice` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fba_ca_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


 CREATE TABLE `auto_datefeed_log` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date` DATETIME NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `auto_datefeed_log` ADD `numitems` INT( 10 ) NOT NULL ;

 CREATE TABLE `auto_datefeed_log_items` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`logid` BIGINT( 20 ) NOT NULL ,
`itemid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM 

 ALTER TABLE `auto_datefeed_log_items` ADD INDEX ( `itemid` ) 
 
ALTER TABLE `ebay_message_center` ADD `keep_alive` TINYINT( 5 ) NOT NULL DEFAULT '0';
ALTER TABLE `ebay_message_center` ADD `no_response` TINYINT( 5 ) NOT NULL DEFAULT '0';


CREATE TABLE IF NOT EXISTS `ebay_message_center_email` (
  `emce_id` int(10) NOT NULL AUTO_INCREMENT,
  `emce_title` varchar(255) NOT NULL,
  `emce_content` text NOT NULL,
  PRIMARY KEY (`emce_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `customer_order_tracking` ADD `shipped_user_id` INT( 10 ) NOT NULL DEFAULT '0';
ALTER TABLE `vendor_pos` ADD `pend_cancel` TINYINT( 1 ) NOT NULL DEFAULT '0';


CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 CREATE TABLE `amazon_order_reports_by_last_update_orders` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT,
`orderid` VARCHAR( 100 ) NOT NULL ,
`purchase_date` VARCHAR( 100 ) NOT NULL ,
`purchase_timestamp` INT( 20 ) NOT NULL ,
`lastupdate_date` VARCHAR( 100 ) NOT NULL ,
`lastupdate_timestamp` INT( 20 ) NOT NULL ,
`status` VARCHAR( 50 ) NOT NULL ,
`sales_channel` VARCHAR( 50 ) NOT NULL ,
`fulfillment_channel` VARCHAR( 50 ) NOT NULL ,
`ship_service_level` VARCHAR( 50 ) NOT NULL ,
`address_city` VARCHAR( 100 ) NOT NULL ,
`address_state` VARCHAR( 100 ) NOT NULL ,
`address_zipcode` VARCHAR( 50 ) NOT NULL ,
`address_country` VARCHAR( 50 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;
 ALTER TABLE `amazon_order_reports_by_last_update_orders` ADD INDEX ( `orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders` ADD `type` INT( 5 ) NOT NULL DEFAULT '0';
ALTER TABLE `amazon_order_reports_by_last_update_orders` ADD `purchase_timestamp_pst` INT( 20 ) NOT NULL AFTER `purchase_timestamp` ;
ALTER TABLE `amazon_order_reports_by_last_update_orders` ADD `lastupdate_timestamp_pst` INT( 20 ) NOT NULL AFTER `lastupdate_timestamp` ;


 CREATE TABLE `amazon_order_reports_by_last_update_orders_items` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT,
`orderid` BIGINT( 20 ) NOT NULL ,
`asin` VARCHAR( 50 ) NOT NULL ,
`sku` VARCHAR( 50 ) NOT NULL ,
`pid` BIGINT( 20 ) NOT NULL ,
`item_status` VARCHAR( 50 ) NOT NULL ,
`product_name` TEXT NOT NULL ,
`qty` INT( 10 ) NOT NULL ,
`price_principal` DOUBLE( 16, 2 ) NOT NULL ,
`price_tax` DOUBLE( 16, 2 ) NOT NULL ,
`price_shipping` DOUBLE( 16, 2 ) NOT NULL ,
`price_giftwrap` DOUBLE( 16, 2 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items` ADD `amazon_orderid` VARCHAR( 100 ) NOT NULL AFTER `orderid` ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items` ADD INDEX ( `orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items` ADD INDEX ( `amazon_orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items` ADD `type` INT( 5 ) NOT NULL DEFAULT '0';

 CREATE TABLE `buy_kit_orders` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`orderid` BIGINT( 20 ) NOT NULL ,
`kitid` BIGINT( 20 ) NOT NULL ,
`qty` INT( 10 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE product_list ADD amazon_ca_realtime INT(10) NOT NULL;


####################### FBA CANADA #######################


CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_network` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_respons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` bigint(20) NOT NULL DEFAULT '0',
  `order_created` bigint(20) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_respons_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sc_id` bigint(20) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 CREATE TABLE `amazon_convert_mfn` (
`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
`date_created` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 
 ALTER TABLE `amazon_convert_mfn` CHANGE `date_created` `date_created` BIGINT( 20 ) NOT NULL 
 
  CREATE TABLE `amazon_convert_log` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` BIGINT( 20 ) NOT NULL ,
`type` VARCHAR( 50 ) NOT NULL ,
`date_created` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM 

 CREATE TABLE `ebay_campaings_surcharge` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`campid` BIGINT( 20 ) NOT NULL ,
`fix_fee` DOUBLE( 16, 2 ) NOT NULL ,
`fix_fee_pound` DOUBLE( 16, 2 ) NOT NULL ,
`perc_fee` DOUBLE( 16, 2 ) NOT NULL
) ENGINE = MYISAM 





CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_canada` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 CREATE TABLE `amazon_order_reports_by_last_update_orders_canada` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT,
`orderid` VARCHAR( 100 ) NOT NULL ,
`purchase_date` VARCHAR( 100 ) NOT NULL ,
`purchase_timestamp` INT( 20 ) NOT NULL ,
`lastupdate_date` VARCHAR( 100 ) NOT NULL ,
`lastupdate_timestamp` INT( 20 ) NOT NULL ,
`status` VARCHAR( 50 ) NOT NULL ,
`sales_channel` VARCHAR( 50 ) NOT NULL ,
`fulfillment_channel` VARCHAR( 50 ) NOT NULL ,
`ship_service_level` VARCHAR( 50 ) NOT NULL ,
`address_city` VARCHAR( 100 ) NOT NULL ,
`address_state` VARCHAR( 100 ) NOT NULL ,
`address_zipcode` VARCHAR( 50 ) NOT NULL ,
`address_country` VARCHAR( 50 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;
 ALTER TABLE `amazon_order_reports_by_last_update_orders_canada` ADD INDEX ( `orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_canada` ADD `type` INT( 5 ) NOT NULL DEFAULT '0';
ALTER TABLE `amazon_order_reports_by_last_update_orders_canada` ADD `purchase_timestamp_pst` INT( 20 ) NOT NULL AFTER `purchase_timestamp` ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_canada` ADD `lastupdate_timestamp_pst` INT( 20 ) NOT NULL AFTER `lastupdate_timestamp` ;


 CREATE TABLE `amazon_order_reports_by_last_update_orders_items_canada` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT,
`orderid` BIGINT( 20 ) NOT NULL ,
`asin` VARCHAR( 50 ) NOT NULL ,
`sku` VARCHAR( 50 ) NOT NULL ,
`pid` BIGINT( 20 ) NOT NULL ,
`item_status` VARCHAR( 50 ) NOT NULL ,
`product_name` TEXT NOT NULL ,
`qty` INT( 10 ) NOT NULL ,
`price_principal` DOUBLE( 16, 2 ) NOT NULL ,
`price_tax` DOUBLE( 16, 2 ) NOT NULL ,
`price_shipping` DOUBLE( 16, 2 ) NOT NULL ,
`price_giftwrap` DOUBLE( 16, 2 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items_canada` ADD `amazon_orderid` VARCHAR( 100 ) NOT NULL AFTER `orderid` ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items_canada` ADD INDEX ( `orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items_canada` ADD INDEX ( `amazon_orderid` ) ;
ALTER TABLE `amazon_order_reports_by_last_update_orders_items_canada` ADD `type` INT( 5 ) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `mc_batch_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bb_import_date` date NOT NULL DEFAULT '0000-00-00',
  `bb_import_count` int(11) NOT NULL COMMENT 'count skus imported',
  `export_file` varchar(50) NOT NULL,
  `export_count` int(11) NOT NULL COMMENT 'count skus exported',
  `export_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `import_datetime` (`bb_import_date`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `mc_bb_mfg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_id` bigint(20) NOT NULL,
  `parts_no` varchar(50) NOT NULL,
  `mfg` varchar(50) NOT NULL,
  `avg_price` double(9,2) NOT NULL,
  `low_price` double(9,2) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `descr` varchar(250) NOT NULL,
  `cond` varchar(10) NOT NULL,
  `freq` int(11) NOT NULL,
  `upc` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parts_no` (`parts_no`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `mc_buy_bad_sku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_id` bigint(20) NOT NULL,
  `sku` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`),
  KEY `batch_id` (`batch_id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `mc_buy_sku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_id` bigint(20) NOT NULL,
  `buy_sku` varchar(20) NOT NULL,
  `manpart_no` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manpart_no` (`manpart_no`),
  KEY `buy_sku` (`buy_sku`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `mc_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_key` varchar(250) NOT NULL,
  `option_value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM
ALTER TABLE `ebay_audit_sku_compare` ADD `date_added` BIGINT( 20 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `list_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `list_groups_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

 CREATE TABLE `ecom`.`price_change_log` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date_created` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM 

 CREATE TABLE `ecom`.`price_change_log_items` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`logid` BIGINT( 20 ) NOT NULL ,
`price_old` DOUBLE( 16, 2 ) NOT NULL ,
`price_new` DOUBLE( 16, 2 ) NOT NULL ,
`pid` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `price_change_log_items` ADD `response` BLOB NOT NULL ;

CREATE TABLE IF NOT EXISTS `fba_fee_calculator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asin` varchar(10) NOT NULL,
  `length` double(9,2) NOT NULL,
  `width` double(9,2) NOT NULL,
  `height` double(9,2) NOT NULL,
  `dim_units` varchar(10) NOT NULL,
  `weight` double(9,2) NOT NULL,
  `weight_units` varchar(10) NOT NULL,
  `category` varchar(30) NOT NULL,
  `is_white_gloves_required` enum('Y','N') NOT NULL,
  `price` double(9,2) NOT NULL,
  `prep_service` double(9,2) NOT NULL,
  `weight_handling` double(9,2) NOT NULL,
  `order_handling` double(9,2) NOT NULL,
  `fba_delivery_services` double(9,2) NOT NULL,
  `commission` double(9,2) NOT NULL,
  `pick_and_pack` double(9,2) NOT NULL,
  `storage` double(9,2) NOT NULL,
  `variable_closing` double(9,2) NOT NULL,
  `new_pick_and_pack` double(9,2) NOT NULL,
  `new_weight_handling` double(9,2) NOT NULL,
  `new_order_hanling` double(9,2) NOT NULL,
  `category_percent` double(9,2) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `dim_finder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upg` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `terminal` varchar(50) NOT NULL,
  `nom` int(11) NOT NULL COMMENT 'Volt',
  `capacity` double(9,2) NOT NULL COMMENT '20 hr. rate',
  `dim_length` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `dim_width` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `dim_height` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `weight` double(9,2) NOT NULL COMMENT 'lbs. +/- 5%',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `mass_price_update` (
  `parent_pid` bigint(20) NOT NULL,
  `price_msrp` double(16,2) DEFAULT NULL,
  `price_map` double(16,2) DEFAULT NULL,
  `price_level1` double(16,2) DEFAULT NULL,
  `price_level2` double(16,2) DEFAULT NULL,
  `price_level3` double(16,2) DEFAULT NULL,
  `price_level4` double(16,2) DEFAULT NULL,
  `price_level5` double(16,2) DEFAULT NULL,
  `price_level6` double(16,2) DEFAULT NULL,
  `price_level7` double(16,2) DEFAULT NULL,
  `price_level8` double(16,2) DEFAULT NULL,
  `price_level9` double(16,2) DEFAULT NULL,
  PRIMARY KEY (`parent_pid`)
) ENGINE=MyISAM

ALTER TABLE `mass_price_update`
  DROP `price_msrp`,
  DROP `price_map`,
  DROP `price_level1`,
  DROP `price_level2`,
  DROP `price_level3`,
  DROP `price_level4`,
  DROP `price_level5`,
  DROP `price_level6`,
  DROP `price_level7`,
  DROP `price_level8`,
  DROP `price_level9`;
  
  alter table ebay_campaigns add std_shipping_cf int NOT NULL default 0;

 CREATE TABLE `hjstores`.`product_description_channels` (
`channel_id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`channel_name` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `product_html` ADD `channel_id` INT( 10 ) NOT NULL ;
ALTER TABLE `product_descr` ADD `channel_id` INT( 10 ) NOT NULL ;
 CREATE TABLE `mc_found_skus` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`sku` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `mc_found_skus` ADD `part_no` VARCHAR( 255 ) NOT NULL ;
 ALTER TABLE `mc_found_skus` ADD INDEX ( `sku` )

ALTER TABLE `mc_batch_history` CHANGE `export_count` `export_match_count` INT( 11 ) NOT NULL COMMENT 'count match skus exported'
alter table `mc_batch_history` add `export_nomatch_count` int(11) not null after `export_match_count`
ALTER TABLE `product_images` ADD `title` VARCHAR(255) NOT NULL
ALTER TABLE `customer_orders` ADD `credit_request` INT(5) NOT NULL default '0'
ALTER TABLE `ebay_campaigns` ADD `dispatch_time` INT(5) NOT NULL DEFAULT  '0'



ALTER TABLE `product_list` ADD `amazonmax_asin` VARCHAR( 50 ) NOT NULL ,
ADD `amazonmax_am_qty` INT( 11 ) NOT NULL ,
ADD `amazonmax_am_std_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazonmax_am_exp_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazonmax_amazon_realtime` INT( 11 ) NOT NULL ,
ADD `amazonmax_am_latency` INT( 11 ) NOT NULL ,
ADD `amazonmax_use_asin` INT( 11 ) NOT NULL ,
ADD `amazonmax_am_update_override` INT( 11 ) NOT NULL ,
ADD `amazonmax_am_condition` ENUM( 'New', 'UsedLikeNew', 'VeryGood', 'Good', 'Acceptable', 'Refurbished' ) NOT NULL ;
ALTER TABLE product_list ADD amazonmax_realtime VARCHAR(10) NOT NULL


CREATE TABLE IF NOT EXISTS `mc_file_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `export_file` varchar(50) NOT NULL,
  `export_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `fba_estimated_fees` (
  `sku` bigint(20) NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_group` varchar(255) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `fulfilled_by` double(9,2) NOT NULL,
  `your_price` double(9,2) NOT NULL,
  `sales_price` double(9,2) NOT NULL,
  `longest_side` double(9,2) NOT NULL,
  `median_side` double(9,2) NOT NULL,
  `shortest_side` double(9,2) NOT NULL,
  `length_and_girth` double(9,2) NOT NULL,
  `unit_of_dimension` varchar(50) NOT NULL,
  `item_package_weight` double(9,2) NOT NULL,
  `unit_of_weight` varchar(50) NOT NULL,
  `product_size_tier` varchar(100) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `estimated_fee` double(9,2) NOT NULL,
  `estimated_referral_fee_per_unit` double(9,2) NOT NULL,
  `estimated_variable_closing_fee` double(9,2) NOT NULL,
  `estimated_order_handling_fee_per_order` double(9,2) NOT NULL,
  `estimated_pick_pack_fee_per_unit` double(9,2) NOT NULL,
  `estimated_weight_handling_fee_per_unit` double(9,2) NOT NULL,
  PRIMARY KEY (`sku`)
) ENGINE=MyISAM;

ALTER TABLE fba_estimated_fees ADD last_update DATETIME default '0000-00-00 00:00:00' NOT NULL;

CREATE TABLE IF NOT EXISTS `fba_estimated_fees_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM


 CREATE TABLE `product_dimensions` (
`pid` BIGINT( 20 ) NOT NULL ,
`width` DOUBLE( 16, 2 ) NOT NULL ,
`height` DOUBLE( 16, 2 ) NOT NULL ,
`length` DOUBLE( 16, 2 ) NOT NULL ,
UNIQUE (
`pid`
)
) ENGINE = MYISAM 

ALTER TABLE `mc_batch_history` ADD `export_match_count_new` INT( 10 ) NOT NULL ,
ADD `export_match_count_new_no_qty` INT( 10 ) NOT NULL ,
ADD `export_match_count_ref` INT( 10 ) NOT NULL ,
ADD `export_match_count_ref_no_qty` INT( 10 ) NOT NULL ;

CREATE TABLE  database_maintenance_log (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`table_name` VARCHAR( 100 ) NOT NULL ,
`tstamp` INT(11) NOT NULL
) ENGINE = MYISAM;

ALTER TABLE database_maintenance_log ADD UNIQUE (
`table_name`
);

CREATE TABLE IF NOT EXISTS `coupon_criteria_lists` (
  `ccl` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `lid` bigint(20) NOT NULL,
  PRIMARY KEY (`ccl`),
  UNIQUE KEY `cid` (`cid`,`lid`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `sellery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid` (`pid`)
) ENGINE=MyISAM

ALTER TABLE `ebay_campaigns` ADD `zip_code` varchar(10) NOT NULL
ALTER TABLE `amazon_feedback` ADD `follow_up` tinyint(3) NOT NULL

CREATE TABLE IF NOT EXISTS `amazon_feedback_report_canada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `amazon_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;


CREATE TABLE IF NOT EXISTS `amazon_feedback_canada` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email`  varchar(100) NOT NULL,
  `rater_role`  varchar(100) NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;
ALTER TABLE `amazon_feedback_canada` ADD `notes` TEXT NOT NULL AFTER `rater_role` ;
ALTER TABLE `amazon_feedback_canada` ADD `follow_up` TINYINT( 3 ) NOT NULL ;
ALTER TABLE `amazon_feedback` ADD `rater_email` varchar( 100 ) NOT NULL ;
ALTER TABLE `amazon_feedback` ADD `rater_role` varchar( 100 ) NOT NULL ;

ALTER TABLE `amazon_feedback_report` DROP `rater_email`, DROP `rater_role`;

ALTER TABLE `amazon_feedback` ADD `follow_up` TINYINT( 3 ) NOT NULL ;
ALTER TABLE `amazon_duplicates_extended` ADD `large_imageurl` varchar( 255 )  DEFAULT NULL AFTER `imageurl`;
ALTER TABLE `customer_order_tracking` ADD `pickerid` INT( 10 ) NOT NULL ;
ALTER TABLE `customer_order_tracking` ADD `shipperid` INT( 10 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `labels_return_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` int(10) NOT NULL,
  `method` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `trackno` varchar(128) NOT NULL,
  `istracked` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `labels_return_log` ADD `isrefund` tinyint( 3 ) NOT NULL ;
ALTER TABLE `order_shipped_globegistic` ADD `reportid` int( 10 ) NOT NULL ;


CREATE TABLE IF NOT EXISTS `fba_removal_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

ALTER TABLE `fba_removal_report` ADD `tstamp_server` int( 10 ) NOT NULL ;
ALTER TABLE `fba_removal_report` ADD `failed` int( 10 ) NOT NULL ;

ALTER TABLE `fba_removal_shipping_report` ADD `tstamp_server` int( 10 ) NOT NULL ;
ALTER TABLE `fba_removal_shipping_report` ADD `failed` int( 10 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `fba_removal_shipping_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `snd_fba_inventory_matcher` (
  `pid` bigint(20) NOT NULL,
  UNIQUE KEY `pid` (`pid`)
) ENGINE=MyISAM


CREATE TABLE IF NOT EXISTS `fba_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `fba_max_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `fba_ca_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;


 CREATE TABLE `amazon_removal_order_log` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`orderid` VARCHAR( 255 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`report_sku` VARCHAR( 255 ) NOT NULL ,
`userid` INT( 10 ) NOT NULL ,
`msg` TEXT NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `amazon_removal_order_log` ADD `datecreated` INT( 10 ) NOT NULL ;
ALTER TABLE `amazon_removal_order_log` ADD `removalid` INT(10) NOT NULL AFTER `report_sku` ;
 ALTER TABLE `amazon_removal_order_log` ADD INDEX ( `orderid` );
 ALTER TABLE `amazon_removal_order_log` ADD INDEX ( `pid` );
 ALTER TABLE `amazon_removal_order_log` ADD INDEX ( `removalid` );
 ALTER TABLE `amazon_removal_order_log` ADD INDEX ( `userid` );
 ALTER TABLE `amazon_removal_order_log` ADD `type` INT( 10 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `fba_reimburse_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

 CREATE TABLE `amazon_removal_order_complete` (
`orderid` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `orderid` )
) ENGINE = MYISAM ;

 CREATE TABLE `ebay_categories_automotive` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`best_offer_enable` TINYINT( 3 ) NOT NULL ,
`auto_pay_enable` TINYINT( 3 ) NOT NULL ,
`category_id` INT( 10 ) NOT NULL ,
`category_level` TINYINT( 3 ) NOT NULL ,
`category_name` VARCHAR( 255 ) NOT NULL ,
`category_parent_id` INT( 10 ) NOT NULL 
) ENGINE = MYISAM ;
 ALTER TABLE `ebay_categories_automotive` ADD INDEX ( `category_id` ) ;
 ALTER TABLE `ebay_categories_automotive` ADD INDEX ( `category_parent_id` ) ;
 ALTER TABLE `ebay_categories_automotive` ADD `category_leaf` TINYINT( 3 ) NOT NULL ,
ADD `category_lsd` TINYINT( 3 ) NOT NULL ;
 
CREATE TABLE `ebay_categories_automotive_map` (
`ebaycatid` INT( 10 ) NOT NULL,
`catid` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `ebay_categories_automotive_map` ADD INDEX ( `ebaycatid` ) ;
ALTER TABLE `ebay_categories_automotive_map` ADD INDEX ( `catid` ) ;
ALTER TABLE `ebay_categories_automotive_map` ADD INDEX ( `pid` ) ;

ALTER TABLE email_campaigns ADD shipped_2days int(10) not null;

ALTER TABLE customer_accounts ADD levelid tinyint(4) not null;
ALTER TABLE user_ip_restriction ADD levelid tinyint(4) not null;

CREATE TABLE  `customer_levels` (
`id` TINYINT( 4 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`level_name` VARCHAR( 250 ) NOT NULL
) ENGINE = MYISAM

INSERT INTO `customer_levels` (`id`,`level_name`) VALUES (NULL, 'Monthly Member'), (NULL, 'Annual Member');
INSERT INTO `customer_levels` (`id`,`level_name`) VALUES (NULL, 'Easy green Reseller'), (NULL, 'Easy green distributors');



 CREATE TABLE `ecom`.`google_feed` (
`orderid` INT( 10 ) NOT NULL ,
INDEX ( `orderid` )
) ENGINE = MYISAM ;

ALTER TABLE customer_orders ADD cancelled_reason varchar(255) not null;


 CREATE TABLE `ebay_categories_canada` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`best_offer_enable` TINYINT( 3 ) NOT NULL ,
`auto_pay_enable` TINYINT( 3 ) NOT NULL ,
`category_id` INT( 10 ) NOT NULL ,
`category_level` TINYINT( 3 ) NOT NULL ,
`category_name` VARCHAR( 255 ) NOT NULL ,
`category_parent_id` INT( 10 ) NOT NULL 
) ENGINE = MYISAM ;
 ALTER TABLE `ebay_categories_canada` ADD INDEX ( `category_id` ) ;
 ALTER TABLE `ebay_categories_canada` ADD INDEX ( `category_parent_id` ) ;
 ALTER TABLE `ebay_categories_canada` ADD `category_leaf` TINYINT( 3 ) NOT NULL ,
ADD `category_lsd` TINYINT( 3 ) NOT NULL ;
 
CREATE TABLE `ebay_categories_canada_map` (
`ebaycatid` INT( 10 ) NOT NULL,
`catid` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `ebay_categories_canada_map` ADD INDEX ( `ebaycatid` ) ;
ALTER TABLE `ebay_categories_canada_map` ADD INDEX ( `catid` ) ;
ALTER TABLE `ebay_categories_canada_map` ADD INDEX ( `pid` ) ;


CREATE TABLE IF NOT EXISTS `fba_blue_estimated_fees` (
  `sku` bigint(20) NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_group` varchar(255) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `fulfilled_by` double(9,2) NOT NULL,
  `your_price` double(9,2) NOT NULL,
  `sales_price` double(9,2) NOT NULL,
  `longest_side` double(9,2) NOT NULL,
  `median_side` double(9,2) NOT NULL,
  `shortest_side` double(9,2) NOT NULL,
  `length_and_girth` double(9,2) NOT NULL,
  `unit_of_dimension` varchar(50) NOT NULL,
  `item_package_weight` double(9,2) NOT NULL,
  `unit_of_weight` varchar(50) NOT NULL,
  `product_size_tier` varchar(100) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `estimated_fee` double(9,2) NOT NULL,
  `estimated_referral_fee_per_unit` double(9,2) NOT NULL,
  `estimated_variable_closing_fee` double(9,2) NOT NULL,
  `estimated_order_handling_fee_per_order` double(9,2) NOT NULL,
  `estimated_pick_pack_fee_per_unit` double(9,2) NOT NULL,
  `estimated_weight_handling_fee_per_unit` double(9,2) NOT NULL,
  PRIMARY KEY (`sku`)
) ENGINE=MyISAM;

ALTER TABLE fba_blue_estimated_fees ADD last_update DATETIME default '0000-00-00 00:00:00' NOT NULL;

CREATE TABLE IF NOT EXISTS `fba_blue_estimated_fees_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;


CREATE TABLE IF NOT EXISTS `fba_blue_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;

alter table coupon_codes add singleuse int NOT NULL default 0;

create table coupon_codes_singleuse
(
couponcode varchar(50) NOT NULL default '',
cid bigint NOT NULL default 0,
index(couponcode),
index(cid)
);

CREATE TABLE `product_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `body` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

create table product_mapping (
id bigint(20) NOT NULL AUTO_INCREMENT,
url varchar(255),
pid bigint,
tid int,
filename varchar(255),
index(id),
index(url),
index(pid)
);

 CREATE TABLE `amazon_item_type` (
`category_id` INT( 10 ) NOT NULL ,
`item_type` VARCHAR( 1000 ) NOT NULL ,
INDEX ( `category_id` )
) ENGINE = MYISAM ;

 CREATE TABLE `amazon_listings` (
`pid` VARCHAR( 255 ) NOT NULL ,
`account` VARCHAR( 255 ) NOT NULL ,
INDEX ( `pid` , `account` )
) ENGINE = MYISAM ;

CREATE TABLE IF NOT EXISTS `fba_ace_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM;


 CREATE TABLE `ebay_variations` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`main_pid` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`sku` VARCHAR( 255 ) NOT NULL ,
`var_color` VARCHAR( 255 ) NOT NULL ,
`var_size` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM;
ALTER TABLE `ebay_variations` ADD INDEX ( `main_pid` );
ALTER TABLE `ebay_variations` ADD INDEX ( `pid` ); 


ALTER TABLE `ebay_campaigns` ADD `variations` TINYINT( 1 ) NOT NULL ;

CREATE TABLE `customer_tmp_cart` (
`acctid` bigint(20) NOT NULL,
`pid` bigint(20) NOT NULL,
`qty` bigint(20) NOT NULL,
`date_added` date NOT NULL default '0000-00-00',
PRIMARY KEY (  `acctid` )
) ENGINE = MYISAM


 CREATE TABLE `amazon_crawl` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` INT( 10 ) NOT NULL ,
`last_update` INT( 10 ) NOT NULL ,
INDEX ( `pid` )
) ENGINE = MYISAM ;

 CREATE TABLE `amazon_crawl_values` (
`id` INT( 10 ) NOT NULL ,
`crawl_id` INT( 10 ) NOT NULL ,
`merchant` VARCHAR( 255 ) NOT NULL ,
`ratings` VARCHAR( 255 ) NOT NULL ,
`numratings` VARCHAR( 255 ) NOT NULL ,
`supersaver` INT( 10 ) NOT NULL ,
`qty` INT( 10 ) NOT NULL ,
`price` DOUBLE( 16, 2 ) NOT NULL ,
`shipping` DOUBLE( 16, 2 ) NOT NULL ,
`shipping_price` DOUBLE( 16, 2 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `amazon_crawl_values` ADD PRIMARY KEY(`id`);
ALTER TABLE `amazon_crawl_values` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `amazon_crawl_values` ADD INDEX ( `crawl_id` ) ;
ALTER TABLE `amazon_crawl` ADD `asin` VARCHAR( 50 ) NOT NULL AFTER `pid` ;

 CREATE TABLE `shop_orders` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`orderid` INT( 10 ) NOT NULL ,
`status` TINYINT( 2 ) NOT NULL ,
INDEX ( `orderid` )
) ENGINE = MYISAM ;
ALTER TABLE `shop_orders` ADD `shop_orderid` INT( 10 ) NOT NULL AFTER `orderid` ,
ADD `shop_invoiceid` INT( 10 ) NOT NULL AFTER `shop_orderid` ;

ALTER TABLE `product_category_map` ADD `sort_order` INT( 10 ) NOT NULL ;

CREATE TABLE  `product_static_order` (
`id` BIGINT NOT NULL ,
`pid` BIGINT NOT NULL ,
`cid` BIGINT NOT NULL ,
`lid` BIGINT NOT NULL ,
`order_val` BIGINT NOT NULL
) ENGINE = MYISAM

CREATE TABLE IF NOT EXISTS `whs_loc_mngt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `whs_isle` varchar(1) NOT NULL,
  `whs_row` varchar(1) NOT NULL,
  `whs_bin` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE `customer_order_claim` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`invoiceid` VARCHAR( 255 ) NOT NULL ,
`trackno` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;
 ALTER TABLE `customer_order_claim` ADD INDEX ( `invoiceid` ) ;
 ALTER TABLE `customer_order_claim` ADD `claim_type` VARCHAR( 255 ) NOT NULL ;
 ALTER TABLE `customer_order_claim` ADD `notes` TEXT NOT NULL ;

alter table amazon_feed_report change type type enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','CBA','ORDERADJUSTMENT','CONVERTSKUSAFN','CONVERTSKUSMFN');

CREATE TABLE IF NOT EXISTS `customer_credits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `credit_amount` double(9,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `customer_credits_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `operation_description` varchar(250) NOT NULL,
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

ALTER TABLE `product_list` ADD `max_pid` VARCHAR( 255 ) NOT NULL AFTER `id` ;

ALTER TABLE `product_list` ADD INDEX ( `max_pid` ) ;

 CREATE TABLE `rawfoodworld`.`customer_orders_raw_emails` (
`orderid` INT( 10 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
`email_sent` TINYINT( 1 ) NOT NULL ,
INDEX ( `orderid` )
) ENGINE = MYISAM 

 CREATE TABLE `ecom`.`fba_estimated_shipping` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`partner` TINYINT( 1 ) NOT NULL ,
`ship_type` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `fba_estimated_shipping` ADD `plan_item_id` INT( 10 ) NOT NULL AFTER `id` ;

 CREATE TABLE `ecom`.`fba_estimated_shipping_boxes` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`shipping_id` INT( 10 ) NOT NULL ,
`box_width` DOUBLE( 16, 2 ) NOT NULL ,
`box_height` DOUBLE( 16, 2 ) NOT NULL ,
`box_length` DOUBLE( 16, 2 ) NOT NULL ,
`box_weight` DOUBLE( 16, 2 ) NOT NULL ,
INDEX ( `shipping_id` )
) ENGINE = MYISAM 

 ALTER TABLE `fba_estimated_shipping` CHANGE `plan_item_id` `shipment_id` VARCHAR( 255 ) NOT NULL 
 ALTER TABLE `fba_estimated_shipping` ADD INDEX ( `shipment_id` ) ;

 CREATE TABLE `ecom`.`fba_estimated_shipping_result` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`shipping_id` INT( 10 ) NOT NULL ,
`total_price` DOUBLE( 16, 2 ) NOT NULL ,
`estimate_status` VARCHAR( 50 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL
) ENGINE = MYISAM ;
 ALTER TABLE `fba_estimated_shipping_result` ADD INDEX ( `shipping_id` ) ;

ALTER TABLE `fba_estimated_shipping_result` ADD `void_estimate` TINYINT( 1 ) NOT NULL AFTER `estimate_status` ,
ADD `confirm_estimate` TINYINT( 1 ) NOT NULL AFTER `void` ;

CREATE TABLE IF NOT EXISTS `customer_eorders_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `is_checked` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `customer_eorders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `kit_pid` bigint(20) NOT NULL DEFAULT '0',
  `count_downloads` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM


CREATE TABLE IF NOT EXISTS `product_html` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` text,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `product_shipping_limit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `countries_list` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

 CREATE TABLE `fba_estimated_address` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`add_name` VARCHAR( 255 ) NOT NULL ,
`add_address1` VARCHAR( 255 ) NOT NULL ,
`add_address2` VARCHAR( 255 ) NOT NULL ,
`add_city` VARCHAR( 255 ) NOT NULL ,
`add_state` VARCHAR( 255 ) NOT NULL ,
`add_zip` VARCHAR( 255 ) NOT NULL ,
`add_country` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `fba_estimated_shipping` ADD `carrier` VARCHAR( 255 ) NOT NULL ;

 CREATE TABLE `ecom`.`fba_estimated_shipping_track` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`shipping_id` INT( 10 ) NOT NULL ,
`trackno` VARCHAR( 255 ) NOT NULL ,
INDEX ( `shipping_id` )
) ENGINE = MYISAM

ALTER TABLE `fba_estimated_shipping` ADD `account` VARCHAR( 50 ) NOT NULL ,
ADD `tstamp` INT( 10 ) NOT NULL ;

ALTER TABLE `fba_blue_shipment_creator` ADD `addr_id` INT( 10 ) NOT NULL;
ALTER TABLE `fba_max_shipment_creator` ADD `addr_id` INT( 10 ) NOT NULL;


 CREATE TABLE `butterfly_reviews`.`products_reviews` (
`id` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`review_id` VARCHAR( 255 ) NOT NULL ,
`review_stars` INT( 10 ) NOT NULL ,
`review_title` VARCHAR( 255 ) NOT NULL ,
`review_descr` TEXT NOT NULL ,
`review_date` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) ,
INDEX ( `pid` )
) ENGINE = MYISAM 

  CREATE TABLE `butterfly_reviews`.`products` (
    `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `asin` VARCHAR( 255 ) NOT NULL ,
    `total_reviews` INT( 10 ) NOT NULL ,
    `last_tstamp` INT( 10 ) NOT NULL
    ) ENGINE = MYISAM ;
 ALTER TABLE `products_reviews` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT 

CREATE TABLE `am_product_reviews_info` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`pid` INT( 10 ) NOT NULL ,
`review_id` VARCHAR( 255 ) NOT NULL ,
`review_stars` INT( 10 ) NOT NULL ,
`review_title` VARCHAR( 255 ) NOT NULL ,
`review_descr` TEXT NOT NULL ,
`review_date` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY ( `id` ) ,
INDEX ( `pid` )
) ENGINE = MYISAM; 

  CREATE TABLE `am_product_reviews` (
    `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `asin` VARCHAR( 255 ) NOT NULL ,
    `total_reviews` INT( 10 ) NOT NULL ,
    `last_tstamp` INT( 10 ) NOT NULL
    ) ENGINE = MYISAM ;
    
ALTER TABLE coupon_codes ADD couponlimit INT(10) NOT NULL;
CREATE TABLE `customer_coupons_used` (
`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`acctid` BIGINT (20) NOT NULL ,
`cid` INT (10) NOT NULL ,
`count` INT (10) NOT NULL default '0'
) ENGINE = MYISAM;

ALTER TABLE customer_credits_log ADD operation_reason VARCHAR (250) NOT NULL AFTER operation_description;

 CREATE TABLE `ecom`.`ebay_price_change_log` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`listingid` INT( 10 ) NOT NULL ,
`price` DOUBLE( 16, 2 ) NOT NULL ,
`qty` INT( 10 ) NOT NULL
) ENGINE = MYISAM 

 ALTER TABLE `ebay_price_change_log` CHANGE `listingid` `listingid` VARCHAR( 255 ) NOT NULL 

ALTER TABLE `fba_estimated_shipping` ADD `pronr` VARCHAR( 255 ) NOT NULL AFTER `carrier` ,
ADD `nr_pallets` INT( 10 ) NOT NULL AFTER `pronr` ;

 CREATE TABLE `ecom`.`fba_estimated_shipping_pallets` (
`id` INT( 10 ) NOT NULL ,
`shipment_id` VARCHAR( 255 ) NOT NULL ,
`pallet_weight` INT( 10 ) NOT NULL ,
`pallet_width` INT( 10 ) NOT NULL ,
`pallet_height` INT( 10 ) NOT NULL ,
`pallet_length` INT( 10 ) NOT NULL ,
`is_stacked` VARCHAR( 10 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM 

 ALTER TABLE `fba_estimated_shipping_pallets` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;
 ALTER TABLE `fba_estimated_shipping_pallets` ADD INDEX ( `shipment_id` ) ;

 CREATE TABLE `ecom`.`fba_estimated_shipping_ltl_info` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`shipment_id` VARCHAR( 255 ) NOT NULL ,
`contact_name` VARCHAR( 255 ) NOT NULL ,
`contact_phone` VARCHAR( 255 ) NOT NULL ,
`contact_email` VARCHAR( 255 ) NOT NULL ,
`contact_fax` VARCHAR( 255 ) NOT NULL ,
`boxcount` INT( 10 ) NOT NULL ,
`freight` DOUBLE( 16, 2 ) NOT NULL ,
`readydate` VARCHAR( 255 ) NOT NULL ,
`totalweight` DOUBLE( 16, 2 ) NOT NULL ,
`declared_value` DOUBLE( 16, 2 ) NOT NULL ,
INDEX ( `shipment_id` )
) ENGINE = MYISAM 

 CREATE TABLE `amazon_inventory_recommendations` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`efluent_pid` INT( 10 ) NOT NULL ,
`sku` VARCHAR( 255 ) NOT NULL ,
`asin` VARCHAR( 255 ) NOT NULL ,
`upc` VARCHAR( 255 ) NOT NULL ,
`last_update` INT( 10 ) NOT NULL ,
`item_name` VARCHAR( 255 ) NOT NULL ,
`fulfillment_channel` VARCHAR( 255 ) NOT NULL ,
`sales_14_days` INT( 10 ) NOT NULL ,
`sales_30_days` INT( 10 ) NOT NULL ,
`available_qty` INT( 10 ) NOT NULL ,
`days_stock_out` INT( 10 ) NOT NULL ,
`inbound_qty` INT( 10 ) NOT NULL ,
`recommended_inbound_qty` INT( 10 ) NOT NULL ,
`days_out_of_stock_30` INT( 10 ) NOT NULL ,
`lost_sale_30` INT( 10 ) NOT NULL ,
`recommendation_id` TEXT NOT NULL ,
`recommendation_reason` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 
 ALTER TABLE `amazon_inventory_recommendations` ADD INDEX ( `efluent_pid` ) ;
 ALTER TABLE `amazon_inventory_recommendations` ADD INDEX ( `sku` ) ;

 CREATE TABLE `ecom`.`ebay_discounts_campaigns` (
`id` INT( 10 ) NOT NULL ,
`campaign_title` VARCHAR( 255 ) NOT NULL ,
`campaign_description` TEXT NOT NULL ,
`campaign_status` TINYINT( 1 ) NOT NULL ,
`campaign_type` VARCHAR( 255 ) NOT NULL ,
`campaign_budget` DOUBLE( 16, 2 ) NOT NULL ,
`campaign_priority` INT( 10 ) NOT NULL ,
`campaign_begin_date` INT( 10 ) NOT NULL ,
`campaign_end_date` INT( 10 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM 
 ALTER TABLE `ebay_discounts_campaigns` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT 
ALTER TABLE `ebay_discounts_campaigns` ADD `campaign_category` INT( 10 ) NOT NULL ;

CREATE TABLE `ebay_discounts_addskus` (
`apsid` bigint(20) NOT NULL AUTO_INCREMENT,
`apid` bigint(20) NOT NULL,
`pid` bigint(20) NOT NULL DEFAULT '0',
`dollar` double(16,2) NOT NULL default 0.00,
`percent` double(16,2) NOT NULL default 0.00,
KEY `apsid` (`apsid`),
KEY `apid` (`apid`)
) ENGINE=MyISAM;

CREATE TABLE `ebay_discounts_criteria` (
  `apcid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `qty_from` int(11) NOT NULL DEFAULT '0.00',
  `qty_to` int(11) NOT NULL DEFAULT '0.00',
  `total_from` double(16,2) NOT NULL DEFAULT '0.00',
  `total_to` double(16,2) NOT NULL DEFAULT '0.00',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  KEY `apcid` (`apcid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM;

 CREATE TABLE `ecom`.`ebay_discounts_offers` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`campaign_id` INT( 10 ) NOT NULL ,
`order_qty_from` INT( 10 ) NOT NULL ,
`order_qty_to` INT( 10 ) NOT NULL ,
`order_total_from` DOUBLE( 16, 2 ) NOT NULL ,
`order_total_to` DOUBLE( 16, 2 ) NOT NULL ,
`discount_fix` DOUBLE( 16, 2 ) NOT NULL ,
`discount_perc` DOUBLE( 16, 2 ) NOT NULL ,
`discount_qty_fix` DOUBLE( 16, 2 ) NOT NULL ,
`discount_qty_perc` DOUBLE( 16, 2 ) NOT NULL ,
`discount_max_price` TINYINT( 1 ) NOT NULL ,
`discount_shipping` DOUBLE( 16, 2 ) NOT NULL ,
`campaign_status` INT( 10 ) NOT NULL ,
INDEX ( `campaign_id` )
) ENGINE = MYISAM 

 CREATE TABLE `ecom`.`amazonblue_recommended_removal` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`efluent_pid` VARCHAR( 255 ) NOT NULL ,
`sku` VARCHAR( 255 ) NOT NULL ,
`fnsku` VARCHAR( 255 ) NOT NULL ,
`asin` VARCHAR( 255 ) NOT NULL ,
`product_name` VARCHAR( 255 ) NOT NULL ,
`condition` VARCHAR( 255 ) NOT NULL ,
`sellable_quantity` INT( 10 ) NOT NULL ,
`sellable_271_365` INT( 10 ) NOT NULL ,
`sellable_365` INT( 10 ) NOT NULL ,
`sellable_removal_quantity` INT( 10 ) NOT NULL ,
`unsellable_quantity` INT( 10 ) NOT NULL ,
`unsellable_0_7` INT( 10 ) NOT NULL ,
`unsellable_8_60` INT( 10 ) NOT NULL ,
`unsellable_61_90` INT( 10 ) NOT NULL ,
INDEX ( `efluent_pid` )
) ENGINE = MYISAM 


 CREATE TABLE `amazon_order_reports_by_last_update_orders_items_avg` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` INT( 10 ) NOT NULL ,
`avg_val` DOUBLE( 16, 2 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
`nr_days` INT( 10 ) NOT NULL ,
`account` VARCHAR( 50 ) NOT NULL ,
INDEX ( `pid` )
) ENGINE = MYISAM 

ALTER TABLE `email_templates_addon` ADD `sendcopy` TINYINT( 1 ) NOT NULL ;
ALTER TABLE `email_templates` ADD `sendcopy` TINYINT( 1 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `product_bulk_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `bulk_qty_min` int(11) NOT NULL,
  `bulk_qty_max` int(11) NOT NULL,
  `bulk_price` double(9,2) NOT NULL DEFAULT '0.00',
  `bulk_unit` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

CREATE TABLE IF NOT EXISTS `product_bulk_options_promo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `bulk_qty_min` int(11) NOT NULL,
  `bulk_qty_max` int(11) NOT NULL,
  `bulk_price` double(9,2) NOT NULL DEFAULT '0.00',
  `bulk_unit` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM

 CREATE TABLE `ecom`.`endicia_scan_form` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`trackno` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 

CREATE TABLE IF NOT EXISTS `amazon_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `amazon_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `amazon_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 CREATE TABLE `ebay_images` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` INT( 10 ) NOT NULL ,
`itype` INT( 10 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
`full_url` VARCHAR( 255 ) NOT NULL ,
`base_url` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM 
 ALTER TABLE `ebay_images` ADD INDEX ( `pid` ) 


CREATE TABLE IF NOT EXISTS `product_categories_title` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `catid` int(20) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `divid` int(20) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `product_categories_description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL,
  `description` blob NOT NULL,
  `divid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

ALTER TABLE `ebay_campaigns` ADD `is_automotive` INT( 10 ) NOT NULL ;
ALTER TABLE `autoprice` ADD `lastupdate` INT( 10 ) NOT NULL ;


 CREATE TABLE `ecom`.`auto_shipping_multiple` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pids` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `auto_shipping_multiple` ADD `qtys` VARCHAR( 255 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `auto_shipping_multiple_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `asid` int(10) NOT NULL,
  `zone_nr` int(10) NOT NULL,
  `shipmethod` varchar(255) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `boxnr` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 CREATE TABLE `ecom`.`auto_shipping_multiple_boxes` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`asid` INT( 10 ) NOT NULL ,
`boxnr` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`qty` INT( 10 ) NOT NULL
) ENGINE = MYISAM 

ALTER TABLE `auto_shipping_multiple_boxes` ADD `zone` INT( 10 ) NOT NULL ;

 CREATE TABLE `ecom`.`endicia_scan_form_groups` (
`id` INT( 10 ) NOT NULL ,
`group1` VARCHAR( 255 ) NOT NULL ,
`group2` VARCHAR( 255 ) NOT NULL ,
`group3` VARCHAR( 255 ) NOT NULL ,
`group4` VARCHAR( 255 ) NOT NULL ,
`tstamp` INT( 10 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM 
 ALTER TABLE `endicia_scan_form_groups` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;

 CREATE TABLE `supplysale`.`brokerbin_title_update` (
`pid` INT( 10 ) NOT NULL ,
PRIMARY KEY ( `pid` )
) ENGINE = MYISAM 


CREATE TABLE IF NOT EXISTS `rma_lines` (
  `lineid` int(10) NOT NULL AUTO_INCREMENT,
  `rmaid` int(10) DEFAULT NULL,
  `pid` int(10) NOT NULL DEFAULT '0',
  `sku` varchar(25) DEFAULT NULL,
  `manid` int(10) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `price` double(16,2) NOT NULL DEFAULT '0.00',
  `qty_ordered` int(11) NOT NULL DEFAULT '0',
  `qty_shipped` int(11) NOT NULL DEFAULT '0',
  `weight` double(16,2) NOT NULL DEFAULT '0.00',
  `baseprice` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `lineid` (`lineid`),
  KEY `rmaid` (`rmaid`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `rma` (
  `rmaid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendorid` smallint(5) NOT NULL DEFAULT '0',
  `invoiceid` varchar(26) DEFAULT NULL,
  `poref` varchar(128) DEFAULT NULL,
  `vendorref` varchar(128) DEFAULT NULL,
  `accountname` varchar(50) DEFAULT NULL,
  `addrline1` varchar(50) DEFAULT NULL,
  `addrline2` varchar(50) DEFAULT NULL,
  `addrcity` varchar(50) DEFAULT NULL,
  `addrstate` char(2) DEFAULT NULL,
  `addrzip` varchar(10) DEFAULT NULL,
  `addrcountry` varchar(50) DEFAULT NULL,
  `saddrname` varchar(50) DEFAULT NULL,
  `saddrline1` varchar(50) DEFAULT NULL,
  `saddrline2` varchar(50) DEFAULT NULL,
  `saddrcity` varchar(50) DEFAULT NULL,
  `saddrstate` char(2) DEFAULT NULL,
  `saddrzip` varchar(10) DEFAULT NULL,
  `saddrcountry` varchar(50) DEFAULT NULL,
  `status` smallint(5) NOT NULL DEFAULT '0',
  KEY `rmaid` (`rmaid`),
  KEY `vendorid` (`vendorid`),
  KEY `poref` (`poref`),
  KEY `vendorref` (`vendorref`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `rma_notes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rmaid` int(10),
  `notes` text NOT NULL,
  KEY `id` (`id`),
  KEY `rmaid` (`rmaid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `amazon_reimbursements` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reimbursement_id` int(10) NOT NULL,
  `tstamp` varchar(255) NOT NULL,
  `tstamp_est` int(10) NOT NULL,
  `amazon_orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `amount` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table customer_orders_inf add cs_interaction tinyint(3) NOT NULL default 0;


 CREATE TABLE `mc_price_markup` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pid` INT( 10 ) NOT NULL ,
`price1` DOUBLE( 16, 2 ) NOT NULL ,
`price2` DOUBLE( 16, 2 ) NOT NULL ,
`price_markup` DOUBLE( 16, 2 ) NOT NULL
) ENGINE = MYISAM ;
 ALTER TABLE `mc_price_markup` ADD INDEX ( `pid` ) ;

 CREATE TABLE `product_reviews_info` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`rev_id` VARCHAR( 255 ) NOT NULL ,
`rev_assigned` INT( 10 ) NOT NULL ,
`rev_ordernr` VARCHAR( 255 ) NOT NULL ,
`rev_phone` VARCHAR( 255 ) NOT NULL ,
`rev_email` VARCHAR( 255 ) NOT NULL ,
`rev_notes` TEXT NOT NULL ,
`rev_status` INT( 10 ) NOT NULL ,
INDEX ( `rev_id` )
) ENGINE = MYISAM ;

 CREATE TABLE `ecom`.`product_reviews_mappings` (
`id` INT( 10 ) NOT NULL ,
`pid` INT( 10 ) NOT NULL ,
`item_color` VARCHAR( 255 ) NOT NULL ,
`item_size` VARCHAR( 255 ) NOT NULL ,
`new_pid` INT( 10 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;
 ALTER TABLE `product_reviews_mappings` CHANGE `id` `id` INT( 10 ) NOT NULL AUTO_INCREMENT ;