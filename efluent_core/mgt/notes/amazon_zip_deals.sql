
DROP TABLE IF EXISTS `amazonzd_base_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM AUTO_INCREMENT=5330 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_base_requests`
--

DROP TABLE IF EXISTS `amazonzd_base_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=1091 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_campaign_lists`
--

DROP TABLE IF EXISTS `amazonzd_campaign_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_campaigns`
--

DROP TABLE IF EXISTS `amazonzd_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_campaigns_categories`
--

DROP TABLE IF EXISTS `amazonzd_campaigns_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_campaigns_categories` (
  `campid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_categories`
--

DROP TABLE IF EXISTS `amazonzd_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_categories` (
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `amazoncat` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_convert_log`
--

DROP TABLE IF EXISTS `amazonzd_convert_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_convert_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_convert_mfn`
--

DROP TABLE IF EXISTS `amazonzd_convert_mfn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_convert_mfn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_duplicates`
--

DROP TABLE IF EXISTS `amazonzd_duplicates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_duplicates_extended`
--

DROP TABLE IF EXISTS `amazonzd_duplicates_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_errors`
--

DROP TABLE IF EXISTS `amazonzd_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_errors_other`
--

DROP TABLE IF EXISTS `amazonzd_errors_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=18300 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_feed_report`
--

DROP TABLE IF EXISTS `amazonzd_feed_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','ORDERADJUSTMENT') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=12693 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_feedback`
--

DROP TABLE IF EXISTS `amazonzd_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_feedback_report`
--

DROP TABLE IF EXISTS `amazonzd_feedback_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_inbound_shipping`
--

DROP TABLE IF EXISTS `amazonzd_inbound_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_inbound_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_inbound_shipping_extra`
--

DROP TABLE IF EXISTS `amazonzd_inbound_shipping_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_inbound_shipping_extra` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poid` (`poid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_inbound_shipping_files`
--

DROP TABLE IF EXISTS `amazonzd_inbound_shipping_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_keywords`
--

DROP TABLE IF EXISTS `amazonzd_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_kit_quantity`
--

DROP TABLE IF EXISTS `amazonzd_kit_quantity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_kit_quantity` (
  `orderid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_log`
--

DROP TABLE IF EXISTS `amazonzd_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=24179 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_log_extended`
--

DROP TABLE IF EXISTS `amazonzd_log_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=17182 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_log_extended_skus`
--

DROP TABLE IF EXISTS `amazonzd_log_extended_skus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports`
--

DROP TABLE IF EXISTS `amazonzd_order_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1765 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports_by_date`
--

DROP TABLE IF EXISTS `amazonzd_order_reports_by_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports_by_date_items`
--

DROP TABLE IF EXISTS `amazonzd_order_reports_by_date_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports_by_last_update`
--

DROP TABLE IF EXISTS `amazonzd_order_reports_by_last_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports_by_last_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1039 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports_by_last_update_orders`
--

DROP TABLE IF EXISTS `amazonzd_order_reports_by_last_update_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports_by_last_update_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=3703 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_order_reports_by_last_update_orders_items`
--

DROP TABLE IF EXISTS `amazonzd_order_reports_by_last_update_orders_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_order_reports_by_last_update_orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=3730 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_orders_uploaded`
--

DROP TABLE IF EXISTS `amazonzd_orders_uploaded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_qty_scripts`
--

DROP TABLE IF EXISTS `amazonzd_qty_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_qty_scripts` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_refunds`
--

DROP TABLE IF EXISTS `amazonzd_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` int(10) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_relationships`
--

DROP TABLE IF EXISTS `amazonzd_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_removal_order`
--

DROP TABLE IF EXISTS `amazonzd_removal_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_removal_order_complete`
--

DROP TABLE IF EXISTS `amazonzd_removal_order_complete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_removal_order_complete` (
  `orderid` varchar(255) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_removal_order_info`
--

DROP TABLE IF EXISTS `amazonzd_removal_order_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_removal_order_log`
--

DROP TABLE IF EXISTS `amazonzd_removal_order_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_removal_order_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `report_sku` varchar(255) NOT NULL,
  `removalid` int(11) NOT NULL,
  `userid` int(10) NOT NULL,
  `msg` text NOT NULL,
  `datecreated` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_removal_shipping`
--

DROP TABLE IF EXISTS `amazonzd_removal_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_ss_map`
--

DROP TABLE IF EXISTS `amazonzd_ss_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_template_att`
--

DROP TABLE IF EXISTS `amazonzd_template_att`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_template_att_values`
--

DROP TABLE IF EXISTS `amazonzd_template_att_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_template_category`
--

DROP TABLE IF EXISTS `amazonzd_template_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_template_category_attributes`
--

DROP TABLE IF EXISTS `amazonzd_template_category_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL,
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_tlid_map`
--

DROP TABLE IF EXISTS `amazonzd_tlid_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_tlid_map_secondary`
--

DROP TABLE IF EXISTS `amazonzd_tlid_map_secondary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `amazonzd_unshipped_orders`
--

DROP TABLE IF EXISTS `amazonzd_unshipped_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amazonzd_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


ALTER TABLE `product_list` ADD `amazonzd_asin` VARCHAR( 50 ) NOT NULL ,
ADD `amazonzd_am_qty` INT( 11 ) NOT NULL ,
ADD `amazonzd_am_std_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazonzd_am_exp_ship` DOUBLE( 16, 2 ) NOT NULL ,
ADD `amazonzd_realtime` INT( 11 ) NOT NULL ,
ADD `amazonzd_am_latency` INT( 11 ) NOT NULL ,
ADD `amazonzd_use_asin` INT( 11 ) NOT NULL ,
ADD `amazonzd_am_update_override` INT( 11 ) NOT NULL ,
ADD `amazonzd_am_condition` ENUM( 'New', 'UsedLikeNew', 'VeryGood', 'Good', 'Acceptable', 'Refurbished' ) NOT NULL ;


CREATE TABLE IF NOT EXISTS `amazonzd_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazonzd_removal_order_complete` (
  `orderid` varchar(255) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `amazonzd_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `amazonzd_removal_order_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `report_sku` varchar(255) NOT NULL,
  `removalid` int(11) NOT NULL,
  `userid` int(10) NOT NULL,
  `msg` text NOT NULL,
  `datecreated` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazonzd_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `amazonzd_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


 CREATE TABLE `amazonzd_item_type` (
`category_id` INT( 10 ) NOT NULL ,
`item_type` VARCHAR( 1000 ) NOT NULL ,
INDEX ( `category_id` )
) ENGINE = MYISAM ;