<?
$addon = "";

if($type != "")
	$addon .= "customer_orders.otype = '$type' ";
else 
	$addon .= "customer_orders.otype != '' ";
	
if($orderid != "") $addon .= " AND customer_orders.orderid = '$orderid'";
if(intval($status) > 0) $addon .= " AND customer_orders.status = '$status'";

if($type == "RETURN" && intval($status) == 0)
	$addon .= " AND customer_orders.status = '0'";
if($type == "EXCHANGE" && intval($status) == 0)
	$addon .= " AND customer_orders.status = '0'";
	
if($on != "") $addon .= " AND customer_orders.invoiceid like '%$on%' ";
if($b2s2 != "") $addon .= " AND (customer_orders.bill2name like '%$b2s2%' OR customer_orders.ship2name like '%$b2s2%') ";
if($zipcode != "") $addon .= " AND (customer_orders.bill2zip like '$zipcode%' OR customer_orders.ship2zip like '$zipcode%') ";
if($emailaddr != "") $addon .= " AND customer_orders.emailaddr like '$emailaddr%' ";

if($ebayacctid != "") 
{
	$addon .= " AND customer_accounts.ebayacctid like '$ebayacctid%' AND customer_accounts.acctid = customer_orders.acctid";
	
	$addontables .= ", customer_accounts ";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($cat4 > 0 && $cat0 > 0 && $cat1 > 0 && $cat2 > 0 && $cat3 > 0){ $addontables .= ", order_category_map "; $addon .= " AND customer_orders.orderid = order_category_map.orderid AND order_category_map.catid = '$cat4'"; }
else if($cat3 > 0 && $cat0 > 0 && $cat1 > 0 && $cat2 > 0){ $addontables .= ", order_category_map "; $addon .= " AND customer_orders.orderid = order_category_map.orderid AND order_category_map.catid = '$cat3'"; }
else if($cat2 > 0 && $cat0 > 0 && $cat1 > 0){ $addontables .= ", order_category_map "; $addon .= " AND customer_orders.orderid = order_category_map.orderid AND order_category_map.catid = '$cat2'"; }
else if($cat1 > 0 && $cat0 > 0){ $addontables .= ", order_category_map "; $addon .= " AND customer_orders.orderid = order_category_map.orderid AND order_category_map.catid = '$cat1'"; }
else if($cat0 > 0){ $addontables .= ", order_category_map "; $addon .= " AND customer_orders.orderid = order_category_map.orderid AND order_category_map.catid = '$cat0'"; }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(intval($older) > 0)
{
	$agedate = time() - (86400 * $older);
	$addon .= " AND orderdate <= '".$agedate."' ";
}

if($phone != "")
{
	$usephone = "";
	for($xx = 0; $xx < strlen($phone); $xx++)
	{
		if(ereg("[0-9]", $phone[$xx]))
		{
			$usephone .= $phone[$xx];
		}
	}
	
	$addon .= " AND (customer_orders.phone like '%$usephone%' OR customer_orders.altphone like '%$usephone%') ";
}

// URGH SKU

if(floatval($min) > 0.00) $addon .= " AND customer_orders.total >= $min";
if(floatval($max) > 0.00) $addon .= " AND customer_orders.total <= $max";

if($disable_range != "true")
{
	if($bdate != "") $addon .= " AND customer_orders.orderdate >= '".strtotime("$bdate 00:00:00")."'";
	if($edate != "") $addon .= " AND customer_orders.orderdate <= '".strtotime("$edate 23:59:59")."'";
}

if($disable_srange != "true")
{
	if($bsdate != "") $addon .= " AND customer_order_lines.shipdate >= '".strtotime("$bsdate 00:00:00")."'";
	if($esdate != "") $addon .= " AND customer_order_lines.shipdate <= '".strtotime("$esdate 23:59:59")."'";
}

if(intval($assignedto) > 0) $addon .= " AND customer_orders.arepid = '$assignedto'";

if($origintype == "web")
{
	$addon .= " AND customer_orders.clickid != 'PH' AND 
					customer_orders.clickid != 'AM' AND 
					customer_orders.clickid != 'EB'";
}
else if($origintype == "phone")
{
	$addon .= " AND customer_orders.clickid = 'PH' ";
}
else if($origintype == "amazon")
{
	$addon .= " AND customer_orders.clickid = 'AM' ";
}

//echo "NOPO: $nopo<BR>";

if($nopo == 1)
{
	$addon .= " AND customer_orders.invoiceid NOT IN (select invoiceid from vendor_pos where invoiceid != '') ";
}

if($lockedup == 1)
{
	$addon .= " AND customer_orders.lockedup = 1 ";
}

$sku = trim($sku);

if($sku != "" || $disable_srange != "true")
{
	if(strlen($sku) > 3)
	{
		$addon .= " AND customer_order_lines.manpart = '$sku' "	;
	}
	
	if($addon != "")
		$addon .= " AND ";
		
		
	$query = "select distinct(customer_orders.invoiceid), customer_orders.smid, customer_orders.orderid, customer_orders.otype, customer_orders.orderdate, customer_orders.status,
				customer_orders.bill2name, customer_orders.ship2name, customer_orders.total, customer_orders.category,
				customer_orders.clicktype, customer_orders.clickid, customer_orders.brand, customer_orders.csmethod, customer_orders.repid, customer_orders.arepid, customer_orders.ship2state,
				customer_orders.subtotal
				from 
					customer_orders, customer_order_lines $addontables where $addon 
					customer_orders.orderid = customer_order_lines.orderid
					group by customer_orders.orderid
					order by customer_orders.orderdate
					";	
}
else 
{
	$query = "select distinct(customer_orders.invoiceid), customer_orders.smid, customer_orders.orderid, customer_orders.otype, customer_orders.orderdate, customer_orders.status,
				customer_orders.bill2name, customer_orders.ship2name, customer_orders.total, customer_orders.category,
				customer_orders.clicktype, customer_orders.clickid, customer_orders.brand, customer_orders.csmethod, customer_orders.repid, customer_orders.arepid, customer_orders.ship2state,
				customer_orders.subtotal
				from 
				customer_orders $addontables where $addon
				group by customer_orders.invoiceid
				order by customer_orders.orderdate
				";
	
}

//echo $query;

if(!$q4ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
$totalnum = mysql_num_rows($q4ok);
if(mysql_num_rows($q4ok) > 0)
{
		
	if(intval($num2return) <= 0)	$num2return = 50;
	
	$rtotal = 0.00;
	$rstotal = 0.00;
	
	ob_start();
	
	while( (list($invoiceid, $smid, $orderid, $otype, $orderdate, $status, $bill2name, $ship2name, $total, $category, $clicktype, $clickid, $brand, $csmethod, $repid, $arepid, $ship2state, $subtotal) = 
			mysql_fetch_array($q4ok)) && 
		($numout < $num2return))
	{
		$numout++;
		
		
		// Get the main item on the order
		$query = "select manname, manpart, descr, prodid from customer_order_lines where orderid = '$orderid' order by price desc limit 0,1";
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($manname, $manpart, $descr, $mainItemID) = mysql_fetch_array($q2ok);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$showcat = $catname = "";
		$parentid = 1;
			
		if($category == "")
		{
			$query = "select catid from product_category_map where pid = '$mainItemID' order by catid desc limit 0,1";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($category) = mysql_fetch_array($q2ok);
			
			$query = "update customer_orders set category = '$category' where orderid = '$orderid'";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		while(intval($parentid) != 0)
		{
			$query = "select catname, parentid from product_categories where catid= '$category'";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catname, $parentid) = mysql_fetch_array($q2ok);
			
			$showcat[] = $catname;
				
			$category = $parentid;
		}
		
		$displaycat	= $space = "";
		$scount = 0;
		
		for($xy = count($showcat); $xy >= 0; $xy--)
		{
			for($pp = 0; $pp < $scount; $pp++) $space .= ".";
			
			if($displaycat == "")
				$displaycat .= $showcat[$xy];
			else 
				$displaycat .= "<BR>$space".$showcat[$xy];
				
			$scount++;
		}
		if(count($showcat) > 0)
		{
			$displaycat = str_replace("\"", "", $displaycat);
			$displaycat = str_replace("'", "", $displaycat);
			
			if($showcat[0] != "")
						$usecategory = "<a href='javascript:void(0);' onmouseover=\"showNotesTree('<span class=g>$displaycat</span>');\" onmouseout=\"hideNotes();\"><img src='/mgt/images/tree.gif' width=16 height=16 border=0></a>".$showcat[1];
			else 
				$usecategory = "";
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$orderdate = date("m/d/Y H:i:s", $orderdate);
		
		if($otype == "INVOICE")
			$status = $efc->lookupOrderStatus($status);
		else if($otype == "RETURN")
			$status =$efc->getReturnStatus($status);
		else if($otype == "EXCHANGE")
			$status =$efc->getExchangeStatus($status);
			
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$query = "select manname from product_manufacturers where manid = '$brand'";
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($manname) = mysql_fetch_array($q2ok);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if(intval($clicktype) == "0") 
		{
			if($clickid == "")
				$origin = "Unk";
			else 
				$origin = $clickid;
		}
		if($clicktype == "1") $origin = "PE:$clickid";
		if($clicktype == "2") $origin = "CA:$clickid";
		if($clicktype == "3") $origin = "SE:".$efc->lookupSE($clickid, $mysqlid);
		if($clicktype == "4") $origin = "DR:$clickid";
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// see if there are notes on the order
		$query = "select orderid from customer_order_notes_internal where orderid = '$orderid'";
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($q2ok) > 0)
		{
			$notes = "<a href='javascript:void(0);' onmouseover=\"showNotes($orderid);\" onmouseout=\"hideNotes();\"><img src='/mgt/images/notes.gif' width=12 height=16 border=0></a>";
		}
		else 
		{
			$notes = "";
		}

		$notes .= "&nbsp;&nbsp;<a href='javascript:void(0);' onmouseover=\"showEmail($orderid);\" onmouseout=\"hideNotes();\"><img src='/mgt/images/e-mail.gif' width=12 height=16 border=0></a>";
		

		///////////////////////////////////////////
		//45
		$descr = htmlentities($descr);
		$mainitem = "$manpart - $descr";
		
		if(strlen($mainitem) > 30)
		{
			$mainitem = substr($mainitem, 0, 27);
			$mainitem .= "<a href='javascript:void(0);' onmouseover=\"showDescr('<span class=g>$manname - $manpart - $descr</span>');\" onmouseout=\"hideNotes();\"><img src='/mgt/images/eshirt.gif' width=16 height=16 border=0></a>";
		}
		
		$csmethod = trim($csmethod);
		
		
		
		//////////////////////////////////////
		// 
		if(intval($arepid) > 0)
		{
			$query = "select displayname from users where id = '$arepid'";	
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($repname) = mysql_fetch_array($q2ok);
		}
		else 
			$repname = "-";
			

		if($type != "INVOICE")
			$stockstatus = 1;
		else
			$stockstatus = $efc->getOrderShipStatus($orderid);
			
		$xotype = substr($type, 0, 1);
		
//  ORDER LINE !
		echo "$orderid\t$invoiceid\t$xotype $notes\t$orderdate\t$ship2state\t$repname\t$status\t$bill2name\t$total\t$csmethod\t$usecategory\t$mainitem\t$origin\t$stockstatus\n";
	
		$rtotal += $total;
		$rstotal += $subtotal;
	}
	
	$avgorder = number_format($rtotal / $numout, 2);
	$rtotal= number_format($rtotal, 2);
	$rstotal= number_format($rstotal, 2);

	$data = ob_get_clean();
	
	if($efc->getUserType() == "MANAGER")
	{
		//echo "$totalnum orders displaying $numout<BR>$ $avgorder Avg<BR>$ $rtotal Total<BR>$ $rstotal (subtotal)\n";
	}
	else 
	{
		//echo " \n";	
	}
	
	$data = strip_tags($data);
	$data = str_replace("&nbsp;","",$data);
    echo "InvoiceID\tType\tDate\tState\tSalesman\tStatus\tBill to/Ship to\tTotal\tMethod\tCategory\tMain Item\tOrigin\n";
    echo $data;
	
	//echo "-\t-\t-\t-\t<B>$numout orders</B>\t<B><div align=right>$ $rtotal</div></B>\t-\t<B>$ $avgorder Avg</B>\t-\t-\n";
	
}
else 
{
	echo "0";	
}
?>