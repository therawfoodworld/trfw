<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
<DocumentVersion>1.01</DocumentVersion>
<MerchantIdentifier>M_BUTTERFLYP_388783</MerchantIdentifier>
</Header>
<MessageType>OrderReport</MessageType>
<Message>
<MessageID>1</MessageID>
<OrderReport>
<AmazonOrderID>002-4617780-5222652</AmazonOrderID>
<AmazonSessionID>002-4617780-5222652</AmazonSessionID>
<OrderDate>2011-03-10T11:33:41-08:00</OrderDate>
<OrderPostedDate>2011-03-10T11:33:41-08:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>p61y3d0vtr2k10v@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>Mark A. Connors</BuyerName>
<BuyerPhoneNumber>(412) 851-9119</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>Mark A. Connors</Name>
<AddressFieldOne>6162 Library Road</AddressFieldOne>
<AddressFieldTwo>Apt 53</AddressFieldTwo>
<City>Bethel Park</City>
<StateOrRegion>PA</StateOrRegion>
<PostalCode>15102-4056</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>(412) 851-9119</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>49028402693546</AmazonOrderItemCode>
<SKU>40447</SKU>
<Title>2 Pack Battery Kit For Sanyo VPC-GH4, VPC-CG102, VPC-GH2, VPC-CS1, VPC-CA102, VPC-CG10, VPC-CG20 Digital Camcorder Includes 2 Extended (900Mah) Replac</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1299785592</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>184-2152030-4902666</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiIC@fdc7</Type>
<Data>M:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-5381856-0233848:40042984ce23d89c596aefdac093a2ef</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>40042984ce23d89c596aefdac093a2ef</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4D792778BDF5000000011DFD1CC4</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0218IX9C2U7</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>2 Pack Battery Kit For Sanyo VPC-GH4, VPC-CG102, VPC-GH2, VPC-CS1, VPC-CA102...</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">16.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">2.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-1.60</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>390036760</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>ALLEGHENY</County>
<State>PA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">16.95</County>
<State currency="USD">16.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">16.95</District>
<City currency="USD">16.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>390036760</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>ALLEGHENY</County>
<State>PA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">2.95</County>
<State currency="USD">2.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">2.95</District>
<City currency="USD">2.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
<Item>
<AmazonOrderItemCode>13690453625666</AmazonOrderItemCode>
<SKU>40734</SKU>
<Title>Essential Accessory Kit For Sanyo VPC-GH4 VPC-GH2 VPC-WH1 VPC-FH1A VPC-SH1 VPC-SH1R VPC-SH1 VPC-TH1 Camcorder Includes 50" Tripod + Compact Hard Shell</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1299785506</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>184-2152030-4902666</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiIC@fdc7</Type>
<Data>M:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-5381856-0233848:568dfda2c18d1d38de9eaf39b94aea1c</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>568dfda2c18d1d38de9eaf39b94aea1c</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4D792722BDF5000000011DFCC164</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0304I6SJM8J</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>Essential Accessory Kit For Sanyo VPC-GH4 VPC-GH2 VPC-WH1 VPC-FH1A VPC-SH1 VPC</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">24.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">4.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-2.40</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>390036760</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>ALLEGHENY</County>
<State>PA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">24.95</County>
<State currency="USD">24.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">24.95</District>
<City currency="USD">24.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>390036760</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>ALLEGHENY</County>
<State>PA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">4.95</County>
<State currency="USD">4.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">4.95</District>
<City currency="USD">4.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
<Message>
<MessageID>2</MessageID>
<OrderReport>
<AmazonOrderID>103-0875166-8771448</AmazonOrderID>
<AmazonSessionID>103-0875166-8771448</AmazonSessionID>
<OrderDate>2011-03-10T11:45:21-08:00</OrderDate>
<OrderPostedDate>2011-03-10T11:45:21-08:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>62q8wgq93wln6zh@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>Norman Jemal</BuyerName>
<BuyerPhoneNumber>9179228499</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>Norman Jemal</Name>
<AddressFieldOne>1385 Broadway</AddressFieldOne>
<AddressFieldTwo>16th floor</AddressFieldTwo>
<City>New York</City>
<StateOrRegion>NY</StateOrRegion>
<PostalCode>10018</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>9179228499</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>67243791002746</AmazonOrderItemCode>
<SKU>20717</SKU>
<Title>2 Pack of Panasonic DMW-BCG10 Replacement Batteries 1200MAH Each For Panasonic Lumix DMC-ZS5 DMC-ZS7 DMC-ZS1, DMC-TZ7S, DMC-TZ7T, DMC-ZS3, DMC-ZS1K, D</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1299786281</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiA3n?fc7</Type>
<Data>A:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>176-6056416-9809169</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-6080452-6382649:-670e1de54af6898c29a262f757e34c9</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>-670e1de54af6898c29a262f757e34c92</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>2#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>0318GCCY157</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>2 Pack of Panasonic DMW-BCG10 Replacement Batteries 1200MAH Each For Panasonic</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">26.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">2.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-2.40</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>330612010</TaxLocationCode>
<City>NEW YORK</City>
<County>NOT APPLICABLE</County>
<State>NY</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">26.95</District>
<City currency="USD">26.95</City>
<County currency="USD">0.00</County>
<State currency="USD">26.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">26.95</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>330612010</TaxLocationCode>
<City>NEW YORK</City>
<County>NOT APPLICABLE</County>
<State>NY</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">2.95</District>
<City currency="USD">2.95</City>
<County currency="USD">0.00</County>
<State currency="USD">2.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">2.95</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
<Message>
<MessageID>3</MessageID>
<OrderReport>
<AmazonOrderID>102-8410926-9091414</AmazonOrderID>
<AmazonSessionID>102-8410926-9091414</AmazonSessionID>
<OrderDate>2011-03-10T11:38:39-08:00</OrderDate>
<OrderPostedDate>2011-03-10T11:38:39-08:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>9cqlz03cpbrzs6c@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>Phaedra J Morgan</BuyerName>
<BuyerPhoneNumber>4158749141</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Standard</FulfillmentServiceLevel>
<Address>
<Name>Phaedra Morgan</Name>
<AddressFieldOne>Levi Strauss &amp; Co.</AddressFieldOne>
<AddressFieldTwo>1155 Battery Street</AddressFieldTwo>
<City>San Francisco</City>
<StateOrRegion>CA</StateOrRegion>
<PostalCode>94111</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>415-501-6481</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>67307242320530</AmazonOrderItemCode>
<SKU>35196</SKU>
<Title>High Resolution 40.5mm 3-piece Filter Set (UV, Fluorescent, Polarizer) For The Olympus PEN E-PL1 E-PL2 E-P1 E-P2 Digital Camera Which Has The ZUIKO Di</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1299784684</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>180-7594135-8234920</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-7834260-6745049:-410f9210735eccd648aca0b2eaa6785</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>-410f9210735eccd648aca0b2eaa67857</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000125</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4D7923ECCBC0000000011DFEF563</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>7#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiY?X\ec7</Type>
<Data>A:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>1130GHE6X27</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>High Resolution 40.5mm 3-piece Filter Set (UV, Fluorescent, Polarizer) For The</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">14.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">2.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-1.44</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>050753040</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>SAN FRANCISCO</County>
<State>CA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">14.95</District>
<City currency="USD">0.00</City>
<County currency="USD">14.95</County>
<State currency="USD">14.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">14.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>050753040</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>SAN FRANCISCO</County>
<State>CA</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">2.95</District>
<City currency="USD">0.00</City>
<County currency="USD">2.95</County>
<State currency="USD">2.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">2.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
</AmazonEnvelope>
