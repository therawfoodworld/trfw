<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
<DocumentVersion>1.01</DocumentVersion>
<MerchantIdentifier>M_BUTTERFLYP_388783</MerchantIdentifier>
</Header>
<MessageType>OrderReport</MessageType>
<Message>
<MessageID>1</MessageID>
<OrderReport>
<AmazonOrderID>002-9303146-4286624</AmazonOrderID>
<AmazonSessionID>002-9303146-4286624</AmazonSessionID>
<OrderDate>2011-05-29T20:38:27-07:00</OrderDate>
<OrderPostedDate>2011-05-29T20:38:27-07:00</OrderPostedDate>
<BillingData>
<BuyerEmailAddress>p26plpg9lxnzgr8@marketplace.amazon.com</BuyerEmailAddress>
<BuyerName>Hassan M. Farah</BuyerName>
<BuyerPhoneNumber>909-261-3517</BuyerPhoneNumber>
</BillingData>
<FulfillmentData>
<FulfillmentMethod>Ship</FulfillmentMethod>
<FulfillmentServiceLevel>Expedited</FulfillmentServiceLevel>
<Address>
<Name>Huralain H. Mohamed</Name>
<AddressFieldOne>1206 George Line</AddressFieldOne>
<City>San Angelo</City>
<StateOrRegion>Texas</StateOrRegion>
<PostalCode>76905</PostalCode>
<CountryCode>US</CountryCode>
<PhoneNumber>9153457297</PhoneNumber>
</Address>
</FulfillmentData>
<Item>
<AmazonOrderItemCode>36845963423042</AmazonOrderItemCode>
<SKU>18838</SKU>
<Title>16GB Essential Accessory Package For Nikon Coolpix L110 L120 L100 Includes 16GB Memory, Case, Tripod, 4 AA High Capacity NIMH Rechargeable Batteries +</Title>
<CustomizationInfo>
<Type>ConditionType</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DeliverySource</Type>
<Data>NEO</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PrimeOnlyCust</Type>
<Data>B0007IJXTE</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:CalcGrpType</Type>
<Data>Purchase</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>highWatermark</Type>
<Data>1</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>addToCartTime</Type>
<Data>1306726204</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PrimeSubId</Type>
<Data>0ADP6HDNGM9E5DKQ6AMQ</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>sessionID</Type>
<Data>175-7880710-1067630</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>FulfillNetwork</Type>
<Data>Marketplace,Ship,873866041</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:dpdiA?7@I:8</Type>
<Data>V:0:</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds0</Type>
<Data>miq://document:1.0/Ordering/amazon:1.0/Unit:1.0/106-1697589-7495460:07d3fa59-fee4-3d30-bee7-91996072</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitIdPrefix</Type>
<Data>07d3fa59-fee4-3d30-bee7-91996072739d</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CustomerOrderInfo:ShipOfferId</Type>
<Data>5000000111</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>Condition</Type>
<Data>new</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestAddressId</Type>
<Data>1077705165</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>PromiseAttr</Type>
<Data>::</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>assocToken</Type>
<Data>4DE30F3C2185000000016C58C144</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>unitGroupIds1</Type>
<Data>739d#0-1,</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>D_DestType</Type>
<Data>home</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>UPC</Type>
<Data>628586129856</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>ListingID</Type>
<Data>1013GJ97G9P</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>CompoundTitle</Type>
<Data>16GB Essential Accessory Package For Nikon Coolpix L110 L120 L100 Includes 16GB</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>DateListed</Type>
<Data>2010-01-01T00:00:00-00:00</Data>
</CustomizationInfo>
<CustomizationInfo>
<Type>MarketplaceID</Type>
<Data>ATVPDKIKX0DER</Data>
</CustomizationInfo>
<Quantity>1</Quantity>
<ProductTaxCode>A_GEN_TAX</ProductTaxCode>
<ItemPrice>
<Component>
<Type>Principal</Type>
<Amount currency="USD">59.95</Amount>
</Component>
<Component>
<Type>Shipping</Type>
<Amount currency="USD">9.95</Amount>
</Component>
<Component>
<Type>Tax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
<Component>
<Type>ShippingTax</Type>
<Amount currency="USD">0.00</Amount>
</Component>
</ItemPrice>
<ItemFees>
<Fee>
<Type>Commission</Type>
<Amount currency="USD">-5.60</Amount>
</Fee>
</ItemFees>
<ItemTaxData>
<TaxJurisdictions>
<TaxLocationCode>444510000</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>TOM GREEN</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">59.95</County>
<State currency="USD">59.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">59.95</District>
<City currency="USD">59.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ItemTaxData>
<ShippingTaxData>
<TaxJurisdictions>
<TaxLocationCode>444510000</TaxLocationCode>
<City>NOT APPLICABLE</City>
<County>TOM GREEN</County>
<State>TX</State>
</TaxJurisdictions>
<TaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxableAmounts>
<NonTaxableAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">9.95</County>
<State currency="USD">9.95</State>
</NonTaxableAmounts>
<ZeroRatedAmounts>
<District currency="USD">9.95</District>
<City currency="USD">9.95</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</ZeroRatedAmounts>
<TaxCollectedAmounts>
<District currency="USD">0.00</District>
<City currency="USD">0.00</City>
<County currency="USD">0.00</County>
<State currency="USD">0.00</State>
</TaxCollectedAmounts>
<TaxRates>
<District>0.0000</District>
<City>0.0000</City>
<County>0.0000</County>
<State>0.0000</State>
</TaxRates>
</ShippingTaxData>
</Item>
</OrderReport>
</Message>
</AmazonEnvelope>
