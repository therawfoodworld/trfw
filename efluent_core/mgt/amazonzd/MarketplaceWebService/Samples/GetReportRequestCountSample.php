<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     MarketplaceWebService
 *  @copyright   Copyright 2009 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-01-01
 */
/******************************************************************************* 

 *  Marketplace Web Service PHP5 Library
 *  Generated: Thu May 07 13:07:36 PDT 2009
 * 
 */

/**
 * Get Report Count  Sample
 */

include_once ('.config.inc.php'); 

/************************************************************************
 * Uncomment to try advanced configuration options. Available options are:
 *
 * - Proxy host and port.
 * - MaxErrorRetry.
 ***********************************************************************/
//$config = array (
//  'ProxyHost' => null,
//  'ProxyPort' => -1,
//  'MaxErrorRetry' => 3,
//);

/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Report Count Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetReportRequestCountRequest
 // object or array of parameters

//$parameters = array (
//  'Marketplace' => MARKETPLACE_ID,
//  'Merchant' => MERCHANT_ID,
//  'ReportProcessingStatusList' => array ('Status' => array ('_SUBMITTED_', '_DONE_')),
//);
//
//$request = new MarketplaceWebService_Model_GetReportRequestCountRequest($parameters);

//$request = new MarketplaceWebService_Model_GetReportRequestCountRequest();
//$request->setMarketplace(MARKETPLACE_ID);
//$request->setMerchant(MERCHANT_ID);
//
//$statusList = new MarketplaceWebService_Model_StatusList();
//$request->setReportProcessingStatusList($statusList->withStatus('_SUBMITTED_', '_DONE_'));
//     
//invokeGetReportRequestCount($service, $request);

                                            
/**
  * Get Report Count Action Sample
  * returns a count of reports matching your criteria;
  * by default, the number of reports generated in the last 90 days,
  * regardless of acknowledgement status
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetReportRequestCount or array of parameters
  */
  function invokeGetReportRequestCount(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReportRequestCount($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        GetReportRequestCountResponse\n");
                if ($response->isSetGetReportRequestCountResult()) { 
                    echo("            GetReportRequestCountResult\n");
                    $getReportRequestCountResult = $response->getGetReportRequestCountResult();
                    if ($getReportRequestCountResult->isSetCount()) 
                    {
                        echo("                Count\n");
                        echo("                    " . $getReportRequestCountResult->getCount() . "\n");
                    }
                } 
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
?>
