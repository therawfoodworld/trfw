<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     MarketplaceWebService
 *  @copyright   Copyright 2009 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-01-01
 */
/******************************************************************************* 
 *  Marketplace Web Service PHP5 Library
 *  Generated: Thu May 07 13:07:36 PDT 2009
 * 
 */

/**
 * Get Feed Submission Count  Sample
 */

include_once ('.config.inc.php');

/************************************************************************
 * Uncomment to try advanced configuration options. Available options are:
 *
 * - Proxy host and port.
 * - MaxErrorRetry.
 ***********************************************************************/
//$config = array (
//  'ProxyHost' => null,
//  'ProxyPort' => -1,
//  'MaxErrorRetry' => 3,
//);

/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();
 
/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Feed Submission Count Action
 ***********************************************************************/
// @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetFeedSubmissionCountRequest

//$parameters = array (
//  'Marketplace' => MARKETPLACE_ID,
//  'Merchant' => MERCHANT_ID,
//  'FeedProcessingStatusList' => array ('Status' => array ('_DONE_', '_SUBMITTED_')),
//);
//
//$request = new MarketplaceWebService_Model_GetFeedSubmissionCountRequest($parameters);
     
//$request = new MarketplaceWebService_Model_GetFeedSubmissionCountRequest();
//$request->setMerchant(MERCHANT_ID);
//$request->setMarketplace(MARKETPLACE_ID);
                                       
// object or array of parameters
//invokeGetFeedSubmissionCount($service, $request);
                                                            
 /**
  * Get Feed Submission Count Action Sample
  * returns the number of feeds matching all of the specified criteria
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetFeedSubmissionCount or array of parameters
  */
function invokeGetFeedSubmissionCount(MarketplaceWebService_Interface $service, $request)  {
      try {
              $response = $service->getFeedSubmissionCount($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        GetFeedSubmissionCountResponse\n");
                if ($response->isSetGetFeedSubmissionCountResult()) { 
                    echo("            GetFeedSubmissionCountResult\n");
                    $getFeedSubmissionCountResult = $response->getGetFeedSubmissionCountResult();
                    if ($getFeedSubmissionCountResult->isSetCount()) 
                    {
                        echo("                Count\n");
                        echo("                    " . $getFeedSubmissionCountResult->getCount() . "\n");
                    }
                } 
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

     } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
     }
 }
 ?>
                                            
