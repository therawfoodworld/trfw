<?php
$uploaddir = './uploads/'; 
$file = $uploaddir . basename($_FILES['uploadfile']['name']); 
$size=$_FILES['uploadfile']['size'];

if(stristr($file, ".php") || stristr($file, ".phtml") || stristr($file, ".zip"))
{
	echo "Error";
	exit;
}

if($size>1048576)
{
	echo "error file size > 1 MB";
	unlink($_FILES['uploadfile']['tmp_name']);
	exit;
}
if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) 
{ 
  echo "success"; 
} 
else 
{
	echo "error ".$_FILES['uploadfile']['error']." --- ".$_FILES['uploadfile']['tmp_name']." %%% ".$file."($size)";
}
?>