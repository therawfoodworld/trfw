<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" media="all" />
<title>RSA</title>
</head>

<body>

<center>
<div style="width: 80%; position:absolute; left:10%; top:0%; z-index:1">
<br />
<div class="tabArea" align="center">
  <a class="tab" href="example.php">Example</a>
      <a class="tab" href="about.html">About RSA</a> 
      <a class="tab" href="DigitalSignature.html">About Digital Signature</a> 
</div>

<div class="Paragraph">
<?php
include('rsa.class.php');
$RSA = new RSA();

/* Example */
$old_card = "xzDt3KKWJ6xZu3b2GVee4EdvPRtGk4VzyfNkxePSm1HiqgcRxuP9JDczvKsHu9/dYsltN7YAi8ZDaF/J0QUEUX3lJL07iaT932pd/AEL0ZEQE/NEUsbKkoZiLSMg5dwecW4ZwJYVAtTOBYJmMnBR70tzaejtXPlbYt9aNmLU9QN5hkGbRaOvOPDfSs9i85Kt0hsAjuhmei0fxvXoXiQL3rGJAq9A/sB6IxOsW+1HSORlweNxgscs8RGItDGQWRcG7JPpiXOi2z3KCZzWN1AaKfbEUg3HvhLdgUbsaWHyW1wkcZsVwESGmUnRV2QX90qcFMhFjdeioceM7vKY42CfXg==";

echo "<B>Old Card:</B>".base64_decode($old_card)."<BR>\n";

echo"<i>Keys:</i><br />";
$keys = $RSA->generate_keys ('091283741', '9076501243', 1);

$message="5263400273994197";
$encoded = $RSA->encrypt ($message, $keys[1], $keys[0], 5);
$decoded = $RSA->decrypt ($encoded, $keys[2], $keys[0]);

echo "<b>Message:</b> $message<br />\n";
echo "<b>Encoded:</b> $encoded<br />\n";
echo "<B>Encoded+base64</B>:".base64_encode($encoded)."<br />\n";
echo "<b>Decoded:</b> $decoded<br />\n";
echo "Success: ".(($decoded == $message) ? "True" : "False")."<hr />\n";

$message = "This is my original message";
$signature = $RSA->sign($message, $keys[1], $keys[0]);
echo "<b>Original Message:</b> <div dir=rtl>$message</div><br />\n";
echo "<b>Message Signature:</b><br /> $signature<br /><br />\n";

$fake_msg = "Fake Message";
echo "<b>Fake Message:</b> <div dir=rtl>$fake_msg</div><br />\n";

echo "<b>Check original message against given signature:</b><br />\n";
echo "Success: ".(($RSA->prove($message, $signature, $keys[2], $keys[0])) ? "True" : "False")."<br /><br />\n";

echo "<b>Check fake message against given signature:</b><br />\n";
echo "Success: ".(($RSA->prove($fake_msg, $signature, $keys[2], $keys[0])) ? "True" : "False")."<br /><hr />\n";

$file = 'about.html';
$signature = $RSA->signFile($file, $keys[1], $keys[0]);
echo "<b>Original File:</b> $file<br /><br />\n";
echo "<b>File Signature:</b><br /> $signature<br /><br />\n";

$fake_file = 'style.css';
echo "<b>Fake File:</b> $fake_file<br /><br />\n";

echo "<b>Check original file against given signature:</b><br />\n";
echo "Success: ".(($RSA->proveFile($file, $signature, $keys[2], $keys[0])) ? "True" : "False")."<br /><br />\n";

echo "<b>Check fake file against given signature:</b><br />\n";
echo "Success: ".(($RSA->proveFile($fake_file, $signature, $keys[2], $keys[0])) ? "True" : "False")."<br /><br />\n";

?>
</div>
</center>
          <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
          </script>
          <script type="text/javascript">
          _uacct = "UA-1268287-1";
          urchinTracker();
          </script>
</body>
</html>
