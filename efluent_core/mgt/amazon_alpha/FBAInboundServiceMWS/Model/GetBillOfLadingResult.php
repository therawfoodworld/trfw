<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     FBAInboundServiceMWS
 *  @copyright   Copyright 2008-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2010-10-01
 */
 
/******************************************************************************* 
 * 
 *  FBA Inbound Service MWS PHP5 Library
 *  Generated: Fri Sep 13 02:43:24 GMT 2013
 * 
 */

/**
 *  @see FBAInboundServiceMWS_Model
 */

require_once (dirname(__FILE__) . '/../Model.php');


/**
 * FBAInboundServiceMWS_Model_GetBillOfLadingResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>TransportDocument: FBAInboundServiceMWS_Model_TransportDocument</li>
 *
 * </ul>
 */

 class FBAInboundServiceMWS_Model_GetBillOfLadingResult extends FBAInboundServiceMWS_Model {

    public function __construct($data = null)
    {
        $this->_fields = array (
            'TransportDocument' => array('FieldValue' => null, 'FieldType' => 'FBAInboundServiceMWS_Model_TransportDocument'),
        );
	    parent::__construct($data);
    }

    /**
     * Get the value of the TransportDocument property.
     *
     * @return TransportDocument TransportDocument.
     */
    public function getTransportDocument()
	{
	    return $this->_fields['TransportDocument']['FieldValue'];
    }

    /**
     * Set the value of the TransportDocument property.
     *
     * @param FBAInboundServiceMWS_Model_TransportDocument transportDocument
     * @return this instance
     */
    public function setTransportDocument($value)
	{
	    $this->_fields['TransportDocument']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if TransportDocument is set.
     *
     * @return true if TransportDocument is set.
     */
    public function isSetTransportDocument()
	{
	            return !is_null($this->_fields['TransportDocument']['FieldValue']);
		    }

    /**
     * Set the value of TransportDocument, return this.
     *
     * @param transportDocument
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withTransportDocument($value)
	{
        $this->setTransportDocument($value);
        return $this;
    }

}
