<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     FBAInboundServiceMWS
 *  @copyright   Copyright 2008-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2010-10-01
 */
 
/******************************************************************************* 
 * 
 *  FBA Inbound Service MWS PHP5 Library
 *  Generated: Fri Sep 13 02:43:24 GMT 2013
 * 
 */

/**
 *  @see FBAInboundServiceMWS_Model
 */

require_once (dirname(__FILE__) . '/../Model.php');


/**
 * FBAInboundServiceMWS_Model_ListInboundShipmentItemsResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>ItemData: FBAInboundServiceMWS_Model_InboundShipmentItemList</li>
 * <li>NextToken: string</li>
 *
 * </ul>
 */

 class FBAInboundServiceMWS_Model_ListInboundShipmentItemsResult extends FBAInboundServiceMWS_Model {

    public function __construct($data = null)
    {
        $this->_fields = array (
            'ItemData' => array('FieldValue' => null, 'FieldType' => 'FBAInboundServiceMWS_Model_InboundShipmentItemList'),
            'NextToken' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
	    parent::__construct($data);
    }

    /**
     * Get the value of the ItemData property.
     *
     * @return InboundShipmentItemList ItemData.
     */
    public function getItemData()
	{
	    return $this->_fields['ItemData']['FieldValue'];
    }

    /**
     * Set the value of the ItemData property.
     *
     * @param FBAInboundServiceMWS_Model_InboundShipmentItemList itemData
     * @return this instance
     */
    public function setItemData($value)
	{
	    $this->_fields['ItemData']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if ItemData is set.
     *
     * @return true if ItemData is set.
     */
    public function isSetItemData()
	{
	            return !is_null($this->_fields['ItemData']['FieldValue']);
		    }

    /**
     * Set the value of ItemData, return this.
     *
     * @param itemData
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withItemData($value)
	{
        $this->setItemData($value);
        return $this;
    }

    /**
     * Get the value of the NextToken property.
     *
     * @return String NextToken.
     */
    public function getNextToken()
	{
	    return $this->_fields['NextToken']['FieldValue'];
    }

    /**
     * Set the value of the NextToken property.
     *
     * @param string nextToken
     * @return this instance
     */
    public function setNextToken($value)
	{
	    $this->_fields['NextToken']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if NextToken is set.
     *
     * @return true if NextToken is set.
     */
    public function isSetNextToken()
	{
	            return !is_null($this->_fields['NextToken']['FieldValue']);
		    }

    /**
     * Set the value of NextToken, return this.
     *
     * @param nextToken
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withNextToken($value)
	{
        $this->setNextToken($value);
        return $this;
    }

}
