<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     FBAInboundServiceMWS
 *  @copyright   Copyright 2008-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2010-10-01
 */
 
/******************************************************************************* 
 * 
 *  FBA Inbound Service MWS PHP5 Library
 *  Generated: Fri Sep 13 02:43:24 GMT 2013
 * 
 */

/**
 *  @see FBAInboundServiceMWS_Model
 */

require_once (dirname(__FILE__) . '/../Model.php');


/**
 * FBAInboundServiceMWS_Model_NonPartneredSmallParcelDataInput
 * 
 * Properties:
 * <ul>
 * 
 * <li>CarrierName: string</li>
 * <li>PackageList: FBAInboundServiceMWS_Model_NonPartneredSmallParcelPackageInputList</li>
 *
 * </ul>
 */

 class FBAInboundServiceMWS_Model_NonPartneredSmallParcelDataInput extends FBAInboundServiceMWS_Model {

    public function __construct($data = null)
    {
        $this->_fields = array (
            'CarrierName' => array('FieldValue' => null, 'FieldType' => 'string'),
            'PackageList' => array('FieldValue' => null, 'FieldType' => 'FBAInboundServiceMWS_Model_NonPartneredSmallParcelPackageInputList'),
        );
	    parent::__construct($data);
    }

    /**
     * Get the value of the CarrierName property.
     *
     * @return String CarrierName.
     */
    public function getCarrierName()
	{
	    return $this->_fields['CarrierName']['FieldValue'];
    }

    /**
     * Set the value of the CarrierName property.
     *
     * @param string carrierName
     * @return this instance
     */
    public function setCarrierName($value)
	{
	    $this->_fields['CarrierName']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if CarrierName is set.
     *
     * @return true if CarrierName is set.
     */
    public function isSetCarrierName()
	{
	            return !is_null($this->_fields['CarrierName']['FieldValue']);
		    }

    /**
     * Set the value of CarrierName, return this.
     *
     * @param carrierName
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withCarrierName($value)
	{
        $this->setCarrierName($value);
        return $this;
    }

    /**
     * Get the value of the PackageList property.
     *
     * @return NonPartneredSmallParcelPackageInputList PackageList.
     */
    public function getPackageList()
	{
	    return $this->_fields['PackageList']['FieldValue'];
    }

    /**
     * Set the value of the PackageList property.
     *
     * @param FBAInboundServiceMWS_Model_NonPartneredSmallParcelPackageInputList packageList
     * @return this instance
     */
    public function setPackageList($value)
	{
	    $this->_fields['PackageList']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Check to see if PackageList is set.
     *
     * @return true if PackageList is set.
     */
    public function isSetPackageList()
	{
	            return !is_null($this->_fields['PackageList']['FieldValue']);
		    }

    /**
     * Set the value of PackageList, return this.
     *
     * @param packageList
     *             The new value to set.
     *
     * @return This instance.
     */
    public function withPackageList($value)
	{
        $this->setPackageList($value);
        return $this;
    }

}
