delete from customer_order_lines;
delete from customer_order_notes;
delete from customer_order_notes_internal;
delete from customer_order_tracking;
delete from customer_orders;
delete from customer_payments;
delete from customer_billinginfo;
delete from customer_billinginfo_extended;
delete from customer_shippinginfo;