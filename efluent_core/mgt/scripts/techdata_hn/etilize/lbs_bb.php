<?php
/*
ID: 19748 	Date: 2012-03-05 16:42:57 By: Alan 	Status: More Info

Jake, are you able to do this? From MERGED PIDs, if the item has a blank weight in table - scrape the Etilize tech spec we have on the PID for the weight field, santizie and store? If so, lets do it and
schedule to run daily to check blank weights for merged items with etilize data.
*/

$ETILIZE_VENDORNAME = 'UPC';


$ROOT_DIR = $argv[1];

//$ROOT_DIR = "/home/httpd/hardwarenation.com"; // <-----

if($ROOT_DIR == "") { die("ERR: ROOT_DIR !\n"); }

REQUIRE($ROOT_DIR."/config.phtml"); // <-----
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

ini_set('display_errors',1);

//error_reporting(E_ALL);
//display_errors(0);

REQUIRE($ROOT_DIR."/mgt/classes/etilizeclass.phtml");


$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$elc = new etilizeClass();
$elc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$elc->db = $DB;
$elc->ROOT_DIR = $ROOT_DIR;

$vid = $argv[2];

if($vid == "")
{
	echo "... $ROOT_DIR\n";
	$query = "select vendorname,vendorid from vendors";
	if(!$q2 = mysql_db_query($DB, $query, $mysqlid)){ echo "ERROR 1"; exit; }
	while(list($v1,$v2) = mysql_fetch_array($q2)) { echo "VENDOR: $v1 VENDORID: $v2\n";  }
	exit;
}


$VENDORID = $vid;





$yar = 0;
$query = "select manpart,pid,vpid from product_sourcing where distid = '$vid'";
if(!$q1 = mysql_db_query($DB, $query, $mysqlid)){ echo "ERROR 1"; exit; }
while(list($f,$pid,$vpid) = mysql_fetch_array($q1))
{
	$yar++;
}

echo "#$yar MAPPED FOR $vid\n";
sleep(3);






$query = "select manpart,pid,vpid from product_sourcing where distid = '$vid'";
if(!$q1 = mysql_db_query($DB, $query, $mysqlid)){ echo "ERROR 1"; exit; }
while(list($f,$pid,$vpid) = mysql_fetch_array($q1))
{   //product_list.shipweight

	$lbs_stat = false;

	//$query = "select id,shipweight from product_list where id = '$pid' and shipweight = ''";
	$query = "select id,shipweight,upccode from product_list where id = '$pid'";
	echo $query ."\n";
	if(!$qZ = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
	while(list($xid,$xshipweight,$f) = mysql_fetch_array($qZ))
	{

		if($xshipweight == "") 	{ $lbs_stat = true; }
		if($xshipweight == "0.00") { $lbs_stat = true; }

		if(!is_numeric($f))
		{

			$query = "select upccode from vendor_product_list where id = '$vpid' and vid = '$vid'";
			echo $query ."\n";
			if(!$qZ2 = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
			list($f) = mysql_fetch_array($qZ2);

		}



		if($lbs_stat)
		{


			echo "'$xid' / '$xshipweight'\n";


			echo "DISTKSU: $f\n";

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			$query = "select productid from productskus where sku = '$f' and name = '$ETILIZE_VENDORNAME'";

			//echo $query ."\n";

			if(!$q2ok = mysql_db_query("etilize", $query, $mysqlid)){ echo mysql_error(); exit; }
			list($etilizeid) = mysql_fetch_array($q2ok);


			if(is_numeric($etilizeid))
			{
				$query = "select product.productid, manufacturer.name, product.mfgpartno, productdescriptions.description
 				from 
	 					product, manufacturer, productdescriptions
	 						where 
	 							product.manufacturerid = manufacturer.manufacturerid and
	 							product.productid = productdescriptions.productid and
	 							productdescriptions.type = 2 AND product.productid = '$etilizeid'";


				//echo $query."\n";

				if(!$q2ok1 = mysql_db_query("etilize", $query, $mysqlid)){ echo "ERROR ".mysql_error(); exit; }

				while(list($eid, $manname, $manpart, $descr) = mysql_fetch_array($q2ok1))
				{
					echo "sES($pid, $eid);\n";

					sES($pid, $eid, &$mysqlid, $DB);

				}

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			}

		}
	}
}








//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function sES($id, $etilizeid, &$mysqlid, $DB)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// New way as per anees@etilize.com 12/19/2011
	$query = "SELECT hn.name, an.name, pa.displayvalue FROM categoryheader ch
				      JOIN headernames hn ON ch.headerid = hn.headerid AND hn.localeid = 1
				      JOIN categorydisplayattributes cda ON ch.headerid = cda.headerid AND cda.categoryid = ch.categoryid AND cda.templatetype = ch.templatetype 
				      JOIN attributenames an ON cda.attributeid = an.attributeid AND an.localeid = hn.localeid
				      JOIN productattribute pa ON ch.categoryid = pa.categoryid AND cda.attributeid = pa.attributeid AND pa.productid = '$etilizeid' AND pa.localeid = hn.localeid
				      WHERE ch.templatetype = 0 ORDER BY ch.displayorder, cda.displayorder";

	if(!$qokx1x1 = mysql_db_query("etilize", $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
	if(mysql_num_rows($qokx1x1) > 0)
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$query = "delete from product_specs where id = '$id'";
		if(!$qo8k = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		while( list($hname, $aname, $dv) = mysql_fetch_array($qok) )
		{
			$hname = addslashes($hname);
			$aname = addslashes($aname);
			$dv = addslashes($dv);

			if($hname == "Physical Characteristics" && $aname == "Weight (Approximate)")
			{
				$query = "update product_list set shipweight = '".floatval($dv)."' where id = '$id'";

				echo "1: ". $query."\n";

				if(!$q4ok = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
			}
		}
	}
	else
	{ // try the old way

		$query = "select categoryid from product where productid = '$etilizeid'";
		if(!$qok2212 = mysql_db_query("etilize", $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}


		if(mysql_num_rows($qok2212) < 1)
		{
			return;
		}
		else
		list($categoryid) = mysql_fetch_array($qok2212);

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$query = "delete from product_specs where id = '$id'";
		if(!$qok332 = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$query = "select ch.headerid, hn.name, ch.categoryid, cda.attributeid, an.name, pa.displayvalue
							from categoryheader ch, categorydisplayattributes cda,  headernames hn, attributenames an, productattribute pa 
						where ch.headerid=cda.headerid 
						and ch.categoryid=cda.categoryid 
						and ch.templatetype = cda.templatetype 
						and hn.headerid=cda.headerid 
						and an.attributeid=cda.attributeid 
						and pa.attributeid=cda.attributeid 
						and pa.localeid=an.localeid 
						and cda.templatetype = 1 
						and an.localeid=1 
						and hn.localeid=1 
						and cda.categoryid = $categoryid 
						and pa.productid = $etilizeid 
						order by ch.defaultdisplayorder, cda.defaultdisplayorder";
		if(!$q3ok = mysql_db_query("etilize", $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}


		while( list($hid, $hname, $catid, $aid, $aname, $dv) = mysql_fetch_array($q3ok) )
		{
			$hname = addslashes($hname);
			$aname = addslashes($aname);
			$dv = addslashes($dv);

			if($hname == "Physical Characteristics" && $aname == "Weight (Approximate)")
			{
				$query = "update product_list set shipweight = '".floatval($dv)."' where id = '$id'";

				echo "2: ".$query."\n";

				if(!$q4ok = mysql_db_query($DB, $query, $mysqlid)){echo "ERROR: ".mysql_error()."<BR><BR>$query";exit;}
			}


		}
	}

}


?>