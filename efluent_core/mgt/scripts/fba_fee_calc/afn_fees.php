<?php
include("json_functions.phtml");
ini_set('display_errors',1);
error_reporting(E_ALL);
//header("Content-Type: text/html");

//set POST variables
if (isset($_POST['model']) && !empty($_POST['model']))
{
	$model = $_POST['model'];
}
else
{
	die('{"error":null,"data":null}');
}	

$url = 'https://sellercentral.amazon.com/gp/fba/revenue-calculator/data/afn-fees.html';

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_REFERER, 'http://sellercentral.amazon.com/gp/fba/revenue-calculator/index.html/ref=xx_xx_cont_xx?ie=UTF8&lang=en_US');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, 'method=GET&model='.$model);

//execute post
$result = curl_exec($ch);
if(curl_exec($ch) === false)
{
	$error = 'Curl error: ' . curl_error($ch);
	die('{"error":'.$error.',"data":null}');
}
else
{
	$result_decoded = json_decode($result, true);
	if (is_null($result_decoded['error']) && is_null($result_decoded['data']))
	{	
		die('{"error":null,"data":null}');
	}
	elseif(is_object($result_decoded['data']))
	{
		echo $result;
	}
	else 
	{
		$error = 'Amazon error: '.$result_decoded['error'];
		die('{"error":'.$error.',"data":null}');
	}
}

//close connection
curl_close($ch);