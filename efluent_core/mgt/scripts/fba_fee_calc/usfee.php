<?php
include("json_functions.phtml");

class FBACalc {
	public $length = null;
	public $width = null;
	public $height = null;
	public $weight = null;
	public $Media = null;
	public $Category = null;
	public $SubCategory = null;
	public $IsWhiteGlovesRequired = null;
	public $Quarter = null;
	
	
	public $Currency = null;
	public $Shipping = null;
	public $Commissions = null;
	public $VariableClosingFees = null;
	public $Envelope = null;
	public $Oversize = null;
	public $OversizeCategory = null;
	public $HighASP = null;
	public $OrderHandling = null;
	
	function __construct($length,$width,$height,$weight,$category,$SubCategory,$IsWhiteGlovesRequired) {
		$this->length = $length;
		$this->width = $width;
		$this->height = $height;
		$this->weight = $weight;
		$this->Category = $category;
		$this->SubCategory = $SubCategory;
		$this->IsWhiteGlovesRequired = $IsWhiteGlovesRequired;
		$this->setCurrency();
		$this->setShipping();
		$this->setCommissions();
		$this->setVariableClosingFees();
		$this->setEnvelope();
		$this->setOversize();
		$this->setOversizeCategory();
		$this->setHighASP();
//		$this->setOrderHandling();
		return $this;
	}
	
	private function setCurrency() {
		$this->Currency = new stdClass();
		$this->Currency->DecimalPlaces = 2;
		$this->Currency->DecimalPoint = '.';
		$this->Currency->ThousandsSeparator = ',';
	}
	private function setShipping() {
		$this->Shipping = new stdClass();
		$this->Shipping->gl_book = 3.99;
		$this->Shipping->gl_music = 2.98;
		$this->Shipping->gl_video = 2.98;
		$this->Shipping->gl_dvd = 2.98;
	}
	private function setCommissions() {
		$this->Commissions = new stdClass();
		$this->Commissions->Other = 0.15;
		$this->Commissions->gl_apparel = 0.15;
		$this->Commissions->gl_automotive = 0.12;
		$this->Commissions->gl_baby_product = 0.15;
		$this->Commissions->gl_beauty = 0.15;
		$this->Commissions->gl_biss = 0.12;
		$this->Commissions->gl_book = 0.15;
		$this->Commissions->gl_camera = 0.08;
		$this->Commissions->gl_digital_accessories = 0.25;
		$this->Commissions->gl_digital_text = 0.15;
		$this->Commissions->gl_drugstore = 0.15;
		$this->Commissions->gl_dvd = 0.15;
		$this->Commissions->gl_electronics = 0.08;
		$this->Commissions->gl_game = 0.15;
		$this->Commissions->gl_gourmet = 0.15;
		$this->Commissions->gl_grocery = 0.15;
		$this->Commissions->gl_home = 0.15;
		$this->Commissions->gl_home_improvement = 0.12;
		$this->Commissions->gl_jewelry = 0.20;
		$this->Commissions->gl_kitchen = 0.15;
		$this->Commissions->gl_lawn_and_garden = 0.15;
		$this->Commissions->gl_mobile_electronics = 0.08;
		$this->Commissions->gl_music = 0.15;
		$this->Commissions->gl_musical_instruments = 0.12;
		$this->Commissions->gl_office_product = 0.15;
		$this->Commissions->gl_pc = 0.06;
		$this->Commissions->gl_personal_care_appliances = 0.15;
		$this->Commissions->gl_photo = 0.08;
		$this->Commissions->gl_shoes = 0.15;
		$this->Commissions->gl_software = 0.15;
		$this->Commissions->gl_sports = 0.15;
		$this->Commissions->gl_tires = 0.10;
		$this->Commissions->gl_toy = 0.15;
		$this->Commissions->gl_video = 0.15;
		$this->Commissions->gl_video_games = 0.15;
		$this->Commissions->gl_watch = 0.15;
		$this->Commissions->gl_wireless = 0.15;
	}
	
	private function setVariableClosingFees() {
		$this->VariableClosingFees = new stdClass();
		$this->VariableClosingFees->gl_book = 1.35;
		$this->VariableClosingFees->gl_software = 1.35;
		$this->VariableClosingFees->gl_video_games = 1.35;
		$this->VariableClosingFees->gl_music = 0.80;
		$this->VariableClosingFees->gl_dvd = 0.80;
		$this->VariableClosingFees->gl_video = 0.80;
	}
	private function setEnvelope() {
		$this->Envelope = new stdClass();
		$this->Envelope->Length = 15;
		$this->Envelope->Width = 12;
		$this->Envelope->Height = 0.75;
		$this->Envelope->Weight = 1;
	}
	private function setOversize() {
		$this->Oversize = new stdClass();
		$this->Oversize->Length = 18;
		$this->Oversize->Width = 14;
		$this->Oversize->Height = 8;
		$this->Oversize->Weight = 20;
	}
	private function setOversizeCategory() {
		$this->OversizeCategory = new stdClass();
		$this->OversizeCategory->SmallOversize = new stdClass();
		$this->OversizeCategory->SmallOversize->Length = 60;
		$this->OversizeCategory->SmallOversize->Width = 30;
		$this->OversizeCategory->SmallOversize->Area = 130;
		$this->OversizeCategory->SmallOversize->Weight = 70;
		
		$this->OversizeCategory->StandardOversize = new stdClass();
		$this->OversizeCategory->StandardOversize->Width = 108;
		$this->OversizeCategory->StandardOversize->Area = 130;
		$this->OversizeCategory->StandardOversize->Weight = 150;
		
		
		$this->OversizeCategory->LargeOversize = new stdClass();
		$this->OversizeCategory->LargeOversize->Length = 108;
		$this->OversizeCategory->LargeOversize->Area = 165;
		$this->OversizeCategory->LargeOversize->Weight = 150;
		
	}
	private function setHighASP() {
		$this->HighASP = new stdClass();
		$this->HighASP->ASP = 300;
		$this->HighASP->Fee = 0;
	}
	public function getDimensionalWeight($length, $width, $height, $weight) {
		$dimensionalWeight = $length * $width * $height;
        
        if ($dimensionalWeight > 5184) {
            $dimensionalWeight = $dimensionalWeight / 166;
            
            if ($dimensionalWeight > $weight) {
                $weight = $dimensionalWeight;
            }
        }
        
        return $weight;
	}
	
	public function getArea() {
		$dim = array($this->length,$this->width,$this->height);
		sort($dim);
		return (2 * ($dim[0] + $dim[1])) + $dim[2];
	}
	public function isMedia($feeType = '') {
		if ($feeType == '') {
			$feeType = 'FBAFees';
		}
		$media = array(
            'gl_book',
            'gl_music',
            'gl_video',
            'gl_dvd',
            'gl_video_games',
            'gl_software'
        );
        if ($this->Media!=null && isset($this->Media[$feeType])) {
        	$media = $this->Media[$feeType];
        }
        if (in_array($this->Category,$media)) {
        	return true;
        }
        else {
        	return false;
        }
	}
	public function isOversize($weight='') {
		if ($weight == '') {
			$weight = $this->weight;
		}
        $rateDims    = array($this->Oversize->Length,$this->Oversize->Width,$this->Oversize->Height);
		$dims        = array($this->length,$this->width,$this->height);
        
        sort($rateDims);
		sort($dims);

		if ($dims[0] > $rateDims[0] || $dims[1] > $rateDims[1] || $dims[2] > $rateDims[2]) {
			$oversizeDims = true;
		}
		else {
			$oversizeDims = false;
		}
		if ($oversizeDims || $weight > $this->Oversize->Weight) {
        	return true;
        } else {
            return false;
        }
	}
	function isEnvelope($weight = '') {
		if ($weight == '') {
			$weight = $this->weight;
		}
        $rateDims    = array($this->Envelope->Length,$this->Envelope->Width,$this->Envelope->Height);
		$dims        = array($this->length,$this->width,$this->height);
        		
        sort($rateDims);
		sort($dims);

		if ($dims[0] <= $rateDims[0] && $dims[1] <= $rateDims[1] && $dims[2] <= $rateDims[2]) {
			$envelopeDims = true;
		}
		else {
			$envelopeDims = false;
		}
		if ($envelopeDims && $weight <= $this->Envelope->Weight) {
        	return true;
        } else {
            return false;
        }
	}
	function isStandard($weight) {
		if (!$this->isEnvelope($weight) && !$this->isOversize($weight)) {
			return true;
		}
		else {
			return false;
		}
	}
	function isHighASP() {
		if ($this->getAFNSellingPrice()>=$this->HighASP->ASP && !$this->isOversize()) {
			return true;
		}
		return false;
	}
	function getOversizeCategory($weight = '') {
		if ($weight == '') {
			$weight = $this->weight;
		}
		if (!$this->isOversize($weight)) {
            return;
        }
        
        if ($this->IsWhiteGlovesRequired == 'Y') {
            return 'SpecialOversize';
        }

        $area        	= $this->getArea();
        $categories  	= $this->OversizeCategory;
        $category    	= 'SpecialOversize';
        $ovs_Cat_tmp = array("LargeOversize","StandardOversize","SmallOversize");
        foreach ($ovs_Cat_tmp as $x=>$y) {
	        foreach ($this->OversizeCategory as $k=>$v) {
	        	if ($k == $y) {
		        	$config      = $v;
		            $rateDims    = array($v->Length, $v->Width, $v->Height);
		            $dims        = array($this->length,$this->width,$this->height);
		            $withinArea  = $config->Area ? $area <= $config->Area : true;
		            $withinDims  = true;
		            
		            sort($rateDims);
		            sort($dims);
		  
		            foreach ($dims as $value) {
		            	$rateDim = $value;
		                $dim     = $value;
		                
		                if ($rateDim && $dim > $rateDim) {
		                    $withinDims = false;
		                    return false;
		                } 
		            }
		            if ($withinDims && $withinArea && $weight <= $config->Weight) {
		                $category = $k;
		            }
	        	}
	        }
        }
        return $category;
	}
	public function getFeeCategory($weight) {
		if ($this->isOversize($weight)) return "Oversize";
		else if ($this->isMedia($weight)) return "Media";
		else return "NonMedia";
	}
	public function getVariableClosingFee() {
		$cat = $this->Category;
		return $this->VariableClosingFees->$cat;
	}
	public function getOrderHandling() {
		$feeCategory = $this->getFeeCategory($this->weight);
		if ($feeCategory == 'NonMedia') {
			return 1;
		}
		else{
			return 0;
		}
	}
	public function getPickAndPack() {
		$feeCategory = $this->getFeeCategory($this->weight);
		$isEnvelope = $this->isEnvelope();
		$isOversize = $this->isOversize();
		if ($isEnvelope) {
			if ($feeCategory == 'Media' || $feeCategory == 'NonMedia') {
			 	return 1;
			} 
		}
		elseif(!$isOversize) {
			if ($feeCategory == 'Media' || $feeCategory == 'NonMedia') {
			 	return 1;
			} 
		}
		else {
			$oversizeCat = $this->getOversizeCategory();
			if ($oversizeCat == 'SmallOversize') {
				return 4;
			}
			elseif ($oversizeCat == 'StandardOversize') {
				return 5;
			}
			elseif ($oversizeCat == 'LargeOversize') {
				return 8;
			}
			elseif ($oversizeCat == 'SpecialOversize') {
				return 10;
			}
		}
	}
	public function getWeightHandling() {
        $category        = $this->getFeeCategory($this->weight);
        $weight          = $this->getDimensionalWeight($this->length,$this->width,$this->height,$this->weight);
        $packageWeight   = 0;
        $category_new        = $this->getFeeCategory($weight);
        $oversizeArr = $this->getWeightHandlingVals();
        
        if ($category_new == 'Media') {
        	$packageWeight +=0.125;
        }
        elseif ($category_new == 'NonMedia') {
        	$packageWeight +=0.250;
        }
        elseif ($category_new == 'Oversize') {
        	$packageWeight +=1;
        }
        
        
        if ($this->isOversize($weight)) {
            $oversizeCategory    = $this->getOversizeCategory($weight);
           
            $oversize = $oversizeArr[$oversizeCategory];
            $threshold           = $oversize['Threshold'];
            
            if ($oversizeCategory == 'SpecialOversize') {
                $weight = $this->weight;
            }
            
            $weight += $packageWeight;
            
            if ($weight <= $threshold) {
                return $oversize['UpToFlatFee'];
            } else {
                return (ceil($weight - $threshold)) * $oversize['MoreThanPerPound'] + $oversize['UpToFlatFee'];
            }
        } else {
            $weight += $packageWeight;
            if ($this->isEnvelope($weight)) {
                return $oversizeArr['Envelope'];
            } else {
                if ($weight <= 1) {
                    return $oversizeArr['Standard']['UpTo1'][$category];
                } else if ($weight <= 2) {
                    return $oversizeArr['Standard']['MoreThan1'][$category];
                } else {
                    return (ceil($weight - 2)) * $oversizeArr['Standard']['MoreThan2PerPound'][$category] + $oversizeArr['Standard']['MoreThan1'][$category];
                }
            }
        }
	}
	public function getWeightHandlingVals() {
		return array(
			"Envelope"=>0.42,
			"Standard"=>array(
				"UpTo1"=>array("Media"=>0.46,"NonMedia"=>0.46),
				"MoreThan1"=>array("Media"=>0.76,"NonMedia"=>0.99),
				"MoreThan2PerPound"=>array("Media"=>0.40,"NonMedia"=>0.38)
			),
			"SmallOversize"=>array(
				"Threshold"=>2,
				"UpToFlatFee"=>0.99,
				"MoreThanPerPound"=>0.38
			),
			"StandardOversize"=>array(
				"Threshold"=>2,
				"UpToFlatFee"=>1.49,
				"MoreThanPerPound"=>0.38
			),
			"LargeOversize"=>array(
				"Threshold"=>90,
				"UpToFlatFee"=>60,
				"MoreThanPerPound"=>0.76
			),
			"SpecialOversize"=>array(
				"Threshold"=>90,
				"UpToFlatFee"=>120,
				"MoreThanPerPound"=>0.90
			),
			"BoxWeight"=>array(
				"Media"=>0.125,
				"NonMedia"=>0.250,
				"Oversize"=>1
			),
			"Surcharge"=>array(
				"2300530"=>40,
				"2300543"=>40
			)
		);
	}
	
	public function getCategoryCommissions($category) {
		return $this->Commissions->$category;
	}
}
//ini_set('display_errors',1);
//error_reporting(E_ALL^E_NOTICE);

if (isset($_POST['nfo']) && !empty($_POST['nfo']))
{
	$nfo = json_decode($_POST['nfo'],true);
	if (is_array($nfo) && count($nfo) > 0) 
	{
		$fba = new FBACalc($nfo['length'],$nfo['width'],$nfo['height'],$nfo['weight'],$nfo['category'],$nfo['sub_category'],$nfo['is_white_gloves_required']);
		if (!$fba->isOversize() && $nfo['item_price']>=300) {
			$fee_result = array('pick_and_pack' => 0,'weight_handling' => 0,'order_handling' => 0,'category_percent' => $fba->getCategoryCommissions($nfo['category']));
		}
		else {
			$fee_result = array('pick_and_pack' => $fba->getPickAndPack(),'weight_handling' => $fba->getWeightHandling(),'order_handling' => $fba->getOrderHandling(),'category_percent' => $fba->getCategoryCommissions($nfo['category']));
		}
		echo json_encode($fee_result);
	}	
}
/*
		<table width="100%" id="fees">
		<tr>
			<td colspan="2" align="center"><b>Item fees (New)</b></td>
		</tr>
		<tr>
			<td>Pick and Pack:</td>
			<td>$<?php echo $fba->getPickAndPack(); ?></td>
		</tr>
		<tr>
			<td>Weight Handling:</td>
			<td>$<?php echo $fba->getWeightHandling(); ?></td>
		</tr>
		<tr>
			<td>Order Handling:</td>
			<td>$<?php echo $fba->getOrderHandling(); ?></td>
		</tr>
		<tr>
			<td>Category percent:</td>
			<td>%<?php echo $fba->getCategoryCommissions($nfo['category']); ?></td>
		</tr>
		</table>
*/