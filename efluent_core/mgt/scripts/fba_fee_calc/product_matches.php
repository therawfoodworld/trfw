<?php
include("json_functions.phtml");
ini_set('display_errors',1);
error_reporting(E_ALL);
//header("Content-Type: text/html");

//set POST variables
if (isset($_POST['model']) && !empty($_POST['model']))
{
	$model = json_decode($_POST['model'], true);
	if (empty($model['searchString']) || $model['searchString'] == 'Enter your product name, UPC, EAN, ISBN, or ASIN')
	{
		die('{"error":Error, ASIN number not given.,"data":null}');
	}
	else 
	{
		$asin = $model['searchString'];
	}
}
else 
{
	die('{"error":Error, ASIN number not given.,"data":null}');
}
$market_place = 'ATVPDKIKX0DER';
$url = 'http://sellercentral.amazon.com/gp/fba/revenue-calculator/data/product-matches.html';

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_REFERER, 'http://sellercentral.amazon.com/gp/fba/revenue-calculator/index.html/ref=xx_xx_cont_xx?ie=UTF8&lang=en_US');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, 'method=GET&model=%7B%22searchString%22%3A%22'.$asin.'%22%2C%22lang%22%3A%22en_US%22%2C%22marketPlace%22%3A%22'.$market_place.'%22%7D');

//execute post
$result = curl_exec($ch);
if(curl_exec($ch) === false)
{
	$error = 'Curl error: ' . curl_error($ch);
	die('{"error":'.$error.',"data":null}');
}
else
{
	$result_decoded = json_decode($result, true);
	if (is_null($result_decoded['error']) && is_null($result_decoded['data']))
	{	
		$error = 'Amazon error: please give a correct ASIN number.';
		die('{"error":'.$error.',"data":null}');
	}
	elseif(is_array($result_decoded['data']))
	{
		echo $result;
	}
	else 
	{
		$error = 'Amazon error: '.$result_decoded['error'];
		die('{"error":'.$error.',"data":null}');
	}
}

//close connection
curl_close($ch);