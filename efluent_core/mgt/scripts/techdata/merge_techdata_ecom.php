<?

ini_set("memory_limit","-1");

//error_reporting(0);

$ROOT_DIR = $argv[1];

if($ROOT_DIR == "") { 	echo "usage: merge_techdata.phtml ROOT_DIR\n"; 	exit; }

set_time_limit(999999);

//echo "$ROOT_DIR/include/mysql.phtml\n";

REQUIRE("$ROOT_DIR/config.phtml");
REQUIRE("$ROOT_DIR/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
//echo "DEBUG: $MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD  ROOT_DIR: $ROOT_DIR\n\n";

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE("$ROOT_DIR/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

$VENDORID = $efc->getConfigValue("techdata", "VENDORID");

function getmicrotime()
{
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

$xtime_start = getmicrotime();
$Ncreated = 0;
$Nupdated = 0;
$grp_count = 0;
$sub_count = 0;
$major_count = 0;




$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comgrp.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit; }
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$cat_grp[$data[0]] = addslashes($data[1]);
}
fclose($fp);



$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comcat.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit;}
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$cat_cat[$data[0].$data[1]] = addslashes($data[2]);

}
fclose($fp);




$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comsub.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit;}
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$cat_sub[$data[2]] = addslashes($data[3]);
}
fclose($fp);




//$cat_file = "products_cats.csv";
//$cat_file = "products_cats.cats";
//$cat_file = "products.csv";

$cat_file = "comgrp.csv";


echo "Importing GROUP Categories ...\n";
$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/$cat_file", "r");
if($fp == FALSE) {	echo "ERROR: Check file path.\n";	exit; }
while($data = fgetcsv($fp, null, ","))
{

	$cat_grp[$data[0]] = addslashes($data[1]);

	$catname = addslashes($data[1]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname' and grp = '$data[0]'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) <= 0)
	{
		$query = "insert into vendor_categories(vid, catname, parentid, grp) values('$VENDORID', '$catname', '0', '$data[0]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid     GRP:   $data[0]      CATNAME:   $catname\n";

	}
}
fclose($fp);

$cat_file = "comcat.csv";

echo "Importing MAJOR Categories ...\n";
$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/$cat_file", "r");
if($fp == FALSE) {	echo "ERROR: Check file path.\n";	exit; }
while($data = fgetcsv($fp, null, ","))
{
	$cat_cat[$data[0].$data[1]] = addslashes($data[2]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = ''";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($grpid) = mysql_fetch_array($qok);
	if($grpid == "") { die("GRP!"); }


	$catname = addslashes($data[2]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) == 0) {

		$query = "insert into vendor_categories(vid, catname, parentid, grp, code) values('$VENDORID', '$catname', '$grpid', '$data[0]', '$data[1]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid   GRP: $data[0] CODE: $data[1]   CATNAME: $catname\n";

	}
}
fclose($fp);


$cat_file = "comsub.csv";


echo "Importing SUB Categories ...\n";
$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/$cat_file", "r");
if($fp == FALSE) {	echo "ERROR: Check file path.\n";	exit; }
while($data = fgetcsv($fp, null, ","))
{

	$cat_sub[$data[2]] = addslashes($data[3]);
	$catname = addslashes($data[3]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and code2 = ''";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($grpid) = mysql_fetch_array($qok);
	if($grpid == "") { die("GRP!"); }

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and code2 = '$data[2]' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) == 0) {

		$query = "insert into vendor_categories(vid, catname, parentid, grp, code, code2) values('$VENDORID', '$catname', '$grpid', '$data[0]', '$data[1]', '$data[2]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid   GRP: $data[0] CODE: $data[1]  CODE2: $data[2]  CATNAME: $catname\n";


	}
}
fclose($fp);












$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/manuf.csv", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}
echo "Importing Brands...\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$code = addslashes($data[0]);
	$manname = addslashes($data[1]);
	$manname = trim($manname);
	$code = trim($code);

	$brands[$code] = $manname;


	//$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '$code' and manname = '$manname'";
	$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and manname = '$manname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) == 0)
	{
		echo " * * *   manname: '$manname'   code: '$code'\n";  // NONE ARE BLANK....

		$query = "insert into vendor_manufacturers(manname, vid, code) values('$manname', '$VENDORID', '$code')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$manid = mysql_insert_id();
	}
}



/*
echo "[+] $grp_catname\n";
echo "  [+] $cat_catname\n";
echo "    [+] $sub_catname\n";
echo "\n\n\n";

$category_map_array[$grp_catname][$cat_catname][$sub_catname] =  '...';
print_r($category_map_array);
*/

/*

//MAN CODE, MAN NAME from manf.csv + bugfix

$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/manuf.csv", "r");
if($fp == FALSE)
{
echo "ERROR OPENING FILE\n";
exit;
}
echo "Importing Brands...\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
$code = addslashes($data[0]);
$manname = addslashes($data[1]);
$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '$code'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) == 0)
{
echo "manname: '$manname'   code: '$code'\n";  // NONE ARE BLANK....

$query = "insert into vendor_manufacturers(manname, vid, code) values('$manname', '$VENDORID', '$code')";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
$manid = mysql_insert_id();
}
}

*/




/*
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
if (!$data[0] == "VEND_CODE,C,6")
{
//print_r($data);
$code = addslashes($data[0]);
$manname = addslashes($data[1]);
if( $data[1] == "") { echo "\n\nMANNAME IS BLANK\n\n"; $manname = "$code"; }
echo "$code / $manname\n";
$query = "select manname, code from vendor_manufacturers where vid = '$VENDORID' and code = '$code'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($mmm, $ccc) = mysql_fetch_array($qok);
if ($mmm == "") {
$mmm = "$code";
$query = "update vendor_manufacturers SET manname='$code' where vid='$VENDORID'and code = '$code'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
echo "corrected------> $ccc / $mmm\n";
echo "$mannameaa<---FIXED WAS BLANK.\n";
} else {
echo "$manname<---perfect\n";
}
}
}
*/


/*
[#263304] manname: 'SYMANTEC UNIX LICENSE'  code: 'SYMUXE'  id: '46203'
60440L UPDATED (STK: 0 / CST: 1986.63 / MAP: 0)
*/



//PRODUCTS...

// 0          1       2          3                    4              5           6           7            8              9             10                      11         12          13          14
//GRP,C,2	CAT,C,2	 SUB,C,2	VEND_CODE,C,6	MANU_PART,C,20	PART_NUM,C,15	DESCR,C,50	COST,N,10,2	RETAIL,N,10,2	QTY,N,6,0	LIST_PRICE,N,10,2	EFF_DATE,C,11	TECH_FAX,L	STATUS,C,1	UPC,C,15


$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/products.csv", "r");
if($fp == FALSE) {
	echo "ERROR: Check file path.\n";
	exit;
}

$query = "delete from vendor_manufacturers where vid = '$VENDORID' and manname = ''";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


echo "Importing Products...\n";
while($data = fgetcsv($fp, null, ","))
{
	
	//RESUME SWITCH

	$continue = 'yes';
	if($data[5] == "60440L") { $continue = 'yes'; }
	if($continue == "yes")
	{
		if ($data[0] == "GRP,C,2") {
			//echo "FeedMap: Drop it like its hottttt...\n";	//
		} else {
			$code = $data[0];
			$code2 = $data[1];
			$code3 = $data[2];

			//$shipweight = $data[14];
			$instock = $data[9];

			$manpart = $data[4];
			//if ($manpart == "WR-100") {
			//$sku = $data[4];
			//$sku = addslashes($sku);
			//$manpart = addslashes($manpart);
			//} else {
			$manpart = addslashes($manpart);
			$sku = $data[5];
			$sku = addslashes($sku);
			//}
			$upccode = $data[14];

			$mancode = $data[3];
			// add manname lookup by mancode.

			$descr = $data[6];
			$descr = addslashes($descr);
			//$salesdescr  = addslashes($data[16]);
			$map = 0.00;
			$cost = $data[7];
			$price = 0.00;
			$shipweight = 0;


			$mancode = trim($mancode);

			$manname = $brands[$mancode];

			$query = "select manid,manname from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($manid,$manname) = mysql_fetch_array($qok);

			//if($manid == "")
			if($manid !="")
			{
				$knt++;
				echo "[#$knt] manname: '$manname'  code: '$mancode'  id: '$manid'\n";

				/*
				//very sad. I die now from broken heart.
				//plan B, self correct?

				$manname = $mancode;
				echo "\n[#$knt] CHANGED=> manname: '$manname'  FROM code: '$mancode'\n";

				$query = "select manid,manname from vendor_manufacturers where code = '$mancode' and vid = '$VENDORID'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(!mysql_num_rows($qok) == 0)
				{
				$query = "insert into vendor_manufacturers(manname, vid, code) values('$manname', '$VENDORID', '$mancode')";
				//if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$manid = mysql_insert_id();

				echo "[#$knt] CORRECTED=> SKU $sku  BRAND '$manname' CODE '$mancode'\n\n";
				}
				}*/



				// We have a Man ID, import the product if no dupes.
				$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok) > 0)
				{

					list($pid) = mysql_fetch_array($qok);




					//echo "Dupe Detected... $manpart\n";






					// Add Price, Stock.
					$query = "delete from vendor_pricing where pid = '$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0.00', '$cost', '0.00')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					if ($sku == "") {
						// Sku is blank?
					}
					else {
						echo "$sku UPDATED (STK: $instock / CST: $cost / MAP: $map)\n";
						$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
						if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

						//$efc->vendorMarkPrice($pid);
						$efc->updateVendorSourcing($pid, $VENDORID);
						$Nupdated++;
					}



				}	else

				{
					// Insert Product.
					$added_dte = time();


					$Ncreated++;
					$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku, upccode, shipweight) ";
					$query .= "values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku', '$upccode', '$shipweight')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$pid = mysql_insert_id();




					$Ncreated++;




					//echo "($Ncreated)..  INSERTED $manpart    mancode: '$mancode'\n";




					$query = "delete from vendor_pricing where pid = '$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0.00', '$cost', '0.00')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					//Insert long descr, AKA sales descr.
					if($salesdescr != "")
					{
						$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
						if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

						$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
						if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					}

					// CATEGORY MAP ..once upon a mid-day dreary as I coded weak and weary.
					//do get cate
					$query = "select catid, code from vendor_categories where vid = '$VENDORID' and code = '$code2' and code2 = '$code3' and grp = '$data[0]'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($catid, $codex) = mysql_fetch_array($qok);
					// Insert Cate'
					$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catid', '$VENDORID', '$codex')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					//do get sub
					$query = "select catid, code from vendor_categories where vid = '$VENDORID' and parentid = '$catid' and code = '$code2'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					list($catidzz, $codexx) = mysql_fetch_array($qok);

					$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catidzz', '$VENDORID', '$codexx')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					//do get group
					$query = "select parentid, code from vendor_categories where vid = '$VENDORID' and code = '$code2' and code2 = '$code3'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					list($parentid, $whyme) = mysql_fetch_array($qok);
					//echo "$code / $code2 / $code3  ($whyme)--- insert into vendor_category_map(pid, catid, vid) values('$pid', '$parentid', '$VENDORID')\n";

					$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$parentid', '$VENDORID')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


					$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$code' and parentid = '0'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($bugfixcode) = mysql_fetch_array($qok);

					$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$bugfixcode', '$VENDORID')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				}


				////////////////////

			} else
			{
				$knt2++;
				echo "***[#$knt2]*** manname: '$manname'  code: '$mancode'  id: '$manid'\n";
			}


			////////////////////


		}

	}
}
fclose($fp);



$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/prodaux.csv", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}
echo "Syncing Ship Weight...\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$theSKU = $data[0];
	$theLBS = str_replace(" lbs","",$data[1]);

	if($theLBS > 65.00)
	{

		$filter++;

		$query = "delete from vendor_product_list where vid = '$VENDORID' and sku = '$theSKU'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		echo "($filter) FILTER:    sku: $theSKU   lbs: $theLBS\n";

	}
	else
	{

		$lbs_count++;

		$query = "update vendor_product_list SET shipweight='$theLBS' where vid='$VENDORID' and sku = '$theSKU';";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		//echo "($lbs_count) SKU:  $theSKU    LBS:  $theLBS\n";
	}
}

fclose($fp);





$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
$msg = "Completed in $qtime Updated($Nupdated) Created($Ncreated)";
echo $msg . "\n";
$efc->logAction("SUCCESS", 4, 0, 'TechData Sync Successful', $msg);

//VENDOR SYNC WIDGET
$last_modified = filemtime("$ROOT_DIR/mgt/scripts/techdata/products.csv");


$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	//echo "vendor did not exist, so inerted them ti log\n";
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else {
	//echo "vendor DID exist, so update log\n";
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}

$query = "delete from vendor_manufacturers where vid = '$VENDORID' and manname = ''";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


$efc->cleanVendorItems($VENDORID);


?>

<?mysql_close();?>
