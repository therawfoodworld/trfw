<?
ini_set("memory_limit","-1");

if(file_exists("products_cats.csv")) { unlink("products_cats.csv"); }
if(file_exists("products_cats.parse")) { unlink("products_cats.parse"); }
if(file_exists("products_cats.cats")) { unlink("products_cats.cats"); }


$fh = fopen("products_cats.cats", 'a') or die("can't open file");

echo "Working ...\n";

$fp = fopen("products.csv", "r");
if($fp == FALSE) {	echo "ERROR: Check file path.\n";	exit; }
while($data = fgetcsv($fp, null, ","))
{
    $sku = $data[4];
	$grp_code = $data[0];
	$cat_code = $data[1];
	$sub_code = $data[2];
	
	$line = "$grp_code,$cat_code,$sub_code\n";

	fwrite($fh, $line);
	
}

fclose($fh);

$cmd = "cat products_cats.cats | sort | uniq > products_cats.csv";
passthru($cmd);


?>