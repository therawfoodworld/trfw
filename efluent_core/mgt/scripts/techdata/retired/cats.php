<?

ini_set("memory_limit","-1");

error_reporting(0);

$ROOT_DIR = $argv[1];

if($ROOT_DIR == "") { 	echo "usage: merge_techdata.phtml ROOT_DIR\n"; 	exit; }

set_time_limit(999999);

//echo "$ROOT_DIR/include/mysql.phtml\n";

REQUIRE("$ROOT_DIR/config.phtml");
REQUIRE("$ROOT_DIR/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
//echo "DEBUG: $MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD  ROOT_DIR: $ROOT_DIR\n\n";

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE("$ROOT_DIR/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

$VENDORID = $efc->getConfigValue("techdata", "VENDORID");

function getmicrotime()
{
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

$xtime_start = getmicrotime();
$Ncreated = 0;
$Nupdated = 0;
$grp_count = 0;
$sub_count = 0;
$major_count = 0;




$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comgrp.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit; }
echo "Groups...\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	$cat_grp[$data[0]] = addslashes($data[1]);


	$catname = addslashes($data[1]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname' and grp = '$data[0]'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) <= 0)
	{
		$query = "insert into vendor_categories(vid, catname, parentid, grp) values('$VENDORID', '$catname', '0', '$data[0]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid     GRP:   $data[0]      CATNAME:   $catname\n";

	}
}

fclose($fp);



$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comcat.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit;}
echo "Comcat....\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	//"GRP,C,2","CAT,C,2","DESCR,C,50"

	$cat_cat[$data[0].$data[1]] = addslashes($data[2]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = ''";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($grpid) = mysql_fetch_array($qok);
	if($grpid == "") { die("GRP!"); }


	$catname = addslashes($data[2]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) == 0) {

		$query = "insert into vendor_categories(vid, catname, parentid, grp, code) values('$VENDORID', '$catname', '$grpid', '$data[0]', '$data[1]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid   GRP: $data[0] CODE: $data[1]   CATNAME: $catname\n";

	}
}
fclose($fp);




$fp = fopen("$ROOT_DIR/mgt/scripts/techdata/comsub.csv", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit;}
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{//"GRP,C,2","CAT,C,2","SUB,C,2","DESCR,C,50"
	$cat_sub[$data[2]] = addslashes($data[3]);
	$catname = addslashes($data[3]);

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and code2 = ''";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($grpid) = mysql_fetch_array($qok);
	if($grpid == "") { die("GRP!"); }

	$query = "select catid from vendor_categories where vid = '$VENDORID' and grp = '$data[0]' and code = '$data[1]' and code2 = '$data[2]' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) == 0) {

		$query = "insert into vendor_categories(vid, catname, parentid, grp, code, code2) values('$VENDORID', '$catname', '$grpid', '$data[0]', '$data[1]', '$data[2]')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
		echo "CATID: $parentid   GRP: $data[0] CODE: $data[1]  CODE2: $data[2]  CATNAME: $catname\n";


	}
}
fclose($fp);




?>