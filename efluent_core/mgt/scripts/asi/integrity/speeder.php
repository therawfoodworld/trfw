<?
// Turn off all error reporting
error_reporting(0);
ini_set("memory_limit","20M");
$ROOT_DIR = $argv[1];


if($ROOT_DIR == "")
{
	echo "usage: image_script ROOT_DIR\n";
	exit;
}
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

echo "DB: $DB - $MYSQL_DATABASE\n";

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

echo "Importing Images to Database ...\n.";

$basedir = "$ROOT_DIR/mgt/scripts/asi/images";

$d = dir($basedir);

$kount = 0;
$have = 0;
$inserted = 0;


while (false !== ($entry = $d->read()))
{
	if($entry != "." && $entry != "..")
	{
		$sku = substr($entry, 0, strrpos($entry, "."));

		//echo "SKU: $sku\n";

		$query = "select id from vendor_product_list where sku = '$sku'";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if(mysql_num_rows($qok) > 0)
		{
			list($pid) = mysql_fetch_array($qok);

			$filename = "$basedir/$entry";

			$imageinfo = getimagesize($filename);
			$width = $imageinfo[0];
			$height = $imageinfo[1];

			$fp = fopen($filename, "r");
			if($fp == false){echo "ERROR OPENING FILE\n";return;}
			$data = "";
			while(!feof($fp))
			{
				$data .= fgets($fp, 4096);
			}
			fclose($fp);
			$data = addslashes($data);

			$kount++;
			echo "SPEED! PID: $pid [$sku] $filename [$width x $height] ($kount)\n";
			$query = "delete from vendor_product_images where id = '$pid'";
            if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_product_images(id, idata, itype, thumb, width, height) values('$pid', '$data', '1', '0', '$width', '$height')";
			if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			//unlink($filename);
		}
	}
}
$d->close();

echo "Completed,  Had: $have Inserted: $inserted Total: $kount\n";

?>