<?
ini_set("memory_limit","-1");

$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");


$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

echo "\$MYSQL_SERVER = $MYSQL_SERVER\n";

$VENDORID = $efc->getConfigValue("ASI", "VENDORID");
$vid =  $efc->getConfigValue("ASI", "VENDORID");

include($ROOT_DIR."/mgt/scripts/vid_date.php");


$feed_file = $argv[2];

$userid = $efc->getConfigValue("ASI", "USERID");
if(!is_numeric($userid)) { die("NEED USERID IN SITE CONFIG....."); }


if($DB == "ecom") { $feed_file = "$userid.csv"; }
if($DB == "supplysale") { $feed_file = "$userid.csv"; }
if($DB == "supplyace") { $feed_file = "$userid.csv"; }

if($feed_file == "") { die("PLEASE TELL ME WHAT FEED FILE!\n"); }


echo "VENDORID: $VENDORID\n";
function getmicrotime() {	list($usec, $sec) = explode(" ",microtime()); 	return ((float)$usec + (float)$sec); }
$xtime_start = getmicrotime();
$Ncreated = 0;  $Nupdated = 0;


if(file_exists("UPC_code.xls"))
{
	$cmd = "rm -rf UPC_code.csv";
	passthru($cmd);
	echo "Converting UPC XLS file to CSV ...\n";
	$cmd = "/usr/local/bin/xls2csv UPC_code.xls > UPC_code.csv";
	passthru($cmd);
	$cmd = "rm -rf UPC_code.xls";
	passthru($cmd);
}



/*if(file_exists("CategoryIDsAndNames.xls"))
{*/
if(file_exists("CategoryAndSubCategory.xls"))
{
	$cmd = "rm -rf CategoryIDsAndNames.csv";
	//passthru($cmd);
	$cmd = "rm -rf CategoryAndSubCategory.csv";
	passthru($cmd);

	echo "Converting XLS files to CSV ...\n";
	$cmd = "/usr/local/bin/xls2csv CategoryIDsAndNames.xls > CategoryIDsAndNames.csv";
	//passthru($cmd);
	$cmd = "/usr/local/bin/xls2csv CategoryAndSubCategory.xls > CategoryAndSubCategory.csv";
	passthru($cmd);
	$cmd = "rm -rf Category*.xls";
	passthru($cmd);
}
//}

$fp = fopen("$ROOT_DIR/mgt/scripts/asi/UPC_code.csv", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Building UPC Array ... ";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{ // "SKU","UPC"
	$sku = $data[0];
	$upc = $data[1];

	if($sku !="SKU") { $upc_code[$sku] = $upc; }
}
echo "OK.\n";


$fp = fopen("$ROOT_DIR/mgt/scripts/asi/CategoryAndSubCategory.csv", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Building Category Array ... ";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{ //MainCatCode","SubCatCode","MainCatName","SubCatName"

	if($data[3] !="") { $happy = 1; }
	if($happy == "1")
	{
		if($data[0] !="")
		{
			$cats_codes[$data[0]] = addslashes($data[2]);
			$cats_codes[$data[1]] = addslashes($data[3]);
			
			echo "$data[0] $data[1] $data[2] $data[3]\n";

		}
	}

}
unset($cats_codes["MainCatCode"]);
unset($cats_codes["SubCatCode"]);
echo "OK.\n";
print_r($cats_codes);
fclose($fp);


$fp = fopen("$ROOT_DIR/mgt/scripts/asi/CategoryAndSubCategory.csv", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Importing TOP Categories ...\n";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{ //MainCatCode","SubCatCode","MainCatName","SubCatName"

	$catname = addslashes($data[2]);

	if($catname !="MainCatName")
	{

		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if(mysql_num_rows($qok) <= 0)
		{
			$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$catname', '0')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			echo "TOP Category: $catname\n";
		}
	}
}
fclose($fp);





$fp = fopen("$ROOT_DIR/mgt/scripts/asi/CategoryAndSubCategory.csv", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Importing SUB Categories ...\n";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{ //MainCatCode","SubCatCode","MainCatName","SubCatName"

	$catname = addslashes($data[2]);
	$catSub = addslashes($data[3]);

	if($catSub !="SubCatName")
	{

		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catSub'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) <= 0)
		{
			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($parentid) = mysql_fetch_array($qok);

			if(intval($parentid) <= 0)
			{
				echo "error finding parent=> $catname\n";
				exit;
			}

			$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$catSub', '$parentid')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			echo "SUB Category: $catSub\n";
		}
	}
}
fclose($fp);


$fp = fopen("$ROOT_DIR/mgt/scripts/asi/$feed_file", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Importing Brands...\n";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{
	$manname = addslashes($data[3]);
	if($manname !="VENDOR")
	{
		if(!preg_match("/\!/",$manname))
		{

			$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) > 0) { 	list($manid) = mysql_fetch_array($qok);	}
			else
			{
				$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$manid = mysql_insert_id();
				echo "MANID: $manid   BRAND: $manname\n";
			}
		}
	}
}
fclose($fp);




$fp = fopen("$ROOT_DIR/mgt/scripts/asi/$feed_file", "r");
if($fp == FALSE){echo "ERROR OPENING FILE\n"; exit; }
echo "Importing Products...\n";
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{

	/*	print_r($data);
	if($iii > 1)  { exit; } else { $iii++; }*/

	/*
	[0] => SKU
	[1] => ITEMID
	[2] => DESCRIPTION
	[3] => VENDOR
	[4] => CAT
	[5] => PRICE
	[6] => Weight
	[9] => SUB-CATEGORY

	STOCK ...
	[11] => FR
	[12] => LA
	[13] => AT
	[14] => CH
	[15] => DA
	[16] => HO
	[17] => KA
	[18] => NJ
	[19] => TA
	[20] => MI
	[21] => PO
	[22] => MH
	*/


	$instock = $data[11] +	$data[12] + $data[13] +	$data[14] + $data[15];
	$instock = $instock + $data[16] + $data[17] + $data[18] + $data[19];
	$instock = $instock + $data[20] +	$data[21] +	$data[22];
	$instock = addslashes($instock);

	$cost = addslashes($data[5]);

	$sku = addslashes($data[0]);
	$manpart = addslashes($data[1]);
	$manname = addslashes($data[3]);

	$descr = addslashes($data[2]);

	$catname = $cats_codes[$data[4]];
	$subcat = $cats_codes[$data[9]];
	
	echo "\n **CATE**  $catname  ... $subcat\n";

	$upccode = $upc_code[$sku];


	if($manname !="VENDOR")
	{
		if(!preg_match("/\!/",$manname))
		{
			$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) > 0) { list($manid) = mysql_fetch_array($qok); }
			else { die("MANNAME: $manname  ID: ???\n"); }
		}
	}


	if($sku !="SKU")
	{

		$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku' and manpart = '$manpart'";
//		echo $query."\n";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0)
		{ // update
			list($pid) = mysql_fetch_array($qok);

			$instock = $data[11] +	$data[12] + $data[13] +	$data[14] + $data[15];
			$instock = $instock + $data[16] + $data[17] + $data[18] + $data[19];
			$instock = $instock + $data[20] +	$data[21] +	$data[22];
			$instock = addslashes($instock);

			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0', '$cost', '0')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$efc->updateVendorSourcing($pid, $VENDORID);
			$Nupdated++;
			echo "UPDATE: pid: $pid sku: $sku\n";

		}
		else
		{ //new

			$instock = $data[11] +	$data[12] + $data[13] +	$data[14] + $data[15];
			$instock = $instock + $data[16] + $data[17] + $data[18] + $data[19];
			$instock = $instock + $data[20] +	$data[21] +	$data[22];
			$instock = addslashes($instock);


			$Ncreated++;
			$added_dte = time();
			$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku, upccode) values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku', '$upccode')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$pid = mysql_insert_id();


			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0.00', '$cost', '0.00')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }



			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			$top_catid = $catid;
			$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$subcat'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			echo "ADDED: ('$sku' / '$manpart') $catname / $subcat QTY: $instock  CST: $cost\n";
		}
	}
} // EOF
fclose($fp);



$efc->cleanVendorItems($VENDORID);

$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
$msg = "Completed in $qtime Updated($Nupdated) Created($Ncreated)";
echo $msg . "\n";
$efc->logAction("SUCCESS", 4, 0, 'ASI Sync Succesful', $msg);




//VENDOR SYNC WIDGET

$last_modified = filemtime("$ROOT_DIR/mgt/scripts/asi/CategoryAndSubCategory.csv");

$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else {
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}
$query = "delete from vendor_product_list where sku = 'ITEMNO'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


?>
<?mysql_close();?>