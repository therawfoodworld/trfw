<?
error_reporting(E_ALL);
ini_set("memory_limit","20M");
$ROOT_DIR = $argv[1];

if($ROOT_DIR == "") { echo "usage: image_script ROOT_DIR\n";	exit; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

echo "DB: $DB - $MYSQL_DATABASE\n";

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$userid = $efc->getConfigValue("ASI", "USERID");
if(!is_numeric($userid)) { die("NEED USERID FOR CLEANER SCRIPT IN SITE CONFIG....."); }


$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_import.php $ROOT_DIR $userid.csv";
passthru($cmd);

$cmd = "php $ROOT_DIR/mgt/scripts/asi/product_specs.php $ROOT_DIR ProductSpec.CSV";
passthru($cmd);

if($DB == "ecom")
{
	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_crawler.php $ROOT_DIR";
	passthru($cmd);

	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_data_parser.php $ROOT_DIR ProductSpec.CSV";
	//passthru($cmd);

	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_data_parser2.php $ROOT_DIR ProductSpec.CSV";
	//passthru($cmd);
}

$zip = 'NewImages.zip';
$cmd = "unzip -o -d ./images/ $zip";
passthru($cmd);


$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_cleaner.php $ROOT_DIR";
//passthru($cmd);

$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_images.php $ROOT_DIR";
passthru($cmd);


?>
<?mysql_close();?>