<?
/*
ftp://asispec:asi@67.104.19.206/ReadMe.doc
This file contains the product specifications for all the products from ASI. You can access it by going to ftp://67.104.19.206/ProductSpec.CSV. This file is updated weekly. Its similar to ProductSpec.xml.
Element Name,SKU,Category,Sub-Category,MPN,Vendor,Description,MarketingInfo,KeyFeatures
Example SKU 52330
52330	Storage 	Hard Drive	ST373455FC	Seagate	Seagate HD ST373455FC 73GB Ultra320 SCSI 15000rmp 16MB 1607Mbits Bulk	The Cheetah 15K.5 is mainstream enterprise disc drive storage. It is the best price/performance&cedil; lowest cost of ownership&cedil; 3.5-inch disc
ftp://67.104.19.206/ProductSpec.CSV
*/

$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { exit("usage: $argv[0] ROOT_DIR\n"); }

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

echo "DB: $DB\n";

$VENDORID = $efc->getConfigValue("ASI", "VENDORID");
$vid = $efc->getConfigValue("ASI", "VENDORID");

$ftp_server = $efc->getConfigValue("ASI", "FTP_SERVER");
$ftp_user_name = $efc->getConfigValue("ASI", "FTP_USER");
$ftp_user_pass = $efc->getConfigValue("ASI", "FTP_PASS");


$userid = $efc->getConfigValue("ASI", "USERID");
if(!is_numeric($userid)) { die("NEED USERID FOR CLEANER SCRIPT IN SITE CONFIG....."); }


/*
a) CategoryAndSubCategory.xls  This file contains the various category and subcategory codes and their descriptions. Please download this once a month.
b) CategoryIDsAndNames.xls  This file contains id and name of product category. This file needs to be downloaded only once.
c) DiscontinuedSKU.csv  This file contains the list of products that were discontinued by ASI. It contains only the sku number. Please download this once a week.
d) NewProductSKU.csv  This file contains the list of products that were added to ASI catalog recently. It contains only the sku number. Please download this once a week.

-->UPC_code.xls
--> NewImages.zip

DO THE DO -> //a) The files images1.zip, images2.zip, images3.zip, images4.zip, images5.zip, images6.zip, images7.zip, images8.zip, images9.zip, images10.zip, images11.zip and images12.zip
*/




//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/$userid.csv";  // GET OFF WEBSITE....  SVN IT....  GOOD TO GO.....

//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/DiscontinuedSKU.CSV";
//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/NewProductSpec.csv";
//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/NewProductSKU.CSV";





//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/UPC_code.xls";

//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/CategoryAndSubCategory.xls";

//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/ProductSpec.CSV";





//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/CategoryIDsAndNames.xls"; // DO NOT NEED


//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/NewImages.zip";
//$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images01.zip";

$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images02.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images03.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images04.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images05.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images06.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images07.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images08.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images09.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images10.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images11.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images12.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images13.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images14.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images15.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images16.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images17.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images18.zip";
$local_file[] = $ROOT_DIR."/mgt/scripts/asi/images19.zip";

foreach($local_file as $key => $remote_file)
{
	echo "Local: $remote_file";
	$remote_file = str_replace("$ROOT_DIR/mgt/scripts/asi","",$remote_file);
	echo "	Remote: $remote_file\n";


	if(file_exists($local_file[$key])) { unlink($local_file[$key]); }
	$location = "ftp://$ftp_user_name:$ftp_user_pass@$ftp_server/$remote_file";
	echo "  %=> $location\n";
	passthru("curl '$location' -o $local_file[$key]");
	if(!file_exists($local_file[$key]))
	{
		echo "Error: problem downloading feed file! ($remote_file)\n";
		$bad = 1;
		//exit;
	}
	else
	{
		echo "DOWNLOAD FILE SUCCESS: $remote_file => $local_file[$key]\n\n\n";
	}
}

// DEAL WITH IMAGE ZIPS .... FOR FIRST RUN....
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images01.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images02.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images03.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images04.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images05.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images06.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images07.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images08.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images09.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images10.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images11.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images12.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images13.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images14.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images15.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images16.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images17.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images18.zip";
$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/images19.zip";

//$ilocal_file[] = $ROOT_DIR."/mgt/scripts/asi/NewImages.zip";

foreach($ilocal_file as $zip)
{
	$cmd = "unzip -o -d ./images/ $zip";
	passthru($cmd);
}




// FIRST SETUP ....   Crontab routine will be different ...
exit;


$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_import.php $ROOT_DIR $userid.csv";
passthru($cmd);


$cmd = "php $ROOT_DIR/mgt/scripts/asi/product_specs.php $ROOT_DIR ProductSpec.CSV";
passthru($cmd);

if($DB == "ecom")
{

	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_crawler.php $ROOT_DIR";
	passthru($cmd);

	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_data_parser.php $ROOT_DIR ProductSpec.CSV";
	passthru($cmd);

	$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_data_parser2.php $ROOT_DIR ProductSpec.CSV";
	passthru($cmd);
}

$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_cleaner.php $ROOT_DIR";
passthru($cmd);

$cmd = "php $ROOT_DIR/mgt/scripts/asi/asi_images.php $ROOT_DIR";
passthru($cmd);


?>
<?mysql_close();?>