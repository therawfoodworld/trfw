<?

ini_set("memory_limit","-1");

$ROOT_DIR = "/home/httpd/ecomelectronics.com";

if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");


$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);


$VENDORID = $efc->getConfigValue("DOBA", "VENDORID");
$vid = $VENDORID;


$fname = 'doba_inventory_update.csv';

echo "FILE: $fname\n";

////  Core Monitor ///////////////////////////////////////////////////////////////////////////////////////////////////////
$time = addslashes(date("Y-m-d H:i:s"));
$data = 'CSV Sync';
$query = "delete from doba_monitor_timestamp where data = '$data'";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
$query = "insert into doba_monitor_timestamp(data,cur_timestamp) values('$data','$time')";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





$fp = fopen("$ROOT_DIR/mgt/scripts/DOBA/$fname", "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }

while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

	$masscount++;

	//print_r($data);
	//item_id,ship_cost,price,prepay_price,qty_avail,stock
	// 0          1       2        3           4      5

	$instock = $data[4];
	$item_id = $data[0];


	$query = "select id from vendor_product_list where vid = '$VENDORID' and manpart = '$item_id'";
	if(!$qok1 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok1) == 0)
	{
		$skip++;
		//echo "..SKIP ($skip)  $item_id\n";


		$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$item_id'";
		if(!$qok1 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok1) == 0)
		{
			$skip++;
			//echo "..SKIP ($skip)  $item_id\n";

		}
		else
		{
			list($pid) = mysql_fetch_array($qok1);

			if(is_numeric($pid))
			{
				$query = "update vendor_pricing set instock='$instock' where pid = '$pid'";
				echo "using SKU column  $item_id ".$query."\n";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$uc++;

				//$efc->updateVendorSourcing($pid, $VENDORID);

				$query = "select pid from product_sourcing where vpid = '$pid' and distid = '$VENDORID'";
				if(!$qok111 = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok111) == 0)
				{ // dont exist. not mapped.
					$no++;
					//echo "NOT MAPPED: $vpid\n";
				}
				else
				{

					list($sku_pid) = mysql_fetch_array($qok111);

					$query = "update product_sourcing set qty='$instock' where pid = '$sku_pid' and vpid = '$pid' and distid = '$VENDORID'";
					if(!$qok222 = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					if($instock > 9)
					{
						$markedvis++;
						$query = "update product_list set visible = 1 where id = '$sku_pid'";
						if(!$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
						echo "(#$markedvis)  PID: $pid  SKU: $sku_pid ITEM_ID: $item_id STOCK: $instock   ---MARKED VISABLE--- \n";
					}
					$efc->touchSKU($sku_pid);


				}


				//echo "#$masscount ... [UPDATED]  PID: $pid   ITEM_ID: $item_id  STOCK: $instock\n";

			}


		}



	}
	else
	{
		list($pid) = mysql_fetch_array($qok1);

		if(is_numeric($pid))
		{
			$query = "update vendor_pricing set instock='$instock' where pid = '$pid'";
			echo "using MANPART column     $item_id ".$query."\n";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$uc++;

			//$efc->updateVendorSourcing($pid, $VENDORID);

			$query = "select pid from product_sourcing where vpid = '$pid' and distid = '$VENDORID'";
			if(!$qok111 = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok111) == 0)
			{ // dont exist. not mapped.
				$no++;
				//echo "NOT MAPPED: $vpid\n";
			}
			else
			{

				list($sku_pid) = mysql_fetch_array($qok111);

				$query = "update product_sourcing set qty='$instock' where pid = '$sku_pid' and vpid = '$pid' and distid = '$VENDORID'";
				if(!$qok222 = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				if($instock > 9)
				{
					$markedvis++;
					$query = "update product_list set visible = 1 where id = '$sku_pid'";
					if(!$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					echo "(#$markedvis)  PID: $pid  SKU: $sku_pid ITEM_ID: $item_id STOCK: $instock   ---MARKED VISABLE--- \n";
				}
				$efc->touchSKU($sku_pid);


			}


			//echo "#$masscount ... [UPDATED]  PID: $pid   ITEM_ID: $item_id  STOCK: $instock\n";

		}


	}









}




////  Core Monitor ///////////////////////////////////////////////////////////////////////////////////////////////////////
$time = addslashes(date("Y-m-d H:i:s"));
$data = 'CSV Sync';
$query = "delete from doba_monitor_timestamp where data = '$data'";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
$query = "insert into doba_monitor_timestamp(data,cur_timestamp) values('$data','$time')";
if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







?>
					