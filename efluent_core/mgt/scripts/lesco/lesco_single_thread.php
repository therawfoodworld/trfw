<?
include("lesco_parser.php");
include("lesco_cookie.php");
include("lesco_codes.php");
include("lesco_drill.php");

function data_dump($data, $brand, $gcc, $vend_code)
{
	if($data == "") { die("DATA WAS BLANK :-(\n"); }

	$brand = preg_replace("/[^a-zA-Z0-9\s]/", "", $brand);

	//$brand = str_replace("_"," ",$brand);
	$brand = str_replace(" ","_",$brand);


	foreach ($data as $dmp) { if($dmp_data == "") { $dmp_data = "$dmp\n"; } else { $dmp_data .= $dmp."\n"; } }

	echo $dmp_data."\n";

	$file = "dump/$brand.csv";

	if(file_exists($file)) { unlink($file); /*echo "rm -rf $file\n";*/ }

	file_put_contents($file, $dmp_data);

	echo "[*] Generated feed for brand $brand ($vend_code) => '$file'\n";

	$gcc++;

	return $gcc;

}


$vend_code = $argv[1];
$asp_c = $argv[2];
$brand = str_replace("_"," ",$argv[3]);
$gc = $argv[4];


if($brand == "Metra") { echo "\n\n* * *     BRAND REJECTED   $brand     * * *\n\n"; }
else {

	echo "VEND_CODE: $vend_code\n";
	echo "ASP_C: $asp_c\n";
	echo "BRAND: $brand\n";

	unset($data);

	$data = drill_code($vend_code,$asp_c);

	$brand = str_replace("_"," ",$brand);
	//$brand = str_replace("+","/",$brand);

	$data = parse_buffer($data,$brand);

	//print_r($data);


	$brand = str_replace(" ","_",$brand);
	$brand = preg_replace("/[^a-zA-Z0-9\s]/", "", $brand); // should fix all specials filename nolonger matters

	$myFile = "dump/$brand.csv";
	echo "\$myFile = '$myFile'\n";

	if(file_exists($myFile)) { unlink($myFile); }
	$fh = fopen($myFile, 'w') or die("can't open file");

	foreach($data as $key => $pline)
	{
		fwrite($fh, $pline."\n");
		echo "-> [$key] $pline\n";

	}
	fclose($fh);

	$wc = trim(`wc -l $myFile`);
	echo "DUMP: $wc\n";

	//echo "$gc = data_dump(\"'$data'\",\"'$brand'\",\"'$gc'\", \"'$vend_code'\");\n";
	//$gc = data_dump("'$data'","'$brand'","'$gc'", "'$vend_code'");
}

?>