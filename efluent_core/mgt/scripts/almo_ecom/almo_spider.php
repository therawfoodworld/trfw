<?
ini_set("memory_limit","-1");
//ini_set("display_errors","2");
ERROR_REPORTING(0);

$ROOT_DIR = "/home/httpd/ecomelectronics.com";

set_time_limit(999999);

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

$VENDORID = $efc->getConfigValue("almo", "VENDORID");
$vid = $VENDORID;

include($ROOT_DIR."/mgt/scripts/vid_date.php");


$max_crawl_value = 10000000;

if(!file_exists("almo.xml")) { die("\nXML FILE MISSING!\nABORT!\n"); }

$buffer = file_get_contents("almo.xml");
$data = xml2array($buffer, 0);

for($xx = 0; $xx < count($data['data']['product_record']); $xx++)
{

	$go = 0;
	if($main_count == "$max_crawl_value") { die("\nEOF  #$max_crawl_value\n");  }

	$manname = addslashes($data['data']['product_record'][$xx]['Brand']);
	$manpart = addslashes($data['data']['product_record'][$xx]['Item']);
	$sku = $manpart;

	$query = "select manpart,psrc from almo_spider_index where manpart = '$manpart'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($manpart,$psrc) = mysql_fetch_array($qok);
	if($psrc !="")
	{
		$go = 1;
		//$a = "ATH-CK1WYL";
		//if($manpart == "$a") { $go = 1; }
		//spider($sku,&$mysqlid,$vid,$DB);
	}

	if($go == 1)
	{
		$main_count++;

		$string = "$manpart,$psrc";

		//if(!preg_match("/ATH-/",$manpart))
		//{


		//if(file_exists("debug/$manpart.htm")) { unlink("debug/$manpart.htm"); } // ...
		if(!file_exists("debug/$manpart.htm")) // CRAWL ONCE UNLESS CLEAN DEBUG OUTPUT.
		{
			//echo "\n\n[?] MANPART: $manpart\n";
			echo "[$main_count] $string\n";
			$manpartfile = str_replace("/","",$manpart);
			$cmd = "php almo_spider_exec.php '$string' > debug/$manpartfile.htm";
			echo $cmd."\n";
			//$cmd = "php almo_spida.php $manpart";
			passthru($cmd);
		} else { echo "*** [DEBUG]  ***:   '$string'\n"; }
		//}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////


function getmicrotime()
{
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}


function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides this.
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}

?>
<?mysql_close();?>
