<?

//$host = "66.132.174.237"; // live.
//$host = "ecom.efluent.com"; // dev.

$host = "www.ecomelectronics.com";

$key = "af832d25e1135c4704f3488fb64964be784bafde";

$sellerid = 4788736;




//$status = "All";

$status = "New";




if(file_exists("xml")) { unlink("xml"); }

$d_1 = date("m");

if($d_1 !="1")
{
	$d_1 = $d_1 - 1;
}

$d_2 = '01';

$d_3 = date("Y");

$date1 = "$d_1-$d_2-$d_3";

$date2 = date("m-d-Y");


///////// force date
//$date1 = '1-01-2011';
//$date2 = '1-31-2011';
////////////////////



$data_Str = "from-date=$date1&order-status=$status&to-date=$date2";

$str = base64_encode(hash_hmac("sha512", $data_Str, "$key", True));
$str = str_replace("+","-",$str);
$str = str_replace("/","_",$str);
$str = str_replace("=","",$str);

$req = "https://api.stores.nextag.com/v1/seller/$sellerid/order?$data_Str&signature=$str";

//echo "\n$req\n";

$cmd = "/usr/bin/curl -k -o xml '$req'";
passthru($cmd);

$raw_data = file_get_contents("xml");

$xml_array = xml2array($raw_data, 0);

foreach($xml_array[orders][order] as $k => $s) { $keys[$k] = $k; }


$data[0] = "Order ID";
$data[1] = "Order Placed Date";
$data[2] = "Order Placed Time";
$data[3] = "Order Status";
$data[4] = "Order Total";
$data[5] = "Product SubTotal";
$data[6] = "Shipping";
$data[7] = "Tax";
$data[8] = "Shipping Method";
$data[9] = "Ship To First Name";
$data[10] = "Ship To Last Name";
$data[11] = "Ship To Street 1";
$data[12] = "Ship To Street 2";
$data[13] = "Ship To City";
$data[14] = "Ship To State";
$data[15] = "Ship To State Code";
$data[16] = "Ship To ZipCode";
$data[17] = "Ship To Country";
$data[18] = "Manufacturer Part Number";
$data[19] = "Merchant SKU";
$data[20] = "Product Name";
$data[21] = "Quantity";
$data[22] = "Category Name";
$data[23] = "Nextag Commission";
$data[24] = "Shipment Confirmation Date";
$data[25] = "Shipment Confirmation Time";
$data[26] = "Total Refunded";
$data[27] = "Refund Date";
$data[28] = "Refund Time";
$data[29] = "Shipping Phone Number";
$data[30] = "Billing Phone Number";
$data[31] = "cntry";
$data[32] = "CustomPriceValueAdded";

//print_r($data); // <-- something bugged out.

$line_key = 0;
unset($line);
while($line_key < 33)
{
	if($line_key == "32") { $line .= $data[32]."\n"; } else { $line .= $data[$line_key].','; }
	$line_key++;
}
$lines[] = $line;

//print_r($lines); // <-- something bugged out.

//print_r($keys);

$single = 0;

foreach($keys as $key)
{
	//echo "KEY CHECK: $key\n";
	if($single !="1")
	{
		if(!is_numeric($key))
		{ // single order
			$single = "1";
			echo "SINGLE ORDER, PROCESSING ....\n";
			//sleep(2);
		}
	}


	if($single == "0")
	{

		unset($data);

		$data[0] = 'NX'.$xml_array['orders']['order'][$key]['nextag-order-id'];
		$data[1] = $xml_array['orders']['order'][$key]['created-date'];
		$data[2] = $xml_array['orders']['order'][$key]['created-date'];
		$data[3] = $xml_array['orders']['order'][$key]['order-status'];
		$data[4] = $xml_array['orders']['order'][$key]['total'];
		$data[5] = $xml_array['orders']['order'][$key]['subtotal'];
		$data[6] = $xml_array['orders']['order'][$key]['shipping'];
		$data[7] = $xml_array['orders']['order'][$key]['tax'];
		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[8] = $xml_array['orders']['order'][$key]['shipments']['shipment']['shipping-method']; } else { $data[8] = ''; }
		$data[9] = $xml_array['orders']['order'][$key]['shipping-address']['first-name'];
		$data[10] = $xml_array['orders']['order'][$key]['shipping-address']['last-name'];

		$data[11] = $xml_array['orders']['order'][$key]['shipping-address']['address1'];
		$data[11] = str_replace(",",".",$data[11]);

		$data[12] = $xml_array['orders']['order'][$key]['shipping-address']['address2'];
		$data[13] = $xml_array['orders']['order'][$key]['shipping-address']['city'];
		$data[14] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
		$data[15] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
		$data[16] = $xml_array['orders']['order'][$key]['shipping-address']['postal-code'];
		$data[17] = 'US';
		$data[18] = $xml_array['orders']['order'][$key]['items']['item']['manufacturer-part-no'];
		$data[19] = $xml_array['orders']['order'][$key]['items']['item']['merchant-part-no'];

		$data[20] = $xml_array['orders']['order'][$key]['items']['item']['name'];
		$data[20] = str_replace(",",".",$data[20]);

		$data[21] = $xml_array['orders']['order'][$key]['items']['item']['quantity'];
		$data[22] = 'LOCAL_CATEGORY';
		$data[23] = $xml_array['orders']['order'][$key]['nextag-fee'];
		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[24] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[24] = ''; }
		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[25] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[25] = ''; }
		$data[26] = $xml_array['orders']['order'][$key]['refund'];
		$data[27] = 'Refund Date_NOT-IN-XML';
		$data[28] = 'Refund Time_NOT-IN-XML';
		$data[29] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
		$data[30] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
		$data[31] = 'US';




		$com = $data[23];
		$total = $data[4];
		$qty = $data[21];

		//echo "com: $com\ntotal: $total\n"; // <-------------------------------------

		if(is_numeric($com) && is_numeric($total)) {

			$newtotal = $total - $com;

			$data[4] = $newtotal;


			$per_com = $com / $qty;
			$per_price = $total / $qty;
			$price = $per_price - $per_com;

			setlocale(LC_MONETARY, 'en_US');
			$string = str_replace("$","",trim(money_format('%(#10n', $price)));
			$string = str_replace(" ","",$string);
			$price = $string;
			$data[32] = $price; // reset new price.



			$line_key = 0;
			unset($line);
			while($line_key < 33)
			{
				if($line_key == "32") { $line .=$data[32]."\n"; } else { $line .=$data[$line_key].','; }
				$line_key++;
			}
			$lines[] = $line;
		}
	}
}


if($single == "1")
{

	unset($data);

	$data[0] = 'NX'.$xml_array['orders']['order']['nextag-order-id'];
	$data[1] = $xml_array['orders']['order']['created-date'];
	$data[2] = $xml_array['orders']['order']['created-date'];
	$data[3] = $xml_array['orders']['order']['order-status'];
	$data[4] = $xml_array['orders']['order']['total'];
	$data[5] = $xml_array['orders']['order']['subtotal'];
	$data[6] = $xml_array['orders']['order']['shipping'];
	$data[7] = $xml_array['orders']['order']['tax'];
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[8] = $xml_array['orders']['order']['shipments']['shipment']['shipping-method']; } else { $data[8] = ''; }
	$data[9] = $xml_array['orders']['order']['shipping-address']['first-name'];
	$data[10] = $xml_array['orders']['order']['shipping-address']['last-name'];

	$data[11] = $xml_array['orders']['order']['shipping-address']['address1'];
	$data[11] = str_replace(",",".",$data[11]);

	$data[12] = $xml_array['orders']['order']['shipping-address']['address2'];
	$data[13] = $xml_array['orders']['order']['shipping-address']['city'];
	$data[14] = $xml_array['orders']['order']['shipping-address']['region'];
	$data[15] = $xml_array['orders']['order']['shipping-address']['region'];
	$data[16] = $xml_array['orders']['order']['shipping-address']['postal-code'];
	$data[17] = 'US';
	$data[18] = $xml_array['orders']['order']['items']['item']['manufacturer-part-no'];
	$data[19] = $xml_array['orders']['order']['items']['item']['merchant-part-no'];

	$data[20] = $xml_array['orders']['order']['items']['item']['name'];
	$data[20] = str_replace(",",".",$data[20]);

	$data[21] = $xml_array['orders']['order']['items']['item']['quantity'];
	$data[22] = 'LOCAL_CATEGORY';
	$data[23] = $xml_array['orders']['order']['nextag-fee'];
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[24] = $xml_array['orders']['order']['shipments']['shipment']['ship-date']; } else { $data[24] = ''; }
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[25] = $xml_array['orders']['order']['shipments']['shipment']['ship-date']; } else { $data[25] = ''; }
	$data[26] = $xml_array['orders']['order']['refund'];
	$data[27] = 'Refund Date_NOT-IN-XML';
	$data[28] = 'Refund Time_NOT-IN-XML';
	$data[29] = $xml_array['orders']['order']['shipping-address']['phone'];
	$data[30] = $xml_array['orders']['order']['shipping-address']['phone'];
	$data[31] = 'US';

	$com = $data[23];
	$total = $data[4];
	$qty = $data[21];

	if(is_numeric($com) && is_numeric($total)) {

		$newtotal = $total - $com;
		$data[4] = $newtotal;


		$per_com = $com / $qty;
		$per_price = $total / $qty;
		$price = $per_price - $per_com;

		setlocale(LC_MONETARY, 'en_US');
		$string = str_replace("$","",trim(money_format('%(#10n', $price)));
		$string = str_replace(" ","",$string);
		$price = $string;
		$data[32] = $price; // reset new price.


		$line_key = 0;
		unset($line);
		while($line_key < 33)
		{
			if($line_key == "32") { $line .=$data[32]."\n"; } else { $line .=$data[$line_key].','; }
			$line_key++;
		}
		$lines[] = $line;
	}
}

$csv_l_c = 0;
if(file_exists($csv_file)) { unlink($csv_file); }


$d_1 = date("m");
$d_2 = '01';
$d_3 = date("Y");

$date1 = "$d_1-$d_2-$d_3";
$date2 = date("m-d-Y");


///////// force date
//$date1 = '1-01-2011';
//$date2 = '1-31-2011';
////////////////////



$csv_file = "nxOrdersCSV_".$date1."_".$date2.".csv";

$fh = fopen($csv_file, 'w') or die("can't open file");
foreach($lines as $string)
{
	$csv_l_c++;
	fwrite($fh, $string);
}
fclose($fh);

echo "$csv_file = $csv_l_c lines\n";

$content = "profilename=&filename=$csv_file&column0=invoiceid&column1=orderdate&column2=&column3=&column4=total&column5=uprice&column6=shipping&column7=tax&column8=shipmethod&column9=fname&column10=lname&column11=bill2street1&column12=bill2street2&column13=bill2city&column14=&column15=bill2state&column16=bill2zip&column17=&column18=&column19=pid&column20=&column21=qty&column22=&column23=&column24=&column25=&column26=&column27=&column28=&column29=&column30=&column31=ship2country&Action=Import+Orders&NAOK=777";
$content_l = strlen($content);

//Accept-Encoding: gzip,deflate

$packet = "
POST /mgt/oi/upload_step3_nx.phtml HTTP/1.1
Host: $host
User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
Keep-Alive: 115
Connection: keep-alive
Referer: http://$host/mgt/oi/upload_step2.phtml?filename=test_5.csv&DoThis=&oid=14&&oiid=14&Action=Load+Profile
Cookie: PHPSESSID=5j29h2a93pbltdlemacfjsi215
Content-Type: application/x-www-form-urlencoded
Content-Length: $content_l


$content

";

//echo "\n\n$packet\n";

$file_location = "/home/httpd/ecomelectronics.com/mgt/oi/uploads/$csv_file";

$shh = `rm -rf $file_location`; // clean
$shh = `cp $csv_file $file_location`; // move

echo "$csv_file => $file_location\n";

if(file_exists("packet")) { unlink("packet"); }
file_put_contents("packet",$packet);

$run = "nc $host 80 < packet > output-".$date1."_".$date2;
$exec = `$run`;




///////////////////////  parse for core monitor values, blah ///////////////////////
$d_1 = date("m");
$d_2 = '01';
$d_3 = date("Y");

$date1 = "$d_1-$d_2-$d_3";
$date2 = date("m-d-Y");


///////// force date
//$date1 = '1-01-2011';
//$date2 = '1-31-2011';
////////////////////



$output_file = "output-".$date1."_".$date2;

echo "filename: $output_file\n";

$output = file_get_contents($output_file);

$output_array = explode("\n",$output);


/*foreach($output_array as $key => $str)
{
echo "[$key] $str\n";
}*/


//$imported = preg_replace('/\D/', '', $output_array[800]);
//$dupes = preg_replace('/\D/', '', $output_array[804]);

$imported = preg_replace('/\D/', '', $output_array[479]);
$dupes = preg_replace('/\D/', '', $output_array[483]);

/*
echo "$output_array[430]\n";
echo "$output_array[431]\n";
echo "$output_array[432]\n";
echo "$output_array[433]\n";
echo "$output_array[434]\n";
echo "$output_array[435]\n";
echo "$output_array[436]\n";
echo "$output_array[437]\n";
echo "$output_array[438]\n";
echo "$output_array[439]\n";
echo "$output_array[440]\n";
*/


//print_r($output_array); exit; // <--------------------------------

// update core monitor with values and time of run.

$nxoi = "/tmp/.nxoi";
//$cmdate = str_replace("\n","",`date`);
//$cmdate = str_replace("2011","",$cmdate);
//$cmdate = str_replace("EST","",$cmdate);
//$cmdate = str_replace("EDT","",$cmdate);

$cmdate = date('m/d/y h:m');

$imported = trim($imported);
$dupes = trim($dupes);

if($imported == "") { $imported = 0; }
if($dupes == "") { $dupes = 0; }

$cmstring = "I/$imported D/$dupes $cmdate";


echo "CORE MONITOR: $cmstring\n";

$mark = `echo '$cmstring' > $nxoi`;


$ROOT_DIR = "/home/httpd/ecomelectronics.com";
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$query = "truncate nxoi_monitor";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

$cmstring = addslashes($cmstring);
$query = "insert into nxoi_monitor(value) values('$cmstring')";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }



///////////////////////////////////////////////////////////////////////////////////


function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides this.
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}

?><?mysql_close();?>
