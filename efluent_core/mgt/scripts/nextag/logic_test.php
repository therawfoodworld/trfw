<pre>
<?

// for quick debug

$raw_data = file_get_contents("xml");

$xml_array = xml2array($raw_data, 0);

foreach($xml_array[orders][order] as $k => $s) { $keys[$k] = $k; }

$data[0] = "Order ID";
$data[1] = "Order Placed Date";
$data[2] = "Order Placed Time";
$data[3] = "Order Status";
$data[4] = "Order Total";
$data[5] = "Product SubTotal";
$data[6] = "Shipping";
$data[7] = "Tax";
$data[8] = "Shipping Method";
$data[9] = "Ship To First Name";
$data[10] = "Ship To Last Name";
$data[11] = "Ship To Street 1";
$data[12] = "Ship To Street 2";
$data[13] = "Ship To City";
$data[14] = "Ship To State";
$data[15] = "Ship To State Code";
$data[16] = "Ship To ZipCode";
$data[17] = "Ship To Country";
$data[18] = "Manufacturer Part Number";
$data[19] = "Merchant SKU";
$data[20] = "Product Name";
$data[21] = "Quantity";
$data[22] = "Category Name";
$data[23] = "Nextag Commission";
$data[24] = "Shipment Confirmation Date";
$data[25] = "Shipment Confirmation Time";
$data[26] = "Total Refunded";
$data[27] = "Refund Date";
$data[28] = "Refund Time";
$data[29] = "Shipping Phone Number";
$data[30] = "Billing Phone Number";
$data[31] = "cntry";
$data[32] = "CustomPriceValueAdded";

//print_r($data); // <-- something bugged out.

$line_key = 0;
unset($line);
while($line_key < 33)
{
	if($line_key == "32") { $line .= $data[32]."\n"; } else { $line .= $data[$line_key].','; }
	$line_key++;
}
$lines[] = $line;

//print_r($lines); // <-- something bugged out.

//print_r($keys);

$single = 0;

foreach($keys as $key)
{
	//echo "KEY CHECK: $key\n";
	if($single !="1")
	{
		if(!is_numeric($key))
		{ // single order
			$single = "1";
			echo "SINGLE ORDER, PROCESSING ....\n";
			//sleep(2);
		}
	}


	if($single == "0")
	{

		unset($data);

		$data[0] = 'NX'.$xml_array['orders']['order'][$key]['nextag-order-id'];
		$data[1] = $xml_array['orders']['order'][$key]['created-date'];
		$data[2] = $xml_array['orders']['order'][$key]['created-date'];
		$data[3] = $xml_array['orders']['order'][$key]['order-status'];


		$data[4] = $xml_array['orders']['order'][$key]['total'];


		$data[5] = $xml_array['orders']['order'][$key]['subtotal'];


		$data[6] = $xml_array['orders']['order'][$key]['shipping'];
		$data[7] = $xml_array['orders']['order'][$key]['tax'];



		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[8] = $xml_array['orders']['order'][$key]['shipments']['shipment']['shipping-method']; } else { $data[8] = ''; }
		$data[9] = $xml_array['orders']['order'][$key]['shipping-address']['first-name'];
		$data[10] = $xml_array['orders']['order'][$key]['shipping-address']['last-name'];

		$data[11] = $xml_array['orders']['order'][$key]['shipping-address']['address1'];
		$data[11] = str_replace(",",".",$data[11]);

		$data[12] = $xml_array['orders']['order'][$key]['shipping-address']['address2'];
		$data[13] = $xml_array['orders']['order'][$key]['shipping-address']['city'];
		$data[14] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
		$data[15] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
		$data[16] = $xml_array['orders']['order'][$key]['shipping-address']['postal-code'];
		$data[17] = 'US';
		$data[18] = $xml_array['orders']['order'][$key]['items']['item']['manufacturer-part-no'];
		$data[19] = $xml_array['orders']['order'][$key]['items']['item']['merchant-part-no'];

		$data[20] = $xml_array['orders']['order'][$key]['items']['item']['name'];
		$data[20] = str_replace(",",".",$data[20]);



		$data[21] = $xml_array['orders']['order'][$key]['items']['item']['quantity'];



		$data[22] = 'LOCAL_CATEGORY';



		$data[23] = $xml_array['orders']['order'][$key]['nextag-fee'];



		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[24] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[24] = ''; }
		if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[25] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[25] = ''; }

		$data[26] = $xml_array['orders']['order'][$key]['refund'];
		$data[27] = 'Refund Date_NOT-IN-XML';
		$data[28] = 'Refund Time_NOT-IN-XML';
		$data[29] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
		$data[30] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
		$data[31] = 'US';


		$com = $data[23];
		$total = $data[4];
		$qty = $data[21];


		$newtotal = $total - $com;
		$data[4] = $newtotal;


		$per_com = $com / $qty;
		$per_price = $total / $qty;
		$price = $per_price - $per_com;

		setlocale(LC_MONETARY, 'en_US');
		$string = str_replace("$","",trim(money_format('%(#10n', $price)));
		$string = str_replace(" ","",$string);
		$price = $string;
		$data[32] = $price; // reset new price.



		$line_key = 0;
		unset($line);
		while($line_key < 33)
		{
			if($line_key == "32") { $line .=$data[32]."\n"; } else { $line .=$data[$line_key].','; }
			$line_key++;
		}
		$lines[] = $line;
	}
}


if($single == "1")
{

	unset($data);

	$data[0] = 'NX'.$xml_array['orders']['order']['nextag-order-id'];
	$data[1] = $xml_array['orders']['order']['created-date'];
	$data[2] = $xml_array['orders']['order']['created-date'];
	$data[3] = $xml_array['orders']['order']['order-status'];
	$data[4] = $xml_array['orders']['order']['total'];
	$data[5] = $xml_array['orders']['order']['subtotal'];
	$data[6] = $xml_array['orders']['order']['shipping'];
	$data[7] = $xml_array['orders']['order']['tax'];
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[8] = $xml_array['orders']['order']['shipments']['shipment']['shipping-method']; } else { $data[8] = ''; }
	$data[9] = $xml_array['orders']['order']['shipping-address']['first-name'];
	$data[10] = $xml_array['orders']['order']['shipping-address']['last-name'];

	$data[11] = $xml_array['orders']['order']['shipping-address']['address1'];
	$data[11] = str_replace(",",".",$data[11]);

	$data[12] = $xml_array['orders']['order']['shipping-address']['address2'];
	$data[13] = $xml_array['orders']['order']['shipping-address']['city'];
	$data[14] = $xml_array['orders']['order']['shipping-address']['region'];
	$data[15] = $xml_array['orders']['order']['shipping-address']['region'];
	$data[16] = $xml_array['orders']['order']['shipping-address']['postal-code'];
	$data[17] = 'US';
	$data[18] = $xml_array['orders']['order']['items']['item']['manufacturer-part-no'];
	$data[19] = $xml_array['orders']['order']['items']['item']['merchant-part-no'];

	$data[20] = $xml_array['orders']['order']['items']['item']['name'];
	$data[20] = str_replace(",",".",$data[20]);

	$data[21] = $xml_array['orders']['order']['items']['item']['quantity'];
	$data[22] = 'LOCAL_CATEGORY';
	$data[23] = $xml_array['orders']['order']['nextag-fee'];
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[24] = $xml_array['orders']['order']['shipments']['shipment']['ship-date']; } else { $data[24] = ''; }
	if(is_array($xml_array['orders']['order']['shipments'])) { $data[25] = $xml_array['orders']['order']['shipments']['shipment']['ship-date']; } else { $data[25] = ''; }
	$data[26] = $xml_array['orders']['order']['refund'];
	$data[27] = 'Refund Date_NOT-IN-XML';
	$data[28] = 'Refund Time_NOT-IN-XML';
	$data[29] = $xml_array['orders']['order']['shipping-address']['phone'];
	$data[30] = $xml_array['orders']['order']['shipping-address']['phone'];
	$data[31] = 'US';


	$com = $data[23];
	$total = $data[4];

	$newtotal = $total - $com;
	$data[4] = $newtotal;

	$qty = $data[21];

	$per_com = $com / $qty;
	$per_price = $total / $qty;
	$price = $per_price - $per_com;

	setlocale(LC_MONETARY, 'en_US');
	$string = str_replace("$","",trim(money_format('%(#10n', $price)));
	$string = str_replace(" ","",$string);
	$price = $string;
	$data[32] = $price; // reset new price.


	$line_key = 0;
	unset($line);
	while($line_key < 33)
	{
		if($line_key == "32") { $line .=$data[32]."\n"; } else { $line .=$data[$line_key].','; }
		$line_key++;
	}
	$lines[] = $line;
}


/*
$csv_file = "logic_test.csv";

$fh = fopen($csv_file, 'w') or die("can't open file");
foreach($lines as $string)
{
$csv_l_c++;
fwrite($fh, $string);
}
fclose($fh);

echo "$csv_file = $csv_l_c lines\n";

*/

foreach($lines as $line_data)
{
	$b = explode(",",$line_data);
	print_r($b);
}

///////////////////////////////////////////////////////////////////////////////////


function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides this.
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}
?>
<?mysql_close();?>
