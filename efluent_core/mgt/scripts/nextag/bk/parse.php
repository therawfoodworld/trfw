<?
INCLUDE("xml2array.php");
$raw_data = file_get_contents("xml");

$xml_array = xml2array($raw_data, 0);

foreach($xml_array[orders][order] as $k => $s) { $keys[$k] = $k; }


$data[0] = 'Order ID';
$data[1] = 'Order Placed Date';
$data[2] = 'Order Placed Time';
$data[3] = 'Order Status';
$data[4] = 'Order Total';
$data[5] = 'Product SubTotal';
$data[6] = 'Shipping';
$data[7] = 'Tax';
$data[8] = 'Shipping Method';
$data[9] = 'Ship To First Name';
$data[10] = 'Ship To Last Name';
$data[11] = 'Ship To Street 1';
$data[12] = 'Ship To Street 2';
$data[13] = 'Ship To City';
$data[14] = 'Ship To State';
$data[15] = 'Ship To State Code';
$data[16] = 'Ship To ZipCode';
$data[17] = 'Ship To Country';
$data[18] = 'Manufacturer Part Number';
$data[19] = 'Merchant SKU';
$data[20] = 'Product Name';
$data[21] = 'Quantity';
$data[22] = 'Category Name';
$data[23] = 'Nextag Commission';
$data[24] = 'Shipment Confirmation Date';
$data[25] = 'Shipment Confirmation Time';
$data[26] = 'Total Refunded';
$data[27] = 'Refund Date';
$data[28] = 'Refund Time';
$data[29] = 'Shipping Phone Number';
$data[30] = 'Billing Phone Number';
$data[31] = 'cntry';

$line_key = 0;
unset($line);
while($line_key < 32)
{
	if($line_key == "31") { $line .=$data[31]."\n"; } else { $line .=$data[$line_key].','; }
	$line_key++;
}
$lines[] = $line;


foreach($keys as $key)
{


	unset($data);

	$data[0] = 'NX'.$xml_array['orders']['order'][$key]['nextag-order-id'];
	$data[1] = $xml_array['orders']['order'][$key]['created-date'];
	$data[2] = $xml_array['orders']['order'][$key]['created-date'];
	$data[3] = $xml_array['orders']['order'][$key]['order-status'];
	$data[4] = $xml_array['orders']['order'][$key]['total'];
	$data[5] = $xml_array['orders']['order'][$key]['subtotal'];
	$data[6] = $xml_array['orders']['order'][$key]['shipping'];
	$data[7] = $xml_array['orders']['order'][$key]['tax'];
	if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[8] = $xml_array['orders']['order'][$key]['shipments']['shipment']['shipping-method']; } else { $data[8] = ''; }
	$data[9] = $xml_array['orders']['order'][$key]['shipping-address']['first-name'];
	$data[10] = $xml_array['orders']['order'][$key]['shipping-address']['last-name'];
	$data[11] = $xml_array['orders']['order'][$key]['shipping-address']['address1'];
	$data[12] = $xml_array['orders']['order'][$key]['shipping-address']['address2'];
	$data[13] = $xml_array['orders']['order'][$key]['shipping-address']['city'];
	$data[14] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
	$data[15] = $xml_array['orders']['order'][$key]['shipping-address']['region'];
	$data[16] = $xml_array['orders']['order'][$key]['shipping-address']['postal-code'];
	$data[17] = 'US';
	$data[18] = $xml_array['orders']['order'][$key]['items']['item']['manufacturer-part-no'];
	$data[19] = $xml_array['orders']['order'][$key]['items']['item']['merchant-part-no'];
	$data[20] = $xml_array['orders']['order'][$key]['items']['item']['name'];
	$data[21] = $xml_array['orders']['order'][$key]['items']['item']['quantity'];
	$data[22] = 'LOCAL_CATEGORY';
	$data[23] = $xml_array['orders']['order'][$key]['nextag-fee'];
	if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[24] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[24] = ''; }
	if(is_array($xml_array['orders']['order'][$key]['shipments'])) { $data[25] = $xml_array['orders']['order'][$key]['shipments']['shipment']['ship-date']; } else { $data[25] = ''; }
	$data[26] = $xml_array['orders']['order'][$key]['refund'];
	$data[27] = 'Refund Date_NOT-IN-XML';
	$data[28] = 'Refund Time_NOT-IN-XML';
	$data[29] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
	$data[30] = $xml_array['orders']['order'][$key]['shipping-address']['phone'];
	$data[31] = 'US';
	
	$line_key = 0;
	unset($line);
	while($line_key < 32)
	{
		if($line_key == "31") { $line .=$data[31]."\n"; } else { $line .=$data[$line_key].','; }
		$line_key++;
	}
	$lines[] = $line;
}


$csv_l_c = 0;
if(file_exists($csv_file)) { unlink($csv_file); }


$d_1 = date("m");
$d_2 = '01';
$d_3 = date("Y");

$date1 = "$d_1-$d_2-$d_3";
$date2 = date("m-d-Y");

$csv_file = "nxOrdersCSV_".$date1."_".$date2.".csv";

$fh = fopen($csv_file, 'w') or die("can't open file");
foreach($lines as $string) 
{
	$csv_l_c++;
	fwrite($fh, $string); 
}
fclose($fh);

echo "$csv_file = $csv_l_c lines\n";

$content = "profilename=&filename=$csv_file&column0=invoiceid&column1=orderdate&column2=&column3=&column4=total&column5=uprice&column6=shipping&column7=tax&column8=shipmethod&column9=fname&column10=lname&column11=bill2street1&column12=bill2street2&column13=bill2city&column14=&column15=bill2state&column16=bill2zip&column17=&column18=&column19=pid&column20=&column21=qty&column22=&column23=&column24=&column25=&column26=&column27=&column28=&column29=&column30=&column31=ship2country&Action=Import+Orders&NAOK=777";
$content_l = strlen($content);

$packet = "
POST /mgt/oi/upload_step3_nx.phtml HTTP/1.1
Host: ecom.efluent.com
User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip,deflate
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
Keep-Alive: 115
Connection: keep-alive
Referer: http://ecom.efluent.com/mgt/oi/upload_step2.phtml?filename=test_5.csv&DoThis=&oid=14&&oiid=14&Action=Load+Profile
Cookie: PHPSESSID=5j29h2a93pbltdlemacfjsi215
Content-Type: application/x-www-form-urlencoded
Content-Length: $content_l


$content

";

//echo "\n\n$packet\n";

$file_location = "/home/httpd/ecomelectronics.com/mgt/oi/uploads/$csv_file";

$shh = `rm -rf $file_location`; // clean
$shh = `cp $csv_file $file_location`; // move

echo "$csv_file => $file_location\n";

if(file_exists("packet")) { unlink("packet"); }
file_put_contents("packet",$packet);

$run = "nc ecom.efluent.com 80 < packet > output-".$date1."_".$date2;
$exec = `$run`;

///////////////////////  parse for core monitor values, blah ///////////////////////
$d_1 = date("m");
$d_2 = '01';
$d_3 = date("Y");

$date1 = "$d_1-$d_2-$d_3";
$date2 = date("m-d-Y");


$output_file = "output-".$date1."_".$date2;

echo "filename: $output_file\n";

$output = file_get_contents($output_file);

$output_array = explode("\n",$output);

/*
foreach($output_array as $key => $str)
{
	echo "[$key] $str\n";
}
*/

$imported = preg_replace('/\D/', '', $output_array[800]);
$dupes = preg_replace('/\D/', '', $output_array[804]);
///////////////////////////////////////////////////////////////////////////////////



?>