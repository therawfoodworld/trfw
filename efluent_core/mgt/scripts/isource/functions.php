<?
// ISOURCE GUTS...


function auth($MODULE_PATH,$CLIENT)
{
	echo "AUTH.PACKET: packets/auth.packet.$CLIENT\n";
	$cookies = str_replace("Set-Cookie: ","",`nc www.inventorysource.com 80 < packets/auth.packet.$CLIENT | grep Cookie`);
	$cookies = explode("\n",$cookies);
	$c1 = explode(";",$cookies[0]);
	$c2 = explode(";",$cookies[1]);
	unset($cookies);
	$cookies = $c1[0].'; '.$c2[0];
	return $cookies;
}




function download(&$data_urls,$auth,$MODULE_PATH)
{
	$exec = `rm -rf *.csv`; // safety!
	//print_r($data_urls);
	foreach($data_urls as $data_url)
	{
		$num++;
		echo "($num) Downloading=> ".$data_url."\n";
		dl_packet($auth,$data_url,$MODULE_PATH);
		$data = curl($data_url,$auth);

		if(file_put_contents("$num.dirtycsv",$data))
		{
			echo "($num) SUCCESS...\n\n";
			$cmd = "sed '1,1d' $num.dirtycsv > $num.csv";
			passthru($cmd);
			unlink("$num.dirtycsv");
			//exit;
		}
		else
		{
			echo "($num) FAILURE!!\n\n";
			exit;
		}
		sleep(1);
	}
   //return $num;
}





function fetch($packet) {
	$fp = fsockopen('www.inventorysource.com', 80, $errno, $errstr, 30);

	fwrite($fp, $packet);

	$body = false;

	while (!feof($fp)) {
		$s = fgets($fp, 1024);
		if ( $body )
		$in .= $s;
		if ( $s == "\r\n" )
		$body = true;
	}

	fclose($fp);
	return $in;
}





function curl($data_url,$auth)
{
	$url = "http://www.inventorysource.com".$data_url;

	$headers = array();
	array_push($headers,'Cookie: '.$auth);

	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_TIMEOUT, 120);

	curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

	$response = curl_exec($ch);

	$responce = utf8_encode($responce);
	$responce = str_replace("\r","",$responce);

	if(is_int($response)) {    die("Errors: " . curl_errno($ch) . " : " . curl_error($ch)); }
	curl_close ($ch);

	return $response;
}





function curl_basic($data_url)
{
	$url = $data_url;

	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_TIMEOUT, 120);

	$response = curl_exec($ch);

	$responce = utf8_encode($responce);
	$responce = str_replace("\r","",$responce);

	if(is_int($response)) {    die("Errors: " . curl_errno($ch) . " : " . curl_error($ch)); }
	curl_close ($ch);

	return $response;
}





function data_packet($cookies,$MODULE_PATH)
{

	$data_packet = file_get_contents("packets/data.packet.$MODULE_PATH");
	$data_packet = explode("\n",$data_packet);

	foreach($data_packet as $k => $v)
	{
		if(preg_match("/Cookie/",$v))
		{
			$data_packet[$k] = 'Cookie: '.$cookies;
		}

	}

	$file = "data.packet.current";
	if(file_exists($file)) { unlink($file); }
	$fh = fopen($file, 'w') or die("can't open file");

	foreach($data_packet as $k => $s)
	{
		//echo $s."\n";
		fwrite($fh, $s."\n");
	}
	fclose($fh);
}






function dl_packet($cookies,$url,$MODULE_PATH)
{
	$dl_packet_path = "packets/dl.packet.$MODULE_PATH";
	$dl_packet = file_get_contents($dl_packet_path);
	$dl_packet = explode("\n",$dl_packet);


	foreach($dl_packet as $k => $v)
	{
		if(preg_match("/Cookie/",$v))
		{
			$dl_packet[$k] = 'Cookie: '.$cookies;
		}
		if(preg_match("/ACTIVE.csv/",$v))
		{
			$dl_packet[$k] = 'GET '.$url.' HTTP/1.1';
		}
	}

	$file = "dl.packet.current";
	if(file_exists($file)) { unlink($file); }
	$fh = fopen($file, 'w') or die("can't open file");

	foreach($dl_packet as $k => $s)
	{
		//echo $s."\n";
		fwrite($fh, $s."\n");
	}
	fclose($fh);
}









function merge($MODULE_PATH)
{
	$exec = `rm -rf $MODULE_PATH.csv`;
	$exec = `cat *.csv >> feed.tmp`;
	$nfiles = trim(`ls -l *.csv | wc -l`);
	$exec = `rm -rf *.csv`;
	$pcount = trim(`cat feed.tmp | wc -l`);
	$exec = `mv feed.tmp $MODULE_PATH.csv`;
	echo "\nMerged $nfiles CSV files into ($MODULE_PATH.csv), $pcount lines...\n\n";
	unlink("dl.packet.current");
}











function download_zips(&$data_urls,$auth,$MODULE_PATH)
{
	$exec = `rm -rf *.zip`; // safety!
	foreach($data_urls as $data_url)
	{
		$num++;
		echo "($num) Dowloading=> ".$data_url."\n";

		$data = curl_basic($data_url);

		if(file_put_contents("$num.zip",$data))
		{
			echo "($num) SUCCESS...\n\n";
		}
		else
		{
			echo "($num) FAILURE!!\n\n";
			exit;
		}
		sleep(1);
	}
	return $num;
}





function data_urls($MODULE_PATH,$text_line)
{
	echo "DOWNLOADING ALL $text_line FILES ...\n";

	if(file_exists("data.packet.current"))
	{
		$text = `nc www.inventorysource.com 80 < packets/data.packet.$MODULE_PATH | grep $text_line`;

		$match_elements = array(
		// HTML
		array('element'=>'a',       'attribute'=>'href'),       // 2.0
		array('element'=>'a',       'attribute'=>'urn'),        // 2.0
		array('element'=>'base',    'attribute'=>'href'),       // 2.0
		array('element'=>'form',    'attribute'=>'action'),     // 2.0
		array('element'=>'img',     'attribute'=>'src'),        // 2.0
		array('element'=>'link',    'attribute'=>'href'),       // 2.0

		array('element'=>'applet',  'attribute'=>'code'),       // 3.2
		array('element'=>'applet',  'attribute'=>'codebase'),   // 3.2
		array('element'=>'area',    'attribute'=>'href'),       // 3.2
		array('element'=>'body',    'attribute'=>'background'), // 3.2
		array('element'=>'img',     'attribute'=>'usemap'),     // 3.2
		array('element'=>'input',   'attribute'=>'src'),        // 3.2

		array('element'=>'applet',  'attribute'=>'archive'),    // 4.01
		array('element'=>'applet',  'attribute'=>'object'),     // 4.01
		array('element'=>'blockquote','attribute'=>'cite'),     // 4.01
		array('element'=>'del',     'attribute'=>'cite'),       // 4.01
		array('element'=>'frame',   'attribute'=>'longdesc'),   // 4.01
		array('element'=>'frame',   'attribute'=>'src'),        // 4.01
		array('element'=>'head',    'attribute'=>'profile'),    // 4.01
		array('element'=>'iframe',  'attribute'=>'longdesc'),   // 4.01
		array('element'=>'iframe',  'attribute'=>'src'),        // 4.01
		array('element'=>'img',     'attribute'=>'longdesc'),   // 4.01
		array('element'=>'input',   'attribute'=>'usemap'),     // 4.01
		array('element'=>'ins',     'attribute'=>'cite'),       // 4.01
		array('element'=>'object',  'attribute'=>'archive'),    // 4.01
		array('element'=>'object',  'attribute'=>'classid'),    // 4.01
		array('element'=>'object',  'attribute'=>'codebase'),   // 4.01
		array('element'=>'object',  'attribute'=>'data'),       // 4.01
		array('element'=>'object',  'attribute'=>'usemap'),     // 4.01
		array('element'=>'q',       'attribute'=>'cite'),       // 4.01
		array('element'=>'script',  'attribute'=>'src'),        // 4.01

		array('element'=>'audio',   'attribute'=>'src'),        // 5.0
		array('element'=>'command', 'attribute'=>'icon'),       // 5.0
		array('element'=>'embed',   'attribute'=>'src'),        // 5.0
		array('element'=>'event-source','attribute'=>'src'),    // 5.0
		array('element'=>'html',    'attribute'=>'manifest'),   // 5.0
		array('element'=>'source',  'attribute'=>'src'),        // 5.0
		array('element'=>'video',   'attribute'=>'src'),        // 5.0
		array('element'=>'video',   'attribute'=>'poster'),     // 5.0

		array('element'=>'bgsound', 'attribute'=>'src'),        // Extension
		array('element'=>'body',    'attribute'=>'credits'),    // Extension
		array('element'=>'body',    'attribute'=>'instructions'),//Extension
		array('element'=>'body',    'attribute'=>'logo'),       // Extension
		array('element'=>'div',     'attribute'=>'href'),       // Extension
		array('element'=>'div',     'attribute'=>'src'),        // Extension
		array('element'=>'embed',   'attribute'=>'code'),       // Extension
		array('element'=>'embed',   'attribute'=>'pluginspage'),// Extension
		array('element'=>'html',    'attribute'=>'background'), // Extension
		array('element'=>'ilayer',  'attribute'=>'src'),        // Extension
		array('element'=>'img',     'attribute'=>'dynsrc'),     // Extension
		array('element'=>'img',     'attribute'=>'lowsrc'),     // Extension
		array('element'=>'input',   'attribute'=>'dynsrc'),     // Extension
		array('element'=>'input',   'attribute'=>'lowsrc'),     // Extension
		array('element'=>'table',   'attribute'=>'background'), // Extension
		array('element'=>'td',      'attribute'=>'background'), // Extension
		array('element'=>'th',      'attribute'=>'background'), // Extension
		array('element'=>'layer',   'attribute'=>'src'),        // Extension
		array('element'=>'xml',     'attribute'=>'src'),        // Extension

		array('element'=>'button',  'attribute'=>'action'),     // Forms 2.0
		array('element'=>'datalist','attribute'=>'data'),       // Forms 2.0
		array('element'=>'form',    'attribute'=>'data'),       // Forms 2.0
		array('element'=>'input',   'attribute'=>'action'),     // Forms 2.0
		array('element'=>'select',  'attribute'=>'data'),       // Forms 2.0

		// XHTML
		array('element'=>'html',    'attribute'=>'xmlns'),

		// WML
		array('element'=>'access',  'attribute'=>'path'),       // 1.3
		array('element'=>'card',    'attribute'=>'onenterforward'),// 1.3
		array('element'=>'card',    'attribute'=>'onenterbackward'),// 1.3
		array('element'=>'card',    'attribute'=>'ontimer'),    // 1.3
		array('element'=>'go',      'attribute'=>'href'),       // 1.3
		array('element'=>'option',  'attribute'=>'onpick'),     // 1.3
		array('element'=>'template','attribute'=>'onenterforward'),// 1.3
		array('element'=>'template','attribute'=>'onenterbackward'),// 1.3
		array('element'=>'template','attribute'=>'ontimer'),    // 1.3
		array('element'=>'wml',     'attribute'=>'xmlns'),      // 2.0
		);

		$match_metas = array(
		'content-base',
		'content-location',
		'referer',
		'location',
		'refresh',
		);

		// Extract all elements
		if ( !preg_match_all( '/<([a-z][^>]*)>/iu', $text, $matches ) )
		return array( );
		$elements = $matches[1];
		$value_pattern = '=(("([^"]*)")|([^\s]*))';

		// Match elements and attributes
		foreach ( $match_elements as $match_element )
		{
			$name = $match_element['element'];
			$attr = $match_element['attribute'];
			$pattern = '/^' . $name . '\s.*' . $attr . $value_pattern . '/iu';
			if ( $name == 'object' )
			$split_pattern = '/\s*/u';  // Space-separated URL list
			else if ( $name == 'archive' )
			$split_pattern = '/,\s*/u'; // Comma-separated URL list
			else
			unset( $split_pattern );    // Single URL
			foreach ( $elements as $element )
			{
				if ( !preg_match( $pattern, $element, $match ) )
				continue;
				$m = empty($match[3]) ? $match[4] : $match[3];
				if ( !isset( $split_pattern ) )
				$urls[$name][$attr][] = $m;
				else
				{
					$msplit = preg_split( $split_pattern, $m );
					foreach ( $msplit as $ms )
					$urls[$name][$attr][] = $ms;
				}
			}
		}

		// Match meta http-equiv elements
		foreach ( $match_metas as $match_meta )
		{
			$attr_pattern    = '/http-equiv="?' . $match_meta . '"?/iu';
			$content_pattern = '/content'  . $value_pattern . '/iu';
			$refresh_pattern = '/\d*;\s*(url=)?(.*)$/iu';
			foreach ( $elements as $element )
			{
				if ( !preg_match( '/^meta/iu', $element ) ||
				!preg_match( $attr_pattern, $element ) ||
				!preg_match( $content_pattern, $element, $match ) )
				continue;
				$m = empty($match[3]) ? $match[4] : $match[3];
				if ( $match_meta != 'refresh' )
				$urls['meta']['http-equiv'][] = $m;
				else if ( preg_match( $refresh_pattern, $m, $match ) )
				$urls['meta']['http-equiv'][] = $match[2];
			}
		}

		// Match style attributes
		$urls['style'] = array( );
		$style_pattern = '/style' . $value_pattern . '/iu';
		foreach ( $elements as $element )
		{
			if ( !preg_match( $style_pattern, $element, $match ) )
			continue;
			$m = empty($match[3]) ? $match[4] : $match[3];
			//        $style_urls = extract_css_urls( $m );
			if ( !empty( $style_urls ) )
			$urls['style'] = array_merge_recursive(
			$urls['style'], $style_urls );
		}

		// Match style bodies
		if ( preg_match_all( '/<style[^>]*>(.*?)<\/style>/siu', $text, $style_bodies ) )
		{
			foreach ( $style_bodies[1] as $style_body )
			{
				//            $style_urls = extract_css_urls( $style_body );
				if ( !empty( $style_urls ) )
				$urls['style'] = array_merge_recursive(
				$urls['style'], $style_urls );
			}
		}
		if ( empty($urls['style']) )
		unset( $urls['style'] );

		foreach($urls['a']['href'] as $value)
		{

			if(preg_match('/'.$text_line.'/',$value)) { $csv_files[] = $value;  }
		}
		unlink("data.packet.current");
		return $csv_files;
	} else { echo "data packet does not exist\n"; exit; }
}



?>


