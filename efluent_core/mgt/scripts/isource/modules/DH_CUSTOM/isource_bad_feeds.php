<?
ini_set("memory_limit","-1");

$VENDORID = $efc->getConfigValue("ISource_DH", "VENDORID");
$vid = $VENDORID;
if($vid == "") { die("VENDORID IS BLANK\n"); }

echo "VENDOR: DH via InventorySource VENDORID: $VENDORID DATABASE: $DB  ---  BAD FEEDS SCRIPT\n";

$fp = fopen("$DHCC", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE\n";	exit; }

echo "\nRemoving products from bad CSV lines ....\n";

while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{
	/*
	[0] => D&H Item No.
	[1] => Vendor Part No.
	[2] => Description
	[3] => UPC Code
	[4] => Weight
	[5] => Est.Retail Price
	[6] => Price
	[7] => Any D&H Whs
	[8] => Dallas Whs
	[9] => Vendor Name
	[10] => Category
	[11] => Subcategory
	*/

	$sku = addslashes('DH'.$data[0]);
	$cost = addslashes($data[6]);
	$upc = $data[3];



	////////////////////////////////////////////////////////////////

	$con1 = 0;
	$con2 = 0;

	if(is_numeric($upc)) { $con1 = 1; }
	if($upc == "") { $con1 = 1; }

	$cost = str_replace(".","",$cost);
	$cost = str_replace(",","",$cost);
	$cost = preg_replace('/\D/','', $cost);

	if(is_numeric($cost))
	{
		$cost_strlen = strlen($cost);
		if($cost_strlen < 8) {  $con2 = 1; }
	}

	$math = $con1 + $con2;

	if($con1 == "1") { $stat = false; } else { $stat = true; }
	if($con2 == "1") { $stat = false; } else { $stat = true; }

	////////////////////////////////////////////////////////////////

	if($stat)
	{
		$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0)
		{
			list($pid) = mysql_fetch_array($qok);

			$query = "delete from vendor_product_list where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$query = "select pid from product_sourcing where vpid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($skupid) = mysql_fetch_array($qok);

			$query = "delete from product_list where id  = '$skupid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$query = "delete from product_sourcing where vpid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$counter++;

			echo "($counter)  DELETED VPID  '$pid'  PID: '$skupid', SKU: '$sku'  UPC: '$upc'  COST: '$cost'   [DEBUG:('$math')]\n";


		}
	}
}
fclose($fp);
?>