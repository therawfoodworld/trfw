<?
ini_set("memory_limit","-1");


//error_reporting(0);//no errors
$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { die("usage: $argv[0] ROOT_DIR\n"); }

$BASE = $ROOT_DIR."/mgt/scripts/isource";

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////


$MODULE_PATH = $argv[2];
if($MODULE_PATH == "") 
{
  $show = "ls -plas $ROOT_DIR/mgt/scripts/isource/modules/";  passthru($show);
  die("usage: $argv[0] ROOT_DIR MODULE_NAME\n"); 
}


if(!file_exists("modules/$MODULE_PATH/isource_specifics_$MODULE_PATH.php")) { die("\n$MODULE_PATH : Specifics needed !!  Existing Module???\n"); }



INCLUDE("auth.php");
INCLUDE("packets.php");
INCLUDE("parser.php");
INCLUDE("download.php");


$output .="\n\nMODULE: $MODULE_PATH\n\n";
echo $output."\n";


$auth = auth($MODULE_PATH);
//print $auth;


data_packet($auth,$MODULE_PATH);


$data_urls = data_urls($MODULE_PATH);
//print_r($data_urls);

download(&$data_urls,$auth,$MODULE_PATH);

merge($MODULE_PATH);

$feed = "$ROOT_DIR/mgt/scripts/isource/modules/$MODULE_PATH/isource_$MODULE_PATH.php";

if(file_exists($feed))
{ 	
	INCLUDE($feed); 
}

INCLUDE("modules/$MODULE_PATH/isource_specifics_$MODULE_PATH.php");


?>