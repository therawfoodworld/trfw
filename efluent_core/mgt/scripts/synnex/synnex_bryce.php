<?
error_reporting(0);
ini_set("memory_limit","-1");

$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }

/////////////////////////////////////////////////////////////////////////////////////////

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$MYSQL_DATABASE = $DB;
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$VENDORID = $efc->getConfigValue("synnex", "VENDORID");
$vid =  $efc->getConfigValue("synnex", "VENDORID");

$Ncreated = 0;
$Nupdated = 0;

/////////////////////////////////////////////////////////////////////////////////////////


function getmicrotime() { 	list($usec, $sec) = explode(" ",microtime()); 	return ((float)$usec + (float)$sec); }

$xtime_start = getmicrotime();


$feed_local = "$ROOT_DIR/mgt/scripts/synnex/category_list.txt";
if(!file_exists($feed_local)) {	echo "\nERROR=> MISSING FEED FILE " . $feed_local . "\n"; exit; }
echo "VENDORID: $VENDORID\n";




echo "Reseting all VID/$VENDORID stock to 0 in vendor_pricing\n";
sleep(3);
$query = "select sku,id from vendor_product_list where vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo mysql_error();exit;  }
while(list($dbsku,$pid) = mysql_fetch_array($qok))
{
	$countar++;

	$query = "update vendor_pricing set instock='0' where pid = '$pid'";
	if(!$qok22 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	//$efc->updateVendorSourcing($vpid, $VENDORID);


}




echo "Reseting all VID/$VENDORID stock to values in feed file $feed_local that exist.\n";

// CATEGORY
echo "Importing Categories...";
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

	//"002026164","Computers and Portables","Personal Computer","Celeron","43211508"
	//  0                      1                     2               3      NULL


	$catcode = $data[0];
	$top =  addslashes($data[1]);
	$sub = addslashes($data[2]);
	$subsub = addslashes($data[3]);

	$strlen = strlen($catcode);
	if($strlen > 8)
	{

		//$i++;
		//if($i < 5) { 	print_r($data);  } else {  exit; }


		if($top !="")
		{
			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$top'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) <= 0)
			{
				$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$top', '0')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$topid = mysql_insert_id();
				$msg = "$catcode ... [$topid: $top ]";
				//$array_string = "$topid:$top";
				$array_string = "$topid";
				$cat_count++;


			}
			else
			{
				list($topid) = mysql_fetch_array($qok);
				$array_string = "$topid";
				$cat_count++;

			}

		}




		if($sub !="")
		{
			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$sub'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) <= 0)
			{
				if(intval($topid) <= 0) { echo "ERROR:  blank parent for 1st sub   $top / $sub / $subsub\n"; exit;	}
				$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$sub', '$topid')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$subid = mysql_insert_id();
				$msg = "$catcode ... [$topid: $top ] => [$subid: $sub ]";
				//$array_string = "$topid:$top\t$subid:$sub";
				$array_string = "$topid,$subid";
				$cat_count++;

			}
			else
			{
				list($subid) = mysql_fetch_array($qok);
				$array_string = "$topid,$subid";
				$cat_count++;
			}

		}





		if($subsub !="")
		{
			$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$subsub'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) <= 0)
			{
				if(intval($topid) <= 0) { echo "ERROR:  blank parent for 2nd sub   $top / $sub / $subsub\n"; exit;	}
				$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$subsub', '$subid')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$subsubid = mysql_insert_id();
				$msg = "$catcode ... [$topid: $top ] => [$subid: $sub ] => [$subsubid: $subsub ]";
				//$array_string = "$topid:$top\t$subid:$sub\t$subsubid:$subsub";
				$array_string = "$topid,$subid,$subsubid";
				$cat_count++;

			}
			else
			{
				list($subsubid) = mysql_fetch_array($qok);
				$array_string = "$topid,$subid,$subsubid";
				$cat_count++;

			}

		}

		if($msg !="") { echo $msg."\n"; }

		$cat_array[$catcode] = $array_string; // reference. less queries.



	}//THE
}//END

echo "  #$cat_count\n";


// BRANDS
$fp = fopen("550958.ap", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE 550958.ap !!!\n";	exit; }

echo "Importing Brands...";

while(($data = fgetcsv($fp, 9999, "~")) !== FALSE)
{
	$catcode = $data[24];
	$strlen = strlen($catcode);
	if($strlen > 8)
	{
		$manname = addslashes($data[7]);
		$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
		else
		{
			$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$manid = mysql_insert_id();
			echo "[+] Added Brand '$manname'\n";
		}
		$brand_array[$manname] = $manid;  // reference. less queries.
		$brand_count++;
	}
}
fclose($fp);

echo "  #$brand_count\n";






/*
[0] => Trading Partner Code
[1] => Detail Record ID
* [2] => Manufacturer Part#
[3] => SYNNEX Internal Use
* [4] => SYNNEX SKU #
[5] => Status Code
* [6] => Part Description
* [7] => Manufacturer Name
[8] => SYNNEX Internal Use
* [9] => Qty on Hand (Total)
[10] => SYNNEX Internal Use
[11] => SYNNEX Internal Use
[12] => Contract Price (With Promo/Rebate)
* [13] => MSRP
[14] => Warehouse Qty on Hand
[15] => Warehouse Qty on Hand
[16] => Returnable Flag (Y/N)
[17] => Warehouse Qty on Hand
[18] => Parcel Shippable
[19] => Warehouse Qty on Hand
* [20] => Unit Cost  (w/o Promo/Rebate)
[21] => Warehouse Qty on Hand
[22] => Media Type
[23] => Warehouse Qty on Hand
* [24] => SYNNEX CAT Code
[25] => Warehouse Qty on Hand
[26] => SYNNEX Internal Use
* [27] => Ship Weight
[28] => Serialized Flag
[29] => Warehouse Qty on Hand
[30] => Warehouse Qty on Hand
[31] => Warehouse Qty on Hand
[32] => SYNNEX Reserved Use
* [33] => UPC Code
[34] => UNSPSC Code
[35] => SYNNEX Internal Use
[36] => SKU Created Date
[37] => One Source Flag
[38] => ETA Date
[39] => ABC Code
[40] => Kit/Stand Alone Flag
[41] => State GOV Price
[42] => Federal GOV Price
[43] => EDUcational Price
[44] => TAA Flag
[45] => GSA Pricing
[46] => Promotion Flag
[47] => Promotion Comment
[48] => Promotion Expiration Date
* [49] => Long Description (1)
* [50] => Long Description (2)
* [51] => Long Description (3)
[52] => Length
[53] => Width
[54] => Height
[55] => SYNNEX Reserved Use
[56] => GSA NTE Price
[57] => Platform Type
[58] => Product Description (FR)
[59] => Warehouse Qty on Hand
[60] => Warehouse Qty on Hand
[61] => SYNNEX Reserved Use
[62] => SYNNEX Reserved Use
[63] => SYNNEX Reserved Use
[64] => Replacement Sku
[65] => Minimum Order Qty
[66] => Purchasing Requirements
[67] => Gov Class
[68] => Warehouse Qty on Hand
[69] => MFG Drop Ship Warehouse QTY
[70] =>

*/


//print_r($cat_array);


$fp = fopen("550958.ap", "r");
if($fp == FALSE){	echo "ERROR OPENING FILE 550958.ap !!!\n";	exit; }

echo "Products...\n";

while(($data = fgetcsv($fp, 9999, "~")) !== FALSE)
{

	//print_r($data);

	$sku = addslashes($data[4]);
	$manpart = addslashes($data[2]);


	$catcode  = addslashes($data[24]);
	$catids = explode(",",$cat_array[$catcode]);
	//$catids = $cat_array[$catcode];
	// 0=top,1=sub,2=subsub


	$manname = addslashes($data[7]);
	$manid = $brand_array[$manname];



	$descr = addslashes($data[6]);
	//$descr = str_replace("~"," ",$descr);

	$instock = addslashes($data[9]);

	$lbs = addslashes($data[27]);

	$price = 0;

	$cost = addslashes($data[20]);

	$map = addslashes($data[13]);

	$upccode = addslashes($data[33]);

	$salesdescr = addslashes($data[49]);

	$html = addslashes($data[49].$data[50].$daya[51]);

	$strlen = strlen($catcode);
	if($strlen > 0)
	{ // We should be perfeftly safe to import ...
		//$strlen = strlen($catcode);
		//if($strlen > 9) { echo "> 9:  $strlen, $catcode\n"; }
		//if($strlen < 9) { echo "< 9:  $strlen, $catcode\n"; }

		//echo "$manid , $manname\n";
		//print_r($catids);
		//echo "\n\n";


		$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if(mysql_num_rows($qok) > 0)
		{
			list($pid) = mysql_fetch_array($qok);
			$Nupdated++;

			if($salesdescr != "")
			{
				$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}

			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$map')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


			echo "[U] $manpart PID: $pid  (STK: $instock  / CST: $cost)\n";


		}
		else
		{
			$added_dte = time();
			$Ncreated++;

			if(!is_numeric($manid)) { die("ERROR: manid is blank ... SKU: $sku MANNAME: $manname"); }

			$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku, upccode, shipweight) ";
			$query .= "values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku', '$upccode','$lbs')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$pid = mysql_insert_id();

			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$map')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			if($salesdescr != "")
			{
				$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}



			foreach($catids as $catid)
			{
				$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}


			$strlen = strlen($html);
			if($strlen > 0)
			{

				$query = "delete from vendor_product_html where vid = '$VENDORID' and id = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$query = "insert into vendor_product_html(id, vid, data) values('$pid','$vid','$html')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				echo "HTML DEBUG: ($pid) $html\n";

			}


			echo "[+]: $manpart PID: $pid  (STK: $instock  / CST: $cost)\n";

		}
	}

}






echo "updateVendorSourcing() is being applied to VID/$VENDOIRD \n";
sleep(3);
$query = "select sku,id from vendor_product_list where vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo mysql_error();exit;  }
while(list($dbsku,$pid) = mysql_fetch_array($qok))
{
	$countar++;

	$query = "update vendor_pricing set instock='0' where pid = '$pid'";
	//if(!$qok22 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	$efc->updateVendorSourcing($pid, $VENDORID);


}






// REPORT
$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
$msg = "Completed in $qtime Updated($Nupdated) Created($Ncreated)";
$efc->logAction("SUCCESS", 4, 0, 'Synnex Sync Succesful', $msg);
echo $msg . "\n";




//VENDOR SYNC WIDGET
$last_modified = filemtime("550958.ap");

$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else
{
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}


$efc->cleanVendorItems($VENDORID);


?><?mysql_close();?>
