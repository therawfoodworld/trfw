<?
ini_set("memory_limit","-1");
$ROOT_DIR = $argv[1];

if($ROOT_DIR == "")
{
	echo "usage: get_fba_order_report_status.phtml ROOT_DIR\n\n";
	exit;
}
//ini_set('display_errors',1);
//error_reporting(E_ALL^E_NOTICE);

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// eBay Provided Stuff
REQUIRE($ROOT_DIR."/mgt/ebay/classes/keys.php");
REQUIRE($ROOT_DIR."/mgt/ebay/classes/eBaySession.php");
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************************************************/
REQUIRE($ROOT_DIR."/mgt/classes/ebayclass.phtml");
$ebc = new ebayClass();
$ebc->db = $DB;
$ebc->mysqlid = $mysqlid;
$ebc->efc = $efc;
$ebc->ROOT_DIR = $ROOT_DIR;

/*
detect for all vendor feeds,    title, manpart, brand
'refurbished' , 'refurb'
mark refurb vendor_product_list
examples,
DH 	RBTK200K   cant find from gui.
CWR Electronics 	36319  cant find from gui
Wynit 	1EM005202R cant find from gui
*/
$h = fopen($ROOT_DIR."/mgt/scripts/refurbscan/endlistng.txt","a");
fwrite($h,"--------\n");
$querymain = "select id,descr,manid from product_list where refurb !='1'";
print_r($querymain."\n");
if(!$qokmain = mysql_db_query($DB, $querymain, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
while(list($pid,$descr,$manid) = mysql_fetch_array($qokmain))
{

	$knt++;
	$querya = "SELECT data FROM product_descr WHERE id='".$pid."'";
	if(!$qoka = mysql_db_query($DB, $querya, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($html) = mysql_fetch_array($qoka);
	
	$querya = "SELECT manname FROM product_manufacturers WHERE manid='".$manid."'";
	if(!$qoka = mysql_db_query($DB, $querya, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	list($manname) = mysql_fetch_array($qoka);
	
    $string = "$pid,\033[1;31m$descr\033[0m,$html,$manname\n";
	$stringUP = strtoupper($string);
	$isrefurb = false;
	$qx = "SELECT id FROM product_specs WHERE ((LOWER(dval) like 'refurb%') OR (LOWER(dval) like 'remanufact%')) AND id='$pid'";
	if(!$qokx = mysql_db_query($DB, $qx, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if (mysql_num_rows($qokx)>0) {
		$isrefurb = true;
	}
	
	if(preg_match("/REFURB/",$stringUP) || preg_match("/REMANUFAC/",$stringUP) || $isrefurb) {
		
		$kntm++; 
		//echo "($knt/\033[1;31m$kntm\033[0m),$pid,$vid,\033[1;31m$brand\033[0m,\033[1;31m$manpart\033[0m,\033[1;31m$descr\033[0m\n"; 
		//echo "($knt/\033[1;31m$kntm\033[0m),$pid,$vid,$brand,$manpart,$descr\n"; 
//		echo "($knt/$kntm),$pid,$vid,$brand,$manpart,$descr\n"; 
		
		
		
		$query = "update product_list set refurb='1' where id = '$pid'";
	    if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	    
/*----------------	END EBAY LISTINGS----------------------    */
	    
	    $query = "select ebaylistingid from ebay_listings where pid = '$pid' and active = 1";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0)
		{
			list($listingid) = mysql_fetch_array($qok);
		}
	    
	    fwrite($h,"PID:$pid----Listing:$listingid----\n");
	    
		$query = "select ecampid from ebay_listings where ebaylistingid = '$listingid'";
		if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($ebaycampaignid) = mysql_fetch_array($q2ok);
		
		$ebc->setTokenFromCampaignID($ebaycampaignid);
		
		$response = $ebc->eBayEndListing($listingid);
		$responseArray = $ebc->xml2array($response, 1);
		
		if($responseArray['EndItemResponse']['Ack']['value'] == "Success")
		{
			echo "0~$listingid~Successfully Ended";
			
			$query = "update ebay_listings set active = '0' where ebaylistingid = '$listingid'";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		
			$query = "select ecampid, pid from ebay_listings where ebaylistingid = '$listingid'";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($ebaycampaignid, $pid) = mysql_fetch_array($q2ok);
			
			$query = "select campaign_type, listid from ebay_campaigns where id = '$ebaycampaignid'";
			if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($campaign_type, $listid) = mysql_fetch_array($q2ok);
			
			
			
			if(strtoupper($campaign_type) == "LIST")
			{
				$query = "delete from list_products where pid = '$pid' and lid = '$listid'";
				if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				
				$query = "select list_title from lists where lid = '$listid'";
				if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				list($list_title) = mysql_fetch_array($q2ok);
				
				$efc->logAction("SUCCESS", "1", $pid, "eBay End Listing Removed $pid from List ($list_title)", "eBay End Listing Removed $pid from List ($list_title)"); 
				$efc->UpdatePool("1", $id);
				$efc->touchSKU($id);
				
				$query = "INSERT INTO list_products(pid,lid) VALUES('$pid','$listid')";
				if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				
				$efc->logAction("SUCCESS", "1", $pid, "eBay End Listing Added $pid to List ($list_title)", "eBay End Listing Added $pid to List ($list_title)"); 
				$efc->UpdatePool("1", $id);
				$efc->touchSKU($id);
			}
			
		}
		else 
		{
			$shortmessage = $responseArray['EndItemResponse']['Errors']['ShortMessage']['value'];
			$longmessage = $responseArray['EndItemResponse']['Errors']['LongMessage']['value'];
			$errorcode = $responseArray['EndItemResponse']['Errors']['ErrorCode']['value'];
			
			echo "1~$listingid~Error[$errorcode] - $shortmessage";
		}
	}
}
fclose($h);
?>