<?
/*
PID,   man.,   man_part_num,   title,   Description,   Specification,   Image
0       1           2            3         4              5              6


Column E as our DESCRIPTION
Column F as our HTML specs
Column G is the image URL       prefix: http://www.mindreachconsulting.com/ecomelectronics/images/
*/


$total_pid = 0;
$total_descr = 0;
$total_image = 0;
$total_html = 0;
$total_reject = 0;


$fp = fopen("$ROOT_DIR/mgt/scripts/specsimporter/data.csv", "r");
if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }


while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{

	$total_pid++;

	$pid = $data[0];

	$imgsrc = addslashes($data[6]);
	$descr = addslashes($data[4]);
	$html = addslashes($data[5]);

	jecho("(Count/#$$total_pid)  PID: $pid ... <a href=http://www.ecomelectronics.com/mgt/sm/viewsku.phtml?id=$pid>http://www.ecomelectronics.com/mgt/sm/viewsku.phtml?id=$pid</a>\n");


	$query = "select * from product_list where id = '$pid'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{

		if($imgsrc !="")
		{
			$total_image++;

			$imgsrc = 'http://www.mindreachconsulting.com/ecomelectronics/images/'.$imgsrc;
			$query = "delete from vendor_product_images where id = '$pid'";
			if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imgsrc')";
			if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			jecho("<b>ADDED IMAGE</b>: <a href=$imgsrc>$imgsrc</a>\n");
		}






		if($descr !="")
		{

			$total_descr++;

			$query = "select data from product_descr where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) > 0)
			{
				$query = "update product_descr set data='$descr' where id='$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
			else
			{
				$query = "insert into product_descr(id, data) values('$pid','$descr')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}

			jecho("<b>ADDED DESCR</b>: $descr");
		}




		if($html !="")
		{

			$total_html++;

			$query = "select data from product_html where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) > 0)
			{
				$query = "update product_html set data='$html' where id='$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
			else
			{
				$query = "insert into product_html(id, data) values('$pid','$html')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}

			jecho("<a>ADDED HTML</b>: $html\n");
		}

	
	}
	else
	{
		$total_reject++;
     	jecho("<font color=red>REJECT</font>:  PID '$pid' DOES NOT MATCH A PRODUCT RECORD IN OUR TABLES....\n");
	}

	jecho("\n\n\n");
	sleep(2);

}
?><?mysql_close();?>
