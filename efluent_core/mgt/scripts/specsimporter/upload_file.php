<pre>
<?

set_time_limit(0);

ob_implicit_flush(1);

ini_set("memory_limit","320M");

$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
if($_SERVER['DOCUMENT_ROOT'] == "") { $ROOT_DIR = $argv[1]; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);


if ($_FILES["file"]["error"] > 0)
{
	echo "ERROR: " . $_FILES["file"]["error"] . "\n";
}
else
{
	echo "UPLOAD: " . $_FILES["file"]["name"] . "\n";
	echo "TYPE: " . $_FILES["file"]["type"]."\n";
	echo "SIZE: ".($_FILES["file"]["size"] / 1024)."KB \n\n";

	if(file_exists("/tmp/data.csv")) { unlink("/tmp/data.csv"); echo "<b>NOTICE</b>: Deleting old data feed file...\n"; }
	move_uploaded_file($_FILES["file"]["tmp_name"],"/tmp/data.csv");

	echo "<b>NOTICE</b>: Stored uploaded data feed in: <a href=# onClick=\"window.open('http://www.ecomelectronics.com/mgt/scripts/specsimporter/data.csv','DATA','width=1000,height=600')\">data.csv</a>\n\n";
	echo "<b>NOTICE</b>: CLICKING A LINK WILL NOT NAVIGATE AWAY FROM THIS REPORT ....\n";
	echo "\n\n<hr>\n\n";
}


function jecho($msge) {	print($msge); ob_flush(); }

//include("import.php");


/*
PID,   man.,   man_part_num,   title,   Description,   Specification,   Image
0       1           2            3         4              5              6


Column E as our DESCRIPTION
Column F as our HTML specs
Column G is the image URL       prefix: http://www.mindreachconsulting.com/ecomelectronics/images/
*/


$total_pid = 0;
$total_descr = 0;
$total_image = 0;
$total_html = 0;
$total_reject = 0;


//$fp = fopen("$ROOT_DIR/mgt/scripts/specsimporter/data.csv", "r");
$fp = fopen("/tmp/data.csv", "r");

if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }


while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{


	$pid = $data[0];

	$imgsrc = addslashes($data[6]);
	$descr = addslashes($data[4]);
	$html = addslashes($data[5]);

	if($pid !="sku")
	{

		$total_pid++;

		jecho("(Count/#$total_pid)  PID: '$pid' ... <a href=# onClick=\"window.open('http://www.ecomelectronics.com/mgt/sm/viewsku.phtml?id=$pid','ViewSku','width=1000,height=600')\">VIEWSKU</a>\n\n");
		$query = "select * from product_list where id = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0)
		{

			if($imgsrc !="")
			{
				$total_image++;

				$imgsrc = 'http://www.mindreachconsulting.com/ecomelectronics/images/'.$imgsrc;

				$query = "delete from product_images where id = '$pid'";
				if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


				$fileloc = "/tmp/$pid.jpg";

				if(file_exists($fileloc)) { unlink($fileloc); }


				$efc->getBinaryFile($imgsrc, "", $fileloc);

				if(file_exists($fileloc))
				{

					$data = file_get_contents($fileloc);
					$data = addslashes($data);

					$itype = 0;
					
					$imageinfo = getimagesize($fileloc);
					$width = $imageinfo[0];
					$height = $imageinfo[1];

					
					$query = "insert into product_images(id, idata, itype, thumb, width, height) values('$pid', '$data', '$itype', '0', '$width', '$height')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					jecho("<b>ADDED IMAGE</b>: <a href=# onClick=\"window.open('$imgsrc','IMGSRC','width=1000,height=600')\">$imgsrc</a>\n");

					unlink($fileloc);

				}





			}






			if($descr !="")
			{

				$total_descr++;

				$query = "select data from product_descr where id = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok) > 0)
				{
					$query = "update product_descr set data='$descr' where id='$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
				else
				{
					$query = "insert into product_descr(id, data) values('$pid','$descr')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}

				jecho("<b>ADDED DESCR</b>: $descr\n");
			}




			if($html !="")
			{

				$total_html++;

				$query = "select data from product_html where id = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok) > 0)
				{
					$query = "update product_html set data='$html' where id='$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
				else
				{
					$query = "insert into product_html(id, data) values('$pid','$html')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}

				//jecho("<b>ADDED HTML</b>: $html\n");
				$strlen = strlen($html);
				jecho("<b>ADDED HTML</b>: Code length $strlen characters.\n");
			}


		}
		else
		{
			$total_reject++;
			jecho("<font color=red>REJECT</font>:  PID '$pid' DOES NOT MATCH A PRODUCT RECORD IN OUR TABLES....\n");
		}

		jecho("\n\n\n");
		//sleep(2);

	}
}
echo "<hr>";
echo "\n\n";
echo "Total PIDS: $total_pid\n";
echo "Total IMAGES: $total_image\n";
echo "Total HTML: $total_html\n";
echo "Total DECR: $total_descr\n";
echo "Total REJECTED: $total_reject\n";

?>
</pre><?mysql_close();?>
