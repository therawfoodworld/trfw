create table assetdata
(
pkid bigint NOT NULL,
ast_url_txt varchar(255),
ast_role_txt varchar(50),
ts datetime,
index(pkid)
);

create table joinassettosku
(
pkid bigint NOT NULL,
pkasset bigint NOT NULL,
pksku bigint NOT NULL,
ts datetime,
index(pkid)
);

create table sku_basic
(
pkid bigint NOT NULL,
pkproduct bigint NOT NULL,
sku varchar(50),
colorcode varchar(4),
colordescription varchar(25),
standardcolor varchar(10),
ts datetime,
index(pkid),
index(pkproduct)
);

create table promotionsbymodel
(
pkid bigint NOT NULL,
pkproduct bigint NOT NULL,
promotiontype varchar(20),
promotionname varchar(30),
promotiondescription varchar(50),
effectivedate datetime,
expirationdate datetime,
postmarkbydate datetime,
promotionvalue double(16,2),
packagedescription varchar(50), 
fulfillmenturl varchar(255),
specialoffertext varchar(255),
ts datetime,
index(pkid),
index(pkproduct)
);

create table productsorts
(
pkid bigint NOT NULL,
pkproduct bigint NOT NULL,
field varchar(35),
fvalue varchar(30),
ts datetime,
index(pkid),
index(pkproduct)
);

create table productenhancedfeatures
(
pkid bigint NOT NULL,
pkproduct bigint NOT NULL,
featurecategory varchar(30),
feature varchar(50),
ts datetime,
index(pkid),
index(pkproduct)
);

create table productbase
(
pkid bigint NOT NULL,
pkbrand bigint NOT NULL,
pkcategory bigint NOT NULL,
colorsavail varchar(25),
seriesname varchar(25),
statuscodes varchar(3), #
ts datetime,
index(pkid),
index(pkbrand),
index(pkcategory)
);

create table brand_data
(
pkid bigint NOT NULL,
commonbrandid varchar(6),
commonbrandname varchar(20),
ts datetime,
index(pkid)
);

create table map_target
(
pkproduct bigint NOT NULL,
currentactivemap double(16,2),
currentmapeffectivedate  datetime,
mintargetprice double(16,2),
maxtargetprice double(16,2),
ts datetime,
index(pkproduct)
);

create table keyfeatures
(
pkproduct bigint NOT NULL,
basicdescription varchar(35),
expandeddescription varchar(50),
keyfeature1 varchar(35),
keyfeature2 varchar(35),
keyfeature3 varchar(35),
keyfeature4 varchar(35),
keyfeature5 varchar(35),
ts datetime,
index(pkproduct)
);

drop table sortfields;
create table sortfields
(
pkid bigint NOT NULL,
pkcategory bigint NOT NULL,
field varchar(35),
units varchar(10),
ts datetime,
index(pkid)
);

drop table measurementsmetadata;
create table measurementsmetadata
(
pkmeasurementsmetadata bigint NOT NULL,
measurementname varchar(40),
unitsofmeasurement varchar(20),
ts datetime,
index(pkmeasurementsmetadata)
);

create table productmeasurements
(
pkmeasurement bigint NOT NULL,
pkproduct bigint NOT NULL,
pkmeasurementsmetadata bigint NOT NULL,
measurementvalue double(16,2),
fractionaldimensionvalue varchar(20),
ts datetime,
index(pkmeasurement)
);

create table categories
(
pkid bigint NOT NULL,
pksupercategory bigint NOT NULL,
pkclass bigint NOT NULL,
category_id varchar(5),
cmicdescription varchar(35),
consumerdescription varchar(35),
ts datetime,
index(pkid)
);

create table classes
(
pkid bigint NOT NULL,
classid varchar(35),
description  varchar(50),
ts datetime,
index(pkid)
);

create table super_categories
(
pkid bigint NOT NULL,
super_category_code varchar(2),
description varchar(255),
ts datetime,
index(pkid)
);

create table sortfieldvals
(
pkid bigint NOT NULL,
pksortfield bigint,
field varchar(35),
fvalue varchar(30),
valuedisplayorder int,
ts datetime,
index(pkid)
);