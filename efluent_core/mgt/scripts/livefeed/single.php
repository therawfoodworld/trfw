<?php

class TinyItemPhp {
	public $Sku;
}

class WSSoapClient extends SoapClient
{
    public $wsHeader = null;
    const wsSecurityXsd = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    const wsSecurityHeader =
        '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken>
                <wsse:Username>%s</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">%s</wsse:Password>
                <wsse:Nonce>%s</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%s</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>';
 
    public function setUsernameToken($username, $password)
    {
        //$ts = Zend_Date::now()->get(Zend_Date::ISO_8601);
        // if you're not using Zend_Date, you need to somehow get timestamp in below mentioned form:
        $ts = '2010-02-08T15:36:29.502Z';
        $rand = mt_rand();
        $digest = base64_encode(
            pack('H*', sha1(pack('H*', $rand)
                . pack('a*', $ts)
                . pack('a*', $password))
            )
        );
        $nonce = base64_encode(pack('H*', $rand));
        $svar = new SoapVar(sprintf(self::wsSecurityHeader, $username, $password, $nonce, $ts), XSD_ANYXML);
        $this->wsHeader = new SoapHeader(self::wsSecurityXsd, 'Security', $svar, true);
        return $this;
    }
  
    public function __soapCall($function_name,$arguments,$options=null,$input_headers=null,$output_headers=null)
    {  
        return  $result = parent::__soapCall($function_name,$arguments,$options,$this->wsHeader);
    }
}

function AddToDataBase($items) {
	// do your database inserting logic here
}

$wsdl = 'https://b2b:9443/LiveFeed.svc?WSDL';
$location = 'https://b2b:9443/LiveFeed.svc';
$sClient = new WSSoapClient($wsdl, array('trace'=>1, 'location'=>$location, 'classmap' => array('TinyItem' => 'TinyItemPhp')));

// set your real user name and password
$sClient->setUsernameToken('sales@giftjoy.net', '8Glo7R@Ze');

// get items hashes
/*$hashes = $sClient->__soapCall('GetItemHashes', array('parameters'=>null));

// compare received hashes with existing in database

// ...

//$numbers = array();
*/


$numbers[] = "01338688/";
//$numbers[] = "01338678/";


$count = 0;
/*
// it should be enough one loop, but my php is too poor to figure it out
foreach ($hashes->GetItemHashesResult->ItemHashes as $hash) {
	foreach ($hash as $subHash) {
		$tinyItem = new TinyItemPhp;
		$tinyItem->Sku = $subHash->Sku;
		$numbers[] = $tinyItem;
		
		$count++;
		if ($count == 100) break;
	}
}
*/

$chunk = $sClient->__soapCall('GetItemChunkBySkuNumbers', array('parameters'=>(array('skuNumbers' => $numbers, 'language'=>'English', 'chunkSize'=>'1', 'transactionId'=>null))));	

 $transactionId = $chunk->GetItemChunkBySkuNumbersResult->TransactionId;
 echo "downloaded1: " . count($chunk->GetItemChunkBySkuNumbersResult->Items->ItemEntity) . "\n";

 while ($chunk->GetItemChunkBySkuNumbersResult->Items->ItemEntity != null && count($chunk->GetItemChunkBySkuNumbersResult->Items->ItemEntity) > 0) {
	 echo "downloaded2: " . count($chunk->GetItemChunkBySkuNumbersResult->Items->ItemEntity) . "\n";
	 $chunk = $sClient->__soapCall('GetItemChunkBySkuNumbers', array('parameters'=>(array('skuNumbers' => null, 'language'=>'English', 'chunkSize'=>'10', 'transactionId'=>$transactionId))));	
	 AddToDataBase($chunk->GetItemChunkBySkuNumbersResult->Items);
	 print_r($chunk->GetItemChunkBySkuNumbersResult->Errors);
}

?>
<?mysql_close();?>
