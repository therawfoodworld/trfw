<? 
/*   http://www.efluent.com/ticketview.phtml?tid=1778

Order Value Range      Estimate Shipping Fee

$0.01 - $199.99         $9.99
$200.00 - $899.99       $14.99
$900.00 - $1199.99      $19.99
$1,200.00 - $1,9999.99  $29.99    <-- this is error, right?
$2,000 - $3,999.99      $49.99
$4,000 - $6,999.99      $79.99
$7000.00 - $9,999.99    $99.99
$10,000+                $149.99

*/




function checkrange($check, $high, $low) {
	if($check <= $low) return false;
	if($check >= $high) return false;
	return true;
}


function shipfee($cost, $debug)
{

	$cost = str_replace(",","",$cost);
	$cost = str_replace("$","",$cost);
	//$cost = str_replace(".00","",$cost);

	//if($cost == "0") { $die = 1; }
	//if($cost == "0.00") { $die = 1; }
	if($cost < 0.01) { $die = 1; }

	if($die > 0) { die("COST '$cost' MUST BE > 0.00 !!!"); }


	//$10,000+                $149.99
	$fee = 149.99;
	if($cost > 9999)
	{
		if($debug > 0) { echo "MATCH   [ 10,000+ ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: [ 10,000+ ]\n"; }
	}



	//$0.00 - $199.99         $9.99
	$high = 199.99;
	$low = 0.00;
	$fee = 9.99;
	if(checkrange($cost, 199.99, 0.00))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


	//$200.00 - $899.99       $14.99
	$high = 899.99;
	$low = 200.00;
	$fee = 14.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}

	//$900.00 - $1199.99      $19.99
	$high = 1199.99;
	$low = 900.00;
	$fee = 19.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


	//$1,200.00 - $1,999.99  $29.99
	$high = '1999.99';
	$low = '1200.00';
	$fee = 29.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


	//$2,000 - $3,999.99      $49.99
	$high = '3999.99';
	$low = '2000';
	$fee = 49.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;

	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


	//$4,000 - $6,999.99      $79.99
	$high = '6999.99';
	$low = '4000';
	$fee = 79.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;

	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


	//$7000.00 - $9,999.99    $99.99
	$high = '9999.99';
	$low = '7000.00';
	$fee = 99.99;
	if(checkrange($cost, $high, $low))
	{
		if($debug > 0) { echo "MATCH  $cost  [  $low / $high ]  FEE = $fee\n"; }
		return $fee;
	}
	else
	{
		if($debug > 0) { echo "The value  $cost  is not in the range: $low / $high\n"; }
	}


}
?><?mysql_close();?>
