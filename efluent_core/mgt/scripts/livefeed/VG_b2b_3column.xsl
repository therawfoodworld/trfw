<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" indent="no" omit-xml-declaration="yes" />  

  <xsl:variable name="characteristics" select="characteristics" />
  <xsl:variable name="imageHost" select="'http://localhost/'" />

  <xsl:template match="/">
    <xsl:call-template name="description" />
  </xsl:template>

  <xsl:template name="description">
    <xsl:apply-templates select="$characteristics" />
  </xsl:template>

  <!-- START: characteristics template -->
  <xsl:template match="characteristics">
    <table border="1">
      <tr>
        <td colspan="3">
          <b>Item Information:</b>
        </td>
      </tr>
      <xsl:apply-templates />

      <xsl:if test="position()=last()">
        <tr>
          <td colspan="3">
          </td>
        </tr>
      </xsl:if>

    </table>

  </xsl:template>
  <!-- END: characteristics template -->

  <xsl:template match="britain">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="up">
    <sup>
      <xsl:apply-templates />
    </sup>
  </xsl:template>

  <xsl:template match="dn">
    <sub>
      <xsl:apply-templates />
    </sub>
  </xsl:template>

  <xsl:template match="_jewelry_set | _set | _lot | _collect | _gift | _clothes">
    <tr>
      <td>
        <span>&#x20;item</span>
      </td>
      <td>
        <b>
          <xsl:value-of select="@text"/>
        </b>
      </td>
      <td></td>
    </tr>
    <xsl:apply-templates>
      <xsl:sort select="@text" />
    </xsl:apply-templates>

  </xsl:template>

  <xsl:template match="_accessories | _alarm_clock | _anklet | _antique | _belt | _belt_buckle | _band | _bar | _body_ring | _bracelet | _bracelets | _brooch | _business_card_holder | _camera | _cardholder | _calculator | _candlestick | _cardholder | _charm | _choker | _cigar_cutter | _clock | _compass | _cosmetics | _cuff_links | _doll | _dvd | _electronic_game | _golf_accessories | _earring | _earrings | _engagement_ring | _flash_light | _frame | _gemstones | _gift_box | _hairpin | _headphone | _health | _houseware | _humidor | _jewelrybox | _jewelry_box | _interchangeable_glasses_for_sunglasses | _key_ring | _kitchenware | _knife | _lariat_necklace | _laser_pointer | _letter_opener | _lighter | _loose | _menorah | _mezzuzah | _money_clip | _multi_purpose_pocket_tool | _music_cd | _musicalbox | _music_box | _nail_clipper | _necklace | _necklaces | _notebook | _organizer | _paintings | _parfume | _pen | _pendant | _purse | _radio | _ring | _rings | _thermometer | _tie | _umbrella | _scarf | _single_earring | _single_earrings | _slotmashine | _stationery | _statue | _supplies | _toe_ring | _torch | _toy | _tray | _vase | _votive | _wallet | _watch | _watches | _tie_clip | _fragrance | _box | _sunglasses | _barrel | _bookend | _bottle_stopper | _bowl | _candelabra | _christmas_ornament | _decanter | _figurine | _flute | _goblet | _mug | _olympic_souvenir | _perfume_bottle | _pitcher | _plate | _saucer | _vase | _wall_clock">

    <xsl:apply-templates select="_hidden_keyword" />

    <xsl:if test="name(..) != 'characteristics'">
      <tr>
        <td colspan="3">&#x20;</td>
      </tr>
    </xsl:if>
    <tr>
      <td>
        <span>&#x20;Product Type</span>
      </td>
      <td>
        <b>
          <xsl:choose>
            <xsl:when test="name()='_engagement_ring'">Ring</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@text"/>
            </xsl:otherwise>
          </xsl:choose>
        </b>
      </td>
      <td></td>
    </tr>

    <xsl:apply-templates select="_brand | _brand_jewelry | _made_in | _movement | _condition | _feature | _functions | _make | _model | _serial_number | _age_group | _quantity" />

    <xsl:apply-templates select="_material" />

    <xsl:apply-templates select="_total_wgt" />

    <xsl:if test="_gemstone">
      <xsl:call-template name="TotalGemWeight" />
    </xsl:if>

    <xsl:apply-templates select="_glass_shape | _glass_material | _earring_lock | _necklace_lock | _bracelet_lock | _choker_lock | _watch_case | _watch_bracelet | _bezel | _dial | _jewels | _interchangeable | _pendant_height | _pendant_width | _pendant_length" />

    <xsl:apply-templates select="_gemstone">
      <xsl:sort select="@text" order="descending" />
    </xsl:apply-templates>

    <xsl:if test="_style | _length | _max_length | _min_length | _gender | _dimensions | _height | _width | _diameter | _capacity | _product | _comes_with">
      <tr>
        <td colspan="3">
          <b>Other Information:</b>
        </td>
      </tr>
    </xsl:if>

    <xsl:apply-templates select="_style | _length | _max_length | _min_length | _gender | _dimensions | _height | _width | _diameter | _capacity | _product | _comes_with">
      <xsl:sort select="@text" order="descending" />
    </xsl:apply-templates>

    <xsl:apply-templates select="_strap_length" />

    <xsl:if test="name()='_ring' or name()='_engagement_ring' or name()='_rings'">

    </xsl:if>

  </xsl:template>

  <xsl:template match="_gemstone">
    <xsl:for-each select="child::node()">
      <xsl:if test="contains(name(.), '_ctr')">
        <xsl:choose>
          <xsl:when test="name(.) = '_diamond_ctr'">
            <tr>
              <td colspan="3">
                <b>
                  Center Diamond info:
                </b>
              </td>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr>
              <td colspan="3">
                <b>
                  Center&#x20;<xsl:value-of select="parent::node()/@text" /> info:
                </b>
              </td>
            </tr>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:variable name="note">
          <xsl:choose>
            <xsl:when test="name(.) = '_diamond_ctr'">
              The Most Brilliant of the gemstones. Diamond jewelry is Treasured as a symbol of Everlasting Love and Perfection
            </xsl:when>
            <xsl:when test="name(.) = '_emerald_ctr'">
              The one of the Most Prized of all gemstones. Has a Rich spring green color that symbolizes Growth and Fertility
            </xsl:when>
            <xsl:when test="name(.) = '_ruby_ctr'">
              Prized by Kings throughout the ages, the Precious Ruby, one of the Rarest of gems, is a symbol of Happiness and Good Fortune
            </xsl:when>
            <xsl:when test="name(.) = '_sapphire_ctr'">
              Known for its Brilliance, Durability and Deep Celestial blue hue. Sapphire is a symbol of Faith and Goodness
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
        </xsl:variable>

        <tr>
          <td>
            <span>
              &#x20;<xsl:value-of select="parent::node()/@text" />
            </span>
          </td>
          <td>
            <b>
              <xsl:if test="_gemtype">
                <xsl:value-of select="_gemtype" />&#x20;
              </xsl:if>
              <xsl:value-of select="@text"/>
            </b>
          </td>
          <td>
            <xsl:copy-of select="$note" />
          </td>
        </tr>
        <xsl:apply-templates select="_ctw" />
        <xsl:apply-templates select="_quantity" />
        <xsl:apply-templates select="_dimensions" />
        <xsl:apply-templates select="_diameter" />
        <xsl:apply-templates select="_shape" />
        <xsl:apply-templates select="_quantity-ctw" />
        <xsl:apply-templates select="_color" />
        <xsl:apply-templates select="_clarity" />
        <xsl:apply-templates select="_color-clarity" />
        <xsl:apply-templates select="_origin" />
      </xsl:if>
    </xsl:for-each>

    <!-- Separate CTR and other gemstomes-->
    <xsl:for-each select="child::node()">
      <xsl:if test="not (contains(name(.), '_ctr'))">
        <xsl:if test="not(contains(name(parent::node()/parent::node()), '_watch_bracelet') or contains(name(parent::node()/parent::node()), '_bezel') or contains(name(parent::node()/parent::node()), '_dial'))">
          <tr>
            <td colspan="3">
              <b>
                <xsl:value-of select="parent::node()/@text" /> info:
              </b>
            </td>
          </tr>
        </xsl:if>

        <xsl:variable name="note">
          <xsl:choose>
            <xsl:when test="name(.) = '_diamond'">
              The Most Brilliant of the gemstones. Diamond jewelry is Treasured as a symbol of Everlasting Love and Perfection<!--C -  Exceptionally hard and durable, has High Refractivity and Brilliance.-->
            </xsl:when>
            <xsl:when test="name(.) = '_amethyst'">
              Amethysts have been coveted by Royalty throughout the ages as symbols of Wisdom, Strength and Confidence<!--SiO<sub>2</sub> - type of Quartz-->
            </xsl:when>
            <xsl:when test="name(.) = '_citrine'">
              Sunny Citrine is Popular for the Flattering Glow it casts on every complexion. Citrine symbolizes Truth and Integrity<!--SiO<sub>2</sub> - type of Quartz-->
            </xsl:when>
            <xsl:when test="name(.) = '_aquamarine'">
              In ancient times it was thought to be capable of preserving Youth and Health. Symbolizes Peace and Tranquility<!--Be<sub>3</sub>Al<sub>2</sub>SiO<sub>6</sub> - type of Beryl-->
            </xsl:when>
            <xsl:when test="name(.) = '_emerald'">
              The one of the Most Prized of all gemstones. Has a Rich spring green color that symbolizes Growth and Fertility<!--Be<sub>3</sub>Al<sub>2</sub>SiO<sub>6</sub> - type of Beryl-->
            </xsl:when>
            <xsl:when test="name(.) = '_goldstone'">
              Goldstone is said to be an Uplifting stone, which helps reduce tension. It is believed to Attract Positive Energies
            </xsl:when>
            <xsl:when test="name(.) = '_garnet'">
              Durable Brilliant gemstone. Was adored by ancient Egyptian Goddess Isis, symbolize Faith, Truth and Friendship<!--Mg<sub>3</sub>Al<sub>2</sub>Si<sub>3</sub>O<sub>12</sub-->
            </xsl:when>
            <xsl:when test="name(.) = '_peridot'">
              Ranges in color from light yellow-green to deep olive. Admired for its Sunlit Glow. Symbolizes Warmth and Charity<!--Mg<sub>2</sub>SiO<sub>4</sub> - the Most Desired member of the Olivine group-->
            </xsl:when>
            <xsl:when test="name(.) = '_ruby'">
              Prized by Kings throughout the ages, the Precious Ruby, one of the Rarest of gems, is a symbol of Happiness and Good Fortune<!--Al<sub>2</sub>O<sub>3</sub> - variety of Corundum-->
            </xsl:when>
            <xsl:when test="name(.) = '_sapphire'">
              Known for its Brilliance, Durability and Deep Celestial blue hue. Sapphire is a symbol of Faith and Goodness<!--Al<sub>2</sub>O<sub>3</sub> - variety of Corundum-->
            </xsl:when>
            <xsl:when test="name(.) = '_topaz'">
              Today is one of the most popular precious stones. Symbolizes Strength and Intelligence<!--Al<sub>2</sub>SiO<sub>4</sub>F<sub>2</sub> - Natural aluminium silicate-->
            </xsl:when>
            <xsl:when test="name(.) = '_pearl'">
              Cultured <xsl:value-of select="_pearl_type" /> Pearl. Extremely Popular in fine jewelry for its Classic, Pure and Quiet Beauty
            </xsl:when>
            <!-- Added gemstones notes -->
            <xsl:when test="name(.) = '_abalone_shell'">
              CaCO<sub>3</sub> - Structured Calcium Carbonate
            </xsl:when>
            <xsl:when test="name(.) = '_agate'">
              SiO<sub>2</sub> - type of Quartz
            </xsl:when>
            <xsl:when test="name(.) = '_alexandrite'">
              BeAl<sub>2</sub>O<sub>4</sub> - variety of Chrysoberyl
            </xsl:when>
            <xsl:when test="name(.) = '_amazonite'">
              KAlSi<sub>3</sub>O<sub>8</sub> - variety of Green Microcline Mineral
            </xsl:when>
            <xsl:when test="name(.) = '_amber'">
              Fossilized tree resin of Ancient pine trees
            </xsl:when>
            <xsl:when test="name(.) = '_carnelian'">
              SiO<sub>2</sub> - type of Quartz
            </xsl:when>
            <xsl:when test="name(.) = '_fluorite'">
              CaF<sub>2</sub> - Calcium Fluoride
            </xsl:when>
            <xsl:when test="name(.) = '_hematite'">
              Fe<sub>2</sub>O<sub>3</sub> - Iron Oxide. Silvery, Shiny Opaque Stone
            </xsl:when>
            <xsl:when test="name(.) = '_iolite'">
              Mg<sub>2</sub>Al<sub>3</sub>O - Magnesium Aluminum Silicate. Has an Extraordinary optical property - changes color depending on light angle
            </xsl:when>
            <xsl:when test="name(.) = '_jade'">
              NaAl(SiO<sub>3</sub>)<sub>2</sub> - Opaque Gemstone ranging in color from dark green to almost white
            </xsl:when>
            <xsl:when test="name(.) = '_jadeite'">
              NaAl(SiO<sub>3</sub>)<sub>2</sub> - Silicate of Sodium and Aluminum
            </xsl:when>
            <xsl:when test="name(.) = '_jasper'">
              SiO<sub>2</sub> - type of Quartz
            </xsl:when>
            <xsl:when test="name(.) = '_lapis_lazuli'">
              Na<sub>3</sub>CaAl<sub>3</sub>Si<sub>3</sub>O<sub>12</sub>S - Valued for its Deep Blue Color
            </xsl:when>
            <xsl:when test="name(.) = '_malachite'">
              Cu<sub>2</sub>CO<sub>3</sub>(OH)<sub>2</sub> - Hydrous Copper Carbonate
            </xsl:when>
            <xsl:when test="name(.) = '_moonstone'">
              KAlSi<sub>3</sub>O<sub>8</sub>
            </xsl:when>
            <xsl:when test="name(.) = '_opal'">
              Known for its Dazzling, Fiery combination of colors. Opal is a symbol of Hope and Inspiration<!--SiO<sub>2</sub> - nH<sub>2</sub>O - Hydrated Silicon Dioxide-->
            </xsl:when>
            <xsl:when test="name(.) = '_quartz'">
              SiO<sub>2</sub> - Silicon Dioxide
            </xsl:when>
            <xsl:when test="name(.) = '_rubelite'">
              Variety of Tourmaline. Price for Rubellite is approximately Two Times Higher than for brownish red or pink Tourmalines
            </xsl:when>
            <xsl:when test="name(.) = '_rhodolite'">
              Blend of Pyrope and Almandine Garnets. Stones are generally not treated
            </xsl:when>
            <xsl:when test="name(.) = '_sodolite'">
              Na<sub>4</sub>Al<sub>3</sub>Si<sub>3</sub>O<sub>12</sub>Cl - Chloric Sodium Aluminum Silicate
            </xsl:when>
            <xsl:when test="name(.) = '_tigers_eye'">
              Variety of SiO<sub>2</sub> Crystalline Quartz formed by alteration of an asbestos mineral
            </xsl:when>
            <xsl:when test="name(.) = '_tourmaline'">
              NaLi<sub>3</sub>Al<sub>6</sub>B<sub>3</sub>Si<sub>6</sub>(OH)<sub>4</sub> - mineral of Complex and Variable composition
            </xsl:when>
            <xsl:when test="name(.) = '_turquoise'">
              [CuAl<sub>6</sub>(PO<sub>4</sub>)<sub>4</sub>(OH)<sub>8</sub>*4H<sub>2</sub> O] - has a Feeble, Faintly Waxy Lustre
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
        </xsl:variable>

        <tr>
          <td>
            <span>
              &#x20;<xsl:value-of select="parent::node()/@text" />
            </span>
          </td>
          <td>
            <b>
              <xsl:if test="contains(name(.), '_pearl')">
                Cultured <xsl:value-of select="_pearl_type/." />
              </xsl:if>
              <xsl:if test="_gemtype">
                <xsl:value-of select="_gemtype" />
              </xsl:if>
              <xsl:value-of select="' '" />
              <xsl:value-of select="@text"/>
            </b>
          </td>
          <td>
            <xsl:copy-of select="$note" />
          </td>
        </tr>
        <xsl:apply-templates select="_ctw" />
        <xsl:apply-templates select="_quantity" />
        <xsl:apply-templates select="_dimensions" />
        <xsl:apply-templates select="_diameter" />
        <xsl:apply-templates select="_shape" />
        <xsl:apply-templates select="_quantity-ctw" />
        <xsl:apply-templates select="_color" />
        <xsl:apply-templates select="_clarity" />
        <xsl:apply-templates select="_color-clarity" />
        <xsl:apply-templates select="_origin" />
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="_pearl_type">
    <xsl:value-of select="." />&#x20;
  </xsl:template>

  <xsl:template match="_watch_bracelet | _watch_case | _bezel | _dial">
    <tr>
      <td colspan="3" >
        <b>
          <xsl:value-of select="@text" />:
        </b>
      </td>
    </tr>
    <xsl:apply-templates />
    <tr>
      <td colspan="3"></td>
    </tr>
  </xsl:template>

  <xsl:template match="_material">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="_watch_bracelet/_material | _bezel/_material | _dial/_material">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="_abalone_shell | _aluminium | _base_metal | _bronze | _bronzite | _ceramic | _enamel | _fluff_velvet_coated_plastic | _genuine_ostrich_leather | _genuine_crocodile_leather | _genuine_python_leather | _genuine_stingray_leather | _gold_plated_base_metal | _gold_plated_silver | _gold_plated_stainless_steel | _gold_plated_titanium | _gold_stainless_steel | _gold_and_sterling_silver | _ivory | _leather | _metal | _mink | _murano_glass | _pewter | _silver | _platinum | _platinum_and_gold | _platinum_coated_sterling_silver | _polyurethane | _ruber | _rubber | _stainless_steel | _stainless_steel_with_solid_gold_inlay | _sterling_silver | _sterling_silver_with_solid_gold_inlay | _pearl | _resin | _titanium | _titanium_with_solid_gold_inlay | _titanium_with_solid_silver_inlay | _mother_of_pearl | _tungsten | _tungsten_with_silver_inlay | _vinyl | _wool | _cashmere | _suede | _satin">

    <xsl:variable name="val">
      <xsl:value-of select="translate(./@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="parent::node()/@text" />
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/>
        </b>
      </td>
      <td>
          <xsl:choose>
            <xsl:when test="$val = 'silver'">
              Ag
            </xsl:when>
            <xsl:when test="$val = 'platinum'">
              Pt
            </xsl:when>
            <xsl:otherwise>
              &#x20;
            </xsl:otherwise>
          </xsl:choose>        
      </td>
    </tr>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="_gold | _gold_foil">

    <xsl:variable name="val">
      <xsl:value-of select="translate(_quality, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>

    <xsl:variable name="note">
      <xsl:if test="not(contains(name(), 'foil'))">
        <xsl:choose>
          <xsl:when test="contains($val, '10K')">
            Contains 41.7% Pure <xsl:value-of select="@text" />
          </xsl:when>
          <xsl:when test="contains($val, '12K')">
            Contains 50% Pure <xsl:value-of select="@text" />
          </xsl:when>
          <xsl:when test="contains($val, '14K')">
            Contains 58.5% Pure <xsl:value-of select="@text" />
          </xsl:when>
          <xsl:when test="contains($val, '18K')">
            Contains 75% Pure <xsl:value-of select="@text" />
          </xsl:when>
          <xsl:when test="contains($val, '24K')">
            ~ 100% Pure <xsl:value-of select="@text" />
          </xsl:when>
          <xsl:otherwise />
        </xsl:choose>
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="contains($val, '14')">
          <td>
            <xsl:value-of select="concat(_quality, ' ', _color, ' ', @text)" />
          </td>
        </xsl:when>
        <xsl:when test="contains($val, '18')">
          <td>
            <xsl:value-of select="concat(_quality, ' ', _color, ' ', @text)" />
          </td>
        </xsl:when>
        <xsl:when test="contains($val, '24')">
          <td>
            <xsl:value-of select="concat(_quality, ' ', _color, ' ', @text)" />
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="concat(_quality, ' ', _color, ' ', @text)" />
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="parent::node()/@text" />
        </span>
      </td>
      <xsl:copy-of select="$col2" />
      <td>
        <xsl:value-of select="$note" />
      </td>
    </tr>
    <xsl:apply-templates select="_item_wgt" />
  </xsl:template>

  <xsl:template match="_color">

    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>

    <xsl:variable name="note">
      <xsl:choose>
        <xsl:when test="name(..) = '_gold'">
          <xsl:choose>
            <xsl:when test="contains($val, 'WHITE')">
              Added Palladium or Nickel creates White Gold alloy
            </xsl:when>
            <xsl:when test="contains($val, 'YELLOW')">
              Silver and Copper produces yellow tones of Gold
            </xsl:when>
            <xsl:when test="contains($val, 'PINK') or contains($val, 'ROSE')">
              Copper is added to create Pink and Rose tones of Gold alloys
            </xsl:when>
            <xsl:when test="contains($val, 'GREEN')">
              Green shades of Gold are created by adding Silver
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
        </xsl:when>
        <xsl:when test="name(..) = '_diamond' or name(..) = '_diamond_ctr'">
          <xsl:call-template name="GemColorDetect">
            <xsl:with-param name="gemcolor" select="$val" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise />
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="(name(..) = '_diamond'  or name(..) = '_diamond_ctr') and string-length($val) &lt; 6">
          <xsl:choose>
            <xsl:when test="contains($val, 'D') or contains($val, 'E') or contains($val, 'F') or contains($val, 'G') or contains($val, 'H')">
              <td>
                <xsl:value-of select="$val"/>
              </td>
            </xsl:when>
            <xsl:when test="contains($val, 'I') or contains($val, 'J')">
              <td>
                <xsl:value-of select="$val"/>
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td>
                <b>
                  <xsl:value-of select="$val"/>
                </b>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
      <td>
        <span>&#x20;color</span>
      </td>
      <xsl:copy-of select="$col2"/>
      <td>
        <xsl:value-of select="$note" />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="_quantity-ctw">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <xsl:variable name="val2">
      <xsl:value-of select="substring-after($val, '/')" />
    </xsl:variable>

    <xsl:variable name="col22">
      <xsl:choose>
        <xsl:when test="$val2 = '' or $val2 = '' or contains($val2, 'may vary') or $val2 != number($val2)">
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val2 &lt; 0.25">
          <td>
            <b>
              <xsl:value-of select="$val"/>ctw
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val2 &gt;= 0.25 and $val2 &lt; 0.75">
          <td>
            <xsl:value-of select="$val"/>ctw
          </td>
        </xsl:when>
        <xsl:when test="$val2 &gt;= 0.75">
          <td>
            <xsl:value-of select="$val"/>ctw
          </td>
          <td bgcolor="#cefed2">
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val"/>ctw
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="note2">
      <xsl:if test="$val2 = number($val2)">
        ~ <xsl:value-of select="format-number($val2 * 0.2, '#0.00')" />g
      </xsl:if>
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <xsl:copy-of select="$col22" />
      <td>
        <xsl:value-of select="$note2" />
      </td>
    </tr>
  </xsl:template>

  <!--Tag separation - next 3 templates. _color already exists -->

  <xsl:template match="_quantity">
    <xsl:choose>
      <xsl:when test="../_ctw" />
      <xsl:otherwise>
        <xsl:variable name="val">
          <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
        </xsl:variable>
        <tr>
          <td>
            <span>&#x20;quantity</span>
          </td>
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
          <td></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="_ctw">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <xsl:variable name="val2">
      <xsl:if test="../_quantity">
        <xsl:value-of select="translate(../_quantity, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
        <xsl:value-of select="'/'" />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="$val = '' or contains($val, 'may vary') or $val != number($val)">
          <td>
            <b>
              <xsl:value-of select="$val2"/>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &lt; 0.25">
          <td>
            <b>
              <xsl:value-of select="$val2"/><xsl:value-of select="$val"/>ctw
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 0.25 and $val &lt; 0.75">
          <td>
            <xsl:value-of select="$val2"/><xsl:value-of select="$val"/>ctw
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 0.75">
          <td>
            <xsl:value-of select="$val2"/><xsl:value-of select="$val"/>ctw
          </td>
          <td bgcolor="#cefed2">
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val2"/><xsl:value-of select="$val"/>ctw
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="note">
      <xsl:if test="$val = number($val)">
        ~ <xsl:value-of select="format-number($val * 0.2, '#0.000')" />g
      </xsl:if>
    </xsl:variable>

    <tr>
      <td>
        <span>&#x20;quantity/ctw</span>
      </td>
      <xsl:copy-of select="$col2" />
      <td>
        <xsl:value-of select="$note" />
      </td>
    </tr>

  </xsl:template>

  <xsl:template match="_clarity">

    <xsl:variable name="val">
      <xsl:choose>
        <xsl:when test="contains(translate(node(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'ENHANCED')">
          <xsl:value-of select="." />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="translate(node(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="note">
      <xsl:choose>
        <xsl:when test="name(..) = '_diamond' or name(..) = '_diamond_ctr'">
          <xsl:choose>
            <xsl:when test="contains($val, 'I') and not(contains($val, 'S')) and not(contains($val, 'F'))">
              Visible Natural Characteristics
            </xsl:when>
            <xsl:when test="contains($val, 'SI')">
              Eye Clean<!--Difficult to find inclusions without 10x magnification-->
            </xsl:when>
            <xsl:when test="contains($val, 'VS')">
              Super Clean Stone. Inclusions Not visible at 10x magnification
            </xsl:when>
            <xsl:when test="contains($val, 'IF')">
              Super Clean Stone. Nearly Flawless. Inclusions Not visible at 10x magnification
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
          <xsl:if test="contains(translate(node(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 'ENHANCED')">
            &#x20;- New Lasering techniques used to improve clarity
          </xsl:if>
        </xsl:when>
        <xsl:otherwise />
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="contains($val, 'I') and not(contains($val, 'S')) and not(contains($val, 'F'))">
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:when>
        <xsl:when test="contains($val, 'SI')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:when test="contains($val, 'VS')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:when test="contains($val, 'IF')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
      <td>
        <span>&#x20;clarity</span>
      </td>
      <xsl:copy-of select="$col2" />
      <td>
        <xsl:value-of select="$note" />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="_dimentions | _dimensions">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<!--xsl:value-of select="@text" /-->dimensions
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/>
        </b>
      </td>
      <td ></td>
    </tr>
  </xsl:template>

  <xsl:template match="_origin">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/>
        </b>
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template name="origin">
    <tr>
      <td>
        <span>&#x20;origin</span>
      </td>
      <td>
        <b>Imported</b>
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_interchangeable" />

  <xsl:template match="_item_wgt">
    <xsl:variable name="val">
      <xsl:value-of select="." />
    </xsl:variable>
    <xsl:variable name="note">
      <xsl:if test="$val = number($val)">
        ~ <xsl:value-of select="format-number($val * 0.0321543408, '#0.00')" />oz
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="$val &lt; 5">
          <td>
            <b>
              <xsl:value-of select="$val"/>g
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 5 and $val &lt; 15">
          <td>
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 15">
          <td>
            <xsl:value-of select="$val"/>g
          </td>
          <td bgcolor="#cefed2">
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$val != ''">
        <tr>
          <td>
            <span>&#x20;weight</span>
          </td>
          <xsl:copy-of select="$col2" />
          <td>
            <xsl:value-of select="$note" />
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template match="_total_wgt">
    <xsl:variable name="val">
      <xsl:value-of select="." />
    </xsl:variable>
    <xsl:variable name="note">
      <xsl:if test="$val = number($val)">
        ~ <xsl:value-of select="format-number($val * 0.0321543408, '#0.00')" />oz
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="$val &lt; 5">
          <td>
            <b>
              <xsl:value-of select="$val"/>g
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 5 and $val &lt; 15">
          <td>
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 15">
          <td>
            <xsl:value-of select="$val"/>g
          </td>
          <td bgcolor="#cefed2">
            <xsl:value-of select="$val"/>g
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$val != ''">
        <tr>
          <td>
            <span>&#x20;total item weight</span>
          </td>
          <xsl:copy-of select="$col2" />
          <td>
            <xsl:value-of select="$note" />
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template name="TotalGemWeight">
    <xsl:variable name="field">
      <xsl:choose>
        <xsl:when test="count(_gemstone/_diamond | _gemstone/_diamond_ctr) = count(_gemstone/child::node())">total diamond carat weight</xsl:when>
        <xsl:otherwise>total gemstone carat weight</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="val">
      <xsl:choose>
        <xsl:when test="sum(_gemstone/child::node()/_ctw) &gt; 0">
          <xsl:value-of select="sum(_gemstone/child::node()/_ctw)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="note">
      <xsl:if test="$val = number($val)">
        ~ <xsl:value-of select="format-number($val * 0.0321543408 * 0.2, '#0.00')" />oz
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="$val = '' or $val != number($val)">
          <td>
            <b>
              <xsl:value-of select="format-number($val, '#0.00')"/>
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &lt; 0.25">
          <td>
            <b>
              <xsl:value-of select="format-number($val, '#0.00')"/>ctw
            </b>
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 0.25 and $val &lt; 0.75">
          <td>
            <xsl:value-of select="format-number($val, '#0.00')"/>ctw
          </td>
        </xsl:when>
        <xsl:when test="$val &gt;= 0.75">
          <td>
            <xsl:value-of select="format-number($val, '#0.00')"/>ctw
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="format-number($val, '#0.00')"/>ctw
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$val != '' and $val = number($val)">
        <tr>
          <td>
            <span>
              &#x20;<xsl:value-of select="$field" />
            </span>
          </td>
          <xsl:copy-of select="$col2" />
          <td>
            <xsl:value-of select="$note" />
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template match="_color-clarity">
    <xsl:choose>
      <xsl:when test="contains(node(), '/')">
        <xsl:variable name="val">
          <xsl:value-of select="translate(node(), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
        </xsl:variable>

        <xsl:variable name="val1">
          <xsl:value-of select="substring-before($val, '/')" />
        </xsl:variable>

        <xsl:variable name="val2">
          <xsl:value-of select="substring-after($val, '/')" />
        </xsl:variable>

        <xsl:variable name="note1">
          <xsl:call-template name="GemColorDetect">
            <xsl:with-param name="gemcolor" select="$val1" />
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="note2">
          <xsl:choose>
            <xsl:when test="name(..) = '_diamond'">
              <xsl:choose>
                <xsl:when test="contains($val2, 'I') and not(contains($val2, 'S'))">
                  Visible Natural Characteristics
                </xsl:when>
                <xsl:when test="contains($val2, 'SI')">
                  Eye Clean<!--Difficult to find inclusions without 10x magnification-->
                </xsl:when>
                <xsl:when test="contains($val2, 'VS')">
                  Inclusions not visible at 10x magnification
                </xsl:when>
                <xsl:otherwise />
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="col21">
          <xsl:choose>
            <xsl:when test="string-length($val1) &lt; 4">
              <xsl:choose>
                <xsl:when test="contains($val1, 'D') or contains($val1, 'E') or contains($val1, 'F') or contains($val1, 'G') or contains($val1, 'H')">
                  <td>
                    <xsl:value-of select="$val1"/>
                  </td>
                </xsl:when>
                <xsl:when test="contains($val1, 'I') or contains($val1, 'J')">
                  <td>
                    <xsl:value-of select="$val1"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td>
                    <b>
                      <xsl:value-of select="$val1"/>
                    </b>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <td>
                <b>
                  <xsl:value-of select="$val1"/>
                </b>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="col22">
          <xsl:choose>
            <xsl:when test="contains($val2, 'I') and not(contains($val2, 'S'))">
              <td>
                <b>
                  <xsl:value-of select="$val2"/>
                </b>
              </td>
            </xsl:when>
            <xsl:when test="contains($val2, 'SI')">
              <td>
                <xsl:value-of select="$val2"/>
              </td>
            </xsl:when>
            <xsl:when test="contains($val2, 'VS')">
              <td>
                <xsl:value-of select="$val2"/>
              </td>
            </xsl:when>
            <xsl:otherwise>
              <td>
                <b>
                  <xsl:value-of select="$val2"/>
                </b>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <tr>
          <td>
            <span>&#x20;color</span>
          </td>
          <xsl:copy-of select="$col21" />
          <td>
            <xsl:value-of select="$note1" />
          </td>
        </tr>
        <tr>
          <td>
            <span>&#x20;clarity</span>
          </td>
          <xsl:copy-of select="$col22" />
          <td>
            <xsl:value-of select="$note2" />
          </td>
        </tr>

      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td>
            <span>
              &#x20;<xsl:value-of select="@text" />
            </span>
          </td>
          <td>
            <b>
              <xsl:value-of select="."/>
            </b>
          </td>
          <td >   </td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- By Int. Color Grading Scale -->
  <xsl:template name="GemColorDetect">
    <xsl:param name="gemcolor" />
    <xsl:choose>
      <xsl:when test="string-length($gemcolor) = 5">
        <xsl:if test="substring($gemcolor, 3,1) = '-'">
          <xsl:variable name="first">
            <xsl:value-of select="substring($gemcolor,1,1)" />
          </xsl:variable>
          <xsl:variable name="last">
            <xsl:value-of select="substring($gemcolor,5,1)" />
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="contains('IJ', $first) and contains('IJ', $last) or contains('KL', $first) and contains('KL', $last) or contains('MNOPQRSTUVWXYZ', $first) and contains('MNOPQRSTUVWXYZ', $last)">
              <xsl:call-template name="NiceColorNames">
                <xsl:with-param name="gemcolor" select="$first" />
              </xsl:call-template>
              <xsl:value-of select="'. '" />
              <xsl:call-template name="GemColorDetect">
                <xsl:with-param name="gemcolor" select="$first" />
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="NiceColorNames">
                <xsl:with-param name="gemcolor" select="$first" />
              </xsl:call-template>
              <xsl:value-of select="'. '" />
              <xsl:call-template name="GemColorDetect">
                <xsl:with-param name="gemcolor" select="$first" />
              </xsl:call-template>
              <xsl:value-of select="' to '" />
              <xsl:call-template name="GemColorDetect">
                <xsl:with-param name="gemcolor" select="$last" />
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </xsl:when>
      <!--end 3 chars-->
      <xsl:when test="string-length($gemcolor) = 1">
        <xsl:variable name="first">
          <xsl:value-of select="substring($gemcolor,1,1)" />
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="contains('D', $first)">
            Exceptional White +
          </xsl:when>
          <xsl:when test="contains('E', $first)">
            Exceptional White
          </xsl:when>
          <xsl:when test="contains('F', $first)">
            Rare White +
          </xsl:when>
          <xsl:when test="contains('G', $first)">
            Rare White
          </xsl:when>
          <xsl:when test="contains('H', $first)">
            White
          </xsl:when>
          <xsl:when test="contains('IJ', $first)">
            Slightly Tinted White
          </xsl:when>
          <xsl:when test="contains('KL', $first)">
            Tinted White
          </xsl:when>
          <xsl:when test="contains('MNOPQRSTUVWXYZ', $first)">
            Tinted Colour
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="contains($gemcolor, 'ENHANCED')">
        Color is improved to excellence by new everlasting techniques
      </xsl:when>
      <xsl:when test="contains($gemcolor, 'C1') or contains($gemcolor, 'C2') or contains($gemcolor, 'C3') or contains($gemcolor, 'C4') or contains($gemcolor, 'C5') or contains($gemcolor, 'C6')">
        Champagne
      </xsl:when>
      <xsl:when test="contains($gemcolor, 'C7')">
        Cognac
      </xsl:when>
      <xsl:when test="not(contains($gemcolor, 'ENHANCED'))">
        Natural Fancy Color
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template name="NiceColorNames">
    <xsl:param name="gemcolor" />
    <xsl:if test="string-length($gemcolor) = 1">
      <xsl:variable name="first">
        <xsl:value-of select="substring($gemcolor,1,1)" />
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="contains('DE', $first)">River</xsl:when>
        <xsl:when test="contains('FG', $first)">Top Wesselton</xsl:when>
        <xsl:when test="contains('H', $first)">Wesselton</xsl:when>
        <xsl:when test="contains('I', $first)">Top Crystal</xsl:when>
        <xsl:when test="contains('JK', $first)">Crystal</xsl:when>
        <xsl:when test="contains('L', $first)">Top Cape</xsl:when>
        <xsl:when test="contains('MNO', $first)">Cape</xsl:when>
        <xsl:when test="contains('PQR', $first)">Light Yellow</xsl:when>
        <xsl:when test="contains('STUVWXYZ', $first)">Yellow</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template match="_quality">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'k', 'K')" />
    </xsl:variable>
    <xsl:variable name="note">
      <xsl:choose>
        <xsl:when test="name(..) = '_gold'">
          <xsl:choose>
            <xsl:when test="contains($val, '10K')">
              Contains 41.7% Pure <xsl:value-of select="../@text" />
            </xsl:when>
            <xsl:when test="contains($val, '12K')">
              Contains 50% Pure <xsl:value-of select="../@text" />
            </xsl:when>
            <xsl:when test="contains($val, '14K')">
              Contains 58.5% Pure <xsl:value-of select="../@text" />
            </xsl:when>
            <xsl:when test="contains($val, '18K')">
              Contains 75% Pure <xsl:value-of select="../@text" />
            </xsl:when>
            <xsl:when test="contains($val, '24K')">
              ~ 100% Pure <xsl:value-of select="../@text" />
            </xsl:when>
            <xsl:otherwise />
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="col2">
      <xsl:choose>
        <xsl:when test="contains($val, '14')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:when test="contains($val, '18')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:when test="contains($val, '24')">
          <td>
            <xsl:value-of select="$val"/>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td>
            <b>
              <xsl:value-of select="$val"/>
            </b>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <xsl:copy-of select="$col2" />
      <td>
        <xsl:value-of select="$note" />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="_shape">
    <xsl:variable name="val">
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <xsl:variable name="note">
      <xsl:choose>
        <xsl:when test="$val = 'round'">
          <img src="{concat($imageHost, 'round.gif')}" border="0" alt="Round Shape" />
        </xsl:when>
        <xsl:when test="$val = 'princess'">
          <img src="{concat($imageHost, 'square-princess.gif')}" border="0" alt="Princess Shape" />
        </xsl:when>
        <xsl:when test="$val = 'baguette'">
          <img src="{concat($imageHost, 'baguette.gif')}" border="0" alt="Baguette Shape" />
        </xsl:when>
        <xsl:when test="$val = 'cushion'">
          <img src="{concat($imageHost, 'cushion.gif')}" border="0" alt="Cushion Shape" />
        </xsl:when>
        <xsl:when test="$val = 'emerald'">
          <img src="{concat($imageHost, 'emerald.gif')}" border="0" alt="Emerald Shape" />
        </xsl:when>
        <xsl:when test="$val = 'trillion'">
          <img src="{concat($imageHost, 'trillion.gif')}" border="0" alt="Trillion Shape" />
        </xsl:when>
        <xsl:when test="$val = 'fancy'">
          <img src="{concat($imageHost, 'fancy.gif')}" border="0" alt="Fancy Shape" />
        </xsl:when>
        <xsl:when test="$val = 'heart'">
          <img src="{concat($imageHost, 'heart.gif')}" border="0" alt="Heart Shape" />
        </xsl:when>
        <xsl:when test="$val = 'marquise'">
          <img src="{concat($imageHost, 'marquise.gif')}" border="0" alt="Marquise Shape" />
        </xsl:when>
        <xsl:when test="$val = 'oval'">
          <img src="{concat($imageHost, 'oval.gif')}" border="0" alt="Oval Shape" />
        </xsl:when>
        <xsl:when test="$val = 'pear'">
          <img src="{concat($imageHost, 'pear.gif')}" border="0" alt="Pear Shape" />
        </xsl:when>
        <xsl:when test="$val = 'radiant'">
          <img src="{concat($imageHost, 'radiant.gif')}" border="0" alt="Radiant Shape" />
        </xsl:when>
        <xsl:otherwise />
      </xsl:choose>
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/>
        </b>
      </td>
      <td>
        <xsl:copy-of select="$note" />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="_style | _length | _max_length | _min_length | _gender">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$val != ''">
        <xsl:if test="$val !='unisex'">
          <tr>
            <td>
              <span>
                &#x20;<xsl:value-of select="@text" />
              </span>
            </td>
            <td>
              <b>
                <xsl:value-of select="$val" />
                <xsl:if test="name() = '_length' or name() = '_strap_length'">
                  <xsl:if test="not (contains($val, 'adjustable'))"> inch</xsl:if>
                </xsl:if>
              </b>
            </td>
            <td>
              <xsl:if test="$val = 'ladies'">
                <img src="{concat($imageHost, 'w.gif')}" border="0" alt="Ladies" />
              </xsl:if>
              <xsl:if test="$val = 'gentlemens'">
                <img src="{concat($imageHost, 'm.gif')}" border="0" alt="Gentlemens" />
              </xsl:if>
            </td>
          </tr>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template match="_frame/_height | _frame/_width | _strap_length | _collect/node()/_diameter | _collect/node()/_length  | _collect/node()/_height | _collect/node()/_width | _gift/node()/_diameter | _gift/node()/_length  | _gift/node()/_height | _gift/node()/_width | _scarf/_width">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/> inch
        </b>
      </td>
      <td ></td>
    </tr>
  </xsl:template>

  <xsl:template match="_diameter | _height | _width | _pendant_height | _pendant_width | _pendant_length | _purse/_length | _brooch/_length | _cuff_links/_length | _earring/_length |  _earrings/_length | _single_earring/_length | _single_earrings/_length | _pendant/_length |  _pendant/_width | _watch_case/_length">
    <xsl:variable name="val">
      <xsl:value-of select="translate(node(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <b>
          <xsl:value-of select="$val"/> mm
        </b>
      </td>
      <td >   </td>
    </tr>
  </xsl:template>

  <xsl:template match="_watch_bracelet/_style | _bezel/_style | _dial/_style | _watch_bracelet/_length | _bezel/_length | _dial/_length | _max_length | _min_length">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$val != ''">
        <tr>
          <td>
            <span>
              &#x20;<xsl:value-of select="@text" />
            </span>
          </td>
          <td>
            <b>
              <xsl:value-of select="$val" />
              <xsl:if test="contains(name(), '_length')">
                inch
              </xsl:if>
            </b>
          </td>
          <td>
            <xsl:if test="$val = 'ladies'">
              <img src="{concat($imageHost, 'w.gif')}" border="0" alt="Ladies" />
            </xsl:if>
            <xsl:if test="$val = 'gentlemens'">
              <img src="{concat($imageHost, 'm.gif')}" border="0" alt="Gentlemens" />
            </xsl:if>
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise />
    </xsl:choose>
  </xsl:template>

  <xsl:template match="_brand | _brand_jewelry">
    <xsl:variable name="val">
      <xsl:value-of select="." />
      <!--xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
		<xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" /-->
    </xsl:variable>

    <tr>
      <td>
        <span>&#x20;original brand name</span>
      </td>
      <td>
        <xsl:value-of select="$val" />
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_movement | _make">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <xsl:value-of select="$val" />
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_made_in">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
      <xsl:value-of select="translate(., 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>
    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td>
        <xsl:value-of select="$val" />
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_condition | _functions | _feature | _glass_shape | _earring_lock | _necklace_lock | _choker_lock | _bracelet_lock | _jewels | _interchangeable | _capacity | _product | _model | _serial_number">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td nowrap="nowrap">
        <b>
          <xsl:value-of select="$val" />
        </b>
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_comes_with">
    <xsl:apply-templates select="_certificate_of_authenticity | _diamond_certificate | _instruction_manual | _international_guaranty | _limited_warranty | _manufacturers_instruction_note | _operating_instructions | _warranty_card" />
  </xsl:template>

  <xsl:template match="_certificate_of_authenticity | _diamond_certificate | _instruction_manual | _international_guaranty | _limited_warranty | _manufacturers_instruction_note | _operating_instructions | _warranty_card">
    <xsl:variable name="val">
      <xsl:value-of select="translate(./@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="translate(parent::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
        </span>
      </td>
      <td nowrap="nowrap">
        <b>
          <xsl:value-of select="$val" />
        </b>
      </td>
      <td></td>
    </tr>
  </xsl:template>

  <xsl:template match="_glass_material">
    <xsl:variable name="val">
      <xsl:value-of select="translate(child::node()/@text, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
      <xsl:value-of select="translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
    </xsl:variable>

    <tr>
      <td>
        <span>
          &#x20;<xsl:value-of select="@text" />
        </span>
      </td>
      <td nowrap="nowrap">
        <b>
          <xsl:value-of select="$val" />
        </b>
      </td>
      <td>
        <xsl:choose>
          <xsl:when test="contains($val, 'sapphire crystal')">Scratch proof and one of the Hardest substances on earth, highly trusted material with its excellent chemical stability</xsl:when>
          <xsl:when test="contains($val, 'mineral glass')">Scratch resistant ordinary glass. Has good thermal properties and excellent transparency</xsl:when>
          <xsl:when test="contains($val, 'sheltered plexiglass')">A clear, lightweight type of plastic covered with a layer of synthetic sapphire</xsl:when>
          <xsl:otherwise />
        </xsl:choose>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="_age_group" /> 

</xsl:stylesheet>
