<?
ini_set("memory_limit","-1");
/*
$fp = fopen("parts735.csv", "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{
echo $data[0]."\n";
}*/

$file = file_get_contents('parts735.csv');

$array = parse_csv($file);

foreach($array as $key => $string)
{

	$manname = $array[$key][2];
	echo "manname: $manname\n";
	
}

/*
[2951] => Array
(
[0] => OSA248
[1] => OSA248
[2] => AMD
[3] => REF
[4] => 55
[5] => 55
[6] => 0
[7] => 55
[8] => 9
[9] => OSA248 AMD 2.2GHZ Processor walt
[10] => 2
[11] => 0
[12] => 0
[13] => 0
[14] => 9
[15]
*/


function parse_csv($file,$comma=',',$quote='"',$newline="\n") {

	$db_quote = $quote . $quote;

	// Clean up file
	$file = trim($file);
	$file = str_replace("\r\n",$newline,$file);

	$file = str_replace($db_quote,'&quot;',$file); // replace double quotes with &quot; HTML entities
	$file = str_replace(',&quot;,',',,',$file); // handle ,"", empty cells correctly

	$file .= $comma; // Put a comma on the end, so we parse last cell


	$inquotes = false;
	$start_point = 0;
	$row = 0;

	for($i=0; $i<strlen($file); $i++) {

		$char = $file[$i];
		if ($char == $quote) {
			if ($inquotes) {
				$inquotes = false;
			}
			else {
				$inquotes = true;
			}
		}

		if (($char == $comma or $char == $newline) and !$inquotes) {
			$cell = substr($file,$start_point,$i-$start_point);
			$cell = str_replace($quote,'',$cell); // Remove delimiter quotes
			$cell = str_replace('&quot;',$quote,$cell); // Add in data quotes
			$data[$row][] = $cell;
			$start_point = $i + 1;
			if ($char == $newline) {
				$row ++;
			}
		}
	}
	return $data;
}



?>