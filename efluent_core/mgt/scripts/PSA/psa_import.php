<?

ini_set("memory_limit","-1");
error_reporting(0);

$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$VENDORID = $efc->getConfigValue("optekaPSA", "VENDORID");
$vid =  $efc->getConfigValue("optekaPSA", "VENDORID");


function getmicrotime() { 	list($usec, $sec) = explode(" ",microtime()); 	return ((float)$usec + (float)$sec); }

$Ncreated = 0;
$Nupdated = 0;
$xtime_start = getmicrotime();



$feed_local = "$ROOT_DIR/mgt/scripts/PSA/psa.csv";
if(!file_exists($feed_local)) {	echo "\nERROR=> MISSING FEED FILE " . $feed_local . "\n"; exit; }
echo "VENDORID: $VENDORID\n";





// Category
echo "Importing Categories...\n";
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{
	$catname = addslashes($data[17]);
	if($catname == "") { $catname = "UnSorted"; }
	$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) <= 0)
	{
		$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$catname', '0')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$catid = mysql_insert_id();
		echo "[+] Added Category '$catname/".mysql_insert_id()."'\n";
	}
}
fclose($fp);








// Brands
echo "Importing Brands...\n";
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
$manname = addslashes($data[1]);
$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
else
{
	$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	echo "[+] Added Brand '$manname/".mysql_insert_id()."'\n";
}
fclose($fp);





//PRODUCTS
echo "Importing Products...\n";
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 9999, ",")) !== FALSE)
{
	/*
	[0] => Changes
	[1] => Manufacturer
	[2] => Product_Name
	[3] => Model
	[4] => Inventory
	[5] => eta
	[6] => Price
	[7] => Retail_Price
	[8] => MAP
	[9] => Weight
	[10] => Average_Freight
	[11] => Box_Dimensions
	[12] => Specials
	[13] => Long_Description
	[14] => UPC_Code
	[15] => Image
	[16] => Thumbnail
	[17] => Category
	*/


	$sku = utf8_encode(addslashes($data[3]));
	$descr = utf8_encode(addslashes($data[2]));
	$salesdescr = utf8_encode(addslashes($data[13]));
	$manname = utf8_encode(addslashes($data[1]));
	$manpart = utf8_encode(addslashes($data[3]));
	
	$instock = utf8_encode(addslashes($data[4]));

	$cost = utf8_encode(addslashes($data[6]));
	$price = utf8_encode(addslashes($data[7]));

	
	$cost = utf8_encode(addslashes(str_replace("$","",$data[6])));
	$price = utf8_encode(addslashes(str_replace("$","",$data[7])));

	$catname = addslashes($data[17]);
	if($catname == "") { $catname = "UnSorted"; }


	$upc = addslashes($data[14]);

	$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0) 	{ list($manid) = mysql_fetch_array($qok); }
	else {
		//echo "Error: $manname , $VENDORID\n"; exit;
		$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
		else
		{
			$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			echo "[+] Added Brand '$manname/".mysql_insert_id()."'\n";
			$manid = mysql_insert_id();
		}

	}

	$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	
	if(mysql_num_rows($qok) > 0) {
		list($pid) = mysql_fetch_array($qok);
		if($dupes_are_for_fags[$sku] !="1") { $Nupdated++; }
		$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		$dupes_are_for_fags[$sku] = 1;
		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$price')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		echo "VPID: $pid SKU: $sku UPDATED (STK: $instock / CST: $cost)\n";
		//$efc->vendorMarkPrice($pid);
		$efc->updateVendorSourcing($pid, $VENDORID);
		if($salesdescr != "")
		{
			$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}
	}
	else
	{

		// ADD
		$added_dte = time();
		$Ncreated++;
		$query = "insert into vendor_product_list(vid, upccode, manid, manpart, descr, added_dte, sku) values('$VENDORID', '$upc','$manid', '$manpart', '$descr', '$added_dte', '$sku')";
		echo "ADDED: $manpart (STK: $instock  / CST: $cost)\n";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$pid = mysql_insert_id();

		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$price')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if($salesdescr != "")
		{
			$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		}


		//CATMAP
		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($catid,$parentid) = mysql_fetch_array($qok);
		$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	}
}





// REPORT
$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
$msg = "Completed in $qtime Updated($Nupdated) Created($Ncreated)";
$efc->logAction("SUCCESS", 4, 0, 'PSA Sync Succesful', $msg);
echo $msg . "\n";




//VENDOR SYNC WIDGET
$last_modified = filemtime("$ROOT_DIR/mgt/scripts/PSA/psa.csv");

$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else {
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}


//$efc->cleanVendorItems($VENDORID);

$query = "delete from vendor_product_list where manpart = 'Model' and vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

$query = "delete from vendor_categories where catname = 'Category' and vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


?>
<?mysql_close();?>
