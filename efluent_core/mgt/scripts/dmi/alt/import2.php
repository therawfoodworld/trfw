<?
ini_set("memory_limit","320M");
$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
if($_SERVER['DOCUMENT_ROOT'] == "") { $ROOT_DIR = $argv[1]; }
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
$MYSQL_DATABASE = $DB;
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$VENDORID = $efc->getConfigValue("DMI", "VENDORID");
$vid =  $efc->getConfigValue("DMI", "VENDORID");

$feed_local = "$ROOT_DIR/mgt/scripts/dmi/alt/data.csv";
if(file_exists($feed_local)) { $filecheck_feed = 1; }
if($filecheck_feed !="1") { jecho("\nERROR=> MISSING FEED FILE $feed_local\n"); exit; }
function getmicrotime() {	list($usec, $sec) = explode(" ",microtime());	return ((float)$usec + (float)$sec); }

$xtime_start = getmicrotime();
$Ncreated = 0;
$Nupdated = 0;

jecho("\nDATABASE: $DB\n");
jecho("\nROOT: $ROOT_DIR\n");
jecho("\nVENDORID: $VENDORID\n\n");
jecho("\n<b>Importing Categories.....</b>\n\n");
$fp = fopen("$ROOT_DIR/mgt/scripts/dmi/alt/data.csv", "r");
if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{
	$catname = addslashes($data[0]);
	if($catname !="") {
		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if(mysql_num_rows($qok) <= 0)
		{
			$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$catname', '0')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$parentid = mysql_insert_id();
			jecho("ADDED NEW CATEGORY  $catname ...\n");
		}
	}
}
fclose($fp);

//MAN MAP
jecho("\n<b>Importing Brands.....</b>\n\n");
$fp = fopen("$ROOT_DIR/mgt/scripts/dmi/alt/BRAND_LIST.CSV", "r");
if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{
	$manname = addslashes($data[1]);
	$code = addslashes($data[0]);
	$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
	else
	{
		$query = "insert into vendor_manufacturers(manname, vid, code) values('$manname', '$VENDORID', '$code')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		jecho("ADDED NEW BRAND '$manname' ...\n");
	}
}
fclose($fp);

//Build array of image+sales_descr+title
/*
$data[1] => PN
$data[2] => MAJOR
$data[3] => MINOR
$data[4] => SHORT_DESCRIPTION
$data[5] => DESCRIPTION
$data[6] => LONG_DESCRIPTION
$data[7] => DIMENSION
$data[8] => PRODUCT_URL
$data[9] => IMG_SRC_URL
*/
jecho("\n<b>NOTICE</b>: Building Array to source Images and Sales Descriptions....</b>\n");
$fp = fopen("$ROOT_DIR/mgt/scripts/dmi/alt/SPEC_DATA.CSV", "r");
if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

	$array[$data[1]][SHORT_DESCRIPTION] = $data[4];
	$array[$data[1]][DESCRIPTION] = $data[5];
	$array[$data[1]][LONG_DESCRIPTION] = $data[6];
	$array[$data[1]][IMG_SRC_URL] = $data[9];
	if($array[$data[1]][IMG_SRC_URL] !="") { $array_img_count++; }
	if($array[$data[1]][LONG_DESCRIPTION] !="") { $array_descr_count++; }
	$array2[] = $data[1];
}
jecho("<b>NOTICE</b>: Array Generated! ($array_descr_count)/Descriptions  ($array_img_count)/Images....</b>\n\n");



/*
[0] => MAJOR CLASS
[1] => MINOR CLASS
[2] => Man ID
[3] => PN
[4] => Description
[5] => MEMO
[6] => Available
[7] => Allocated
[8] => B/O
[9] => Price
[10] => LC
[11] => 5
[12] => 10
[13] => 15
[14] => 20
[15] => 25
[16] => 30
[17] => 40
*/

jecho("\n<br><b>Importing Products.....</b>\n\n");
$fp = fopen("$ROOT_DIR/mgt/scripts/dmi/alt/data.csv", "r");
if($fp == FALSE) { jecho("ERROR OPENING FILE\n"); exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

// Little Kid Training Wheels For Jeff......
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//	"MAJOR CLASS","MINOR CLASS","Man ID","PN","Description","0","MEMO","Available","Allocated","B/O","Price","LC","5", "10","15","20","25","30","40"
//       0               1         2       3       4         5    6      7               8      9       10    11   12   13   14   15   16   17   18
if($checklight !="green") {

if($data[0] == "MAJOR CLASS") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[1] == "MINOR CLASS") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[2] == "Man ID")  {  $light[] = "green"; } else { $light[] = "red"; }
if($data[3] == "PN") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[4] == "Description")  {  $light[] = "green"; } else { $light[] = "red"; }
if($data[5] == "0")   {  $light[] = "green"; } else { $light[] = "red"; }
if($data[6] == "MEMO") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[7] == "Available")  {  $light[] = "green"; } else { $light[] = "red"; }
if($data[8] == "Allocated") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[9] == "B/O") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[10] == "Price") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[11] == "LC") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[12] == "5") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[13] == "10") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[14] == "15") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[15] == "20") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[16] == "25") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[17] == "30") {  $light[] = "green"; } else { $light[] = "red"; }
if($data[18] == "40") {  $light[] = "green"; } else { $light[] = "red"; }


print_r($light);

foreach($light as $key => $color) {	if($color == "red") { $checklight = "red"; } }
		
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
if($checklight !="red")
{		
    $sku = addslashes($data[3]);
	foreach($array2 as $check) { if($check == "$sku") { $have++;  } }
	$manpart = addslashes($data[3]);
	if($manpart !="") {
		if($sku !="PN") {
			$catname = addslashes($data[0]);
			$mancode = addslashes($data[2]);
			$descr = addslashes($data[4]);
			$instock = addslashes($data[6]);
			$map = addslashes($data[9]);
			$cost = addslashes($data[10]);
			$price = addslashes("0.00");

			$mancode = addslashes($data[2]);
			if($mancode == "AMA") { $mancode = "AMN"; }
			if($mancode == "AVAN") { $mancode = "AVA"; }
			if($mancode == "BROA") { $mancode = "BRN"; }
			if($mancode == "COBY") { $mancode = "CBY"; }
			if($mancode == "DANB") { $mancode = "DAN"; }
			if($mancode == "DANS") { $mancode = "DAN"; }
			if($mancode == "DELO") { $mancode = "DLG"; }
			if($mancode == "DIRT") { $mancode = "DIRTD"; }
			if($mancode == "ELIT") { $mancode = "ELITA"; }
			if($mancode == "ELITE") { $mancode = "ELI"; }
			if($mancode == "EURE") { $mancode = "EKA"; }
			if($mancode == "FEDD") { $mancode = "FDR"; }
			if($mancode == "FRGG") { $mancode = "FRIG"; }
			if($mancode == "FRI") { $mancode = "FRD"; }
			if($mancode == "FRIE") { $mancode = "FRD"; }
			if($mancode == "GEAC") { $mancode = "GE"; }

			$query = "select manid from vendor_manufacturers where code = '$mancode' and vid = '$VENDORID'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			if(mysql_num_rows($qok) > 0) 	{ list($manid) = mysql_fetch_array($qok); } else
			{

				jecho("\n<b>ERROR</b>: $mancode,$VENDORID,$sku DOES NOT MAP TO A BRAND !!\n");
				$query = "insert into vendor_manufacturers(manname, vid, code) values('$mancode', '$VENDORID', '$mancode')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$manid = mysql_insert_id();
				jecho("(<b>Self Correcting</b>) ADDED NEW BRAND '$mancode' ID($manid), Recovered from ERROR, continue...\n\n");
			}

			$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			if(mysql_num_rows($qok) > 0) {
				//update
				$Nupdated++;
				list($pid) = mysql_fetch_array($qok);
				$query = "delete from vendor_pricing where pid = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0.00', '$cost', '$map')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				jecho("UPDATE: VPID: $pid SKU: $sku UPDATED (STK: $instock / CST: $cost)\n");
				$STFU = $efc->updateVendorSourcing($pid, $VENDORID);

				if($array[$sku][LONG_DESCRIPTION] !="") {
					$salesdescr = addslashes($array[$sku][LONG_DESCRIPTION]);
					$TDI++;
					$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}

				if($array[$sku][IMG_SRC_URL] !="") {
					$TII++;
					$imgurl = addslashes($array[$sku][IMG_SRC_URL]);
					$query = "delete from vendor_product_images where id = '$pid'";
					if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imgurl')";
					if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}


			}
			else
			{
				// New
				$Ncreated++;
				$added_dte = time();

				$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku, upccode, shipweight) values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku', '$upccode','$shipweight')";
				jecho("ADDED: $manpart (STK: $instock  / CST: $cost)\n");
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$pid = mysql_insert_id();

				$query = "delete from vendor_pricing where pid = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '0.00', '$cost', '$map')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


				$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				list($catid,$parentid) = mysql_fetch_array($qok);
				$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


				// Images + Sales Descr

				if($array[$sku][LONG_DESCRIPTION] !="") {
					$salesdescr = addslashes($array[$sku][LONG_DESCRIPTION]);
					$TDI++;
					$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}

				if($array[$sku][IMG_SRC_URL] !="") {
					$TII++;
					$imgurl = addslashes($array[$sku][IMG_SRC_URL]);
					$query = "delete from vendor_product_images where id = '$pid'";
					if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imgurl')";
					if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
			}
		}
	}
} else {
$example = '"MAJOR CLASS","MINOR CLASS","Man ID","PN","Description","0","MEMO","Available","Allocated","B/O","Price","LC","5", "10","15","20","25","30","40"';
$endmsg = "RetailDeck Export Error: INVALID COLUMN MAP USED WHEN EXPORTED.....<br />EXAMPLE: $example<br />Please export a new XLS file following the example above and try again.";
echo $endsmg;
exit;
}

}
$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);

//VENDOR SYNC WIDGET
$last_modified = filemtime("$ROOT_DIR/mgt/scripts/dmi/alt/data.csv");

$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else {
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}
$query = "delete from vendor_product_list where sku = 'PN'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

$msg = "\n\n<h4>Completed in $qtime Updated($Nupdated) Created($Ncreated)</h4>";
$efc->logAction("SUCCESS", 4, 0, 'DMI Sync Succesful', $msg);
echo $msg . "\n";

$efc->cleanVendorItems($VENDORID);
$thetot = $Nupdated + $Ncreated;
$missingi = $thetot - $TII;
$missings = $thetot - $TDI;
echo "[<b>NOTICE</b>]: Total of <b>$have</b> relevant SKUs matching collected SPECS data with a total of <b>$thetot</b> products stored.\n";
echo "[<b>NOTICE</b>]: Inserted <b>$TII</b> Image URLs and Inserted <b>$TDI</b> Sales Descriptions\n";
echo "[<b>NOTICE</b>]: <b>$missings</b> Products missing Sales Description <b>$missingi</b> Products missing Image.\n";
?>
