<?
ini_set("display_errors","2"); 
ERROR_REPORTING(E_ALL);


$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") {
$ROOT_DIR = "/home/httpd/justbuyelectronics.com";
}

if($ROOT_DIR == "")
{
	echo "usage: merge_dmi.phtml ROOT_DIR";
	exit;	
}

$VENDORID = 35;

set_time_limit(999999);
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

function getmicrotime()
{ 
	list($usec, $sec) = explode(" ",microtime()); 
	return ((float)$usec + (float)$sec); 
}

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff


REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////
$VENDORID = $efc->getConfigValue("DMI", "VENDORID");
if($VENDORID == "") { $VENDORID = 59; }
echo "VENDORID: $VENDORID\n";





error_reporting(E_ALL);


$xtime_start = getmicrotime();

$numnewatingram = 0;
$numcreated = 0;
$numupdated = 0;
$numhidden = 0;
$numdeleted = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "Importing Brands...\n";

$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/data/BRAND_LIST.CSV", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}

while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) 
{// 
	//BRAND_CODE	BRAND_NAME
	echo $data[0]."\n";
	
	if($data[0] != "BRAND_CODE")
	{
		$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '".$data[0]."'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		
		if(mysql_num_rows($qok) <= 0)
		{
			$query = "insert into vendor_manufacturers(manname, vid, code) values('".addslashes($data[1])."', '$VENDORID', '".addslashes($data[0])."')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		}
	}
}

fclose($fp);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo "Importing Major Categories...\n";

$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/data/MAJOR_CLASS_LIST.CSV", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}

while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) 
{// 
	//MAJOR_CLASS	DESC

	if($data[0] != "MAJOR_CLASS")
	{
		$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '".$data[0]."'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		
		if(mysql_num_rows($qok) <= 0)
		{
			$query = "insert into vendor_categories(vid, catname, parentid, code) values('$VENDORID', '".addslashes($data[1])."', '0', '".addslashes($data[0])."')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		}
	}
}

fclose($fp);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "Importing Minor Categories...\n";

$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/data/MINOR_CLASS_LIST.CSV", "r");
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) 
{// 
	//MAJOR_CLASS	MINOR_CLASS		DESC
	if($data[0] != "MAJOR_CLASS")
	{
		$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '".$data[1]."'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		
		if(mysql_num_rows($qok) <= 0)
		{
			$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '".$data[0]."'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($parentid) = mysql_fetch_array($qok);
			
			if(intval($parentid) <= 0)
			{
				echo "error finding parent for ".$data[0]."\n";
				exit;
			}
			$query = "insert into vendor_categories(vid, catname, parentid, code) values('$VENDORID', '".addslashes($data[2])."', '$parentid', '".addslashes($data[1])."')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}
	}
}

fclose($fp);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo "Importing Products...\n";
//BRAND,PN,MAJOR,MINOR,SHORT_DESCRIPTION,DESCRIPTION,LONG_DESCRIPTION,DIMENSION,PRODUCT_URL,IMG_SRC_URL,IMG_FILE_NAME,IMG_SRC_URL0,IMG_FILE_NAME0,IMG_SRC_URL1,IMG_FILE_NAME1,IMG_SRC_URL2,IMG_FILE_NAME2,IMG_SRC_URL3,IMG_FILE_NAME3,IMG_SRC_URL4,IMG_FILE_NAME4,IMG_SRC_URL5,IMG_FILE_NAME5,IMG_SRC_URL6,IMG_FILE_NAME6,IMG_SRC_URL7,IMG_FILE_NAME7,IMG_SRC_URL8,IMG_FILE_NAME8,IMG_SRC_URL9,IMG_FILE_NAME9,DOCDESC0,DOC_SRC_URL0,DOC_FILE_NAME0,DOCDESC1,DOC_SRC_URL1,DOC_FILE_NAME1,DOCDESC2,DOC_SRC_URL2,DOC_FILE_NAME2,DOCDESC3,DOC_SRC_URL3,DOC_FILE_NAME3,DOCDESC4,DOC_SRC_URL4,DOC_FILE_NAME4,DOCDESC5,DOC_SRC_URL5,DOC_FILE_NAME5,DOCDESC6,DOC_SRC_URL6,DOC_FILE_NAME6,DOCDESC7,DOC_SRC_URL7,DOC_FILE_NAME7,DOCDESC8,DOC_SRC_URL8,DOC_FILE_NAME8,DOCDESC9,DOC_SRC_URL9,DOC_FILE_NAME9,FEATURE0,FEATURE1,FEATURE2,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,FEATURE11,FEATURE12,FEATURE13,FEATURE14,FEATURE15,FEATURE16,FEATURE17,FEATURE18,FEATURE19,FEATURE20,FEATURE21,FEATURE22,FEATURE23,FEATURE24
// 0    1   2     3           4             5               6              7       8          9            10           11           12
$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/data/SPEC_DATA.CSV", "r");
while(($data = fgetcsv($fp, 10000, ",", "\"")) !== FALSE) 
{// 
	if($data[0] != "BRAND")
	{
		if(strlen($data[0]) < 25) {
		$manpart = addslashes($data[1]);
		if($manpart !="") {
		$numcreated++;
		} else { echo "manpart is blank!\n"; print_r($data); exit; }
		$sku = addslashes($data[1]);
		$major = $data[2];
		$minor = $data[3];
		$productURL = addslashes($data[8]);
		$imageURL = "http://specs.retaildeck.com/CACHE/".addslashes($data[10])."?WIDTH=250";;
		
		$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '".$data[1]."'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
		
		if(mysql_num_rows($qok) <= 0)
		{
			$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '".$data[0]."'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($manid) = mysql_fetch_array($qok);
			
			if(intval($manid) <= 0)
			{
				echo "ERROR FINDING BRAND: ".$data[0]."\n";
				$query = "insert into vendor_manufacturers(manname, vid, code) values('?".addslashes($data[0])."?', '$VENDORID', '".addslashes($data[0])."')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
				$manid = mysql_insert_id();
			}
			
			$descr = addslashes($data[4]);
			$salesdescr = addslashes($data[6]);

					$added_dte = time();


			$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku) ";
			$query .= "values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$manpart')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$pid = mysql_insert_id();
			
			echo "Added $pid [$numcreated]  MPRT:$manpart MAJOR:$major MINOR:$minor BRAND:$data[0]\n";
			
			/////////////////////////
			// get the major
			$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '$major'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			
			// insert the major
			$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catid', '$VENDORID', '$major')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			// get the minor
			$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '$minor'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			
			// insert the minor
			$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catid', '$VENDORID', '$minor')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			
			//////////////////////////
			if($salesdescr != "")
			{
				$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				
				$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}

			///////////////////////////
			//  the features
			$query = "delete from vendor_features where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			for($xx = 0; $xx < 24; $xx++)
			{
				if($data[$xx] != "")
				{
					$feature = addslashes( $data[ 61 + $xx ] );
					$query = "insert into vendor_features(pid, vid, feature) values('$pid', '$VENDORID', '$feature')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
			}
			
			///////////////////////////
			//  the images
			$query = "delete from vendor_product_images where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			if($imageURL != "")
			{// insert the main Image
				$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imageURL')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
			
			for($xx = 0; $xx < 18; $xx+=2)
			{
				if($data[ 11 + $xx ] != "")
				{
					$imageURL = addslashes( $data[ 11 + $xx ] );
					$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imageURL')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
			}
			///////////////////////////
			//  the docs
			$query = "delete from vendor_product_docs where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			if($productURL != "")
			{// insert the main Image
				$query = "insert into vendor_product_docs(pid, title, link) values('$pid', 'Product URL', '$productURL')";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			}
			
			for($xx = 0; $xx < 18; $xx+=2)
			{
				if($data[31 + $xx + 1] != "")
				{
					$docname = addslashes( $data[ 31 + $xx ] );
					$docurl = addslashes( $data[ 31 + $xx + 1 ] );
					
					$query = "insert into vendor_product_docs(pid, title, link) values('$pid', '$docname', '$docurl')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				}
			}
			
			
		}
	}
 }
}

fclose($fp);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "Importing manmap...\n";
$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/data/manmap.dat", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}

$manmap = array();

while(($data = fgetcsv($fp, 1000, "`")) !== FALSE) 
{
	if($data[0] != "GROUP_ID" && $data[0] == "2000")
	{
		$manmap['FROM_MAN'][] = $data[1];
		$manmap['UNIVERSAL_MAN'][] = $data[2];
	}
}

fclose($fp);

echo "LOADED ".count($manmap['FROM_MAN'])." MANCODES\n";
sleep(2);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo "Importing Pricing...\n";

$fp = fopen($ROOT_DIR."/mgt/scripts/dmi/pn_export.csv", "r");
if($fp == FALSE)
{
	echo "ERROR OPENING FILE\n";
	exit;
}

while(($data = fgetcsv($fp, 1000, ",")) !== FALSE) 
{// 
	//MAJOR_CLASS	DESC

	if($data[0] != "MAN_CODE")
	{
		$code = $data[0];
		$sku = $data[1];
		$major = $data[2];
		$minor = $data[3];
		$instock = $data[4];
		$cost = $data[5];
		$map = $data[6];
		$descr = addslashes($data[7]);
		
		/*$query = "select vendor_product_list.id from vendor_product_list, vendor_manufacturers where 
					vendor_product_list.manid = vendor_manufacturers.manid AND
					vendor_manufacturers.code = '$code' AND
					vendor_product_list.sku = '$sku' AND
					vendor_product_list.vid = '$VENDORID' ";*/
		
		$query = "select id from vendor_product_list where sku = '$sku' and vid = '$VENDORID' ";
			
		//echo "$query\n\n";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($pid) = mysql_fetch_array($qok);
		
		if(intval($pid) > 0)
		{
			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			$query = "insert into vendor_pricing(pid, instock, cost, price, map) values('$pid', '$instock', '$cost', '0.00', '$map')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			echo "$sku UPDATED (STK: $instock / CST: $cost / MAP: $map)\n";
		}
		else 
		{
			$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '$code'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($manid) = mysql_fetch_array($qok);
			
			if(intval($manid) <= 0)
			{
				for($as = 0; $as < count($manmap['FROM_MAN']); $as++)
				{
					if($manmap['FROM_MAN'][$as] == $code)
					{
						$code = $manmap['UNIVERSAL_MAN'][$as];
						
						echo "NEWCODE (from manmap): $code\n";
						
						$query = "select manid from vendor_manufacturers where vid = '$VENDORID' and code = '$code'";
						if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
						list($manid) = mysql_fetch_array($qok);

						break;
					}
				}
				
				if(intval($manid) <= 0)
				{
					echo "ERROR FINDING BRAND: $code\n";
					$query = "insert into vendor_manufacturers(manname, vid, code) values('?".addslashes($code)."?', '$VENDORID', '".addslashes($code)."')";
					if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
					$manid = mysql_insert_id();
				}
			}
			
			$added_dte = date("Y-m-d H:i:s", time());
			
			$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku) ";
			$query .= "values('$VENDORID', '$manid', '$sku', '$descr', '$added_dte', '$sku')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$pid = mysql_insert_id();
			
			
			////////////////////////
			// get the major
			$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '$major'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			
			// insert the major
			$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catid', '$VENDORID', '$major')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			echo "MAJOR: $major => CATID: $catid VENDOR => $VENDORID\n";
			
			// get the minor
			$query = "select catid from vendor_categories where vid = '$VENDORID' and code = '$minor'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($catid) = mysql_fetch_array($qok);
			
			// insert the minor
			$query = "insert into vendor_category_map(pid, catid, vid, code) values('$pid', '$catid', '$VENDORID', '$minor')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }			
			
			echo "MINOR: $minor => CATID: $catid VENDOR => $VENDORID\n";
			
			$query = "delete from vendor_pricing where pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			$query = "insert into vendor_pricing(pid, instock, cost, price, map) values('$pid', '$instock', '$cost', '0.00', '$map')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			
			echo "$sku UPDATED (STK: $instock / CST: $cost / MAP: $map)\n";
//		    $efc->vendorMarkPrice($vpid);

			
			echo "$code - $sku NOT FOUND!!! ADDED AS: $pid\n";
			
			//exit;
		}
	}
}

fclose($fp);



$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
/*
$tstamp = date("Y-m-d H:i:s", time());

$query = "insert into action_log(ltype, type, updateid, title, tstamp, userid) 
			values('SUCCESS', '4', '0', 'Database Build Successful in $qtime seconds (UPDATED: $numupdated CREATED: $numcreated HIDDEN: $numhidden DELETED: $numdeleted)', '$tstamp', '0')";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }	
*/
?>

<?$efc->cleanVendorItems($VENDORID);?>