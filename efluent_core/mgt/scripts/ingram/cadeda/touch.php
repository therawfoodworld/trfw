<?
ini_set("memory_limit","-1");

error_reporting(0);
$ROOT_DIR = $argv[1];
if($ROOT_DIR == ""){ echo "usage: merge_ingram.phtml ROOT_DIR\n";  exit;}

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

$VENDORID = $efc->getConfigValue("Ingram", "VENDORID");
$vid = $efc->getConfigValue("Ingram", "VENDORID");
if($vid == "") { echo "ERROR: vendorid is blank!\n"; }

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);


$fp = fopen($ROOT_DIR."/mgt/scripts/ingram/PRICE.TXT", "r");
if($fp == FALSE) {
	echo "ERROR: Check file path.\n";
	exit;
}


echo "Importing Products...\n";
while(($data = fgetcsv($fp, 1000, ",")) !== FALSE)
{
	//print_r($data);
	//exit;

	//"A",    "BA0011",    "U206",     "OPEN VALUE SUBSCRIPTION  ",       "1YR OFFICESTD ALNG LICSAPK OLV ",          "D AP  ",          0000000000000149.00,      "021-09108           ",000000.00,"             ",0000.00,0000.00,0000.00," ",0000000000000115.76,"S","N"," ","N","INT-SW","SLIC","1539"," ","N"
	//0         1             2                      3                                     4                             5                   6                         7                        8                        9                          11               12  13     14    15      16   17


	$codeDEBUG = $data[21];

	$code = $codeDEBUG;
	//$cat = substr($code, 0, 2);
	//$sub = substr($code, 2);

	$manname = $data[3];
	$manname = addslashes($manname);

	$shipweight = $data[8];

	$sku = $data[1];
	$sku = str_replace(" ","",$sku);
	$manpart = $data[7];
	$manpart = addslashes($manpart);

	$upccode = $data[9];

	$descr = $data[4];
	$descr = addslashes($descr);



	$price = $data[6];

	$cost = $data[14];
	$cost = addslashes($cost);


	$salesdescr  = addslashes($data[4]);

	$instock = 0;

	$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		list($manid) = mysql_fetch_array($qok);
	} else {
		$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$manid = mysql_insert_id();
	}
	$query = "select id from vendor_product_list where vid = '$VENDORID' and manpart = '$manpart' and sku = '$sku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0)
	{
		list($pid) = mysql_fetch_array($qok);
		$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '0.00')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$Nupdated++;

		$efc->updateVendorSourcing($pid, $VENDORID);

		echo "(UPDATE) $manname MPART: $manpart VSKU: $sku CODE: $code  PID: $pid  $price/$cost\n";
		
?>