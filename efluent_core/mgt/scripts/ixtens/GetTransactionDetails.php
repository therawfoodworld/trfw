<?php
/** GetTransactionDetails SOAP example; last modified 08MAY23.
 *
 *  Get detailed information about a single transaction. 
*/

include 'ppsdk_include_path.inc';

require_once 'PayPal.php';
require_once 'PayPal/Profile/Handler/Array.php';
require_once 'PayPal/Profile/API.php';
require_once 'PayPal/Type/GetTransactionDetailsRequestType.php';
require_once 'PayPal/Type/GetTransactionDetailsResponseType.php';

$environment = 'live';	// 'sandbox or 'beta-sandbox' or 'live'

//--------------------------------------------------
// PROFILE
//--------------------------------------------------
/**
 *                    W A R N I N G
 * Do not embed plaintext credentials in your application code.
 * Doing so is insecure and against best practices.
 *
 * Your API credentials must be handled securely. Please consider
 * encrypting them for use in any production environment, and ensure
 * that only authorized individuals may view or modify them.
 */

$handler = & ProfileHandler_Array::getInstance(array(
            'username' => 'my_api_username',
            'certificateFile' => null,
            'subject' => null,
            'environment' => $environment));

$pid = ProfileHandler::generateID();

$profile = & new APIProfile($pid, $handler);

// Set up your API credentials, PayPal end point, and API version.
$profile->setAPIUsername('acohen_api1.ecomelectronics.com');
$profile->setAPIPassword('88PFHZSC5EH693YM');
//$profile->setSignature('my_api_signature');
$profile->setCertificateFile('/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt');
$profile->setEnvironment($environment);
//--------------------------------------------------

$trans_details =& PayPal::getType('GetTransactionDetailsRequestType');
$trans_details->setVersion("51.0");

// Set request-specific fields.
$trans_details->setTransactionId('96D277992R750214F', 'iso-8859-1');

$caller =& PayPal::getCallerServices($profile);

// Execute SOAP request.
$response = $caller->GetTransactionDetails($trans_details);

switch($response->getAck()) 
{
	case 'Success':
	case 'SuccessWithWarning':
        // Extract the response details.
		$ptd = $response->getPaymentTransactionDetails();
		$payer_info = $ptd->getPayerInfo();
		$payment_info = $ptd->getPaymentInfo();

		// Payer fields
		$payer = $payer_info->getPayer();
		$payer_id = $payer_info->getPayerID();
		$payer_name = $payer_info->getPayerName();
		$payer_fname = $payer_name->getFirstName();
		$payer_lname = $payer_name->getLastName();

		// Payment fields
		$tran_ID = $payment_info->getTransactionID();
		$tran_ID_parent = $payment_info->getParentTransactionID();
		if(! isset($tran_ID_parent)){
			$tran_ID_parent = "Not Available";
		}
		$gross_amt_obj = $payment_info->getGrossAmount();
		$gross_amt = $gross_amt_obj->_value;
		$currency_cd = $gross_amt_obj->getattr('currencyID');
		$status = $payment_info->getPaymentStatus();
		exit('GetTransactionDetails Completed Successfully: ' . print_r($response, true));

	default:
		exit('GetTransactionDetails failed: ' . print_r($response, true));
}

?>