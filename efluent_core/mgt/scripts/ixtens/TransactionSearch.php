<?php
/** TransactionSearch SOAP example; last modified 08MAY23.
 *
 *  Search your account history for transactions that meet the criteria you specify. 
*/

include 'ppsdk_include_path.inc';

require_once 'PayPal.php';
require_once 'PayPal/Profile/Handler/Array.php';
require_once 'PayPal/Profile/API.php';
require_once 'PayPal/Type/TransactionSearchRequestType.php';
require_once 'PayPal/Type/TransactionSearchResponseType.php';

$environment = 'live';	// sandbox or 'beta-sandbox' or 'live'


$handler = & ProfileHandler_Array::getInstance(array(
            'username' => 'my_api_username',
            'certificateFile' => null,
            'subject' => null,
            'environment' => $environment));

$pid = ProfileHandler::generateID();

$profile = & new APIProfile($pid, $handler);

// Set up your API credentials, PayPal end point, and API version.
$profile->setAPIUsername('acohen_api1.ecomelectronics.com');
$profile->setAPIPassword('88PFHZSC5EH693YM');
//$profile->setSignature('my_api_signature');
$profile->setCertificateFile('/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt');

$profile->setEnvironment($environment);
//--------------------------------------------------

$trans_search =& PayPal::getType('TransactionSearchRequestType');
$trans_search->setVersion("51.0");

$trans_search->setStartDate( date('Y-m-d\T00:00:00\Z', strtotime("2009-06-01")), 'iso-8859-1');
$trans_search->setEndDate( date('Y-m-d\T24:00:00\Z', strtotime("2009-06-30")), 'iso-8859-1');

$trans_search->setPayer("alan@ecomelectronics.com", 'iso-8859-1');
$trans_search->setAmount("270.90", 'iso-8859-1');

$caller =& PayPal::getCallerServices($profile);

// Execute SOAP request.
$response = $caller->TransactionSearch($trans_search);

switch($response->getAck()) {
	case 'Success':
	case 'SuccessWithWarning':
        // Extract the response details.
		$ptsr = $response->getPaymentTransactions();
		if(!is_array($ptsr)) {
			$ptsr = array($ptsr);
		}

		$tran_id = array();
		$tran_ts = array();
		$tran_status = array();
		$tran_payer_name = array();
		$gross_amt_obj = array();
        $tran_amount = array();

		foreach($ptsr as $i => $trans) {
			$tran_id[$i] = $trans->getTransactionID();
			$tran_ts[$i] = $trans->getTimestamp();
			$tran_status[$i] = $trans->getStatus();
			$tran_payer_name[$i] = $trans->getPayerDisplayName();
			$gross_amt_obj[$i] = $trans->getGrossAmount();
        	$tran_amount[$i] = $gross_amt_obj->_value;
        }
		//exit('TransactionSearch Completed Successfully: ' . print_r($response, true));
		
		$transactionID = $response->PaymentTransactions->TransactionID;
		
		echo "TRANSACTIONID: $transactionID<BR>";
		
/*		echo "<pre>";
		print_r($response);
		echo "</pre>";
*/
}

if($transactionID != "")
{
	$handler = & ProfileHandler_Array::getInstance(array(
	            'username' => 'my_api_username',
	            'certificateFile' => null,
	            'subject' => null,
	            'environment' => $environment));
	
	$pid = ProfileHandler::generateID();
	
	$profile = & new APIProfile($pid, $handler);
	
	// Set up your API credentials, PayPal end point, and API version.
	$profile->setAPIUsername('acohen_api1.ecomelectronics.com');
	$profile->setAPIPassword('88PFHZSC5EH693YM');
	//$profile->setSignature('my_api_signature');
	$profile->setCertificateFile('/home/httpd/ecomelectronics.com/pp/cert_key_pem.txt');
	$profile->setEnvironment($environment);
	//--------------------------------------------------
	
	$trans_details =& PayPal::getType('GetTransactionDetailsRequestType');
	$trans_details->setVersion("51.0");
	
	// Set request-specific fields.
	$trans_details->setTransactionId($transactionID, 'iso-8859-1');
	
	$caller =& PayPal::getCallerServices($profile);
	
	// Execute SOAP request.
	$response = $caller->GetTransactionDetails($trans_details);
	
	switch($response->getAck()) 
	{
		case 'Success':
		case 'SuccessWithWarning':
	        // Extract the response details.
			$ptd = $response->getPaymentTransactionDetails();
			$payer_info = $ptd->getPayerInfo();
			$payment_info = $ptd->getPaymentInfo();
	
			// Payer fields
			$payer = $payer_info->getPayer();
			$payer_id = $payer_info->getPayerID();
			$payer_name = $payer_info->getPayerName();
			$payer_fname = $payer_name->getFirstName();
			$payer_lname = $payer_name->getLastName();
	
			// Payment fields
			$tran_ID = $payment_info->getTransactionID();
			$tran_ID_parent = $payment_info->getParentTransactionID();
			if(! isset($tran_ID_parent)){
				$tran_ID_parent = "Not Available";
			}
			$gross_amt_obj = $payment_info->getGrossAmount();
			$gross_amt = $gross_amt_obj->_value;
			$currency_cd = $gross_amt_obj->getattr('currencyID');
			$status = $payment_info->getPaymentStatus();
			
			echo "<pre>";
			print_r($response, true);
			echo "</pre>";
	
		default:
			exit('GetTransactionDetails failed: ' . print_r($response, true));
	}
}
?>