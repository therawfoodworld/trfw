<?

//passthru("curl http://www.infiniteshopping.biz/exportdata/infbizcsvproductstock.csv > istock.csv");
//passthru("curl http://www.infiniteshopping.biz/exportdata/infbizcsvproducts.csv > iproducts.csv");

/*

[root@Development  /home/httpd/giftjoy.net/mgt/scripts/ishopping]# head iproducts.csv -n 5
Brand,Model,Product Name,Image,description,Series,Designed For,Dial Color,Band Material,Movement Type,Water Resistance,Case Material,Function,Crown Type,Crystal Material,Calendar Type,Case Width,Features,Warranty,Clasp Type,Discontinued,Display Type,Face shape,Case Width,Case Height,Case Thickness,Case Depth,Case Diameter,Band Length,Band Width,Band Color
BCBG,#BG6234,"Womens BCBG Plisse BG6234 Japanese Quartz movement Black dial watch ",http://c759341.r43.cf2.rackcdn.com/BCBG-BG6234-1.jpg,,"Plisse","Womens","Black","Leather with Satin Ribbon","Japanese Quartz","30 Meter","Stainless Steel","Hour and Minute Hands","","Mineral","","","Signature Two-Hand Dial With Two Applied Indexes","Limited Lifetime Seller Warranty","Buckle","","Analog","Square","","","12 mm","","38 mm with crown","Womens Standard","33 mm","Black"
BCBG,#BG6236,"Womens BCBG Plisse BG6236 Japanese Quartz movement Grey dial watch ",http://c759341.r43.cf2.rackcdn.com/BCBG-BG6236-1.jpg,,"Plisse","Womens","Grey","Leather with Satin Ribbon","Japanese Quartz","30 Meter","Stainless Steel","Hour and Minute Hands","Signature Dome Crown With Etched Graphic Icon","Mineral","","","Signature Two-Hand Dial With 12 o'clock and 6 o'clock Applied Dots ","Limited Lifetime Seller Warranty","Buckle","","Analog","Rectangular","","","10 mm","","38 mm with crown","Womens Standard","33 mm","Grey"
BCBG,#BG6254,"Womens BCBG Florence BG6254 Japanese Quartz movement Gold dial watch ",http://c759341.r43.cf2.rackcdn.com/BCBG-BG6254-1.jpg,,"Florence","Womens","Gold","Leather","Japanese Quartz","30 Meter","Ion Plated Stainless Steel","Hour and Minute Hands","","Mineral","","","Gold Tone Hour And Minute Hands, Roman Numerals Etched On Bezel","Limited Lifetime Seller Warranty","Buckle","","Analog","Round","","","7 mm","","35 mm; 37 mm with crown","Womens Standard","14 mm","Black"
BCBG,#BG6271,"Womens BCBG Arabesque BG6271 Japanese Quartz White MOP dial watch ",http://c759341.r43.cf2.rackcdn.com/BCBG-BG6271-1.jpg,,"Arabesque","Womens","White MOP","Leather","Japanese Quartz","30 Meter","Stainless Steel","Hour and Minute Hands","","Mineral","","","Arabic Numeral Index At 12 O'Clock, Sparkling Crystal On Bezel","Limited Lifetime Seller Warranty","Buckle","","Analog","Square","","","7 mm","","22 mm; 24 mm with crown","Womens Standard","16 mm","Brown"
[root@Development  /home/httpd/giftjoy.net/mgt/scripts/ishopping]#



[root@Development  /home/httpd/giftjoy.net/mgt/scripts/ishopping]# head istock.csv -n 5
Brand,Model,Sellprice,Listprice,Available Qty
BCBG,#BG6234,75.5,125,1
BCBG,#BG6236,75.5,125,1
BCBG,#BG6254,81.55,135,2
BCBG,#BG6271,75.5,125,1
[root@Development  /home/httpd/giftjoy.net/mgt/scripts/ishopping]#

*/

ini_set("memory_limit","-1");

$ROOT_DIR = "/home/httpd/giftjoy.net";
if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$VENDORID = $efc->getConfigValue("ishopping", "VENDORID");
$vid =  $efc->getConfigValue("ishopping", "VENDORID");

$vid = 146;
$VENDORID = 146;


echo "VENDORID: $VENDORID\n";

error_reporting(1);

$feed_local = "istock.csv";
if(!file_exists($feed_local)) { die($feed_local); } else { echo "OK... $feed_local\n"; }

$feed_local = "iproducts.csv";
if(!file_exists($feed_local)) { die($feed_local); } else { echo "OK... $feed_local\n"; }

function getmicrotime() {	list($usec, $sec) = explode(" ",microtime());	return ((float)$usec + (float)$sec); }


$xtime_start = getmicrotime();

$Ncreated = 0;
$Nupdated = 0;


$fp = fopen("iproducts.csv", "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

	$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = 'NOT_PROVIDED'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) <= 0)
	{
		$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', 'NOT_PROVIDED', '0')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$parentid = mysql_insert_id();
	}

}



echo "Importing Products.....\n";
$fp = fopen("iproducts.csv", "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, ",")) !== FALSE)
{

	$image_url = addslashes($data[3]);


	/*

	[4] => description
	[5] => Series
	[6] = > Designed For
	[7] = > Dial Color
	[8] = > Band Material
	[9] = > Movement Type
	[10] = > Water Resistance
	[11] = > Case Material
	[12] = > Function
	[13] = > Crown Type
	[14] = > Crystal Material
	[15] = > Calendar Type
	[16] = > Case Width
	[17] = > Features
	[18] = > Warranty
	[19] = > Clasp Type
	[20] = > Discontinued
	[21] = > Display Type
	[22] = > Face shape
	[23] = > Case Width
	[24] = > Case Height
	[25] = > Case Thickness
	[26] = > Case Depth
	[27] = > Case Diameter
	[28] = > Band Length
	[29] = > Band Width
	[30] = > Band Color

	*/

	$descr = addslashes($data[2]);

	$salesdescr = addslashes($data[17]);
	
	if($data[4] !="") { $htmlvalue = addslashes($data[4]).'<br><br>'; } 
	else 
	{ $htmlvalue = $salesdescr.'<br><br>'; }

	if($data[5] !="") { $htmlvalue .= "Series: ".addslashes($data[5]).'<br>'; }
	if($data[6] !="") { $htmlvalue .= "Designed For: ".addslashes($data[6]).'<br>'; }
	if($data[7] !="") { $htmlvalue .= "Dial Color: ".addslashes($data[7]).'<br>'; }
	if($data[8] !="") { $htmlvalue .= "Band Material: ".addslashes($data[8]).'<br>'; }
	if($data[9] !="") { $htmlvalue .= "Movement Type: ".addslashes($data[9]).'<br>'; }
	if($data[10] !="") { $htmlvalue .= "Water Resistance: ".addslashes($data[10]).'<br>'; }
	if($data[11] !="") { $htmlvalue .= "Case Material: ".addslashes($data[11]).'<br>'; }
	if($data[12] !="") { $htmlvalue .= "Function: ".addslashes($data[12]).'<br>'; }
	if($data[13] !="") { $htmlvalue .= "Crown Type: ".addslashes($data[13]).'<br>'; }
	if($data[14] !="") { $htmlvalue .= "Crystal Material: ".addslashes($data[14]).'<br>'; }
	if($data[15] !="") { $htmlvalue .= "Calendar Type: ".addslashes($data[15]).'<br>'; }
	if($data[16] !="") { $htmlvalue .= "Case Width: ".addslashes($data[16]).'<br>'; }
	if($data[17] !="") { $htmlvalue .= "Features: ".addslashes($data[17]).'<br>'; }
	if($data[18] !="") { $htmlvalue .= "Warranty: ".addslashes($data[18]).'<br>'; }
	if($data[19] !="") { $htmlvalue .= "Clasp Type: ".addslashes($data[19]).'<br>'; }
	if($data[20] !="") { $htmlvalue .= "Discontinued: ".addslashes($data[20]).'<br>'; }
	if($data[21] !="") { $htmlvalue .= "Display Type: ".addslashes($data[21]).'<br>'; }
	if($data[22] !="") { $htmlvalue .= "Face shape: ".addslashes($data[22]).'<br>'; }
	if($data[23] !="") { $htmlvalue .= "Case Width: ".addslashes($data[23]).'<br>'; }
	if($data[24] !="") { $htmlvalue .= "Case Height: ".addslashes($data[24]).'<br>'; }
	if($data[25] !="") { $htmlvalue .= "Case Thickness: ".addslashes($data[25]).'<br>'; }
	if($data[26] !="") { $htmlvalue .= "Case Depth: ".addslashes($data[26]).'<br>'; }
	if($data[27] !="") { $htmlvalue .= "Case Diameter: ".addslashes($data[27]).'<br>'; }
	if($data[28] !="") { $htmlvalue .= "Band Length: ".addslashes($data[28]).'<br>'; }
	if($data[29] !="") { $htmlvalue .= "Band Width: ".addslashes($data[29]).'<br>'; }
	if($data[30] !="") { $htmlvalue .= "Band Color: ".addslashes($data[30]).'<br>'; }

	
	
	$sku = addslashes($data[1]);
	$manpart = addslashes($data[1]);



	$cost = $acost[$sku];
	$price = $aprice[$sku];
	$instock = $astock[$sku];



	$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) > 0)	{
		list($pid) = mysql_fetch_array($qok);

    	//if(strlen($salesdescr) > "110") { echo "[grep] $pid\n"; echo $salesdescr."\n"; }

		$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


		$efc->updateVendorSourcing($pid, $VENDORID);

		if($salesdescr != "")
		{
			$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

			$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		}

		if($image_url !="") {
			$query = "delete from vendor_product_images where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_product_images(id, imageurl) values('$pid','$image_url')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}


		$query = "delete from vendor_product_html where id = '$pid' and vid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_product_html(id, vid, data) values('$pid','$vid','$htmlvalue')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		echo "VPID: $pid SKU: $sku UPDATED PROD DATA\n";


	}
}





?>
