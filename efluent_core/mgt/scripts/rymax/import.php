<?ini_set("memory_limit","-1");?>


<?
$ROOT_DIR = $argv[1];
if($ROOT_DIR == "") { echo "usage: $argv[0] ROOT_DIR\n"; exit; }
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
$MYSQL_DATABASE = $DB;
$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$VENDORID = $efc->getConfigValue("Rymax", "VENDORID");
$vid =  $efc->getConfigValue("Rymax", "VENDORID");


function getmicrotime() { 	list($usec, $sec) = explode(" ",microtime()); 	return ((float)$usec + (float)$sec); }

$Ncreated = 0;
$Nupdated = 0;
$xtime_start = getmicrotime();

$feed_local = "$ROOT_DIR/mgt/scripts/rymax/rymax.txt";
if(!file_exists($feed_local)) {	echo "\nERROR=> MISSING FEED FILE " . $files_array['feed.csv'] . "\n"; exit; }
echo "VENDORID: $VENDORID\n";



// Category
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8900, "|")) !== FALSE)
{
	$catname = "RYMAX CATALOG";
	$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) <= 0)
	{
		$query = "insert into vendor_categories(vid, catname, parentid) values('$VENDORID', '$catname', '0')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$catid = mysql_insert_id();
	}
}
fclose($fp);




// Brands
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8192, "|")) !== FALSE)
$manname = addslashes($data[3]);
$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
else
{
	$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	echo "[+] Added Brand '$manname/".mysql_insert_id()."'\n";
}
fclose($fp);





//PRODUCTS
echo "Importing Products....\n";
$fp = fopen($feed_local, "r");
if($fp == FALSE) { echo "ERROR OPENING FILE\n"; exit; }
while(($data = fgetcsv($fp, 8192, "|")) !== FALSE)
{
	/*
    [0] => 01-1266
    [1] => Maxime Luxor Credit Card Case - Brown
    [2] => With logo-embossing on the front and tonal topstitching, as well as interior card slots, this card case is sure to make a statement.
    [3] => Ferragamo
    [4] => FERRA-668632-0419808
    [5] => 95.03
    [6] => 14
    [7] => 170
    [8] =>
    [9] => http://maxrewards.rymaxinc.com/catalog/images/products/01-1266.jpg
	*/
	$sku = utf8_encode(addslashes($data[0]));
	$descr = utf8_encode(addslashes($data[1]));
	$salesdescr = utf8_encode(addslashes($data[2]));
	$manname = utf8_encode(addslashes($data[3]));
	$manpart = utf8_encode(addslashes($data[4]));
	$cost = utf8_encode($data[5]);
	$instock = utf8_encode($data[6]);
	$price = utf8_encode($data[7]);

	$catname = "RYMAX CATALOG";

	$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	if(mysql_num_rows($qok) > 0) 	{ list($manid) = mysql_fetch_array($qok); }
	else {
		//echo "Error: $manname , $VENDORID\n"; exit;
		$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0) {list($manid) = mysql_fetch_array($qok);}
		else
		{
			$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			echo "[+] Added Brand '$manname/".mysql_insert_id()."'\n";
			$manid = mysql_insert_id();
		}

	}

	$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	if(mysql_num_rows($qok) > 0)	{
		list($pid) = mysql_fetch_array($qok);
		if($dupes_are_for_fags[$sku] !="1") { $Nupdated++; }
					$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		$dupes_are_for_fags[$sku] = 1;
		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$price')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		echo "VPID: $pid SKU: $sku UPDATED (STK: $instock / CST: $cost)\n";
		$efc->vendorMarkPrice($pid);
		$efc->updateVendorSourcing($pid, $VENDORID);
		if($salesdescr != "")
		{
			$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		}
	}
	else
	{
		// ADD
		$added_dte = time();
		$Ncreated++;
		$query = "insert into vendor_product_list(vid, manid, manpart, descr, added_dte, sku) values('$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku')";
		echo "ADDED: $manpart (STK: $instock  / CST: $cost)\n";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$pid = mysql_insert_id();

		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$price')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		if($salesdescr != "")
		{
			$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		}

		/*
		vendor_metatag_data
		type = 1
		subtype = keywords
		typeid = product_list.id  <--pid
		vid = vendor
		data = keywords frm feed
		*/
		$keywords = addslashes($data[8]);
		if($keywords !="")
		{
			$query = "delete from vendor_metatag_data where vid = '$VENDORID' and pid = '$pid' and type = '1' and subtype = 'keywords'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			$query = "insert into vendor_metatag_data(vid, pid, typeid, type, subtype, data) values('$VENDORID','$pid','$pid','1','keywords','$keywords')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		}


		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		list($catid,$parentid) = mysql_fetch_array($qok);
		$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

	}
}





// REPORT
$xtime_end = getmicrotime();
$qtime = number_format(($xtime_end - $xtime_start), 2);
$msg = "Completed in $qtime Updated($Nupdated) Created($Ncreated)";
$efc->logAction("SUCCESS", 4, 0, 'RYMAX Sync Succesful', $msg);
echo $msg . "\n";




//VENDOR SYNC WIDGET
$last_modified = filemtime("$ROOT_DIR/mgt/scripts/rymax/rymax.txt");

$query = "select vendorname from vendors where vendorid = '$VENDORID'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
list($VENDORNAME) = mysql_fetch_array($qok);


$last_modified_date = date("dS F, Y @ h:ia", $last_modified);
$date = date("Y-m-d H:i:s", time());


$query = "select filedate from vendor_sync where vendorname = '$VENDORNAME'";
if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

if(mysql_num_rows($qok) <= 0)
{
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
}
else {
	$query = "delete from vendor_sync where vendorname = '$VENDORNAME'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
	$query = "insert into vendor_sync(vendorname, filedate, lastsync, updated, created) values('$VENDORNAME','$last_modified_date','$date','$Nupdated','$Ncreated')";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

}


$efc->cleanVendorItems($VENDORID);

?><?mysql_close();?>
