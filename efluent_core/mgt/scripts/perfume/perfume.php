<?php



function xml2array($contents, $get_attributes=1) 
{ 
    if(!$contents) return array(); 

    if(!function_exists('xml_parser_create')) { 
        //print "'xml_parser_create()' function not found!"; 
        return array(); 
    } 
    //Get the XML parser of PHP - PHP must have this module for the parser to work 
    $parser = xml_parser_create(); 
    xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 ); 
    xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 ); 
    xml_parse_into_struct( $parser, $contents, $xml_values ); 
    xml_parser_free( $parser ); 

    if(!$xml_values) return;//Hmm... 

    //Initializations 
    $xml_array = array(); 
    $parents = array(); 
    $opened_tags = array(); 
    $arr = array(); 

    $current = &$xml_array; 

    //Go through the tags. 
    foreach($xml_values as $data) { 
        unset($attributes,$value);//Remove existing values, or there will be trouble 

        //This command will extract these variables into the foreach scope 
        // tag(string), type(string), level(int), attributes(array). 
        extract($data);//We could use the array by itself, but this cooler. 

        $result = ''; 
        if($get_attributes) {//The second argument of the function decides this. 
            $result = array(); 
            if(isset($value)) $result['value'] = $value; 

            //Set the attributes too. 
            if(isset($attributes)) { 
                foreach($attributes as $attr => $val) { 
                    if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
                    /**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */ 
                } 
            } 
        } elseif(isset($value)) { 
            $result = $value; 
        } 

        //See tag status and do the needed. 
        if($type == "open") {//The starting of the tag '<tag>' 
            $parent[$level-1] = &$current; 

            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
                $current[$tag] = $result; 
                $current = &$current[$tag]; 

            } else { //There was another element with the same tag name 
                if(isset($current[$tag][0])) { 
                    array_push($current[$tag], $result); 
                } else { 
                    $current[$tag] = array($current[$tag],$result); 
                } 
                $last = count($current[$tag]) - 1; 
                $current = &$current[$tag][$last]; 
            } 

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
            //See if the key is already taken. 
            if(!isset($current[$tag])) { //New Key 
                $current[$tag] = $result; 

            } else { //If taken, put all things inside a list(array) 
                if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array... 
                        or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) { 
                    array_push($current[$tag],$result); // ...push the new element into that array. 
                } else { //If it is not an array... 
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value 
                } 
            } 

        } elseif($type == 'close') { //End of tag '</tag>' 
            $current = &$parent[$level-1]; 
        } 
    } 

    return($xml_array); 
} 

function xml2array2($elements_string)
{
    $xml_array = array();
    $elements_regex = '/<(\w+)\s*([^\/>]*)\s*(?:\/>|>(.*?)<(\/\s*\1\s*)>)/s';
    $attributes_regex = '/(\w+)=(?:"|\')([^"\']*)(:?"|\')/';
    preg_match_all ($elements_regex, $elements_string, $elements_array);
    foreach ( $elements_array[1] as $e_key => $e_value )
    {
        $xml_array[$e_key]["name"] = $e_value;
        if ( ($attributes_string = trim($elements_array[2][$e_key])) )
        {
            preg_match_all($attributes_regex, $attributes_string, $attributes_array);
            foreach ( $attributes_array[1] as $a_key => $a_value )
            {
                $xml_array[$a_key]["attributes"][$a_value] = $attributes_array[2][$a_key];
            }
        }
        if ( ($p = strpos($elements_array[3][$e_key], "<")) > 0 )
        {
            $xml_array[$e_key]["text"] = substr($elements_array[3][$e_key], 0, $p - 1);
        }
        if ( preg_match($elements_regex, $elements_array[3][$e_key]) )
        {       
            $xml_array[$e_key]["elements"] = xml2array($elements_array[3][$e_key]);
        }
        else if ( isset($elements_array[3][$e_key]) )
        {
            $xml_array[$e_key]["text"] = $elements_array[3][$e_key];
        }
        $xml_array[$e_key]["closetag"] = $elements_array[4][$e_key];
    }
    return $xml_array;
}


function get_all_products() {

	$data = '<?xml version="1.0" encoding="UTF-8"?>
 <Request>
  <Api>Get_All_Products</Api>
   <Authentication>
    <Userid>abc</Userid>
    <Password>abc</Password>
   </Authentication>
</Request>



';	


	$fp = fsockopen("www.namebrandsperfume.com", 80);

	fputs($fp, "POST /api/ HTTP/1.1\r\n");
	fputs($fp, "Host: www.namebrandsperfume.com\r\n");
	fputs($fp, "Referer: http://www.namebrandsperfume.com\r\n");
	fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
	fputs($fp, "Content-length: ". strlen($data) ."\r\n");
	fputs($fp, "Connection: close\r\n\r\n");
	fputs($fp, $data);

	$result = '';
	while(!feof($fp)) {
		// receive the results of the request
		$result .= fgets($fp, 128);
	}

	// close the socket connection:
	fclose($fp);

	// split the result header from the content
	$result = explode("\r\n\r\n", $result, 2);

	$header = isset($result[0]) ? $result[0] : '';
	$content = isset($result[1]) ? $result[1] : '';

	// return as array:
	return array($header, $content);
}

$array = get_all_products();

//print_r($array[1]);

file_put_contents("tmp.xml",$array[1]);

$filedata = file_get_contents("tmp.xml");

$array = xml2array($filedata, 1);

print_r($array);



?>