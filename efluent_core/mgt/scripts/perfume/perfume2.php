<?php
$ROOT_DIR = "/home/httpd/starbestbuy.com";

REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;

$MYSQL_DATABASE = $DB;

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$vendorid = 153;
$vid = 153;
$VENDORID = 153;



function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have  module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		// command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but  cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides .
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}

function getallproducts()
{
	$data ='<?xml version="1.0" encoding="UTF-8"?>
 <Request>
  <Api>Get_All_Products</Api>
   <Authentication>
    <Userid>abc</Userid>
    <Password>abc</Password>
   </Authentication>
</Request>';

	$url = "http://www.namebrandsperfume.com/api/";

	$ch = curl_init();    // initialize curl handle
	curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
	curl_setopt($ch, CURLOPT_TIMEOUT, 120); // times out after 4s
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // add POST fields
	$result = curl_exec($ch); // run the whole process

	if($result == FALSE)
	{
		echo "Curl Error: ". curl_errno($ch) . " " . curl_error($ch);
		return;
	}

	return xml2array($result, 1);
}

$array = getallproducts();
//print_r($array);


/*
[Product] => Array
(
[0] => Array
(
[SKU] => Array
(
[value] => DJBDRI75TSM
)
[SKU]
[Name]
=> Array
(
[value] => DEREK JETER DRIVEN BLACK By AVON For MEN
)

[SKU]
[Name]
[Designer]

=> Array
(
[value] => AVON
)

[SKU]
[Name]
[Designer]
[Description]
=> Array
(
[value] => Derek Jeter Driven Black 2.5 Fl. Oz.eau De Toilette Spray Men.designer:avon
)

[SKU]
[Name]
[Designer]
[Description]
[Gender]
=> Array
(
[value] => MEN
)

[SKU]
[Name]
[Designer]
[Description]
[Gender]
[Retailprice]
=> Array
(
[value] => 35.00
)

[SKU]
[Name]
[Designer]
[Description]
[Gender]
[Retailprice]
[Price]

=> Array
(
[value] => 23.00
)

[SKU]
[Name]
[Designer]
[Description]
[Gender]
[Retailprice]
[Price]
[Image_URL]
=> Array
(
[value] => http://www.namebrandsperfume.com/images/products/mediumimage/DJBDRI75TSM.gif
)


[SKU]
[Name]
[Designer]
[Description]
[Gender]
[Retailprice]
[Price]
[Image_URL]
[Quantity]
=> Array
(
[value] => 29
)

)

*/
foreach($array['Response']['ProductArray']['Product'] as $key => $str)
{

	echo "[$key] $str\n";

	$sku = addslashes($array['Response']['ProductArray']['Product'][$key]['SKU']['value']);
	$manpart = addslashes($array['Response']['ProductArray']['Product'][$key]['SKU']['value']);

	$title = addslashes($array['Response']['ProductArray']['Product'][$key]['Name']['value']);
	$descr = addslashes($array['Response']['ProductArray']['Product'][$key]['Name']['value']);

	$manname = addslashes($array['Response']['ProductArray']['Product'][$key]['Designer']['value']);
	$brand = addslashes($array['Response']['ProductArray']['Product'][$key]['Designer']['value']);

	$sales_descr = addslashes($array['Response']['ProductArray']['Product'][$key]['Description']['value']);
	$salesdescr = addslashes($array['Response']['ProductArray']['Product'][$key]['Description']['value']);


	$gender = addslashes($array['Response']['ProductArray']['Product'][$key]['Gender']['value']);
	$map = addslashes($array['Response']['ProductArray']['Product'][$key]['Retailprice']['value']);
	$price = addslashes($array['Response']['ProductArray']['Product'][$key]['Retailprice']['value']);
	$cost = addslashes($array['Response']['ProductArray']['Product'][$key]['Price']['value']);
	$imgsrc = addslashes($array['Response']['ProductArray']['Product'][$key]['Image_URL']['value']);
	$instock = addslashes($array['Response']['ProductArray']['Product'][$key]['Quantity']['value']);

	$upc = addslashes($array['Response']['ProductArray']['Product'][$key]['UPC']['value']);
	$upccode = addslashes($array['Response']['ProductArray']['Product'][$key]['UPC']['value']);

	if(!is_numeric($upccode)) { $upc = ''; $upccode = ''; }
	
	$catname = 'Health and Beauty';
	$catsub ='Fragrances';


	$query = "select manid from vendor_manufacturers where manname = '$manname' and vid = '$VENDORID'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

	if(mysql_num_rows($qok) > 0)
	{
		list($manid) = mysql_fetch_array($qok);
		echo "EXISTS: $manid\n";
	}
	else
	{
		echo "Created: $manname , $VENDORID\n";
		//exit;
		$query = "insert into vendor_manufacturers(manname, vid) values('$manname', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		$manid = mysql_insert_id();
		echo "DEBUG: $manname [$manid]\n";
	}

	echo "MANNAME: $manname SKU: $sku MANID: $manid\n";

	$query = "select id from vendor_product_list where vid = '$VENDORID' and sku = '$sku' and manpart = '$manpart' and refurb = '$refurb' and manid = '$manid'";
	if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


	if(mysql_num_rows($qok) > 0)
	{
		
		echo "[grep]  $upccode\n";
		
		
		
		list($pid) = mysql_fetch_array($qok);

		echo "UPDATING PID: $pid\n";

		$query = "update vendor_product_list set manpart = '$sku' where manpart = '$manpart' and vid = '$vid' and id = '$pid'";
		//		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "update vendor_product_list set sku = '$manpart' where sku = '$sku' and vid = '$vid' and id = '$pid'";
		//		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$price')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }
		
	
		$query = "update vendor_product_list set upccode='$upccode' where id = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }



		/*$query = "select pid from product_sourcing where vpid = '$pid' and distid = '$vid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		list($zpid) = mysql_fetch_array($qok);

		if(is_numeric($zpid)) {

		$tstamp = date("Y-m-d H:i:s", time());

		$query = "update product_sourcing set tstamp = '$tstamp' where vpid = '$vpid' and distid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "update product_sourcing set qty='$instock' where vpid = '$vpid' and distid = '$VENDORID'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		echo "[update] PID: $zpid  VPID: $pid SKU: $sku MANPART: $manpart UPDATED (STK: $instock / CST: $cost)  R/$refurb  MAP/$map";

		if($instock > 0)
		{
		$markedvis++;
		$query = "update product_list set visible = 1 where id = '$zpid'";
		if(!$queryok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		echo "  [MARKED VISABLE]";
		}

		$efc->touchsku($zpid);*/

		$Nupdated++;

		echo "[update] VPID: $pid SKU: $sku MANPART: $manpart UPDATED (STK: $instock / CST: $cost)  MAP/$map\n";

		echo "\n";

		$efc->updateVendorSourcing($pid, $VENDORID);  //<-----------------------------------------------------------------------------------------------------


		$query = "delete from vendor_product_images where id = '$pid'";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imgsrc')";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }




	}

	else
	{
		// New product data.  YAY.


		$added_dte = time();
		$Ncreated++;

		$query = "insert into vendor_product_list(refurb, vid, manid, manpart, descr, added_dte, sku, upccode, shipweight, etilizeid)
			values('$refurb', '$VENDORID', '$manid', '$manpart', '$descr', '$added_dte', '$sku', '$upccode','$shipweight', '$etilizeid')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		$pid = mysql_insert_id();
		echo "[new] VPID: $pid VID: $VENDORID  MANPART: $manpart SKU: $sku (STK: $instock  / CST: $cost) ---  mname: '$manname' mid: '$manid'  MAP/$map\n";

		$query = "delete from vendor_pricing where pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		$query = "insert into vendor_pricing(pid, instock, price, cost, map) values('$pid', '$instock', '$price', '$cost', '$map')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "delete from vendor_product_images where id = '$pid'";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		$query = "insert into vendor_product_images(id, imageurl) values('$pid', '$imgsrc')";
		if(!$qok = mysql_db_query($MYSQL_DATABASE, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


		$catname = 'Health and Beauty';
		$catsub ='Fragrances';

		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catname'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		if(mysql_num_rows($qok) > 0)
		{
			list($catid) = mysql_fetch_array($qok);
		}
		else
		{

			$query = "insert into vendor_categories(vid, catname) values('$VENDORID', '$catname')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

			$catid = mysql_insert_id();

		}

		$query = "delete from vendor_category_map where vid = '$vid' and catid = '$catid' and pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catid', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }



		$query = "select catid from vendor_categories where vid = '$VENDORID' and catname = '$catsub'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

		if(mysql_num_rows($qok) > 0)
		{
			list($catidsub) = mysql_fetch_array($qok);
		}
		else
		{
			$query = "insert into vendor_categories(vid, catname) values('$VENDORID', '$catsub')";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }

			$catidsub = mysql_insert_id();

		}

		$query = "delete from vendor_category_map where vid = '$vid' and catid = '$catidsub' and pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "insert into vendor_category_map(pid, catid, vid) values('$pid', '$catidsub', '$VENDORID')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }






		$query = "delete from vendor_sales_descr where vid = '$VENDORID' and pid = '$pid'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }


		$query = "insert into vendor_sales_descr(pid, vid, data) values('$pid', '$VENDORID', '$salesdescr')";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ echo $query."\n";echo mysql_error();exit;  }





	}

}


?>