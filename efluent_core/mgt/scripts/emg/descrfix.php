<?
$ROOT_DIR = $argv[1];


if($ROOT_DIR == "")
{
	echo "usage: merge_teledyn.phtml ROOT_DIR\n";
	exit;
}

set_time_limit(999999);
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);


//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

$VENDORID = $efc->getConfigValue("EMG", "VENDORID");

echo "VID: $VENDORID\n";

function getmicrotime()
{
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

$xtime_start = getmicrotime();
$Ncreated = 0;
$Nupdated = 0;

$customerid = $efc->getConfigValue("EMG", "customerid");
//GET FRESH FEED.
echo "Updating Data Feed...\n";
unlink("$ROOT_DIR/mgt/scripts/emg/products.xml");
passthru("curl -d \"data=<getFullInv><credentials>$customerid</credentials></getFullInv>\" \"https://www.ez-om.com/om/lib/emg_dispatcher.php?cmd=getFullInv&format=xml\" -k -o $ROOT_DIR/mgt/scripts/emg/products.xml -s");

function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides this.
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}

$buffer = "";
//$handle = fopen("fullInv.xml", "r");
$handle = fopen($ROOT_DIR."/mgt/scripts/emg/products.xml", "r");
if ($handle)
{
	while (!feof($handle))
	{
		$buffer .= fgets($handle, 4096);
	}
	fclose($handle);
}

$data = xml2array($buffer, 0);




for($xx = 0; $xx < count($data['getFullInvResponse']['item']); $xx++)
{
	//print_r($data);

	$manname = addslashes($data['getFullInvResponse']['item'][$xx]['supMfgr']);
	$upccode = addslashes($data['getFullInvResponse']['item'][$xx]['supUPC']);
	$manpart = addslashes($data['getFullInvResponse']['item'][$xx]['supMfgrModel']);
	$descr = addslashes($data['getFullInvResponse']['item'][$xx]['supDesc']);
	$sku = $manpart;
	$map = addslashes($data['getFullInvResponse']['item'][$xx]['supMinAdvPrice']);
	$price = 0.00;
	$cost = addslashes($data['getFullInvResponse']['item'][$xx]['supPrice']);
	$stock = addslashes($data['getFullInvResponse']['item'][$xx]['supQty']);

	$added_dte = date("Y-m-d H:i:s", time());
	$shipweight = addslashes($data['getFullInvResponse']['item'][$xx]['supShipWt']);


	$tmp1 = $data['getFullInvResponse']['item'][$xx]['supInfo1'];
	$tmp2 = $data['getFullInvResponse']['item'][$xx]['supInfo2'];
	$tmp3 = $data['getFullInvResponse']['item'][$xx]['supInfo3'];
	$tmp4 = $data['getFullInvResponse']['item'][$xx]['supInfo4'];
	$tmp5 = $data['getFullInvResponse']['item'][$xx]['supInfo5'];

	$salesdescr = addslashes("$tmp1 $tmp2 $tmp3 $tmp4 $tmp5");
	$supComment = $data['getFullInvResponse']['item'][$xx]['supComment'];


	$instock = $data['getFullInvResponse']['item'][$xx]['supQty'];


		$query = "select id from vendor_product_list where vid = '$VENDORID' and manpart = '$manpart'";
		if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
		if(mysql_num_rows($qok) > 0)
		{
			list($pid) = mysql_fetch_array($qok);
			
			
			$query = "update vendor_product_list set descr='$descr' where id = '$pid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			echo "PID: $pid DESCR: $descr\n";


		}
		
}
?><?mysql_close();?>
