$(document).ready(function(){
//	addCategorySaveAction();
	addAmazonPctSaveAction();
	addRepSaveAction();
});
function addAmazonPctSaveAction() {
	$('.amazonpct_perc').each(function(){
		$(this).click(function(){
			var field_name = '';
			var prod_idArr = $(this).attr('id').split("_"); 
			document.getElementById("amazonpct_perc_"+prod_idArr[2]).style.background = '#ffeeee';
			var pct_val = $('#amazonpct_perc_'+prod_idArr[2]).val();
			$.post('ajax_savecustom_field.phtml',
					{ action: "save_amazonpct", 
						id: prod_idArr[2],
						'pct': pct_val
					}
					, function(data) {
						document.getElementById("amazonpct_perc_"+prod_idArr[2]).style.background = '#ffffff';
						$('#amz'+prod_idArr[2]+'-amazonpct').html(pct_val);
						updateFBATable(prod_idArr[2]);
	
			});
		});
	});
}
function addRepSaveAction() {
	$('.rep_save').each(function(){
		$(this).click(function(){
			var field_name = '';
			var prod_idArr = $(this).attr('id').split("_"); 
			$.post('ajax_savecustom_field.phtml',
					{ action: "save_rep", 
						id: prod_idArr[1],
						'rep_id':$('#repsavesel_'+prod_idArr[1]).val()
					}
					, function(data) {
	
			});
		});
	});
}

function showMenu(productID, manpart)
{
    var pos = $('#item_'+productID).position();

    var dlg = document.getElementById("dialog");
    dlg.style.display = "block";
    dlg.style.left  = pos.left+20;
    dlg.style.top  = pos.top;

    document.getElementById("menuOption1").innerHTML = "<a href=\"javascript:e2window('/mgt/sm/viewsku.phtml?id="+productID+"&inwin=1', 'View SKU: "+productID+"','item_"+productID+"');\">View SKU</a>";
    document.getElementById("menuOption2").innerHTML = "<a href=\"javascript:e2window('/mgt/sm/editsku.phtml?id="+productID+"&inwin=1', 'Edit SKU: "+productID+"','item_"+productID+"');\">Edit SKU</a>";
    document.getElementById("menuOption3").innerHTML = "<a href=\"javascript:void(0);\" onclick=\"javascript:addItem2PO('"+productID+"')\">Add to PO</a>";
    document.getElementById("menuOption4").innerHTML = "<a href=\"javascript:void(0);\" onclick=\"javascript:addItem2ISOBLUE('"+productID+"')\">Add to ISO BD</a>";
}
function hideMenu()
{
    var dlg = document.getElementById("dialog");
    dlg.style.display = "none";
}
function showPriceLevels(pid) {
    $('#all_pricelevels_'+pid).toggle();
}

function updateCost_all(costType, pid) {
	var pl_level_arr = costType.split('_');
	var pl_level = pl_level_arr[1];
	document.getElementById(costType+"-"+pid).style.background = '#ffeeee';
	if(pl_level == 'pl'+fwsprice) {
		document.getElementById("color-"+pid+"-fws").style.background = '#ffeeee';
	}
	$.get('/mgt/pereport_new/ajax_saveprice.phtml',{ 'pid': pid,'price':$('#'+costType+"-"+pid).val(),'costtype':pl_level,'sid':Math.random()}, function(data) {
		document.getElementById(costType+"-"+pid).style.background = '#ffffff';
		if(pl_level == 'pl'+fwsprice) {
			changed_fws_price = $('#'+costType+"-"+pid).val();
			updateFWSTable(pid);
			document.getElementById("color-"+pid+"-fws").style.background = '#ffffff';
		}
	});
}
function updateCost_allOver(pid) {
	$.get('/mgt/pereport_new/ajax_saveprice_all.phtml',{ 'pid': pid,'price':$('#all_over_pl_'+pid).val(),'sid':Math.random()}, function(data) {
		$('.plBox'+pid).each(function(){
			if($(this).attr('id') != 'pl6-'+pid) {
				$(this).val($('#all_over_pl_'+pid).val());
			}
		});
		changed_fws_price = $('#all_over_pl_'+pid).val();
		updateFCO_all(pid);
		updateFBATable(pid);
//		updateAMZTable(pid);
//		updateFWSTable(pid);
	});
}
function updateCost(costType, pid)
{
	// scost, myprice, amprice
	var data = $("#"+costType+"-"+pid).val();
	
	document.getElementById(costType+"-"+pid).style.background = '#ffeeee';
	
	if(costType ==  'pl'+fprice)
	    document.getElementById("color-"+pid).style.background= '#ffeeee';
//	else
//	    document.getElementById("color-"+pid+"-amz").style.background= '#ffeeee';
	
	$.get('/mgt/pereport_new/ajax_saveprice.phtml',{ 'pid': pid,'price':data,'costtype':costType,'sid':Math.random()}, function(data) {
	    if(costType === 'avgcost') {
		$('#product_cost_'+pid).val(newCost);
//		$('#amz'+pid+'_cost').html(newCost);
	    }
		updateFBATable(pid);
//		updateAMZTable(pid);
//		updateFWSTable(pid);;

	    document.getElementById(costType+"-"+pid).style.background = '#ffffff';
	    document.getElementById("color-"+pid).style.background= '#ffffff';
//	    document.getElementById("color-"+pid+"-amz").style.background= '#ffffff';
	});
}

function updateFCO_all(pid) {
	if($('#all_over_c_pl-'+pid).is(':checked')) {
	    var override = 1;
	    $('.pl_checkbox'+pid).each(function(){
		if($(this).attr('id') != 'fprice-'+pid) {
		    $(this).attr('checked', true);
		}
	    });
	}
	else{
	    var override = 0;
	    $('.pl_checkbox'+pid).each(function(){
		if($(this).attr('id') != 'fprice-'+pid) {
		    $(this).attr('checked', false);
		}
	    });
	}
	$.get('/mgt/pereport_new/ajax_updatefco_all.phtml',{ 'pid': pid,'override':override,'sid':Math.random()}, function(data) {
		updateFBATable(pid);
		/*updateAMZTable(pid);
		updateFWSTable(pid);*/
	});
}
function updateFCO(priceName, pid, pl) //'fprice'
{
    var checkbox = document.getElementById(priceName+"-"+pid);
    if(checkbox.checked)
    {
	$.get('/mgt/pereport_new/ajax_updatefco.phtml',{ 'pid': pid,'pl': pl,'override':1,'sid':Math.random()}, function(data) {
	    updateFWSTable(pid);
	});
    }
    else
    {
	$.get('/mgt/pereport_new/ajax_updatefco.phtml',{ 'pid': pid,'pl': pl,'override':0,'sid':Math.random()}, function(data) {
	    updateFWSTable(pid);
	});			

    }
}

function addSourcing(pid)
{
	document.addSourcingForm.sid.value = "";
//	document.addSourcingForm.sourcing_pid.value = pid;
	$('#sourcing_pid').val(pid);
	var pos = $('#addsourcing_'+pid).offset();
	
	var dlg = document.getElementById("sourcingForm");
	dlg.style.display = "block";
	$("#sourcingForm").offset({ top: pos.top, left: pos.left+20});
}
function hideSourcingForm()
{
	$('#sourcing_pid').val('');
	var dlg = document.getElementById("sourcingForm");
	dlg.style.display = "none";
}
function addSourcingValues() {
	var vendorid = $('#sourcing_vendorid').val();
	var sourcing_manpart = $('#sourcing_manpart').val();
	var sourcing_cost = $('#sourcing_cost').val();
	var sourcing_override_price = $('#sourcing_override_price').val();
	var sourcing_distsku = $('#sourcing_distsku').val();
	var sourcing_distqty = $('#sourcing_distqty').val();
	var pid = $('#sourcing_pid').val();
	
	if(document.addSourcingForm.sourcing_stealth.checked)
		sourcing_stealth = 1;
	else
		sourcing_stealth = 0;
		
	$.get('ajax_add_sourcing.phtml',
			{ 
				action: "AddSourcing", 
				'vendorid': vendorid,
				'manpart': sourcing_manpart,
				'distsku': sourcing_distsku,
				'cost': sourcing_cost,
				'distqty': sourcing_distqty,
				'id': pid,
				'stealth': sourcing_stealth,
				'oprice': sourcing_override_price
			}
			, function(data) {
		$('#new_sourcing_table').show();
		$('#new_sourcing_table tr:last').after(data);
		hideSourcingForm();
	});
}
function resetCostManual(pid)
{
	var newcost = parseFloat($("#manual_cost_"+pid).val());
	if(document.getElementById("results_"+pid))
	{
		var table = document.getElementById("results_"+pid);
		
		for(xx = 2; xx < table.rows.length; xx++)
		{
			table.rows[xx].cells[7].innerHTML = newcost;
		}
	}
		
	recalcTable(pid);
}
function resetCost(pid, newcost)
{
//	if(document.getElementById("results_"+pid))
//	{
//		var table = document.getElementById("results_"+pid);
//		
//		for(xx = 2; xx < table.rows.length; xx++)
//		{
//			table.rows[xx].cells[7].innerHTML = newcost;
//			fba159620-cost
//			document.getElementById("fba"+pid+"-cost").innerHTML = newcost;
//		}
//	}
//	document.getElementById("fba"+pid+"-cost").innerHTML = newcost;	
	$('#product_cost_'+pid).val(newcost);
	recalcTable(pid);
}
function recalcTable(pid)
{
//	if(document.getElementById("results_"+pid))
//	{
//		var table = document.getElementById("results_"+pid);
//						
//		for(xx = 2; xx < table.rows.length; xx++)
//		{
//			var price = table.rows[xx].cells[4].innerHTML;
//			var newprice = parseFloat(price) - af;
//			table.rows[xx].cells[6].innerHTML = newprice.toFixed(2);
//							
//			//var cost = parseFloat(getCost(pid));
//			var cost = parseFloat( table.rows[xx].cells[7].innerHTML );
//							
//			/////////////////////////////////////////////////////////////////////////////////////////
//			// amazon fees
//			var amzfee = 0.00;
//			
//			if(newprice <= 299.99)
//				amzfee += 1.00;
//			else if(newprice > 300)
//				amzfee = 0;
//								
//			amzfee += 1; // per order fee
//
//			if( parseInt( document.getElementById("amz"+pid+"-oversized").innerHTML ) > 0 )
//			{
//				amzfee += 5;
//			}
//				
//			//var weightDIV = document.getElementById("weight-"+pid);
//			//var weight = parseFloat(weightDIV.innerHTML);
//			var weight = getWeight(pid);
//							
//			amzfee += weight * .37;
//							
//			var pct = parseFloat( document.getElementById("amz"+pid+"-amazonpct").innerHTML ) / 100;
//							
//			amzfee += newprice * pct;
//							
//			table.rows[xx].cells[8].innerHTML = amzfee.toFixed(2);
//			/////////////////////////////////////////////////////////////////////////////////////////
//			var profit = newprice - cost - amzfee;
//			table.rows[xx].cells[9].innerHTML = "$"+profit.toFixed(2);
//			/////////////////////////////////////////////////////////////////////////////////////////
//			var margin = 100 * (profit / newprice);
//			table.rows[xx].cells[10].innerHTML = margin.toFixed(2)+"%";
//							
//			//////////////////////////////////////////////////////////////////////////////////////////////////////
//			//////////////////////////////////////////////////////////////////////////////////////////////////////
//			// Now to the Amazon portion of the Show
//			//////////////////////////////////////////////////////////////////////////////////////////////////////
//			//////////////////////////////////////////////////////////////////////////////////////////////////////
//			table.rows[xx].cells[12].innerHTML = cost.toFixed(2);
//							
//			amzfee = newprice * pct;
//			amzfee += getShipping(pid); // awww shit
//			table.rows[xx].cells[13].innerHTML = amzfee.toFixed(2);
//							
//			var profit = newprice - cost - amzfee;
//			table.rows[xx].cells[14].innerHTML = "$"+profit.toFixed(2);
//			/////////////////////////////////////////////////////////////////////////////////////////
//			var margin = 100 * (profit / newprice);
//			table.rows[xx].cells[15].innerHTML = margin.toFixed(2)+"%";
//		}
//	}
//					
	updateFBATable(pid);
//	updateAMZTable(pid);	
//	updateFWSTable(pid);	
}
function saveCasePack(pid) {
    $('#case_pack_btn_'+pid).hide();
    var cf_val = $('#case_pack_'+ pid +'').val();
    $.get('/mgt/pereport_new/ajax_savecustom_field.phtml',{ 'action':'save_case_pack','cfid':'66','id':pid,'cf_val':cf_val}, function(data) {
		$('#case_pack_btn_'+pid).show();
    });
}
function updatePricenote(pid) {
    var minqty = $('#pricenote_'+pid).val();
    $.get('/mgt/pereport/ajax_savecustom_field.phtml',{ 'action':'save_pricenote','id':pid,'minqty':minqty}, function(data) {

    });
}

function saveSuperPricer(pid) {	
    $('#minprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#maxprice_amazon_'+pid).css('background-color','#ffeeee');
    $('#profile_amazon_'+pid).css('background-color','#ffeeee');
    $('#managed_amazon_'+pid).css('background-color','#ffeeee');
    
    var minprice_amazon = $('#minprice_amazon_'+pid).val();
    var maxprice_amazon = $('#maxprice_amazon_'+pid).val();
    var profile_amazon = $('#profile_amazon_'+pid).val();
    if($('#managed_amazon_'+pid).is(':checked')) {
        managed_amazon = 1;
    }
    else {
        managed_amazon = 0;
    }
    $.post('/mgt/sm/ajax_save_superpricer.phtml', {
                'skus': pid,
                'minprice_amazon': minprice_amazon,
                'maxprice_amazon': maxprice_amazon,
                'profile_amazon': profile_amazon,
                'managed_amazon': managed_amazon
            }, function(res) {
                $('#minprice_amazon_'+pid).css('background-color','#ffffff');
                $('#maxprice_amazon_'+pid).css('background-color','#ffffff');
                $('#profile_amazon_'+pid).css('background-color','#ffffff');
                $('#managed_amazon_'+pid).css('background-color','#ffffff');
        });
}
function getListsByGroup() {
    var group_id = $('#groupid').val();
    $.get('/mgt/sm/ajax_get_lists_by_group.phtml',{ 'group_id' : group_id }, function(data) {
	    $('.listid option').remove();
	    $('.listid').append(data);
    });
}
function addItemToList(pid) {
	var listId = $('#lid_'+pid).val();
	$.get('ajax_get_lists.phtml',{ action: "add_item",'id': pid,'lid':listId}, function(data) {
		loadItemLists(pid);
	});
}
function loadItemLists(pid) {
	$.get('ajax_get_lists.phtml',{ action: "get_lists",'id': pid}, function(data) {
		$('#listDisplay_'+pid).html(data);
	});
}
function confirmDeleteList(msg,lid,id) {
	var answer = confirm(msg)
	if (answer)
	{
		$.get('ajax_get_lists.phtml',{ action: "DeleteList",'id': id,'lid':lid}, function(data) {
			loadItemLists(id);
		});
	}
}
function getSettlementData(pid,account) {
    $('#settelemnt_data_'+pid).html('<img src="/mgt/images/loading.gif" />');
    $.get('ajax_getsettlementdata.phtml',
            { 
                "pid": pid, 
                "account":account
            }
            , function(data) {
                $('#settelemnt_data_'+pid).html(data);
    });
}

function showFBAOrdersShipped(pid) {
	if($('#fbaordersshipped-'+pid).html() == '') {
		$.get('ajax_getfbaorders_shipped.phtml',{ 'pid': pid}, function(data) {
			$('#fbaordersshipped-'+pid).html(data);
			$('#fbaordersshipped-'+pid).toggle();
			if($('#fbaordersimgshipped-'+pid).attr('src') == '/mgt/images/minus.gif') {
				$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/plus.gif')
			}
			else {
				$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/minus.gif')
			}
		});
	}
	else {
		$('#fbaordersshipped-'+pid).toggle();
		if($('#fbaordersimgshipped-'+pid).attr('src') == '/mgt/images/minus.gif') {
			$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/plus.gif')
		}
		else {
			$('#fbaordersimgshipped-'+pid).attr('src','/mgt/images/minus.gif')
		}
	}
}
function showShippedOrders(pid) {
	if($('#shippedorders-'+pid).html() == '') {
		$.get('ajax_getshippedorders.phtml',{ 'pid': pid}, function(data) {
			$('#shippedorders-'+pid).html(data);
			$('#shippedorders-'+pid).toggle();
			if($('#shippedordersimg-'+pid).attr('src') == '/mgt/images/minus.gif') {
				$('#shippedordersimg-'+pid).attr('src','/mgt/images/plus.gif')
			}
			else {
				$('#shippedordersimg-'+pid).attr('src','/mgt/images/minus.gif')
			}
		});
	}
	else {
		$('#shippedorders-'+pid).toggle();
		if($('#shippedordersimg-'+pid).attr('src') == '/mgt/images/minus.gif') {
			$('#shippedordersimg-'+pid).attr('src','/mgt/images/plus.gif')
		}
		else {
			$('#shippedordersimg-'+pid).attr('src','/mgt/images/minus.gif')
		}
	}
}
function showFBAOrders(pid,amazon_account)
{
	if(parseInt(document.getElementById("fbaordersshowing-"+pid).value) == 0)
	{
		document.getElementById("fbaordersshowing-"+pid).value = "1"
		document.getElementById("fbaordersimg-"+pid).src = "/mgt/images/minus.gif";
		document.getElementById("fbaorders-"+pid).innerHTML = "<img src='/mgt/images/loading.gif'>";
		
		/////////////////////////////////////////////////
		xml9Http=GetXmlHttpObject();
			
		var url = "/mgt/pereport_new/ajax_getfbaorders.phtml?pid="+pid+"&amazon_account="+amazon_account+"&sid="+Math.random();
		
		xml9Http.onreadystatechange=gotFBAOrders; // throw away the result..... the pool will catch the price change
		xml9Http.open("GET",url,true);
		xml9Http.send(null);	
	}
	else
	{
		document.getElementById("fbaordersshowing-"+pid).value = "0"
		document.getElementById("fbaorders-"+pid).innerHTML = "&nbsp;";
	}
	
}

function gotFBAOrders()
{
	if(xml9Http.readyState==4)
	{ 
		var data = xml9Http.responseText.split("~");
		document.getElementById("fbaorders-"+data[0]).innerHTML = data[1];
	}
}
function showPOHistory(pid,amazon_account)
{
	if(parseInt(document.getElementById("pohistoryshowing-"+pid).value) == 0)
	{
		document.getElementById("pohistoryshowing-"+pid).value = "1"
		document.getElementById("pohistoryimg-"+pid).src = "/mgt/images/minus.gif";
		document.getElementById("pohistory-"+pid).innerHTML = "<img src='/mgt/images/loading.gif'>";
		
		/////////////////////////////////////////////////
		xml9Http=GetXmlHttpObject();
			
		var url = "/mgt/pereport_new/ajax_getpohistory.phtml?pid="+pid+"&amazon_account="+amazon_account+"&sid="+Math.random();
		
		xml9Http.onreadystatechange=gotPOHistory; // throw away the result..... the pool will catch the price change
		xml9Http.open("GET",url,true);
		xml9Http.send(null);	
	}
	else
	{
		document.getElementById("pohistoryshowing-"+pid).value = "0"
		document.getElementById("pohistory-"+pid).innerHTML = "&nbsp;";
	}
	
}

function gotPOHistory()
{
	if(xml9Http.readyState==4)
	{ 
		var data = xml9Http.responseText.split("~");
		$('#showMorePOS-'+data[0]).before(data[1]);
		document.getElementById("pohistory-"+data[0]).innerHTML ='';
	}
}

function getWeight(pid)
{// compute the shipping based on the config values in Site Config->AmazonGrid
	var shipweight = 0;
//	if(useweight.toUpperCase() == "A") // use amazon
//	{
//	    var tmpweight = document.getElementById("weight-"+pid).innerHTML;
//	    if(tmpweight == '') {
//		    tmpweight = 0.00;
//	    }
//	    shipweight = Math.ceil( parseFloat( tmpweight  ) );
//	}
//	else if(useweighttoUpperCase() == "B") // use stored
//	{
//		shipweight = Math.ceil( parseFloat( document.getElementById("amz"+pid+"-weight").innerHTML ) );
//	}
//	else if(useweight.toUpperCase() == "C")
//	{
//		shipweight = Math.ceil( parseFloat( document.getElementById("amz"+pid+"-weight").innerHTML ) );
//		if(shipweight <= 0.00)
//			shipweight = Math.ceil( parseFloat( document.getElementById("weight-"+pid).innerHTML ) );
//	}
//	else if(useweight.toUpperCase() == "D")
//	{
//	    shipweight = parseFloat( document.getElementById("weight-"+pid).innerHTML );
//	    if(shipweight <= 0.00)
//		shipweight = parseFloat( document.getElementById("amz"+pid+"-weight").innerHTML );
//	}
	shipweight = parseFloat( document.getElementById("weight-"+pid).innerHTML );
	return shipweight;
}

function updateFBATable(pid)
{
	var amzover = 0.00;
//	amzover = parseFloat( document.getElementById("fba"+pid+"-amzoversize").innerHTML );
	
	var textBox = document.getElementById("pl"+fprice+"-"+pid);
	var newprice = parseFloat( textBox.value );
	
	document.getElementById("fba"+pid+"-price").innerHTML = newprice;
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	var cost = $('#product_cost_'+pid).val();
//	if(document.getElementById("results_"+pid))
//	{
//		var table = document.getElementById("results_"+pid);
//		
//		for(xx = 2; xx < table.rows.length; xx++)
//		{
//			cost = parseFloat( table.rows[xx].cells[7].innerHTML )
//		}
//	}
	
//	document.getElementById("fba"+pid+"-cost").innerHTML = cost.toFixed(2);
	document.getElementById("fba"+pid+"-cost").innerHTML = cost;
	//////////////////////////////////////////////////////////////////////////////////////////////
	var weight = parseFloat(getWeight(pid));
	var debug = '';						
	var fullfee = 0.00;
    
	weight += 0.25;
	weight = Math.ceil(weight);
	if(weight<=1) {
	    fullfee = 2.48;
	}
	else if(weight>1 && weight<= 2 ) {
	    fullfee = 3.36;
	}
	else if(weight>2) {
	    fullfee = 3.36+((weight-2)*0.39);
	}

	if(newprice > 300)
	    {
	    fullfee = 0;
	}
    
    if( parseInt( document.getElementById("amz"+pid+"-oversized").innerHTML ) > 0 )
    {
	var weight_tmp2 = Math.ceil(weight+1);
	if(weight_tmp2<= 2) {
	    fullfee = 5.37;
	}
	else {
	    fullfee = 5.37+(weight*0.39);
	}
//		fullfee += 3;
	    debug += "Oversized Adding $5<BR>";
    }	
    else
    {
	    debug += "Not Oversized<BR>";
    }
    document.getElementById("fba"+pid+"-ship").innerHTML = fullfee.toFixed(2);
    ////////////////////////////////////////////////////////////////////////////////////////

    var pct = parseFloat( document.getElementById("amz"+pid+"-amazonpct").innerHTML ) / 100;

    amazonpct = newprice * pct;
    if(parseFloat(amazonpct) < 1) {
	amazonpct = 1.00;
    }
    document.getElementById("fba"+pid+"-amzfee").innerHTML = amazonpct.toFixed(2);

    /////////////////////////////////////////////////////////////////////////////////////////
    var profit = newprice - cost - fullfee - amazonpct-amzover;

    if(profit >= 0)
	    document.getElementById("fba"+pid+"-profit").innerHTML = "<B><font color='#00ff00'>$"+profit.toFixed(2)+"</font></B>";
    else
	    document.getElementById("fba"+pid+"-profit").innerHTML = "<B><font color='#ff0000'>$"+profit.toFixed(2)+"</font></B>";


    var margin = 100 * (profit / newprice);

    if(margin >= 0)
	    document.getElementById("fba"+pid+"-margin").innerHTML = "<B><font color='#00ff00'>"+margin.toFixed(2)+"%</font></B>";
    else
	    document.getElementById("fba"+pid+"-margin").innerHTML = "<B><font color='#ff0000'>"+margin.toFixed(2)+"%</font></B>";


    document.getElementById("fba"+pid+"-profit2").innerHTML = "$"+profit.toFixed(2);
}

function showMoreOffers (pid) {
    $('.sellerTr'+pid).show();
    $('#showMore'+pid).html('');
}

function addItem2PO(productID)
{
	xml7Http=GetXmlHttpObject();

	var url="/mgt/sm/ajax_additem2po.phtml?pid="+productID+"&sid="+Math.random();
	xml7Http.onreadystatechange=poResult;
	xml7Http.open("GET",url,true);
	xml7Http.send(null);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// animation stuff
	var productimage = document.getElementById("productimage");

	productimage.innerHTML = "<center><img src=\"/getimage.phtml?id="+productID+"&itype=0&thumb=1\" border=0 width=50></center>";

	productimage.style.display = "block";

	productimage.style.left = posX;
	productimage.style.top = posY;

	paX = posX;
	paY = posY;
	posArray = findPos(document.getElementById("orderdisplay"));
	dX = posArray[0];

	setTimeout("animateCart()", 0);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return false;
}
function poResult()
{
	if(xml7Http.readyState==4)
	{
		document.getElementById("podisplay").innerHTML = xml7Http.responseText;

		document.getElementById("podisplay").style.backgroundColor = "#ffcccc";
		setTimeout("unColorPODisplay()", 2000);

		document.getElementById("poimage").style.display = "";
	}
}
function unColorPODisplay()
{
	document.getElementById("podisplay").style.backgroundColor = "#ffffff";
}

function animateCart()
{
	if(paX < dX)
	paX = paX + 20;

	paY = paY - 20;

	var productimage = document.getElementById("productimage");
	productimage.style.left = paX;
	productimage.style.top = paY;

	if(paY > -75)
	{
		setTimeout("animateCart()", 0);
	}
	else
	{
		productimage.style.display = "none";
	}
}

function updateOversized(pid) {
    var over = parseInt( document.getElementById("amz"+pid+"-oversized").innerHTML);
    if(over == 0) {
    	var new_over = 1;
    }
    else {
    	var new_over = 0;
    }
    $.get('/mgt/pereport_new/ajax_savecustom_field.phtml',{ 'action':'save_oversized','id':pid,'new_val':new_over}, function(data) {
		document.getElementById("amz"+pid+"-oversized").innerHTML = new_over;
		if(new_over == 0) {
	    	document.getElementById("xvalover_"+pid).innerHTML = '';
	    	document.getElementById("fba"+pid+"-amzoversize").innerHTML = '0.15';
	    }
	    else {
	    	document.getElementById("xvalover_"+pid).innerHTML = '<span style="color:red;">X</span>';
	    	document.getElementById("fba"+pid+"-amzoversize").innerHTML = '1.30';
	    }
		updateFBATable(pid);
    });
}

function stockDetail(pid, type)
{
	e2window("/mgt/sm/stockdetail.phtml?pid="+pid+"&type="+type+"&inwin=1", "Stock Detail "+pid);
}

function changeAvgCostSource(pid)
{
//	if(document.myform.use_avgcost.options[ document.myform.use_avgcost.selectedIndex ].value == "AVGCOST")
	if($('#use_avgcost_'+pid).val() == "AVGCOST")
	{
		document.getElementById("warehouselist_"+pid).style.display = "none";
		document.getElementById("vendorlist_"+pid).style.display = "none";
	}
	else if($('#use_avgcost_'+pid).val() == "WAREHOUSE")
	{
		document.getElementById("warehouselist_"+pid).style.display = "block";
		document.getElementById("vendorlist_"+pid).style.display = "none";
	}
	else if($('#use_avgcost_'+pid).val() == "VENDOR")
	{
		document.getElementById("warehouselist_"+pid).style.display = "none";
		document.getElementById("vendorlist_"+pid).style.display = "block";
	}
}

function updateAutopricer(pid) {
    var aggressive_factor = $('#aggressive_factor_'+pid).val();
    var minimum_profit = $('#minimum_profit_'+pid).val();
    var profit_type = $('#profit_type_'+pid).val();
    if($('#add_fba_cost_'+pid).is(':checked')) {
	var add_fba_cost = 1;
    }
    else {
	add_fba_cost = 0;
    }
    var maximum_profit = $('#maximum_profit_'+pid).val();
    var max_profit_type = $('#max_profit_type_'+pid).val();
    var commission = $('#commission_'+pid).val();
    var ceiling_price = $('#ceiling_price_'+pid).val();
    if($('#fight_amazon_'+pid).is(':checked')) {
	var fight_amazon = 1;
    }
    else {
	fight_amazon = 0;
    }
    var minprice = $('#minprice_'+pid).val();
    var use_avgcost = $('#use_avgcost_'+pid).val();
    var warehouseID = $('#warehouseID'+pid).val();
    var vendorID = $('#vendorID_'+pid).val();
    $('#loading_autopricer_'+pid).show();
    $.get('ajax_save_autopricer.phtml',
            { 
                "id": pid, 
                "aggressive_factor":aggressive_factor,
                "minimum_profit":minimum_profit,
                "profit_type":profit_type,
                "add_fba_cost":add_fba_cost,
                "maximum_profit":maximum_profit,
                "max_profit_type":max_profit_type,
                "commission":commission,
                "ceiling_price":ceiling_price,
                "fight_amazon":fight_amazon,
                "minprice":minprice,
                "use_avgcost":use_avgcost,
                "warehouseID":warehouseID,
                "vendorID":vendorID,
                "Action":'Update Autoprice Values'
            }
            , function(data) {
//                $('#settelemnt_data_'+pid).html(data);
		$('#loading_autopricer_'+pid).hide();
    });
    
}

function flushAmzPrice(pid) {
    $('#loading_autopricer_'+pid).show();
    $.get('/mgt/pereport_new/ajax_savecustom_field.phtml',{ 'action':'flush_amz_price','id':pid}, function(data) {
	$('#loading_autopricer_'+pid).hide();
	$('#autopricer_details_'+pid).html('');
    });
}