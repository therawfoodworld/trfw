-- MySQL dump 10.9
--
-- Host: localhost    Database: uclick
-- ------------------------------------------------------
-- Server version	4.1.8-standard

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `mid` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  `parent` bigint(20) NOT NULL default '0',
  `public` int(11) NOT NULL default '0',
  `dorder` int(11) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--


/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
LOCK TABLES `menu_items` WRITE;
INSERT INTO `menu_items` VALUES (1,'Home','/mgt/main.phtml',0,0,0),(2,'Config','',0,0,1),(3,'Edit Menu','/mgt/menu/default.phtml',2,0,0),(4,'Marketing','',0,0,6),(5,'Datafeed Manager','/mgt/df/default.phtml',4,0,0),(6,'Orders','',0,0,3),(16,'Search','/mgt/cm/search.phtml',15,0,0),(8,'Help','',0,0,8),(9,'Manage Help','/mgt/help/help.phtml',8,0,0),(10,'Email Marketing','/mgt/em/default.phtml',4,0,0),(11,'SKUs','/mgt/sm/default.phtml',0,0,4),(12,'Search','/mgt/sm/search.phtml',11,0,0),(15,'Customers','',0,0,2),(14,'Search','/mgt/om/search.phtml',6,0,0),(17,'Site Configuration','/mgt/cf/default.phtml',2,0,0),(18,'Shipping','/mgt/cf/shipping.phtml',2,0,0),(19,'Order Flow','/mgt/cf/orderflow.phtml',2,1,0),(20,'Email Templates','/mgt/cf/emailtemplates.phtml',2,0,0),(21,'Vendors','',0,0,5),(22,'Vendor Search','/mgt/vn/search.phtml',21,0,0),(23,'PO Search','/mgt/vn/po_search.phtml',21,0,0),(24,'Ship','/mgt/ship/default.phtml',0,0,6),(25,'Ready to Ship','/mgt/ship/default.phtml',24,0,0),(26,'Ship Orders','/mgt/ship/shiporders.phtml',24,0,0),(27,'Category Manager','/mgt/cf/categories.phtml',2,0,0),(40,'Action Log','/mgt/cf/action_log.phtml',33,0,0),(31,'Ingram Markup','/mgt/cf/ingram_markup.phtml',2,0,0),(32,'Featured Products','/mgt/sm/featured_products.phtml',11,0,0),(33,'Reports','',0,0,7),(34,'Traffic','/mgt/t/',33,0,0),(35,'* Click Summary','/mgt/t/default.phtml',34,0,0),(36,'Geographic Summary','/mgt/t/default.phtml?r=summary_location',34,0,0),(37,'Exit Pages','/mgt/t/default.phtml?r=exit_pages',34,0,0),(38,'Click Paths','/mgt/t/default.phtml?r=click_paths',34,0,0),(39,'Search Terms Internal','/mgt/t/default.phtml?r=search_terms_internal',34,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

