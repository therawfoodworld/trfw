<PRE>
<?php

REQUIRE($_SERVER['DOCUMENT_ROOT']."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

/******************************************************************************************/
////////////////////////////////////////////////////////////////////////////////////////////
// Login Stuff
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");
REQUIRE($ROOT_DIR."/mgt/auth.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
		$username = $efc->getConfigValue("DOBA", "XML_USER");
		$password = $efc->getConfigValue("DOBA", "XML_PASS");
		$retailerid = $efc->getConfigValue("DOBA", "RETAILER_ID");

////////////////////////////////////////////////////////////////////////////////////////////

$URL = "https://sandbox.doba.com/soap/wsdl/ApiRetailerOrder.wsdl";

$client = new SoapClient($URL, array("trace" => 1));
$objAuth = new stdClass();
$objAuth->username = $username;
$objAuth->password = $password;
$objRequest = new stdClass();
$objRequest->authentication = $objAuth;
$objRequest->retailer_id=$retailerid;
$objRequest->fund_method="default_payment_account";
$objRequest->cvv2="111";
$objRequest->order_ids=Array("326099");

try { $Response = $client->fundOrder($objRequest); }
catch ( SOAPFault $e ) {$Response = ("Error ".$e->faultcode.": ".$e->faultstring); }

print "URL = $URL\n\n";
print "Method = fundOrder\n\n";
print "Request:\n=====================================================\n\n";
print_r($objRequest);
print "\n\nResponse:\n=====================================================\n\n";
print_r($Response);
?>
