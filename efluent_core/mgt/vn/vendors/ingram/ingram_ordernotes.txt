carrier code 
	(pg 7)
auto release
	0 - Finalize (stock allocated and held till end of bizday)
	1 - Finalize and Release (allocated and shipped)
	H - Hold

third party freight account
	Note: For FedEx Air only (carrier codes F1, FO, F2, FG.),
		please send three leading zeros before your
		thirdPartyFreightAccount number(i.e: 000999999999.

backorder flag
	1. Y Allow back orders and ship as inventory becomes available (possible multiple shipments).
	2. N Do not allow back order.
	3. C Allow back orders but ship when all products become available at the warehouse. 	
	4. E Complete order. This is similar to the C option, but for multiple warehouses. 
		The order will not be shipped until all products from all warehouses become available.
	
SplitShipmentFlag
	Y Allow split shipments.
	N Do not allow split shipments.

SplitLine
	1. Y Allow split lines.
	2. N Do not allow split lines.

DynamicMessage

Government/Educational
	1) GovProgramType = PA = GOVERNMENT  ED = EDUCATION -
	2) GovEndUserType = GOVT = 'F' 'S' 'L' - ED = 'K' 'H'
		F = Federal
		S = State
		L = Local
		K = K-12 school
		H = Higher Education
	3) GovSolicitationNBR = GOV - ED CONTRACT NUMBER
	4) GovEndUserData = EXAMPLES STATE OF OHIO, CHICAGO SCHOOL DISTRICT ETC
	5) GovEndUserZip = 9 CHAR FIELD / ZIP CODE OF END USER OF ORDER
	6) GovPublicPrivateSW = DETERMINES TAX / NO TAX.
		'P' PUBLIC SECTOR 'R' PRIVATE SECTOR. Value needs only to be provided for
		EDUCATION orders.

----------------------------------------------------------------------------------------------------------


TEST Orders should be auto-release => "H"
CustomerPO should be "TEST ONLY"
