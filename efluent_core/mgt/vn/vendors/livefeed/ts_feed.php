<?php


class WSSoapClient extends SoapClient
{

	public $wsHeader = null;
	const wsSecurityXsd = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
	const wsSecurityHeader =
	'<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken>
                <wsse:Username>%s</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">%s</wsse:Password>
                <wsse:Nonce>%s</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%s</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>';

	public function setUsernameToken($username, $password)
	{
		$ts = '2010-02-08T15:36:29.502Z';
		$rand = mt_rand();
		$digest = base64_encode(
		pack('H*', sha1(pack('H*', $rand)
		. pack('a*', $ts)
		. pack('a*', $password))
		)
		);
		$nonce = base64_encode(pack('H*', $rand));
		$svar = new SoapVar(sprintf(self::wsSecurityHeader, $username, $password, $nonce, $ts), XSD_ANYXML);
		$this->wsHeader = new SoapHeader(self::wsSecurityXsd, 'Security', $svar, true);
		return $this;
	}

	public function __soapCall($function_name,$arguments,$options=null,$input_headers=null,$output_headers=null)
	{
		return  $result = parent::__soapCall($function_name,$arguments,$options,$this->wsHeader);
	}
}
		// TEST  ORDER !!!! /////////////////////////////      <----------------
		$wsdl = 'https://b2btest:9443/LiveFeed.svc?WSDL';
		$location = 'https://b2btest:9443/LiveFeed.svc';
		echo "WDSL: $wsdl\nLOCATION: $location\n";
		/////////////////////////////////////////////////      <---------------- 


$sClient = new WSSoapClient($wsdl, array('trace'=>1, 'location'=>$location));

$sClient->setUsernameToken('sales@giftjoy.net', '8Glo7R@Ze');

$chunk = $sClient->__soapCall('GetItemListInChunks', array('parameters'=>(array('language'=>'English', 'chunkSize'=>'500'))));
$transactionId = $chunk->GetItemListInChunksResult->TransactionId;
echo "downloaded: " . count($chunk->GetItemListInChunksResult->Items->ItemEntity) . "\n";
AddToDataBase($chunk->GetItemListInChunksResult->Items);



while (count($chunk->GetItemListInChunksResult->Items->ItemEntity) > 0) {
	echo "downloaded: " . count($chunk->GetItemListInChunksResult->Items->ItemEntity) . "\n";
	$chunk = $sClient->__soapCall('GetItemListInChunks', array('parameters'=>(array('language'=>'English', 'chunkSize'=>'500', 'transactionId'=>$transactionId))));
	AddToDataBase($chunk->GetItemListInChunksResult->Items,$mysqlid,$DB,$efc,$VENDORID);
    echo "--> print_r(chunk->GetItemListInChunksResult->Errors);    START\n";
	print_r($chunk->GetItemListInChunksResult->Errors);
    echo "--> print_r(chunk->GetItemListInChunksResult->Errors);    END\n";
}



function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}





function AddToDataBase($items) {

	$ix = 0;
	$display_count = 500;


	while($ix < $display_count)
	{

		unset($item_array);
		unset($items_array);
		unset($image_array);

		$img_c = 0;

		//echo "\n\nKEY==> #$ix\n\n";
		//sleep(2);

		$item_array = objectToArray($items->ItemEntity[$ix]);
		$items_array = objectToArray($items->ItemEntity[$ix]); // cause i keep mistype

		//print_r($item_array);
		//if($item_array[HtmlDescription] !="") { 		echo $item_array[HtmlDescription]."\n";   }

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		$salesdescr = addslashes($item_array[Description]);
		$manname = addslashes($item_array[Brand]);
		if($manname == "") { $manname = "NOT_SPECIFIED"; }
		$cost = addslashes($item_array[Price]);
		$map = addslashes($item_array[RecommendedPrice]);
		$sku = addslashes($item_array[Sku]); // leave it be.
		$manpart = $sku; // leave it be.
		$instock = addslashes($item_array[StockQuantity][Quantity]);
		$title = addslashes($item_array[Title]);
		$maintitle = addslashes($item_array[MainTitle]);
		$lbs = addslashes($item_array[Weight]);

		if($sku !="")
		{
			echo "[#$ix] API GAVE SKU==> '$sku'\n";

			$ix++; // <-- YAR HUH
		}
		else 
		{//blank SKU?

			print_r($item_array);
			exit;
			
		}

	}
}


?>