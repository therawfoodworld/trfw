<?php

// LIVE
/*$wsdl = 'https://b2b:9443/LiveFeed.svc?WSDL';
$location = 'https://b2b:9443/LiveFeed.svc';*/


// TEST
$wsdl = 'https://b2btest:9443/LiveFeed.svc?WSDL';
$location = 'https://b2btest:9443/LiveFeed.svc';

$sClient = new WSSoapClient($wsdl, array('trace'=>1, 'location'=>$location));

$sClient->setUsernameToken('sales@giftjoy.net', '8Glo7R@Ze');

$orderInformation = new OrderInformationEntity();
$orderInformation->BillingAddress1 = "Billing address";
$orderInformation->BillingTown = "Billing Town";
$orderInformation->BillingCounty = "Billing County";
$orderInformation->BillingPostCode = "Billing Post Code";
$orderInformation->BillingCountryCode = "US";
$orderInformation->BillingTelephone = "Billing Telephone";
$orderInformation->ShippingAddress1 = "Shipping Address1";
$orderInformation->ShippingTown = "Shipping Town";
$orderInformation->ShippingCounty = "Shipping County";
$orderInformation->ShippingPostCode = "Shipping Post Code";
$orderInformation->ShippingCountryCode = "US";
$orderInformation->ShippingTelephone = "Shipping Telephone";
$orderInformation->ShippingMethodCode = "MANUAL";
$orderInformation->CompanyName = "Company Name";
$orderInformation->AdditionalInformation = "-";




// create order line  #1
$orderLine1 = new OrderLineEntity();	
$orderLine1->Sku = '1100390/7';      //<--product being ordered.
$orderLine1->OrderedQuantity = 1;

// add it to array
$orderLines[] = $orderLine1;

// create order line #2  (multi products from vendor)
$orderLine2 = new OrderLineEntity();	
$orderLine2->Sku = '1098592/5';      //<--product being ordered.
$orderLine2->OrderedQuantity = 1;

// add it to array
$orderLines[] = $orderLine2;



// place order to livefeed 
$order = $sClient->__soapCall('PlaceOrder', array('parameters'=>(array('orderInformation'=>$orderInformation, 'orderLines'=>$orderLines))));



// print result, work wit it.
print_r($order->PlaceOrderResult);




class WSSoapClient extends SoapClient
{
    public $wsHeader = null;
    const wsSecurityXsd = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    const wsSecurityHeader =
        '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken>
                <wsse:Username>%s</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">%s</wsse:Password>
                <wsse:Nonce>%s</wsse:Nonce>
                <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">%s</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>';
 
    public function setUsernameToken($username, $password)
    {

        $ts = '2010-02-08T15:36:29.502Z';
        $rand = mt_rand();
        $digest = base64_encode(
            pack('H*', sha1(pack('H*', $rand)
                . pack('a*', $ts)
                . pack('a*', $password))
            )
        );
        
        echo "USER: $username\nPASS: $password\n"; 

        $nonce = base64_encode(pack('H*', $rand));
        $svar = new SoapVar(sprintf(self::wsSecurityHeader, $username, $password, $nonce, $ts), XSD_ANYXML);
        $this->wsHeader = new SoapHeader(self::wsSecurityXsd, 'Security', $svar, true);
        return $this;
    }
  
    public function __soapCall($function_name,$arguments,$options=null,$input_headers=null,$output_headers=null)
    {  
        return  $result = parent::__soapCall($function_name,$arguments,$options,$this->wsHeader);
    }	
}



class OrderInformationEntity {
	public $BillingAddress1;
	public $BillingTown;
	public $BillingCounty;
	public $BillingPostCode;
	public $BillingCountryCode;
	public $BillingTelephone;
	public $ShippingAddress1;
	public $ShippingTown;
	public $ShippingCounty;
	public $ShippingPostCode;
	public $ShippingCountryCode;
	public $ShippingTelephone;
	public $ShippingMethodCode;
	public $CompanyName;
	public $AdditionalInformation;
}

class OrderLineEntity {
	public $Sku;
	public $OrderedQuantity;
}


?>
