Item ordering workflow

Item stock information is not provided in real time, and stock status changes very rapidly, item ordering workflow is following:

1. You place order as described in Placing order. As a result, you receive order number from LiveFeed. If you have a webshop, you should inform customer, that his order will be confirmed in next few hours.

2. During next few hours you should periodically check order status. 
   You can check it using method described in Getting order by number or Getting orders by status. For possible order status values you should read section Getting orders by status.

3. If order status is confirmed, that means that items are reserved for you and you should arrange payment to VividGemz, also in case you have webshop, you can inform your client, 
   that order is confirmed. If VividGemz does not receive payment during agreed amount of time, your order and item reservations are deleted.



Value Description
Pending = 1 Order was craeted, but quantities are not confirmed yet.
Confirmed = 2 Order was confirmed, all requested quantities are reserved, waiting for payment.
PartialyConfirmed = 3 Not all items are in stock and you can see what quantities of what items are available.
Rejected = 4 Ordered items are not in stock, order is rejected.
Completed = 5 Payment for order is received, order is completed.
PendingForCanceling = 6 Order is asked to cancel.
Canceled = 7 Order is canceled.


[root@Development  /home/httpd/giftjoy.net/mgt/vn/vendors/livefeed]# php getorderstatus.phtml /home/httpd/giftjoy.net
USING: /home/httpd/giftjoy.net
CHECKING   poid: 7489  vref: 1095 vid: 143 inv: 1198548  distsku: MD01225984/5/White
STATUS: Pending
....FAIL!
[root@Development  /home/httpd/giftjoy.net/mgt/vn/vendors/livefeed]#





