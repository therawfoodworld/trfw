<?
/*

Outbound Files

Purchase Order Acknowledgement - The Purchase Order Acknowledgement (855X-01) document is used to acknowledge orders placed via an (850X-02) Purchase Order document. Every order placed via the FTP system will create an 855X-01 document.
Invoice - The Invoice (810X-01) document is an invoice and contains shipping and cost data for each shipment
Shipment Confirmation - The Shipment Confirmation (856X-01) document contains shipping data for each shipment. This is a supplement to the invoice 810X-01 and contains the same information
Order Cancel Acknowledgement - The Order Cancel Acknowledgement (865X-01) document is used to acknowledge order cancellations placed via an Order Cancellation (860X-02).
Order Cancel Notification - The 856X-01 document contains shipping data for each shipment. This is a supplement to the invoice 810X-01 and contains the same information

The Purchase Order Acknowledgement (855X-01)  --step1
The Invoice (810X-01)  --step2
The Shipment Confirmation (856X-01) -- This is a supplement to the invoice 810X-01 and contains the same information


*/

$typeref['855X-01'] = 'ACK';//'Purchase Order Acknowledgement';
$typeref['810X-01'] = 'INV';//'Invoice';
$typeref['856X-01'] = 'INV2'; //This is a supplement to the invoice 810X-01 and contains the same information';

$ROOT_DIR = "/home/httpd/ecomelectronics.com";
if($ROOT_DIR == ""){	die("usage: image_script ROOT_DIR\n"); }
REQUIRE($ROOT_DIR."/config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

//////////////////////////////////////////////////////////////////////////////////////////////////////
REQUIRE($ROOT_DIR."/mgt/classes/efluentclass.phtml");

$efc = new efluentClass();
$efc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$efc->db = $DB;
//////////////////////////////////////////////////////////////////////////////////////////////////////

//echo "DB: $DB - $MYSQL_DATABASE\n\n";


$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

$basedir = "$ROOT_DIR/mgt/vn/vendors/crw/xml/";

$d = dir($basedir);

while (false !== ($entry = $d->read()))
{
	if($entry != "." && $entry != "..")
	{
		$ext = substr($entry, 0, strrpos($entry, "."));

		$filename = "$basedir/$entry";

		$fp = fopen($filename, "r");
		if($fp == false){echo "ERROR OPENING FILE\n";return;}
		$data = "";
		while(!feof($fp)) {	$data .= fgets($fp, 4096); }
		fclose($fp);

		$string = xml2array($data, 1);

		//echo "\nSOF=>$entry\n\n";


		$type = $string[document][attr][type];
		$ref = $typeref[$type];

		//echo "TYPE: $type   REF: $ref\n";



		if($ref == "ACK")
		{

			echo "\n\nTYPE: $type   REF: $ref\n";
			$poid = $string[document][order][attr][id];
			echo "POID: $poid\nACKNOWLEDGED...\n";
			//print_r($string[document][order]);

			/*
			[order] => Array
			(
			[attr] => Array
			(
			[id] => 7484
			[order_date] => 2011-05-09- 14:03
			[ack_date] => 2011-05-10 11:57:39
			[ack_type] => AK
			)

			[order][attr][id];
			[order][attr][ack_date];
			[order][attr][ack_type];
			*/

			//print_r($string);
			$branchorderno = $poid;

			$query = "update vendor_pos set edixml = '1', vendorref = '$branchorderno', status = '1' where poid = '$poid' and status !='2	'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

		}



		if($ref == "INV")
		{

			echo "\n\nTYPE: $type   REF: $ref\n";
			$poid = $string[document][invoice][attr][order_id];
			echo "POID: $poid\n";

			//print_r($string);
			$tracking = $string[document][invoice][line][attr][tracking_number];

			/*
			[line] => Array
			(
			[attr] => Array
			(
			[id] => 1
			[uom] => EA
			[supplier_sku] => 10006
			[unit_product_item_charges] => 52.94
			[line_quantity] => 1
			[line_product_item_charges] => 52.94
			[line_total_cost] => 52.94
			[tracking_number] => 1ZJUSTTESTING
			[product_id] => TBD
			)

			*/


			$query = "select vendorid, poid, invoiceid from vendor_pos where vendorref = '$poid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($povendorid, $poid, $invoiceid) = mysql_fetch_array($qok);

			echo "vid: $povendorid  poid: $poid  inv: $invoiceid\n";


			$query = "select pid from vendor_po_lines where poid = '$poid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			while(list($zeepid) = mysql_fetch_array($qok)) { $zeepids[] = $zeepid; }


			$query = "select orderid from customer_orders where invoiceid = '$invoiceid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($on) = mysql_fetch_array($qok);

			echo "ON: $on\n";


			// DEAL WITH SHIPPING ....
			$status = "Shipped";
			foreach($zeepids as $pid)
			{
				echo "PID: $pid\n";

				if($status == "Shipped")
				{

					$qtyquery = "select qty_ordered from vendor_po_lines where poid = '$poid' and pid = '$pid'";
					$qiok = mysql_db_query($DB, $qtyquery, $mysqlid);
					while(list($qty_ordered) = mysql_fetch_array($qiok))
					{
						$query = "update vendor_po_lines set qty_shipped = '$qty_ordered' where poid = '$poid' and pid = '$pid'";
						if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

						$query = "update customer_order_lines set qty_shipped = '$qty_ordered' where orderid = '$on'  and prodid = '$pid' ";
						if(!$q22ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					}

				}
			}

			//  FINAL STEPS
			if($status == "Shipped")
			{

				$query = "select lineid, prodid from customer_order_lines where orderid = '$on' AND qty_ordered > qty_shipped";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok) <= 0)
				{

					// success
					echo "Success: order shipped\n";
					$query = "select customer_orders.status, company_orderflow.success from customer_orders, company_orderflow where customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
					if(!$qok_1 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($this_step, $success_step) = mysql_fetch_array($qok_1);

					//No works?
					$query = "update customer_orders set status = '$success_step' where orderid = '$on'";
					if(!$qok_2 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					//Works!
					$query = "update customer_orders set status = '5' where orderid = '$on'";
					if(!$qok_4 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


					$query = "update vendor_pos set status = '2' where poid = '$poid'";
					if(!$qok_3 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


				}
				else
				{
					echo "Failure: partially shipped\n";
					$report .= "Failure: partially shipped\n";
					$query = "select customer_orders.status, company_orderflow.failure from customer_orders, company_orderflow where customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
					if(!$qok_4 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($this_step, $failure_step) = mysql_fetch_array($qok);

					$query = "update customer_orders set status = '$failure_step' where orderid = '$on'";
					if(!$qok_5 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				}

			}




			// TRACKING ....  if a tracking # is there no matter what status says, we tag it to the order

			if($tracking !="")
			{
				//echo "TRACKING: $tracking\n";

				$on = $efc->getOrderNumber($invoiceid);
				$xshipdate = time();

				$query = "select prodid from customer_order_lines where orderid = '$on'";
				if(!$qok_BUG = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				while(list($prodid) = mysql_fetch_array($qok_BUG))
				{
					$query = "delete from customer_order_tracking_extended where prodid = '$prodid' and orderid = '$on' and trackno = '$tracking'";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

                	$query = "delete from customer_order_tracking where invoiceid = '$invoiceid' and trackno = '$tracking' and type = 'Ground'";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					$query = "insert into customer_order_tracking(invoiceid, trackno, type, carrier, shipdate, cost) ";
					$query .= "values('$invoiceid', '$tracking', 'Ground', '$carrier', '$xshipdate', '0.00') ";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$tid = mysql_insert_id();


					$serial = " ";
					//$query = "delete from customer_order_tracking_extended where prodif = '$prodid' and orderid = '$on'";
					//if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					$query = "insert into customer_order_tracking_extended(orderid, tid, trackno, prodid, serial) values('$on', '$tid', '$tracking', '$prodid', '$serial') ";
					if(!$qok_b = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					echo "TRACKING: $tracking  e2ORDER#: $on  PRODID: $prodid TRACKINGID: $tid\n";

				}
			}
			else
			{
				echo "NO TRACKING ???   :~(~~\n";

			}
		}






		if($ref == "INV2")
		{
			echo "\n\nTYPE: $type   REF: $ref\n";
			$poid = $string[document][order][attr][id];
			echo "POID: $poid\n";
			//print_r($string);

			//print_r($string[document][order][order_shipment][package]);

			$tracking = $string[document][order][order_shipment][package][attr][tracking];

			/*
			[order] => Array
			(
			[attr] => Array
			(
			[id] => 7484
			[order_date] => 2011-05-09- 14:03
			)

			[order_shipment] => Array
			(
			[attr] => Array
			(
			[id] => 105942350
			[supplier_invoice] => 105942350
			)

			[package] => Array
			(
			[attr] => Array
			(
			[ship_date] => 2011-05-10 13:13:00.000
			[carrier_code] => FEDP
			[tracking] => 1ZJUSTTESTING
			)

			)

			)


			*/


			$query = "select vendorid, poid, invoiceid from vendor_pos where vendorref = '$poid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($povendorid, $poid, $invoiceid) = mysql_fetch_array($qok);

			echo "vid: $povendorid  poid: $poid  inv: $invoiceid\n";


			$query = "select pid from vendor_po_lines where poid = '$poid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			while(list($zeepid) = mysql_fetch_array($qok)) { $zeepids[] = $zeepid; }


			$query = "select orderid from customer_orders where invoiceid = '$invoiceid'";
			if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
			list($on) = mysql_fetch_array($qok);

			echo "ON: $on\n";


			// DEAL WITH SHIPPING ....
			$status = "Shipped";
			foreach($zeepids as $pid)
			{
				echo "PID: $pid\n";

				if($status == "Shipped")
				{

					$qtyquery = "select qty_ordered from vendor_po_lines where poid = '$poid' and pid = '$pid'";
					$qiok = mysql_db_query($DB, $qtyquery, $mysqlid);
					while(list($qty_ordered) = mysql_fetch_array($qiok))
					{
						$query = "update vendor_po_lines set qty_shipped = '$qty_ordered' where poid = '$poid' and pid = '$pid'";
						if(!$q2ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

						$query = "update customer_order_lines set qty_shipped = '$qty_ordered' where orderid = '$on'  and prodid = '$pid' ";
						if(!$q22ok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					}

				}
			}

			//  FINAL STEPS
			if($status == "Shipped")
			{

				$query = "select lineid, prodid from customer_order_lines where orderid = '$on' AND qty_ordered > qty_shipped";
				if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				if(mysql_num_rows($qok) <= 0)
				{

					// success
					echo "Success: order shipped\n";
					$query = "select customer_orders.status, company_orderflow.success from customer_orders, company_orderflow where customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
					if(!$qok_1 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($this_step, $success_step) = mysql_fetch_array($qok_1);

					//No works?
					$query = "update customer_orders set status = '$success_step' where orderid = '$on'";
					if(!$qok_2 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					//Works!
					$query = "update customer_orders set status = '5' where orderid = '$on'";
					if(!$qok_4 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


					$query = "update vendor_pos set status = '2' where poid = '$poid'";
					if(!$qok_3 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }


				}
				else
				{
					echo "Failure: partially shipped\n";
					$report .= "Failure: partially shipped\n";
					$query = "select customer_orders.status, company_orderflow.failure from customer_orders, company_orderflow where customer_orders.status = company_orderflow.step and customer_orders.orderid = '$on'";
					if(!$qok_4 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					list($this_step, $failure_step) = mysql_fetch_array($qok);

					$query = "update customer_orders set status = '$failure_step' where orderid = '$on'";
					if(!$qok_5 = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

				}

			}




			// TRACKING ....  if a tracking # is there no matter what status says, we tag it to the order
			if($tracking !="")
			{

				//echo "TRACKING: $tracking\n";


				$on = $efc->getOrderNumber($invoiceid);
				$xshipdate = time();

				$query = "select prodid from customer_order_lines where orderid = '$on'";
				if(!$qok_BUG = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
				while(list($prodid) = mysql_fetch_array($qok_BUG))
				{
					$query = "delete from customer_order_tracking_extended where prodid = '$prodid' and orderid = '$on' and trackno = '$tracking'";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

                	$query = "delete from customer_order_tracking where invoiceid = '$invoiceid' and trackno = '$tracking' and type = 'Ground'";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					$query = "insert into customer_order_tracking(invoiceid, trackno, type, carrier, shipdate, cost) ";
					$query .= "values('$invoiceid', '$tracking', 'Ground', '$carrier', '$xshipdate', '0.00') ";
					if(!$qok_a = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					$tid = mysql_insert_id();


					$serial = " ";
					//$query = "delete from customer_order_tracking_extended where prodif = '$prodid' and orderid = '$on'";
					//if(!$qok = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }

					$query = "insert into customer_order_tracking_extended(orderid, tid, trackno, prodid, serial) values('$on', '$tid', '$tracking', '$prodid', '$serial') ";
					if(!$qok_b = mysql_db_query($DB, $query, $mysqlid)){ if(stristr($_SERVER["HTTP_HOST"], ".efluent.")){echo mysql_error();exit; }else{ echo "Whoops!  Internal Error"; exit; }  }
					echo "TRACKING: $tracking  e2ORDER#: $on  PRODID: $prodid TRACKINGID: $tid\n";

				}
			}
			else
			{
				echo "NO TRACKING ???   :~(~~\n";

			}


			//print_r($string);

			//echo "\nEOF=>$entry\n\n";
		}
	}
}


$d->close();


function getPOStatus($status)
{
	if($status == 2) return "Complete";
	else if($status == 3) return "Error";
	else if($status == 0) return "In-Queue";
	else if($status == 1) return "Submitted";
	else if($status == 4) return "Void";

	return "?UNKNOWN?";
}









function xml2array($contents, $get_attributes=1)
{
	if(!$contents) return array();

	if(!function_exists('xml_parser_create')) {
		//print "'xml_parser_create()' function not found!";
		return array();
	}
	//Get the XML parser of PHP - PHP must have this module for the parser to work
	$parser = xml_parser_create();
	xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
	xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
	xml_parse_into_struct( $parser, $contents, $xml_values );
	xml_parser_free( $parser );

	if(!$xml_values) return;//Hmm...

	//Initializations
	$xml_array = array();
	$parents = array();
	$opened_tags = array();
	$arr = array();

	$current = &$xml_array;

	//Go through the tags.
	foreach($xml_values as $data) {
		unset($attributes,$value);//Remove existing values, or there will be trouble

		//This command will extract these variables into the foreach scope
		// tag(string), type(string), level(int), attributes(array).
		extract($data);//We could use the array by itself, but this cooler.

		$result = '';
		if($get_attributes) {//The second argument of the function decides this.
			$result = array();
			if(isset($value)) $result['value'] = $value;

			//Set the attributes too.
			if(isset($attributes)) {
				foreach($attributes as $attr => $val) {
					if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
					/**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
				}
			}
		} elseif(isset($value)) {
			$result = $value;
		}

		//See tag status and do the needed.
		if($type == "open") {//The starting of the tag '<tag>'
			$parent[$level-1] = &$current;

			if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
				$current[$tag] = $result;
				$current = &$current[$tag];

			} else { //There was another element with the same tag name
				if(isset($current[$tag][0])) {
					array_push($current[$tag], $result);
				} else {
					$current[$tag] = array($current[$tag],$result);
				}
				$last = count($current[$tag]) - 1;
				$current = &$current[$tag][$last];
			}

		} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
			//See if the key is already taken.
			if(!isset($current[$tag])) { //New Key
				$current[$tag] = $result;

			} else { //If taken, put all things inside a list(array)
				if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
				or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
					array_push($current[$tag],$result); // ...push the new element into that array.
				} else { //If it is not an array...
					$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
				}
			}

		} elseif($type == 'close') { //End of tag '</tag>'
			$current = &$parent[$level-1];
		}
	}

	return($xml_array);
}
?>