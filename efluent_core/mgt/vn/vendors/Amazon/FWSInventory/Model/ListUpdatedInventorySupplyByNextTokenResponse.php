<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse
 * 
 * Properties:
 * <ul>
 * 
 * <li>ListUpdatedInventorySupplyByNextTokenResult: Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult</li>
 * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>ListUpdatedInventorySupplyByNextTokenResult: Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult</li>
     * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ListUpdatedInventorySupplyByNextTokenResult' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_ResponseMetadata'),
        );
        parent::__construct($data);
    }

       
    /**
     * Construct Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse from XML string
     * 
     * @param string $xml XML string to construct from
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse 
     */
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a', 'http://fba-inventory.amazonaws.com/doc/2009-07-31/');
        $response = $xpath->query('//a:ListUpdatedInventorySupplyByNextTokenResponse');
        if ($response->length == 1) {
            return new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse from provided XML. 
                                  Make sure that ListUpdatedInventorySupplyByNextTokenResponse is a root element");
        }
          
    }
    
    /**
     * Gets the value of the ListUpdatedInventorySupplyByNextTokenResult.
     * 
     * @return ListUpdatedInventorySupplyByNextTokenResult ListUpdatedInventorySupplyByNextTokenResult
     */
    public function getListUpdatedInventorySupplyByNextTokenResult() 
    {
        return $this->_fields['ListUpdatedInventorySupplyByNextTokenResult']['FieldValue'];
    }

    /**
     * Sets the value of the ListUpdatedInventorySupplyByNextTokenResult.
     * 
     * @param ListUpdatedInventorySupplyByNextTokenResult ListUpdatedInventorySupplyByNextTokenResult
     * @return void
     */
    public function setListUpdatedInventorySupplyByNextTokenResult($value) 
    {
        $this->_fields['ListUpdatedInventorySupplyByNextTokenResult']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ListUpdatedInventorySupplyByNextTokenResult  and returns this instance
     * 
     * @param ListUpdatedInventorySupplyByNextTokenResult $value ListUpdatedInventorySupplyByNextTokenResult
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse instance
     */
    public function withListUpdatedInventorySupplyByNextTokenResult($value)
    {
        $this->setListUpdatedInventorySupplyByNextTokenResult($value);
        return $this;
    }


    /**
     * Checks if ListUpdatedInventorySupplyByNextTokenResult  is set
     * 
     * @return bool true if ListUpdatedInventorySupplyByNextTokenResult property is set
     */
    public function isSetListUpdatedInventorySupplyByNextTokenResult()
    {
        return !is_null($this->_fields['ListUpdatedInventorySupplyByNextTokenResult']['FieldValue']);

    }

    /**
     * Gets the value of the ResponseMetadata.
     * 
     * @return ResponseMetadata ResponseMetadata
     */
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseMetadata.
     * 
     * @param ResponseMetadata ResponseMetadata
     * @return void
     */
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ResponseMetadata  and returns this instance
     * 
     * @param ResponseMetadata $value ResponseMetadata
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyByNextTokenResponse instance
     */
    public function withResponseMetadata($value)
    {
        $this->setResponseMetadata($value);
        return $this;
    }


    /**
     * Checks if ResponseMetadata  is set
     * 
     * @return bool true if ResponseMetadata property is set
     */
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);

    }



    /**
     * XML Representation for this object
     * 
     * @return string XML for this object
     */
    public function toXML() 
    {
        $xml = "";
        $xml .= "<ListUpdatedInventorySupplyByNextTokenResponse xmlns=\"http://fba-inventory.amazonaws.com/doc/2009-07-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</ListUpdatedInventorySupplyByNextTokenResponse>";
        return $xml;
    }

}