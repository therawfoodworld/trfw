<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_GetServiceStatusRequest
 * 
 * Properties:
 * <ul>
 * 
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_GetServiceStatusRequest extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_GetServiceStatusRequest
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        );
        parent::__construct($data);
    }

    


}