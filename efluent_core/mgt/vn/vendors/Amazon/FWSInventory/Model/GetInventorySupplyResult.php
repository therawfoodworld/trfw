<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_GetInventorySupplyResult
 * 
 * Properties:
 * <ul>
 * 
 * <li>MerchantSKUSupply: Amazon_FWSInventory_Model_MerchantSKUSupply</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_GetInventorySupplyResult extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_GetInventorySupplyResult
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>MerchantSKUSupply: Amazon_FWSInventory_Model_MerchantSKUSupply</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'MerchantSKUSupply' => array('FieldValue' => array(), 'FieldType' => array('Amazon_FWSInventory_Model_MerchantSKUSupply')),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the MerchantSKUSupply.
     * 
     * @return array of MerchantSKUSupply MerchantSKUSupply
     */
    public function getMerchantSKUSupply() 
    {
        return $this->_fields['MerchantSKUSupply']['FieldValue'];
    }

    /**
     * Sets the value of the MerchantSKUSupply.
     * 
     * @param mixed MerchantSKUSupply or an array of MerchantSKUSupply MerchantSKUSupply
     * @return this instance
     */
    public function setMerchantSKUSupply($merchantSKUSupply) 
    {
        if (!$this->_isNumericArray($merchantSKUSupply)) {
            $merchantSKUSupply =  array ($merchantSKUSupply);    
        }
        $this->_fields['MerchantSKUSupply']['FieldValue'] = $merchantSKUSupply;
        return $this;
    }


    /**
     * Sets single or multiple values of MerchantSKUSupply list via variable number of arguments. 
     * For example, to set the list with two elements, simply pass two values as arguments to this function
     * <code>withMerchantSKUSupply($merchantSKUSupply1, $merchantSKUSupply2)</code>
     * 
     * @param MerchantSKUSupply  $merchantSKUSupplyArgs one or more MerchantSKUSupply
     * @return Amazon_FWSInventory_Model_GetInventorySupplyResult  instance
     */
    public function withMerchantSKUSupply($merchantSKUSupplyArgs)
    {
        foreach (func_get_args() as $merchantSKUSupply) {
            $this->_fields['MerchantSKUSupply']['FieldValue'][] = $merchantSKUSupply;
        }
        return $this;
    }   



    /**
     * Checks if MerchantSKUSupply list is non-empty
     * 
     * @return bool true if MerchantSKUSupply list is non-empty
     */
    public function isSetMerchantSKUSupply()
    {
        return count ($this->_fields['MerchantSKUSupply']['FieldValue']) > 0;
    }




}