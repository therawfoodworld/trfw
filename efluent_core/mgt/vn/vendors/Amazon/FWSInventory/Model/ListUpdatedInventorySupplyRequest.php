<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest
 * 
 * Properties:
 * <ul>
 * 
 * <li>NumberOfResultsRequested: int</li>
 * <li>QueryStartDateTime: string</li>
 * <li>ResponseGroup: string</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>NumberOfResultsRequested: int</li>
     * <li>QueryStartDateTime: string</li>
     * <li>ResponseGroup: string</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'NumberOfResultsRequested' => array('FieldValue' => null, 'FieldType' => 'int'),
        'QueryStartDateTime' => array('FieldValue' => null, 'FieldType' => 'string'),
        'ResponseGroup' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }

        /**
     * Gets the value of the NumberOfResultsRequested property.
     * 
     * @return int NumberOfResultsRequested
     */
    public function getNumberOfResultsRequested() 
    {
        return $this->_fields['NumberOfResultsRequested']['FieldValue'];
    }

    /**
     * Sets the value of the NumberOfResultsRequested property.
     * 
     * @param int NumberOfResultsRequested
     * @return this instance
     */
    public function setNumberOfResultsRequested($value) 
    {
        $this->_fields['NumberOfResultsRequested']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the NumberOfResultsRequested and returns this instance
     * 
     * @param int $value NumberOfResultsRequested
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest instance
     */
    public function withNumberOfResultsRequested($value)
    {
        $this->setNumberOfResultsRequested($value);
        return $this;
    }


    /**
     * Checks if NumberOfResultsRequested is set
     * 
     * @return bool true if NumberOfResultsRequested  is set
     */
    public function isSetNumberOfResultsRequested()
    {
        return !is_null($this->_fields['NumberOfResultsRequested']['FieldValue']);
    }

    /**
     * Gets the value of the QueryStartDateTime property.
     * 
     * @return string QueryStartDateTime
     */
    public function getQueryStartDateTime() 
    {
        return $this->_fields['QueryStartDateTime']['FieldValue'];
    }

    /**
     * Sets the value of the QueryStartDateTime property.
     * 
     * @param string QueryStartDateTime
     * @return this instance
     */
    public function setQueryStartDateTime($value) 
    {
        $this->_fields['QueryStartDateTime']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the QueryStartDateTime and returns this instance
     * 
     * @param string $value QueryStartDateTime
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest instance
     */
    public function withQueryStartDateTime($value)
    {
        $this->setQueryStartDateTime($value);
        return $this;
    }


    /**
     * Checks if QueryStartDateTime is set
     * 
     * @return bool true if QueryStartDateTime  is set
     */
    public function isSetQueryStartDateTime()
    {
        return !is_null($this->_fields['QueryStartDateTime']['FieldValue']);
    }

    /**
     * Gets the value of the ResponseGroup property.
     * 
     * @return string ResponseGroup
     */
    public function getResponseGroup() 
    {
        return $this->_fields['ResponseGroup']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseGroup property.
     * 
     * @param string ResponseGroup
     * @return this instance
     */
    public function setResponseGroup($value) 
    {
        $this->_fields['ResponseGroup']['FieldValue'] = $value;
        return $this;
    }

    /**
     * Sets the value of the ResponseGroup and returns this instance
     * 
     * @param string $value ResponseGroup
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyRequest instance
     */
    public function withResponseGroup($value)
    {
        $this->setResponseGroup($value);
        return $this;
    }


    /**
     * Checks if ResponseGroup is set
     * 
     * @return bool true if ResponseGroup  is set
     */
    public function isSetResponseGroup()
    {
        return !is_null($this->_fields['ResponseGroup']['FieldValue']);
    }




}