<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse
 * 
 * Properties:
 * <ul>
 * 
 * <li>ListUpdatedInventorySupplyResult: Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResult</li>
 * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>ListUpdatedInventorySupplyResult: Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResult</li>
     * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ListUpdatedInventorySupplyResult' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_ResponseMetadata'),
        );
        parent::__construct($data);
    }

       
    /**
     * Construct Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse from XML string
     * 
     * @param string $xml XML string to construct from
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse 
     */
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a', 'http://fba-inventory.amazonaws.com/doc/2009-07-31/');
        $response = $xpath->query('//a:ListUpdatedInventorySupplyResponse');
        if ($response->length == 1) {
            return new Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse from provided XML. 
                                  Make sure that ListUpdatedInventorySupplyResponse is a root element");
        }
          
    }
    
    /**
     * Gets the value of the ListUpdatedInventorySupplyResult.
     * 
     * @return ListUpdatedInventorySupplyResult ListUpdatedInventorySupplyResult
     */
    public function getListUpdatedInventorySupplyResult() 
    {
        return $this->_fields['ListUpdatedInventorySupplyResult']['FieldValue'];
    }

    /**
     * Sets the value of the ListUpdatedInventorySupplyResult.
     * 
     * @param ListUpdatedInventorySupplyResult ListUpdatedInventorySupplyResult
     * @return void
     */
    public function setListUpdatedInventorySupplyResult($value) 
    {
        $this->_fields['ListUpdatedInventorySupplyResult']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ListUpdatedInventorySupplyResult  and returns this instance
     * 
     * @param ListUpdatedInventorySupplyResult $value ListUpdatedInventorySupplyResult
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse instance
     */
    public function withListUpdatedInventorySupplyResult($value)
    {
        $this->setListUpdatedInventorySupplyResult($value);
        return $this;
    }


    /**
     * Checks if ListUpdatedInventorySupplyResult  is set
     * 
     * @return bool true if ListUpdatedInventorySupplyResult property is set
     */
    public function isSetListUpdatedInventorySupplyResult()
    {
        return !is_null($this->_fields['ListUpdatedInventorySupplyResult']['FieldValue']);

    }

    /**
     * Gets the value of the ResponseMetadata.
     * 
     * @return ResponseMetadata ResponseMetadata
     */
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseMetadata.
     * 
     * @param ResponseMetadata ResponseMetadata
     * @return void
     */
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ResponseMetadata  and returns this instance
     * 
     * @param ResponseMetadata $value ResponseMetadata
     * @return Amazon_FWSInventory_Model_ListUpdatedInventorySupplyResponse instance
     */
    public function withResponseMetadata($value)
    {
        $this->setResponseMetadata($value);
        return $this;
    }


    /**
     * Checks if ResponseMetadata  is set
     * 
     * @return bool true if ResponseMetadata property is set
     */
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);

    }



    /**
     * XML Representation for this object
     * 
     * @return string XML for this object
     */
    public function toXML() 
    {
        $xml = "";
        $xml .= "<ListUpdatedInventorySupplyResponse xmlns=\"http://fba-inventory.amazonaws.com/doc/2009-07-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</ListUpdatedInventorySupplyResponse>";
        return $xml;
    }

}