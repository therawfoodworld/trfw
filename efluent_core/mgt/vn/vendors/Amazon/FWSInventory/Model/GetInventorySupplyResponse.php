<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInventory
 *  @copyright   Copyright 2008 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-07-31
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inventory PHP5 Library
 *  Generated: Fri Nov 20 09:59:57 PST 2009
 * 
 */

/**
 *  @see Amazon_FWSInventory_Model
 */
require_once ('Amazon/FWSInventory/Model.php');  

    

/**
 * Amazon_FWSInventory_Model_GetInventorySupplyResponse
 * 
 * Properties:
 * <ul>
 * 
 * <li>GetInventorySupplyResult: Amazon_FWSInventory_Model_GetInventorySupplyResult</li>
 * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
 *
 * </ul>
 */ 
class Amazon_FWSInventory_Model_GetInventorySupplyResponse extends Amazon_FWSInventory_Model
{


    /**
     * Construct new Amazon_FWSInventory_Model_GetInventorySupplyResponse
     * 
     * @param mixed $data DOMElement or Associative Array to construct from. 
     * 
     * Valid properties:
     * <ul>
     * 
     * <li>GetInventorySupplyResult: Amazon_FWSInventory_Model_GetInventorySupplyResult</li>
     * <li>ResponseMetadata: Amazon_FWSInventory_Model_ResponseMetadata</li>
     *
     * </ul>
     */
    public function __construct($data = null)
    {
        $this->_fields = array (
        'GetInventorySupplyResult' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_GetInventorySupplyResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'Amazon_FWSInventory_Model_ResponseMetadata'),
        );
        parent::__construct($data);
    }

       
    /**
     * Construct Amazon_FWSInventory_Model_GetInventorySupplyResponse from XML string
     * 
     * @param string $xml XML string to construct from
     * @return Amazon_FWSInventory_Model_GetInventorySupplyResponse 
     */
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a', 'http://fba-inventory.amazonaws.com/doc/2009-07-31/');
        $response = $xpath->query('//a:GetInventorySupplyResponse');
        if ($response->length == 1) {
            return new Amazon_FWSInventory_Model_GetInventorySupplyResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct Amazon_FWSInventory_Model_GetInventorySupplyResponse from provided XML. 
                                  Make sure that GetInventorySupplyResponse is a root element");
        }
          
    }
    
    /**
     * Gets the value of the GetInventorySupplyResult.
     * 
     * @return GetInventorySupplyResult GetInventorySupplyResult
     */
    public function getGetInventorySupplyResult() 
    {
        return $this->_fields['GetInventorySupplyResult']['FieldValue'];
    }

    /**
     * Sets the value of the GetInventorySupplyResult.
     * 
     * @param GetInventorySupplyResult GetInventorySupplyResult
     * @return void
     */
    public function setGetInventorySupplyResult($value) 
    {
        $this->_fields['GetInventorySupplyResult']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the GetInventorySupplyResult  and returns this instance
     * 
     * @param GetInventorySupplyResult $value GetInventorySupplyResult
     * @return Amazon_FWSInventory_Model_GetInventorySupplyResponse instance
     */
    public function withGetInventorySupplyResult($value)
    {
        $this->setGetInventorySupplyResult($value);
        return $this;
    }


    /**
     * Checks if GetInventorySupplyResult  is set
     * 
     * @return bool true if GetInventorySupplyResult property is set
     */
    public function isSetGetInventorySupplyResult()
    {
        return !is_null($this->_fields['GetInventorySupplyResult']['FieldValue']);

    }

    /**
     * Gets the value of the ResponseMetadata.
     * 
     * @return ResponseMetadata ResponseMetadata
     */
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    /**
     * Sets the value of the ResponseMetadata.
     * 
     * @param ResponseMetadata ResponseMetadata
     * @return void
     */
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    /**
     * Sets the value of the ResponseMetadata  and returns this instance
     * 
     * @param ResponseMetadata $value ResponseMetadata
     * @return Amazon_FWSInventory_Model_GetInventorySupplyResponse instance
     */
    public function withResponseMetadata($value)
    {
        $this->setResponseMetadata($value);
        return $this;
    }


    /**
     * Checks if ResponseMetadata  is set
     * 
     * @return bool true if ResponseMetadata property is set
     */
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);

    }



    /**
     * XML Representation for this object
     * 
     * @return string XML for this object
     */
    public function toXML() 
    {
        $xml = "";
        $xml .= "<GetInventorySupplyResponse xmlns=\"http://fba-inventory.amazonaws.com/doc/2009-07-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</GetInventorySupplyResponse>";
        return $xml;
    }

}