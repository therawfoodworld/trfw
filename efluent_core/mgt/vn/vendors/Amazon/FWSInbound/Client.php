<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     Amazon_FWSInbound
 *  @copyright   Copyright 2008-2009 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2007-05-10
 */
/******************************************************************************* 
 *    __  _    _  ___ 
 *   (  )( \/\/ )/ __)
 *   /__\ \    / \__ \
 *  (_)(_) \/\/  (___/
 * 
 *  Amazon FWS Inbound PHP5 Library
 *  Generated: Tue Feb 23 12:31:13 PST 2010
 * 
 */

/**
 *  @see Amazon_FWSInbound_Interface
 */
require_once ('Amazon/FWSInbound/Interface.php');

/**
 * Fulfillment  Web Service (FWS) used to set up inbound shipments
 * to Amazon and to view inventory in the Amazon Fulfillment Network.
 * 
 * Release Notes: https://fba-inbound.amazonaws.com/doc/2007-05-10/ReleaseNotes.html
 * 
 * Amazon_FWSInbound_Client is an implementation of Amazon_FWSInbound
 *
 */
class Amazon_FWSInbound_Client implements Amazon_FWSInbound_Interface
{

    const SERVICE_VERSION = '2007-05-10';

    /** @var string */
    private  $_awsAccessKeyId = null;

    /** @var string */
    private  $_awsSecretAccessKey = null;

    /** @var array */
    private  $_config = array ('ServiceURL' => 'https://fba-inbound.amazonaws.com',
                               'UserAgent' => 'Amazon FWSInbound PHP5 Library',
                               'SignatureVersion' => 1,
                               'SignatureMethod' => 'HmacSHA256',
                               'ProxyHost' => null,
                               'ProxyPort' => -1,
                               'MaxErrorRetry' => 3
                               );

    /**
     * Construct new Client
     *
     * @param string $awsAccessKeyId AWS Access Key ID
     * @param string $awsSecretAccessKey AWS Secret Access Key
     * @param array $config configuration options.
     * Valid configuration options are:
     * <ul>
     * <li>ServiceURL</li>
     * <li>UserAgent</li>
     * <li>SignatureVersion</li>
     * <li>TimesRetryOnError</li>
     * <li>ProxyHost</li>
     * <li>ProxyPort</li>
     * <li>MaxErrorRetry</li>
     * </ul>
     */
    public function __construct($awsAccessKeyId, $awsSecretAccessKey, $config = null)
    {
        iconv_set_encoding('output_encoding', 'UTF-8');
        iconv_set_encoding('input_encoding', 'UTF-8');
        iconv_set_encoding('internal_encoding', 'UTF-8');

        $this->_awsAccessKeyId = $awsAccessKeyId;
        $this->_awsSecretAccessKey = $awsSecretAccessKey;
        if (!is_null($config)) $this->_config = array_merge($this->_config, $config);
    }

    // Public API ------------------------------------------------------------//


            
    /**
     * Get Service Status 
     * Gets a brief status message from the service.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetServiceStatus.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetServiceStatusRequest request
     * or Amazon_FWSInbound_Model_GetServiceStatusRequest object itself
     * @see Amazon_FWSInbound_Model_GetServiceStatus
     * @return Amazon_FWSInbound_Model_GetServiceStatusResponse Amazon_FWSInbound_Model_GetServiceStatusResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getServiceStatus($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetServiceStatusRequest) {
            require_once ('Amazon/FWSInbound/Model/GetServiceStatusRequest.php');
            $request = new Amazon_FWSInbound_Model_GetServiceStatusRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetServiceStatusResponse.php');
        return Amazon_FWSInbound_Model_GetServiceStatusResponse::fromXML($this->_invoke($this->_convertGetServiceStatus($request)));
    }


            
    /**
     * Get Fulfillment Identifier 
     * Gets the Fulfillment Network SKU (FNSKU) for each supplied
     * Merchant Item, creating it if needed. This operation is
     * idempotent in that it can be called multiple times without
     * any adverse effects. This operation is necessary to register
     * items for Amazon fulfillment in order to send them to Amazon,
     * but does NOT do the work of marking any offer for this item
     * as Amazon-fulfilled. Providing more than 250 Merchant Items
     * will result in an error.
     * 
     * NOTE: Use this operation over GetFulfillmentIdentifierForMSKU
     * if an offer does NOT already exist for the MerchantSKU.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetFulfillmentIdentifier.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetFulfillmentIdentifierRequest request
     * or Amazon_FWSInbound_Model_GetFulfillmentIdentifierRequest object itself
     * @see Amazon_FWSInbound_Model_GetFulfillmentIdentifier
     * @return Amazon_FWSInbound_Model_GetFulfillmentIdentifierResponse Amazon_FWSInbound_Model_GetFulfillmentIdentifierResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getFulfillmentIdentifier($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetFulfillmentIdentifierRequest) {
            require_once ('Amazon/FWSInbound/Model/GetFulfillmentIdentifierRequest.php');
            $request = new Amazon_FWSInbound_Model_GetFulfillmentIdentifierRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetFulfillmentIdentifierResponse.php');
        return Amazon_FWSInbound_Model_GetFulfillmentIdentifierResponse::fromXML($this->_invoke($this->_convertGetFulfillmentIdentifier($request)));
    }


            
    /**
     * Get Fulfillment Identifier For MSKU 
     * Gets the Fulfillment Network SKU (FNSKU) for each supplied
     * Merchant Item, creating it if needed. This operation is
     * idempotent in that it can be called multiple times without
     * any adverse effects. This operation is necessary to register
     * items for Amazon fulfillment in order to send them to Amazon,
     * but does NOT do the work of marking any offer for this item
     * as Amazon-fulfilled. Providing more than 250 Merchant Items
     * will result in an error.
     * 
     * NOTE: Use this operation over GetFulfillmentIdentifier if
     * an offer already exists for the SKU.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetFulfillmentIdentifierForMSKU.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKURequest request
     * or Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKURequest object itself
     * @see Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKU
     * @return Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKUResponse Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKUResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getFulfillmentIdentifierForMSKU($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKURequest) {
            require_once ('Amazon/FWSInbound/Model/GetFulfillmentIdentifierForMSKURequest.php');
            $request = new Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKURequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetFulfillmentIdentifierForMSKUResponse.php');
        return Amazon_FWSInbound_Model_GetFulfillmentIdentifierForMSKUResponse::fromXML($this->_invoke($this->_convertGetFulfillmentIdentifierForMSKU($request)));
    }


            
    /**
     * List All Fulfillment Items 
     * Gets the first set of fulfillment items registered to a
     * merchant and returns a NextToken which can be used to
     * iterate through the remaining fulfillment items (via the
     * 'ListAllFulfillmentItemsByNextToken operation).  The
     * service will return between 1 and 'MaxCount' number of
     * items.  Please check the value of 'HasNext' to determine
     * whether there were some items which were not returned.  If
     * 'IncludeInactive' is set to true, it will return both
     * active and inactive mappings (this parameter defaults to
     * false).  Inactive mappings can be used to track inventory in
     * Amazon's fulfillment centers although they can't be fulfilled.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListAllFulfillmentItems.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListAllFulfillmentItemsRequest request
     * or Amazon_FWSInbound_Model_ListAllFulfillmentItemsRequest object itself
     * @see Amazon_FWSInbound_Model_ListAllFulfillmentItems
     * @return Amazon_FWSInbound_Model_ListAllFulfillmentItemsResponse Amazon_FWSInbound_Model_ListAllFulfillmentItemsResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listAllFulfillmentItems($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListAllFulfillmentItemsRequest) {
            require_once ('Amazon/FWSInbound/Model/ListAllFulfillmentItemsRequest.php');
            $request = new Amazon_FWSInbound_Model_ListAllFulfillmentItemsRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListAllFulfillmentItemsResponse.php');
        return Amazon_FWSInbound_Model_ListAllFulfillmentItemsResponse::fromXML($this->_invoke($this->_convertListAllFulfillmentItems($request)));
    }


            
    /**
     * List All Fulfillment Items By Next Token 
     * Gets the next set of fulfillment items registered to a
     * merchant and returns a NextToken which can be used to
     * iterate through the remaining fulfillment items.  The
     * service will return between 1 and 'MaxCount' number of
     * items.  Please check the value of 'HasNext' to determine
     * whether there were some items which were not returned.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListAllFulfillmentItemsByNextToken.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenRequest request
     * or Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenRequest object itself
     * @see Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextToken
     * @return Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenResponse Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listAllFulfillmentItemsByNextToken($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenRequest) {
            require_once ('Amazon/FWSInbound/Model/ListAllFulfillmentItemsByNextTokenRequest.php');
            $request = new Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListAllFulfillmentItemsByNextTokenResponse.php');
        return Amazon_FWSInbound_Model_ListAllFulfillmentItemsByNextTokenResponse::fromXML($this->_invoke($this->_convertListAllFulfillmentItemsByNextToken($request)));
    }


            
    /**
     * Get Fulfillment Item By MSKU 
     * Gets fulfillment item data for the provided Merchant SKUs.  If
     * any of the provided MSKUs are invalid (e.g. does not have an
     * assigned Fulfillment Network SKU) they will be ignored and only
     * the valid SKUs will be returned.  NOTE: Providing more than
     * 2,000 MSKUs will result in an error.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetFulfillmentItemByMSKU.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetFulfillmentItemByMSKURequest request
     * or Amazon_FWSInbound_Model_GetFulfillmentItemByMSKURequest object itself
     * @see Amazon_FWSInbound_Model_GetFulfillmentItemByMSKU
     * @return Amazon_FWSInbound_Model_GetFulfillmentItemByMSKUResponse Amazon_FWSInbound_Model_GetFulfillmentItemByMSKUResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getFulfillmentItemByMSKU($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetFulfillmentItemByMSKURequest) {
            require_once ('Amazon/FWSInbound/Model/GetFulfillmentItemByMSKURequest.php');
            $request = new Amazon_FWSInbound_Model_GetFulfillmentItemByMSKURequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetFulfillmentItemByMSKUResponse.php');
        return Amazon_FWSInbound_Model_GetFulfillmentItemByMSKUResponse::fromXML($this->_invoke($this->_convertGetFulfillmentItemByMSKU($request)));
    }


            
    /**
     * Get Fulfillment Item By FNSKU 
     * Gets fulfillment item data for the provided Fulfillment
     * Network SKUs.  If any of the provided FNSKUs are invalid
     * they will be ignored and only the valid SKUs will be returned.
     * NOTE: Providing more than 2,000 FNSKUs will result in an error.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetFulfillmentItemByFNSKU.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKURequest request
     * or Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKURequest object itself
     * @see Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKU
     * @return Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKUResponse Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKUResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getFulfillmentItemByFNSKU($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKURequest) {
            require_once ('Amazon/FWSInbound/Model/GetFulfillmentItemByFNSKURequest.php');
            $request = new Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKURequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetFulfillmentItemByFNSKUResponse.php');
        return Amazon_FWSInbound_Model_GetFulfillmentItemByFNSKUResponse::fromXML($this->_invoke($this->_convertGetFulfillmentItemByFNSKU($request)));
    }


            
    /**
     * Get Inbound Shipment Preview 
     * Returns the information needed to create a set of shipments
     * for the given collection of items and source address.  When
     * all the items are not all in the same category (e.g. some
     * sortable, some non-sortable) it may be necessary to create
     * multiple shipments (one for each of the shipment groups
     * returned).
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetInboundShipmentPreview.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetInboundShipmentPreviewRequest request
     * or Amazon_FWSInbound_Model_GetInboundShipmentPreviewRequest object itself
     * @see Amazon_FWSInbound_Model_GetInboundShipmentPreview
     * @return Amazon_FWSInbound_Model_GetInboundShipmentPreviewResponse Amazon_FWSInbound_Model_GetInboundShipmentPreviewResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getInboundShipmentPreview($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetInboundShipmentPreviewRequest) {
            require_once ('Amazon/FWSInbound/Model/GetInboundShipmentPreviewRequest.php');
            $request = new Amazon_FWSInbound_Model_GetInboundShipmentPreviewRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetInboundShipmentPreviewResponse.php');
        return Amazon_FWSInbound_Model_GetInboundShipmentPreviewResponse::fromXML($this->_invoke($this->_convertGetInboundShipmentPreview($request)));
    }


            
    /**
     * Put Inbound Shipment 
     * Creates or replaces information about a shipment. If the
     * shipment already exists, the information about the shipment
     * (and its associated items) will be replaced (for the provided
     * ShipmentId).  If the shipment does not exist, one will be
     * created.
     * 
     * Note, the merchant should call SetInboundShipmentStatus with a
     * status of 'Shipped' when the shipment is picked up, or set the status
     * to 'Cancelled' if the shipment is abandoned. The intial status
     * of a shipment will be set to 'Working'. Once a shipment's status
     * has been set to 'Shipped', the merchant may make no further changes
     * except to update the status to 'Cancelled'.  Any items not received
     * at the time a shipment is 'Cancelled' will be sidelined if they
     * arrive at the fulfillment center.
     * This operation will simply return a requestId upon success,
     * otherwise an explicit error will be returned.
     * NOTE: If you are experiencing time-outs due to too many
     * items in the shipment, calling this operation with a subset
     * of the items should work.  You may add more items to the
     * shipment by calling PutInboundShipmentItems.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}PutInboundShipment.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_PutInboundShipmentRequest request
     * or Amazon_FWSInbound_Model_PutInboundShipmentRequest object itself
     * @see Amazon_FWSInbound_Model_PutInboundShipment
     * @return Amazon_FWSInbound_Model_PutInboundShipmentResponse Amazon_FWSInbound_Model_PutInboundShipmentResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function putInboundShipment($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_PutInboundShipmentRequest) {
            require_once ('Amazon/FWSInbound/Model/PutInboundShipmentRequest.php');
            $request = new Amazon_FWSInbound_Model_PutInboundShipmentRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/PutInboundShipmentResponse.php');
        return Amazon_FWSInbound_Model_PutInboundShipmentResponse::fromXML($this->_invoke($this->_convertPutInboundShipment($request)));
    }


            
    /**
     * Put Inbound Shipment Items 
     * Adds line items to the pre-existing shipment specified by the
     * ShipmentId. If the MerchantSKU is already in the shipment, then
     * that particular line item will be replaced. Call
     * PutInboundShipment to create the shipment to be worked on.
     * 
     * This call will return an exception when attempting to add line
     * items to a shipment that is in a status other than 'Working'.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}PutInboundShipmentItems.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_PutInboundShipmentItemsRequest request
     * or Amazon_FWSInbound_Model_PutInboundShipmentItemsRequest object itself
     * @see Amazon_FWSInbound_Model_PutInboundShipmentItems
     * @return Amazon_FWSInbound_Model_PutInboundShipmentItemsResponse Amazon_FWSInbound_Model_PutInboundShipmentItemsResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function putInboundShipmentItems($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_PutInboundShipmentItemsRequest) {
            require_once ('Amazon/FWSInbound/Model/PutInboundShipmentItemsRequest.php');
            $request = new Amazon_FWSInbound_Model_PutInboundShipmentItemsRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/PutInboundShipmentItemsResponse.php');
        return Amazon_FWSInbound_Model_PutInboundShipmentItemsResponse::fromXML($this->_invoke($this->_convertPutInboundShipmentItems($request)));
    }


            
    /**
     * Delete Inbound Shipment Items 
     * Removes line items from the pre-existing shipment specified by the
     * ShipmentId. If the MerchantSKU is not currently a part of the
     * shipment, then that particular line item will be ignored and all
     * others will still be processed. Call PutInboundShipment to create
     * the shipment to be worked on.
     * 
     * This call will return an exception when attempting to remove line
     * items from a shipment that is in a status other than 'Working'.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}DeleteInboundShipmentItems.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_DeleteInboundShipmentItemsRequest request
     * or Amazon_FWSInbound_Model_DeleteInboundShipmentItemsRequest object itself
     * @see Amazon_FWSInbound_Model_DeleteInboundShipmentItems
     * @return Amazon_FWSInbound_Model_DeleteInboundShipmentItemsResponse Amazon_FWSInbound_Model_DeleteInboundShipmentItemsResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function deleteInboundShipmentItems($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_DeleteInboundShipmentItemsRequest) {
            require_once ('Amazon/FWSInbound/Model/DeleteInboundShipmentItemsRequest.php');
            $request = new Amazon_FWSInbound_Model_DeleteInboundShipmentItemsRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/DeleteInboundShipmentItemsResponse.php');
        return Amazon_FWSInbound_Model_DeleteInboundShipmentItemsResponse::fromXML($this->_invoke($this->_convertDeleteInboundShipmentItems($request)));
    }


            
    /**
     * List Inbound Shipments 
     * Get the first set of inbound shipments created by a merchant
     * according to the specified query parameters.  A NextToken is also
     * returned to further iterate through the merchant's remaining
     * shipments.  The service will return between 1 and 'MaxCount' number
     * of items.  Please check the value of 'HasNext' to determine
     * whether there were some items which were not returned.  If
     * ShipmentStatus is not set, then all shipments for a merchant will
     * be returned, otherwise only shipments in the specified status will
     * be returned.  If CreatedBefore is set, only shipments which were
     * created before the specified date will be returned (if left
     * unspecified, it defaults to "now").  If CreatedAfter is set, only
     * shipments which were created after the specified date will be
     * returned (if left unspecified, it defaults to the epoch).
     * NOTE: The CreatedBefore and CreatedAfter functionality is not
     * currently implemented and defaults to the values mentioned above.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListInboundShipments.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListInboundShipmentsRequest request
     * or Amazon_FWSInbound_Model_ListInboundShipmentsRequest object itself
     * @see Amazon_FWSInbound_Model_ListInboundShipments
     * @return Amazon_FWSInbound_Model_ListInboundShipmentsResponse Amazon_FWSInbound_Model_ListInboundShipmentsResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listInboundShipments($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListInboundShipmentsRequest) {
            require_once ('Amazon/FWSInbound/Model/ListInboundShipmentsRequest.php');
            $request = new Amazon_FWSInbound_Model_ListInboundShipmentsRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListInboundShipmentsResponse.php');
        return Amazon_FWSInbound_Model_ListInboundShipmentsResponse::fromXML($this->_invoke($this->_convertListInboundShipments($request)));
    }


            
    /**
     * List Inbound Shipments By Next Token 
     * Gets the next set of inbound shipments created by a
     * merchant and returns the next NextToken which can be used to
     * iterate through the remaining inbound shipments.  The
     * service will return between 1 and 'MaxCount' number of
     * items.  Please check the value of 'HasNext' to determine
     * whether there were some items which were not returned.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListInboundShipmentsByNextToken.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenRequest request
     * or Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenRequest object itself
     * @see Amazon_FWSInbound_Model_ListInboundShipmentsByNextToken
     * @return Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenResponse Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listInboundShipmentsByNextToken($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenRequest) {
            require_once ('Amazon/FWSInbound/Model/ListInboundShipmentsByNextTokenRequest.php');
            $request = new Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListInboundShipmentsByNextTokenResponse.php');
        return Amazon_FWSInbound_Model_ListInboundShipmentsByNextTokenResponse::fromXML($this->_invoke($this->_convertListInboundShipmentsByNextToken($request)));
    }


            
    /**
     * Get Inbound Shipment Data 
     * Gets the merchant's inbound shipment header information for the
     * given ShipmentId.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}GetInboundShipmentData.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_GetInboundShipmentDataRequest request
     * or Amazon_FWSInbound_Model_GetInboundShipmentDataRequest object itself
     * @see Amazon_FWSInbound_Model_GetInboundShipmentData
     * @return Amazon_FWSInbound_Model_GetInboundShipmentDataResponse Amazon_FWSInbound_Model_GetInboundShipmentDataResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function getInboundShipmentData($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_GetInboundShipmentDataRequest) {
            require_once ('Amazon/FWSInbound/Model/GetInboundShipmentDataRequest.php');
            $request = new Amazon_FWSInbound_Model_GetInboundShipmentDataRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/GetInboundShipmentDataResponse.php');
        return Amazon_FWSInbound_Model_GetInboundShipmentDataResponse::fromXML($this->_invoke($this->_convertGetInboundShipmentData($request)));
    }


            
    /**
     * Put Inbound Shipment Data 
     * Adds or replaces the merchant's inbound shipment header information for the
     * given ShipmentId.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}PutInboundShipmentData.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_PutInboundShipmentDataRequest request
     * or Amazon_FWSInbound_Model_PutInboundShipmentDataRequest object itself
     * @see Amazon_FWSInbound_Model_PutInboundShipmentData
     * @return Amazon_FWSInbound_Model_PutInboundShipmentDataResponse Amazon_FWSInbound_Model_PutInboundShipmentDataResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function putInboundShipmentData($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_PutInboundShipmentDataRequest) {
            require_once ('Amazon/FWSInbound/Model/PutInboundShipmentDataRequest.php');
            $request = new Amazon_FWSInbound_Model_PutInboundShipmentDataRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/PutInboundShipmentDataResponse.php');
        return Amazon_FWSInbound_Model_PutInboundShipmentDataResponse::fromXML($this->_invoke($this->_convertPutInboundShipmentData($request)));
    }


            
    /**
     * List Inbound Shipment Items 
     * Gets the first set of inbound shipment items for the given
     * ShipmentId. A NextToken is also returned to further iterate
     * through the merchant's remaining inbound shipment items.
     * The service will return between 1 and 'MaxCount' number of
     * shipment items.  Please check the value of 'HasNext' to
     * determine whether there are some items which were not
     * returned. To get the next set of inbound shipment items, you
     * must call ListInboundShipmentItemsByNextToken and pass in
     * the 'NextToken' this call returned.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListInboundShipmentItems.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListInboundShipmentItemsRequest request
     * or Amazon_FWSInbound_Model_ListInboundShipmentItemsRequest object itself
     * @see Amazon_FWSInbound_Model_ListInboundShipmentItems
     * @return Amazon_FWSInbound_Model_ListInboundShipmentItemsResponse Amazon_FWSInbound_Model_ListInboundShipmentItemsResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listInboundShipmentItems($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListInboundShipmentItemsRequest) {
            require_once ('Amazon/FWSInbound/Model/ListInboundShipmentItemsRequest.php');
            $request = new Amazon_FWSInbound_Model_ListInboundShipmentItemsRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListInboundShipmentItemsResponse.php');
        return Amazon_FWSInbound_Model_ListInboundShipmentItemsResponse::fromXML($this->_invoke($this->_convertListInboundShipmentItems($request)));
    }


            
    /**
     * List Inbound Shipment Items By Next Token 
     * Gets the next set of inbound shipment items and returns the
     * NextToken which can be used to iterate through the remaining
     * inbound shipment items. The service will return between 1 and
     * 'MaxCount' number of shipment items.  Please check the value
     * of 'HasNext' to determine whether there are some items which
     * were not returned. You must first call
     * ListInboundShipmentItems to get a 'NextToken'.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}ListInboundShipmentItemsByNextToken.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenRequest request
     * or Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenRequest object itself
     * @see Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextToken
     * @return Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenResponse Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function listInboundShipmentItemsByNextToken($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenRequest) {
            require_once ('Amazon/FWSInbound/Model/ListInboundShipmentItemsByNextTokenRequest.php');
            $request = new Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/ListInboundShipmentItemsByNextTokenResponse.php');
        return Amazon_FWSInbound_Model_ListInboundShipmentItemsByNextTokenResponse::fromXML($this->_invoke($this->_convertListInboundShipmentItemsByNextToken($request)));
    }


            
    /**
     * Set Inbound Shipment Status 
     * Sets the inbound shipment status to the specified ShipmentStatus.
     * Valid statuses are 'Shipped' (the shipment has left the merchant)
     * or 'Cancelled'.
     * Once a shipment's status has been set to 'Shipped', the merchant
     * may make no further changes except to update the status to
     * 'Cancelled'.  Any items not received at the time a shipment is
     * 'Cancelled' will be put into problem receive at the
     * fulfillment center.
     * This operation will simply return a requestId upon success,
     * otherwise an explicit error will be returned.
     * 
     * @see http://docs.amazonwebservices.com/${docPath}SetInboundShipmentStatus.html
     * @param mixed $request array of parameters for Amazon_FWSInbound_Model_SetInboundShipmentStatusRequest request
     * or Amazon_FWSInbound_Model_SetInboundShipmentStatusRequest object itself
     * @see Amazon_FWSInbound_Model_SetInboundShipmentStatus
     * @return Amazon_FWSInbound_Model_SetInboundShipmentStatusResponse Amazon_FWSInbound_Model_SetInboundShipmentStatusResponse
     *
     * @throws Amazon_FWSInbound_Exception
     */
    public function setInboundShipmentStatus($request)
    {
        if (!$request instanceof Amazon_FWSInbound_Model_SetInboundShipmentStatusRequest) {
            require_once ('Amazon/FWSInbound/Model/SetInboundShipmentStatusRequest.php');
            $request = new Amazon_FWSInbound_Model_SetInboundShipmentStatusRequest($request);
        }
        require_once ('Amazon/FWSInbound/Model/SetInboundShipmentStatusResponse.php');
        return Amazon_FWSInbound_Model_SetInboundShipmentStatusResponse::fromXML($this->_invoke($this->_convertSetInboundShipmentStatus($request)));
    }

        // Private API ------------------------------------------------------------//

    /**
     * Invoke request and return response
     */
    private function _invoke(array $parameters)
    {
        $actionName = $parameters["Action"];
        $response = array();
        $responseBody = null;
        $statusCode = 200;

        /* Submit the request and read response body */
        try {

            /* Add required request parameters */
            $parameters = $this->_addRequiredParameters($parameters);

            $shouldRetry = true;
            $retries = 0;
            do {
                try {
                        $response = $this->_httpPost($parameters);
                        if ($response['Status'] === 200) {
                            $shouldRetry = false;
                        } else {
                            if ($response['Status'] === 500 || $response['Status'] === 503) {
                                $shouldRetry = true;
                                $this->_pauseOnRetry(++$retries, $response['Status']);
                            } else {
                                throw $this->_reportAnyErrors($response['ResponseBody'], $response['Status']);
                            }
                       }
                /* Rethrow on deserializer error */
                } catch (Exception $e) {
                    require_once ('Amazon/FWSInbound/Exception.php');
                    if ($e instanceof Amazon_FWSInbound_Exception) {
                        throw $e;
                    } else {
                        require_once ('Amazon/FWSInbound/Exception.php');
                        throw new Amazon_FWSInbound_Exception(array('Exception' => $e, 'Message' => $e->getMessage()));
                    }
                }

            } while ($shouldRetry);

        } catch (Amazon_FWSInbound_Exception $se) {
            throw $se;
        } catch (Exception $t) {
            throw new Amazon_FWSInbound_Exception(array('Exception' => $t, 'Message' => $t->getMessage()));
        }

        return $response['ResponseBody'];
    }

    /**
     * Look for additional error strings in the response and return formatted exception
     */
    private function _reportAnyErrors($responseBody, $status, Exception $e =  null)
    {
        $ex = null;
        if (!is_null($responseBody) && strpos($responseBody, '<') === 0) {
            if (preg_match('@<RequestId>(.*)</RequestId>.*<Error><Code>(.*)</Code><Message>(.*)</Message></Error>.*(<Error>)?@mi',
                $responseBody, $errorMatcherOne)) {

                $requestId = $errorMatcherOne[1];
                $code = $errorMatcherOne[2];
                $message = $errorMatcherOne[3];

                require_once ('Amazon/FWSInbound/Exception.php');
                $ex = new Amazon_FWSInbound_Exception(array ('Message' => $message, 'StatusCode' => $status, 'ErrorCode' => $code,
                                                           'ErrorType' => 'Unknown', 'RequestId' => $requestId, 'XML' => $responseBody));

            } elseif (preg_match('@<Error><Code>(.*)</Code><Message>(.*)</Message></Error>.*(<Error>)?.*<RequestID>(.*)</RequestID>@mi',
                $responseBody, $errorMatcherTwo)) {

                $code = $errorMatcherTwo[1];
                $message = $errorMatcherTwo[2];
                $requestId = $errorMatcherTwo[4];
                require_once ('Amazon/FWSInbound/Exception.php');
                $ex = new Amazon_FWSInbound_Exception(array ('Message' => $message, 'StatusCode' => $status, 'ErrorCode' => $code,
                                                              'ErrorType' => 'Unknown', 'RequestId' => $requestId, 'XML' => $responseBody));
            } elseif (preg_match('@<Error><Type>(.*)</Type><Code>(.*)</Code><Message>(.*)</Message>.*</Error>.*(<Error>)?.*<RequestId>(.*)</RequestId>@mi',
                $responseBody, $errorMatcherThree)) {

                $type = $errorMatcherThree[1];
                $code = $errorMatcherThree[2];
                $message = $errorMatcherThree[3];
                $requestId = $errorMatcherThree[5];
                require_once ('Amazon/FWSInbound/Exception.php');
                $ex = new Amazon_FWSInbound_Exception(array ('Message' => $message, 'StatusCode' => $status, 'ErrorCode' => $code,
                                                              'ErrorType' => $type, 'RequestId' => $requestId, 'XML' => $responseBody));

            } else {
                require_once ('Amazon/FWSInbound/Exception.php');
                $ex = new Amazon_FWSInbound_Exception(array('Message' => 'Internal Error', 'StatusCode' => $status));
            }
        } else {
            require_once ('Amazon/FWSInbound/Exception.php');
            $ex = new Amazon_FWSInbound_Exception(array('Message' => 'Internal Error', 'StatusCode' => $status));
        }
        return $ex;
    }



    /**
     * Perform HTTP post with exponential retries on error 500 and 503
     *
     */
    private function _httpPost(array $parameters)
    {

        $query = $this->_getParametersAsString($parameters);
        $url = parse_url ($this->_config['ServiceURL']);
        $post  = "POST / HTTP/1.0\r\n";
        $post .= "Host: " . $url['host'] . "\r\n";
        $post .= "Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n";
        $post .= "Content-Length: " . strlen($query) . "\r\n";
        $post .= "User-Agent: " . $this->_config['UserAgent'] . "\r\n";
        $post .= "\r\n";
        $post .= $query;
        $port = array_key_exists('port',$url) ? $url['port'] : null;
        $scheme = '';

        switch ($url['scheme']) {
            case 'https':
                $scheme = 'ssl://';
                $port = $port === null ? 443 : $port;
                break;
            default:
                $scheme = '';
                $port = $port === null ? 80 : $port;
        }

        $response = '';
        if ($socket = @fsockopen($scheme . $url['host'], $port, $errno, $errstr, 10)) {

            fwrite($socket, $post);

            while (!feof($socket)) {
                $response .= fgets($socket, 1160);
            }
            fclose($socket);

            list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
            $other = preg_split("/\r\n|\n|\r/", $other);
            list($protocol, $code, $text) = explode(' ', trim(array_shift($other)), 3);
        } else {
            throw new Exception ("Unable to establish connection to host " . $url['host'] . " $errstr");
        }


        return array ('Status' => (int)$code, 'ResponseBody' => $responseBody);
    }

    /**
     * Exponential sleep on failed request
     * @param retries current retry
     * @throws Amazon_FWSInbound_Exception if maximum number of retries has been reached
     */
    private function _pauseOnRetry($retries, $status)
    {
        if ($retries <= $this->_config['MaxErrorRetry']) {
            $delay = (int) (pow(4, $retries) * 100000) ;
            usleep($delay);
        } else {
            require_once ('Amazon/FWSInbound/Exception.php');
            throw new Amazon_FWSInbound_Exception (array ('Message' => "Maximum number of retry attempts reached :  $retries", 'StatusCode' => $status));
        }
    }

    /**
     * Add authentication related and version parameters
     */
    private function _addRequiredParameters(array $parameters)
    {
        $parameters['AWSAccessKeyId'] = $this->_awsAccessKeyId;
        $parameters['Timestamp'] = $this->_getFormattedTimestamp();
        $parameters['Version'] = self::SERVICE_VERSION;
        $parameters['SignatureVersion'] = $this->_config['SignatureVersion'];
        if ($parameters['SignatureVersion'] > 1) {
            $parameters['SignatureMethod'] = $this->_config['SignatureMethod'];
        }
        $parameters['Signature'] = $this->_signParameters($parameters, $this->_awsSecretAccessKey);

        return $parameters;
    }

    /**
     * Convert paremeters to Url encoded query string
     */
    private function _getParametersAsString(array $parameters)
    {
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }


    /**
     * Computes RFC 2104-compliant HMAC signature for request parameters
     * Implements AWS Signature, as per following spec:
     *
     * If Signature Version is 0, it signs concatenated Action and Timestamp
     *
     * If Signature Version is 1, it performs the following:
     *
     * Sorts all  parameters (including SignatureVersion and excluding Signature,
     * the value of which is being created), ignoring case.
     *
     * Iterate over the sorted list and append the parameter name (in original case)
     * and then its value. It will not URL-encode the parameter values before
     * constructing this string. There are no separators.
     *
     * If Signature Version is 2, string to sign is based on following:
     *
     *    1. The HTTP Request Method followed by an ASCII newline (%0A)
     *    2. The HTTP Host header in the form of lowercase host, followed by an ASCII newline.
     *    3. The URL encoded HTTP absolute path component of the URI
     *       (up to but not including the query string parameters);
     *       if this is empty use a forward '/'. This parameter is followed by an ASCII newline.
     *    4. The concatenation of all query string components (names and values)
     *       as UTF-8 characters which are URL encoded as per RFC 3986
     *       (hex characters MUST be uppercase), sorted using lexicographic byte ordering.
     *       Parameter names are separated from their values by the '=' character
     *       (ASCII character 61), even if the value is empty.
     *       Pairs of parameter and values are separated by the '&' character (ASCII code 38).
     *
     */
    private function _signParameters(array $parameters, $key) {
        $signatureVersion = $parameters['SignatureVersion'];
        $algorithm = "HmacSHA1";
        $stringToSign = null;
        if (0 === $signatureVersion) {
            $stringToSign = $this->_calculateStringToSignV0($parameters);
        } else if (1 === $signatureVersion) {
            $stringToSign = $this->_calculateStringToSignV1($parameters);
        } else if (2 === $signatureVersion) {
            $algorithm = $this->_config['SignatureMethod'];
            $parameters['SignatureMethod'] = $algorithm;
            $stringToSign = $this->_calculateStringToSignV2($parameters);
        } else {
            throw new Exception("Invalid Signature Version specified");
        }
        return $this->_sign($stringToSign, $key, $algorithm);
    }

    /**
     * Calculate String to Sign for SignatureVersion 0
     * @param array $parameters request parameters
     * @return String to Sign
     */
    private function _calculateStringToSignV0(array $parameters) {
        return $parameters['Action'] .  $parameters['Timestamp'];
    }

    /**
     * Calculate String to Sign for SignatureVersion 1
     * @param array $parameters request parameters
     * @return String to Sign
     */
    private function _calculateStringToSignV1(array $parameters) {
        $data = '';
        uksort($parameters, 'strcasecmp');
        foreach ($parameters as $parameterName => $parameterValue) {
            $data .= $parameterName . $parameterValue;
        }
        return $data;
    }

    /**
     * Calculate String to Sign for SignatureVersion 2
     * @param array $parameters request parameters
     * @return String to Sign
     */
    private function _calculateStringToSignV2(array $parameters) {
        $data = 'POST';
        $data .= "\n";
        $endpoint = parse_url ($this->_config['ServiceURL']);
        $data .= $endpoint['host'];
        $data .= "\n";
        $uri = array_key_exists('path', $endpoint) ? $endpoint['path'] : null;
        if (!isset ($uri)) {
        	$uri = "/";
        }
		$uriencoded = implode("/", array_map(array($this, "_urlencode"), explode("/", $uri)));
        $data .= $uriencoded;
        $data .= "\n";
        uksort($parameters, 'strcmp');
        $data .= $this->_getParametersAsString($parameters);
        return $data;
    }

    private function _urlencode($value) {
		return str_replace('%7E', '~', rawurlencode($value));
    }


    /**
     * Computes RFC 2104-compliant HMAC signature.
     */
    private function _sign($data, $key, $algorithm)
    {
        if ($algorithm === 'HmacSHA1') {
            $hash = 'sha1';
        } else if ($algorithm === 'HmacSHA256') {
            $hash = 'sha256';
        } else {
            throw new Exception ("Non-supported signing method specified");
        }
        return base64_encode(
            hash_hmac($hash, $data, $key, true)
        );
    }


    /**
     * Formats date as ISO 8601 timestamp
     */
    private function _getFormattedTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    }


                        
    /**
     * Convert GetServiceStatusRequest to name value pairs
     */
    private function _convertGetServiceStatus($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetServiceStatus';

        return $parameters;
    }
        
                                        
    /**
     * Convert GetFulfillmentIdentifierRequest to name value pairs
     */
    private function _convertGetFulfillmentIdentifier($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetFulfillmentIdentifier';
        foreach ($request->getMerchantItem() as $merchantItemgetFulfillmentIdentifierRequestIndex => $merchantItemgetFulfillmentIdentifierRequest) {
            if ($merchantItemgetFulfillmentIdentifierRequest->isSetASIN()) {
                $parameters['MerchantItem' . '.'  . ($merchantItemgetFulfillmentIdentifierRequestIndex + 1) . '.' . 'ASIN'] =  $merchantItemgetFulfillmentIdentifierRequest->getASIN();
            }
            if ($merchantItemgetFulfillmentIdentifierRequest->isSetCondition()) {
                $parameters['MerchantItem' . '.'  . ($merchantItemgetFulfillmentIdentifierRequestIndex + 1) . '.' . 'Condition'] =  $merchantItemgetFulfillmentIdentifierRequest->getCondition();
            }
            if ($merchantItemgetFulfillmentIdentifierRequest->isSetMerchantSKU()) {
                $parameters['MerchantItem' . '.'  . ($merchantItemgetFulfillmentIdentifierRequestIndex + 1) . '.' . 'MerchantSKU'] =  $merchantItemgetFulfillmentIdentifierRequest->getMerchantSKU();
            }

        }

        return $parameters;
    }
        
                                        
    /**
     * Convert GetFulfillmentIdentifierForMSKURequest to name value pairs
     */
    private function _convertGetFulfillmentIdentifierForMSKU($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetFulfillmentIdentifierForMSKU';
        foreach  ($request->getMerchantSKU() as $merchantSKUgetFulfillmentIdentifierForMSKURequestIndex => $merchantSKUgetFulfillmentIdentifierForMSKURequest) {
            $parameters['MerchantSKU' . '.'  . ($merchantSKUgetFulfillmentIdentifierForMSKURequestIndex + 1)] =  $merchantSKUgetFulfillmentIdentifierForMSKURequest;
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListAllFulfillmentItemsRequest to name value pairs
     */
    private function _convertListAllFulfillmentItems($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListAllFulfillmentItems';
        if ($request->isSetIncludeInactive()) {
            $parameters['IncludeInactive'] =  $request->getIncludeInactive() ? "true" : "false";
        }
        if ($request->isSetMaxCount()) {
            $parameters['MaxCount'] =  $request->getMaxCount();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListAllFulfillmentItemsByNextTokenRequest to name value pairs
     */
    private function _convertListAllFulfillmentItemsByNextToken($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListAllFulfillmentItemsByNextToken';
        if ($request->isSetNextToken()) {
            $parameters['NextToken'] =  $request->getNextToken();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert GetFulfillmentItemByMSKURequest to name value pairs
     */
    private function _convertGetFulfillmentItemByMSKU($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetFulfillmentItemByMSKU';
        foreach  ($request->getMerchantSKU() as $merchantSKUgetFulfillmentItemByMSKURequestIndex => $merchantSKUgetFulfillmentItemByMSKURequest) {
            $parameters['MerchantSKU' . '.'  . ($merchantSKUgetFulfillmentItemByMSKURequestIndex + 1)] =  $merchantSKUgetFulfillmentItemByMSKURequest;
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert GetFulfillmentItemByFNSKURequest to name value pairs
     */
    private function _convertGetFulfillmentItemByFNSKU($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetFulfillmentItemByFNSKU';
        foreach  ($request->getFulfillmentNetworkSKU() as $fulfillmentNetworkSKUgetFulfillmentItemByFNSKURequestIndex => $fulfillmentNetworkSKUgetFulfillmentItemByFNSKURequest) {
            $parameters['FulfillmentNetworkSKU' . '.'  . ($fulfillmentNetworkSKUgetFulfillmentItemByFNSKURequestIndex + 1)] =  $fulfillmentNetworkSKUgetFulfillmentItemByFNSKURequest;
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert GetInboundShipmentPreviewRequest to name value pairs
     */
    private function _convertGetInboundShipmentPreview($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetInboundShipmentPreview';
        if ($request->isSetShipFromAddress()) {
            $shipFromAddressgetInboundShipmentPreviewRequest = $request->getShipFromAddress();
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetName()) {
                $parameters['ShipFromAddress' . '.' . 'Name'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getName();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetAddressLine1()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine1'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getAddressLine1();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetAddressLine2()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine2'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getAddressLine2();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetCity()) {
                $parameters['ShipFromAddress' . '.' . 'City'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getCity();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetStateOrProvinceCode()) {
                $parameters['ShipFromAddress' . '.' . 'StateOrProvinceCode'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getStateOrProvinceCode();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetCountryCode()) {
                $parameters['ShipFromAddress' . '.' . 'CountryCode'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getCountryCode();
            }
            if ($shipFromAddressgetInboundShipmentPreviewRequest->isSetPostalCode()) {
                $parameters['ShipFromAddress' . '.' . 'PostalCode'] =  $shipFromAddressgetInboundShipmentPreviewRequest->getPostalCode();
            }
        }
        foreach ($request->getMerchantSKUQuantityItem() as $merchantSKUQuantityItemgetInboundShipmentPreviewRequestIndex => $merchantSKUQuantityItemgetInboundShipmentPreviewRequest) {
            if ($merchantSKUQuantityItemgetInboundShipmentPreviewRequest->isSetMerchantSKU()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemgetInboundShipmentPreviewRequestIndex + 1) . '.' . 'MerchantSKU'] =  $merchantSKUQuantityItemgetInboundShipmentPreviewRequest->getMerchantSKU();
            }
            if ($merchantSKUQuantityItemgetInboundShipmentPreviewRequest->isSetQuantity()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemgetInboundShipmentPreviewRequestIndex + 1) . '.' . 'Quantity'] =  $merchantSKUQuantityItemgetInboundShipmentPreviewRequest->getQuantity();
            }

        }

        return $parameters;
    }
        
                                        
    /**
     * Convert PutInboundShipmentRequest to name value pairs
     */
    private function _convertPutInboundShipment($request) {
        
        $parameters = array();
        $parameters['Action'] = 'PutInboundShipment';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        if ($request->isSetShipmentName()) {
            $parameters['ShipmentName'] =  $request->getShipmentName();
        }
        if ($request->isSetDestinationFulfillmentCenter()) {
            $parameters['DestinationFulfillmentCenter'] =  $request->getDestinationFulfillmentCenter();
        }
        if ($request->isSetShipFromAddress()) {
            $shipFromAddressputInboundShipmentRequest = $request->getShipFromAddress();
            if ($shipFromAddressputInboundShipmentRequest->isSetName()) {
                $parameters['ShipFromAddress' . '.' . 'Name'] =  $shipFromAddressputInboundShipmentRequest->getName();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetAddressLine1()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine1'] =  $shipFromAddressputInboundShipmentRequest->getAddressLine1();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetAddressLine2()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine2'] =  $shipFromAddressputInboundShipmentRequest->getAddressLine2();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetCity()) {
                $parameters['ShipFromAddress' . '.' . 'City'] =  $shipFromAddressputInboundShipmentRequest->getCity();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetStateOrProvinceCode()) {
                $parameters['ShipFromAddress' . '.' . 'StateOrProvinceCode'] =  $shipFromAddressputInboundShipmentRequest->getStateOrProvinceCode();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetCountryCode()) {
                $parameters['ShipFromAddress' . '.' . 'CountryCode'] =  $shipFromAddressputInboundShipmentRequest->getCountryCode();
            }
            if ($shipFromAddressputInboundShipmentRequest->isSetPostalCode()) {
                $parameters['ShipFromAddress' . '.' . 'PostalCode'] =  $shipFromAddressputInboundShipmentRequest->getPostalCode();
            }
        }
        foreach ($request->getMerchantSKUQuantityItem() as $merchantSKUQuantityItemputInboundShipmentRequestIndex => $merchantSKUQuantityItemputInboundShipmentRequest) {
            if ($merchantSKUQuantityItemputInboundShipmentRequest->isSetMerchantSKU()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemputInboundShipmentRequestIndex + 1) . '.' . 'MerchantSKU'] =  $merchantSKUQuantityItemputInboundShipmentRequest->getMerchantSKU();
            }
            if ($merchantSKUQuantityItemputInboundShipmentRequest->isSetQuantity()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemputInboundShipmentRequestIndex + 1) . '.' . 'Quantity'] =  $merchantSKUQuantityItemputInboundShipmentRequest->getQuantity();
            }

        }

        return $parameters;
    }
        
                                        
    /**
     * Convert PutInboundShipmentItemsRequest to name value pairs
     */
    private function _convertPutInboundShipmentItems($request) {
        
        $parameters = array();
        $parameters['Action'] = 'PutInboundShipmentItems';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        foreach ($request->getMerchantSKUQuantityItem() as $merchantSKUQuantityItemputInboundShipmentItemsRequestIndex => $merchantSKUQuantityItemputInboundShipmentItemsRequest) {
            if ($merchantSKUQuantityItemputInboundShipmentItemsRequest->isSetMerchantSKU()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemputInboundShipmentItemsRequestIndex + 1) . '.' . 'MerchantSKU'] =  $merchantSKUQuantityItemputInboundShipmentItemsRequest->getMerchantSKU();
            }
            if ($merchantSKUQuantityItemputInboundShipmentItemsRequest->isSetQuantity()) {
                $parameters['MerchantSKUQuantityItem' . '.'  . ($merchantSKUQuantityItemputInboundShipmentItemsRequestIndex + 1) . '.' . 'Quantity'] =  $merchantSKUQuantityItemputInboundShipmentItemsRequest->getQuantity();
            }

        }

        return $parameters;
    }
        
                                        
    /**
     * Convert DeleteInboundShipmentItemsRequest to name value pairs
     */
    private function _convertDeleteInboundShipmentItems($request) {
        
        $parameters = array();
        $parameters['Action'] = 'DeleteInboundShipmentItems';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        foreach  ($request->getMerchantSKU() as $merchantSKUdeleteInboundShipmentItemsRequestIndex => $merchantSKUdeleteInboundShipmentItemsRequest) {
            $parameters['MerchantSKU' . '.'  . ($merchantSKUdeleteInboundShipmentItemsRequestIndex + 1)] =  $merchantSKUdeleteInboundShipmentItemsRequest;
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListInboundShipmentsRequest to name value pairs
     */
    private function _convertListInboundShipments($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListInboundShipments';
        foreach  ($request->getShipmentStatus() as $shipmentStatuslistInboundShipmentsRequestIndex => $shipmentStatuslistInboundShipmentsRequest) {
            $parameters['ShipmentStatus' . '.'  . ($shipmentStatuslistInboundShipmentsRequestIndex + 1)] =  $shipmentStatuslistInboundShipmentsRequest;
        }
        if ($request->isSetCreatedBefore()) {
            $parameters['CreatedBefore'] =  $request->getCreatedBefore();
        }
        if ($request->isSetCreatedAfter()) {
            $parameters['CreatedAfter'] =  $request->getCreatedAfter();
        }
        if ($request->isSetMaxCount()) {
            $parameters['MaxCount'] =  $request->getMaxCount();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListInboundShipmentsByNextTokenRequest to name value pairs
     */
    private function _convertListInboundShipmentsByNextToken($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListInboundShipmentsByNextToken';
        if ($request->isSetNextToken()) {
            $parameters['NextToken'] =  $request->getNextToken();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert GetInboundShipmentDataRequest to name value pairs
     */
    private function _convertGetInboundShipmentData($request) {
        
        $parameters = array();
        $parameters['Action'] = 'GetInboundShipmentData';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert PutInboundShipmentDataRequest to name value pairs
     */
    private function _convertPutInboundShipmentData($request) {
        
        $parameters = array();
        $parameters['Action'] = 'PutInboundShipmentData';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        if ($request->isSetShipmentName()) {
            $parameters['ShipmentName'] =  $request->getShipmentName();
        }
        if ($request->isSetDestinationFulfillmentCenter()) {
            $parameters['DestinationFulfillmentCenter'] =  $request->getDestinationFulfillmentCenter();
        }
        if ($request->isSetShipFromAddress()) {
            $shipFromAddressputInboundShipmentDataRequest = $request->getShipFromAddress();
            if ($shipFromAddressputInboundShipmentDataRequest->isSetName()) {
                $parameters['ShipFromAddress' . '.' . 'Name'] =  $shipFromAddressputInboundShipmentDataRequest->getName();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetAddressLine1()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine1'] =  $shipFromAddressputInboundShipmentDataRequest->getAddressLine1();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetAddressLine2()) {
                $parameters['ShipFromAddress' . '.' . 'AddressLine2'] =  $shipFromAddressputInboundShipmentDataRequest->getAddressLine2();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetCity()) {
                $parameters['ShipFromAddress' . '.' . 'City'] =  $shipFromAddressputInboundShipmentDataRequest->getCity();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetStateOrProvinceCode()) {
                $parameters['ShipFromAddress' . '.' . 'StateOrProvinceCode'] =  $shipFromAddressputInboundShipmentDataRequest->getStateOrProvinceCode();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetCountryCode()) {
                $parameters['ShipFromAddress' . '.' . 'CountryCode'] =  $shipFromAddressputInboundShipmentDataRequest->getCountryCode();
            }
            if ($shipFromAddressputInboundShipmentDataRequest->isSetPostalCode()) {
                $parameters['ShipFromAddress' . '.' . 'PostalCode'] =  $shipFromAddressputInboundShipmentDataRequest->getPostalCode();
            }
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListInboundShipmentItemsRequest to name value pairs
     */
    private function _convertListInboundShipmentItems($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListInboundShipmentItems';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        if ($request->isSetMaxCount()) {
            $parameters['MaxCount'] =  $request->getMaxCount();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert ListInboundShipmentItemsByNextTokenRequest to name value pairs
     */
    private function _convertListInboundShipmentItemsByNextToken($request) {
        
        $parameters = array();
        $parameters['Action'] = 'ListInboundShipmentItemsByNextToken';
        if ($request->isSetNextToken()) {
            $parameters['NextToken'] =  $request->getNextToken();
        }

        return $parameters;
    }
        
                                        
    /**
     * Convert SetInboundShipmentStatusRequest to name value pairs
     */
    private function _convertSetInboundShipmentStatus($request) {
        
        $parameters = array();
        $parameters['Action'] = 'SetInboundShipmentStatus';
        if ($request->isSetShipmentId()) {
            $parameters['ShipmentId'] =  $request->getShipmentId();
        }
        if ($request->isSetShipmentStatus()) {
            $parameters['ShipmentStatus'] =  $request->getShipmentStatus();
        }

        return $parameters;
    }
        
                                                                                                                                                                                                                
}
