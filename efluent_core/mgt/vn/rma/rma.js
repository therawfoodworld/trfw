function showMenuProd(productID, invoice)
{
    var pos = $('#orderitem_'+productID).offset();
    $('#dialog1').show();
    $('#dialog1').offset({ top: pos.top, left: pos.left+20});
    $('#menuOption1Prod').html("<a href=\"javascript:top.e2window('/mgt/sm/viewsku.phtml?id="+productID+"&inwin=1', 'View SKU: "+productID+"','"+invoice+"');\">View SKU</a>");
    $('#menuOption2Prod').html("<a href=\"javascript:void(0);\" onclick=\"javascript:getItemLabelProd('"+productID+"')\">Print Asin Labels</a>");
} 
function hideMenuProd()
{
    $('#dialog1').hide();
}
function getItemLabelProd(pid) {
    var asin = '';
    $.get("/mgt/sm/ajax_get_asin.phtml", 
	{
		'id':pid
	},function(data){
	    asin = data;
	    if(asin!='') {
		var win = window.open("/mgt/reports/fba_shipment_creator/barcode_pdf/pdf_barcode.phtml?asin="+asin+"&pid="+pid,"mywindow","top=-1"); 
	    }
	    else {
		alert('Invalid ASIN number!');
	    }
    });
}

function emailPO()
{
    showSaveCover();

    $('#emailbox').show();
    $('#emailbox').css('top','25px');
    $('#emailbox').css('left','25px');
    document.mypo.emailto.focus();
}
function hideEmailBox()
{
    hideSaveCover()
    $('#emailbox').hide();
    $('#emailbox2').hide();
}
function showSaveCover() {
    $('#savecover').show();
    $('#savecover').css('top','0px');
    $('#savecover').css('left','0px');
    $('#savecover').css('width','100%');
    $('#savecover').css('height','100%');
}
function hideSaveCover() {
     $('#savecover').hide();
}
function SendEmail(rmaid)
{
	hideEmailBox();
	showSaveCover();
	$('#pleasewait').show();
	$('#pleasewait').css('top','25px');
	$('#pleasewait').css('left','25px');
	
  	var emailto = $('#emailto').val();
  	
	$.get("/mgt/vn/rma/ajax_emailrma.phtml", 
	    {
		    'emailto':emailto,
		    'rmaid':rmaid,
		    'sid':Math.random()
	    },function(data){
		alert("Email Successfully Sent");
		hideSaveCover();
		$('#pleasewait').hide();
		document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
	});
}
function updateRmaStatus(rmaid) {
    showSaveCover();
    $('#pleasewait').show();
    $('#pleasewait').css('top','25px');
    $('#pleasewait').css('left','25px');
    var status = $('#rmastatus').val();
    $.get("/mgt/vn/rma/ajax_updaterma.phtml", 
	{
		'status':status,
		'rmaid':rmaid,
		'sid':Math.random()
	},function(data){
	    hideSaveCover();
	    $('#pleasewait').hide();
	    document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
    });
}
function saveRmaQty(rmaid,pid) {
    showSaveCover();
    var qty = $('#rmaitem_qty_'+pid).val();
    $.get("/mgt/vn/rma/ajax_updatermaqty.phtml", 
	{
		'qty':qty,
		'rmaid':rmaid,
		'pid':pid,
		'sid':Math.random()
	},function(data){
	    hideSaveCover();
	    document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
    });
}
function saveRmaPrice(rmaid,pid) {
    showSaveCover();
    var price = $('#rmaitem_price_'+pid).val();
    $.get("/mgt/vn/rma/ajax_updatermaprice.phtml", 
	{
		'price':price,
		'rmaid':rmaid,
		'pid':pid,
		'sid':Math.random()
	},function(data){
	    hideSaveCover();
	    document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
    });
}
function deleteRmaItem(rmaid,pid) {
    showSaveCover();
    if(confirm('Are you sure you want to delete this item? pid:'+pid))
    {
	$.get("/mgt/vn/rma/ajax_deletermaitem.phtml", 
	    {
		    'rmaid':rmaid,
		    'pid':pid,
		    'sid':Math.random()
	    },function(data){
		hideSaveCover();
		document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
	});
    }
    else {
	 hideSaveCover();
    }
}
function createOrder(rmaid) {
    showSaveCover();
    if(confirm('Are you sure you want to convert this rma?'))
    {
	$.get("/mgt/vn/rma/saveorder.phtml", 
	    {
		    'rmaid':rmaid,
		    'sid':Math.random()
	    },function(data){
		hideSaveCover();
		document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
	});
    }
    else {
	 hideSaveCover();
    }
}
function saveRmaNotes(rmaid) {
    showSaveCover();
    var notes = $('#rma_notes').val();
    $.post("/mgt/vn/rma/ajax_savenotes.phtml", 
	{
		'notes':notes,
		'rmaid':rmaid,
		'sid':Math.random()
	},function(data){
	    hideSaveCover();
	    document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
    });
}
function saveVendorRma(rmaid) {
    showSaveCover();
    var vendor_rma = $('#vendor_rma_val').val();
    $.post("/mgt/vn/rma/ajax_savevendorrma.phtml", 
	{
		'vendor_rma':vendor_rma,
		'rmaid':rmaid,
		'sid':Math.random()
	},function(data){
	    hideSaveCover();
	    document.location = "/mgt/vn/rma/viewrma.phtml?rmaid="+rmaid+"&inwin=1";
    });
}


function attachFile()
{
	var cvr = document.getElementById("savecover");
	cvr.style.display = "block";
	
	cvr.style.top = 0;
	cvr.style.left = 0;
	
	cvr.style.width = "100%";
	cvr.style.height = "100%";
	
	var dlg = document.getElementById("attach_file");
	dlg.style.display = "block";
	
	dlg.style.top = 25;
	dlg.style.left = 225;
	
}
function hideAttachBox()
{
	var cvr = document.getElementById("savecover");
	cvr.style.display = "none";

	var dlg = document.getElementById("attach_file");
	dlg.style.display = "none";
	
}
function deleteAttachedFile(fid) {
	if(confirm('Are you sure you want to delete this file?')) {
		$.get("/mgt/vn/rma/ajax_delete_file.phtml", {'fid':fid},
	   		function(data){
			$('#file_'+fid).remove();
	   	});
	}
}

function getHistory(sku) {
    if($("tr[id='item_history_"+sku+"']").length>0) {
        $("tr[id='item_history_"+sku+"']").toggle();
    }
    else {
        $.get('ajax_getpohistory.phtml', 
            {
                    'sku':sku
            }, function(data) {
                $("tr[id='item_"+sku+"']").after(data);
        });
    }
}