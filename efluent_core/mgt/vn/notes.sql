
drop table vendor_merge_profiles;
create table vendor_merge_profiles
(
id bigint NOT NULL auto_increment,
profile_name varchar(255),
index(id)
);

drop table vendor_merge_profiles_extended;
create table vendor_merge_profiles_extended
(
pfid bigint NOT NULL default 0,
data longblob,
index(pfid)
);


