var oTable;
var columnSort = new Array; 
$(document).ready(function(){
    $('#sitesDataTable thead tr th').each(function(){
        if($(this).attr('bSortable') == 'true') {
            columnSort.push({ "bSortable": true });
        } else {
            columnSort.push({ "bSortable": false });
        }
    });
	
	$('.button_search').click(function(){
		$('div#skuresults').show();
		var search_url = base_url + 'sm/search/get_result';

		$("#sitesDataTable").dataTable().fnDestroy();    // destroy the old datatable
		oTable = $('#sitesDataTable').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"sDom": '<"H"Zp>t<""l>',
			"bFilter": true,
			"bInfo": false,
			"bLengthChange": false,
	        "iDisplayLength": $('select.search_num2return').val(),
	        "iDisplayStart" : 0,
	        'bProcessing': false,
	        "oListNav": {
	        	iIndex: 2
	        },
			'bServerSide': true,
	        "fnServerParams": function (aoData){
				aoData.push({ "name": "searchKeyword", "value": "" },
							{ "name": "filterKeyword", "value": ""},
							{ "name": "filterType", "value": ""}
							);
				$("input[class^='search_'],select[class^='search_']").each(function() {
					form_name = $(this).attr('id');
	
					// check for checkboxes
					if ($(this).attr('type') == 'checkbox') {
						if ($(this).attr('checked')) {
							var form_value = $(this).val();
						}
					} else {
						form_value = $(this).val();
					}
					var form_obj = {};
					form_obj['name'] = form_name;	
					form_obj['value'] = form_value;
					aoData.push(form_obj);
				});
				$('div#skuresults').show();
	        },
	        "aoColumns": columnSort,
			'sAjaxSource': search_url,
			"sServerMethod": "POST",
	        'fnDrawCallback': function(data){
	        	var res = $.parseJSON(data.jqXHR.responseText);
				$('div#skuresults').show();
	        	var num_rows = data._iRecordsDisplay;
	        	$('div#num_rows').html(num_rows + ' SKUs');
	
	        	$("#sitesDataTable tbody").click(function(event) {
	            	$(oTable.fnSettings().aoData).each(function (){
	            		$(this.nTr).removeClass('row_selected');
	            	});
	            	$(event.target.parentNode).addClass('row_selected');
	            });
	       		$('div#skuresults').hide();
			}
		});
	});

	$('.search_group').change(function() {
		var group_id = $('.search_group').val();
		$.post(base_url + 'sm/search/get_lists',{ 'id' : group_id },function(data){
			$('.search_list').empty().append(data);	
		});	
	});
});

function get_categories(level) {
	var thislevel = level - 1;
	for (var i = 0; i <= 5; i++) {
		if(thislevel == i) {
			var cat_id = $('.search_cat' + i).val();
		}
	}
	
	if (cat_id == 0) {
		for (var i = level; i <= 5; i++) {
			$('.search_cat' + i).empty().fadeOut();
		}
		get_brands(cat_id);
	} else {
		$.post(base_url + 'sm/search/get_categories',{ 'id' : cat_id, 'sid' : Math.random() },function(data){
			if (data != '') {
				for (var i = level; i <= 5; i++) {
					$('.search_cat' + i).empty().fadeOut();
				}
				
				$('.search_cat' + level).empty().append(data).fadeIn();	
				get_brands(cat_id);
			}
		});
	}	
}

function get_brands(cat_id) {
	$.post(base_url + 'sm/search/get_brands',{ 'id' : cat_id },function(data){
			$('.search_brand').empty().append(data);	
	});	
}