$(document).ready(function() {
	$('.button_search').click(function() {
		$('div#skuresults').show();
		var form_array = {};	
		$("input[class^='search_'],select[class^='search_']").each(function() {
			form_name = $(this).attr('id');
			// check for checkboxes
			if ($(this).attr('type') == 'checkbox') {
				if ($(this).is(':checked')) {
					var form_value = $(this).val();
				}
			} else {
				form_value = $(this).val();
			}
			form_array[form_name] = form_value;
		});		

		$(function() {
	        $('#dg').datagrid({
	            url: base_url + 'sm/searcheasyui/get_result',
				fitColumns: true,
	            pagination: true,
	            pageList: [50,100,1000,2500,9999],
	            queryParams: form_array,
	        });
		});	
		$('div#skuresults').hide();
	});
	
	$('.search_group').change(function() {
		var group_id = $('.search_group').val();
		$.post(base_url + 'sm/searcheasyui/get_lists',{ 'id' : group_id },function(data){
			$('.search_list').empty().append(data);	
		});	
	});
});

function get_categories(level) {
	var thislevel = level - 1;
	for (var i = 0; i <= 5; i++) {
		if(thislevel == i) {
			var cat_id = $('.search_cat' + i).val();
		}
	}
	
	if (cat_id == 0) {
		for (var i = level; i <= 5; i++) {
			$('.search_cat' + i).empty().fadeOut();
		}
	} else {
		$.post(base_url + 'sm/search/get_categories',{ 'id' : cat_id, 'sid' : Math.random() },function(data){
			if (data != '') {
				for (var i = level; i <= 5; i++) {
					$('.search_cat' + i).empty().fadeOut();
				}
				
				$('.search_cat' + level).empty().append(data).fadeIn();	
				get_brands(cat_id);
			}
		});
	}	
}

function get_brands(cat_id) {
	$.post(base_url + 'sm/search/get_brands',{ 'id' : cat_id },function(data){
			$('.search_brand').empty().append(data);	
	});	
}