<?php
class MY_Controller extends CI_Controller
{
    public $data = array();
    public $header_file;
    public $menu_file;
    public $footer_file;

	function __construct()
	{
		parent::__construct();

		$this->write_db = $this->load->database('write', TRUE);		
		$this->read_db = $this->load->database('read', TRUE);
		
		// init the js and css arrays
		$this->data['css'] = array();
		$this->data['js'] = array();
		
		// init the js variables that might be needed as void array
		$this->data['js_vars'] = array('BASE_URL' => $this->config->item('base_url'));
		$this->data['BASE_URL'] = $this->config->item('base_url');
		
		// init the js_code that might be needed as void string
		$this->data['js_code'] = array();
		$this->data['external_js'] = array();

		$this->header_file = "common/header_tpl";
		$this->menu_file = "common/menu_tpl";
		$this->footer_file = "common/footer_tpl";
		$this->account = 'ecom';
	}
}