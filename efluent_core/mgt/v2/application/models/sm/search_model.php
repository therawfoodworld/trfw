<?php
class Search_model extends CI_Model
{
    private $searchTables = array();
    private $searchFields = array();
    private $searchClauses = array();
    private $resultsHeader = array();
	
	public function __construct()
    {
		parent::__construct();
		$this->load->model('efluent_model', 'efluent');
		$this->userID = $this->session->userdata('ID');
		return $this;
    }
    
    public function getSearchFilters($userID)
    {
    	$filters_array = array();
		$query = $this->read_db->query("SELECT colname, coltitle, dorder
										FROM user_search_filters 
										WHERE userid = '$userID'
										ORDER BY dorder ASC");
		$result = $query->result();
		if ($query->num_rows() > 0)
		{
			foreach ($result AS $filter)
			{
				array_push($filters_array,array('id' => $filter->colname,
												'title' => $filter->coltitle
											   ));
			}
		}
		else
		{
			$filters_array = array(array('id' => 'keywords',
										 'title' => 'Keywords'),
								   array('id' => 'id',
										 'title' => 'ID'),
								   array('id' => 'asin',
										 'title' => 'ASIN'),
								   array('id' => 'sku',
								   		 'title' => 'SKU'),
								   array('id' => 'category',
										 'title' => 'Category'),
								   array('id' => 'brand',
								   		 'title' => 'Brand'),
								   array('id' => 'list',
								   		 'title' => 'List'),
								   array('id' => 'exc_list',
										 'title' => 'EXC if on list'),
								   array('id' => 'man_part',
								   		 'title' => 'Man Part'),
								   array('id' => 'condition',
										 'title' => 'Condition'),
								   array('id' => 'upc',
								   		 'title' => 'UPC'),
								   array('id' => 'visible',
										 'title' => 'Visible'),
								   array('id' => 'display_on',
										 'title' => 'Display On'),
								   array('id' => 'return',
										 'title' => 'Return')
								  );
		}
		return $filters_array;
    }
    
    public function orderResultColumns($query_result,$header_columns)
    {
		$search_result = array();
		foreach ($query_result AS $k => $v)
		{
			foreach ($header_columns AS $column)
    		{
    			switch ($column['id'])
				{
		    		case 'id':
			        	$search_result[$k][] = $v['id'];
			        	break;
		    		case 'sku':
			        	$search_result[$k][] = $v['sku'];
			        	break;
			        case 'asin':
			        	$search_result[$k][] = $v['asin'];
			        	break;
			        case 'avgcost':
			        	$search_result[$k][] = $v['avgcost'];
			        	break;
			        case 'instock':
			        	$search_result[$k][] = $v['instock'] ." / ". $v['dsqty'];
			        	break;
		        	case 'cat':
			        	$search_result[$k][] = $v['cattree'];
			        	break;
			    	case 'eta':
						$query = $this->read_db->query("SELECT vendor_pos.eta
														FROM vendor_pos
														JOIN vendor_po_lines ON vendor_po_lines.poid = vendor_pos.poid
														WHERE vendor_po_lines.pid = '".$v['id']."'
														AND vendor_pos.status = '1'
														ORDER BY vendor_pos.eta ASC");
						$result = $query->row();
						if ($result->eta > 0)
						{
							$search_result[$k][] = date("m/d/Y", strtotime($result->eta));
						}
						else
						{
							$search_result[$k][] = '';
						}			    	
				       	break;
				    case 'man':
				        	$search_result[$k][] = $v['manname'];
				        break;
				    case 'manpart':
				        	$search_result[$k][] = $v['manpart'];
				        break;
				    case 'descr':
				        	$search_result[$k][] = $v['descr'];
				        break;
				    case 'upccode':
				        	$search_result[$k][] = $v['upccode'];
				        break;
				    case 'scost':
				        	$search_result[$k][] = $this->efluent_model->getSalesmanCost($v['id']);
				        break;
				    case 'status':
				    	if ($v['status'] == 0)
				    	{
				    		$search_result[$k][] = 'Active';
				    	}
				    	elseif ($v['status'] == 1)
				    	{
				    		$search_result[$k][] = 'Special Order';
				    	}
				    	elseif ($v['status'] == 2)
				    	{
				    		$search_result[$k][] = 'End of Life';
				    	}
						elseif ($v['status'] == 3)
						{
				    		$search_result[$k][] = 'Discontinued';
						}
				        break;
				    case 'visible':
				    	if ($v['visible'] == 1)
				    	{
				    		$search_result[$k][] = 'X';
				    	}
				    	else
				    	{
				        	$search_result[$k][] = '';
				    	}
				        break;
				    case 'dropship':
				    	if ($v['dropship'] == 1)
				    	{
				    		$search_result[$k][] = 'X';
				    	}
				    	else
				    	{
				        	$search_result[$k][] = '';
				    	}
				        break;
				    case 'refurb':
				    	if ($v['refurb'] == 1)
				    	{
				    		$search_result[$k][] = 'X';
				    	}
				    	else
				    	{
				        	$search_result[$k][] = '';
				    	}
				        break;
				    case 'buy':
							$query = $this->read_db->query("SELECT pid
															FROM buy_listing
															WHERE pid = '".$v['id']."'");
							if ($query->num_rows() > 0)
							{
								$search_result[$k][] = '1';
							}
							else {
								$search_result[$k][] = '0';
							}
				    	break;
				    case 'alt_total':
						// specific Feed (from SiteConfig)
						$alt_sku_parent = $this->efluent_model->getConfigValue('eFluent Settings', 'ALT_SKU_PARENT');
						$alt_sku_parentid = $this->efluent_model->getConfigValue('eFluent Settings', 'ALT_SKU_PARENTID');

						$query = $this->read_db->query("SELECT text_short
														FROM custom_values
														WHERE pid = '".$v['id']."'
														AND cfid = '$alt_sku_parent'");
						if ($query->num_rows() > 0)
						{
							$is_parent = $query->row()->text_short;
							if ($is_parent == '1')
							{
								$query = $this->read_db->query("SELECT SUM(product_prices.onhold) AS alt_onhold, SUM(product_prices.committed) AS alt_committed
																FROM product_prices 
																JOIN custom_values ON custom_values.pid = product_prices.id
																WHERE custom_values.text_short = '".$v['id']."'
																AND custom_values.cfid = '$alt_sku_parentid'");
								$alt_onhold = $query->row()->alt_onhold;
								$alt_committed = $query->row()->alt_committed;
								$search_result[$k][] = ($alt_onhold+$v['onhold']) . ",".($alt_committed+$v['committed']).",".($v['instock'] - ($alt_onhold+$v['onhold'])-($alt_committed+$v['committed']));
							}
							else
							{
								$search_result[$k][] = '';
							}
						}
						else 
						{
							$search_result[$k][] = '';
						}				    	
				    	break;
				    case 'stockdetail':

							if ($this->efluent_model->isAltSKU($v['id']))
							{
								$iavailable = $v['instock'];
							}
							else 
							{
								$iavailable = $v['instock'] - $v['onhold'] - $v['committed'];
							}
							
							$search_result[$k][] = $v['instock'];
							$search_result[$k][] = $v['onhold'];
							$search_result[$k][] = $v['committed'];
							$search_result[$k][] = $iavailable;
							$search_result[$k][] = $v['onpo'];
				    	break;
				    default:
				    	break;    
				}
			 
				if (strpos($column['id'],'cfid') !== false)
				{
					$col_cfid = substr($column['id'],4);
					$query = $this->read_db->query("SELECT custom_fields.type, custom_values.text_short, custom_values.text_long
													FROM custom_fields
													LEFT JOIN custom_values ON custom_fields.id = custom_values.cfid
													AND custom_values.pid = '".$v['id']."'
													WHERE custom_fields.id = '$col_cfid'");
					$cfid_type = $query->row()->type;
					$cfid_text_short = $query->row()->text_short;
					$cfid_text_long = $query->row()->text_long;
					
					switch ($cfid_type) 
					{
						case '0':
							if ($cfid_text_short == '1') 
							{
								$search_result[$k][] = 'Yes';
							}
							else
							{
								$search_result[$k][] = 'No';
							}
							break;
						case '1':
							$search_result[$k][] = $cfid_text_short;
							break;
						case '2':
							$search_result[$k][] = $cfid_text_long;
							break;
					}
				}
        	}
		}
		return $search_result;    
    }    
    
    public function getSearchColumns($userID)
    {
    	$columns_array = array();
		$query = $this->read_db->query("SELECT colname, dorder, coltitle
										FROM user_search_columns 
										WHERE userid = '$userID'
										ORDER BY dorder ASC");
		$result = $query->result();
		if ($query->num_rows() > 0)
		{
			foreach ($result AS $column)
			{
				array_push($columns_array,array('id' => $column->colname,
												'title' => $column->coltitle
											   ));
											   											   
			}
		}
		else
		{
			$columns_array = array(array('id' => 'id',
										 'title' => 'ID'),
								   array('id' => 'man_part',
								   		 'title' => 'MAN'),
								   array('id' => 'descr',
								   		 'title' => 'Descr (title)'),
								   array('id' => 'upccode',
								   		 'title' => 'UPC'),
								   array('id' => 'sku',
								   		 'title' => 'SKU')
								  );
		} 
		return $columns_array;
    }

    public function addExceptionsSearchColumns($columns_array)
    {
		$columns_array_tmp = array();
    	foreach ($columns_array AS $k => $v)
		{
			$exclude_columns = array('columnperhouse','columnperstock','stockdetail','dsinventory','namedpl');
			if ($v['id'] == 'stockdetail')
			{
				$columns_array_tmp[] = 'OnHand';
				$columns_array_tmp[] = 'OnHold';
				$columns_array_tmp[] = 'Committed';
				$columns_array_tmp[] = 'Available';
				$columns_array_tmp[] = 'OnPO';
			}
			else if($v['id'] == 'namedpl')
			{
				$query = $this->read_db->query("SELECT config_key, config_value
												FROM configuration
												WHERE config_group = 'Price Level Names'
												AND config_key != 'NULL'
												AND config_key != ''");
				foreach ($query->result_array() as $row)
				{
					$columns_array_tmp[] = $row['config_value'];
				}
			}
			elseif($v['id'] == 'columnperhouse')
			{
				$query = $this->read_db->query("SELECT config_key AS whid, config_value AS whname
												FROM configuration
												WHERE config_group = 'Warehouse List'
												AND config_key IS NOT NULL");
				foreach ($query->result_array() as $row)
				{
					$columns_array_tmp[] = 'WH '.$row['whname'];
				}
			}
			elseif ($v['id'] == 'dsinventory')
			{
				$query = $this->read_db->query("SELECT DISTINCT(ps.distid), v.vendorname
												FROM product_sourcing ps, vendors v
												WHERE ps.distid = v.vendorid
												AND ps.stealth = 0
												ORDER BY vendorid");
				foreach ($query->result_array() AS $row)
				{
					$vendorname = $row['vendorname'];
					if(strlen($row['vendorname']) > 15)
					{
						$vendorname = substr($row['vendorname'], 0 , 12) . '..';
					}
					$columns_array_tmp[] = '[' . $row['distid'] . '] ' . $vendorname;
				}
			}
			else
			{
				if (!in_array($v['id'],$exclude_columns)) 
				{
					$columns_array_tmp[] = $v;
				}
			}
		}
		return $columns_array_tmp;    
    }
    
	public function getResultNew($query,$query_count,$filters)
    {
    	if (is_array($filters))
    	{
    		$itemsPerPage = $filters['iDisplayLength'];
    		$limitStart = $filters['iDisplayStart'];
    		$sort_column = $filters['iSortCol_0'];
    		$sort_type = $filters['sSortDir_0'];
    	}
    	if ($sort_column == 0)
        {
        	$sort_column = 'product_list.id';
        }
        elseif ($sort_column == 1)
        {
            $sort_column = 'product_list.manid';
        }
        elseif ($sort_column == 2)
        {
            $sort_column = 'product_list.descr';
        }
        elseif ($sort_column == 3)
        {
            $sort_column = 'product_list.upccode';
        }
        elseif ($sort_column == 4)
        {
            $sort_column = 'product_list.sku';
        }
        else 
        {
        	$sort_column = 'product_list.id';
        }
        
        if ($sort_type == '') {
        	$sort_type = 'DESC';
        }
        
		$query_count = $this->read_db->query($query_count);
		$total_records_res = $query_count->row();    
		$total_records = $total_records_res->nr;
		$search_results = '';
		
		if ($limitStart == '' || $limitStart == null)
		{
			$limitStart = 0;
		}
		if ($itemsPerPage == '' || $itemsPerPage == null)
		{
			$itemsPerPage = (int) $filters['search_num2return'];
		}
		if ($total_records > 0)
    	{
    		$q = "$query
    			  ORDER BY $sort_column $sort_type 
    			  LIMIT $limitStart,$itemsPerPage";
    		$query_res = $this->read_db->query($q);
    		
			$result = $query_res->result_array();
			$search_results = $this->orderResultColumns($result,$this->getSearchColumns($this->userID));
    	}
		return array("accounts"=>$search_results,"total_records"=>$total_records);
	}
}