<?php
class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('efluent_model', 'efluent');
	}
    
	public function isLoggedIn()
	{
		if(!$this->session->userdata('ID'))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function authUser($user, $pass)
	{
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$tstamp = date('Y-m-d H:i:s', time());
		$response = array();

		if (!empty($user) && !empty($pass))
		{
			$query_user = $this->read_db->query("SELECT id, displayname, emailaddr, type
										   		 FROM users
										   		 WHERE un = '".mysql_real_escape_string(trim($user))."'
										   		 AND pw_md5 = MD5('".mysql_real_escape_string(trim($pass))."')
												 LIMIT 1");
			if ($query_user->num_rows() > 0)
			{
				// okay, it looks like they're legit...  let's see if there are any ip restrictions
				$query = $this->read_db->query("SELECT ipaddr
												FROM user_ip_restriction
												WHERE userid = '$ip_address'
												LIMIT 1");
				if ($query->num_rows() > 0)
				{
					$response['errors'][] = 'Your IP is banned.';
					$this->efluent->logAction('FAILURE', '5', $query_user->row()->id, $query_user->row()->displayname.' Attempted Login in from IP: '.$ip_address.' (DENIED BY RULE)', $query_user->row()->displayname.' Attempted Login in from IP: '.$ip_address.' (DENIED BY RULE)');
				}
				else 
				{
					$set_cookie_array = array('ID' => $query_user->row()->id, 'UN' => $user, 'DISPLAYNAME' => $query_user->row()->displayname, 'EMAILADDR' => $query_user->row()->emailaddr, 'USERTYPE' => $query_user->row()->type);
					$this->session->set_userdata($set_cookie_array);
					$this->efluent->touch();
					$this->efluent->logAction('SUCCESS', '5', $query_user->row()->id, $query_user->row()->displayname.' Logged in from IP: '.$ip_address, $query_user->row()->displayname.' Logged in from IP: '.$ip_address);
					
					$this->write_db->query("INSERT INTO login_log (success, tstamp, un, ip_addr, descr)
											VALUES ('1', '$tstamp', '$user', '$ip_address', '".$query_user->row()->displayname." Logged in')");
				}
			}
			else 
			{
				$response['errors'][] = 'Rong username or password, please try again.';
				$this->efluent->logAction('FAILURE', '5', 0, 'Failed login from: '.$ip_address.' (username: '.$user.')', 'Failed login from: '.$ip_address.' (username: '.$user.')');
				$this->write_db->query("INSERT INTO login_log (success, tstamp, un, ip_addr, descr)
										VALUES ('1', '$tstamp', '$user', '$ip_address', '$user Failed Login')");
			}
        }
        else 
        {
        	$response['errors'][] = 'Please write your username and password.';
        }
        return $response;
	}
}