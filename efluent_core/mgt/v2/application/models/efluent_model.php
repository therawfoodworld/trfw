<?php
class Efluent_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
    }
    
	public function logAction($ltype, $type, $updateid, $title, $details)
	{
		$details = mysql_real_escape_string(trim($details));
		$title = mysql_real_escape_string(trim($title));
		$tstamp = date("Y-m-d H:i:s");
		$session_user_id = @$this->session->userdata('ID');
		
		$this->write_db->query("INSERT INTO action_log (ltype, type, updateid, title, tstamp, userid)
								VALUES ('$ltype', '$type', '$updateid', '$title', '$tstamp', '$session_user_id')");
		$lid = $this->write_db->insert_id();

		$this->write_db->query("INSERT INTO action_log_extended (lid, details)
								VALUES ('$lid', '$details')");
	}
	
	public function touch()
	{
		$id = @$this->session->userdata('ID');
		$ip_address = $_SERVER['REMOTE_ADDR'];
		if ($id > 0)
		{
			$tstamp = date('Y-m-d H:i:s', time());
			$this->write_db->query("UPDATE users
									SET last_ip = '$ip_address', tstamp = '$tstamp'
									WHERE id = '$id'
									LIMIT 1");
		}
	}
	
	public function getConfigValue($group, $key)
	{
		$group = mysql_real_escape_string(trim($group));
		$key = mysql_real_escape_string(trim($key));
		
		$query = $this->read_db->query("SELECT config_value
										FROM configuration
										WHERE config_group = '$group'
										AND config_key = '$key'
										LIMIT 1");
		if ($query->num_rows() > 0)
		{
			return $query->row()->config_value;
		}
	}
	
    public function getCategories($catID)
    {
    	if ($catID > 0)
    	{
	    	$query = $this->read_db->query("SELECT REPLACE(catid,',','') AS catid, REPLACE(catname,',','') AS catname
											FROM product_categories
											WHERE parentid = '$catID'
											ORDER BY catname ASC");
    	}
    	else 
    	{
	    	$query = $this->read_db->query("SELECT REPLACE(catid,',','') AS catid, REPLACE(catname,',','') AS catname
											FROM product_categories
											WHERE parentid = '0'
											ORDER BY catname ASC");
    	}
		$result = $query->result();
        return $result;
    }
	
    public function getAllAssignedTo()
    {
    	$query = $this->read_db->query("SELECT id, displayname
    									FROM users
    									ORDER BY displayname ASC");
		$result = $query->result();
        return $result;
    }
    
    public function getGroups()
    {
		$query = $this->read_db->query("SELECT id, group_name
										FROM list_groups
										ORDER BY group_name ASC");
		$result = $query->result();
        return $result;
    }
    
    
    public function getLists($groupID)
    {
    	if ($groupID > 0)
    	{
			$query = $this->read_db->query("SELECT l.lid, l.list_title
											FROM lists l
											JOIN list_groups_map lgm
											ON l.lid = lgm.list_id
											WHERE lgm.group_id = '$groupID'
											ORDER BY l.list_title ASC");
    	}
    	else 
    	{
			$query = $this->read_db->query("SELECT lid, list_title
											FROM lists
											ORDER BY list_title ASC");
    	}
		$result = $query->result();
        return $result;
    }
    
	public function getBrands($catID)
    {
    	if ($catID > 0)
    	{
    		$query = $this->read_db->query("SELECT DISTINCT(pm.manid), pm.manname
    										FROM product_manufacturers pm
    										JOIN product_list pl ON pm.manid = pl.manid
    										JOIN product_category_map pcm ON pl.id = pcm.pid
    										WHERE pcm.catid = '$catID'
    										ORDER BY pm.manname ASC");
    	}
    	else 
    	{
			$query = $this->read_db->query("SELECT manid, manname
											FROM product_manufacturers 
											ORDER BY manname ASC");
    	}
		$result = $query->result();
        return $result;
    }
    
    public function getAllWarehouses()
    {
    	$query = $this->read_db->query("SELECT config_key AS whid, config_value AS whname
    									FROM configuration
    									WHERE config_group = 'Warehouse List'
    									AND config_key != ''");
		$result = $query->result();
        return $result;
    }    
    
	public function getAllVendors()
    {
    	$query = $this->read_db->query("SELECT vendorid, vendorname
    									FROM vendors
    									ORDER BY vendorname ASC");
		$result = $query->result();
        return $result;
    }
   
	public function getAllSitesDisplayOn()
    {
    	$query = $this->read_db->query("SELECT divid, sitename
    									FROM site_list
    									ORDER BY sitename ASC");
		$result = $query->result();
        return $result;
    }    

	public function getActualCost($id)
	{
		$query = $this->read_db->query("SELECT avgcost
										FROM product_prices
										WHERE id = '$id'
										LIMIT 1");
		$avgcost = $query->row()->avgcost;
		if($avgcost <= 0)
		{
			// no avgcost?  Go to sourcing
			$query = $this->read_db->query("SELECT cost
											FROM product_sourcing
											WHERE pid = '$id'
											ORDER BY tstamp DESC
											LIMIT 0, 1");
			$avgcost = $query->row()->cost;
		}
		return $avgcost;
	}    
    
	public function getSalesmanCost($id)
	{
		$avgcost = $this->getActualCost($id);

		$query = $this->read_db->query("SELECT sdollar, spercent
										FROM salesman_cost
										WHERE pid = '$id'
										AND type = '1'");
		if ($query->num_rows() > 0)
		{
			// per sku.... you can enter a fixed cost
			$sdollar = $query->row()->sdollar;
			$spercent = $query->row()->spercent;
			if (floatval($sdollar) > 0)
			{
				return $sdollar;
			}
			elseif (floatval($spercent) > 0)
			{
				return $avgcost + ($avgcost * ($spercent / 100));
			}
		}
		else
		{
			// see if there's a cost per category
			$cattree = $this->getCatTree($id);
			if (is_array($cattree))
			{
				for($xx = 0; $xx < count($cattree['catid']); $xx++)
				{
					$query = $this->read_db->query("SELECT sdollar, spercent
													FROM salesman_cost
													WHERE catid = '".$cattree['catid'][$xx]."'
													AND type = '0'");
					if ($query->num_rows() > 0)
					{
						$sdollar = $query->row()->sdollar;
						$spercent = $query->row()->spercent;
						
						// on a category level...... its cost + %markup + sdollar
						$newcost = $avgcost; // just set a default

						if(floatval($spercent) > 0) $newcost = $avgcost + ($avgcost * ($spercent / 100));
						if(floatval($sdollar) > 0) $newcost += $sdollar;

						return $newcost;
					}
				}
			}
		}

		// if we made it here..... we have no salesmancost available
		// see if there's a global cost basis
		$query = $this->read_db->query("SELECT sdollar, spercent
										FROM salesman_cost
										WHERE type = '0'
										AND catid = '0'");
		if($query->num_rows() > 0)
		{
			$sdollar = $query->row()->sdollar;
			$spercent = $query->row()->spercent;

			// on a category level...... its cost + %markup + sdollar
			$newcost = $avgcost; // just set a default

			if(floatval($spercent) > 0) $newcost = $avgcost + ($avgcost * ($spercent / 100));
			if(floatval($sdollar) > 0) $newcost += $sdollar;

			return $newcost;
		}
		return 0.00;
	}
    
	public function getCatTree($id)
	{
		$cattree = array();
		$query = $this->read_db->query("SELECT product_category_map.catid
										FROM product_category_map
										JOIN product_categories ON product_categories.catid = product_category_map.catid
										WHERE product_category_map.pid = '$id'
										AND product_categories.parentid = '0'");
		$catid = $query->row()->catid;
		if (intval($catid) == 0)
		{
			return;
		}

		$query = $this->read_db->query("SELECT product_categories.catid, product_categories.parentid, product_categories.catname
										FROM product_categories
										JOIN product_category_map ON product_categories.catid = product_category_map.catid
										WHERE product_category_map.pid = '$id'
										AND product_categories.catid = '$catid'
										AND product_categories.parentid = '0'");
		$xcatid = $query->row()->catid;
		$xparentid = $query->row()->parentid;
		$xcatname = $query->row()->catname;
		
		$cattree['catid'][] = $xcatid;
		$cattree['catname'][] = $xcatname;
		$cattree['parentid'][] = $xparentid;

		$continue = true;

		while($continue)
		{
			$query = $this->read_db->query("SELECT product_categories.catid, product_categories.parentid, product_categories.catname
											FROM product_categories
											JOIN product_category_map ON product_categories.catid = product_category_map.catid
											WHERE product_category_map.pid = '$id'
											AND product_categories.parentid = '$xcatid'"); 
			if ($query->num_rows() > 0)
			{
				$xcatid = $query->row()->catid;
				$xparentid = $query->row()->parentid;
				$xcatname = $query->row()->catname;
				
				$cattree['catid'][] = $xcatid;
				$cattree['catname'][] = $xcatname;
				$cattree['parentid'][] = $xparentid;
			}
			else
			{
				$continue = false;
			}
		}

		// now we gotta reverse it to be
		$newcattree = array();

		for($xx = count($cattree['catid']) - 1; $xx >= 0; $xx--)
		{
			$newcattree['catid'][] = $cattree['catid'][$xx];
			$newcattree['catname'][] = $cattree['catname'][$xx];
			$newcattree['parentid'][] = $cattree['parentid'][$xx];
		}
		return $newcattree;
	}    
	
	public function isAltSKU($id)
	{
		$alt_sku_flag = $this->getConfigValue('eFluent Settings', 'ALT_SKU_FLAG');
		$query = $this->read_db->query("SELECT text_short
										FROM custom_values
										WHERE cfid = '$alt_sku_flag'
										AND pid = '$id'");
		$alt_sku = $query->row()->text_short;
		return $alt_sku;
	}	
    
}