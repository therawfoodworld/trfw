<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
	Search Build library
 */
class Search_query
{
	private $searchFields = array();
	private $searchTables = array();
	private $searchConditions = array();
	private $searchClauses = array();
	private $searchCountTotalAlias = array();
	
    public function __construct()
    {
	}
    
    private function addSearchColumns() 
    {
    	$searchColumns = $this->searchColumns;
    	foreach ($searchColumns as $k => $field) 
    	{
    		switch ($field['id'])
    		{
    			case 'id':
//			        $this->searchFields[] = 'product_list.id';
			        break;
			    case 'sku':
			        $this->searchFields[] = 'product_list.sku';
			        break;
			    case 'manpart':
			        $this->searchFields[] = 'product_list.manpart';
			        break;
			    case 'man':
			        $this->searchFields[] = 'product_manufacturers.manname';
			        $this->buildQueryJoin('product_manufacturers');
			        break;
			    case 'descr':
			        $this->searchFields[] = 'REPLACE(product_list.descr,\'"\',\'\') AS descr';
			        break;
			    case 'upccode':
			        $this->searchFields[] = 'product_list.upccode';
			        break;
			    case 'roimast':
			        $this->searchFields[] = 'product_list.roimast';
			        break;
			    case 'cat':
			        $this->searchFields[] = 'product_search_extended.cattree';
			        $this->buildQueryJoin('product_search_extended');
			        break;
			    case 'weight':
			        $this->searchFields[] = 'product_list.shipweight';
			        break;
			    case 'platform':
			        $this->searchFields[] = 'product_list.platform';
			        break;
			    case 'hasimage':
			        $this->searchFields[] = 'product_list.platform';
			        break;
			    case 'visible':
			        $this->searchFields[] = 'product_list.visible';
			        break;
			    case 'dropship':
			        $this->searchFields[] = 'product_list.dropship';
			        break;
			    case 'refurb':
			        $this->searchFields[] = 'product_list.refurb';
			        break;
			    case 'oem':
			        $this->searchFields[] = 'product_list.oem';
			        break;
			    case 'returnable':
			        $this->searchFields[] = 'product_list.returnable';
			        break;
			    case 'demo':
			        $this->searchFields[] = 'product_list.demo';
			        break;
			    case 'hidestock':
			        $this->searchFields[] = 'product_list.hidestock';
			        break;
			    case 'hidestock':
			        $this->searchFields[] = 'product_list.hidestock';
			        break;
			    case 'usemap':
			        $this->searchFields[] = 'product_list.usemap';
			        break;
			    case 'price':
			        $this->searchFields[] = 'product_prices.price_level1';
			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'scost':
			        $this->searchFields[] = 'product_search_extended.scost';
			        $this->buildQueryJoin('product_search_extended');
			        break; 
			    case 'avgcost':
			        $this->searchFields[] = 'product_prices.avgcost';
			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'instock':
			    	$this->searchFields[] = 'product_prices.instock';
			    	$this->searchFields[] = 'product_search_extended.dsqty';
			    	$this->buildQueryJoin('product_prices');
			        $this->buildQueryJoin('product_search_extended');
			        break;
			    case 'columnperhouse':
//			        $this->searchFields[] = 'product_prices.instock';
//			        $this->buildQueryJoin('product_prices');
			        break; 
			    case 'columnperstock':
//			        $this->searchFields[] = 'product_prices.instock';
//			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'stockdetail':
			        $this->searchFields[] = 'product_prices.instock AS onhand';
			        $this->searchFields[] = 'product_prices.onhold';
			        $this->searchFields[] = 'product_prices.committed';
			        $this->searchFields[] = 'product_prices.onpo';
			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'webinventory':
			        $this->searchFields[] = 'product_list.alwaysinstock';
			        $this->searchFields[] = 'product_list.alwaysoutstock';
			        $this->searchFields[] = 'product_list.invisibleonstock';
//			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'assignedto':
			        $this->searchFields[] = 'product_search_extended.displayname';
			        $this->buildQueryJoin('product_search_extended');
			        break;
			    case 'dsinventory':
//			        $this->searchFields[] = 'product_search_extended.displayname';
//			        $this->buildQueryJoin('product_search_extended');
			        break;
			    case 'displayedon':
			        $this->searchFields[] = 'product_search_extended.sitelist';
			        $this->buildQueryJoin('product_search_extended');
			        break;
			    case 'namedpl':
			        $this->searchFields[] = 'product_prices.price_level2';
			        $this->searchFields[] = 'product_prices.price_level3';
			        $this->searchFields[] = 'product_prices.price_level4';
			        $this->searchFields[] = 'product_prices.price_level5';
			        $this->searchFields[] = 'product_prices.price_level6';
			        $this->searchFields[] = 'product_prices.price_level7';
			        $this->searchFields[] = 'product_prices.price_level8';
			        $this->searchFields[] = 'product_prices.price_level9';
			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'amazon_avil':
//			        $this->searchFields[] = 'product_prices.price_level2,product_prices.price_level3,product_prices.price_level4,product_prices.price_level5,product_prices.price_level6,product_prices.price_level7,product_prices.price_level8,product_prices.price_level9,';
//			        $this->buildQueryJoin('product_prices');
			        break;
			    case 'amazon_avil_flag':
			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'specificfeed':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'ebay':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'amazon':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'amazonca':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'amazonmax':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'buy':
//			        $this->searchFields[] = 'product_list.amazon_realtime';
			        break;
			    case 'asin':
			        $this->searchFields[] = 'product_list.asin';
			        break;  
			    case 'showimage':
//			        $this->searchFields[] = 'product_list.asin';
			        break;
//			    case 'eta':
//			        $this->searchFields[] = 'product_list.eta';
			        break;
			    case 'alt_total':
//			        $this->searchFields[] = 'product_list.alt_total';
			        break;   
			    default:
			    	break;    
			}
    	}
    }
	private function buildSearchConditions()
    {
    	$formData = $this->searchFilter;
    	// keywords
		if ($formData['search_keywords'] && !empty($formData['search_keywords']))
		{
			$keywords = str_replace('^','#',$formData['search_keywords']);
			if (strlen($keywords) >= 2)
			{
				$wordlist = explode(' ', $keywords);
				$count_wordlist = count($wordlist);
				$kwpart = '(';
				for($i = 0; $i < $count_wordlist; $i++)
				{
					$thisword = $wordlist[$i];
					$kwpart .= "product_list.manpart LIKE '%$thisword%'
								OR product_list.upccode LIKE '%$thisword%'
								OR product_list.descr LIKE '%$thisword%'
								OR product_list.id = '$thisword'
								OR product_list.asin LIKE '%$thisword%'
								OR product_list.sku LIKE '%$thisword%' OR ";
				}
				$this->searchConditions[] = substr($kwpart,0,-4) . ')';
			}
		}
    	
    	// pid
		if ($formData['search_id'] && !empty($formData['search_id']))
		{
			$this->searchConditions[] = "product_list.id = '".$formData['search_id']."'";
		}
				
		// ebay id
		if ($formData['search_ebayid'] && !empty($formData['search_ebayid']))
		{
			$this->buildQueryJoin('ebay_listings');
			$this->searchConditions[] = "ebay_listings.ebaylistingid = '".$formData['search_ebayid']."'";
		}
		
		// asin
		if ($formData['search_asin'] && !empty($formData['search_asin']))
		{
			$this->searchConditions[] = "product_list.asin = '".$formData['search_asin']."'";
		}
		
		// has asin
		if ($formData['search_hasasin'] && !empty($formData['search_hasasin']))
		{
			if ($formData['search_hasasin'] == 1)
			{
				$this->searchConditions[] = "product_list.use_asin = '1'";
			}
		}		

		// fba sku
		if ($formData['search_fbasku'] && !empty($formData['search_fbasku']))
		{
			$this->buildQueryJoin('fba_sku_map');
			$this->searchConditions[] = "fba_sku_map.fba_sku = '".mysql_real_escape_string(trim($formData['search_fbasku']))."'";
		}
		
		// sku
		if ($formData['search_sku'] && !empty($formData['search_sku']))
		{
			$this->searchConditions[] = "product_list.sku = '".mysql_real_escape_string(trim($formData['search_sku']))."'";
		}

		// dist sku
		if ($formData['search_distsku'] && !empty($formData['search_distsku']))
		{
			$this->buildQueryJoin('product_sourcing');
			$this->searchConditions[] = "product_sourcing.distsku = '".mysql_real_escape_string(trim($formData['search_distsku']))."'";
		}
		
		// category
		$where_category = '';
		if ($formData['search_cat0'] && !empty($formData['search_cat0']))
		{
			$this->buildQueryJoin('product_category_map');
			$where_category = "product_category_map.catid = '".$formData['search_cat0']."'";
		}
		if ($formData['search_cat1'] && !empty($formData['search_cat1']) && 
				$formData['search_cat0'] && !empty($formData['search_cat0']))
		{
			$where_category = "product_category_map.catid = '".$formData['search_cat1']."'";
		}
		if ($formData['search_cat2'] && !empty($formData['search_cat2']) && 
				$formData['search_cat0'] && !empty($formData['search_cat0']) &&
				$formData['search_cat1'] && !empty($formData['search_cat1']))
		{
			$where_category = " product_category_map.catid = '".$formData['search_cat2']."'";
		}
		if ($formData['search_cat3'] && !empty($formData['search_cat3']) &&
				$formData['search_cat0'] && !empty($formData['search_cat0']) && 
				$formData['search_cat1'] && !empty($formData['search_cat1']) &&
				$formData['search_cat2'] && !empty($formData['search_cat2']))
		{
			$where_category = " product_category_map.catid = '".$formData['search_cat3']."'";
		}
		if ($formData['search_cat4'] && !empty($formData['search_cat4']) &&
				$formData['search_cat0'] && !empty($formData['search_cat0']) &&
				$formData['search_cat1'] && !empty($formData['search_cat1']) && 
				$formData['search_cat2'] && !empty($formData['search_cat2']) &&
				$formData['search_cat3'] && !empty($formData['search_cat3']))
		{
			$where_category = " product_category_map.catid = '".$formData['search_cat4']."'";
		}
		$this->searchConditions[] = $where_category;		

		// brand
		if ($formData['search_brand'] && !empty($formData['search_brand']))
		{
			$this->buildQueryJoin('product_manufacturers');
			$this->searchConditions[] = "product_list.manid = '".$formData['search_brand']."'";
		}

		// lists
		if ($formData['search_list'] && !empty($formData['search_list']))
		{
			$this->buildQueryJoin('list_products');
			$list_array = explode(',',$formData['search_list']);
			if (count($list_array) == 1)
			{
				if (!empty($list_array['0']) && $list_array['0'] > 0)
				{
					$this->searchConditions[] = "list_products.lid = '".$list_array['0']."'";
				}
			}
			else 
			{
				$list_tmp = '(';
				foreach ($list_array AS $list_id)
				{
					if (!empty($list_id) && $list_id > 0)
					{
						$list_tmp .= "list_products.lid = '$list_id' OR ";
					}	
				}
				$this->searchConditions[] = substr($list_tmp,0,-4) . ')';
			}
		}
			
		// exclude list
		if ($formData['search_not_list'] && !empty($formData['search_not_list']))
		{
			$this->buildQueryJoin('list_products');
			$list_array = explode(',',$formData['search_not_list']);
			if (count($list_array) == 1)
			{
				if (!empty($list_array['0']) && $list_array['0'] > 0)
				{
					$this->searchConditions[] = "product_list.id NOT IN (SELECT pid FROM list_products WHERE lid = '".$list_array['0']."')";
				}
			}
			else 
			{
				foreach ($list_array AS $list_id)
				{
					$this->searchConditions[] = "product_list.id NOT IN (SELECT pid FROM list_products WHERE lid = '$list_id')";
				}		
			}
		}
		
		// manpart
		if ($formData['search_manpart'] && !empty($formData['search_manpart']))
		{
			$this->searchConditions[] = "product_list.manpart = '".mysql_real_escape_string(trim($formData['search_manpart']))."'";
		}
			
		// condition
		if ($formData['search_condition'] && !empty($formData['search_condition']))
		{
			$condition = $formData['search_condition'];
			if ($condition > 0)
			{
				if ($condition == 1)
				{
					$this->searchConditions[] = "product_list.oem = '1'";
				}
				elseif ($condition == 2)
				{
					$this->searchConditions[] = "product_list.demo = '1'";
				}			
				elseif ($condition == 3)
				{
					$this->searchConditions[] = "product_list.refurb = '1'";
				}			
				elseif ($condition == 4)
				{
					$this->searchConditions[] = "product_list.oem = '0' AND product_list.demo = '0' AND product_list.refurb = '0'";
				}			
			}
		}
		
		// upc
		if ($formData['search_upc'] && !empty($formData['search_upc']))
		{
			$this->searchConditions[] = "product_list.upccode = '".mysql_real_escape_string(trim($formData['search_upc']))."'";
		}
    	
    	// no upc
		if ($formData['search_no_upc'] && !empty($formData['search_no_upc']))
		{
    		$no_upc = $formData['search_no_upc'];
    		if ($no_upc == 1)
    		{
    			$this->searchConditions[] = "product_list.upccode = ''";
    		} 
		}
    	
    	// status
		if ($formData['search_status'] && !empty($formData['search_status']))
		{
    		$status =  $formData['search_status'];
    		if ($status > 0)
    		{
				$status--;
				if ($status == 0) // active can be 1 or NULL
				{
					$this->searchConditions[] = "(product_list.status = '$status' OR product_list.status is NULL)";
				}
				else 
				{
					$this->searchConditions[] = "product_list.status = '$status'";				
				}
    		}
		}
    	
		// min price
		if ($formData['search_minprice'] && !empty($formData['search_minprice']))
		{
			$min_price =  $formData['search_minprice'];
			if ($min_price > 0)
			{
				$this->buildQueryJoin('product_prices');
				$this->searchConditions[] = "product_prices.price_level1 >= '$minprice'";
			}
		}
    	
		// max price
		if ($formData['search_maxprice'] && !empty($formData['search_maxprice']))
		{
			$max_price =  $formData['search_maxprice'];
			if ($max_price > 0)
			{
				$this->buildQueryJoin('product_prices');
				$this->searchConditions[] = "product_prices.price_level1 <= '$max_price'";
			}
		}
    
		// weight
		if ($formData['search_weight'] && !empty($formData['search_weight']))
		{
			if ($formData['search_weight'] == 1)
			{
				$this->searchConditions[] = "product_list.shipweight <= '".$formData['search_weight']."'";
			}
			elseif ($formData['search_weight'] == 2)
			{
				$this->searchConditions[] = "product_list.shipweight >= '".$formData['search_weight']."'";
			}
		}

		// weight range
		if ($formData['search_weight_range_min'] && !empty($formData['search_weight_range_min']) &&
			$formData['search_weight_range_max'] && !empty($formData['search_weight_range_max']))
		{
			$weight_range_min =  $formData['search_weight_range_min'];
			$weight_range_max =  $formData['search_weight_range_max'];
			
			if ($weight_range_min > 0)
			{
				$this->searchConditions[] = "product_list.shipweight >= '$weight_range_min'";
            }
            
            if ($weight_range_max > 0)
            {
				$this->searchConditions[] = "product_list.shipweight <= '$weight_range_min'";
			}
		}
		
		// visible
		if ($formData['search_vizible'] && !empty($formData['search_vizible']))
		{
			$visible =  $formData['search_vizible'];
			if ($visible == -1)
			{
			}
			elseif ($visible == 2)
			{
				$this->searchConditions[] = "product_list.visible = '0'";
			}
			elseif ($visible == 1)
			{
				$this->searchConditions[] = "product_list.visible = '1'";
			}
		}

		// inventory	
		if ($formData['search_inventory'] && !empty($formData['search_inventory']))
		{
			$inventory =  $formData['search_inventory'];
			if ($inventory > 0)
			{
				/*
				-1 = ALL
				1 = In-Stock (exc DS)
				2 = Out of Stock
				3 = In-Stock (inc DS)
				*/
				if($inventory == 1) // 1 = In-Stock (exc DS)
				{
					$this->searchConditions[] = "product_prices.instock > '0'";
				}
				else if($inventory == 2) // 2 = Out of Stock
				{
					$this->searchConditions[] = "product_prices.instock <= '0'";
				}
				else if($inventory == 3) // 3 = In-Stock (inc DS)
				{
					$this->buildQueryJoin('product_search_extended');
					$this->searchConditions[] = "product_search_extended.dsqty > '0'";
				}
				else if($inventory == 4) // 3 = Out of Stock ( DS)
				{
					$this->buildQueryJoin('product_search_extended');
					$this->searchConditions[] = "product_search_extended.dsqty = '0'";
				}
			}
		}		
		
		// include kits
		if ($formData['search_includekits'] && !empty($formData['search_includekits']))
		{
			$includekits =  $formData['search_includekits'];
			if ($includekits == 2) // exclude kits
			{
				$this->searchConditions[] = "product_list.kit = '0'";
			}
			elseif ($includekits == 3) // only show kits
			{
				$this->searchConditions[] = "product_list.kit = '1'";
			}
		}
		
		// kit sku		
		if ($formData['search_component'] && !empty($formData['search_component']))
		{
			$this->buildQueryJoin('product_kitskus');
			$this->searchConditions[] = "product_kitskus.kid = '".$formData['search_component']."'"; 
		}
		
		// accessory sku
		if ($formData['search_accessory'] && !empty($formData['search_accessory']))
		{
			$this->buildQueryJoin('product_accessories');
			$this->searchConditions[] = "product_accessories.accid = '".$formData['search_accessory']."'";
		}
		
		// assigned to
		if ($formData['search_assignedto'] && !empty($formData['search_assignedto']))
		{
			$assignedto =  $formData['search_assignedto'];
			if ($assignedto > 0)
			{
				$this->buildQueryJoin('sku_assignment');
				$this->searchConditions[] = "sku_assignment.userid = '$assignedto'";
			}
			elseif ($assignedto == 'UN')
			{
				$this->searchConditions[] = "product_list.id NOT IN (SELECT pid FROM sku_assignment)";
			}
		}
		
		// alt sku
		if ($formData['search_alt_sku'] && !empty($formData['search_alt_sku']))
		{
//			$alt_sku_flag = $this->efluent->getConfigValue('eFluent Settings', 'ALT_SKU_FLAG');
//			$alt_sku_parent = $this->efluent->getConfigValue('eFluent Settings', 'ALT_SKU_PARENT');
//			$alt_sku_parentid = $this->efluent->getConfigValue('eFluent Settings', 'ALT_SKU_PARENTID');
			
			$alt_sku =  $formData['search_alt_sku'];
			if ($alt_sku == '1')
			{
//				$this->searchConditions[] = "product_list.id NOT IN (SELECT pid FROM custom_values WHERE cfid = '$alt_sku_flag' AND text_short = '1')";		
			}
		}		
		
		// scost to price gap
		if ($formData['search_mingap'] && !empty($formData['search_mingap']))
		{
		}		
		
		// WH in stock
		if (isset($formData['search_whid']) && $formData['search_whid'] != '')
		{
			$this->buildQueryJoin('product_inventory');
			$this->searchFields[] = 'SUM(product_inventory.inv_available) AS whstock';
			$this->searchConditions[] = "product_inventory.whid = '".$formData['search_whid']."'";
			$this->searchCountTotalAlias[] = 'SUM(product_inventory.inv_available) AS whstock';
			if ($formData['search_whmin'] && !empty($formData['search_whmin']))
			{
				if ($formData['search_whtype'] && $formData['search_whtype'] == '1')
				{
					$this->searchClauses['having'][] = "whstock > '".$formData['search_whmin']."'";
				}
				elseif ($formData['search_whtype'] && $formData['search_whtype'] == '2')
				{
					$this->searchClauses['having'][] = "whstock < '".$formData['search_whmin']."'";
				}
				elseif ($formData['search_whtype'] && $formData['search_whtype'] == '3')
				{
					$this->searchClauses['having'][] = "whstock = '".$formData['search_whmin']."'";
				}
			}

			if ($formData['search_whminqty'] == '1')
			{
				$this->searchConditions[] = "product_list.minqty > COALESCE((SELECT SUM(product_inventory.inv_available) FROM product_inventory WHERE product_list.id = product_inventory.id AND product_inventory.whid = '".$formData['search_whid']."'),0)";
			}
			elseif ($formData['search_whminqty'] == '2')
			{
				$this->searchConditions[] = "COALESCE((SELECT SUM(product_inventory.inv_available) FROM product_inventory WHERE product_list.id = product_inventory.id AND product_inventory.whid = '".$formData['search_whid']."'),0) = '0'";
			}
		}
		
		// sourced from
		if ($formData['search_sourcing'] && !empty($formData['search_sourcing']))
		{
			$sourcing =  $formData['search_sourcing'];
			$this->buildQueryJoin('product_sourcing');	

			if ($sourcing > 0)
			{
				$this->searchConditions[] = "product_sourcing.distid = '$sourcing'";
				
				if ($formData['search_sourcing_qty'] && !empty($formData['search_sourcing_qty']))
				{
					$this->searchConditions[] = "product_sourcing.qty >= '".$formData['search_sourcing_qty']."'";
				}
				
				if($formData['search_distsku'] && !empty($formData['search_distsku']))
				{
					$this->searchConditions[] = "product_sourcing.distsku = '".$formData['search_distsku']."'";
				}
			}
			else
			{
				if($formData['search_distsku'] && !empty($formData['search_distsku']))
				{
					$this->searchConditions[] = "product_sourcing.distsku = '".$formData['search_distsku']."'";
				}
			}
		}

		// no image	
		if ($formData['search_noimage'] && !empty($formData['search_noimage']))
		{
			$noimage =  $formData['search_noimage'];
			if ($noimage == 1)
			{
				$this->searchConditions[] = "product_list.id NOT IN (SELECT DISTINCT(id) FROM product_images)";
			}
		}

		// super pricer
		if ($formData['search_superpricer'] && !empty($formData['search_superpricer']))
		{
			$superpricer =  $formData['search_superpricer'];
			if($superpricer > 0)
			{
				$this->buildQueryJoin('superpricer');
				$this->searchConditions[] = "superpricer.sp_managed = '1'";
			}
		}		
		
		// display on
		if (!empty($formData))
		{
			foreach ($formData AS $k => $v)
			{
				if (strpos($k,'search_displayon_') !== false)
				{
					if ($v == 1)
					{
						$this->buildQueryJoin('site_skus');
						$site_id = str_replace('search_displayon_','',$k);
						$this->searchConditions[] = "product_list.id IN (SELECT pid FROM site_skus WHERE divid = '$site_id')";
					}
				}
			}
		}
    }
    
	private function buildQueryJoin($table)
	{
		if (!array_key_exists($table,$this->searchTables))
		{
			switch ($table)
			{
			    case 'product_accessories':
			        $this->searchTables['product_accessories'] = 'INNER JOIN product_accessories ON product_list.id = product_accessories.id';
			        break;
			    case 'product_kitskus':
			        $this->searchTables['product_kitskus'] = 'INNER JOIN product_kitskus ON product_list.id = product_kitskus.pid';
			        break;
		        case 'product_prices':
			        $this->searchTables['product_prices'] = 'INNER JOIN product_prices ON product_list.id = product_prices.id';
			        break;
		        case 'sku_assignment':
			        $this->searchTables['sku_assignment'] = 'INNER JOIN sku_assignment ON product_list.id = sku_assignment.pid';
			        break;
			    case 'product_category_map':
			        $this->searchTables['product_category_map'] = 'INNER JOIN product_category_map ON product_list.id = product_category_map.pid';
			        break;
			    case 'product_sourcing':
			        $this->searchTables['product_sourcing'] = 'INNER JOIN product_sourcing ON product_list.id = product_sourcing.pid';
			        break;
			    case 'ebay_listings':
			        $this->searchTables['ebay_listings'] = 'INNER JOIN ebay_listings ON product_list.id = ebay_listings.pid';
			        break;
			    case 'site_skus':
			        $this->searchTables['site_skus'] = 'INNER JOIN site_skus ON product_list.id = site_skus.pid';
			        break;
			    case 'list_products':
			        $this->searchTables['list_products'] = 'INNER JOIN list_products ON product_list.id = list_products.pid';
			        break;
			    case 'superpricer':
			        $this->searchTables['superpricer'] = 'INNER JOIN superpricer ON product_list.id = superpricer.pid';
			        break;
			    case 'fba_sku_map':
			        $this->searchTables['fba_sku_map'] = 'INNER JOIN fba_sku_map ON product_list.id = fba_sku_map.pid';
			        break;
			    case 'custom_values':
			        $this->searchTables['custom_values'] = 'INNER JOIN custom_values ON product_list.id = custom_values.pid';
			        break;
			    case 'product_inventory':
			        $this->searchTables['product_inventory'] = 'INNER JOIN product_inventory ON product_list.id = product_inventory.id';
			        break;
			    case 'product_static_specs':
			        $this->searchTables['sku_assignment'] = 'INNER JOIN sku_assignment ON product_list.id = product_static_specs.id';
			        break;
			    case 'product_manufacturers':
			        $this->searchTables['product_manufacturers'] = 'INNER JOIN product_manufacturers ON product_list.manid = product_manufacturers.manid';
			        break;
			    case 'product_search_extended':
			        $this->searchTables['product_search_extended'] = 'INNER JOIN product_search_extended ON product_list.id = product_search_extended.pid';
			        break;
			    default:
			    	break;    
			}
		}
    }
    public function createQuery($searchFiters,$searchColumns)
    {
    	
    	$this->searchFilter = $searchFiters;
		$this->searchColumns = $searchColumns;
    	$this->addSearchColumns();
    	$this->buildSearchConditions();

    	$tables = implode(" ",$this->searchTables);
    	$fields = implode(",",$this->searchFields);
   	
    	$where_conditions = '';
		$search_conditions = $this->searchConditions;
		if (count($search_conditions) > 0)
		{
			// clear empty values
			foreach ($search_conditions AS $k => $v)
			{
				if (empty($v))
				{
					unset($search_conditions[$k]);
				}
			}

			if (count($search_conditions) > 0)
			{
				$conditions = implode(' AND ',$search_conditions);
				$where_conditions = ' WHERE '.$conditions;
			}
		}
		    	
    	$having_clauses = '';
		$search_clauses = $this->searchClauses['having'];
		if (count($search_clauses) > 0)
		{
			// clear empty values
			foreach ($search_clauses AS $k => $v)
			{
				if (empty($v))
				{
					unset($search_clauses[$k]);
				}
			}

			if (count($search_clauses) > 0)
			{
				$clauses = implode(' AND ',$search_clauses);
				$having_clauses = ' HAVING '.$clauses;
			}
		}	

		if (count($this->searchCountTotalAlias) > 0)
		{
			$fields_count_total = ', ' . implode(',',$this->searchCountTotalAlias);
		}
		
    	$query = "SELECT product_list.id, $fields
    			  FROM product_list $tables
    			  $where_conditions
    			  GROUP BY product_list.id
    			  $having_clauses";
    	$query_count = "SELECT COUNT(*) AS nr
    					FROM (SELECT product_list.id $fields_count_total FROM product_list $tables $where_conditions GROUP BY product_list.id $having_clauses ORDER BY NULL) a
    					LIMIT 1";
    	return array('query' => $query, 'count_query' => $query_count);
    }
}
