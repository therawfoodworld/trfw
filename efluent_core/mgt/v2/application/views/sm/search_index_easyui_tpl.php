<div id="container">
	
	<div id="body">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
	<tr>
		<td colspan="2">
			<h1>SKU Search - eFluent v.2</h1>
		</td>
	</tr>
	<tr>
		<td class="form">
			<form name="skusearch">
	        <table>
			<tbody>
			<?php
			foreach ($search_filters AS $k => $v)
			{
				$colname = $v['id'];
				if ($colname == 'keywords')
				{
					?>
					<tr>
						<td class="g1">Keywords:</td>
						<td><input type="text" id="search_keywords" name="search_keywords" value="" style="width:75px" class="search_keywords formBox"></td>
					</tr>
					<?
				}
				elseif ($colname == 'id')
				{
					?>			
					<tr>
		                <td class="g1">ID:</td>
		                <td><input type="text" name="search_id" id="search_id" value="" class="search_id formBox"></td>
		    	    </tr>
		    	    <?php
				}
				elseif ($colname == 'ebay_id')
				{
					?>
					<tr>
						<td class="g1">Ebay ID:</td>
						<td><input type="text" id="search_ebayid" name="search_ebayid" style="width:75px" value="" class="search_ebayid formBox"></td>
					</tr>
					<?
				}				
				elseif ($colname == 'asin')
				{
					?>
					<tr>
						<td class="g1">ASIN:</td>
						<td><input type="text" id="search_asin" name="search_asin" style="width:75px" value="" class="search_asin formBox">
						<input type="checkbox" name="search_hasasin" id="search_hasasin" class="search_hasasin" value="1">Has ASIN</td>
					</tr>
					<?
				}				
				elseif ($colname == 'fba_sku')
				{
					?>
					<tr>
						<td class="g1">FBA SKU:</td>
						<td><input type="text" id="search_fbasku" name="search_fbasku" style="width:75px" value="" class="search_fbasku formBox"></td>
					</tr>
					<?
				}
				elseif ($colname == 'sku')
				{
					?>
					<tr>
		                <td class="g1">SKU:</td>
		                <td><input type="text" name="search_sku" id="search_sku" value="" class="search_sku formBox"></td>
		        	</tr>
		        	<?php
				}
				elseif ($colname == 'dist_sku')
				{
					?>
					<tr>
						<td class="g1">DIST SKU:</td>
						<td><input type="text" id="search_distsku" name="search_distsku" style="width:75px" value="" class="search_distsku formBox"></td>
					</tr>
					<?
				}
				elseif ($colname == 'category')
				{
					?>
					<tr>
						<td class="g1">Category:</td>
						<td>
							<select id="search_cat0" name="search_cat0" class="search_cat0 formSelect" onchange="get_categories(1);">
		                	<option value="0">------- SELECT -------</option>
							<?php
							foreach ($categories AS $category)
							{
								?>
								<option value="<?php echo $category->catid; ?>"><?php echo $category->catname; ?></option>
								<?php
							}
							?>
							</select>		
							<select name="search_cat1" id="search_cat1" class="search_cat1 formSelect display_none" onchange="get_categories(2);"></select>
							<select name="search_cat2" id="search_cat2" class="search_cat2 formSelect display_none" onchange="get_categories(3);"></select>
							<select name="search_cat3" id="search_cat3" class="search_cat3 formSelect display_none" onchange="get_categories(4);"></select>
							<select name="search_cat4" id="search_cat4" class="search_cat4 formSelect display_none" onchange="get_categories(5);"></select>
							<select name="search_cat5" id="search_cat5" class="search_cat5 formSelect display_none" onchange="get_categories(6);"></select>
						</td>
					</tr>	
					<?				
				}				
				elseif ($colname == 'brand')
				{
					?>
		   			<tr>
		                <td class="g1">Brand:</td>
		                <td><select name="search_brand" id="search_brand" class="search_brand formSelect">
		                	<option value="0">------- SELECT -------</option>
		                    <?php
							foreach ($brands AS $brand)
							{
								?>
								<option value="<?php echo $brand->manid; ?>"><?php echo $brand->manname; ?></option>
								<?php
							}
							?>
		                	</select></td>
	                </tr>
	                <?php
				}
				elseif ($colname == 'list')
				{
					?>
	                <tr>
	                	<td class="g1">Groups:</td>
	                	<td><select name="search_group" id="search_group" class="search_group formSelect">
		                	<option value="0">------- SELECT -------</option>
	                        <?php
							foreach ($groups AS $group)
							{
								?>
								<option value="<?php echo $group->id; ?>"><?php echo $group->group_name; ?></option>
								<?php
							}
							?>
	                        </select></td>
	                </tr>
			        <tr>
	        	        <td class="g1">List:</td>
	        	        <td>
	                        <select name="search_list" id="search_list" class="search_list formSelect" multiple="multiple">
		                	<option value="0">------- SELECT -------</option>
	                        <?php
							foreach ($lists AS $list)
							{
								?>
								<option value="<?php echo $list->lid; ?>"><?php echo $list->list_title; ?></option>
								<?php
							}
							?>
	                        </select></td>
			        </tr>
			        <?php
				}
				elseif ($colname == 'exc_list')
				{				
					?>
					<tr>
						<td class="g1">EXC if on list:</td>
						<td>
							<select name="search_not_list" id="search_not_list" multiple class="search_not_list formSelect">
							<option value="0">------- SELECT -------</option>
							<?
							foreach ($lists AS $list)
							{
								if(strlen($list->list_title) > 25)
								{
									$list->list_title = substr($list->list_title, 0, 18) . '...';
								}
								?>
								<option value="<?php echo $list->lid; ?>"><?php echo $list->list_title; ?></option>
								<?php
							}
							?>
							</select>	
						</td>
					</tr>
					<?				
				}				
				elseif ($colname == 'man_part')
				{
					?>
	    		    <tr>
	            	    <td class="g1">Man Part:</td>
	            	    <td><input type="text" name="search_manpart" id="search_manpart" value="" class="search_manpart formBox"></td>
	        		</tr>
	        		<?php
				}
				elseif ($colname == 'condition')
				{
					?>
					<tr>
						<td class="g1">Condition:</td>
						<td>
							<select id="search_condition" name="search_condition" class="search_condition formSelect">
								<option value="0">------- SELECT -------</option>
								<option value="3">Refurb</option>
								<option value="2">Demo</option>
								<option value="1">OEM</option>
								<option value="4">New</option>
							</select>
						</td>
					</tr>
					<?
				}
				elseif ($colname == 'upc')
				{
					?>
			        <tr>
	        	        <td class="g1">UPC:</td>
	        	        <td>
	                        <input type="text" name="search_upc" id="search_upc" value="" class="search_upc formBox">
	        	        </td>
	        		</tr>
					<?php
				}
				elseif ($colname == 'no_upc')
				{
					?>
					<tr>
						<td class="g1">No UPC:</td>
						<td><input type="checkbox" id="search_no_upc" name="search_no_upc" value="1" class="search_no_upc"></td>
					</tr>
					<?
				}		
				elseif ($colname == 'status')
				{
					?>
					<tr>
						<td class="g1">Status:</td>
						<td>
							<select id="search_status" name="search_status" class="search_status formSelect">
								<option value="0">------- SELECT -------</option>
								<option value="1">Active</option>
								<option value="2">Allocated</option>
								<option value="3">End of Life</option>
								<option value="4">Discontinued</option>
							</select>
						</td>
					</tr>	
					<?
				}
				elseif ($colname == 'min_price')
				{
					?>
					<tr>
						<td class="g1">Min Price:</td>
						<td><input type="text" id="search_minprice" name="search_minprice" value="" style="width:50px" class="search_minprice formBox"></td>
					</tr>
				<?
				}				
				elseif ($colname == 'max_price')
				{
					?>
					<tr>
						<td class="g1">Max Price:</td>
						<td><input type="text" id="search_maxprice" name="search_maxprice" value="" style="width:50px" class="search_maxprice formBox"></td>
					</tr>
				<?
				}
				elseif ($colname == 'weight')
				{
					?>
					<tr>
						<td class="g1">Weight:</td>
						<td>
							<select name="search_weight" id="search_weight" class="search_weight formSelect">
								<option value="">-----</option>
								<option value="1">--<--</option>
								<option value="2">-->--</option>
							</select>
							<input type="text" name="search_weight_value" id="search_weight_value" value="" style="width:50px" class="search_weight_value formBox">
						</td>
					</tr>	
					<?
				}				
				elseif ($colname == 'weight_range')
				{
					?>
					 <tr>
						<td class="g1">Weight Range:</td>
                        <td><input type="text" name="search_weight_range_min" id="search_weight_range_min" value="" style="width:50px" class="search_weight_range_min formBox">
                        	&nbsp;&nbsp;-&nbsp;&nbsp;
                        	<input type="text" name="search_weight_range_max" id="search_weight_range_max" value="" style="width:50px" class="search_weight_range_max formBox"/>
                        </td>
					</tr>	
					<?
				}
				elseif ($colname == 'visible')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Visible:</td>
						<td valign=top>
							<select name="search_vizible" id="search_vizible" class="search_vizible formSelect">
								<option value="-1">------- ALL -------</option>
								<option value="1">Visible Only</option>
								<option value="2">Hidden Only</option>
							</select>						
						</td>
					</tr>
					<?
				}			
				elseif ($colname == 'inventory')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Inventory:</td>
						<td valign=top>
							<select name="search_inventory" id="search_inventory" class="search_inventory formSelect">
								<option value="-1">------- ALL -------</option>
								<option value="1">In-Stock (exc DS)</option>
								<option value="3">In-Stock (inc DS)</option>
								<option value="2">Out of Stock</option>
								<option value="4">Out of Stock (DS)</option>
							</select>						
						</td>
					</tr>
					<?
				}	
				elseif ($colname == 'include')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Include:</td>
						<td valign=top>
							<select name="search_includekits" id="search_includekits" class="search_includekits formSelect">
								<option value="1">All SKUs</option>
								<option value="2">Exclude Kits</option>	
								<option value="3">Only Show Kits</option>
								<option value="4">Only Show AutoPack</option>
							</select>						
						</td>
					</tr>	
					<?
				}				
				elseif ($colname == 'kit_sku')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Kit SKU (PID):</td>
						<td valign=top><input type="text" name="search_component" id="search_component" value="" style="width:75px" class="search_component formBox"></td>
					</tr>
					<?
				}				
				elseif ($colname == 'acc_sku')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Acc. SKU (PID):</td>
						<td valign=top><input type="text" name="search_accessory" id="search_accessory" value="" style="width:75px" class="search_accessory formBox"></td>
					</tr>
					<?
				}				
				elseif ($colname == 'assigned_to')
				{
					?>
					<tr>
						<td class="g1"><B>Assigned To:</B></td>
						<td>
							<select name="search_assignedto" id="search_assignedto" class="search_assignedto formSelect">
							<option value="-1">------- SELECT -------</option>
							<option value="UN">Unassigned</option>
							<?
							foreach ($assigned_to AS $single_assigned)
							{
								?>
								<option value="<?php echo $single_assigned->id; ?>"><?php echo $single_assigned->displayname; ?></option>
								<?php
							}
							?>
							</select>
						</td>	
					</tr>
					<?				
				}				
				elseif ($colname == 'alt_sku')
				{
					// TODO - display only if 'ecom' OR 'ajritchie' database
					?>
					<tr>
						<td class="g1"><B>ALT Sku:</B></td>
						<td>
							<select name="search_alt_sku" id="search_alt_sku" class="search_alt_sku formSelect">
							<option value="1">Dont show alts</option>
							<option value="2">Show all</option>
							<option value="3">Show children</option>
						</td>	
					</tr>		
					<?
				}
				elseif ($colname == 'scost_to_price_gap')
				{
					// TODO - display only if SESSION USERTYPE is MANAGER
					?>
					<tr>
						<td class="g1">SCost to Price Gap:</td>
						<td><input type="text" name="search_mingap" id="search_mingap" value="" style="width:50px;text-align:right;" class="search_mingap formBox"> or ></td>
					</tr>
					<?
				}
				elseif ($colname == 'wh_instock')
				{
					// TODO - display only if SESSION USERTYPE is MANAGER
					?>
					<tr>
						<td class="g1">WH Instock:</td>
						<td>
							<select name="search_whid" id="search_whid" class="search_whid formSelect">
							<option value="">------- SELECT -------</option>
							<?
							foreach ($whid AS $single_whid)
							{
								?>
								<option value="<?php echo $single_whid->whid; ?>"><?php echo $single_whid->whname; ?></option>
								<?php
							}
							?>
							</select>
							<select name="search_whtype" id="search_whtype" class="search_whtype formSelect">
								<option value="">---</option>
								<option value="1"> > </option>
                                <option value="2"> < </option>
                                <option value="3"> = </option>
                            </select>
                            <input type="text" name="search_whmin" id="search_whmin" value="" style="width:25px" class="search_whmin formBox">						
						</td>
					</tr>							
					<?
				}
				elseif ($colname == 'whminqty')
				{
					// TODO - display only if SESSION USERTYPE is MANAGER
					?>
					<tr>
						<td class="g1">Min Qty:</td>
                        <td>
                        	<select name="search_whminqty" id="search_whminqty" class="search_whminqty formSelect">
                        		<option value="">------- SELECT -------</option>
                        		<option value="1">Items below Min QTY</option>
                        		<option value="2">Items w 0 Qty</option>
                        	</select>
                        </td>
					</tr>
					<?
				}
				elseif ($colname == 'sourced_from')
				{
					?>
					<tr>
						<td class="g1"><B>Sourced From:</B></td>
						<td>
							<select name="search_sourcing" id="search_sourcing" class="search_sourcing formSelect">
							<option value="-1">------- SELECT -------</option>
							<?
							foreach ($sourcings AS $single_source)
							{
								if(strlen($single_source->vendorname) > 25)
								{
									$single_source->vendorname = substr($single_source->vendorname, 0, 22) . '...';
								}
								?>
								<option value="<?php echo $single_source->vendorid; ?>"><?php echo $single_source->vendorname; ?></option>
								<?php
							}
							?>
							</select>
						</td>	
					</tr>	
					<?
				}			
				elseif ($colname == 'min_sourced_qty')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Min Sourced QTY:</td>
						<td valign=top><input type="text" name="search_sourcing_qty" id="search_sourcing_qty" value="" style="width:75px" class="search_sourcing_qty formBox"></td>
					</tr>
					<?
				}			
				elseif ($colname == 'no_image')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;"></td>
						<td valign=top><input type="checkbox" name="search_noimage" id="search_noimage" class="search_noimage" value="1">No Image<BR></td>
					</tr>
					<?
				}				
				elseif ($colname == 'superpricer')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;"></td>
						<td valign=top><input type="checkbox" name="search_superpricer" id="search_superpricer" value="1" class="search_superpricer">SuperPricer Managed<BR></td>
					</tr>
					<?
				}				
				elseif ($colname == 'display_on')
				{
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Display On:</td>
						<td valign=top>
							<?
							foreach ($display_on AS $single_display_on)
							{
								?>
								<input class="search_displayon" type="checkbox" name="search_displayon_<?php echo $single_display_on->divid; ?>" value="1" id="search_displayon_<?php echo $single_display_on->divid; ?>"><?php echo $single_display_on->sitename; ?><BR>
								<?
							}
							?>	
						</td>
					</tr>
					<?
				}				
				elseif ($colname == 'return')
				{
					/*
					?>
					<tr>
						<td class="g1"><div style="padding-top:2px;">Return:</td>
						<td valign=top>
							<select name="search_num2return" id="search_num2return" class="search_num2return formSelect">
								<option value="50">50 items (default)</option>
								<option value="100">100 items</option>
								<option value="1000">1000 items</option>
								<option value="9999">2500 items</option>
								<option value="9999">9999 items</option>
							</select>						
						</td>
					</tr>	
					<?
					*/
				}			
			}
			?>
		    <tr>
                <td colspan="2"><div align="center"><br /><input type="button" name="search" value="Search" class="button_search"></div></td>
	        </tr>									
	        <tr>
				<td colspan="2"><div align="center">
								<div id="num_rows"></div><br />
								<div id="skuresults" style="display:none;"><img src="<?php echo $base_url; ?>images/loading.gif" /></div></div>
				</td>
			</tr>
			</tbody>
			</table>
			</form>		
		</td><!-- .form -->
		<td class="search_result">
			<table id="dg" class="easyui-datagrid" style="width:auto;height:auto;">  
			<thead>  
	        <tr>
	        	<?php
	        	$count_columns = count($search_columns);
	        	foreach ($search_columns AS $column)
	        	{
	        		?>
		            <th field="<?php echo $column['id']; ?>"><?php echo $column['title']; ?></th>  
	        		<?php
	        	}  
	        	?>
	        </tr>                            
		    </thead>                             
		    <tbody> 
			<tr>
	            <td colspan="<?php echo $count_columns; ?>">&nbsp;</td>  
			</tr>
	    	</tbody>                             
			</table> 		
 		</td><!-- .search_result -->
	</tr>
	</tbody>
	</table>
	</div>
</div>