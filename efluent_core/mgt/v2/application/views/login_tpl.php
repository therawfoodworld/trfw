<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>eFluent v.2 - Login</title>
	<base href="<?php echo $base_url; ?>">

	<!-- load css files -->
	<?php
	if (isset($css))
	{
		foreach (array_unique($css) AS $css_file)
		{
			?>
			<link rel="stylesheet" href="<?php echo $base_url . $css_file; ?>" type="text/css" media="all" />
			<?php
		}
	}
	?>
	<!-- end of load css files -->
	
	<!-- load js files -->
	<?php
	if (isset($js))
	{
		foreach (array_unique($js) AS $js_file)
		{
			?>
			<script src="<?php echo $base_url . $js_file; ?>" type="text/javascript"></script>
    		<?php
		}
	}
	?>
	<!-- end of load js files -->

	<script type="text/javascript">
		var base_url = '<?php echo $base_url; ?>';
	</script>
</head>
<body>
<?php
if (isset($errors) && count($errors) > 0)
{
	foreach ($errors AS $error)
	{
	?>
		<p>Error: <?php echo $error; ?></p>	
	<?php
	}
}
?>
<form action="<?=$base_url?>auth/login" id="loginForm" class="mainForm" method="post" accept-charset="utf-8">
            <fieldset>
                <div class="loginRow noborder">
                    <label for="req1">Username:</label>
                    <div class="loginInput"><input type="text" name="username" id="username" /></div>
                    <div class="fix"></div>
                </div>

                <div class="loginRow">
                    <label for="req2">Password:</label>
                    <div class="loginInput"><input type="password" name="password" id="password" /></div>
                    <div class="fix"></div>
                </div>

                <div class="loginRow">
                    <input type="submit" value="Log me in" class="greyishBtn submitForm" />
                    <div class="fix"></div>
                </div>
            </fieldset>
</form>
