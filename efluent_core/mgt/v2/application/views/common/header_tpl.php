<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>eFluent v.2</title>
	<base href="<?php echo $base_url; ?>">

	<!-- load css files -->
	<?php
	if (isset($css))
	{
		foreach (array_unique($css) AS $css_file)
		{
			?>
			<link rel="stylesheet" href="<?php echo $base_url . $css_file; ?>" type="text/css" media="all" />
			<?php
		}
	}
	?>
	<!-- end of load css files -->
	
	<!-- load js files -->
	<?php
	if (isset($js))
	{
		foreach (array_unique($js) AS $js_file)
		{
			?>
			<script src="<?php echo $base_url . $js_file; ?>" type="text/javascript"></script>
    		<?php
		}
	}
	?>
	<!-- end of load js files -->

	<script type="text/javascript">
		var base_url = '<?php echo $base_url; ?>';
	</script>
</head>
<body>