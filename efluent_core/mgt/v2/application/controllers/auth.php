<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller
{
	public function __construct() 
	{
        parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('auth_model');
    }

    public function index() 
    {
		if ($this->session->userdata('ID'))
		{
			redirect($this->config->item('base_url'));
		}
		else 
		{
	    	$this->data['base_url'] = $this->config->item('base_url');
			$this->load->view('login_tpl', $this->data);
		}
	}

    public function login() 
    {
    	$return_url = $this->session->set_userdata('redirect');
    	$this->session->unset_userdata('redirect');
		if (!$this->session->userdata('ID'))
		{
	    	$user = $this->input->post('username');
			$pass = $this->input->post('password');
	    	$this->data['base_url'] = $this->config->item('base_url');
			$auth_response = $this->auth_model->authUser($user, $pass);
			if (count($auth_response['errors']) == 0)
			{
				redirect($this->config->item('base_url').$return_url);
			}
			else 
			{
		    	$this->data['errors'] = $auth_response['errors'];
				$this->load->view('login_tpl', $this->data);
			}
		}
		else 
		{
			redirect($this->config->item('base_url').$return_url);
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect($this->config->item('base_url').'auth/');
	}
}