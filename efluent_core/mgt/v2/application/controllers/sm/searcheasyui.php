<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Searcheasyui extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->helper('json');
		$this->load->library('session');
		$this->load->library('search_query');
		$this->load->model('auth_model');
		$this->load->model('efluent_model');
		$this->load->model('sm/searcheasyui_model');
		$this->userID = $this->session->userdata('ID');
//		$this->userID = 89;
	}
	
	public function index()
	{
		$this->data['css'][] = 'jquery_easyui/themes/default/easyui.css';
		$this->data['css'][] = 'jquery_easyui/themes/icon.css';
		$this->data['css'][] = 'css/style.css';
		
		$this->data['js'][] = 'js/jquery-1.9.1.min.js';
		$this->data['js'][] = 'jquery_easyui/jquery.easyui.min.js';
		$this->data['js'][] = 'js/sm/search_easyui.js';

		$this->data['base_url'] = $this->config->item('base_url');
        
		if ($this->auth_model->isLoggedIn() === false)
		{
			$this->session->set_userdata(array('redirect' => $_SERVER['REQUEST_URI']));
			redirect($this->config->item('base_url').'auth');
		}
		else 
		{
			$this->data['categories'] = $this->efluent_model->getCategories(0);
			$this->data['brands'] = $this->efluent_model->getBrands(0);
			$this->data['groups'] = $this->efluent_model->getGroups();
			$this->data['assigned_to'] = $this->efluent_model->getAllAssignedTo();
			$this->data['whid'] = $this->efluent_model->getAllWarehouses();
			$this->data['sourcings'] = $this->efluent_model->getAllVendors();
			$this->data['display_on'] = $this->efluent_model->getAllSitesDisplayOn();
			$this->data['lists'] = $this->efluent_model->getLists(0);
			$this->data['search_filters'] = $this->searcheasyui_model->getSearchFilters($this->userID);
			$this->data['search_columns'] = $this->get_columns();	
			
			$this->load->view('common/header_tpl', $this->data);
			$this->load->view('sm/search_index_easyui_tpl');
			$this->load->view('common/footer_tpl');
		}
	}

	public function get_columns()
	{
		$search_columns_tmp = $this->searcheasyui_model->getSearchColumns($this->userID);	
		$search_columns = $this->searcheasyui_model->addExceptionsSearchColumns($search_columns_tmp);	
		return $search_columns;
		/*
		$columns_structure = '';
		if (is_array($search_columns))
		{
			$columns_structure .= '[[';
			foreach ($search_columns AS $k => $v)
			{
				$columns_structure .= "{field:'".$v['id']."',title:'".$v['title']."'},";				
			}		
			echo substr($columns_structure,0,-1) . ']]';
		}
		*/
	}
	
	public function get_lists()
	{
        $group_id = (int) $this->input->post('id');
        $this->data['lists'] = $this->efluent_model->getLists($group_id);
        $this->load->view('sm/form_select_lists_tpl', $this->data);
	}

	public function get_brands()
	{
        $catID = (int) $this->input->post('id');
        $this->data['brands'] = $this->efluent_model->getBrands($catID);
		$this->load->view('sm/form_select_brands_tpl', $this->data);
	}
	
	public function get_categories()
	{
        $catID = (int) $this->input->post('id');
        $this->data['categories'] = $this->efluent_model->getCategories($catID);
		$this->load->view('sm/form_select_categories_tpl', $this->data);
	}
	
	public function get_result() 
	{
		$columns = $this->searcheasyui_model->getSearchColumns($this->userID);
		$filters =  $this->input->post(NULL, TRUE);
		$query = $this->search_query->createQuery($filters,$columns);
		$output = $this->searcheasyui_model->getResultNew($query,$filters);
		echo json_encode($output);				
	}
}

/* End of file search.php */
/* Location: ./application/controllers/search.php */