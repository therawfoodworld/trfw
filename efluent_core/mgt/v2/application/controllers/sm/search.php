<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->helper('json2');
		$this->load->library('session');
		$this->load->library('search_query');
		$this->load->model('auth_model');
		$this->load->model('efluent_model');
		$this->load->model('sm/search_model');
		$this->userID = $this->session->userdata('ID');
	}
	
	public function index()
	{
		$this->data['css'][] = 'css/dataTable.css';
		$this->data['css'][] = 'css/style.css';
		$this->data['css'][] = 'css/ui_custom.css';
		
		$this->data['js'][] = 'js/jquery-1.9.1.min.js';
		$this->data['js'][] = 'js/jquery-migrate-1.1.1.js';
		$this->data['js'][] = 'js/jquery-ui.min.js';
		$this->data['js'][] = 'js/plugins/tables/jquery.dataTables.js';
		$this->data['js'][] = 'js/plugins/tables/standing_redraw.js';
		$this->data['js'][] = 'js/plugins/tables/jquery.dataTables.reload.js';
		$this->data['js'][] = 'js/plugins/tables/jquery.listnav.min-2.1.js';
		$this->data['js'][] = 'js/plugins/tables/jquery.dataTables.listnav.js';
		$this->data['js'][] = 'js/plugins/forms/forms.js';
		$this->data['js'][] = 'js/plugins/ui/jquery.timeentry.min.js';
		$this->data['js'][] = 'js/sm/search.js';

		$this->data['base_url'] = $this->config->item('base_url');
        
		if ($this->auth_model->isLoggedIn() === false)
		{
			$this->session->set_userdata(array('redirect' => $_SERVER['REQUEST_URI']));
			redirect($this->config->item('base_url').'auth');
		}
		else 
		{
			$this->data['categories'] = $this->efluent_model->getCategories(0);
			$this->data['brands'] = $this->efluent_model->getBrands(0);
			$this->data['groups'] = $this->efluent_model->getGroups();
			$this->data['assigned_to'] = $this->efluent_model->getAllAssignedTo();
			$this->data['whid'] = $this->efluent_model->getAllWarehouses();
			$this->data['sourcings'] = $this->efluent_model->getAllVendors();
			$this->data['display_on'] = $this->efluent_model->getAllSitesDisplayOn();
			$this->data['lists'] = $this->efluent_model->getLists(0);
			$this->data['search_filters'] = $this->search_model->getSearchFilters($this->userID);
			$search_columns_tmp = $this->search_model->getSearchColumns($this->userID);	
			$this->data['search_columns'] = $this->search_model->addExceptionsSearchColumns($search_columns_tmp);	
			
			$this->load->view('common/header_tpl', $this->data);
			$this->load->view('sm/search_index_tpl');
			$this->load->view('common/footer_tpl');
		}
	}

	public function get_lists()
	{
        $group_id = (int) $this->input->post('id');
        $this->data['lists'] = $this->efluent_model->getLists($group_id);
		$this->load->view('sm/form_select_lists_tpl', $this->data);
	}

	public function get_brands()
	{
        $catID = (int) $this->input->post('id');
        $this->data['brands'] = $this->efluent_model->getBrands($catID);
		$this->load->view('sm/form_select_brands_tpl', $this->data);
	}
	
	public function get_categories()
	{
        $catID = (int) $this->input->post('id');
        $this->data['categories'] = $this->efluent_model->getCategories($catID);
		$this->load->view('sm/form_select_categories_tpl', $this->data);
	}
	
	public function get_result() 
	{
		$columns = $this->search_model->getSearchColumns($this->userID);
		$filters =  $this->input->post(NULL, TRUE);
		$query = $this->search_query->createQuery($filters,$columns);
		$result = $this->search_model->getResultNew($query['query'],$query['count_query'],$filters);
		$output = array('sEcho' => $this->input->post('sEcho'),
						'iTotalRecords' => $result['total_records'],
						'iTotalDisplayRecords' => $result['total_records'],
						'aaData' => $result['accounts'],
						'queryres' => $result['query']
						);
		echo jsonEncode($output);				
	}
}

/* End of file search.php */
/* Location: ./application/controllers/search.php */