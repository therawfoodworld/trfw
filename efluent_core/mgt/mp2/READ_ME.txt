To install the example......   copy the files to a LAMP server and edit config.phtml with the appropriate database information.
You will then need to load the attached mp2.sql database which contains one table with a list of skus and crawler ids.

Developer Note:  Years ago we called the crawlerid "roimast".  To make my life easier I only changed it to crawlerid in the newer code.

Some examples:

To get the listing of PE Urls for a given SKU:
http://mp2.vendortools.com/mp2/pegrid.php?sku=SONKDL46W4150

To get the real-time listings for a given SKU:
http://mp2.vendortools.com/mp2/pereport.php?skulist=SONKDL46W4150&pelist=PG,AM,DT,NT

To get the real-time listings for a list of SKUs:
/pereport.php?skulist=sku,sku,sku,sku,sku,sku&pelist=XX,XX,XX,XX




The available Price Engines
PG - PriceGrabber
CN - C|Net Shopper
DT - Shopping.com
BR - BizRate.com
YS - Yahoo Shopping
AM - Amazon.com
NT - NexTag
BO - Bountii
F2 - Froogle (grp)