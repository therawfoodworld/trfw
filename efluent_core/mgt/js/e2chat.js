/////////////////////////////////////////
var eChatcount = 0; // the # of windows currently open
var eChatID=new Array(); //  chatID
var eChatDV=new Array(); //  divID
var eChatLM=new Array();
var eChat = 5; // its actually this - 1 (cuz we start at 1)
var g_lastmsg = 0;

for(xx = 0; xx < eChat; xx++){ eChatID[xx] = 0; eChatDV[xx] = 0; eChatLM[xx] = 0; }


function openChat(chatID, chatName)
{
	hideMessageCenter();

	for(var xx = 0; xx < 5; xx++)
	{
		if(parseInt(eChatID[xx]) == parseInt(chatID)) // there's already a window open
		{
			
			break;
		}
		else if(parseInt(eChatID[xx]) == 0)
		{
			eChatID[xx] = chatID;
			eChatDV[xx] = xx;
			break;
		}
	}
	
	eChatLM[xx] = g_lastmsg;
	
	////////////////////////////////////////////////////////////
	// open up the container
	var chat = document.getElementById("chatwindow"+xx);
	chat.style.display = "block";
	
	///////////////////////////////////////////////////////////
	// Let's set 
	var chat_title = document.getElementById("chatwindowtitle"+xx);
	chat_title.innerHTML = "Chat with <B>"+chatName+"</B>";
	

	var chattext = document.getElementById("message"+xx);
	chattext.focus();
	
	return xx; // return the div
}

function hideChat(divid)
{
	for(var xx = 0; xx < 5; xx++)
	{// find the match and shut it down
		if(parseInt(eChatDV[xx]) == parseInt(divid))
		{
			var chat = document.getElementById("chatwindow"+xx);
			chat.style.display = "none";
			
			var lastmsg = eChatLM[xx];
			var chatid  = eChatID[xx];
			
			var chat_text = document.getElementById("chatText"+divid);
			chat_text.innerHTML = ""; // clear out the old conversation
						
			eChatLM[xx] = 0;
			eChatID[xx] = 0;
			eChatDV[xx] = 0;
			//detectMinMax();
			/////////////////////////////////////////////////////////////////////////
			
			xmlHttp=GetXmlHttpObject();
			
			var url="/mgt/chat_markread.phtml?id="+lastmsg+"&chatid="+chatid+"&sid="+Math.random();
			xmlHttp.onreadystatechange=messageSent;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);				
		}
	}
	

		
	return false;	
}

function showMessageCenter()
{
	var mc = document.getElementById("messagecenter");
	mc.style.display = "block";
}

function hideMessageCenter()
{
	var chat = document.getElementById("messagecenter");
	chat.style.display = "none";
	
}




function Browser() {

  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

var browser = new Browser();

// Global object to hold drag information.

var dragObj = new Object();
dragObj.zIndex = 0;

function dragStartX(event, id) {

  var el;
  var x, y;

  // If an element id was given, find it. Otherwise use the element being
  // clicked on.

  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isIE)
      dragObj.elNode = window.event.srcElement;
    if (browser.isNS)
      dragObj.elNode = event.target;

    // If this is a text node, use its parent element.

    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Save starting positions of cursor and element.

  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
  dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);

  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

  // Update element's z-index.

  dragObj.elNode.style.zIndex = ++dragObj.zIndex;

  // Capture mousemove and mouseup events on the page.

  if (browser.isIE) {
    document.attachEvent("onmousemove", dragGoX);
    document.attachEvent("onmouseup",   dragStopX);
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS) {
    document.addEventListener("mousemove", dragGoX,   true);
    document.addEventListener("mouseup",   dragStopX, true);
    event.preventDefault();
  }
}

function dragGoX(event) {

  var x, y;

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Move drag element by the same amount the cursor has moved.

  dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
  dragObj.elNode.style.top  = (dragObj.elStartTop  + y - dragObj.cursorStartY) + "px";

  if (browser.isIE) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS)
    event.preventDefault();
}

function dragStopX(event) {

  // Stop capturing mousemove and mouseup events.

  if (browser.isIE) {
    document.detachEvent("onmousemove", dragGoX);
    document.detachEvent("onmouseup",   dragStopX);
  }
  if (browser.isNS) {
    document.removeEventListener("mousemove", dragGoX,   true);
    document.removeEventListener("mouseup",   dragStopX, true);
  }
}

function GetXmlHttpObject()
{
	var xmlHttp=null;

	try
    {
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
    catch (e)
    {
    	// Internet Explorer
        try
        {
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
        {
        	try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
            catch (e)
            {
            	alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	
	return xmlHttp;
}

function onSendMessage(divid)
{
	var message_to = 0;
	
	for(var xx = 0; xx < 5; xx++)
	{// find the match 

		if(parseInt(eChatDV[xx]) == parseInt(divid))
		{
			//var chat = document.getElementById("chatwindow"+xx);
			//chat.style.display = "none";
				
			message_to = eChatID[xx];
			break;
		}	
	}
	
	if(message_to == 0)
	{
		alert('Internal Error Finding chatDV');
		return;
	}

	var msg = document.getElementById("message"+divid);
	if(!msg)
	{
		alert("Cannot find message"+div+" window");
		return;
	}

	var text_to_send = msg.value;
	msg.value = "";
	
	if(text_to_send == "")
		return;
		
	var chat_text = document.getElementById("chatText"+divid);
	chat_text.innerHTML += "<BR><font color='#808080'>You Said: "+text_to_send+"</font>";
	scrollDiv(divid);
	
	xmlHttp=GetXmlHttpObject();
	
	var url="/mgt/chat_sendmessage.phtml?msg_to="+message_to+"&message_body="+text_to_send+"&sid="+Math.random();
	xmlHttp.onreadystatechange=messageSent;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);	
		
	return false;
}

function messageSent(){if(xmlHttp.readyState==4){}}

function onCheckMessages()
{
	xml2Http=GetXmlHttpObject();
	
	var url="/mgt/chat_getmessage.phtml?lastmsg="+g_lastmsg+"&sid="+Math.random();
	xml2Http.onreadystatechange=messageReceived;
	xml2Http.open("GET",url,true);
	xml2Http.send(null);	
		
	return false;
}

function messageReceived() 
{ 
	if(xml2Http.readyState==4)
	{ 
		//xmlHttp.responseText;
		if(xml2Http.responseText != "")
		{
			//alert(xml2Http.responseText);
			var msgList = xml2Http.responseText.split('\n');
			
			for(i = 0; i < msgList.length; i++)
			{
				var msgData = msgList[i].split("|");
				
				/*
				0 - msg_from
				1 - buyername
				2 - showdate
				3 - $msg_body
				4 - $id
				*/
				
				if( typeof msgData[4] != 'undefined')
				g_lastmsg = parseInt( msgData[4] );
				
				var msg = msgData[3];
	
				if(msg != "" && typeof msg != 'undefined')
				{
					if(msg.substring(0, 4) == "PUSH")
					{
						pushData = msg.split(">");
						msgData[3] = pushData[1];
						window.open( pushData[1] );
					}
					else
					{
						
						var divid = openChat(msgData[0], msgData[1]);
						var chat_text = document.getElementById("chatText"+divid);
						chat_text.innerHTML += "<BR>"+msgData[1]+" ("+msgData[2]+"):"+msgData[3]+"";
						scrollDiv(divid);
					}
				}
			}
			
		}
		
		setTimeout('onCheckMessages()', 5000);
	}
}

function scrollDiv(divid)
{
	var el;
	if ((el = document.getElementById("chatText"+divid))&& ('undefined' != typeof el.scrollTop))
	{
		el.scrollTop = 0;
		el.scrollTop = 5000;
	}
}

function stopReturn(oTextbox, oEvent, divid) 
{ 
	var keyCode;
	
	if (browser.isIE) 
   	{
   		keyCode = oEvent.keyCode;	
   	}
   	else if (browser.isNS) 
   	{
		keyCode = oEvent.charCode;
   	}
    
   	
	switch(keyCode) 
	{ 
		case 0: // enter in firefox
 		case 13: //enter  
 		{
 			onSendMessage(divid);
 			
			if(browser.isIE) 
		   	{
		   		window.event.cancelBubble = true;
			    window.event.returnValue = false;
		   	}
		   	else if(browser.isNS) 
		   	{
				event.preventDefault();
		   	}
			
 			return false; 
 		}
	}  
} 

onCheckMessages();