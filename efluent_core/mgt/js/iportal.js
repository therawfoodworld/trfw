var g_usediv = 0;

function getPageWH()
{
	if (parseInt(navigator.appVersion)>3) 
	{
		if (navigator.appName=="Netscape") 
		{
			winW = window.innerWidth;
			winH = window.innerHeight;
	 	}
	 
	 	if (navigator.appName.indexOf("Microsoft")!=-1) 
	 	{
	  		winW = document.body.offsetWidth;
	  		winH = document.body.offsetHeight;
	 	}
	}	
}

function dragGoWidget(event) 
{
	var x, y;

	if (browser.isIE) 
	{
		x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
	}
  
	if (browser.isNS) 
	{
		x = event.clientX + window.scrollX;
		y = event.clientY + window.scrollY;
	}

	// Move drag element by the same amount the cursor has moved.
	dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
	dragObj.elNode.style.top  = (dragObj.elStartTop  + y - dragObj.cursorStartY) + "px";

	if (browser.isIE) 
	{
		window.event.cancelBubble = true;
		window.event.returnValue = false;
	}
  
	if (browser.isNS)
    	event.preventDefault();
    
    
    x = dragObj.elStartLeft + x - dragObj.cursorStartX;
    y = dragObj.elStartTop  + y - dragObj.cursorStartY;
    
    var usediv = 0;
	var bestmatch = 0;
    
	for(xx = 1; xx < 10; xx++)
	{
		var xpos = 0;
		var ypos = 0;
		
		var contentDIV = document.getElementById("gf"+xx);
		contentDIV.style.backgroundColor = "#ffffff";
		contentDIV.style.border = 0;

		var data = getPos(contentDIV);
		
		xpos = data.x;
		ypos = data.y;
		
		var pointA = 300 - Math.abs(parseInt(xpos) - parseInt(x));
		var pointB = 300 - Math.abs(parseInt(ypos) - parseInt(y));
		
		var stddim = 300 * 300;
		var curdim = pointA * pointB;
		var curpct = (curdim / stddim) * 100;
		

		if( (curdim <= stddim) &&
			(curpct > bestmatch) )
		{
			usediv = xx;
			bestmatch = curpct;
		}
		
	}

	if(usediv > 0)
	{
		var contentDIV = document.getElementById("gf"+usediv);
		contentDIV.style.backgroundColor = "#eeeeee";
		contentDIV.style.border = "1px";
		contentDIV.style.borderColor = '#808080';
		contentDIV.style.borderStyle = 'dashed';
		g_usediv = usediv;	
	}
}

function getPos(el) 
{
    for (var lx=0, ly=0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}


function dragStopWidget(event) 
{
	var coverFrame = "cover"+currentDragID;
 
	var cvr = document.getElementById(coverFrame);
	cvr.style.display = "none";

	// Stop capturing mousemove and mouseup events.
	if (browser.isIE) 
	{
		document.detachEvent("onmousemove", dragGoWidget);
		document.detachEvent("onmouseup",   dragStopWidget);
	}
  	if (browser.isNS) 
  	{
		document.removeEventListener("mousemove", dragGoWidget,   true);
		document.removeEventListener("mouseup",   dragStopWidget, true);
	}

	if (browser.isIE) 
  	{
    	x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
    	y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
	}

	if (browser.isNS) 
	{
		x = event.clientX + window.scrollX;
		y = event.clientY + window.scrollY;
	}
  
	if(parseInt(g_usediv) > 0)
  	{
  		var contentDIV = document.getElementById("gf"+g_usediv);
  		var data = getPos(contentDIV);
		
  		contentDIV.style.backgroundColor = "#ffffff";
  				
		xpos = data.x;
		ypos = data.y;

		//contentDIV.innerHTML = "xpos: "+xpos+" ypos: "+ypos;
		
		var myDIV = document.getElementById("win"+currentDragID);
		myDIV.style.top = data.y;
		myDIV.style.left = data.x;
		myDIV.style.zIndex = 0;
		
		contentDIV.style.border = "0px";
  	}
  	
  //alert("WIDGET DRAGID:"+currentDragID+" X: "+x+" Y: "+y);
}

function posWidget()
{
  	var contentDIV = document.getElementById("gf1");
  	var data = getPos(contentDIV);
		
  	contentDIV.style.backgroundColor = "#ffffff";
  				
	xpos = data.x;
	ypos = data.y;

	//contentDIV.innerHTML = "xpos: "+xpos+" ypos: "+ypos;
		
	var myDIV = document.getElementById("win100");
	myDIV.style.top = data.y;
	myDIV.style.left = data.x;	
}

function GetXmlHttpObject()
{
	var xmlHttp=null;

	try
    {
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
    catch (e)
    {
    	// Internet Explorer
        try
        {
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
        {
        	try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
            catch (e)
            {
            	alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	
	return xmlHttp;
}

function loadiPortal()
{
	var url="/mgt/iportal/panel.phtml";
	url += "?sid="+Math.random();

	xmlHttp=GetXmlHttpObject();
	
	if (xmlHttp==null)
 	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 	

 	xmlHttp.onreadystatechange=gotPortal;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);	
}

function gotPortal()
{
	if(xmlHttp.readyState==4)
	{ 
		var contentDIV = document.getElementById("iportal");
		contentDIV.innerHTML = xmlHttp.responseText;
	}			
}

function StartUp()
{
	//posWidgets();
	loadiPortal();
}

function showWidgetList()
{
	var mydiv=document.getElementById("widgetlist");
	
	if(!mydiv)
	{
		alert('DIV NOT FOUND');
	}
	else
	{
		if(mydiv.style.display == "block")
		{
			mydiv.style.display = "none";
		}
		else
		{
			mydiv.style.display = "block";
			mydiv.style.left = posX - 255;
			mydiv.style.top = posY + 5;
			mydiv.style.zIndex = 1000;
		}
	}
	
}

function hideWidgetList()
{
	var mydiv=document.getElementById("widgetlist");
	
	if(!mydiv)
	{
		alert('DIV NOT FOUND');
	}
	else
	{
		mydiv.style.display = "none";
		mydiv.style.zIndex = -1;
	}
}

function addWidget(wid)
{
	document.location = '/mgt/main.phtml?wid='+wid;
}