$(document).ready(function(){
	$('.show_bulk_popup').each(function(){
		var pid = $(this).attr('id');
		$(this).click(function() {
			$.get('/m/ajax_bulk_price_calc.phtml?pid=' + pid + '&list_bulk_options', function(result_data) {
                $('.bulk_popup').html(result_data);
                $('.bulk_popup_mask').show();
                $('.bulk_popup').css('position', 'fixed');
                $('.bulk_popup').show();
                            
            	$('.hide_bulk_popup').on('click', function() {	
                    $('.bulk_popup_mask').hide();
                    $('.bulk_popup').html('').hide();
                });	

                $('.bulk_popup_mask').on('click', function() {	
                    $('.bulk_popup_mask').hide();
                    $('.bulk_popup').html('').hide();
                });	
			});
		});
	});
	
	$('.show_atcost_popup').each(function(){
		var pid = $(this).attr('id');
		$(this).click(function() {
			$.get('/m/ajax_atcost_price_calc.phtml?pid=' + pid + '&list_atcost_options', function(result_data) {
                            $('.atcost_popup').html(result_data);
                            $('.bulk_popup_mask').show();
                            $('.atcost_popup').css('position', 'fixed');
                            $('.atcost_popup').show();
                            
                            	$('.hide_bulk_popup').on('click', function() {	
                                    $('.bulk_popup_mask').hide();
                                    $('.atcost_popup').html('').hide();
                                });	
                            
			});
		});
	});
});

function toggleMenu(mainCat, subCat) {
	main = document.getElementById(mainCat);
	sub = document.getElementById(subCat);
	
	if (sub.style.display == "none") {
		sub.style.display = "block";
		main.getElementsByTagName("span")[0].firstChild.nodeValue = '-';
	} else {
		sub.style.display = "none";
		main.getElementsByTagName("span")[0].firstChild.nodeValue = '+';
	}
}