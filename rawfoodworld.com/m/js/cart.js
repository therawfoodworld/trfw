jQuery(document).ready(function(){
	jQuery('.add2cart-btn').each(function(){
		jQuery(this).click(function(){
			var pid = jQuery(this).attr('id');
			var qty = 1;
			// check for shipping location
			jQuery.get('/m/ajax_functions.phtml',{ action: "check_ship_loc", 'pid': pid }, function(response){
				if (response == 'yes') {
					window.location = "/m/cart.phtml?id=" + pid + "&qty=" + qty + "&upsell=ok";
				} else if (response == 'no') {
					alert('We\'re sorry, this item cannot be shipped to your country due to licensing restrictions with the manufacturer. Please contact us for alternative options.');					
				} else {
					window.location = "/m/cart.phtml?id=" + pid + "&qty=" + qty + "&upsell=ok";
				}
			});
		});
	});
	
	jQuery('.update-qty').each(function(){
		jQuery(this).click(function(){
			var pid = jQuery(this).attr('id');
			var qty = jQuery('#qty_' + pid).val();
			jQuery.ajax({method: 'GET', url: '/m/ajax_functions.phtml', data: { action: 'change_qty', prod: pid, qty: qty}, cache: false, dataType: 'html', success: function(result){
				location.reload();
			}});		
		});
	});

	jQuery('.remove-from-cart').click(function() {
		var pid = jQuery(this).attr('id');
		jQuery.ajax({method: 'GET', url: '/m/ajax_functions.phtml', data: { action: 'change_qty', prod: pid, qty: 0}, cache: false, dataType: 'html', success: function(result){
			location.reload();
		}});		
	});

	jQuery('.empty-cart').click(function(){
		var pid_list = '';
		jQuery('.remove-from-cart').each(function(){
			var pid = jQuery(this).attr('id');
			pid_list += pid + '_';
		});
		jQuery.get('/m/ajax_functions.phtml',{ action: 'remove_checked', prods: pid_list,}, function(data){
			location.reload();
		}); 
	});
	
	$('.btnApply').click(function(){
		var coupon_val = $('.cuppon').val();
		$.get('/m/ajax_functions.phtml',{ action: "apply_cuppon", couponcode: coupon_val }, function(data) {
			window.location = "/cart.phtml";
		}); 
	});	
	
	$('#applyCuppon').click(function(){
		var promoVal = $('#promo').val();
		$.get('/m/ajax_functions.phtml',{ action: "apply_cuppon",couponcode: promoVal}, function(data) {
			  alert(promoVal);
			location.reload();
		});
	});
});