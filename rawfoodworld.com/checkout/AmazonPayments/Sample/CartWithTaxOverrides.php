<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */


include_once ('.config.inc.php'); 

// Defining item1 attributes
$item1SKU = "444-44-4";
$item1Title = "Product1";
$item1UnitPrice = 0.20;
$item1Quantity = 1;
$item1Description = "Sample Product1";
$item1Weight = 2.00;
$item1Discount = 1.00;
$item1PromoId = "promo1";

// Setting TaxTable for Item1
// Item level tax can be enabled to each item and in this case cart level tax will be disabled
// If you want to use cart level tax comment the item level tax and uncomment the cart level tax
$taxtTable1USBaseRate = 0.10;
$taxtTable1NonUSBaseRate = 0.12;
// Tax for State1
$taxTable1State1 = "WA";
$taxTable1State1Rate = 0.10;
$taxTable1State1IsShippingtaxed = true;
// Tax for State2
$taxTable1State2 = "PA";
$taxTable1State2Rate = 0.03;
$taxTable1State2IsShippingtaxed = true;
// taxTable definition for item1
$taxTable1 = new \AmazonPayments\Model\TaxTable($taxtTable1USBaseRate, $taxtTable1NonUSBaseRate);
$taxTable1->setUSStateTaxRate($taxTable1State1, $taxTable1State1Rate,$taxTable1State1IsShippingtaxed);
$taxTable1->setUSStateTaxRate($taxTable1State2, $taxTable1State2Rate, $taxTable1State2IsShippingtaxed);

//Creating and initializing another item
// Defining item2 attributes
$item2SKU = "555-55-5";
$item2Title = "Product 2";
$item2UnitPrice = 0.40;
$item2Quantity = 1;
$item2Description = "Sample Product 2";
$item2Weight = 6.00;

// Setting TaxTable for item2
// Item level tax can be enabled to each item and in this case cart level tax will be disabled
// If you want to use cart level tax comment the item level tax and uncomment the cart level tax
$taxtTable2USBaseRate = 0.10;
$taxtTable2NonUSBaseRate = 0.12;
// Tax for State2
$taxTable2State2 = "CA";
$taxTable2State2Rate = 0.10;
$taxTable2State2IsShippingtaxed = true;
// Tax for State2
$taxTable2Zip = "98414";
$taxTable2ZipRate = 0.11;
$taxTable2ZipIsShippingtaxed = false;
// taxTable definition for item2
$taxTable2 = new \AmazonPayments\Model\TaxTable($taxtTable2USBaseRate, $taxtTable2NonUSBaseRate);
$taxTable2->setUSStateTaxRate($taxTable2State2, $taxTable2State2Rate,$taxTable2State2IsShippingtaxed);
$taxTable2->setUSZipCodeTaxRate($taxTable2Zip, $taxTable2ZipRate, $taxTable2ZipIsShippingtaxed);


//First Item
//Initializing an Item object with SKU, quantity, unit price and name
$item1 = new \AmazonPayments\Model\Item($item1SKU,$item1Title,$item1UnitPrice,$item1Quantity);

$item1->setWeight($item1Weight);
$item1->setDescription($item1Description);
$item1->setTaxTable($taxTable1);

//Second Item
$item2 = new \AmazonPayments\Model\Item($item2SKU,$item2Title,$item2UnitPrice,$item2Quantity);

$item2->setWeight($item2Weight);
$item2->setDescription($item2Description);
$item2->setTaxTable($taxTable2);

//The following properties are essential for the functioning of the sample code
//These are mandatory, and without them the API will not function
//uncomment the below code and fill them with the correct values to use the properties array with the cart
//Define the missing properties below
// $propertiesAccessKey = "";
// $propertiesSecretKey = "";
// $propertiesMerchantId = "";
// $propertiesCurrencyCode = "USD";
// $propertiesWeightUnit = "LB";

// $properties = array();

// $properties['accessKey'] = $propertiesAccessKey;
// $properties['secretKey'] = $propertiesSecretKey;
// $properties['merchantId'] = $propertiesMerchantId;
// $properties['currencyCode'] = $propertiesCurrencyCode;
// $properties['weightUnit'] = $propertiesWeightUnit;

// $cart = new \AmazonPayments\Model\Cart($properties);

//To load the values from a properties file, comment the above code and use the below code

//Define the path to the properties file here.If you have followed the default instructions and put
//the downloaded cba_config.ini in the AmazonPayments directory then the following will work
$propertiesFilePath = "../cba_config.ini";

//Create a cart object by reading the properties from the path provided
$cart = new \AmazonPayments\Model\Cart($propertiesFilePath);

//Adding the items to the cart
$cart->addItem($item1);
$cart->addItem($item2);

// Setting TaxTable for Cart
// If you want to use cart level tax comment the item level tax tables above and uncomment the cart level tax
$taxtTable3USBaseRate = 0.10;
$taxtTable3NonUSBaseRate = 0.13;

$taxTable3State3 = "CA";
$taxTable3State3Rate = 0.10;
$taxTable3State3IsShippingtaxed = true;

$taxTable3Zip = "98414";
$taxTable3ZipRate = 0.11;
$taxTable3ZipIsShippingtaxed = false;

$taxTable3 = new \AmazonPayments\Model\TaxTable($taxtTable3USBaseRate, $taxtTable3NonUSBaseRate);
$taxTable3->setUSStateTaxRate($taxTable3State3, $taxTable3State3Rate,$taxTable3State3IsShippingtaxed);
$taxTable3->setUSZipCodeTaxRate($taxTable3Zip, $taxTable3ZipRate, $taxTable3ZipIsShippingtaxed);

//Adding the tax table object as default tax table of the cart, by uncommenting the below line
$cart->setTaxTable($taxTable3);

//For debugging the Cart object uncomment the following line
// echo var_dump($cart);

//The value to be placed in the orderInput: part of the HTML snippet
echo "orderInput value:<br>".$cart->createOrderInputValue();

?>
