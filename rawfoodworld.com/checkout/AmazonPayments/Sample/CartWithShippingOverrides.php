<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */


include_once ('.config.inc.php'); 

// Defining item1 attributes
$item1SKU = "444-44-4";
$item1Title = "Product1";
$item1UnitPrice = 0.20;
$item1Quantity = 1;
$item1Description = "Sample Product1";
$item1Weight = 2.00;


//Creating and setting up a ShippingRate object to specify the type of shipping rate to be used in a ShippingMethod
$shippingRate1ShipmentBasedRate = 10.20;
$shippingrate1 = new \AmazonPayments\Model\ShippingRate();

//Setting the shipping rate as Shipment Based
$shippingrate1->setShipmentBasedRate($shippingRate1ShipmentBasedRate);

//Initializing a ShippingMethod with REGION, SERVICELEVEL, name, and a shipping rate
$shippingMethod1ShippingLabel = "USPS Ground";

$shippingMethod = new \AmazonPayments\Model\ShippingMethod(\AmazonPayments\Model\ShippingMethod::REGION_US_CONTINENTAL_48_STATES,
										\AmazonPayments\Model\ShippingMethod::SERVICE_ONE_DAY,$shippingMethod1ShippingLabel,$shippingrate1);

//For debugging ShippingMethod object uncomment the following line
// echo var_dump($shippingMethod);

//First Item
//Initializing an Item object with SKU, quantity, unit price and name
$item1 = new \AmazonPayments\Model\Item($item1SKU,$item1Title,$item1UnitPrice,$item1Quantity);

$item1->setWeight($item1Weight);
$item1->setDescription($item1Description);
$item1->setFulfillmentMethod(\AmazonPayments\Model\Item::FULFILLED_BY_MERCHANT);

//Associating the ShippingMethod with an item
$item1->addShippingMethod($shippingMethod);


//Creating and setting up a ShippingRate object to specify the type of shipping rate to be used in a ShippingMethod
$shippingRate2WeightBasedRate = 0.90;
$shippingrate2 = new \AmazonPayments\Model\ShippingRate();

//Setting the shipping rate as Weight Based
$shippingrate2->setWeightBasedRate($shippingRate2WeightBasedRate);

//Initializing a ShippingMethod with REGION, SERVICELEVEL, name, and a shipping rate
$shippingMethod2ShippingLabel = "USPS Sea";

$shippingMethod2 = new \AmazonPayments\Model\ShippingMethod(\AmazonPayments\Model\ShippingMethod::REGION_CUSTOM,
										\AmazonPayments\Model\ShippingMethod::SERVICE_STANDARD,$shippingMethod2ShippingLabel,$shippingrate2);
$shippingMethod2->includeCustomCountry("DE");
$shippingMethod2->includeCustomCountry("FR");
$shippingMethod2->excludeCustomCountry("UK");

//Uncomment the below line to associate the ShippingMethod with an item
// $item1->addShippingMethod($shippingMethod2);

//Creating and initializing another item
// Defining item2 attributes
$item2SKU = "555-55-5";
$item2Title = "Product 2";
$item2UnitPrice = 0.40;
$item2Quantity = 1;
$item2Description = "Sample Product 2";
$item2Weight = 6.00;

//Second Item
$item2 = new \AmazonPayments\Model\Item($item2SKU,$item2Title,$item2UnitPrice,$item2Quantity);

$item2->setWeight($item2Weight);
$item2->setDescription($item2Description);
$item2->setFulfillmentMethod(\AmazonPayments\Model\Item::FULFILLED_BY_MERCHANT);

//Defining another ShippingRate and ShippingMethod objects
$shippingRate3ItemQuantityBasedRate = 12.20;
$shippingrate3 = new \AmazonPayments\Model\ShippingRate();
$shippingrate3->setItemQuantityBasedRate($shippingRate3ItemQuantityBasedRate);

$shippingMethod3ShippingLabel = "UPS Next Day";
$shippingMethod3 = new \AmazonPayments\Model\ShippingMethod(\AmazonPayments\Model\ShippingMethod::REGION_US_ALL,
										\AmazonPayments\Model\ShippingMethod::SERVICE_ONE_DAY,$shippingMethod3ShippingLabel,$shippingrate3);

//Associating the ShipmentMethods to the item
$item2->addShippingMethod($shippingMethod2);

//The following properties are essential for the functioning of the sample code
//These are mandatory, and without them the API will not function
//uncomment the below code and fill them with the correct values to use the properties array with the cart
//Define the missing properties below
// $propertiesAccessKey = "";
// $propertiesSecretKey = "";
// $propertiesMerchantId = "";
// $propertiesCurrencyCode = "USD";
// $propertiesWeightUnit = "LB";

// $properties = array();

// $properties['accessKey'] = $propertiesAccessKey;
// $properties['secretKey'] = $propertiesSecretKey;
// $properties['merchantId'] = $propertiesMerchantId;
// $properties['currencyCode'] = $propertiesCurrencyCode;
// $properties['weightUnit'] = $propertiesWeightUnit;

// $cart = new \AmazonPayments\Model\Cart($properties);

//To load the values from a properties file, comment the above code and use the below code

//Define the path to the properties file here.If you have followed the default instructions and put
//the downloaded cba_config.ini in the AmazonPayments directory then the following will work
$propertiesFilePath = "../cba_config.ini";

//Create a cart object by reading the properties from the path provided
$cart = new \AmazonPayments\Model\Cart($propertiesFilePath);

//Uncomment the following line to add a shipping method at the cart level. This method will be associated to an item when it doesn't have
//any shipping method
// $cart->addShippingMethod($shippingMethod2);

//Adding the items to the cart
$cart->addItem($item1);
$cart->addItem($item2);


//For debugging the Cart object uncomment the following line
// echo var_dump($cart);

//The value to be placed in the orderInput: part of the HTML snippet
echo "orderInput value:<br>".$cart->createOrderInputValue();

?>
