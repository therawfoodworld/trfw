<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */


include_once ('.config.inc.php'); 

// Defining item1 attributes
$item1SKU = "444-44-4";
$item1Title = "Product1";
$item1UnitPrice = 0.20;
$item1Quantity = 1;
$item1Description = "Sample Product1";
$item1Weight = 2.00;
$item1CurrencyCode = "USD";

// Defining item2 attributes
$item2SKU = "555-55-5";
$item2Title = "Product 2";
$item2UnitPrice = 0.40;
$item2Quantity = 1;
$item2Description = "Sample Product 2";
$item2Weight = 6.00;

//Initializing an Item object with SKU, quantity, unit price and name
$item1 = new \AmazonPayments\Model\Item($item1SKU,$item1Title,$item1UnitPrice,$item1Quantity);

//Setting the weight of the object
$item1->setWeight($item1Weight);

/*
 * Setting a property using setter method takes the highest precedence over passing the
 * property as part of the constuctor which is above the value present in the cba_config.ini file 
 */
//Setting the currency code for the item. 
$item1->setCurrencyCode($item1CurrencyCode);

//Adding the item description
$item1->setDescription($item1Description);

//For debugging Item object uncomment the following line
//echo var_dump($item1)


//Creating another Item object
$item2 = new \AmazonPayments\Model\Item($item2SKU,$item2Title,$item2UnitPrice,$item2Quantity);

$item2->setWeight($item2Weight);
$item2->setDescription($item2Description);

//Building a property associative array to pass on to Cart object 

//Define the missing properties below
$propertiesAccessKey = "";
$propertiesSecretKey = "";
$propertiesMerchantId = "";
$propertiesCurrencyCode = "USD";
$propertiesWeightUnit = "LB";
$propertiesCallbackEndpointUrl = "http://www.yourwebsite.com/ordercalculation";

//Set the $properties array with the properties below
$properties = array();

//The following properties are essential for the functioning of the sample code
//These are mandatory, and without them the API will not function

$properties['accessKey'] = $propertiesAccessKey;
$properties['secretKey'] = $propertiesSecretKey;
$properties['merchantId'] = $propertiesMerchantId;
$properties['currencyCode'] = $propertiesCurrencyCode;
$properties['weightUnit'] = $propertiesWeightUnit;

//The following properties are optional and can be set based on requirement

$properties['callbackEndpointURL'] = $propertiesCallbackEndpointUrl;
$properties['calculateTaxRates'] = false;
$properties['calculatePromotions'] = false;
$properties['calculateShippingRates'] = false;
$properties['processOrderOnCallbackFailure'] = true;
// $properties['integratorId'] = "Test_Id";
// $properties['integratorName'] = "Test Id";
// $properties['returnURL'] = "";
// $properties['cancelURL'] = "";

/*
 * Pass the properties array with the values to Cart constructor,  
 * Note: If you are using this, ensure that the properties that are marked as essential in the above section are
 * all defined, before exectuing the sample code
 */ 
// $cart = new \AmazonPayments\Model\Cart($properties);


/*
 * Alternatively, you can also pass the path of the cba_config.ini (downloaded along with the sample code)
 * or your own properties file, such that the properties can be read directly from the file. 
 * Uncomment the below lines, and define the $propertiesFilePath to the properties file
 * Note: The path provides should be accessible, so take care of the required permissions
 */
$propertiesFilePath = "../cba_config.ini";
$cart = new \AmazonPayments\Model\Cart($propertiesFilePath);

//Adding the items to the cart object
$cart->addItem($item1);
$cart->addItem($item2);

//For debugging the Cart object uncomment the following line
// echo var_dump($cart);

//The value to be placed in the orderInput: part of the HTML snippet
echo "orderInput value:<br>".$cart->createOrderInputValue();

?>