<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */

include_once ('.config.inc.php'); 

// Defining item1 attributes
$item1SKU = "444-44-4";
$item1Title = "Product1";
$item1UnitPrice = 0.20;
$item1Quantity = 1;
$item1Description = "Sample Product1";
$item1Weight = 2.00;
$item1PromotionAmount = 0.20;
$item1PromotionId = "promo1";

// Defining item2 attributes
$item2SKU = "555-55-5";
$item2Title = "Product 2";
$item2UnitPrice = 0.40;
$item2Quantity = 1;
$item2Description = "Sample Product 2";
$item2Weight = 6.00;


//First Item
//Initializing an Item object with SKU, quantity, unit price and name
$item1 = new \AmazonPayments\Model\Item($item1SKU,$item1Title,$item1UnitPrice,$item1Quantity);

$item1->setWeight($item1Weight);
$item1->setDescription($item1Description);

//Add promotion for item1
$item1->setPromotion($item1PromotionAmount, $item1PromotionId);


//Second Item
$item2 = new \AmazonPayments\Model\Item($item2SKU,$item2Title,$item2UnitPrice,$item2Quantity);

$item2->setWeight($item2Weight);
$item2->setDescription($item2Description);

//The following properties are essential for the functioning of the sample code
//These are mandatory, and without them the API will not function
//uncomment the below code and fill them with the correct values to use the properties array with the cart
//Define the missing properties below
// $propertiesAccessKey = "";
// $propertiesSecretKey = "";
// $propertiesMerchantId = "";
// $propertiesCurrencyCode = "USD";
// $propertiesWeightUnit = "LB";

// $properties = array();

// $properties['accessKey'] = $propertiesAccessKey;
// $properties['secretKey'] = $propertiesSecretKey;
// $properties['merchantId'] = $propertiesMerchantId;
// $properties['currencyCode'] = $propertiesCurrencyCode;
// $properties['weightUnit'] = $propertiesWeightUnit;

// $cart = new \AmazonPayments\Model\Cart($properties);

//To load the values from a properties file, comment the above code and use the below code

//Define the path to the properties file here.If you have followed the default instructions and put
//the downloaded cba_config.ini in the AmazonPayments directory then the following will work
$propertiesFilePath = "../cba_config.ini";

//Create a cart object by reading the properties from the path provided
$cart = new \AmazonPayments\Model\Cart($propertiesFilePath);

//Adding the items to the cart
$cart->addItem($item1);
$cart->addItem($item2);

//Uncomment the following lines for setting up cart level promotion, and comment the item level promotion sections above
// $cartDiscount = 0.10;
// $cartPromoId = "cartpromo";
// $cart->setPromotion($cartDiscount, $cartPromoId);

//For debugging the Cart object uncomment the following line
// echo var_dump($cart);

//The value to be placed in the orderInput: part of the HTML snippet
echo "orderInput value:<br>".$cart->createOrderInputValue();

?>