<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/

namespace AmazonPayments\Model;

class Cart
{
	const CALLBACK_CALCULATE_PROMOTIONS     = 0;  //!< if to calculate promotions
	const CALLBACK_CALCULATE_SHIPPING       = 1;  //!< if to calculate shipping
	const CALLBACK_CALCULATE_TAXES          = 2;  //!< if to calculate taxes
	const CALLBACK_PROCESS_ORDER_ON_FAILURE = 3;  //!< if to process orders on failure

	private $callbacks;

	private $items = array();

	private $taxTable;

	private $shippingMethods = array();

	private $promotionAmount;

	private $promotionId;

	private $cartCustomData = "";

	private $properties;

	/**
	 * Constructor for the Cart, which takes the properties defined in an associated array
	 * or the path to the properties file
	 *
	 */
	public function __construct($properties)
	{
		//Initializing properties array
		$this->properties = array();


		if (!is_null($properties))
		{
			//Checking if a properties associative array is passed in the constructor
			if (is_array($properties))
			{
				//Using the properties array passed in the constructor
				$this->properties = $properties;
			}
			//Reading the properties from the file at the given $properties path (will fire if the given arg is not an array
			else if ($properties != "")
			{
				if (file_exists($properties))
				{
					$this->properties = parse_ini_file($properties);
				}
				else //Exiting the program with an error message,as cba_config.ini is not found or in invalid format
				{
					trigger_error("Could not read Properties file. propertiesPath:"
							. $properties . ". Please ensure the file exists and can be read.",E_USER_ERROR);
				}
			}
		}
		else
		{
			trigger_error("Either a properties array or the path to the properties file must be passed in the constructor!",E_USER_ERROR);
		}

		$this->validateProperties();
	}

	/**
	 * Return the value of the given propertyName
	 */
	private function getProperty($propertyName)
	{
		if (array_key_exists($propertyName, $this->properties))
		{
			return $this->properties[$propertyName];
		}
		else
		{
			return null;
		}
	}

	/**
	 * Method to add an item to the cart
	 *
	 * @param $item an object of the \AmazonPayments\Model\Item class
	 */
	public function addItem($item)
	{
		if (is_object($item) && $item instanceof \AmazonPayments\Model\Item)
		{
			array_push($this->items,$item);
		}
		else
		{
			trigger_error("Only objects of \AmazonPayments\Model\Item should be passed to addItem.",E_USER_ERROR);
		}
	}

	/**
	 * Method to add a promotion to the cart
	 */
	public function setPromotion($promotionAmount,$promotionId)
	{
		if (is_double($promotionAmount) && $this->validateStringDataType("PromotionId",$promotionId))
		{
			$this->promotionAmount = $promotionAmount;
			$this->promotionId = $promotionId;
		}
		else
		{
			trigger_error("PromotionAmount should be a double.",E_USER_ERROR);
		}
	}

	/**
	 * Method to add a shipping method to the cart
	 */
	public function addShippingMethod($shippingMethod)
	{
		if (is_object($shippingMethod) && $shippingMethod instanceof \AmazonPayments\Model\ShippingMethod)
		{
			array_push($this->shippingMethods,$shippingMethod);
		}
		else
		{
			trigger_error("Only objects of \AmazonPayments\Model\ShippingMethod should be passed to addShippingMethod.",E_USER_ERROR);
		}
	}

	/**
	 * Method to set the tax table to the cart
	 */
	public function setTaxTable($taxTable)
	{
		if (is_object($taxTable) && $taxTable instanceof \AmazonPayments\Model\TaxTable)
		{
			$this->taxTable = $taxTable;
		}
		else
		{
			trigger_error("Only objects of \AmazonPayments\Model\TaxTable should be passed to setTaxTable.",E_USER_ERROR);
		}
	}

	/**
	 *Method to set the custom XML that needs to be added to the cart
	 */
	public function setCartCustomData($cartCustomData)
	{
		if ($this->validateStringDataType("CartCustomData",$cartCustomData))
		{
			$this->cartCustomData = $cartCustomData;
		}
	}

	/**
	 * Method to set a specific callback value
	 */
	public function setCallback($callbackID,$value)
	{
		if (is_bool($value))
		{
			switch($callbackID)
			{
				case Cart::CALLBACK_CALCULATE_TAXES:
					$this->properties[\AmazonPayments\Model\OptionalProperties::calculateTaxRates] = $value;
					break;
				case Cart::CALLBACK_CALCULATE_PROMOTIONS:
					$this->properties[\AmazonPayments\Model\OptionalProperties::calculatePromotions] = $value;
					break;
				case Cart::CALLBACK_CALCULATE_SHIPPING:
					$this->properties[\AmazonPayments\Model\OptionalProperties::calculateShippingRates] = $value;
					break;
				case Cart::CALLBACK_PROCESS_ORDER_ON_FAILURE:
					$this->properties[\AmazonPayments\Model\OptionalProperties::processOrderOnCallbackFailure] = $value;
					break;
				default:
					trigger_error("Unknown Callback code.",E_USER_ERROR);
			}
		}
		else
		{
			trigger_error("Callback value can only be set to a boolean.",E_USER_ERROR);
		}

	}

	public function setMerchantId($merchantId)
	{
		$this->setMandatoryProperty(\AmazonPayments\Model\MandatoryProperties::merchantId, $merchantId);
	}

	public function setCurrencyCode($currencyCode)
	{
		$this->setMandatoryProperty(\AmazonPayments\Model\MandatoryProperties::currencyCode, $currencyCode);
	}

	public function setWeightUnit($weigthUnit)
	{
		$this->setMandatoryProperty(\AmazonPayments\Model\MandatoryProperties::weightUnit, $weigthUnit);
	}

	public function setAccessKey($accessKey)
	{
		$this->setMandatoryProperty(\AmazonPayments\Model\MandatoryProperties::accessKey,$accessKey);
	}

	public function setSecretKey($secretKey)
	{
		$this->setMandatoryProperty(\AmazonPayments\Model\MandatoryProperties::secretKey,$secretKey);
	}

	public function setCallbackEndPointURL($callbackEndpointURL)
	{
		if ($this->validateStringDataType("CallbackEndpointURL",$callbackEndpointURL))
		{
			$this->properties[\AmazonPayments\Model\OptionalProperties::callbackEndpointURL] = $callbackEndpointURL;
		}

	}

	public function setIntegratorId($integratorId)
	{
		if ($this->validateStringDataType("IntegratorId", $integratorId))
		{
			$this->properties[\AmazonPayments\Model\OptionalProperties::integratorId] = $integratorId;
		}
	}

	public function setIntegratorName($integratorName)
	{
		if ($this->validateStringDataType("IntegratorName", $integratorName)) 
		{
			$this->properties[\AmazonPayments\Model\OptionalProperties::integratorName] = $integratorName;
		}
	}

	public function setReturnURL($returnURL)
	{
		if ($this->validateStringDataType("ReturnURL", $returnURL)) 
		{
			$this->properties[\AmazonPayments\Model\OptionalProperties::returnURL] = $returnURL;
		}
	}

	public function setCancelURL($cancelURL)
	{
		if ($this->validateStringDataType("CancelURL", $cancelURL)) 
		{
			$this->properties[\AmazonPayments\Model\OptionalProperties::cancelURL] = $cancelURL;
		}
	}

	private function toXML($dom)
	{
		$this->validateOptionalProperties();

		$orderXML = $dom->createElement("Order");
		$orderXML->setAttribute("xmlns","http://payments.amazon.com/checkout/2009-05-15/");

		//Arrays for item Tax Table, Shipping Tables and Promotions
		$itemTaxTables =  array();
		$itemShippingMethods = array();
		$itemPromotions = array();

		$cart = $dom->createElement("Cart");

		$itemsTag = $dom->createElement("Items");


			
		foreach ($this->items as $item)
		{


			//Storing the Item's tax table for generating the XML later
			if (!is_null($item->getTaxTable()))
			{
				//Checking if the TaxTable has already been added
				if (!in_array($item->getTaxTable(), $itemTaxTables))
				{
					array_push($itemTaxTables,$item->getTaxTable());
				}
			}

			//Storing the Item's shipping tables for generating XML later
			if (!is_null($item->getShippingMethods()) && sizeof($item->getShippingMethods()) > 0)
			{
				//Checking if the ShippingMethod has already been added
				foreach ($item->getShippingMethods() as $shippingMethod)
				{
					if (!in_array($shippingMethod, $itemShippingMethods))
					{
						array_push($itemShippingMethods,$shippingMethod);
					}
				}
			}
			//In case the item does not have a shippingMethods associated with it,
			//then adding those shippingMethodIds defined at Cart level (if exists) to the Item XML
			else if (sizeof($this->shippingMethods) > 0)
			{
				foreach ($this->shippingMethods as $shippingMethod)
				{
					$item->addShippingMethod($shippingMethod);
				}
			}


			//Storing the Item's promotions for generating XML later
			if (!is_null($item->getPromotion()))
			{
				//Checking if the Promotion has already been added
				if (!in_array($item->getPromotion(), $itemPromotions))
				{
					array_push($itemPromotions,$item->getPromotion());
				}
			}

			//Adding the $item's XML
			$itemsTag->appendChild($dom->importNode($item->toXML($this->properties,$dom),true));
		}

		$cart->appendChild($itemsTag);
		$orderXML->appendChild($cart);

		//Creating TaxTables XML
		//If a TaxTable is defined at the cart level, assuming it to be the DefaultTaxTable to be used if no TaxTable is associated with an item
		if (!is_null($this->taxTable))
		{
			$orderXML->appendChild($dom->importNode($this->taxTable->toXML(true,$dom),true));
		}

		if (sizeof($itemTaxTables) > 0)
		{
			$taxTables = $dom->createElement("TaxTables");

			foreach ($itemTaxTables as $itemTaxTable)
			{
				$taxTables->appendChild($dom->importNode($itemTaxTable->toXML(false,$dom),true));
			}
			$orderXML->appendChild($taxTables);
		}

		//Creating the Promotions XML
		if (sizeof($itemPromotions) > 0 || !is_null($this->promotionId))
		{
			$promotions = $dom->createElement("Promotions");

			//Checking if any Item level promotions were defined and creating the XML
			if (sizeof($itemPromotions) > 0)
			{
				foreach ($itemPromotions as $promotion)
				{
					$promotions->appendChild($dom->importNode($promotion->toXML($this->properties,$dom),true));
				}
			}

			//Checking if cart level promotion is defined and creating the XML
			if (!is_null($this->promotionId))
			{
				$cart->appendChild($dom->createElement("CartPromotionId",$this->promotionId));

				$promotion = new \AmazonPayments\Model\Promotion($this->promotionAmount, $this->promotionId);

				//Adding XML only if the Promotion has not been added before
				if (!in_array($promotion,$itemPromotions))
				{
					$promotions->appendChild($dom->importNode($promotion->toXML($this->properties,$dom),true));
				}
					
			}
			$orderXML->appendChild($promotions);
		}

		//Creating ShippingMethods XML
		if (sizeof($itemShippingMethods) > 0 || sizeof($this->shippingMethods) > 0)
		{
			$shippingMethods = $dom->createElement("ShippingMethods");

			//Checking if any Item level shippingMethods were defined and creating the XML
			if (sizeof($itemShippingMethods) > 0)
			{
				foreach ($itemShippingMethods as $shippingMethod)
				{
					$shippingMethods->appendChild($dom->importNode($shippingMethod->toXML($this->properties),true));
				}
			}

			//Checking if any shippingmethods are defined at cart level and creating the XML
			if (sizeof($this->shippingMethods) > 0)
			{
				foreach ($this->shippingMethods as $shippingMethod)
				{
					//Adding the XML only if the ShippingMethodId has not been added before
					if (!in_array($shippingMethod, $itemShippingMethods))
					{
						$shippingMethods->appendChild($dom->importNode($shippingMethod->toXML($this->properties),true));
					}

				}
			}

			$orderXML->appendChild($shippingMethods);
		}

		if (!is_null($this->getProperty('integratorId')) && $this->getProperty('integratorId') != "")
		{
			$orderXML->appendChild($dom->createElement("IntegratorId",$this->getProperty('integratorId')));
		}

		if (!is_null($this->getProperty('integratorName')) && $this->getProperty('integratorName') != "")
		{
			$orderXML->appendChild($dom->createElement("IntegratorName",$this->getProperty('integratorName')));
		}

		if (!is_null($this->getProperty('returnURL')) && $this->getProperty('returnURL') != "")
		{
			$orderXML->appendChild($dom->createElement("ReturnUrl",$this->getProperty('returnURL')));
		}

		if (!is_null($this->getProperty('cancelURL')) && $this->getProperty('cancelURL') != "")
		{
			$orderXML->appendChild($dom->createElement("CancelUrl",$this->getProperty('cancelURL')));
		}

		//Setting Callback related XML, if and only if the callbackEndpointURL is set
		if (array_key_exists('callbackEndpointURL',$this->properties) && $this->getProperty('callbackEndpointURL') != "")
		{
			$callbacksTag = $dom->createElement("OrderCalculationCallbacks");

			$callbacksTag->appendChild($dom->createElement("CalculateTaxRates",
					($this->getProperty(\AmazonPayments\Model\OptionalProperties::calculateTaxRates)?"true":"false")));
			$callbacksTag->appendChild($dom->createElement("CalculatePromotions",
					($this->getProperty(\AmazonPayments\Model\OptionalProperties::calculatePromotions)?"true":"false")));
			$callbacksTag->appendChild($dom->createElement("CalculateShippingRates",
					($this->getProperty(\AmazonPayments\Model\OptionalProperties::calculateShippingRates)?"true":"false")));
			$callbacksTag->appendChild($dom->createElement("OrderCallbackEndpoint",
					$this->getProperty(\AmazonPayments\Model\OptionalProperties::callbackEndpointURL)));
			$callbacksTag->appendChild($dom->createElement("ProcessOrderOnCallbackFailure",
					($this->getProperty(\AmazonPayments\Model\OptionalProperties::processOrderOnCallbackFailure)?"true":"false")));

			$orderXML->appendChild($callbacksTag);
		}


		if($this->cartCustomData != "")
		{
			$childDoc = new \DOMDocument();
			$childDoc->preserveWhiteSpace = true;
			$childDoc->loadXML($this->cartCustomData);
			$newNode = $dom->importNode($childDoc->documentElement,true);

			$cartCustomData = $dom->createElement("CartCustomData");
			$cartCustomData->appendChild($newNode);
			$cart->appendChild($cartCustomData);
		}

		return $orderXML;
	}

	//Function returns the signature of the Cart to be used in the "signature:" field of "orderInput:" in the HTML/Javascript snippet
	private function signCart($orderXML)
	{
		$calculator = new \AmazonPayments\Signature\SignatureCalculator();
		return $calculator->calculateRFC2104HMAC($orderXML, $this->getProperty('secretKey'));
	}

	//Function returns the Base 64 Encoded Cart value to be used in the "order:" field of "orderInput:" in the HTML/Javascript snippet
	private function encodeCart($orderXML)
	{
		return base64_encode($orderXML);
	}


	//Function returns the value to be used in orderInput of the HTML/JS snippet
	public function createOrderInputValue()
	{
		//DomDocument for generating XML
		$dom = new \DOMDocument("1.0","utf-8");
		$dom->formatOutput = true;

		$orderXML = $dom->saveXML($this->toXML($dom));
		$orderInput = "type:merchant-signed-order/aws-accesskey/1;order:".$this->encodeCart($orderXML).";".
				"signature:".$this->signCart($orderXML) .";aws-access-key-id:". $this->getProperty('accessKey');
			
		return $orderInput;
	}

	/**
	 * Function validates the properties
	 */
	private function validateProperties()
	{
		$this->validateMandatoryProperties();
		$this->validateOptionalProperties();
	}

	/**
	 * Validates mandatory properties
	 */
	private function validateMandatoryProperties()
	{
		foreach (\AmazonPayments\Model\MandatoryProperties::values() as $mandatoryProperty)
		{
			if (array_key_exists($mandatoryProperty, $this->properties))
			{
				$propertyValue = $this->properties[$mandatoryProperty];
				$this->validateMandatoryProperty($mandatoryProperty,$propertyValue);
			}
			else
			{
				trigger_error("Mandatory property is missing. PropertyName: " . $mandatoryProperty,E_USER_ERROR);
			}
		}
	}

	/**
	 * Validates a mandatory property for the given property value
	 */
	private function validateMandatoryProperty($mandatoryProperty, $propertyValue)
	{
		if (is_null($propertyValue) || $propertyValue == "")
		{
			trigger_error("Mandatory property is missing or empty. PropertyName: " . $mandatoryProperty,E_USER_ERROR);
		}
	}

	/**
	 * Validates optional properties
	 */
	private function validateOptionalProperties()
	{
		foreach (\AmazonPayments\Model\OptionalProperties::values() as $optionalProperty)
		{
			if (array_key_exists($optionalProperty, $this->properties))
			{
				$propertyValue = $this->properties[$optionalProperty];
				$this->validateOptionalProperty($optionalProperty,$propertyValue);
			}
		}
	}

	/**
	 * Validate optional property checking for the properties which need to be boolean
	 */
	private function validateOptionalProperty($optionalProperty,$propertyValue)
	{
		//Checking if the optional property should be a boolean
		if (in_array($optionalProperty,\AmazonPayments\Model\OptionalProperties::booleanProperties())
				&& !is_null($propertyValue) && $propertyValue != "")
		{
			if ($propertyValue != 1 && $propertyValue != 0)
			{
				trigger_error("Invalid boolean value for property. PropertyName: " . $optionalProperty
						. " PropertyValue: " . $propertyValue, E_USER_ERROR);
			}
		}
	}

	/**
	 * Function to validate string data type
	 */
	private function validateStringDataType($parameterName,$data)
	{
		if (is_string($data) && !is_null($data) && $data != "")
		{
			return true;
		}
		else
		{
			trigger_error($parameterName . " should be a string and not be empty!",E_USER_ERROR);
		}
	}

	/**
	 * Sets the mandatory property
	 */
	private function setMandatoryProperty($mandatoryProperty, $propertyValue)
	{
		$this->validateMandatoryProperty($mandatoryProperty, $propertyValue);
		$this->properties[$mandatoryProperty] = $propertyValue;
	}
}


?>
