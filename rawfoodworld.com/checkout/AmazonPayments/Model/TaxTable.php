<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */

 namespace AmazonPayments\Model;

 class TaxTable
 {
 	private $taxTableId = "";

 	private $usBaseRate = 0.00;

	private $nonUSBaseRate = 0.00;
	
	private $usStateTaxRates = array();
	
	private $usZipCodeTaxRates = array();
	
	
	public function __construct($usBaseRate, $nonUSBaseRate)
	{
		//generating a unique id for use as TaxTableId
		$this->taxTableId = uniqid("tax_table_");
		$this->usBaseRate = $usBaseRate;
		$this->nonUSBaseRate = $nonUSBaseRate;
	}
	
	public function setUSStateTaxRate($stateCode, $rate, $isShippingTaxed = true)
	{
		$taxRule = new \AmazonPayments\Model\TaxRule($rate);
		$taxRule->setUSStateRegion($stateCode);
		$taxRule->setIsShippingTaxed($isShippingTaxed);		
		array_push($this->usStateTaxRates,$taxRule);
	}
	
	public function setUSZipCodeTaxRate($usZipCode, $rate, $isShippingTaxed = true)
	{
		$taxRule = new \AmazonPayments\Model\TaxRule($rate);
		$taxRule->setUSZipRegion($usZipCode);
		$taxRule->setIsShippingTaxed($isShippingTaxed);
		array_push($this->usZipCodeTaxRates,$taxRule);
		
	}
	
	public function getTaxTableId()
	{
		return $this->taxTableId;
	}
	
	/*
	 * Pass true (optional) if you want the DOM element is to be returned as DefaultTaxTable.
	 * Else, do not pass anything if the DOM Element to be returned is TaxTable
	 */
	public function toXML($isDefaultTaxTable= false,$dom)
	{
		if ($isDefaultTaxTable)
		{
			$taxTableXML = $dom->createElement("DefaultTaxTable");
		}
		else
		{
			$taxTableXML = $dom->createElement("TaxTable");
		}
		
		
		$taxTableXML->appendChild($dom->createElement("TaxTableId",$this->taxTableId));
		
		$taxRules = $dom->createElement("TaxRules");
		$taxTableXML->appendChild($taxRules);
		
		if ($this->usBaseRate != 0.00)
		{
			//Creating an USAll TaxRule Object and appending the DOMElement returned by toXML() method
			$usBaseRule = new \AmazonPayments\Model\TaxRule($this->usBaseRate);
			$usBaseRule->setPredefinedRegion(\AmazonPayments\Model\TaxRule::REGION_US_ALL);
			$taxRules->appendChild($dom->importNode($usBaseRule->toXML($dom),true));
		}
		
	    if ($this->nonUSBaseRate != 0.00)
		{
			//Creating an WorldAll TaxRule Object and appending the DOMElement returned by toXML() method
			$nonUSBaseRule = new \AmazonPayments\Model\TaxRule($this->nonUSBaseRate);
			$nonUSBaseRule->setPredefinedRegion(\AmazonPayments\Model\TaxRule::REGION_WORLD_ALL);
			$taxRules->appendChild($dom->importNode($nonUSBaseRule->toXML($dom),true));
		}
		
		foreach($this->usStateTaxRates as $stateTaxRule )
		{
			$taxRules->appendChild($dom->importNode($stateTaxRule->toXML($dom),true));
		}
		
		foreach($this->usZipCodeTaxRates as $zipCodeTaxRule )
		{
			$taxRules->appendChild($dom->importNode($zipCodeTaxRule->toXML($dom),true));
		}
		
		return $taxTableXML;
	}
	
 }
 
 
?>
