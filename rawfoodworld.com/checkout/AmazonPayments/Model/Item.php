<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/


namespace AmazonPayments\Model;

class Item
{

	const FULFILLED_BY_MERCHANT = "MERCHANT";
	const FULFILLED_BY_AMAZON_NA = "AMAZON_NA";
	const FULFILLED_BY_AMAZON = "AMAZON";

	private $SKU = "";

	private $title = "";

	private $description = "";

	private $quantity;

	private $weight;

	private $price;

	private $taxTable;

	private $shippingMethods = array();

	private $customData = "";

	private $fulfillmentNetwork;

	private $currencyCode;

	private $weightUnit;

	private $promotionId;

	private $promotionAmount;

	public function __construct($SKU, $title, $unitPrice, $quantity)
	{
		$this->SKU = $SKU;
		$this->title = $title;
		$this->price = $unitPrice;
		$this->quantity = $quantity;
	}

	public function setCurrencyCode($currencyCode)
	{
		if ($this->validateStringDataType("CurrencyCode", $currencyCode))
		{
			$this->currencyCode = $currencyCode;
		}
	}

	public function setWeightUnit($weightUnit)
	{
		if ($this->validateStringDataType("WeightUnit", $weightUnit))
		{
			$this->weightUnit = $weightUnit;
		}
	}

	//Function to return the currencyCode. The value set through the setter method of the object is returned, if
	//it has not been set using setter method than the value from the properties file is returned
	private function getCurrencyCode($properties)
	{
		return !is_null($this->currencyCode) ? $this->currencyCode : $properties["currencyCode"];
	}

	//Function to return the weightUnit. The value set through the setter method of the object is returned,and  if
	//it has not been set using setter method than the value from the properties file is returned
	private function getWeightUnit($properties)
	{
		return !is_null($this->weightUnit) ? $this->weightUnit : $properties["weightUnit"];
	}

	public function setDescription($description)
	{
		if ($this->validateStringDataType("Description", $description))
		{
			$this->description = $description;
		}
	}

	public function setWeight($weight)
	{
		if (is_double($weight))
		{
			$this->weight = $weight;
		}
		else
		{
			trigger_error("Weight should be a double.", E_USER_ERROR);
		}
	}

	public function setPromotion($promotionAmount,$promotionId)
	{
		if ($this->validateStringDataType("PromotionId", $promotionId) && is_double($promotionAmount))
		{
			$this->promotionAmount = $promotionAmount;
			$this->promotionId = $promotionId;
		}
		else
		{
			trigger_error("Promotion Amount should be a double.", E_USER_ERROR);
		}
	}

	public function getPromotion()
	{
		if (!is_null($this->promotionId))
		{
			return new \AmazonPayments\Model\Promotion($this->promotionAmount,$this->promotionId);
		}
		else
		{
			return null;
		}
	}

	public function setTaxTable($taxTable)
	{
		if (is_object($taxTable) && $taxTable instanceof \AmazonPayments\Model\TaxTable)
		{
			$this->taxTable = $taxTable;
		}
		else
		{
			trigger_error("Only objects of \AmazonPayments\Model\TaxTable should be passed to setTaxTable.",E_USER_ERROR);
		}
	}

	public function getTaxTable()
	{
		return $this->taxTable;
	}

	public function addShippingMethod($shippingMethod)
	{
		if (is_object($shippingMethod) && $shippingMethod instanceof \AmazonPayments\Model\ShippingMethod)
		{
			array_push($this->shippingMethods,$shippingMethod);
		}
		else
		{
			trigger_error("Only objects of \AmazonPayments\Model\ShippingMethod should be passed to addShippingMethod.",E_USER_ERROR);
		}
	}

	public function getShippingMethods()
	{
		return $this->shippingMethods;
	}


	public function setCustomData($customData)
	{
		if ($this->validateStringDataType("CustomData", $customData))
		{
			$this->customData = $customData;
		}
	}

	public function setFulfillmentMethod($fulfillmentNetwork)
	{
		if ($this->validateFulfillmentNetwork($fulfillmentNetwork))
		{
			$this->fulfillmentNetwork = $fulfillmentNetwork;
		}
	}

	public function toXML($properties,$dom)
	{

		$itemXML = $dom->createElement("Item");

		$itemXML->appendChild($dom->createElement("SKU",$this->SKU));

		$itemXML->appendChild($dom->createElement("MerchantId",$properties['merchantId']));

		$itemXML->appendChild($dom->createElement("Title",$this->title));

		$itemXML->appendChild($dom->createElement("Description",$this->description));

		$priceTag = new \AmazonPayments\Model\Price($this->price);

		$itemXML->appendChild($dom->importNode($priceTag->toXML("Price",$properties,$dom),true));

		$itemXML->appendChild($dom->createElement("Quantity",$this->quantity));

		if (!is_null($this->weight))
		{
			$weightTag = new \AmazonPayments\Model\Weight($this->weight,$this->getWeightUnit($properties));
			$itemXML->appendChild($dom->importNode($weightTag->toXML($dom),true));
		}

		if (!is_null($this->taxTable))
		{
			$itemXML->appendChild($dom->createElement("TaxTableId",$this->taxTable->getTaxTableId()));
		}

		if (!is_null($this->promotionId))
		{
			$promotionIds  = $dom->createElement("PromotionIds");
			$promotionIds->appendChild($dom->createElement("PromotionId",$this->promotionId));
			$itemXML->appendChild($promotionIds);
		}

		if(sizeof($this->shippingMethods) > 0)
		{
			$shippingMethodIds  = $dom->createElement("ShippingMethodIds");
			foreach ($this->shippingMethods as $shippingMethod)
				$shippingMethodIds->appendChild($dom->createElement("ShippingMethodId",$shippingMethod->getShippingMethodId()));
			$itemXML->appendChild($shippingMethodIds);
		}

		if (!is_null($this->fulfillmentNetwork))
		{
			$itemXML->appendChild($dom->createElement("FulfillmentNetwork",$this->fulfillmentNetwork));
		}

		if($this->customData != "")
		{
			$childDoc = new \DOMDocument();
			$childDoc->preserveWhiteSpace = true;
			$childDoc->loadXML($this->customData);

			$newNode = $dom->importNode($childDoc->documentElement,true);
			$itemCustomData = $dom->createElement("ItemCustomData");
			$itemCustomData->appendChild($newNode);
			$itemXML->appendChild($itemCustomData);
		}

		return $itemXML;
	}

	private function validateFulfillmentNetwork($fulfillmentNetwork)
	{
		if ($this->validateStringDataType("FulfillmentNetwork", $fulfillmentNetwork))
		{
			switch ($fulfillmentNetwork){
				case Item::FULFILLED_BY_MERCHANT:
				case Item::FULFILLED_BY_AMAZON_NA:
				case Item::FULFILLED_BY_AMAZON:
					return true;
				default:
					trigger_error("Invalid FulfillmentNetwork value, use the constants provided in \AmazonPayments\Model\Item class to avoid errors.", E_USER_ERROR);
			}
		}
	}

	/**
	 * Function to validate string data type
	 */
	private function validateStringDataType($parameterName,$data)
	{
		if (is_string($data) && !is_null($data) && $data != "")
		{
			return true;
		}
		else
		{
			trigger_error($parameterName . " should be a string and not be empty!",E_USER_ERROR);
		}
	}
}

?>
