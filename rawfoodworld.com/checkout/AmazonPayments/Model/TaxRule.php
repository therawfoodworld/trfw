<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */

 namespace AmazonPayments\Model;
 
 class TaxRule
 {
 	
	const REGION_US_CONTINENTAL_48_STATES = "USContinental48States";
    const REGION_US_FULL_50_STATES = "USFull50States";
    const REGION_US_ALL = "USAll";
    const REGION_WORLD_ALL = "WorldAll";
     	
	private $rate;
	
	private $isShippingTaxed;
	
	private $usStateRegion;
	
	private $predefinedRegion;
	
	private $usZipRegion;
	
	public function __construct($rate)
	{
		$this->rate = $rate;
		
		$this->isShippingTaxed = false;
	}
	
	public function setRate($rate)
	{
		$this->rate = $rate;
	}
	
	public function setPredefinedRegion($predefinedRegion)
	{
		if ($this->validateRegion($predefinedRegion))
		{
			$this->predefinedRegion = $predefinedRegion;
		}
	}
	
	public function setUSStateRegion($usStateCodeRegion)
	{
		$this->usStateRegion = $usStateCodeRegion;
	}
	
	public function setUSZipRegion($usZipRegion)
	{
		$this->usZipRegion = $usZipRegion;
	}
	
	public function setIsShippingTaxed($isShippingTaxed)
	{
		if (is_bool($isShippingTaxed))
		{
			$this->isShippingTaxed = $isShippingTaxed;
		}
		else
		{
			trigger_error("isShippingTaxed should be boolean.", E_USER_ERROR);
		}
	}
	
	public function toXML($dom)
	{
		$taxRule = $dom->createElement("TaxRule");
		$taxRule->appendChild($dom->createElement("Rate",$this->rate));
		
		if(!is_null($this->isShippingTaxed))
		{
			$isShippingTaxedValue = $this->isShippingTaxed?"true":"false";
			$taxRule->appendChild($dom->createElement("IsShippingTaxed",$isShippingTaxedValue));
		}
		
		if(!is_null($this->predefinedRegion))
		{
				$taxRule->appendChild($dom->createElement("PredefinedRegion",$this->predefinedRegion));
		}
		else if(!is_null($this->usStateRegion))
		{
			$taxRule->appendChild($dom->createElement("USStateRegion",$this->usStateRegion));
		}
		else if(!is_null($this->usZipRegion))
		{
			$taxRule->appendChild($dom->createElement("USZipRegion",$this->usZipRegion));
		}
		else
		{
			trigger_error("Either PredefinedRegion,USStateRegion or USZipRegion needs to be set for the TaxRule!",E_USER_ERROR);
		}
		
		return $taxRule;
	}
	
	private static function validateRegion($predefinedRegion)
	{
		switch ($predefinedRegion){
			case \AmazonPayments\Model\TaxRule::REGION_US_CONTINENTAL_48_STATES:
			case \AmazonPayments\Model\TaxRule::REGION_US_FULL_50_STATES:
			case \AmazonPayments\Model\TaxRule::REGION_US_ALL:
			case \AmazonPayments\Model\TaxRule::REGION_WORLD_ALL:
				return true;
			default:
				trigger_error("Invalid Region value given. Use the REGION constants in the \AmazonPayments\Model\TaxRule class.", E_USER_ERROR);
		}
			
	}
 }
 
?>