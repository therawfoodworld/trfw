<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/


namespace AmazonPayments\Model;

final class OptionalProperties
{
	const callbackEndpointURL = "callbackEndpointURL";
	const calculatePromotions = "calculatePromotions";
	const calculateShippingRates = "calculateShippingRates";
	const calculateTaxRates = "calculateTaxRates";
	const processOrderOnCallbackFailure = "processOrderOnCallbackFailure";
	const integratorId = "integratorId";
	const integratorName = "integratorName";
	const returnURL = "returnURL";
	const cancelURL = "cancelURL";
	
	public static function values()
	{
		return array("callbackEndpointURL",
				"calculatePromotions",
				"calculateShippingRates",
				"calculateTaxRates",
				"processOrderOnCallbackFailure",
				"integratorId",
				"integratorName",
				"returnURL",
				"cancelURL");
	}
	
	public static function booleanProperties()
	{
		return array("calculatePromotions",
				"calculateShippingRates",
				"calculateTaxRates",
				"processOrderOnCallbackFailure");
	}
}
?>