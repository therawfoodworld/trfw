<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/******************************************************************************* 
 * 
 */

 namespace AmazonPayments\Model;
 
 class Weight
 {
 	private $weight;
	
	private $weightUnit;
	
	public function __construct($weight,$weightUnit)
	{
		if (is_double($weight))
		{
			$this->weight = $weight;
		}
		else
		{
			trigger_error("Weight should be double.",E_USER_ERROR);
		}
		
		if (is_string($weightUnit) && !is_null($weightUnit) && $weightUnit != "")
		{
			$this->weightUnit = $weightUnit;
		}
		else
		{
			trigger_error("WeightUnit should be a string and not empty.",E_USER_ERROR);
		}
	}	
	
	public function toXML($dom)
	{
		$itemWeight = $dom->createElement("Weight");
		$itemWeight->appendChild($dom->createElement("Amount",$this->weight));
		$itemWeight->appendChild($dom->createElement("Unit",$this->weightUnit));
		
		return $itemWeight;
	}
	
 }
 
 
?>