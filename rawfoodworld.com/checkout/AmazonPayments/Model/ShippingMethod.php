<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/

namespace AmazonPayments\Model;

class ShippingMethod
{
	//Shipping Service levels
	const SERVICE_EXPEDITED = "Expedited";
	const SERVICE_ONE_DAY = "OneDay";
	const SERVICE_TWO_DAY = "TwoDay";
	const SERVICE_STANDARD = "Standard";

	//Shipping Predefined regions
	const REGION_US_CONTINENTAL_48_STATES = "USContinental48States";
	const REGION_US_FULL_50_STATES = "USFull50States";
	const REGION_US_ALL = "USAll";
	const REGION_WORLD_ALL = "WorldAll";
	const REGION_CUSTOM = "Custom";

	private $shippingMethodId;

	private $regionId;

	private $shippingLabel;

	private $shippingLevelId;

	private $shippingRate;

	private $isPOBoxSupported = true;

	private $includedRegions;

	private $excludedRegions;

	private $dom;

	public function __construct($regionId, $shippingLevelId, $shippingLabel,$shippingRate)
	{
		$this->dom = new \DOMDocument("1.0","utf-8");
		$this->dom->formatOutput = true;

		$this->includedRegions = array();
		$this->excludedRegions = array();

		$this->shippingMethodId = uniqid("shipping-table-");
		$this->shippingLabel = $shippingLabel;

		//Checking if the regionId provided is valid
		if ($this->validateRegion($regionId))
		{
			$this->regionId = $regionId;
		}

		//If regionId is not REGION_CUSTOM, then adding it to the list of included regions as a PredefinedRegion
		if ($regionId != ShippingMethod::REGION_CUSTOM)
		{
			$predefinedRegionTag = $this->dom->createElement("PredefinedRegion",$this->regionId);
			array_push($this->includedRegions, $predefinedRegionTag);
		}

		//Checking if the shippingLevelId provided is valid
		if ($this->validateServiceLevel($shippingLevelId))
		{
			$this->shippingLevelId = $shippingLevelId;
		}

		$this->shippingRate = $shippingRate;
	}

	public function includeCustomCountry($countryCode)
	{
		//Creating WorldRegion Tags for the included country code
		$worldRegionTag = $this->dom->createElement("WorldRegion");
		$worldRegionTag->appendChild($this->dom->createElement("CountryCode",$countryCode));
		array_push($this->includedRegions,$worldRegionTag);
	}

	public function excludeCustomCountry($countryCode)
	{
		//Creating WorldRegion Tags for the excluded country code
		$worldRegionTag = $this->dom->createElement("WorldRegion");
		$worldRegionTag->appendChild($this->dom->createElement("CountryCode",$countryCode));
		array_push($this->excludedRegions,$worldRegionTag);
	}

	public function includeCustomState($stateCode)
	{
		//Creating USStateRegion Tags for the included US state code
		$stateRegionTag = $this->dom->createElement("USStateRegion",$stateCode);
		array_push($this->includedRegions,$stateRegionTag);
	}

	public function excludeCustomState($stateCode)
	{
		//Creating USStateRegion Tags for the exluded US state code
		$stateRegionTag = $this->dom->createElement("USStateRegion",$stateCode);
		array_push($this->excludedRegions,$stateRegionTag);
	}

	public function includeCustomZip($zipCode)
	{
		//Creating USZipRegion Tags for the included US zip code
		$zipRegionTag = $this->dom->createElement("USZipRegion",$zipCode);
		array_push($this->includedRegions,$zipRegionTag);
	}

	public function excludeCustomZip($zipCode)
	{
		//Creating USZipRegion Tags for the excluded US zip code
		$zipRegionTag = $this->dom->createElement("USZipRegion",$zipCode);
		array_push($this->excludedRegions,$zipRegionTag);
	}

	public function getShippingMethodId()
	{
		return $this->shippingMethodId;
	}

	public function setPOBoxSupport($isPOBoxSupported)
	{
		if(is_bool($isPOBoxSupported))
		{
			$this->isPOBoxSupported = $isPOBoxSupported;
		}
		else
		{
			trigger_error("isPOBoxSupported should be a boolean.",E_USER_ERROR);
		}
	}

	public function toXML($properties)
	{
		$shippingMethodXML = $this->dom->createElement("ShippingMethod");
		$shippingMethodXML->appendChild($this->dom->createElement("ShippingMethodId",$this->shippingMethodId));
		$shippingMethodXML->appendChild($this->dom->createElement("ServiceLevel",$this->shippingLevelId));
		$shippingMethodXML->appendChild($this->dom->importNode($this->shippingRate->toXML($properties,$this->dom),true));

		$includedRegionXML = $this->dom->createElement("IncludedRegions");

		foreach ($this->includedRegions as $regionTag)
		{
			$includedRegionXML->appendChild($regionTag);
		}

		$shippingMethodXML->appendChild($includedRegionXML);

		if (sizeof($this->excludedRegions) > 0)
		{
			$excludedRegionXML = $this->dom->createElement("ExcludedRegions");

			foreach ($this->excludedRegions as $regionId)
			{
				$excludedRegionXML->appendChild($regionId);
			}
			$shippingMethodXML->appendChild($excludedRegionXML);
		}

		$shippingMethodXML->appendChild($this->dom->createElement("DisplayableShippingLabel",$this->shippingLabel));

		return $shippingMethodXML;
	}

	/**
	 * Function to validate the given ShippingServiceLevel
	 */
	private function validateServiceLevel($shippingLevelId)
	{
		if ($this->validateStringDataType("ShippingServiceLevel", $shippingLevelId))
		{
			switch ($shippingLevelId){
				case \AmazonPayments\Model\ShippingMethod::SERVICE_EXPEDITED:
				case \AmazonPayments\Model\ShippingMethod::SERVICE_ONE_DAY:
				case \AmazonPayments\Model\ShippingMethod::SERVICE_TWO_DAY:
				case \AmazonPayments\Model\ShippingMethod::SERVICE_STANDARD:
					return true;
				default:
					trigger_error("Invalid Shipping Service level value given. Use the SERVICE constants provided in \AmazonPayments\Model\ShippingMethod class.",E_USER_ERROR);
			}
		}
	}

	/**
	 * Function to validate the given ShippingRegion
	 */
	private function validateRegion($regionId)
	{
		if ($this->validateStringDataType("ShippingRegion", $regionId))
		{
			switch ($regionId){
				case \AmazonPayments\Model\ShippingMethod::REGION_US_CONTINENTAL_48_STATES:
				case \AmazonPayments\Model\ShippingMethod::REGION_US_FULL_50_STATES:
				case \AmazonPayments\Model\ShippingMethod::REGION_US_ALL:
				case \AmazonPayments\Model\ShippingMethod::REGION_WORLD_ALL:
				case \AmazonPayments\Model\ShippingMethod::REGION_CUSTOM:
					return true;
				default:
					trigger_error("Invalid Shipping Region value. Use REGION constants provided in \AmazonPayments\Model\ShippingMethod to avoid errors!",E_USER_ERROR);
			}
		}
			
	}

	/**
	 * Function to validate string data type
	 */
	private function validateStringDataType($parameterName,$data)
	{
		if (is_string($data) && !is_null($data) && $data != "")
		{
			return true;
		}
		else
		{
			trigger_error($parameterName . " should be a string and not be empty!",E_USER_ERROR);
		}
	}

}

?>
