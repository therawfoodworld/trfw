<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/


namespace AmazonPayments\Model;

final class MandatoryProperties
{
	const merchantId = "merchantId";
	const accessKey = "accessKey";
	const secretKey = "secretKey";
	const currencyCode = "currencyCode";
	const weightUnit = "weightUnit";
	
	public static function values()
	{
		return array("merchantId",
				"accessKey",
				"secretKey",
				"currencyCode",
				"weightUnit");
	}
}


?>