<?php
/**
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     AmazonPayments
 *  @copyright   Copyright 2012 Amazon Technologies, Inc.
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2012-02-28
 */
/*******************************************************************************
 *
*/

namespace AmazonPayments\Model;

class ShippingRate
{
	private $weightBasedRate;

	private $itemQuantityBasedRate;

	private $shipmentBasedRate;

	private $currencyCode;

	public function __construct()
	{
	}

	public function setCurrencyCode($currencyCode)
	{
		$this->currencyCode = $currencyCode;
	}

	public function setWeightBasedRate($rate)
	{
		$this->weightBasedRate = $rate;
	}

	public function setItemQuantityBasedRate($rate)
	{
		$this->itemQuantityBasedRate = $rate;
	}

	public function setShipmentBasedRate($rate)
	{
		$this->shipmentBasedRate = $rate;
	}

	public function toXML($properties,$dom)
	{
		$rateXML = $dom->createElement("Rate");

		if (!is_null($this->shipmentBasedRate))
		{
			$shipmentBasedRateTag = new \AmazonPayments\Model\Price($this->shipmentBasedRate);
			$rateXML->appendChild($dom->importNode($shipmentBasedRateTag->toXML("ShipmentBased",
					$properties,$dom),true));
		}

		if (!is_null($this->itemQuantityBasedRate))
		{
			$itemQuantityBasedRateTag = new \AmazonPayments\Model\Price($this->itemQuantityBasedRate);
			$rateXML->appendChild($dom->importNode($itemQuantityBasedRateTag->toXML("ItemQuantityBased",
					$properties,$dom),true));
		}

		if (!is_null($this->weightBasedRate))
		{
			$weightBasedRateTag = new \AmazonPayments\Model\Price($this->weightBasedRate);
			$rateXML->appendChild($dom->importNode($weightBasedRateTag->toXML("WeightBased",
					$properties,$dom),true));
		}

		return $rateXML;
	}

	//Function to return the currencyCode. The value set through the setter method of the object is returned, if
	//it has not been set using setter method than the value from the properties file is returned
	private function getCurrencyCode($properties)
	{
		return !is_null($this->currencyCode) ? $this->currencyCode : $properties["currencyCode"];
	}
}

?>