<?php $BASE_URL="http://www.alumni.d3vsource.com";
 $ROOT_DIR="http://www.alumni.d3vsource.com"; ?>
<div class="navBreadCrumb">  
    <a href="<?php echo $BASE_URL; ?>/">Home</a>&nbsp;::&nbsp;Order Processing
</div>
<div class="centerColSep"></div>
<div class="checkoutShipping">
    <B>Please wait while we process your order...  <img src="<?php echo $BASE_URL; ?>/images/processing.gif"></B><BR><BR>
    DO NOT HIT RELOAD OR BACK AS IT MAY DUPLICATE YOUR ORDER.<br /><br /> This page should automatically re-direct you
</div>
<div class="content_t">
    <table id="contentMainWrapper" style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td id="column_left" style="width: 250px;">
                <?php include($ROOT_DIR."/include/left_col.phtml"); ?>
            </td>
            <td id="column_center" style="width: 512px;padding:0px 16px;" valign="top">
               <form action="<?php echo $BASE_URL; ?>/checkout/complete_processpayment.php" method="POST" id="processorder">
                   <input type="hidden" value="<?php echo $_POST['x_response_code'];?>" name="x_response_code">
                   <input type="hidden" value="<?php echo $_POST['x_trans_id'];?>" name="x_trans_id">
                   <input type="hidden" value="<?php echo $_POST['x_amount'];?>" name="x_amount">
                   <input type="hidden" value="<?php echo $_POST['x_avs_code'];?>" name="x_avs_code">
                   <input type="hidden" value="<?php echo $_POST['x_cavv_response'];?>" name="x_cavv_response">
                    <input type="hidden" value="<?php echo $_POST['x_response_reason_text'];?>" name="x_response_reason_text">
                    <input type="hidden" value="<?php echo $_POST['x_account_number'];?>" name="x_account_number">
                     <input type="hidden" value="<?php echo $_POST['x_invoice_num'];?>" name="x_invoice_num">
                   <input type="submit" value="Process order" style="display:none;">
               </form>
            </td>
            <td id="column_right" style="width: 250px;">
                <?php include($ROOT_DIR."/include/right_col.phtml"); ?>
            </td>
        </tr>
    </table>
</div>
<script>
    document.getElementById("processorder").submit();
</script>