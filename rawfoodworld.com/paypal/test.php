<?php
/* STEP #1 -> We get the token from Paypal....... and pass them back to paypal */
REQUIRE("../../config.phtml");
REQUIRE($ROOT_DIR."/include/mysql.phtml");

$mysqlid = mysql_connect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);

require_once($ROOT_DIR."/mgt/classes/efluentclass.phtml");
REQUIRE($ROOT_DIR."/classes/cartclass.phtml");

//////////////////////////////////////////////////////////////////////////////////////////////////////////
$efc = new efluentClass();
$efc->mysqlid = $mysqlid;
$efc->db = $MYSQL_DATABASE;

$cc = new cartClass();
$cc->dbconnect($MYSQL_SERVER, $MYSQL_USERNAME, $MYSQL_PASSWORD);
$cc->db = $MYSQL_DATABASE;

$PAYPAL_API_USERNAME = $efc->getConfigValue("Paypal", "API_Username");
$PAYPAL_API_PASSWORD = $efc->getConfigValue("Paypal", "API_Password");

if($_SERVER["HTTP_HOST"] == '209.213.105.215')
{
	$PAYPAL_API_RETURNURL = "http://209.213.105.215/checkout/paypal/review.phtml";
	$PAYPAL_API_CANCELURL = "http://www.therawfoodworld.com/customer/orders.phtml";
	$environment = 'live'; // 'beta-sandbox' , 'live' , 'sandbox'
} else {
	$PAYPAL_API_RETURNURL = "https://www.therawfoodworld.com/checkout/paypal/review.phtml";
	$PAYPAL_API_CANCELURL = "http://www.therawfoodworld.com/cart.phtml";
	$environment = 'live'; // 'beta-sandbox' , 'live' , 'sandbox'
}

/**
 * Send HTTP POST Request
 *
 * @param	string	The API method name
 * @param	string	The POST Message fields in &name=value pair format
 * @return	array	Parsed HTTP Response body
 */
function PPHttpPost($methodName_, $nvpStr_) 
{
	global $environment;
	global $PAYPAL_API_USERNAME;
	global $PAYPAL_API_PASSWORD;
	
	$API_UserName = $PAYPAL_API_USERNAME;
	$API_Password = $PAYPAL_API_PASSWORD;
	
	$API_Signature = urlencode('my_api_signature');
	$API_Endpoint = " https://api.paypal.com/nvp ";
	if("sandbox" === $environment || "sandbox" === $environment)
	 {
		$API_Endpoint = "https://api.$environment.paypal.com/nvp ";
	}
	$version = urlencode('51.0');

	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName".$nvpStr_;

	$h = fopen("/tmp/flid/".$_SESSION["flid"].".txt","a+");
    fwrite($h, "Session:". print_r($_SESSION,TRUE)."\n");
    fwrite($h, "Server:". print_r($_SERVER,TRUE)."\n");
    fwrite($h, "paypal/default.phtml - function[send]: ".serialize($nvpreq)."\n");
    fclose($h);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$cmd = "/usr/bin/curl -v -E /home/httpd/rawfoodworld.com/pp/cert_key_pem.txt -d \"$nvpreq\" $API_Endpoint";
//	echo $cmd;
//	exit;
	
	exec( $cmd, $returnArray ); //run curl with the data 
	$httpResponse = "";
	for($xx = 0; $xx < count($returnArray); $xx++) 
		$httpResponse .= $returnArray[$xx];
	   
    $h = fopen("/tmp/flid/".$_SESSION["flid"].".txt","a+");
    fwrite($h, "Session:". print_r($_SESSION,TRUE)."\n");
    fwrite($h, "Server:". print_r($_SERVER,TRUE)."\n");
    fwrite($h, "paypal/default.phtml - function[recv]: ".serialize($httpResponse)."\n");
    fclose($h);
	if($httpResponse == "") 
	{
		echo "CURL ERROR<BR>\n";
		exit;
	}	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Extract the RefundTransaction response details
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) 
	{
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}




$total = $_SESSION['interNationalOrderTotal'];

if (isset($_SESSION['use_credit_amount']) && $_SESSION['use_credit_amount'] > 0)
{
	if ($_SESSION['use_credit_amount'] <= $total)
	{
		$total -= $_SESSION['use_credit_amount'];
	}
	elseif ($_SESSION['use_credit_amount'] > $subtotal)
	{
		$total = '0.00';
	} else {
	
	}			
}

//$cmd = "/home/httpd/rawfoodworld.com/pp/ec_set.sh $PAYPAL_API_USERNAME $PAYPAL_API_PASSWORD $total $PAYPAL_API_RETURNURL $PAYPAL_API_CANCELURL";
// if user came form his account order page.
if(isset($_POST['order_price']) && !empty($_POST['order_price'])){
  $total = $_POST['order_price'];
  $PAYPAL_API_CANCELURL ="https://www.therawfoodworld.com/customer/orders.phtml";
}

$paymentAmount = number_format($total, 2, ".", ""); // I think this is the fix
$total;
//exit;
$currencyID = urlencode('USD');							// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
$paymentType = urlencode('Authorization');				// or 'Sale' or 'Order'

$returnURL = $PAYPAL_API_RETURNURL;
$cancelURL = $PAYPAL_API_CANCELURL;

// Pay Later
$promoCode = urlencode('101');

$nvpStr = "&Amt=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID";
$nvpStr .= "&L_PROMOCODE0=$promoCode";
///echo  $nvpStr;
//exit;
$httpParsedResponseAr = PPHttpPost('SetExpressCheckout', $nvpStr,$API_UserName,$API_Password,$API_Signature);
//print_r($httpParsedResponseAr);
//exit;
$h = fopen("/tmp/flid/".$_SESSION["flid"].".txt","a+");
fwrite($h, "Session:". print_r($_SESSION,TRUE)."\n");
fwrite($h, "Server:". print_r($_SERVER,TRUE)."\n");
fwrite($h, "paypal/default.phtml: ".serialize($httpParsedResponseAr)."\n");
fclose($h);
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
{
	// Redirect to paypal.com here
	$token = urldecode($httpParsedResponseAr["TOKEN"]);
	//print_r($token);
	//exit;
   $payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&useraction=commit&token=$token";
	
	
	if("sandbox" === $environment || "sandbox" === $environment) {
		$payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&token=$token";
	}
    print_r($httpParsedResponseAr);
	header("Location: $payPalURL");
	exit;
} 
else  
{
	exit('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
}
?>