$(document).ready(function(){
	$('.add2cartbtn').each(function(){
		$(this).click(function(){
			var item = $(this).attr('id');
			var qty = $('.cart_quantity').val();
			window.location = "/cart.phtml?id=" + item + "&qty=" + qty;
		});
	});

	$('.ship4add2cartbtn').each(function(){
		$(this).click(function(){
			var pid = $(this).attr('id');
			var qty = $('.cart_quantity').val();
			var bulk_add = $('#bulk_add_'+pid).val();
			
			// check for shipping location
			$.get('/ajax_functions.phtml',{ action: "check_ship_loc", 'pid': pid }, function(response){
				if (response == 'yes') {
					window.location = "/cart.phtml?id=" + pid + "&qty=" + qty+"&bulk_add="+bulk_add;
				} else if (response == 'no') {
					$('.error_' + pid).html('We\'re sorry, this item cannot be shipped to your country due to licensing restrictions with the manufacturer. Please contact us for alternative options.').show();					
				} else {
					window.location = "/cart.phtml?id=" + pid + "&qty=" + qty+"&bulk_add="+bulk_add;
				}
			});
		});
	});
	
	$('#reorder_btn').click(function() {
		var oid = $(this).attr('rel');
		// check for items
		$.get('/ajax_functions.phtml',{ action: "check_reorder", 'oid': oid }, function(response){
			$('#reorder_msg').html(response).show();					
		});
	});
});

function changeSort(type,sb_val) {
    $.get('/ajax_functions.phtml',{ action: "sort_by",'type': type,'sb_val':sb_val}, function(data) {
        location.reload();
    });
}
function addToFavorite(pid,cont) {
    $.get('/ajax_functions.phtml',{ action: "add_to_favorite",'pid': pid}, function(data) {
//        location.reload();
        if(cont == 'prodInfo') {
//            $('#favBtn').remove();
//            $('.cartAddBtn').append('<p class="favProdTxt">Favorite</p>');
            window.location = "/favorite.phtml";
        }
        if(cont == 'viewOrderFav') {
            $('#viewOrderFav_'+pid).html(data);
        }
    });
}
function removeFav(pid) {
    $.get('/ajax_functions.phtml',{ action: "remove_favorite",'pid': pid}, function(data) {
        location.reload();
    });
}
function addToNotify(pid,cont) {
    $.get('/ajax_functions.phtml',{ action: "add_to_notify",'pid': pid}, function(data) {
//        location.reload();
        if(cont == 'prodInfo') {
            $('#notBtn').remove();
        }
        if(cont == 'prods') {
            $('#notBtn_'+pid).remove();
        }
    });
}
function setSortBy(nr,type,type_id) {
     $.get('/ajax_functions.phtml',{ action : 'sort_by', 'sb' : nr, 'cat_lid' : type, 'cat_lid_val' : type_id }, function(data) {
        location.reload();
    });
}
function addMultimpleItems() {
    var items = '';
    $('.multiadd').each(function(){
		var pid = $(this).attr('id').split('_');
        if($(this).is(':checked')) {
           items += pid[1]+',';
        }
	});
    items = items.substr(0,items.length-1);
    $.get('/ajax_functions.phtml',{ action : 'add_to_cart', 'id' : items}, function(data) {
        window.location = "/cart.phtml";
    });
    if(items !== '') {
//        window.location = "/cart.phtml";
    }
}