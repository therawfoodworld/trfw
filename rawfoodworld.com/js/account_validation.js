var form = "";
var submitted = false;
var error = false;
var error_message = "";

function GetXmlHttpObject()
{
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}

	return xmlHttp;
}

function check_input(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;
    
    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      error = true;
	} else if (field_name == 'email') {

	$(function() {
		$.ajax({
	        url: "/ajax_functions.phtml?action=verify_register_email&email="+field_value+"&sid="+Math.random(),
	        context: document.body,
	        async: false, // you want a synchronous call
	        success: function(data){
				if(data == 'exist') {
					error_message = error_message + "* Your Email is already used, change email address or try to login.\n";
					error = true;
				}
	        }
		});
	});		

/*		
		xmlHttp=GetXmlHttpObject();
		var url="/ajax_functions.phtml?action=verify_register_email&email="+field_value+"&sid="+Math.random();
		xmlHttp.onreadystatechange=verifyEmailAddressResult(message);
		xmlHttp.open("GET",url,true);
		xmlHttp.send(null);
*/
}
    
  }
}

function verifyEmailAddressResult(message) {
	if(xmlHttp.readyState==4) {
		if (xmlHttp.responseText == 'exist') {
			error_message = error_message + "* Your Email is already used, change email address or try to login.\n";
			error = true;
		}
	}
}

function check_radio(field_name, message) {
  var isChecked = false;

  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var radio = form.elements[field_name];

    for (var i=0; i<radio.length; i++) {
      if (radio[i].checked == true) {
        isChecked = true;
        break;
      }
    }

    if (isChecked == false) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_select(field_name, field_default, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value == field_default) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_password(field_name_1, field_name_2, field_size, message_1, message_2) {
  if (form.elements[field_name_1] && (form.elements[field_name_1].type != "hidden")) {
    var password = form.elements[field_name_1].value;
    var confirmation = form.elements[field_name_2].value;

    if (password == '' || password.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      error = true;
    } else if (password != confirmation) {
      error_message = error_message + "* " + message_2 + "\n";
      error = true;
    }
  }
}

function check_password_new(field_name_1, field_name_2, field_name_3, field_size, message_1, message_2, message_3) {
  if (form.elements[field_name_1] && (form.elements[field_name_1].type != "hidden")) {
    var password_current = form.elements[field_name_1].value;
    var password_new = form.elements[field_name_2].value;
    var password_confirmation = form.elements[field_name_3].value;

    if (password_current == '' || password_current.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      error = true;
    } else if (password_new == '' || password_new.length < field_size) {
      error_message = error_message + "* " + message_2 + "\n";
      error = true;
    } else if (password_new != password_confirmation) {
      error_message = error_message + "* " + message_3 + "\n";
      error = true;
    }
  }
}

function check_form(form_name) {
  if (submitted == true) {
    alert("This form has already been submitted. Please press OK and wait for this process to be completed.");
    return false;
  }

  error = false;
  form = form_name;
  error_message = "Errors have occurred during the processing of your form.\n\nPlease make the following corrections:\n\n";


  check_input("fame", 2, "Is your first name correct? Our system requires a minimum of 2 characters. Please try again.");
  check_input("lname", 2, "Is your last name correct? Our system requires a minimum of 2 characters. Please try again.");

  check_input("email", 6, "Is your email address correct? It should contain at least 6 characters. Please try again.");
  
  check_input("street_address", 5, "Your Street Address must contain a minimum of 5 characters.");

  check_select("country", "", "You must select a country from the Countries pull down menu.");

  
	var e = document.getElementById("country");    
	var country_abbr = e.options[e.selectedIndex].value;   
	if (country_abbr == 'US') {
		check_select("usa_state", "", "You must select a state from the State/Province pull down menu.");
	} else {
		check_input("state", 2, "Your State must contain a minimum of 2 characters.");
	}

	check_input("city", 2, "Your City must contain a minimum of 2 characters.");
	check_input("zip", 4, "Your Post/ZIP Code must contain a minimum of 4 characters.");
 
  check_input("phone", 3, "Your Telephone Number must contain a minimum of 3 characters.");

  check_password("password", "confirmation", 5, "Your Password must contain a minimum of 5 characters.", "The Password Confirmation must match your Password.");
  check_password_new("password_current", "password_new", "password_confirmation", 5, "Your Password must contain a minimum of 5 characters.", "Your new Password must contain a minimum of 5 characters.", "The Password Confirmation must match your new Password.");

  if (error == true) {
    alert(error_message);
    return false;
  } else {
    submitted = true;
    return true;
  }
}

function checkPassForm() {
	var new_pass = $('#password-new').val();
	var new_repass = $('#password-confirm').val();
	var acctid = $('#acctid').val();

	/*
	var curr_pass = $('#password-current').val();
	if(curr_pass == '') {
		alert('You must enter your current password!');
		$('#password-current').focus();
		return;
	}
	*/
	if(new_pass == '') {
		alert('You must choose a password!');
		$('#new_pass').focus();
		return;
	}
	if(new_repass != new_pass) {
		alert("The two passwords you entered do not match.  Be careful not to copy the 'New Password'\ninto the 'Confirm Password' box.  You *MUST* re-type the password (manually).");
		$('#new_repass').focus();
		return;
	}
	$.get('/ajax_functions.phtml',{action:'change_pass','new_pass':new_pass,'acctid':acctid}, function(data) {
		$('#password-new').val('');
		$('#password-confirm').val('');
		alert(data);
	});  
}

function checkResetPassForm() {
	var emailaddr = $('#emailaddr').val();
	if(emailaddr == '') {
		alert('You must enter your email address!');
		$('#emailaddr').focus();
		return false;
	} else if (!validateEmail(emailaddr)) {
		alert('You must enter a valid email address!');
		return false;
	} else {
		$("form#reset_password").submit();	
	}
}

function validateEmail($email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (!emailReg.test($email)) {
		return false;
	} else {
		return true;
	}
}