var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
		if (field_value == '' || field_value.length < field_size) {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}

function check_select(field_name, field_default, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
	
		if (field_value == field_default) {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}

function check_form(form_name) {
	if (submitted == true) {
		alert("This form has already been submitted. Please press OK and wait for this process to be completed.");
		return false;
	}

	error = false;
	form = form_name;
	error_message = "Errors have occurred during the processing of your form.\n\nPlease make the following corrections:\n\n";
	
	check_input("firstname", 2, "Is your first name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("lastname", 2, "Is your last name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("street_address", 5, "Your Street Address must contain a minimum of 5 characters.");
	
	var e = document.getElementById("country");    
	var country_abbr = e.options[e.selectedIndex].value;   
	if (country_abbr == 'US') {
		check_select("usa_state", "", "You must select a state from the State/Province pull down menu.");
	} else {
		check_input("state", 2, "Your State must contain a minimum of 2 characters.");
	}
	
	check_input("city", 2, "Your City must contain a minimum of 2 characters.");
	check_input("postcode", 3, "Your Post/Zip Code must contain a minimum of 3 characters.");
	
	if (error == true) {
		alert(error_message);
		return false;
	} else {
		submitted = true;
		return true;
	}
}