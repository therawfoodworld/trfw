$(document).ready(function(){
       /* $('.cart_update').each(function(){
            $(this).click(function(){
                var item = $(this).attr('id').split('_');
                var qty_val = $('#val_'+item[2]).val();
                $.get('/ajax_functions.phtml',{ action: "change_qty", prod: item[2],qty: qty_val }, function(data) {
//                        window.location = "/cart.phtml";
                        location.reload();
                }); 
            });
	}); */
	
	$('.remove_cart_item').click(function() {
		var prodId_arr = $(this).attr('id').split('_');
		var prodId = prodId_arr[2];
		$.get('/ajax_functions.phtml',{ action: "change_qty",prod: prodId,qty:0}, function(data) {
			location.reload();
		});
	});

	$('#update_qty_all').click(function(){
		var final_s = '';
		$('.cartqty').each(function() {
			var prodId_arr = $(this).attr('id').split('_');
			var prodId = prodId_arr[1];
			var qty_val = $(this).val();
			final_s += prodId+'_'+qty_val+',';
		});
		$.post('/ajax_functions.phtml',{ action: "change_qty_all",prods: final_s}, function(data) {
			location.reload();
		});
		
	});
	
	$('.btnApply').click(function(){
		var coupon_val = $('.cuppon').val();
		$.get('/ajax_functions.phtml',{ action: "apply_cuppon", couponcode: coupon_val }, function(data) {
			window.location = "/cart.phtml";
		}); 
	});	
	
	$('#applyCuppon').click(function(){
		var promoVal = $('#promo').val();
		$.get('/ajax_functions.phtml',{ action: "apply_cuppon",couponcode: promoVal}, function(data) {
//			  alert(data);
			location.reload();
		});
	});
});
function removeChecked() {
    var final_s = '';
    $('.cart_item_check').each(function() {
    	if($(this).attr('checked') == true) {    	
            var prodId_arr = $(this).attr('id').split('_');
            var prodId = prodId_arr[1];
            final_s += prodId+'_';
    	}
    });
    $.get('/ajax_functions.phtml',{ action: "remove_checked",prods: final_s}, function(data) {
            location.reload();
//				window.location='/cart.phtml';
    });
}
