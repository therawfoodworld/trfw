var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
		if (field_value == '' || field_value.length < field_size) {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}

function check_select(field_name, field_default, message) {
	if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
		var field_value = form.elements[field_name].value;
	
		if (field_value == field_default) {
			error_message = error_message + "* " + message + "\n";
			error = true;
		}
	}
}

function check_form(form_name) {
	if (submitted == true) {
		alert("This form has already been submitted. Please press OK and wait for this process to be completed.");
		return false;
	}

	error = false;
	form = form_name;
	error_message = "Errors have occurred during the processing of your form.\n\nPlease make the following corrections:\n\n";
	
	// My Account Information	
	check_input("firstname", 2, "Is your first name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("lastname", 2, "Is your last name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("email_address", 6, "Is your email address correct? It should contain at least 6 characters. Please try again.");
	check_input("telephone", 3, "Your Telephone Number must contain a minimum of 3 characters.");
	
	// My Shipping Address
	check_input("ship2fname", 2, "Is your shipping first name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("ship2lname", 2, "Is your shipping last name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("ship2street1", 5, "Your Shipping Street Address must contain a minimum of 5 characters.");
	
	var e = document.getElementById("ship2country");    
	var country_abbr = e.options[e.selectedIndex].value;   
	if (country_abbr == 'US') {
		check_select("ship2usa_state", "", "You must select a shipping state from the State/Province pull down menu.");
	} else {
		check_input("ship2state", 2, "Your shipping State must contain a minimum of 2 characters.");
	}
	
	check_input("ship2city", 2, "Your shipping City must contain a minimum of 2 characters.");
	check_input("ship2zip", 3, "Your shipping Post/Zip Code must contain a minimum of 3 characters.");
	
	// My Billing Address
	check_input("bill2fname", 2, "Is your billing first name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("bill2lname", 2, "Is your billing last name correct? Our system requires a minimum of 2 characters. Please try again.");
	check_input("bill2street1", 5, "Your billing Street Address must contain a minimum of 5 characters.");
	
	var e = document.getElementById("bill2country");    
	var country_abbr = e.options[e.selectedIndex].value;   
	if (country_abbr == 'US') {
		check_select("bill2usa_state", "", "You must select a billing state from the State/Province pull down menu.");
	} else {
		check_input("bill2state", 2, "Your billing State must contain a minimum of 2 characters.");
	}
	
	check_input("bill2city", 2, "Your billing City must contain a minimum of 2 characters.");
	check_input("bill2zip", 3, "Your billing Post/Zip Code must contain a minimum of 3 characters.");
	
	if (error == true) {
		alert(error_message);
		return false;
	} else {
		submitted = true;
		return true;
	}
}