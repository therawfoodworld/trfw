-- phpMyAdmin SQL Dump
-- version 2.11.9.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2013 at 03:56 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rawfoodworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_notify`
--

CREATE TABLE IF NOT EXISTS `product_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `accid` int(10) NOT NULL,
  `notify` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `product_notify`
--

INSERT INTO `product_notify` (`id`, `pid`, `accid`, `notify`) VALUES
(1, 1006228, 56708, 1),
(2, 1001675, 56708, 1),
(3, 1001333, 56710, 0),
(4, 1002259, 56710, 0),
(5, 1002145, 56710, 0),
(6, 1002145, 56712, 0);
