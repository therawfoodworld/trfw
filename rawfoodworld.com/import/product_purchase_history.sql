-- phpMyAdmin SQL Dump
-- version 2.11.9.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2013 at 03:57 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rawfoodworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_purchase_history`
--

CREATE TABLE IF NOT EXISTS `product_purchase_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `vendorid` bigint(20) DEFAULT NULL,
  `vendorname` varchar(128) DEFAULT NULL,
  `poid` bigint(20) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `manid` (`manid`),
  KEY `vendorid` (`vendorid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=162176 ;

--
-- Dumping data for table `product_purchase_history`
--

INSERT INTO `product_purchase_history` (`id`, `pid`, `tstamp`, `sku`, `manid`, `manname`, `vendorid`, `vendorname`, `poid`, `qty`, `cost`) VALUES
(162175, 3, '2011-02-03 10:25:47', 'MD01225984/5/White', 8149, '', 143, 'LiveFeed', 7489, 1, 12.00);
