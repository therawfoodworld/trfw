-- phpMyAdmin SQL Dump
-- version 2.11.9.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2013 at 03:55 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rawfoodworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory`
--

CREATE TABLE IF NOT EXISTS `product_inventory` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `distname` varchar(25) NOT NULL DEFAULT '',
  `warehouse` varchar(50) NOT NULL DEFAULT '',
  `wh_state` char(2) NOT NULL DEFAULT 'XX',
  `inv_available` bigint(20) NOT NULL DEFAULT '0',
  `inv_backorder` bigint(20) NOT NULL DEFAULT '0',
  `inv_onorder` bigint(20) NOT NULL DEFAULT '0',
  `eta` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cost` double(16,2) DEFAULT NULL,
  `reccost` double(16,2) DEFAULT NULL,
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `whid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `lineid` (`lineid`),
  KEY `warehouse` (`warehouse`),
  KEY `id_2` (`id`,`whid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=527202 ;

--
-- Dumping data for table `product_inventory`
--

INSERT INTO `product_inventory` (`id`, `distname`, `warehouse`, `wh_state`, `inv_available`, `inv_backorder`, `inv_onorder`, `eta`, `cost`, `reccost`, `lineid`, `whid`) VALUES
(1002429, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527188, 0),
(1002429, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527189, 2),
(983, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527192, 0),
(983, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527193, 2),
(1005636, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527194, 0),
(1005636, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527195, 2),
(1004208, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527196, 0),
(1004208, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527197, 2),
(1004207, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527198, 0),
(1004207, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527199, 2),
(1004242, '', 'New', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527200, 0),
(1004242, '', 'RMA ', 'XX', 0, 0, 0, '0000-00-00 00:00:00', 0.00, NULL, 527201, 2);
