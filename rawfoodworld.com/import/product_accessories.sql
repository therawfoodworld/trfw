-- phpMyAdmin SQL Dump
-- version 2.11.9.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2013 at 03:51 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rawfoodworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_accessories`
--

CREATE TABLE IF NOT EXISTS `product_accessories` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `accid` bigint(20) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_accessories`
--

INSERT INTO `product_accessories` (`id`, `accid`, `dorder`) VALUES
(24, 4, 0),
(24, 5, 0),
(24, 7, 0),
(24, 20, 0),
(24, 28, 0),
(24, 16, 0),
(24, 24, 0),
(24, 19, 0),
(24, 27, 0),
(24, 30, 0),
(24, 47, 0);
