CREATE TABLE `amazon_alpha_base_messages` (
  `mid` int(10) unsigned NOT NULL auto_increment,
  `pid` int(10) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `brid` int(10) NOT NULL default '0',
  `campid` smallint(5) NOT NULL,
  `asin` varchar(50) NOT NULL default '',
  `pid_lastupdate` int(10) unsigned NOT NULL default '0',
  `qty` int(11) NOT NULL default '0',
  `price` double NOT NULL default '0',
  `deleteflag` tinyint(3) NOT NULL default '0',
  `listid` smallint(5) NOT NULL default '0',
  PRIMARY KEY  (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_base_requests` (
  `brid` int(10) unsigned NOT NULL auto_increment,
  `amazon_submissionid` varchar(50) default NULL,
  `amazon_requestid` varchar(255) NOT NULL default '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) NOT NULL,
  PRIMARY KEY  (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`),
  KEY `campid` (`campid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_alpha_campaign_lists` (
  `campid` smallint(5) NOT NULL default '0',
  `listid` smallint(5) NOT NULL default '0',
  PRIMARY KEY  (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_campaigns` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `campaign_name` varchar(255) default NULL,
  `min_profit_pct` double(16,2) NOT NULL default '0.00',
  `min_profit_cur` double(16,2) NOT NULL default '0.00',
  `quantity` int(11) NOT NULL default '-1',
  `qty_control` int(11) NOT NULL default '80',
  `pricelevel` int(11) NOT NULL default '0',
  `lastupdate` int(10) unsigned default NULL,
  `min_qty` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_categories` (
  `catid` int(10) NOT NULL default '0',
  `amazoncat` int(10) NOT NULL default '0',
  `pid` int(10) NOT NULL default '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_convert_log` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_convert_mfn` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_debug` (
  `invoiceid` varchar(26) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_duplicates` (
  `did` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_duplicates_extended` (
  `did` int(10) NOT NULL default '0',
  `asin` varchar(50) default NULL,
  `salesrank` varchar(20) default NULL,
  `imageurl` varchar(255) default NULL,
  `brand` varchar(50) default NULL,
  `upc` varchar(50) default NULL,
  `features` text,
  `lowestprice` varchar(50) default NULL,
  `numsellers` int(11) default NULL,
  `title` varchar(255) default NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_email_history` (
  `invoiceid` varchar(255) NOT NULL,
  PRIMARY KEY  (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_errors` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` int(10) NOT NULL default '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_errors_other` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `message` varchar(255) default NULL,
  `mid` int(10) NOT NULL default '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) default NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') default NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_feed_report` (
  `id` int(10) NOT NULL auto_increment,
  `type` enum('BASE','BASE_TAX','BASE_CF','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','CBA','ORDERADJUSTMENT','CONVERTSKUSAFN','CONVERTSKUSMFN','REIMBURSEMENTS','RETURNS') default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` tinyint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_feedback` (
  `fbid` int(20) NOT NULL auto_increment,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL,
  `removed` int(5) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_feedback_report` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` tinyint(3) unsigned NOT NULL default '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_inbound_shipping` (
  `id` int(10) NOT NULL auto_increment,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_inbound_shipping_extra` (
  `id` int(10) NOT NULL auto_increment,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL default '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL default '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `poid` (`poid`),
  KEY `ais_id` (`ais_id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_inbound_shipping_files` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `inbound_id` int(10) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_inventory_recommendations` (
  `id` int(10) NOT NULL auto_increment,
  `efluent_pid` int(10) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `asin` varchar(255) NOT NULL,
  `upc` varchar(255) NOT NULL,
  `last_update` int(10) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `fulfillment_channel` varchar(255) NOT NULL,
  `sales_14_days` int(10) NOT NULL,
  `sales_30_days` int(10) NOT NULL,
  `available_qty` int(10) NOT NULL,
  `days_stock_out` int(10) NOT NULL,
  `inbound_qty` int(10) NOT NULL,
  `recommended_inbound_qty` int(10) NOT NULL,
  `days_out_of_stock_30` int(10) NOT NULL,
  `lost_sale_30` int(10) NOT NULL,
  `recommendation_id` text NOT NULL,
  `recommendation_reason` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `efluent_pid` (`efluent_pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_item_dimensions` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `item_length` double(16,2) NOT NULL,
  `item_width` double(16,2) NOT NULL,
  `item_height` double(16,2) NOT NULL,
  `item_weight` double(16,2) NOT NULL,
  `item_size` double(16,2) NOT NULL,
  `item_dim_weight` double(16,2) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_item_type` (
  `category_id` int(10) NOT NULL,
  `item_type` varchar(1000) NOT NULL,
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_kit_quantity` (
  `orderid` int(10) unsigned default NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) default NULL,
  `qty` int(11) default NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_log` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `tstamp` datetime default NULL,
  `userid` int(10) NOT NULL default '-1',
  `requestid` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_log_extended` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') default NULL,
  `data` longblob,
  PRIMARY KEY  (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL default '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_order_reports` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` tinyint(3) unsigned NOT NULL default '0',
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_order_shipmethod` (
  `id` int(10) NOT NULL auto_increment,
  `orderid` int(10) NOT NULL,
  `shipmethod` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime default NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_qty_scripts` (
  `campid` int(10) NOT NULL default '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_reimbursements` (
  `id` int(10) NOT NULL auto_increment,
  `reimbursement_id` int(10) NOT NULL,
  `tstamp` varchar(255) NOT NULL,
  `tstamp_est` int(10) NOT NULL,
  `amazon_orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `amount` double(16,2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_returns` (
  `id` int(10) NOT NULL auto_increment,
  `tstamp` varchar(255) NOT NULL,
  `tstamp_est` int(10) NOT NULL,
  `amazon_orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `asin` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `fc` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `sku` (`sku`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `tstamp_est` (`tstamp_est`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_reviews_collection` (
  `asin` varchar(25) NOT NULL default '',
  `reviews` varchar(40) default NULL,
  PRIMARY KEY  (`asin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_shipping_map` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL default '0',
  `amazon_method` varchar(50) default NULL,
  `carrier` varchar(50) default NULL,
  `method` varchar(255) default NULL,
  `weight_min` double(16,2) NOT NULL default '0.00',
  `weight_max` double(16,2) NOT NULL default '0.00',
  `cost_min` double(16,2) NOT NULL default '0.00',
  `cost_max` double(16,2) NOT NULL default '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs` (
  `id` int(10) NOT NULL auto_increment,
  `asin` varchar(100) NOT NULL,
  `uniqueid` varchar(255) NOT NULL,
  `tstamp` int(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_buybox_price` (
  `id` int(10) NOT NULL auto_increment,
  `sqs_id` int(10) NOT NULL,
  `asin` varchar(100) NOT NULL,
  `item_condition` tinyint(2) NOT NULL,
  `price` double(16,2) NOT NULL default '0.00',
  PRIMARY KEY  (`id`),
  KEY `sqs_id` (`sqs_id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_lowest_price` (
  `id` int(10) NOT NULL auto_increment,
  `sqs_id` int(10) NOT NULL,
  `asin` varchar(100) NOT NULL,
  `fulfillment_channel` varchar(100) NOT NULL,
  `item_condition` tinyint(2) NOT NULL,
  `price` double(16,2) NOT NULL default '0.00',
  PRIMARY KEY  (`id`),
  KEY `sqs_id` (`sqs_id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_nodes` (
  `id` int(10) NOT NULL auto_increment,
  `nodeid` varchar(255) NOT NULL,
  `nodename` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_offers` (
  `id` int(10) NOT NULL auto_increment,
  `sqs_id` int(10) NOT NULL,
  `asin` varchar(100) NOT NULL,
  `seller_id` varchar(100) NOT NULL,
  `item_condition` tinyint(2) NOT NULL,
  `seller_positive_feedback_reating` int(10) NOT NULL,
  `seller_feedback_count` int(10) NOT NULL,
  `seller_shipping_availability_type` varchar(100) NOT NULL,
  `seller_listing_price` double(16,2) NOT NULL default '0.00',
  `seller_shipping` double(16,2) NOT NULL default '0.00',
  `seller_is_fulfilled` tinyint(2) NOT NULL,
  `seller_is_box_winner` tinyint(2) NOT NULL,
  `seller_condition_notes` text,
  PRIMARY KEY  (`id`),
  KEY `sqs_id` (`sqs_id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_sales_rank` (
  `id` int(10) NOT NULL auto_increment,
  `sqs_id` int(10) NOT NULL,
  `asin` varchar(100) NOT NULL,
  `category_id` varchar(100) NOT NULL,
  `rank` int(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `sqs_id` (`sqs_id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_sellers` (
  `id` int(10) NOT NULL auto_increment,
  `seller_id` varchar(255) NOT NULL,
  `seller_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_alpha_sqs_summary` (
  `id` int(10) NOT NULL auto_increment,
  `sqs_id` int(10) NOT NULL,
  `asin` varchar(100) NOT NULL,
  `used_amazon` int(10) NOT NULL,
  `used_merchant` int(10) NOT NULL,
  `new_amazon` int(10) NOT NULL,
  `new_merchant` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `sqs_id` (`sqs_id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `amazon_alpha_unshipped_orders` (
  `invoiceid` varchar(26) default NULL,
  `paydate` datetime default NULL,
  `dayslate` int(11) NOT NULL default '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `fba_alpha_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_audit_report` (
  `id` bigint(20) NOT NULL auto_increment,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL default '0',
  `tstamp` datetime default NULL,
  `ack` int(11) NOT NULL default '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_feed_report` (
  `id` int(10) NOT NULL auto_increment,
  `type` varchar(32) default NULL,
  `tstamp` datetime default NULL,
  `reportid` varchar(50) default NULL,
  `requestid` varchar(50) default NULL,
  `filename` varchar(255) default NULL,
  `complete` tinyint(3) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_inventory_mismatch` (
  `id` int(10) NOT NULL auto_increment,
  `sku` varchar(32) default NULL,
  `fcsku` varchar(32) default NULL,
  `asin` varchar(32) default NULL,
  `cond` varchar(32) default NULL,
  `cond_code` varchar(32) default NULL,
  `qty` int(11) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_line_items_import` (
  `id` int(10) NOT NULL auto_increment,
  `shipment_item_id` varchar(24) default NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_order_mismatch` (
  `id` int(10) NOT NULL auto_increment,
  `invoiceid` varchar(32) default NULL,
  `sku` varchar(32) default NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_adjustment` (
  `id` int(10) NOT NULL auto_increment,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_adjustment_items` (
  `id` int(10) NOT NULL auto_increment,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL auto_increment,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL auto_increment,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_order` (
  `id` int(10) NOT NULL auto_increment,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `import_date` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `report_id` (`report_id`),
  KEY `merchant_fulfillment_id` (`merchant_fulfillment_id`),
  KEY `marketplace_name` (`marketplace_name`),
  KEY `repid` (`repid`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_order_items` (
  `id` int(10) NOT NULL auto_increment,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `orid` (`orid`),
  KEY `AmazonOrderItemCode` (`AmazonOrderItemCode`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_order_items_cost` (
  `id` int(10) NOT NULL auto_increment,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `whid` (`whid`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`),
  KEY `amazon_settlement_id` (`amazon_settlement_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_order_items_promotion` (
  `id` bigint(20) NOT NULL auto_increment,
  `item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_other` (
  `id` int(11) NOT NULL auto_increment,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_settlement_report` (
  `settlement_report_id` int(10) NOT NULL auto_increment,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY  (`settlement_report_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator_items` (
  `id` int(10) NOT NULL auto_increment,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator_link` (
  `id` int(10) NOT NULL auto_increment,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator_network` (
  `id` int(10) NOT NULL auto_increment,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator_respons` (
  `id` int(10) NOT NULL auto_increment,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL default '0',
  `po_created` int(10) NOT NULL default '0',
  `order_created` int(10) NOT NULL default '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_shipment_creator_respons_items` (
  `id` int(10) NOT NULL auto_increment,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_sku_map` (
  `fba_sku` varchar(32) default NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `fba_alpha_skus` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sku` varchar(32) default NULL,
  `lastupdate` int(10) unsigned NOT NULL default '0',
  `fcsku` varchar(32) NOT NULL default '',
  `updateprice` tinyint(3) NOT NULL default '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;





