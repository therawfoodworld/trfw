-- MySQL dump 10.9
--
-- Host: localhost    Database: uclick
-- ------------------------------------------------------
-- Server version	4.1.8-standard

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `mid` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  `parent` bigint(20) NOT NULL default '0',
  `public` int(11) NOT NULL default '0',
  `dorder` int(11) NOT NULL default '0',
  KEY `mid` (`mid`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--


/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
LOCK TABLES `menu_items` WRITE;
INSERT INTO `menu_items` VALUES (1,'Home','/mgt/main.phtml',0,0,0),(2,'Configuration','',0,0,1),(3,'Edit Menu','/mgt/menu/default.phtml',2,0,0),(4,'Marketing','',0,0,3),(5,'Datafeed Manager','/mgt/df/default.phtml',4,0,0),(6,'Order Management','',0,0,2),(7,'View Daily Orders','/mgt/om/dailyview.phtml',6,0,0),(8,'Online Help','',0,0,4),(9,'Manage Help','/mgt/help/help.phtml',8,0,0),(10,'Email Marketing','/mgt/em/default.phtml',4,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

