-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 07:28 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rawfoodworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `a9_pricing`
--

CREATE TABLE IF NOT EXISTS `a9_pricing` (
  `masterid` bigint(20) DEFAULT NULL,
  `roimast` bigint(20) DEFAULT NULL,
  `mid` varchar(25) DEFAULT NULL,
  `url` text,
  `tstamp` datetime DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `shipping` double(16,2) DEFAULT NULL,
  `shipprice` double(16,2) DEFAULT NULL,
  `shipnote` text,
  `pricenote` varchar(25) DEFAULT NULL,
  `merchant` varchar(50) DEFAULT NULL,
  `condition_note` longblob,
  `supersaver` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `rating` double(16,2) DEFAULT NULL,
  `numratings` int(11) NOT NULL DEFAULT '0',
  `lshipprice` double(16,2) DEFAULT NULL,
  `ltstamp` datetime DEFAULT NULL,
  `lastprice` double(16,2) NOT NULL DEFAULT '0.00',
  `lastshipping` double(16,2) NOT NULL DEFAULT '0.00',
  `lastrank` int(11) NOT NULL DEFAULT '0',
  `lastshipnote` varchar(25) NOT NULL DEFAULT '',
  `lastpricenote` varchar(25) NOT NULL DEFAULT '',
  `lasttstamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `refurb` int(11) NOT NULL DEFAULT '0',
  `asin` varchar(32) DEFAULT NULL,
  `offerlistingid` varchar(255) DEFAULT NULL,
  KEY `masterid` (`masterid`),
  KEY `roimast` (`roimast`),
  KEY `roimast_2` (`roimast`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accbycat_products`
--

CREATE TABLE IF NOT EXISTS `accbycat_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `topid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) DEFAULT NULL,
  `minprice` double(16,2) DEFAULT NULL,
  `maxprice` double(16,2) DEFAULT NULL,
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `origin` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `topid` (`topid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

-- --------------------------------------------------------

--
-- Table structure for table `accbycat_top`
--

CREATE TABLE IF NOT EXISTS `accbycat_top` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_log`
--

CREATE TABLE IF NOT EXISTS `action_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ltype` enum('SUCCESS','FAILURE') DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `updateid` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `ltype` (`ltype`,`tstamp`),
  KEY `type` (`type`,`updateid`,`tstamp`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1973935 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_extended`
--

CREATE TABLE IF NOT EXISTS `action_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) DEFAULT NULL,
  `details` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1973931 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_inventory`
--

CREATE TABLE IF NOT EXISTS `action_log_inventory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ltype` enum('SUCCESS','FAILURE') DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `updateid` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `ltype` (`ltype`,`tstamp`),
  KEY `type` (`type`,`updateid`,`tstamp`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31330 ;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_inventory_extended`
--

CREATE TABLE IF NOT EXISTS `action_log_inventory_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) DEFAULT NULL,
  `details` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31330 ;

-- --------------------------------------------------------

--
-- Table structure for table `additional_costs`
--

CREATE TABLE IF NOT EXISTS `additional_costs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(26) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `aging_orders`
--

CREATE TABLE IF NOT EXISTS `aging_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `arepid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `orderage` int(11) DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`),
  KEY `arepid` (`arepid`,`status`,`orderage`),
  KEY `orderage` (`orderage`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `almo_spider_index`
--

CREATE TABLE IF NOT EXISTS `almo_spider_index` (
  `indexid` int(11) NOT NULL AUTO_INCREMENT,
  `manpart` varchar(100) DEFAULT NULL,
  `psrc` varchar(200) DEFAULT NULL,
  `col` varchar(900) DEFAULT NULL,
  PRIMARY KEY (`indexid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=295 ;

-- --------------------------------------------------------

--
-- Table structure for table `alpha_inventory`
--

CREATE TABLE IF NOT EXISTS `alpha_inventory` (
  `manpart` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazonace_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=265 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazonace_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazonace_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_campaigns_categories`
--

CREATE TABLE IF NOT EXISTS `amazonace_campaigns_categories` (
  `campid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazonace_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_categories`
--

CREATE TABLE IF NOT EXISTS `amazonace_categories` (
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `amazoncat` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_convert_log`
--

CREATE TABLE IF NOT EXISTS `amazonace_convert_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_convert_mfn`
--

CREATE TABLE IF NOT EXISTS `amazonace_convert_mfn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazonace_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazonace_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_errors`
--

CREATE TABLE IF NOT EXISTS `amazonace_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazonace_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2658 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_feedback`
--

CREATE TABLE IF NOT EXISTS `amazonace_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_feedback_report`
--

CREATE TABLE IF NOT EXISTS `amazonace_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazonace_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','ORDERADJUSTMENT') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1116 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazonace_inbound_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazonace_inbound_shipping_extra` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poid` (`poid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_inbound_shipping_files`
--

CREATE TABLE IF NOT EXISTS `amazonace_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_keywords`
--

CREATE TABLE IF NOT EXISTS `amazonace_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_kit_quantity`
--

CREATE TABLE IF NOT EXISTS `amazonace_kit_quantity` (
  `orderid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_log`
--

CREATE TABLE IF NOT EXISTS `amazonace_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2278 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazonace_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1685 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazonace_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazonace_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports_by_date`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports_by_date_items`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports_by_last_update`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports_by_last_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports_by_last_update_orders`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports_by_last_update_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_order_reports_by_last_update_orders_items`
--

CREATE TABLE IF NOT EXISTS `amazonace_order_reports_by_last_update_orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazonace_qty_scripts` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_refunds`
--

CREATE TABLE IF NOT EXISTS `amazonace_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` int(10) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_relationships`
--

CREATE TABLE IF NOT EXISTS `amazonace_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_removal_order`
--

CREATE TABLE IF NOT EXISTS `amazonace_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_removal_order_complete`
--

CREATE TABLE IF NOT EXISTS `amazonace_removal_order_complete` (
  `orderid` varchar(255) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_removal_order_info`
--

CREATE TABLE IF NOT EXISTS `amazonace_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_removal_order_log`
--

CREATE TABLE IF NOT EXISTS `amazonace_removal_order_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `report_sku` varchar(255) NOT NULL,
  `removalid` int(11) NOT NULL,
  `userid` int(10) NOT NULL,
  `msg` text NOT NULL,
  `datecreated` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_removal_shipping`
--

CREATE TABLE IF NOT EXISTS `amazonace_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazonace_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_template_att`
--

CREATE TABLE IF NOT EXISTS `amazonace_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazonace_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_template_category`
--

CREATE TABLE IF NOT EXISTS `amazonace_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazonace_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL,
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazonace_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_tlid_map_secondary`
--

CREATE TABLE IF NOT EXISTS `amazonace_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonace_unshipped_orders`
--

CREATE TABLE IF NOT EXISTS `amazonace_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazonblue_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=250 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazonblue_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=331 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazonblue_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_campaigns_categories`
--

CREATE TABLE IF NOT EXISTS `amazonblue_campaigns_categories` (
  `campid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazonblue_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_categories`
--

CREATE TABLE IF NOT EXISTS `amazonblue_categories` (
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `amazoncat` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_convert_log`
--

CREATE TABLE IF NOT EXISTS `amazonblue_convert_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_convert_mfn`
--

CREATE TABLE IF NOT EXISTS `amazonblue_convert_mfn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazonblue_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazonblue_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_errors`
--

CREATE TABLE IF NOT EXISTS `amazonblue_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazonblue_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12514 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_feedback`
--

CREATE TABLE IF NOT EXISTS `amazonblue_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_feedback_report`
--

CREATE TABLE IF NOT EXISTS `amazonblue_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazonblue_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','ORDERADJUSTMENT') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3018 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazonblue_inbound_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazonblue_inbound_shipping_extra` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poid` (`poid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_inbound_shipping_files`
--

CREATE TABLE IF NOT EXISTS `amazonblue_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_keywords`
--

CREATE TABLE IF NOT EXISTS `amazonblue_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_kit_quantity`
--

CREATE TABLE IF NOT EXISTS `amazonblue_kit_quantity` (
  `orderid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_log`
--

CREATE TABLE IF NOT EXISTS `amazonblue_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10233 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazonblue_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6995 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazonblue_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazonblue_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports_by_date`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports_by_date_items`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports_by_last_update`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports_by_last_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=408 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports_by_last_update_orders`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports_by_last_update_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1282 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_order_reports_by_last_update_orders_items`
--

CREATE TABLE IF NOT EXISTS `amazonblue_order_reports_by_last_update_orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1283 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazonblue_qty_scripts` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_refunds`
--

CREATE TABLE IF NOT EXISTS `amazonblue_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` int(10) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_relationships`
--

CREATE TABLE IF NOT EXISTS `amazonblue_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_removal_order`
--

CREATE TABLE IF NOT EXISTS `amazonblue_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_removal_order_complete`
--

CREATE TABLE IF NOT EXISTS `amazonblue_removal_order_complete` (
  `orderid` varchar(255) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_removal_order_info`
--

CREATE TABLE IF NOT EXISTS `amazonblue_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_removal_order_log`
--

CREATE TABLE IF NOT EXISTS `amazonblue_removal_order_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `report_sku` varchar(255) NOT NULL,
  `removalid` int(11) NOT NULL,
  `userid` int(10) NOT NULL,
  `msg` text NOT NULL,
  `datecreated` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_removal_shipping`
--

CREATE TABLE IF NOT EXISTS `amazonblue_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazonblue_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_template_att`
--

CREATE TABLE IF NOT EXISTS `amazonblue_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazonblue_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_template_category`
--

CREATE TABLE IF NOT EXISTS `amazonblue_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazonblue_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL,
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazonblue_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_tlid_map_secondary`
--

CREATE TABLE IF NOT EXISTS `amazonblue_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonblue_unshipped_orders`
--

CREATE TABLE IF NOT EXISTS `amazonblue_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazonmax_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`),
  KEY `listid_2` (`listid`),
  KEY `listid_3` (`listid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=352387 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazonmax_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) unsigned NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3684 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazonmax_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_campaigns_categories`
--

CREATE TABLE IF NOT EXISTS `amazonmax_campaigns_categories` (
  `campid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazonmax_campaign_lists` (
  `campid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listid` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `campid` (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_categories`
--

CREATE TABLE IF NOT EXISTS `amazonmax_categories` (
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `amazoncat` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_convert_log`
--

CREATE TABLE IF NOT EXISTS `amazonmax_convert_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_convert_mfn`
--

CREATE TABLE IF NOT EXISTS `amazonmax_convert_mfn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazonmax_duplicates` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazonmax_duplicates_extended` (
  `did` int(10) unsigned NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_errors`
--

CREATE TABLE IF NOT EXISTS `amazonmax_errors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) unsigned NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5011 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazonmax_errors_other` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37948 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_feedback`
--

CREATE TABLE IF NOT EXISTS `amazonmax_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_feedback_report`
--

CREATE TABLE IF NOT EXISTS `amazonmax_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazonmax_feed_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','ORDERADJUSTMENT') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41459 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazonmax_inbound_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazonmax_inbound_shipping_extra` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ais_id` int(10) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` int(10) NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poid` (`poid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_keywords`
--

CREATE TABLE IF NOT EXISTS `amazonmax_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_kit_quantity`
--

CREATE TABLE IF NOT EXISTS `amazonmax_kit_quantity` (
  `orderid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_log`
--

CREATE TABLE IF NOT EXISTS `amazonmax_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85736 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazonmax_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60384 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazonmax_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazonmax_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1578 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports_by_date`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports_by_date_items`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports_by_last_update`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports_by_last_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1987 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports_by_last_update_orders`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports_by_last_update_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2462 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_order_reports_by_last_update_orders_items`
--

CREATE TABLE IF NOT EXISTS `amazonmax_order_reports_by_last_update_orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2491 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazonmax_qty_scripts` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_refunds`
--

CREATE TABLE IF NOT EXISTS `amazonmax_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` int(10) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_relationships`
--

CREATE TABLE IF NOT EXISTS `amazonmax_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazonmax_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_template_att`
--

CREATE TABLE IF NOT EXISTS `amazonmax_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazonmax_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_template_category`
--

CREATE TABLE IF NOT EXISTS `amazonmax_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazonmax_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL,
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazonmax_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_tlid_map_secondary`
--

CREATE TABLE IF NOT EXISTS `amazonmax_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazonmax_unshipped_orders`
--

CREATE TABLE IF NOT EXISTS `amazonmax_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_base_messages` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` int(10) NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `deleteflag` tinyint(3) NOT NULL DEFAULT '0',
  `listid` smallint(5) NOT NULL DEFAULT '0',
  `sku` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`),
  KEY `listid` (`listid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=958 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_base_requests` (
  `brid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` smallint(5) NOT NULL,
  PRIMARY KEY (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`),
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `lastupdate` int(10) unsigned DEFAULT NULL,
  `min_qty` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_campaign_lists` (
  `campid` smallint(5) NOT NULL DEFAULT '0',
  `listid` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`campid`),
  KEY `listid` (`listid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_categories`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_categories` (
  `catid` int(10) NOT NULL DEFAULT '0',
  `amazoncat` int(10) NOT NULL DEFAULT '0',
  `pid` int(10) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_convert_log`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_convert_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_convert_mfn`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_convert_mfn` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_debug`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_debug` (
  `invoiceid` varchar(26) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_duplicates` (
  `did` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_duplicates_extended` (
  `did` int(10) NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_email_history`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_email_history` (
  `invoiceid` varchar(255) NOT NULL,
  PRIMARY KEY (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_errors`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_errors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_errors_other` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) NOT NULL DEFAULT '0',
  `campid` smallint(5) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_feed_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','CBA','ORDERADJUSTMENT','CONVERTSKUSAFN','CONVERTSKUSMFN') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_item_type`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_item_type` (
  `category_id` int(10) NOT NULL,
  `item_type` varchar(1000) NOT NULL,
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_kit_quantity`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_kit_quantity` (
  `orderid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_log`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_log_extended` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lid` int(10) unsigned NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_log_extended_skus` (
  `lid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_orders_uploaded` (
  `orderid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_order_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_order_reports_by_date`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_order_reports_by_date` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_order_reports_by_date_items`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_order_reports_by_date_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) unsigned NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amazon_order_id` (`amazon_order_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_order_shipmethod`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_order_shipmethod` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` int(10) NOT NULL,
  `shipmethod` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_qty_scripts` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_alpha_reimbursements`
--

CREATE TABLE IF NOT EXISTS `amazon_alpha_reimbursements` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reimbursement_id` int(10) NOT NULL,
  `tstamp` varchar(255) NOT NULL,
  `tstamp_est` int(10) NOT NULL,
  `amazon_orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL,
  `amount` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazon_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(50) DEFAULT NULL,
  KEY `mid` (`mid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4229 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazon_base_requests` (
  `brid` bigint(20) NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39846 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazon_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` bigint(20) DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_campaigns_categories`
--

CREATE TABLE IF NOT EXISTS `amazon_campaigns_categories` (
  `campid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazon_campaign_lists` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_categories`
--

CREATE TABLE IF NOT EXISTS `amazon_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `amazoncat` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_base_messages`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL DEFAULT '',
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `listid` int(11) NOT NULL DEFAULT '0',
  `deleteflag` int(11) NOT NULL DEFAULT '0',
  KEY `mid` (`mid`),
  KEY `pid` (`pid`),
  KEY `pid_lastupdate` (`pid_lastupdate`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46166 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_base_requests`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_base_requests` (
  `brid` bigint(20) NOT NULL AUTO_INCREMENT,
  `amazon_submissionid` varchar(50) DEFAULT NULL,
  `amazon_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `amazon_submissionid` (`amazon_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=836 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_campaigns`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` bigint(20) DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_campaign_lists` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_categories`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `amazoncat` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`amazoncat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_duplicates` (
  `did` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_duplicates_extended` (
  `did` bigint(20) NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_errors`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=820 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_errors_other` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4305 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_feedback`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_feed_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11876 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping_extra` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ais_id` bigint(20) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` bigint(2) NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_inbound_shipping_files`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_log`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18576 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16748 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_order_reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=132 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_qty_scripts` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_relationships`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_shipping_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `amazon_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `weight_min` double(16,2) NOT NULL DEFAULT '0.00',
  `weight_max` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_min` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_att`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `catid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_category`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL COMMENT 'set to true if the attribute is generic to the category id',
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_value_cat`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_value_cat` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_catid` bigint(20) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_template_value_pid`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_template_value_pid` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_field` varchar(255) NOT NULL,
  `amz_value` varchar(255) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `slid` bigint(20) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_tlid_map_secondary`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ca_unshipped_orders`
--

CREATE TABLE IF NOT EXISTS `amazon_ca_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_convert_log`
--

CREATE TABLE IF NOT EXISTS `amazon_convert_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3960 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_convert_mfn`
--

CREATE TABLE IF NOT EXISTS `amazon_convert_mfn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=465 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_debug`
--

CREATE TABLE IF NOT EXISTS `amazon_debug` (
  `invoiceid` varchar(26) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_duplicates`
--

CREATE TABLE IF NOT EXISTS `amazon_duplicates` (
  `did` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_duplicates_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_duplicates_extended` (
  `did` bigint(20) NOT NULL DEFAULT '0',
  `asin` varchar(50) DEFAULT NULL,
  `salesrank` varchar(20) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `upc` varchar(50) DEFAULT NULL,
  `features` text,
  `lowestprice` varchar(50) DEFAULT NULL,
  `numsellers` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `did` (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_errors`
--

CREATE TABLE IF NOT EXISTS `amazon_errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_errors_other`
--

CREATE TABLE IF NOT EXISTS `amazon_errors_other` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4023896 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feedback`
--

CREATE TABLE IF NOT EXISTS `amazon_feedback` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  `solicited` int(5) NOT NULL DEFAULT '0',
  `removed` int(5) NOT NULL DEFAULT '0',
  `follow_up` tinyint(3) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feedback_canada`
--

CREATE TABLE IF NOT EXISTS `amazon_feedback_canada` (
  `fbid` int(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `ontime` varchar(10) NOT NULL,
  `described` varchar(10) NOT NULL,
  `custservice` varchar(10) NOT NULL,
  `orderid` varchar(25) NOT NULL,
  `rater_email` varchar(100) NOT NULL,
  `rater_role` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `solicited` tinyint(3) NOT NULL,
  `removed` tinyint(3) NOT NULL,
  `follow_up` tinyint(3) NOT NULL,
  KEY `fbid` (`fbid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feedback_report`
--

CREATE TABLE IF NOT EXISTS `amazon_feedback_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feedback_report_canada`
--

CREATE TABLE IF NOT EXISTS `amazon_feedback_report_canada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_feed_report`
--

CREATE TABLE IF NOT EXISTS `amazon_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` enum('BASE','BASE_TAX','BASE_CF','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','CBA','ORDERADJUSTMENT','CONVERTSKUSAFN','CONVERTSKUSMFN','REIMBURSEMENTS','RETURNS') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46584 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping`
--

CREATE TABLE IF NOT EXISTS `amazon_inbound_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(50) NOT NULL,
  `shipment_name` varchar(50) NOT NULL,
  `shipment_address_name` varchar(100) NOT NULL,
  `shipment_address_line1` varchar(100) NOT NULL,
  `shipment_address_line2` varchar(100) NOT NULL,
  `shipment_district_county` varchar(50) NOT NULL,
  `shipment_city` varchar(50) NOT NULL,
  `shipment_state` varchar(50) NOT NULL,
  `shipment_country` varchar(50) NOT NULL,
  `shipment_postal` varchar(50) NOT NULL,
  `destination_fulfillment_center_id` varchar(50) NOT NULL,
  `shipment_status` varchar(50) NOT NULL,
  `label_prep_type` varchar(50) NOT NULL,
  `shipment_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping_extra`
--

CREATE TABLE IF NOT EXISTS `amazon_inbound_shipping_extra` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ais_id` bigint(20) NOT NULL,
  `mgr_notes` varchar(255) NOT NULL,
  `clerk_notes` varchar(255) NOT NULL,
  `in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `claim` varchar(255) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` bigint(2) NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_inbound_shipping_files`
--

CREATE TABLE IF NOT EXISTS `amazon_inbound_shipping_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbound_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_item_type`
--

CREATE TABLE IF NOT EXISTS `amazon_item_type` (
  `category_id` int(10) NOT NULL,
  `item_type` varchar(1000) NOT NULL,
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_keywords`
--

CREATE TABLE IF NOT EXISTS `amazon_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_kit_quantity`
--

CREATE TABLE IF NOT EXISTS `amazon_kit_quantity` (
  `orderid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  `ampid` varchar(24) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_log`
--

CREATE TABLE IF NOT EXISTS `amazon_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=315501 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_log_extended`
--

CREATE TABLE IF NOT EXISTS `amazon_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=277079 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `amazon_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_notfound`
--

CREATE TABLE IF NOT EXISTS `amazon_notfound` (
  `title` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `asin` varchar(255) DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_notfound_remainder`
--

CREATE TABLE IF NOT EXISTS `amazon_notfound_remainder` (
  `title` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `asin` varchar(255) DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `amazon_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37967 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_date`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_date_items`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_date_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `amazon_order_purchase_date` int(10) NOT NULL,
  `amazon_order_status` varchar(50) NOT NULL,
  `amazon_sales_chanel` varchar(50) NOT NULL,
  `amazon_item_asin` varchar(50) NOT NULL,
  `amazon_item_sku` varchar(50) NOT NULL,
  `amazon_item_status` varchar(50) NOT NULL,
  `amazon_item_qty` smallint(5) NOT NULL,
  `amazon_order_purchase_date_string` varchar(100) NOT NULL,
  `type` tinyint(5) NOT NULL,
  `pid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98537 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5582 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update_canada`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_canada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5141 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update_orders`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=152194 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update_orders_canada`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_orders_canada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` varchar(100) NOT NULL,
  `purchase_date` varchar(100) NOT NULL,
  `purchase_timestamp` int(10) unsigned NOT NULL,
  `purchase_timestamp_pst` int(10) unsigned NOT NULL,
  `lastupdate_date` varchar(100) NOT NULL,
  `lastupdate_timestamp` int(10) unsigned NOT NULL,
  `lastupdate_timestamp_pst` int(10) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `sales_channel` varchar(50) NOT NULL,
  `fulfillment_channel` varchar(50) NOT NULL,
  `ship_service_level` varchar(50) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_state` varchar(100) NOT NULL,
  `address_zipcode` varchar(50) NOT NULL,
  `address_country` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2611 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update_orders_items`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_orders_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=154868 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_reports_by_last_update_orders_items_canada`
--

CREATE TABLE IF NOT EXISTS `amazon_order_reports_by_last_update_orders_items_canada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `amazon_orderid` varchar(100) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `item_status` varchar(50) NOT NULL,
  `product_name` text NOT NULL,
  `qty` int(10) NOT NULL,
  `price_principal` double(16,2) NOT NULL,
  `price_tax` double(16,2) NOT NULL,
  `price_shipping` double(16,2) NOT NULL,
  `price_giftwrap` double(16,2) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `amazon_orderid` (`amazon_orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2694 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_order_shipmethod`
--

CREATE TABLE IF NOT EXISTS `amazon_order_shipmethod` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` int(10) NOT NULL,
  `shipmethod` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2954 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_qty_scripts`
--

CREATE TABLE IF NOT EXISTS `amazon_qty_scripts` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `script` longblob,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_refunds`
--

CREATE TABLE IF NOT EXISTS `amazon_refunds` (
  `invoiceid` varchar(26) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `ampid` varchar(24) NOT NULL,
  `creditid` varchar(26) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `tstamp` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  KEY `orderid` (`orderid`,`ampid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `creditid` (`creditid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_relationships`
--

CREATE TABLE IF NOT EXISTS `amazon_relationships` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `relpid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_removal_order`
--

CREATE TABLE IF NOT EXISTS `amazon_removal_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `lastupdate_date` int(10) unsigned NOT NULL,
  `lastupdate_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `requested_qty` int(10) NOT NULL,
  `cancelled_qty` int(10) NOT NULL,
  `disposed_qty` int(10) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `inprocess_qty` int(11) NOT NULL,
  `removal_fee` double(16,2) NOT NULL,
  `efluent_pid` int(10) NOT NULL,
  `received_qty` int(10) NOT NULL,
  `moved_qty` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=373 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_removal_order_complete`
--

CREATE TABLE IF NOT EXISTS `amazon_removal_order_complete` (
  `orderid` varchar(255) NOT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_removal_order_info`
--

CREATE TABLE IF NOT EXISTS `amazon_removal_order_info` (
  `aro_id` int(10) NOT NULL,
  `complete` tinyint(3) NOT NULL,
  `mgr_review` tinyint(3) NOT NULL,
  `user_flag` tinyint(3) NOT NULL,
  `clerk_notes` text,
  `mgr_notes` text,
  PRIMARY KEY (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_removal_order_log`
--

CREATE TABLE IF NOT EXISTS `amazon_removal_order_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) NOT NULL,
  `pid` int(10) NOT NULL,
  `report_sku` varchar(255) NOT NULL,
  `removalid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `msg` text NOT NULL,
  `datecreated` int(10) NOT NULL,
  `type` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`),
  KEY `removalid` (`removalid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1059 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_removal_shipping`
--

CREATE TABLE IF NOT EXISTS `amazon_removal_shipping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` int(10) unsigned NOT NULL,
  `request_date_org` varchar(255) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `shipment_date_org` varchar(255) NOT NULL,
  `orderid` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `fnsku` varchar(255) NOT NULL,
  `disposition` varchar(255) NOT NULL,
  `shipped_qty` int(10) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `trackno` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sku` (`sku`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1338 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_shipped_temp`
--

CREATE TABLE IF NOT EXISTS `amazon_shipped_temp` (
  `invoiceid` varchar(32) DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_shipping_map`
--

CREATE TABLE IF NOT EXISTS `amazon_shipping_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `amazon_method` varchar(50) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `weight_min` double(16,2) NOT NULL DEFAULT '0.00',
  `weight_max` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_min` double(16,2) NOT NULL DEFAULT '0.00',
  `cost_max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`amazon_method`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_ss_map`
--

CREATE TABLE IF NOT EXISTS `amazon_ss_map` (
  `sscid` bigint(20) NOT NULL DEFAULT '0',
  `amzkid` bigint(20) NOT NULL DEFAULT '0',
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_temp`
--

CREATE TABLE IF NOT EXISTS `amazon_temp` (
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `listing_id` varchar(255) DEFAULT NULL,
  `seller_sku` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `open_date` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `item_is_marketplace` varchar(255) DEFAULT NULL,
  `product_id_type` varchar(255) DEFAULT NULL,
  `zshop_shipping_fee` varchar(255) DEFAULT NULL,
  `item_note` varchar(255) DEFAULT NULL,
  `item_condition` varchar(255) DEFAULT NULL,
  `zshop_category1` varchar(255) DEFAULT NULL,
  `zshop_browse_path` varchar(255) DEFAULT NULL,
  `zshop_storefront_feature` varchar(255) DEFAULT NULL,
  `asin1` varchar(255) DEFAULT NULL,
  `asin2` varchar(255) DEFAULT NULL,
  `asin3` varchar(255) DEFAULT NULL,
  `will_ship_internationally` varchar(255) DEFAULT NULL,
  `expedited_shipping` varchar(255) DEFAULT NULL,
  `zshop_boldface` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `bid_for_featured_placement` varchar(255) DEFAULT NULL,
  `add_delete` varchar(255) DEFAULT NULL,
  `pending_quantity` varchar(255) DEFAULT NULL,
  `fulfillment_channel` varchar(255) DEFAULT NULL,
  `business_price` varchar(255) DEFAULT NULL,
  `Quantity_Price_Type` varchar(255) DEFAULT NULL,
  `Quantity_Lower_Bound_1` varchar(255) DEFAULT NULL,
  `Quantity_Price_1` varchar(255) DEFAULT NULL,
  `Quantity_Lower_Bound_2` varchar(255) DEFAULT NULL,
  `Quantity_Price_2` varchar(255) DEFAULT NULL,
  `Quantity_Lower_Bound_3` varchar(255) DEFAULT NULL,
  `Quantity_Price_3` varchar(255) DEFAULT NULL,
  `Quantity_Lower_Bound_4` varchar(255) DEFAULT NULL,
  `Quantity_Price_4` varchar(255) DEFAULT NULL,
  `Quantity_Lower_Bound_5` varchar(255) DEFAULT NULL,
  `Quantity_Price_5` varchar(255) DEFAULT NULL,
  KEY `seller_sku` (`seller_sku`),
  KEY `asin1` (`asin1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_att`
--

CREATE TABLE IF NOT EXISTS `amazon_template_att` (
  `att_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_amz` varchar(255) NOT NULL,
  `att_name` varchar(255) NOT NULL,
  `catid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_att_values`
--

CREATE TABLE IF NOT EXISTS `amazon_template_att_values` (
  `att_value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `att_id` bigint(20) NOT NULL,
  `att_value` varchar(255) NOT NULL,
  PRIMARY KEY (`att_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5307 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_category`
--

CREATE TABLE IF NOT EXISTS `amazon_template_category` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_category_attributes`
--

CREATE TABLE IF NOT EXISTS `amazon_template_category_attributes` (
  `attid` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `amz_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `min_occurs` int(10) NOT NULL,
  `max_occurs` int(10) NOT NULL,
  `catid` bigint(20) NOT NULL,
  `is_generic` char(1) DEFAULT NULL COMMENT 'set to true if the attribute is generic to the category id',
  PRIMARY KEY (`attid`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1954 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_value_cat`
--

CREATE TABLE IF NOT EXISTS `amazon_template_value_cat` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_catid` bigint(20) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_template_value_pid`
--

CREATE TABLE IF NOT EXISTS `amazon_template_value_pid` (
  `amz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amz_pid` bigint(20) NOT NULL,
  `amz_field` varchar(255) NOT NULL,
  `amz_value` varchar(255) NOT NULL,
  PRIMARY KEY (`amz_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=258 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_tlid_map`
--

CREATE TABLE IF NOT EXISTS `amazon_tlid_map` (
  `tid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `slid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_tlid_map_secondary`
--

CREATE TABLE IF NOT EXISTS `amazon_tlid_map_secondary` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `tlid` bigint(20) NOT NULL DEFAULT '0',
  `override_rid` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_tmp`
--

CREATE TABLE IF NOT EXISTS `amazon_tmp` (
  `title` varchar(255) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `asin` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amazon_unshipped_orders`
--

CREATE TABLE IF NOT EXISTS `amazon_unshipped_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `dayslate` int(11) NOT NULL DEFAULT '0',
  KEY `invoiceid` (`invoiceid`),
  KEY `dayslate` (`dayslate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amz_alan`
--

CREATE TABLE IF NOT EXISTS `amz_alan` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `asin` varchar(200) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `brand` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`dataid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=710925 ;

-- --------------------------------------------------------

--
-- Table structure for table `attributenames`
--

CREATE TABLE IF NOT EXISTS `attributenames` (
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(110) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `attributenames_attributeID` (`attributeid`),
  KEY `attributenames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autopackages`
--

CREATE TABLE IF NOT EXISTS `autopackages` (
  `apid` bigint(20) NOT NULL AUTO_INCREMENT,
  `packagename` varchar(255) DEFAULT NULL,
  `packagetitle` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `markup_dollar` double(16,2) DEFAULT NULL,
  `markup_percent` double(16,2) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `subtitlecfid` int(11) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `regenerate` int(11) NOT NULL DEFAULT '0',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  KEY `apid` (`apid`),
  KEY `packagename` (`packagename`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `autopackage_addskus`
--

CREATE TABLE IF NOT EXISTS `autopackage_addskus` (
  `apsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '1',
  KEY `apsid` (`apsid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `autopackage_criteria`
--

CREATE TABLE IF NOT EXISTS `autopackage_criteria` (
  `apcid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `min` double(16,2) NOT NULL DEFAULT '0.00',
  `max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `apcid` (`apcid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `autopackage_sites`
--

CREATE TABLE IF NOT EXISTS `autopackage_sites` (
  `apid` bigint(20) NOT NULL,
  `divid` bigint(20) NOT NULL,
  KEY `apid` (`apid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autopackage_skus`
--

CREATE TABLE IF NOT EXISTS `autopackage_skus` (
  `pid` bigint(20) NOT NULL,
  `apid` bigint(20) NOT NULL,
  `mainid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `apid` (`apid`),
  KEY `pid` (`pid`),
  KEY `mainid` (`mainid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autoprice`
--

CREATE TABLE IF NOT EXISTS `autoprice` (
  `pid` bigint(20) NOT NULL,
  `aggressive_factor` double(16,2) DEFAULT NULL,
  `minimum_profit` double(16,2) DEFAULT NULL,
  `profit_type` enum('DOLLAR','PERCENT') DEFAULT NULL,
  `use_avgcost` enum('AVGCOST','WAREHOUSE','VENDOR') DEFAULT NULL,
  `maximum_profit` double(16,2) DEFAULT '0.00',
  `max_profit_type` enum('DOLLAR','PERCENT') DEFAULT NULL,
  `commission` double(16,2) DEFAULT '0.00',
  `ceiling_price` double(16,2) DEFAULT '0.00',
  `fight_amazon` int(11) NOT NULL DEFAULT '0',
  `add_fba_cost` int(11) NOT NULL DEFAULT '0',
  `minprice` double(16,2) DEFAULT '0.00',
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autoprice_amazon`
--

CREATE TABLE IF NOT EXISTS `autoprice_amazon` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `autoprice_batches`
--

CREATE TABLE IF NOT EXISTS `autoprice_batches` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `numskus` int(11) NOT NULL DEFAULT '0',
  `pctcomplete` double NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `autoprice_batches_skus`
--

CREATE TABLE IF NOT EXISTS `autoprice_batches_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apbid` bigint(20) NOT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `result` tinytext,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `apbid` (`apbid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=165 ;

-- --------------------------------------------------------

--
-- Table structure for table `autoprice_extended`
--

CREATE TABLE IF NOT EXISTS `autoprice_extended` (
  `pid` bigint(20) NOT NULL,
  `avgcost_id` int(11) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auto_datefeed_log`
--

CREATE TABLE IF NOT EXISTS `auto_datefeed_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `numitems` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=259 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_datefeed_log_items`
--

CREATE TABLE IF NOT EXISTS `auto_datefeed_log_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logid` int(10) NOT NULL,
  `itemid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44012 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_shipping`
--

CREATE TABLE IF NOT EXISTS `auto_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=587 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_shipping_details`
--

CREATE TABLE IF NOT EXISTS `auto_shipping_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asid` int(10) NOT NULL COMMENT 'uniq ID from auto_shipping table',
  `zone` tinyint(4) NOT NULL,
  `carrier` varchar(250) NOT NULL,
  `method` varchar(250) NOT NULL,
  `boxes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `asid` (`asid`,`zone`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4670 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_shipping_ignore`
--

CREATE TABLE IF NOT EXISTS `auto_shipping_ignore` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_shipping_items`
--

CREATE TABLE IF NOT EXISTS `auto_shipping_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_shipping_items_results`
--

CREATE TABLE IF NOT EXISTS `auto_shipping_items_results` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bsid` int(10) NOT NULL,
  `invoiceid` varchar(255) DEFAULT NULL,
  `resultline` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `bsid` (`bsid`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `background_jobs`
--

CREATE TABLE IF NOT EXISTS `background_jobs` (
  `jobid` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` enum('PENDING','RUNNING','COMPLETE') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `jobid` (`jobid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=278067 ;

-- --------------------------------------------------------

--
-- Table structure for table `background_job_skus`
--

CREATE TABLE IF NOT EXISTS `background_job_skus` (
  `jobid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `listid` bigint(20) NOT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `jobid` (`jobid`),
  KEY `complete` (`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=278067 ;

-- --------------------------------------------------------

--
-- Table structure for table `banners_manager`
--

CREATE TABLE IF NOT EXISTS `banners_manager` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `shop` char(2) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `rotation` int(11) DEFAULT NULL,
  `img_data` longblob,
  `img_rich` text,
  `tstamp` datetime DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `alt_data` longblob NOT NULL,
  `impressions` int(11) NOT NULL,
  `lp` int(11) NOT NULL,
  `rp` int(11) NOT NULL,
  `tp` int(11) NOT NULL,
  `bp` int(11) NOT NULL,
  `inote` longblob NOT NULL,
  `bwidth` int(11) NOT NULL,
  `bheight` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `divid` int(5) NOT NULL,
  KEY `id` (`id`),
  KEY `position` (`position`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `banner_impressions`
--

CREATE TABLE IF NOT EXISTS `banner_impressions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bannerid` bigint(20) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `referrer` text,
  `ipaddr` varchar(26) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `bannerid` (`bannerid`,`tstamp`),
  KEY `ipaddr` (`ipaddr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banner_positions`
--

CREATE TABLE IF NOT EXISTS `banner_positions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `shortdescr` text,
  `longdescr` text,
  `tp` int(11) NOT NULL DEFAULT '0',
  `lp` int(11) NOT NULL DEFAULT '0',
  `rp` int(11) NOT NULL DEFAULT '0',
  `bp` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Table structure for table `benefit_members`
--

CREATE TABLE IF NOT EXISTS `benefit_members` (
  `emailaddr` varchar(255) DEFAULT NULL,
  KEY `emailaddr` (`emailaddr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bfly_inventory`
--

CREATE TABLE IF NOT EXISTS `bfly_inventory` (
  `sku` varchar(50) DEFAULT NULL,
  `inventory` double(16,2) DEFAULT NULL,
  `avgcost` double(16,2) DEFAULT NULL,
  `totalcost` double(16,2) DEFAULT NULL,
  `throwA` varchar(50) DEFAULT NULL,
  `throwB` varchar(50) DEFAULT NULL,
  `throwC` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bfly_remaining`
--

CREATE TABLE IF NOT EXISTS `bfly_remaining` (
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bfly_skulist`
--

CREATE TABLE IF NOT EXISTS `bfly_skulist` (
  `sku` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `itemtype` varchar(100) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`),
  KEY `manpart` (`manpart`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bilist`
--

CREATE TABLE IF NOT EXISTS `bilist` (
  `id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brand_mapper`
--

CREATE TABLE IF NOT EXISTS `brand_mapper` (
  `mannameA` varchar(50) DEFAULT NULL,
  `mannameB` varchar(50) DEFAULT NULL,
  KEY `mannameA` (`mannameA`,`mannameB`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `btg_categories`
--

CREATE TABLE IF NOT EXISTS `btg_categories` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  `nodeid` bigint(20) NOT NULL,
  `query` varchar(255) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24287 ;

-- --------------------------------------------------------

--
-- Table structure for table `btg_categories_map`
--

CREATE TABLE IF NOT EXISTS `btg_categories_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL,
  `btgcat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `btg_categories_pid_map`
--

CREATE TABLE IF NOT EXISTS `btg_categories_pid_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `btgcat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `bulkship`
--

CREATE TABLE IF NOT EXISTS `bulkship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `bulkship_results`
--

CREATE TABLE IF NOT EXISTS `bulkship_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bsid` bigint(20) NOT NULL,
  `invoiceid` varchar(255) DEFAULT NULL,
  `resultline` varchar(255) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `bsid` (`bsid`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Table structure for table `bundles`
--

CREATE TABLE IF NOT EXISTS `bundles` (
  `apid` bigint(20) NOT NULL AUTO_INCREMENT,
  `packagename` varchar(255) DEFAULT NULL,
  `packagetitle` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `subtitlecfid` int(11) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `regenerate` int(11) NOT NULL DEFAULT '0',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `bdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  KEY `apid` (`apid`),
  KEY `packagename` (`packagename`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `bundles_addskus`
--

CREATE TABLE IF NOT EXISTS `bundles_addskus` (
  `apsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `dollar` double(16,2) NOT NULL DEFAULT '0.00',
  `percent` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `apsid` (`apsid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `bundles_criteria`
--

CREATE TABLE IF NOT EXISTS `bundles_criteria` (
  `apcid` bigint(20) NOT NULL AUTO_INCREMENT,
  `apid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `min` double(16,2) NOT NULL DEFAULT '0.00',
  `max` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `apcid` (`apcid`),
  KEY `apid` (`apid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `bundles_skus`
--

CREATE TABLE IF NOT EXISTS `bundles_skus` (
  `pid` bigint(20) NOT NULL,
  `apid` bigint(20) NOT NULL,
  `mainid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `bundleid` varchar(128) DEFAULT NULL,
  KEY `apid` (`apid`),
  KEY `pid` (`pid`),
  KEY `mainid` (`mainid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buysafe_orders`
--

CREATE TABLE IF NOT EXISTS `buysafe_orders` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `shoppingcartid` varchar(255) DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buy_base_messages`
--

CREATE TABLE IF NOT EXISTS `buy_base_messages` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `pid_lastupdate` bigint(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `deleteflag` int(11) NOT NULL,
  `listid` int(11) NOT NULL,
  KEY `mid` (`mid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_base_requests`
--

CREATE TABLE IF NOT EXISTS `buy_base_requests` (
  `brid` bigint(20) NOT NULL AUTO_INCREMENT,
  `buy_submissionid` varchar(50) DEFAULT NULL,
  `buy_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` bigint(20) NOT NULL,
  KEY `brid` (`brid`),
  KEY `buy_submissionid` (`buy_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_campaigns`
--

CREATE TABLE IF NOT EXISTS `buy_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` bigint(20) DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `buy_campaign_lists` (
  `campid` bigint(20) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buy_categories`
--

CREATE TABLE IF NOT EXISTS `buy_categories` (
  `catid` int(10) NOT NULL,
  `buycat` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buy_categories_api`
--

CREATE TABLE IF NOT EXISTS `buy_categories_api` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buycat` int(10) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9359 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_categories_api2`
--

CREATE TABLE IF NOT EXISTS `buy_categories_api2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buycat` int(10) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=219 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_categories_api_old`
--

CREATE TABLE IF NOT EXISTS `buy_categories_api_old` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buycat` int(10) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3933 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_categories_pid`
--

CREATE TABLE IF NOT EXISTS `buy_categories_pid` (
  `pid` bigint(20) NOT NULL,
  `buycat` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buy_duplicates`
--

CREATE TABLE IF NOT EXISTS `buy_duplicates` (
  `did` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_errors`
--

CREATE TABLE IF NOT EXISTS `buy_errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`,`campid`,`requestid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_errors_other`
--

CREATE TABLE IF NOT EXISTS `buy_errors_other` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` bigint(20) NOT NULL DEFAULT '0',
  `campid` bigint(20) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_feed_report`
--

CREATE TABLE IF NOT EXISTS `buy_feed_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_feed_respons`
--

CREATE TABLE IF NOT EXISTS `buy_feed_respons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_kit_orders`
--

CREATE TABLE IF NOT EXISTS `buy_kit_orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` int(10) NOT NULL,
  `kitid` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_listing`
--

CREATE TABLE IF NOT EXISTS `buy_listing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `buy_pid` bigint(20) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_log`
--

CREATE TABLE IF NOT EXISTS `buy_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_log_extended`
--

CREATE TABLE IF NOT EXISTS `buy_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `buy_log_extended_skus` (
  `lid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buy_log_orders`
--

CREATE TABLE IF NOT EXISTS `buy_log_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(50) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `shipped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_log_orders_items`
--

CREATE TABLE IF NOT EXISTS `buy_log_orders_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(50) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `receipt_item_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_prelisting`
--

CREATE TABLE IF NOT EXISTS `buy_prelisting` (
  `pid` bigint(20) NOT NULL,
  `campid` bigint(20) NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cache_channel_sales`
--

CREATE TABLE IF NOT EXISTS `cache_channel_sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `type` enum('DOLLAR','COUNT','COST') DEFAULT NULL,
  `web` double(16,2) DEFAULT NULL,
  `phone` double(16,2) DEFAULT NULL,
  `amazon` double(16,2) DEFAULT NULL,
  `ebay` double(16,2) DEFAULT NULL,
  `buycom` double(16,2) DEFAULT NULL,
  `wholesale` double(16,2) DEFAULT NULL,
  `claim` double(16,2) DEFAULT NULL,
  `fba` double(16,2) DEFAULT NULL,
  `amazonca` double(16,2) DEFAULT NULL,
  `ebayace` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7231 ;

-- --------------------------------------------------------

--
-- Table structure for table `cache_sales_by_cat`
--

CREATE TABLE IF NOT EXISTS `cache_sales_by_cat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `sales` double(16,2) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=107524 ;

-- --------------------------------------------------------

--
-- Table structure for table `cache_sales_by_rep`
--

CREATE TABLE IF NOT EXISTS `cache_sales_by_rep` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `repid` bigint(20) DEFAULT NULL,
  `sales` double(16,2) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`repid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19382 ;

-- --------------------------------------------------------

--
-- Table structure for table `cardinal_results`
--

CREATE TABLE IF NOT EXISTS `cardinal_results` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `xid` varchar(128) DEFAULT NULL,
  `cavv` varchar(128) DEFAULT NULL,
  `eci` varchar(26) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart_sessions`
--

CREATE TABLE IF NOT EXISTS `cart_sessions` (
  `sessionid` varchar(128) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `icount` int(11) DEFAULT NULL,
  `data` longblob,
  `bond` int(11) NOT NULL DEFAULT '0',
  `bondvalue` double(16,2) DEFAULT NULL,
  `orderid` varchar(50) DEFAULT NULL,
  `alwaysid` varchar(255) DEFAULT NULL,
  `csmethod` varchar(255) NOT NULL DEFAULT '',
  KEY `sessionid` (`sessionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buycatid` bigint(20) NOT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `parentid` (`parentid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=529 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories_new`
--

CREATE TABLE IF NOT EXISTS `categories_new` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buycat` int(10) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6986 ;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `parentcategoryid` int(11) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ordernumber` int(11) NOT NULL DEFAULT '0',
  `catlevel` tinyint(4) NOT NULL DEFAULT '0',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `catery_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorydisplayattributes`
--

CREATE TABLE IF NOT EXISTS `categorydisplayattributes` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `templatetype` int(11) NOT NULL DEFAULT '0',
  `defaultdisplayorder` int(11) NOT NULL DEFAULT '1',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `caterydisplayattributes_hID` (`headerid`),
  KEY `caterydisplayattributes_cID` (`categoryid`),
  KEY `caterydisplayattributes_aID` (`attributeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categoryheader`
--

CREATE TABLE IF NOT EXISTS `categoryheader` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `templatetype` int(11) NOT NULL DEFAULT '0',
  `defaultdisplayorder` int(11) NOT NULL DEFAULT '1',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `cateryheader_headerID` (`headerid`),
  KEY `cateryheader_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorynames`
--

CREATE TABLE IF NOT EXISTS `categorynames` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `caterynames_categoryID` (`categoryid`),
  KEY `caterynames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorysearchattributes`
--

CREATE TABLE IF NOT EXISTS `categorysearchattributes` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ispreferred` tinyint(1) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `caterysearchattributes_aID` (`attributeid`),
  KEY `caterysearchattributes_cID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_images`
--

CREATE TABLE IF NOT EXISTS `category_images` (
  `catid` bigint(20) DEFAULT NULL,
  `banner` longblob,
  `banner_filename` varchar(255) DEFAULT NULL,
  `thumb` longblob,
  `thumb_filename` varchar(255) DEFAULT NULL,
  `divid` int(5) NOT NULL,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_images_store`
--

CREATE TABLE IF NOT EXISTS `category_images_store` (
  `catid` bigint(20) DEFAULT NULL,
  `banner` longblob,
  `banner_filename` varchar(255) DEFAULT NULL,
  `thumb` longblob,
  `thumb_filename` varchar(255) DEFAULT NULL,
  `divid` int(10) NOT NULL,
  `manid` int(10) NOT NULL,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_taxonomy`
--

CREATE TABLE IF NOT EXISTS `category_taxonomy` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `taxonomy` text,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc_errors`
--

CREATE TABLE IF NOT EXISTS `cc_errors` (
  `invoiceid` varchar(50) DEFAULT NULL,
  `debuginfo` longblob,
  `tstamp` datetime DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_log`
--

CREATE TABLE IF NOT EXISTS `chat_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `msg_to` bigint(20) NOT NULL DEFAULT '0',
  `msg_from` bigint(20) NOT NULL DEFAULT '0',
  `msg_body` longblob,
  `shown` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `msg_to` (`msg_to`),
  KEY `msg_from` (`msg_from`),
  KEY `shown` (`shown`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=841 ;

-- --------------------------------------------------------

--
-- Table structure for table `cmic_drops`
--

CREATE TABLE IF NOT EXISTS `cmic_drops` (
  `sku` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_orderflow`
--

CREATE TABLE IF NOT EXISTS `company_orderflow` (
  `step` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `explanation` text,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `failure` int(11) NOT NULL DEFAULT '0',
  `success` int(11) NOT NULL DEFAULT '0',
  `alt_step1` int(11) NOT NULL DEFAULT '0',
  `alt_step2` int(11) NOT NULL DEFAULT '0',
  `alt_step3` int(11) NOT NULL DEFAULT '0',
  `authorize` int(11) NOT NULL DEFAULT '0',
  `capture` int(11) NOT NULL DEFAULT '0',
  `charge` int(11) NOT NULL DEFAULT '0',
  `pick_submit` int(11) NOT NULL DEFAULT '0',
  `ship` int(11) NOT NULL DEFAULT '0',
  `complete` int(11) NOT NULL DEFAULT '0',
  `assign` int(11) NOT NULL DEFAULT '0',
  `email` int(11) NOT NULL DEFAULT '0',
  `emailid` int(11) NOT NULL DEFAULT '0',
  `script` int(11) NOT NULL DEFAULT '0',
  `assign_sales` int(11) NOT NULL DEFAULT '0',
  `manual` int(11) NOT NULL DEFAULT '0',
  `refund` int(11) NOT NULL DEFAULT '0',
  `editable` int(11) NOT NULL DEFAULT '0',
  `anypush` int(11) NOT NULL DEFAULT '0',
  `zerobal` int(11) NOT NULL DEFAULT '0',
  `exclude` int(11) NOT NULL DEFAULT '0',
  `dropship` int(11) NOT NULL DEFAULT '0',
  `sendinvoice` int(11) NOT NULL DEFAULT '0',
  KEY `step` (`step`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_orderflow_onstep`
--

CREATE TABLE IF NOT EXISTS `company_orderflow_onstep` (
  `orderid` bigint(20) DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT '0',
  KEY `orderid` (`orderid`,`step`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_orderflow_scripts`
--

CREATE TABLE IF NOT EXISTS `company_orderflow_scripts` (
  `ofid` bigint(20) DEFAULT NULL,
  `script` longblob,
  KEY `ofid` (`ofid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_shipping_assigned`
--

CREATE TABLE IF NOT EXISTS `company_shipping_assigned` (
  `id` bigint(20) DEFAULT NULL,
  `csmid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `grpid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `subid` bigint(20) DEFAULT NULL,
  `ppr` double(16,2) DEFAULT NULL,
  `ppr_min` double(16,2) DEFAULT NULL,
  `ppr_max` double(16,2) DEFAULT NULL,
  `flatrate` double(16,2) DEFAULT NULL,
  `flatrate_incremental` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `csmid` (`csmid`),
  KEY `pid` (`pid`),
  KEY `grpid` (`grpid`,`catid`,`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_shipping_methods`
--

CREATE TABLE IF NOT EXISTS `company_shipping_methods` (
  `id` bigint(20) DEFAULT NULL,
  `method` varchar(128) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `default_method` int(11) DEFAULT NULL,
  `ppr` double(16,2) DEFAULT NULL,
  `ppr_min` double(16,2) DEFAULT NULL,
  `ppr_max` double(16,2) DEFAULT NULL,
  `flatrate` double(16,2) DEFAULT NULL,
  `flatrate_incremental` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `compatibility_lists`
--

CREATE TABLE IF NOT EXISTS `compatibility_lists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `clid` bigint(20) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `clid` (`clid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `compatibility_list_templates`
--

CREATE TABLE IF NOT EXISTS `compatibility_list_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `template` (`template`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `compatibility_map`
--

CREATE TABLE IF NOT EXISTS `compatibility_map` (
  `clid` bigint(20) NOT NULL,
  `cldid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `clid` (`clid`),
  KEY `cldid` (`cldid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config_group` varchar(255) DEFAULT NULL,
  `config_key` varchar(255) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  `note` text,
  KEY `id` (`id`),
  KEY `config_group` (`config_group`,`config_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=606 ;

-- --------------------------------------------------------

--
-- Table structure for table `consolidated`
--

CREATE TABLE IF NOT EXISTS `consolidated` (
  `grpid` int(11) DEFAULT NULL,
  `grpdescr` varchar(26) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `catdescr` varchar(26) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `productid` varchar(26) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `profit` double(16,2) DEFAULT NULL,
  KEY `grpid` (`grpid`),
  KEY `catid` (`catid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `controlpanel`
--

CREATE TABLE IF NOT EXISTS `controlpanel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL DEFAULT '0',
  `widgetid` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `x` int(11) NOT NULL DEFAULT '0',
  `y` int(11) NOT NULL DEFAULT '0',
  `colid` int(11) NOT NULL DEFAULT '1',
  `rowid` int(11) NOT NULL DEFAULT '1',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1925 ;

-- --------------------------------------------------------

--
-- Table structure for table `countrylist`
--

CREATE TABLE IF NOT EXISTS `countrylist` (
  `countryname` varchar(255) DEFAULT NULL,
  `countrycode` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_codes`
--

CREATE TABLE IF NOT EXISTS `coupon_codes` (
  `cid` bigint(20) NOT NULL AUTO_INCREMENT,
  `couponcode` varchar(50) DEFAULT NULL,
  `coupon_text` varchar(255) DEFAULT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `combine` bigint(20) NOT NULL DEFAULT '0',
  `bonus` bigint(20) NOT NULL DEFAULT '0',
  `rlookup` int(11) NOT NULL DEFAULT '0',
  `usedatetime` int(11) NOT NULL DEFAULT '0',
  `couponlimit` int(10) NOT NULL,
  `singleuse` int(11) NOT NULL DEFAULT '0',
  KEY `cid` (`cid`),
  KEY `couponcode` (`couponcode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=178 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_codes_singleuse`
--

CREATE TABLE IF NOT EXISTS `coupon_codes_singleuse` (
  `couponcode` varchar(50) NOT NULL DEFAULT '',
  `cid` bigint(20) NOT NULL DEFAULT '0',
  `used` tinyint(1) NOT NULL,
  `tstamp` int(10) NOT NULL,
  KEY `couponcode` (`couponcode`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_criteria`
--

CREATE TABLE IF NOT EXISTS `coupon_criteria` (
  `ccid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `min` double(16,2) NOT NULL DEFAULT '0.00',
  `max` double(16,2) NOT NULL DEFAULT '0.00',
  `skucriteria` enum('AND','OR') DEFAULT NULL,
  KEY `ccid` (`ccid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=763 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_criteria_lists`
--

CREATE TABLE IF NOT EXISTS `coupon_criteria_lists` (
  `ccl` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `lid` bigint(20) NOT NULL,
  PRIMARY KEY (`ccl`),
  UNIQUE KEY `cid` (`cid`,`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_criteria_skus`
--

CREATE TABLE IF NOT EXISTS `coupon_criteria_skus` (
  `crid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `crid` (`crid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_criteria_times`
--

CREATE TABLE IF NOT EXISTS `coupon_criteria_times` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) DEFAULT NULL,
  `mon` int(11) NOT NULL DEFAULT '0',
  `tue` int(11) NOT NULL DEFAULT '0',
  `wed` int(11) NOT NULL DEFAULT '0',
  `thu` int(11) NOT NULL DEFAULT '0',
  `fri` int(11) NOT NULL DEFAULT '0',
  `sat` int(11) NOT NULL DEFAULT '0',
  `sun` int(11) NOT NULL DEFAULT '0',
  `bdtimeh` int(11) NOT NULL DEFAULT '0',
  `bdtimem` int(11) NOT NULL DEFAULT '0',
  `edtimeh` int(11) NOT NULL DEFAULT '0',
  `edtimem` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=496 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_discounts`
--

CREATE TABLE IF NOT EXISTS `coupon_discounts` (
  `cdid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `fixed_price` double(16,2) NOT NULL DEFAULT '0.00',
  `fixed_discount` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `cdid` (`cdid`),
  KEY `cid` (`cid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=899 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_items_added_removed`
--

CREATE TABLE IF NOT EXISTS `coupon_items_added_removed` (
  `car` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `action` enum('ADD','REMOVE') DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  KEY `car` (`car`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_shipping`
--

CREATE TABLE IF NOT EXISTS `coupon_shipping` (
  `csid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) NOT NULL,
  `csmid` int(11) NOT NULL DEFAULT '0',
  `csmethod` varchar(255) DEFAULT NULL,
  `csprice` double(16,2) DEFAULT NULL,
  KEY `csid` (`csid`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_sites`
--

CREATE TABLE IF NOT EXISTS `coupon_sites` (
  `cid` bigint(20) NOT NULL,
  `divid` bigint(20) NOT NULL,
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

CREATE TABLE IF NOT EXISTS `credits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `vendor` int(10) NOT NULL,
  `order_nr` varchar(250) NOT NULL,
  `po_nr` varchar(250) NOT NULL,
  `rma` varchar(250) NOT NULL,
  `tracking` varchar(250) NOT NULL,
  `vendor_rec` tinyint(1) NOT NULL DEFAULT '0',
  `confirm_type` varchar(250) NOT NULL,
  `vendor_credit` tinyint(1) NOT NULL DEFAULT '0',
  `rf_cust` tinyint(1) NOT NULL DEFAULT '0',
  `entered_by` bigint(20) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `complete` tinyint(1) NOT NULL DEFAULT '0',
  `claim` tinyint(1) NOT NULL DEFAULT '0',
  `rma_mgt` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_accounts`
--

CREATE TABLE IF NOT EXISTS `customer_accounts` (
  `acctid` bigint(20) NOT NULL AUTO_INCREMENT,
  `smacctid` bigint(20) NOT NULL DEFAULT '0',
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `emailspecials` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `altphone` varchar(50) DEFAULT NULL,
  `faxnumber` varchar(50) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bill2name` varchar(128) DEFAULT NULL,
  `bill2company` varchar(128) NOT NULL,
  `bill2street1` varchar(128) DEFAULT NULL,
  `bill2street2` varchar(128) DEFAULT NULL,
  `bill2city` varchar(128) DEFAULT NULL,
  `bill2state` varchar(16) DEFAULT NULL,
  `bill2zip` varchar(20) DEFAULT NULL,
  `ship2name` varchar(128) DEFAULT NULL,
  `ship2company` varchar(128) NOT NULL,
  `ship2street1` varchar(128) DEFAULT NULL,
  `ship2street2` varchar(128) DEFAULT NULL,
  `ship2city` varchar(128) DEFAULT NULL,
  `ship2state` varchar(16) DEFAULT NULL,
  `ship2zip` varchar(20) DEFAULT NULL,
  `default_pl` int(11) DEFAULT NULL,
  `nontaxable` int(11) DEFAULT NULL,
  `taxid` varchar(255) DEFAULT NULL,
  `created` bigint(20) DEFAULT NULL,
  `accttype` int(11) DEFAULT NULL,
  `netterms` int(11) DEFAULT NULL,
  `creditlimit` double(16,2) DEFAULT NULL,
  `balance` double(16,2) DEFAULT NULL,
  `bankacct` varchar(50) DEFAULT NULL,
  `routingno` varchar(50) DEFAULT NULL,
  `bankname` varchar(50) DEFAULT NULL,
  `repid` bigint(20) DEFAULT NULL,
  `arepid` bigint(20) DEFAULT NULL,
  `bill2country` char(2) DEFAULT 'US',
  `ship2country` char(2) DEFAULT 'US',
  `billing_id` bigint(20) DEFAULT NULL,
  `shipping_id` bigint(20) DEFAULT NULL,
  `ebayacctid` bigint(20) NOT NULL,
  `levelid` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  KEY `acctid` (`acctid`),
  KEY `smacctid` (`smacctid`),
  KEY `emailaddr` (`emailaddr`,`password`),
  KEY `bill2name` (`bill2name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=628300 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff`
--

CREATE TABLE IF NOT EXISTS `customer_aff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `approve_commissions` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'auto approve commissions',
  `payments_type` enum('paypal','check','tocredit') NOT NULL DEFAULT 'paypal',
  `commission` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'percent',
  `email` varchar(200) NOT NULL,
  `address_street1` varchar(200) NOT NULL,
  `address_street2` varchar(200) NOT NULL,
  `address_country` varchar(2) NOT NULL,
  `address_state` varchar(200) NOT NULL,
  `address_city` varchar(200) NOT NULL,
  `address_zipcode` varchar(20) NOT NULL,
  `hc_provider` enum('0','1') NOT NULL DEFAULT '0',
  `hc_provider_length` int(11) NOT NULL DEFAULT '365',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3339 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_allowed_links`
--

CREATE TABLE IF NOT EXISTS `customer_aff_allowed_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `verify_code` varchar(20) NOT NULL DEFAULT '',
  `verify_status` enum('pending','approved','denied') NOT NULL DEFAULT 'pending',
  `domain` varchar(100) NOT NULL,
  `subdomain` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_banners`
--

CREATE TABLE IF NOT EXISTS `customer_aff_banners` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `idata` longblob,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `return_url` text NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_banner_groups`
--

CREATE TABLE IF NOT EXISTS `customer_aff_banner_groups` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `idata` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_commissions_log`
--

CREATE TABLE IF NOT EXISTS `customer_aff_commissions_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `acctid` bigint(20) NOT NULL DEFAULT '0',
  `traffic_log_id` bigint(20) NOT NULL DEFAULT '0',
  `sale_order_id` bigint(20) NOT NULL DEFAULT '0',
  `sale_amount` double(9,2) NOT NULL DEFAULT '0.00',
  `sale_status` enum('pending','completed','canceled') NOT NULL DEFAULT 'pending',
  `commission_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `commission_amount` double(9,2) NOT NULL DEFAULT '0.00',
  `commission_status` enum('pending','approved','canceled','removed') NOT NULL DEFAULT 'pending',
  `payment_status` enum('unpaid','paid') NOT NULL DEFAULT 'unpaid',
  `payment_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_commission_payments`
--

CREATE TABLE IF NOT EXISTS `customer_aff_commission_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commission_id` bigint(20) NOT NULL,
  `aff_acctid` bigint(20) NOT NULL,
  `payment_type` enum('credit','check','paypal') NOT NULL DEFAULT 'credit',
  `payment_amount` double(9,2) NOT NULL DEFAULT '0.00',
  `payment_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_hcp`
--

CREATE TABLE IF NOT EXISTS `customer_aff_hcp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `acctid` bigint(20) NOT NULL,
  `expiry_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_request_payments`
--

CREATE TABLE IF NOT EXISTS `customer_aff_request_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `commission_ids` text NOT NULL,
  `request_status` enum('pending','completed','cancelled') NOT NULL DEFAULT 'pending',
  `request_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_aff_traffic_log`
--

CREATE TABLE IF NOT EXISTS `customer_aff_traffic_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aff_acctid` bigint(20) NOT NULL,
  `traffic_referer` text NOT NULL,
  `utm_source` varchar(100) NOT NULL,
  `utm_medium` varchar(100) NOT NULL,
  `utm_campaign` varchar(100) NOT NULL,
  `traffic_ip` varchar(15) NOT NULL,
  `traffic_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17483 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_billinginfo`
--

CREATE TABLE IF NOT EXISTS `customer_billinginfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `exp1a` varchar(4) DEFAULT NULL,
  `exp1b` varchar(4) DEFAULT NULL,
  `firstdigit` int(11) DEFAULT NULL,
  `lastfour` varchar(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `street1` varchar(128) DEFAULT NULL,
  `street2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(16) DEFAULT NULL,
  `country` varchar(4) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `issuingbank` varchar(128) DEFAULT NULL,
  `issuingphone` varchar(128) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  `cardnum` text,
  `debitcard` int(11) NOT NULL DEFAULT '0',
  `card_type` int(5) NOT NULL,
  KEY `id` (`id`),
  KEY `acctid` (`acctid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172805 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_billinginfo_extended`
--

CREATE TABLE IF NOT EXISTS `customer_billinginfo_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bid` bigint(20) NOT NULL DEFAULT '0',
  `c` longblob,
  `e` longblob,
  KEY `id` (`id`),
  KEY `bid` (`bid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=435 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_coupons_used`
--

CREATE TABLE IF NOT EXISTS `customer_coupons_used` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `cid` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_credits`
--

CREATE TABLE IF NOT EXISTS `customer_credits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `credit_amount` double(9,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=132822 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_credits_log`
--

CREATE TABLE IF NOT EXISTS `customer_credits_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `credit_amount` double(9,2) NOT NULL DEFAULT '0.00',
  `operation_description` varchar(250) NOT NULL,
  `operation_reason` varchar(250) NOT NULL,
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128494 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_eorders`
--

CREATE TABLE IF NOT EXISTS `customer_eorders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `kit_pid` bigint(20) NOT NULL DEFAULT '0',
  `count_downloads` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1696 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_eorders_log`
--

CREATE TABLE IF NOT EXISTS `customer_eorders_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `is_checked` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=789 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_levels`
--

CREATE TABLE IF NOT EXISTS `customer_levels` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE IF NOT EXISTS `customer_orders` (
  `orderid` bigint(20) NOT NULL AUTO_INCREMENT,
  `smid` bigint(20) NOT NULL DEFAULT '0',
  `otype` varchar(26) DEFAULT NULL,
  `acctid` bigint(20) DEFAULT NULL,
  `smacctid` bigint(20) NOT NULL DEFAULT '0',
  `invoiceid` varchar(26) DEFAULT NULL,
  `orderdate` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `bill2name` varchar(128) DEFAULT NULL,
  `bill2street1` varchar(128) DEFAULT NULL,
  `bill2street2` varchar(128) DEFAULT NULL,
  `bill2city` varchar(128) DEFAULT NULL,
  `bill2state` varchar(16) DEFAULT NULL,
  `bill2zip` varchar(20) DEFAULT NULL,
  `ship2name` varchar(128) DEFAULT NULL,
  `ship2street1` varchar(128) DEFAULT NULL,
  `ship2street2` varchar(128) DEFAULT NULL,
  `ship2city` varchar(128) DEFAULT NULL,
  `ship2state` varchar(16) DEFAULT NULL,
  `ship2zip` varchar(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `altphone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `weight` double(16,2) DEFAULT NULL,
  `subtotal` double(16,2) DEFAULT NULL,
  `tax` double(16,2) DEFAULT NULL,
  `shipping` double(16,2) DEFAULT NULL,
  `total` double(16,2) DEFAULT NULL,
  `ip_addr` varchar(26) DEFAULT NULL,
  `repid` int(11) DEFAULT NULL,
  `arepid` int(11) DEFAULT NULL,
  `cardnum` text,
  `exp1a` varchar(4) DEFAULT NULL,
  `exp1b` varchar(4) DEFAULT NULL,
  `pmttype` char(2) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  `bill2country` char(2) NOT NULL DEFAULT 'US',
  `ship2country` char(2) NOT NULL DEFAULT 'US',
  `package` int(11) NOT NULL DEFAULT '0',
  `csmid` int(11) NOT NULL DEFAULT '0',
  `csmethod` varchar(255) NOT NULL DEFAULT '',
  `divid` int(11) NOT NULL DEFAULT '0',
  `brand` bigint(20) DEFAULT NULL,
  `clickid` varchar(50) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `billing_id` bigint(20) DEFAULT NULL,
  `shipping_id` bigint(20) DEFAULT NULL,
  `envelope` text,
  `bid` int(11) NOT NULL DEFAULT '0',
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `houseorder` int(11) NOT NULL DEFAULT '0',
  `completedate` bigint(20) NOT NULL DEFAULT '0',
  `sessionid` varchar(128) DEFAULT NULL,
  `cost` double(16,2) NOT NULL DEFAULT '0.00',
  `wholesale` int(11) NOT NULL DEFAULT '0',
  `claim` int(11) NOT NULL DEFAULT '0',
  `ginvoiceid` varchar(26) DEFAULT NULL,
  `lockedup` int(11) NOT NULL DEFAULT '0',
  `allocated` int(11) NOT NULL DEFAULT '0',
  `pallocated` int(11) NOT NULL DEFAULT '0',
  `fbaid` varchar(50) NOT NULL,
  `assigned` bigint(20) NOT NULL,
  `solicited` int(5) NOT NULL,
  `reached` int(5) NOT NULL,
  `callback_later` int(5) NOT NULL,
  `credit_request` int(5) NOT NULL DEFAULT '0',
  `cancelled_reason` varchar(255) NOT NULL,
  `price_overwrite` int(5) NOT NULL,
  KEY `orderid` (`orderid`),
  KEY `smid` (`smid`),
  KEY `otype` (`otype`),
  KEY `sessionid` (`sessionid`),
  KEY `ginvoice` (`ginvoiceid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `status` (`status`),
  KEY `completedate` (`completedate`),
  KEY `orderdate` (`orderdate`),
  KEY `acctid` (`acctid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=514675 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders_inf`
--

CREATE TABLE IF NOT EXISTS `customer_orders_inf` (
  `orderid` int(10) unsigned NOT NULL,
  `send_back` tinyint(3) unsigned NOT NULL,
  `fresh` tinyint(3) unsigned NOT NULL,
  `in_progress` tinyint(3) unsigned NOT NULL,
  `lorder` smallint(5) unsigned NOT NULL,
  `notes` varchar(255) NOT NULL,
  `assigndto` varchar(255) NOT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders_raw_emails`
--

CREATE TABLE IF NOT EXISTS `customer_orders_raw_emails` (
  `orderid` int(10) NOT NULL,
  `tstamp` int(10) NOT NULL,
  `email_sent` tinyint(1) NOT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders_stock_status`
--

CREATE TABLE IF NOT EXISTS `customer_orders_stock_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_label_history`
--

CREATE TABLE IF NOT EXISTS `customer_order_label_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `tracking` varchar(255) NOT NULL,
  `user` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125786 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_lines`
--

CREATE TABLE IF NOT EXISTS `customer_order_lines` (
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL DEFAULT '0',
  `prodid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `qty_ordered` int(11) DEFAULT NULL,
  `qty_shipped` int(11) NOT NULL DEFAULT '0',
  `weight` double(16,2) DEFAULT NULL,
  `shipdate` bigint(20) DEFAULT NULL,
  `shipid` bigint(20) DEFAULT NULL,
  `exchange` int(11) DEFAULT NULL,
  `qty_returned` int(11) NOT NULL DEFAULT '0',
  `kit` int(11) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  `printed` int(11) NOT NULL DEFAULT '0',
  `trackingid` bigint(20) NOT NULL,
  `qty_onhold` int(11) NOT NULL DEFAULT '0',
  `qty_committed` int(11) NOT NULL DEFAULT '0',
  `scost` double(16,2) NOT NULL DEFAULT '0.00',
  `ex_return` int(11) NOT NULL DEFAULT '0',
  `ampid` varchar(24) DEFAULT NULL,
  `qty_dropship` int(11) NOT NULL DEFAULT '0',
  `kitpid` bigint(20) NOT NULL DEFAULT '0',
  `whid` bigint(20) NOT NULL DEFAULT '0',
  KEY `lineid` (`lineid`),
  KEY `orderid` (`orderid`),
  KEY `prodid` (`prodid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1576525 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_lines_log`
--

CREATE TABLE IF NOT EXISTS `customer_order_lines_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `type` enum('ADDED','REMOVED') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`,`pid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32407 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_notes`
--

CREATE TABLE IF NOT EXISTS `customer_order_notes` (
  `orderid` bigint(20) DEFAULT NULL,
  `notes` text,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_notes_internal`
--

CREATE TABLE IF NOT EXISTS `customer_order_notes_internal` (
  `orderid` bigint(20) DEFAULT NULL,
  `notes` longblob,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_tracking`
--

CREATE TABLE IF NOT EXISTS `customer_order_tracking` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `trackno` varchar(128) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `carrier` varchar(10) DEFAULT NULL,
  `shipdate` bigint(20) DEFAULT NULL,
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cost` double(16,2) NOT NULL DEFAULT '0.00',
  `whid` int(11) NOT NULL DEFAULT '0',
  `pickerid` int(11) DEFAULT NULL,
  `shipperid` int(11) DEFAULT NULL,
  `istracked` tinyint(5) NOT NULL,
  KEY `invoiceid` (`invoiceid`),
  KEY `tid` (`tid`),
  KEY `type` (`type`,`carrier`,`shipdate`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=257141 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_tracking_extended`
--

CREATE TABLE IF NOT EXISTS `customer_order_tracking_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `tid` bigint(20) NOT NULL,
  `trackno` varchar(128) DEFAULT NULL,
  `prodid` bigint(20) DEFAULT NULL,
  `serial` varchar(128) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`),
  KEY `tid` (`tid`),
  KEY `trackno` (`trackno`),
  KEY `prodid` (`prodid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26957967 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_payments`
--

CREATE TABLE IF NOT EXISTS `customer_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `avscode` varchar(10) DEFAULT NULL,
  `txid` varchar(50) DEFAULT NULL,
  `approvalcode` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `amount` double(16,2) DEFAULT NULL,
  `eci` varchar(10) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `voided` int(11) NOT NULL DEFAULT '0',
  `cvvcode` varchar(50) DEFAULT NULL,
  `pname` varchar(50) DEFAULT NULL,
  `firstdigit` int(11) DEFAULT NULL,
  `lastfour` varchar(6) DEFAULT NULL,
  `txtype` varchar(16) DEFAULT NULL,
  `bid` bigint(20) NOT NULL DEFAULT '0',
  `cardnum` text,
  `exp1a` varchar(4) DEFAULT NULL,
  `exp1b` varchar(4) DEFAULT NULL,
  `pmttype` char(2) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=283430 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_shippinginfo`
--

CREATE TABLE IF NOT EXISTS `customer_shippinginfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `street1` varchar(128) DEFAULT NULL,
  `street2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(16) DEFAULT NULL,
  `country` varchar(4) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `acctid` (`acctid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_tmp_cart`
--

CREATE TABLE IF NOT EXISTS `customer_tmp_cart` (
  `acctid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `qty` bigint(20) NOT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  `qty_bulk` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE IF NOT EXISTS `custom_fields` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `note` longblob,
  `failover` varchar(32) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  `display` int(11) NOT NULL DEFAULT '0',
  `ord` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_specs_category`
--

CREATE TABLE IF NOT EXISTS `custom_specs_category` (
  `specid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `filters` varchar(255) DEFAULT NULL,
  `ranges` blob,
  `inputId` blob NOT NULL,
  KEY `specid` (`specid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_specs_products_result`
--

CREATE TABLE IF NOT EXISTS `custom_specs_products_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `manid` varchar(255) DEFAULT NULL,
  `manname` varchar(255) DEFAULT NULL,
  `manpart` varchar(255) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price_level4` double(16,2) DEFAULT NULL,
  `price_map` double(16,2) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  `kit` tinyint(1) NOT NULL DEFAULT '0',
  `usemap` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specid` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30705 ;

-- --------------------------------------------------------

--
-- Table structure for table `custom_tabs`
--

CREATE TABLE IF NOT EXISTS `custom_tabs` (
  `tab1` int(11) NOT NULL DEFAULT '1',
  `tab2` int(11) NOT NULL DEFAULT '1',
  `tab3` int(11) NOT NULL DEFAULT '1',
  `tab4` int(11) NOT NULL DEFAULT '1',
  `tab5` int(11) NOT NULL DEFAULT '1',
  `tab6` int(11) NOT NULL DEFAULT '1',
  `tab7` int(11) NOT NULL DEFAULT '1',
  `tab8` int(11) NOT NULL DEFAULT '1',
  `tab9` int(11) NOT NULL DEFAULT '1',
  `tab10` int(11) NOT NULL DEFAULT '1',
  `tab11` int(11) NOT NULL DEFAULT '1',
  `tab12` int(11) NOT NULL DEFAULT '1',
  `tab13` int(11) NOT NULL DEFAULT '1',
  `tab14` int(11) NOT NULL DEFAULT '1',
  `tab15` int(11) NOT NULL DEFAULT '1',
  `tab16` int(11) NOT NULL DEFAULT '1',
  `tab17` int(11) NOT NULL DEFAULT '1',
  `tab18` int(11) NOT NULL DEFAULT '1',
  `tab19` int(11) NOT NULL DEFAULT '1',
  `tab20` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `custom_values`
--

CREATE TABLE IF NOT EXISTS `custom_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cfid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) DEFAULT NULL,
  `text_short` text,
  `text_long` text,
  `ord` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `cfid` (`cfid`),
  KEY `pid` (`pid`),
  FULLTEXT KEY `text_short` (`text_short`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=389564 ;

-- --------------------------------------------------------

--
-- Table structure for table `database_maintenance_log`
--

CREATE TABLE IF NOT EXISTS `database_maintenance_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `tstamp` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name` (`table_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=670 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeeds`
--

CREATE TABLE IF NOT EXISTS `datafeeds` (
  `dfid` bigint(20) NOT NULL AUTO_INCREMENT,
  `dfname` varchar(255) DEFAULT NULL,
  `delimiter` char(2) DEFAULT NULL,
  `enclosedby` char(2) DEFAULT NULL,
  `min` double NOT NULL DEFAULT '0',
  `max` double NOT NULL DEFAULT '0',
  `filename` text,
  `tracking_code` varchar(255) DEFAULT NULL,
  `options_append` int(11) DEFAULT NULL,
  `options_listall` int(11) DEFAULT NULL,
  `options_instock` int(11) DEFAULT NULL,
  `options_content` int(11) DEFAULT NULL,
  `options_upc` int(11) DEFAULT NULL,
  `options_visible` int(11) DEFAULT NULL,
  `category` varchar(50) NOT NULL DEFAULT '',
  `options_noheader` int(11) NOT NULL DEFAULT '0',
  `customheader` longblob,
  `options_30min` int(11) NOT NULL DEFAULT '0',
  `divid` bigint(20) NOT NULL DEFAULT '0',
  `copyfeed` bigint(20) NOT NULL DEFAULT '0',
  `peid` varchar(4) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `writeback` int(11) NOT NULL DEFAULT '0',
  `exclude_nonvis` int(11) NOT NULL DEFAULT '0',
  `no_zeroprice` int(11) NOT NULL DEFAULT '1',
  `options_nokits` int(11) NOT NULL DEFAULT '0',
  `utf8` int(11) NOT NULL DEFAULT '0',
  `mdollar` double(16,2) DEFAULT NULL,
  `mpercent` double(16,2) DEFAULT NULL,
  `applyrebate` int(11) NOT NULL DEFAULT '0',
  `options_dsstock` int(11) NOT NULL DEFAULT '0',
  KEY `dfid` (`dfid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_columns`
--

CREATE TABLE IF NOT EXISTS `datafeed_columns` (
  `dfcid` bigint(20) NOT NULL AUTO_INCREMENT,
  `dfid` bigint(20) NOT NULL DEFAULT '0',
  `column_name` varchar(255) DEFAULT NULL,
  `header_name` varchar(255) DEFAULT NULL,
  `static_data` varchar(255) DEFAULT NULL,
  `specs_dkey` varchar(255) DEFAULT NULL,
  `column_name_blank` varchar(255) DEFAULT NULL,
  `specs_dkey_blank` varchar(255) DEFAULT NULL,
  KEY `dfcid` (`dfcid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=529 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_columns_list`
--

CREATE TABLE IF NOT EXISTS `datafeed_columns_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `colid` varchar(255) DEFAULT NULL,
  `colname` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_columns_order`
--

CREATE TABLE IF NOT EXISTS `datafeed_columns_order` (
  `dfcid` bigint(20) NOT NULL DEFAULT '0',
  `dorder` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_schedule`
--

CREATE TABLE IF NOT EXISTS `datafeed_schedule` (
  `sid` bigint(20) NOT NULL AUTO_INCREMENT,
  `dfid` bigint(20) NOT NULL DEFAULT '0',
  `sun` int(11) NOT NULL DEFAULT '0',
  `mon` int(11) NOT NULL DEFAULT '0',
  `tue` int(11) NOT NULL DEFAULT '0',
  `wed` int(11) NOT NULL DEFAULT '0',
  `thu` int(11) NOT NULL DEFAULT '0',
  `fri` int(11) NOT NULL DEFAULT '0',
  `sat` int(11) NOT NULL DEFAULT '0',
  `hour` int(11) NOT NULL DEFAULT '0',
  `minute` int(11) NOT NULL DEFAULT '0',
  `ftp` int(11) NOT NULL DEFAULT '0',
  `ftplocation` varchar(255) DEFAULT NULL,
  KEY `sid` (`sid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_skus`
--

CREATE TABLE IF NOT EXISTS `datafeed_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dfid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `dfid` (`dfid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17615 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_status`
--

CREATE TABLE IF NOT EXISTS `datafeed_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` double(16,2) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=610 ;

-- --------------------------------------------------------

--
-- Table structure for table `datafeed_writeback_skus`
--

CREATE TABLE IF NOT EXISTS `datafeed_writeback_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dfid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `dfid` (`dfid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `debug`
--

CREATE TABLE IF NOT EXISTS `debug` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `data` text,
  `title` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1191092 ;

-- --------------------------------------------------------

--
-- Table structure for table `delete_skus`
--

CREATE TABLE IF NOT EXISTS `delete_skus` (
  `pid` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dim_finder`
--

CREATE TABLE IF NOT EXISTS `dim_finder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upg` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `terminal` varchar(50) NOT NULL,
  `nom` int(11) NOT NULL COMMENT 'Volt',
  `capacity` double(9,2) NOT NULL COMMENT '20 hr. rate',
  `dim_length` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `dim_width` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `dim_height` double(9,2) NOT NULL COMMENT 'Dimensions (in.)',
  `weight` double(9,2) NOT NULL COMMENT 'lbs. +/- 5%',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=233 ;

-- --------------------------------------------------------

--
-- Table structure for table `discontinued_replacedby`
--

CREATE TABLE IF NOT EXISTS `discontinued_replacedby` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `replacedby` bigint(20) NOT NULL DEFAULT '0',
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `display_order`
--

CREATE TABLE IF NOT EXISTS `display_order` (
  `grpid` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `subid` int(11) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  KEY `grpid` (`grpid`),
  KEY `grpid_2` (`grpid`,`catid`),
  KEY `grpid_3` (`grpid`,`catid`,`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dmi_brands`
--

CREATE TABLE IF NOT EXISTS `dmi_brands` (
  `brand_code` varchar(16) DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  KEY `brand_code` (`brand_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dmi_class_map`
--

CREATE TABLE IF NOT EXISTS `dmi_class_map` (
  `major_class` varchar(128) DEFAULT NULL,
  `major_title` varchar(255) DEFAULT NULL,
  `minor_class` varchar(128) DEFAULT NULL,
  `minor_title` varchar(255) DEFAULT NULL,
  KEY `major_class` (`major_class`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dmi_major_class`
--

CREATE TABLE IF NOT EXISTS `dmi_major_class` (
  `major_class` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `major_class` (`major_class`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dmi_minor_class`
--

CREATE TABLE IF NOT EXISTS `dmi_minor_class` (
  `major_class` varchar(128) DEFAULT NULL,
  `minor_class` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `major_class` (`major_class`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dmi_pnexport`
--

CREATE TABLE IF NOT EXISTS `dmi_pnexport` (
  `mancode` varchar(10) DEFAULT NULL,
  `partnum` varchar(50) DEFAULT NULL,
  `majorcat` varchar(20) DEFAULT NULL,
  `minorcat` varchar(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `map` varchar(25) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  KEY `mancode` (`mancode`,`partnum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doba_monitor_timestamp`
--

CREATE TABLE IF NOT EXISTS `doba_monitor_timestamp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) DEFAULT NULL,
  `cur_timestamp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=138 ;

-- --------------------------------------------------------

--
-- Table structure for table `DOBA_spider_helper`
--

CREATE TABLE IF NOT EXISTS `DOBA_spider_helper` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(100) DEFAULT NULL,
  `vpid` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dataid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=518290 ;

-- --------------------------------------------------------

--
-- Table structure for table `drilldown_cache`
--

CREATE TABLE IF NOT EXISTS `drilldown_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dmid` int(11) NOT NULL,
  `doid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  KEY `id` (`id`),
  KEY `dmid` (`dmid`),
  KEY `doid` (`doid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drilldown_main`
--

CREATE TABLE IF NOT EXISTS `drilldown_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) DEFAULT NULL,
  `ltype` enum('SS','CAT','BRND','PRICE') DEFAULT NULL,
  `dtype` int(11) NOT NULL DEFAULT '0',
  `tid` int(11) NOT NULL,
  `colid` int(11) NOT NULL,
  `catname` varchar(128) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `catpage` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `drilldown_options`
--

CREATE TABLE IF NOT EXISTS `drilldown_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dmid` int(11) NOT NULL,
  `minval` double(16,2) DEFAULT NULL,
  `maxval` double(16,2) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `dmid` (`dmid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `e1_customer_orders`
--

CREATE TABLE IF NOT EXISTS `e1_customer_orders` (
  `orderid` bigint(20) NOT NULL DEFAULT '0',
  `divid` int(11) NOT NULL DEFAULT '0',
  `webid` bigint(20) DEFAULT NULL,
  `otype` varchar(26) DEFAULT NULL,
  `webacctid` bigint(20) DEFAULT NULL,
  `acctid` bigint(20) NOT NULL DEFAULT '0',
  `adivid` int(11) NOT NULL DEFAULT '0',
  `invoiceid` varchar(26) NOT NULL DEFAULT '',
  `orderdate` bigint(20) NOT NULL DEFAULT '0',
  `ordertime` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `bill2name` varchar(128) DEFAULT NULL,
  `bill2street1` varchar(128) DEFAULT NULL,
  `bill2street2` varchar(128) DEFAULT NULL,
  `bill2city` varchar(128) DEFAULT NULL,
  `bill2state` varchar(16) DEFAULT NULL,
  `bill2country` char(2) DEFAULT NULL,
  `bill2zip` varchar(20) DEFAULT NULL,
  `ship2name` varchar(128) DEFAULT NULL,
  `ship2street1` varchar(128) DEFAULT NULL,
  `ship2street2` varchar(128) DEFAULT NULL,
  `ship2city` varchar(128) DEFAULT NULL,
  `ship2state` varchar(16) DEFAULT NULL,
  `ship2country` char(2) DEFAULT NULL,
  `ship2zip` varchar(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `altphone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `weight` double(16,2) DEFAULT NULL,
  `subtotal` double(16,2) DEFAULT NULL,
  `tax` double(16,2) DEFAULT NULL,
  `shipping` double(16,2) DEFAULT NULL,
  `total` double(16,2) DEFAULT NULL,
  `ip_addr` varchar(26) DEFAULT NULL,
  `repid` int(11) NOT NULL DEFAULT '0',
  `arepid` int(11) NOT NULL DEFAULT '0',
  `cardnum` text,
  `exp1a` varchar(4) DEFAULT NULL,
  `exp1b` varchar(4) DEFAULT NULL,
  `pmttype` char(2) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  `dstamp` bigint(20) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `package` int(11) DEFAULT NULL,
  `csmid` int(11) DEFAULT NULL,
  `csmethod` varchar(255) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(128) DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`),
  KEY `invoiceid_2` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `e1_customer_order_lines`
--

CREATE TABLE IF NOT EXISTS `e1_customer_order_lines` (
  `orderid` bigint(20) NOT NULL DEFAULT '0',
  `divid` int(11) NOT NULL DEFAULT '0',
  `lineid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `pdiv` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(25) DEFAULT NULL,
  `subsku` varchar(25) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `qty_ordered` int(11) NOT NULL DEFAULT '0',
  `qty_shipped` int(11) NOT NULL DEFAULT '0',
  `qty_returned` int(11) NOT NULL DEFAULT '0',
  `weight` decimal(2,0) DEFAULT NULL,
  `grp` int(11) DEFAULT NULL,
  `cat` int(11) DEFAULT NULL,
  `sub` int(11) DEFAULT NULL,
  `kit` int(11) DEFAULT NULL,
  `shipid` bigint(20) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `shipdate` bigint(20) DEFAULT NULL,
  `exchange` int(11) DEFAULT NULL,
  `printed` int(11) DEFAULT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `e1_customer_order_notes_internal`
--

CREATE TABLE IF NOT EXISTS `e1_customer_order_notes_internal` (
  `orderid` bigint(20) DEFAULT NULL,
  `notes` longblob,
  `dstamp` bigint(20) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `e1_product_inventory`
--

CREATE TABLE IF NOT EXISTS `e1_product_inventory` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `divid` bigint(20) DEFAULT NULL,
  `distname` varchar(25) DEFAULT NULL,
  `warehouse` varchar(50) DEFAULT NULL,
  `wh_state` char(2) DEFAULT NULL,
  `inv_available` bigint(20) DEFAULT NULL,
  `inv_backorder` bigint(20) DEFAULT NULL,
  `inv_onorder` bigint(20) DEFAULT NULL,
  `eta` bigint(20) DEFAULT NULL,
  `dstamp` bigint(20) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `company_warehouse` int(11) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `reccost` double(16,2) DEFAULT NULL,
  `lineid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit`
--

CREATE TABLE IF NOT EXISTS `ebay_audit` (
  `listingid` varchar(50) DEFAULT NULL,
  `endtime` bigint(20) DEFAULT NULL,
  `auditdate` datetime DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `listingid` (`listingid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_reverse`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_reverse` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `prod_name` varchar(250) NOT NULL,
  `is_not_active` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18537 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_audit_sku_compare`
--

CREATE TABLE IF NOT EXISTS `ebay_audit_sku_compare` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `sku` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `sku_exist` enum('0','1') NOT NULL,
  `ended` tinyint(1) NOT NULL,
  `date_added` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29158 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_bad_listings`
--

CREATE TABLE IF NOT EXISTS `ebay_bad_listings` (
  `listingid` varchar(255) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `ebay_pid` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_campaigns`
--

CREATE TABLE IF NOT EXISTS `ebay_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `begindate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `auto_relist` int(11) NOT NULL DEFAULT '0',
  `listing_type` varchar(255) DEFAULT NULL,
  `item_title` varchar(255) DEFAULT '#producttitle# #sku#',
  `item_subtitle` varchar(255) DEFAULT NULL,
  `ebay_template` bigint(20) DEFAULT NULL,
  `listing_duration` varchar(10) DEFAULT NULL,
  `starting_price` double(16,2) NOT NULL DEFAULT '0.00',
  `buyitnow_price` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '100',
  `campaign_type` varchar(25) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `hitcounter` varchar(30) NOT NULL DEFAULT '',
  `bold` int(11) NOT NULL DEFAULT '0',
  `featured` int(11) NOT NULL DEFAULT '0',
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `immediate_pay` int(11) NOT NULL DEFAULT '0',
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `divid` int(11) NOT NULL DEFAULT '0',
  `std_shipping` double(16,2) NOT NULL DEFAULT '0.00',
  `exp_shipping` double(16,2) NOT NULL DEFAULT '0.00',
  `std_addon` double(16,2) NOT NULL DEFAULT '-1.00',
  `exp_addon` double(16,2) NOT NULL DEFAULT '-1.00',
  `mainimage` int(11) NOT NULL DEFAULT '0',
  `useconfig` char(1) DEFAULT 'A',
  `calculated_uspspmi` int(11) NOT NULL DEFAULT '0',
  `calculated_uspsexi` int(11) NOT NULL DEFAULT '0',
  `calculated_upswwsi` int(11) NOT NULL DEFAULT '0',
  `std_shipping_cf` int(11) NOT NULL DEFAULT '0',
  `dispatch_time` int(5) NOT NULL DEFAULT '0',
  `zip_code` varchar(10) NOT NULL,
  KEY `id` (`id`),
  KEY `active` (`active`,`begindate`,`enddate`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_campaigns_lastmod`
--

CREATE TABLE IF NOT EXISTS `ebay_campaigns_lastmod` (
  `campid` bigint(20) NOT NULL,
  `lastupdate` bigint(20) NOT NULL,
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_campaigns_useinv`
--

CREATE TABLE IF NOT EXISTS `ebay_campaigns_useinv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ecampid` bigint(20) NOT NULL DEFAULT '0',
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `ecampid` (`ecampid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_campaigns_usepricing`
--

CREATE TABLE IF NOT EXISTS `ebay_campaigns_usepricing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ecampid` bigint(20) NOT NULL DEFAULT '0',
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `ecampid` (`ecampid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_campaign_methods`
--

CREATE TABLE IF NOT EXISTS `ebay_campaign_methods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campid` bigint(20) NOT NULL,
  `code` varchar(64) DEFAULT NULL,
  `shipping` double(16,2) DEFAULT NULL,
  `addon` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `campid` (`campid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cancels`
--

CREATE TABLE IF NOT EXISTS `ebay_cancels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) DEFAULT NULL,
  `disputeid` varchar(255) DEFAULT NULL,
  `correlationid` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases`
--

CREATE TABLE IF NOT EXISTS `ebay_cases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `case_id` bigint(20) NOT NULL,
  `case_type` varchar(100) NOT NULL,
  `case_user_id` varchar(100) NOT NULL,
  `case_user_role` varchar(100) NOT NULL,
  `case_otherparty_userid` varchar(100) NOT NULL,
  `case_otherparty_role` varchar(100) NOT NULL,
  `case_status` varchar(100) NOT NULL,
  `case_status_stype` varchar(100) NOT NULL,
  `case_item_id` bigint(20) NOT NULL,
  `case_item_title` varchar(255) NOT NULL,
  `case_transaction_id` bigint(20) NOT NULL,
  `case_qty` int(10) NOT NULL,
  `case_amount` double(16,2) NOT NULL,
  `case_currency` varchar(10) NOT NULL,
  `case_respond_date` datetime NOT NULL,
  `case_creation_date` datetime NOT NULL,
  `case_last_modified_date` datetime NOT NULL,
  `case_notes` text NOT NULL,
  `case_account` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_activity_options`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_activity_options` (
  `case_id` bigint(20) NOT NULL,
  `activity_name` varchar(100) NOT NULL,
  `activity_buyer_pref` tinyint(1) NOT NULL,
  `activity_opt1_name` varchar(100) NOT NULL,
  `activity_opt1_value` varchar(100) NOT NULL,
  `activity_opt2_name` varchar(100) NOT NULL,
  `activity_opt2_value` varchar(100) NOT NULL,
  KEY `case_id` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_details`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `case_id` bigint(20) NOT NULL,
  `case_agree_refund_amount` double(16,2) NOT NULL,
  `case_agree_refund_currency` varchar(5) NOT NULL,
  `case_decision` varchar(50) NOT NULL,
  `case_decision_date` datetime NOT NULL,
  `case_decision_reason` varchar(255) NOT NULL,
  `case_detail_status` varchar(255) NOT NULL,
  `case_vfv_credited` tinyint(1) NOT NULL,
  `case_global_id` varchar(255) NOT NULL,
  `case_initial_buyer_expect` varchar(255) NOT NULL,
  `case_open_reason` varchar(255) NOT NULL,
  `case_return_merchandise_authorization` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_details_appeal`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_details_appeal` (
  `id` bigint(20) NOT NULL,
  `case_id` bigint(20) NOT NULL,
  `appeal_id` bigint(20) NOT NULL,
  `appeal_creation_date` datetime NOT NULL,
  `appeal_decision` varchar(255) NOT NULL,
  `appeal_decision_date` datetime NOT NULL,
  `appeal_decision_reason` varchar(255) NOT NULL,
  `appeal_decision_reason_code` varchar(255) NOT NULL,
  `appeal_decision_reason_content` varchar(255) NOT NULL,
  `appeal_decision_reason_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_details_payment`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_details_payment` (
  `case_id` bigint(20) NOT NULL,
  `balance_due` double(16,2) NOT NULL,
  `money_movement_id` varchar(50) NOT NULL,
  `money_movement_parentid` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `from_party_role` varchar(50) NOT NULL,
  `from_party_userid` varchar(50) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `paypal_transaction_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `toparty_role` varchar(255) NOT NULL,
  `toparty_userid` varchar(255) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  KEY `case_id` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_details_respons_history`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_details_respons_history` (
  `case_id` bigint(20) NOT NULL,
  `activity` varchar(255) NOT NULL,
  `activity_code` varchar(255) NOT NULL,
  `activity_content` varchar(255) NOT NULL,
  `activity_description` varchar(255) NOT NULL,
  `appeal_ref` varchar(255) NOT NULL,
  `money_movement_ref` varchar(255) NOT NULL,
  `onhold_reason` varchar(255) NOT NULL,
  `onhold_reason_code` varchar(255) NOT NULL,
  `onhold_reason_content` varchar(255) NOT NULL,
  `onhold_reason_description` varchar(255) NOT NULL,
  `author_role` varchar(255) NOT NULL,
  `author_userid` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `note` text NOT NULL,
  KEY `case_id` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_cases_details_return_shipment`
--

CREATE TABLE IF NOT EXISTS `ebay_cases_details_return_shipment` (
  `case_id` bigint(20) NOT NULL,
  `carrier_used` varchar(50) NOT NULL,
  `delivery_date` datetime NOT NULL,
  `delivery_status` varchar(100) NOT NULL,
  `shipping_city` varchar(50) NOT NULL,
  `shipping_country` varchar(50) NOT NULL,
  `shipping_name` varchar(100) NOT NULL,
  `shipping_zip` varchar(50) NOT NULL,
  `shipping_state` varchar(50) NOT NULL,
  `shipping_street1` varchar(255) NOT NULL,
  `shipping_street2` varchar(255) NOT NULL,
  `shipping_cost` double(16,2) NOT NULL,
  `tracking_number` varchar(100) NOT NULL,
  `seller_buyer` tinyint(4) NOT NULL,
  KEY `case_id` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories`
--

CREATE TABLE IF NOT EXISTS `ebay_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `ebay1` varchar(26) DEFAULT NULL,
  `ebay2` varchar(26) DEFAULT NULL,
  `ebay3` varchar(26) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories_api`
--

CREATE TABLE IF NOT EXISTS `ebay_categories_api` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `parentid` (`parentid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories_automotive`
--

CREATE TABLE IF NOT EXISTS `ebay_categories_automotive` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `best_offer_enable` tinyint(3) NOT NULL,
  `auto_pay_enable` tinyint(3) NOT NULL,
  `category_id` int(10) NOT NULL,
  `category_level` tinyint(3) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_parent_id` int(10) NOT NULL,
  `category_leaf` tinyint(3) NOT NULL,
  `category_lsd` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_parent_id` (`category_parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8914 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories_automotive_map`
--

CREATE TABLE IF NOT EXISTS `ebay_categories_automotive_map` (
  `ebaycatid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  KEY `ebaycatid` (`ebaycatid`),
  KEY `catid` (`catid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories_canada`
--

CREATE TABLE IF NOT EXISTS `ebay_categories_canada` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `best_offer_enable` tinyint(3) NOT NULL,
  `auto_pay_enable` tinyint(3) NOT NULL,
  `category_id` int(10) NOT NULL,
  `category_level` tinyint(3) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_parent_id` int(10) NOT NULL,
  `category_leaf` tinyint(3) NOT NULL,
  `category_lsd` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_parent_id` (`category_parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20004 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_categories_canada_map`
--

CREATE TABLE IF NOT EXISTS `ebay_categories_canada_map` (
  `ebaycatid` int(10) NOT NULL,
  `catid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  KEY `ebaycatid` (`ebaycatid`),
  KEY `catid` (`catid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_category_temp`
--

CREATE TABLE IF NOT EXISTS `ebay_category_temp` (
  `catid` bigint(20) NOT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_debug`
--

CREATE TABLE IF NOT EXISTS `ebay_debug` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `data` longblob,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33236 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_errors`
--

CREATE TABLE IF NOT EXISTS `ebay_errors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ebaycampid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `pid` (`pid`,`resolved`),
  KEY `tstamp` (`tstamp`,`resolved`),
  KEY `ebaycampid` (`ebaycampid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=472 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_errors_extended`
--

CREATE TABLE IF NOT EXISTS `ebay_errors_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `eid` bigint(20) NOT NULL,
  `message` longblob,
  KEY `id` (`id`),
  KEY `eid` (`eid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=601 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_feedback`
--

CREATE TABLE IF NOT EXISTS `ebay_feedback` (
  `orderid` bigint(20) NOT NULL,
  `ebayacctid` bigint(20) NOT NULL,
  `feedbackid` bigint(20) NOT NULL,
  `txid` varchar(50) DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(256) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `ebayacctid` (`ebayacctid`),
  KEY `feedbackid` (`feedbackid`),
  KEY `txid` (`txid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_keywords`
--

CREATE TABLE IF NOT EXISTS `ebay_keywords` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_listings`
--

CREATE TABLE IF NOT EXISTS `ebay_listings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ecampid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `ebaylistingid` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `pid_lastupdate` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `ecampid` (`ecampid`,`pid`,`active`),
  KEY `endtime` (`endtime`),
  KEY `pid_lastupdate` (`pid_lastupdate`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=939 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_log`
--

CREATE TABLE IF NOT EXISTS `ebay_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ebcampid` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) NOT NULL DEFAULT '-1',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`),
  KEY `listid` (`listid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2253415 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_log_extended`
--

CREATE TABLE IF NOT EXISTS `ebay_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lid` bigint(20) NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4168631 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center` (
  `emc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emc_status` varchar(50) NOT NULL,
  `emc_date_created` varchar(50) NOT NULL,
  `emc_last_modified` varchar(50) NOT NULL,
  `emc_item_id` bigint(20) NOT NULL,
  `emc_question_id` bigint(20) NOT NULL,
  `emc_assignedto` int(5) NOT NULL,
  `emc_followup` int(5) NOT NULL,
  `emc_notes` blob NOT NULL,
  `emc_site` varchar(10) NOT NULL,
  `keep_alive` tinyint(5) NOT NULL DEFAULT '0',
  `no_response` tinyint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center_account_orders`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center_account_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(100) NOT NULL,
  `nr_orders` tinyint(5) NOT NULL,
  `duplicates` tinyint(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=138 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center_email`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center_email` (
  `emce_id` int(10) NOT NULL AUTO_INCREMENT,
  `emce_title` varchar(255) NOT NULL,
  `emce_content` text NOT NULL,
  PRIMARY KEY (`emce_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center_item`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center_item` (
  `emci_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emci_ebayid` bigint(20) NOT NULL,
  `emci_listing_start_time` varchar(50) NOT NULL,
  `emci_listing_end_time` varchar(50) NOT NULL,
  `emci_listing_url` varchar(255) NOT NULL,
  `emci_sellerid` varchar(50) NOT NULL,
  `emci_price` double(16,2) NOT NULL,
  `emci_timeleft` varchar(50) NOT NULL,
  `emci_title` varchar(255) NOT NULL,
  `emci_condition_id` int(10) NOT NULL,
  `emci_condition_name` varchar(50) NOT NULL,
  PRIMARY KEY (`emci_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center_question`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center_question` (
  `emcq_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emcq_message_type` varchar(50) NOT NULL,
  `emcq_question_type` varchar(50) NOT NULL,
  `emcq_display_public` varchar(10) NOT NULL,
  `emcq_sender_id` varchar(50) NOT NULL,
  `emcq_sender_email` varchar(255) NOT NULL,
  `emcq_recipient_id` varchar(255) NOT NULL,
  `emcq_subject` text NOT NULL,
  `emcq_body` text NOT NULL,
  `emcq_message_id` varchar(50) NOT NULL,
  `emcq_item_id` varchar(255) NOT NULL,
  PRIMARY KEY (`emcq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_message_center_question_response`
--

CREATE TABLE IF NOT EXISTS `ebay_message_center_question_response` (
  `emcqr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emcqr_qid` bigint(20) NOT NULL,
  `emcqr_public` int(5) NOT NULL,
  `emcqr_copy` int(5) NOT NULL,
  `emcqr_response` text NOT NULL,
  `emcqr_userid` int(10) NOT NULL,
  PRIMARY KEY (`emcqr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_orders`
--

CREATE TABLE IF NOT EXISTS `ebay_orders` (
  `orderid` bigint(20) NOT NULL,
  `listingid` bigint(20) NOT NULL,
  `ebaytxid` varchar(255) DEFAULT NULL,
  `txid` varchar(255) DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `listingid` (`listingid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_orders_fulfillment`
--

CREATE TABLE IF NOT EXISTS `ebay_orders_fulfillment` (
  `invoiceid` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `ebay_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_out_synk`
--

CREATE TABLE IF NOT EXISTS `ebay_out_synk` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `ebay` bigint(20) NOT NULL,
  `nopid` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17871 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_pid`
--

CREATE TABLE IF NOT EXISTS `ebay_pid` (
  `upccode` varchar(15) DEFAULT NULL,
  `epid` varchar(50) DEFAULT NULL,
  `itemid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `onnum` int(11) DEFAULT NULL,
  KEY `upccode` (`upccode`),
  KEY `epid` (`epid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_ship_methods`
--

CREATE TABLE IF NOT EXISTS `ebay_ship_methods` (
  `code` varchar(64) DEFAULT NULL,
  `descr` text,
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_store_categories`
--

CREATE TABLE IF NOT EXISTS `ebay_store_categories` (
  `catid` bigint(20) NOT NULL,
  `parentid` bigint(20) NOT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `ebaycatid` bigint(20) NOT NULL,
  `ebayparentid` bigint(20) NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `ebaycatid` (`ebaycatid`),
  KEY `parentid` (`parentid`),
  KEY `ebayparentid` (`ebayparentid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_templates`
--

CREATE TABLE IF NOT EXISTS `ebay_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `body` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebay_title_audit`
--

CREATE TABLE IF NOT EXISTS `ebay_title_audit` (
  `listingid` varchar(255) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  KEY `listingid` (`listingid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `edit_sku_fields`
--

CREATE TABLE IF NOT EXISTS `edit_sku_fields` (
  `field` varchar(128) DEFAULT NULL,
  KEY `field` (`field`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_block`
--

CREATE TABLE IF NOT EXISTS `email_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(26) DEFAULT NULL,
  `templateid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `invoiceid` (`invoiceid`,`templateid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=353003 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_campaigns`
--

CREATE TABLE IF NOT EXISTS `email_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `templateid` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `bdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  `ostatus` int(11) DEFAULT NULL,
  `cat0` int(11) DEFAULT NULL,
  `cat1` int(11) DEFAULT NULL,
  `cat2` int(11) DEFAULT NULL,
  `cat3` int(11) DEFAULT NULL,
  `cat4` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(50) DEFAULT NULL,
  `zipcode` varchar(32) DEFAULT NULL,
  `pl` int(11) NOT NULL DEFAULT '1',
  `minskus` int(11) NOT NULL DEFAULT '1',
  `maxskus` int(11) NOT NULL DEFAULT '1',
  `em_clicktype` int(11) DEFAULT NULL,
  `em_clickid` varchar(50) DEFAULT NULL,
  `exclusive` int(11) NOT NULL DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `min` double(16,2) DEFAULT NULL,
  `max` double(16,2) DEFAULT NULL,
  `exclusiveacct` int(11) NOT NULL DEFAULT '0',
  `amonly` int(11) NOT NULL DEFAULT '0',
  `exclude_feedback` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_sent_log`
--

CREATE TABLE IF NOT EXISTS `email_sent_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acctid` bigint(20) DEFAULT NULL,
  `smacctid` bigint(20) DEFAULT NULL,
  `invoiceid` varchar(50) NOT NULL DEFAULT '',
  `emailaddr` varchar(255) DEFAULT NULL,
  `campaign` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  `ostatus` int(11) DEFAULT NULL,
  `odate` datetime DEFAULT NULL,
  `sentdate` datetime DEFAULT NULL,
  `md5invoice` varchar(255) DEFAULT NULL,
  `templateid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `acctid` (`acctid`,`smacctid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `emailaddr` (`emailaddr`),
  KEY `campaign` (`campaign`),
  KEY `md5invoice` (`md5invoice`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=424771 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_sent_log_extended`
--

CREATE TABLE IF NOT EXISTS `email_sent_log_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `esid` bigint(20) NOT NULL DEFAULT '0',
  `errormsg` text,
  `subject` text,
  `body` longblob,
  KEY `id` (`id`),
  KEY `esid` (`esid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=424771 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_skus`
--

CREATE TABLE IF NOT EXISTS `email_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `campid` (`campid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_subscribers`
--

CREATE TABLE IF NOT EXISTS `email_subscribers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emailaddr` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `ip_addr` varchar(16) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `emailaddr` (`emailaddr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1014 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `body` longblob,
  `tbody` longblob,
  `cid` bigint(20) NOT NULL DEFAULT '0',
  `divid` bigint(20) NOT NULL DEFAULT '0',
  `sendinvoice` int(11) NOT NULL DEFAULT '0',
  `sendcopy` tinyint(1) NOT NULL,
  KEY `id` (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_addon`
--

CREATE TABLE IF NOT EXISTS `email_templates_addon` (
  `tid` bigint(20) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `body` longblob,
  `tbody` longblob,
  `divid` bigint(20) NOT NULL DEFAULT '0',
  `sendinvoice` int(11) NOT NULL DEFAULT '0',
  `sendcopy` tinyint(1) NOT NULL,
  KEY `id` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_template_categories`
--

CREATE TABLE IF NOT EXISTS `email_template_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_variables`
--

CREATE TABLE IF NOT EXISTS `email_variables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) DEFAULT NULL,
  `tabledata` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `skulevel` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `etilize_brand_numbers`
--

CREATE TABLE IF NOT EXISTS `etilize_brand_numbers` (
  `brandname` varchar(200) DEFAULT NULL,
  `brandnumber` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `etilize_categories`
--

CREATE TABLE IF NOT EXISTS `etilize_categories` (
  `catid` bigint(20) DEFAULT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `etilize_map`
--

CREATE TABLE IF NOT EXISTS `etilize_map` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `eid` bigint(20) NOT NULL DEFAULT '0',
  `topcat` bigint(20) DEFAULT NULL,
  `cat` bigint(20) DEFAULT NULL,
  `catlist` varchar(255) DEFAULT NULL,
  `emanid` int(11) DEFAULT NULL,
  `manname` varchar(60) DEFAULT NULL,
  `title` mediumtext,
  `visible` int(11) NOT NULL DEFAULT '0',
  `kit` int(11) NOT NULL DEFAULT '0',
  `price` double(16,2) DEFAULT NULL,
  `manpart` varchar(70) DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `eid` (`eid`),
  KEY `topcat` (`topcat`),
  KEY `cat` (`cat`),
  KEY `catlist` (`catlist`),
  KEY `topcat_2` (`topcat`),
  KEY `manname` (`manname`),
  KEY `topcat_3` (`topcat`),
  KEY `emanid` (`emanid`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_ace_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_ace_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_ace_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_ace_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_ace_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_removal_report`
--

CREATE TABLE IF NOT EXISTS `fba_ace_removal_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_removal_shipping_report`
--

CREATE TABLE IF NOT EXISTS `fba_ace_removal_shipping_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settelement`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_items_newdata` (
  `dataid` int(10) unsigned NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_adjustment_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_adjustment_newdata` (
  `dataid` int(10) NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_order`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `repid` (`repid`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_order_items`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_order_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_order_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_order_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_order_items_new`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_order_items_new` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_order_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_order_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_other`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_settlement_report`
--

CREATE TABLE IF NOT EXISTS `fba_ace_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_ace_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_skus`
--

CREATE TABLE IF NOT EXISTS `fba_ace_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ace_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_ace_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_age_report` (
  `id` bigint(20) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_audit_report`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=302 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=908 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=787 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_skus`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=254 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_alpha_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_alpha_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_audit_report`
--

CREATE TABLE IF NOT EXISTS `fba_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_audit_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_audit_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_estimated_fees`
--

CREATE TABLE IF NOT EXISTS `fba_blue_estimated_fees` (
  `sku` bigint(20) NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_group` varchar(255) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `fulfilled_by` double(9,2) NOT NULL,
  `your_price` double(9,2) NOT NULL,
  `sales_price` double(9,2) NOT NULL,
  `longest_side` double(9,2) NOT NULL,
  `median_side` double(9,2) NOT NULL,
  `shortest_side` double(9,2) NOT NULL,
  `length_and_girth` double(9,2) NOT NULL,
  `unit_of_dimension` varchar(50) NOT NULL,
  `item_package_weight` double(9,2) NOT NULL,
  `unit_of_weight` varchar(50) NOT NULL,
  `product_size_tier` varchar(100) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `estimated_fee` double(9,2) NOT NULL,
  `estimated_referral_fee_per_unit` double(9,2) NOT NULL,
  `estimated_variable_closing_fee` double(9,2) NOT NULL,
  `estimated_order_handling_fee_per_order` double(9,2) NOT NULL,
  `estimated_pick_pack_fee_per_unit` double(9,2) NOT NULL,
  `estimated_weight_handling_fee_per_unit` double(9,2) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_estimated_fees_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_estimated_fees_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3952 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_blue_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_blue_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1078 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_blue_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_removal_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_removal_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_removal_shipping_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_removal_shipping_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settelement`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_items_newdata` (
  `dataid` int(10) unsigned NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_adjustment_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_adjustment_newdata` (
  `dataid` int(10) NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_order`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `repid` (`repid`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=261 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_order_items`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_order_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=355 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_order_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_order_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3121 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_order_items_new`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_order_items_new` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_order_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_order_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_other`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_settlement_report`
--

CREATE TABLE IF NOT EXISTS `fba_blue_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_blue_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=236 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_skus`
--

CREATE TABLE IF NOT EXISTS `fba_blue_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_blue_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_blue_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_ca_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_ca_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36191 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_ca_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_ca_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=458 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_ca_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settelement`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_items_newdata` (
  `dataid` int(10) unsigned NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_adjustment_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_adjustment_newdata` (
  `dataid` int(10) NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_order`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `repid` (`repid`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2400 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_order_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_order_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2587 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_order_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_order_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20597 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_order_items_new`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_order_items_new` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_order_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_order_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_other`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_settlement_report`
--

CREATE TABLE IF NOT EXISTS `fba_ca_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_ca_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_skus`
--

CREATE TABLE IF NOT EXISTS `fba_ca_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10436 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_ca_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_ca_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_estimated_fees`
--

CREATE TABLE IF NOT EXISTS `fba_estimated_fees` (
  `sku` int(10) NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_group` varchar(255) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `fulfilled_by` double(9,2) NOT NULL,
  `your_price` double(9,2) NOT NULL,
  `sales_price` double(9,2) NOT NULL,
  `longest_side` double(9,2) NOT NULL,
  `median_side` double(9,2) NOT NULL,
  `shortest_side` double(9,2) NOT NULL,
  `length_and_girth` double(9,2) NOT NULL,
  `unit_of_dimension` varchar(50) NOT NULL,
  `item_package_weight` double(9,2) NOT NULL,
  `unit_of_weight` varchar(50) NOT NULL,
  `product_size_tier` varchar(100) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `estimated_fee` double(9,2) NOT NULL,
  `estimated_referral_fee_per_unit` double(9,2) NOT NULL,
  `estimated_variable_closing_fee` double(9,2) NOT NULL,
  `estimated_order_handling_fee_per_order` double(9,2) NOT NULL,
  `estimated_pick_pack_fee_per_unit` double(9,2) NOT NULL,
  `estimated_weight_handling_fee_per_unit` double(9,2) NOT NULL,
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sku`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_estimated_fees_report`
--

CREATE TABLE IF NOT EXISTS `fba_estimated_fees_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` tinyint(3) NOT NULL DEFAULT '0',
  `type` tinyint(3) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=364 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_feed_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3917 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_fee_calculator`
--

CREATE TABLE IF NOT EXISTS `fba_fee_calculator` (
  `id` int(10) NOT NULL,
  `asin` varchar(10) NOT NULL,
  `length` double(9,2) NOT NULL,
  `width` double(9,2) NOT NULL,
  `height` double(9,2) NOT NULL,
  `dim_units` varchar(10) NOT NULL,
  `weight` double(9,2) NOT NULL,
  `weight_units` varchar(10) NOT NULL,
  `category` varchar(30) NOT NULL,
  `is_white_gloves_required` enum('Y','N') NOT NULL,
  `price` double(9,2) NOT NULL,
  `prep_service` double(9,2) NOT NULL,
  `weight_handling` double(9,2) NOT NULL,
  `order_handling` double(9,2) NOT NULL,
  `fba_delivery_services` double(9,2) NOT NULL,
  `commission` double(9,2) NOT NULL,
  `pick_and_pack` double(9,2) NOT NULL,
  `storage` double(9,2) NOT NULL,
  `variable_closing` double(9,2) NOT NULL,
  `new_pick_and_pack` double(9,2) NOT NULL,
  `new_weight_handling` double(9,2) NOT NULL,
  `new_order_hanling` double(9,2) NOT NULL,
  `category_percent` double(9,2) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_inventory_mismatch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=695 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_line_items_import` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30236 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_age_report`
--

CREATE TABLE IF NOT EXISTS `fba_max_age_report` (
  `id` int(10) NOT NULL,
  `reportid` varchar(50) NOT NULL,
  `snapshot-date` datetime NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  `product-name` varchar(250) NOT NULL,
  `disposition` varchar(50) NOT NULL,
  `time1` int(10) NOT NULL,
  `time2` int(10) NOT NULL,
  `time3` int(10) NOT NULL,
  `time4` int(10) NOT NULL,
  `time5` int(10) NOT NULL,
  `time6` int(10) NOT NULL,
  `time7` int(10) NOT NULL,
  `total-quantity` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_feed_report`
--

CREATE TABLE IF NOT EXISTS `fba_max_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15492 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_inventory_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_max_inventory_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `fcsku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `cond` varchar(32) DEFAULT NULL,
  `cond_code` varchar(32) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_line_items_import`
--

CREATE TABLE IF NOT EXISTS `fba_max_line_items_import` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_item_id` varchar(24) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipment_item_id` (`shipment_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=526 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_max_order_mismatch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settelement`
--

CREATE TABLE IF NOT EXISTS `fba_max_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `SKU` (`SKU`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_id` (`adj_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=511 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_newdata` (
  `dataid` int(10) unsigned NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adj_item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adj_item_id` (`adj_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_adjustment_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_adjustment_newdata` (
  `dataid` int(10) NOT NULL,
  KEY `dataid` (`dataid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_order`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `repid` int(10) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `repid` (`repid`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2417 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_order_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `amazon_order_id` (`amazon_order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2594 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_order_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_cost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26309 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_order_items_new`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_new` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orid` int(10) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orid` (`orid`),
  KEY `SKU` (`SKU`),
  KEY `OUR_SKU` (`OUR_SKU`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_order_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_order_items_promotion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_other`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_id` (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_settlement_report`
--

CREATE TABLE IF NOT EXISTS `fba_max_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_id` (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_network` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_respons` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `iso_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` int(10) NOT NULL DEFAULT '0',
  `order_created` int(10) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_id` (`iso_id`),
  KEY `po_created` (`po_created`),
  KEY `order_created` (`order_created`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_max_shipment_creator_respons_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sc_id` int(10) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_id` (`sc_id`),
  KEY `seller_sku` (`seller_sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_skus`
--

CREATE TABLE IF NOT EXISTS `fba_max_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `updateprice` tinyint(3) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1374 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_max_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_max_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` int(10) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_order_mismatch`
--

CREATE TABLE IF NOT EXISTS `fba_order_mismatch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(32) DEFAULT NULL,
  `sku` varchar(32) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1476 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_reimburse_report`
--

CREATE TABLE IF NOT EXISTS `fba_reimburse_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_removal_report`
--

CREATE TABLE IF NOT EXISTS `fba_removal_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  `tstamp_server` int(10) NOT NULL,
  `failed` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=884 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_removal_shipping_report`
--

CREATE TABLE IF NOT EXISTS `fba_removal_shipping_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportid` varchar(50) NOT NULL,
  `requestid` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `ack` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(5) NOT NULL,
  `tstamp_server` int(10) NOT NULL,
  `failed` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=961 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settelement`
--

CREATE TABLE IF NOT EXISTS `fba_settelement` (
  `fba_settelement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `settlement_file` varchar(50) NOT NULL,
  `settlement_id` bigint(20) NOT NULL,
  `settlement_start_date` datetime NOT NULL,
  `settlement_end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `transaction_type` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `amount_type` varchar(50) NOT NULL,
  `amount_description` varchar(50) NOT NULL,
  `amount` double(16,2) NOT NULL,
  `fulfillment_id` varchar(50) NOT NULL,
  `posted_date` date NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `order_item_code` varchar(50) NOT NULL,
  `merchant_order_item_id` varchar(50) NOT NULL,
  `merchant_adjustment_item_id` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `quantity_purchased` int(5) NOT NULL,
  `promotion_id` varchar(50) NOT NULL,
  PRIMARY KEY (`fba_settelement_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3357 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `adjustment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_items`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_id` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `RefundCommission` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingHB` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items_cost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=475 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adj_item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `MerchantAdjustmentItemID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_adjustment_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_adjustment_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repid` bigint(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3045 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3191 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items_cost`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_cost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `whid` int(10) NOT NULL,
  `cost` double(16,2) NOT NULL,
  `qty` int(10) NOT NULL,
  `amazon_settlement_id` varchar(20) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10897 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items_cost_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_cost_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items_new`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_new` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orid` bigint(20) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `OUR_SKU` varchar(50) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Principal` double(16,2) NOT NULL,
  `Shipping` double(16,2) NOT NULL,
  `FBAPerUnitFulfillmentFee` double(16,2) NOT NULL,
  `FBAWeightBasedFee` double(16,2) NOT NULL,
  `FBAPerOrderFulfillmentFee` double(16,2) NOT NULL,
  `Commission` double(16,2) NOT NULL,
  `ShippingChargeback` double(16,2) NOT NULL,
  `GiftWrap` double(16,2) NOT NULL,
  `GiftwrapChargeback` double(16,2) NOT NULL,
  `amazon_order_id` varchar(50) NOT NULL,
  `merchant_order_id` varchar(50) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `marketplace_name` varchar(50) NOT NULL,
  `merchant_fulfillment_id` varchar(10) NOT NULL,
  `posted_date` datetime NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_items_promotion`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_items_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `MerchantPromotionID` varchar(255) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderItemCode` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_order_newdata`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_order_newdata` (
  `dataid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_other`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` varchar(50) NOT NULL,
  `TransactionType` varchar(50) NOT NULL,
  `PostedDate` datetime NOT NULL,
  `Amount` double(16,2) NOT NULL,
  `AmazonOrderID` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_settlement_report`
--

CREATE TABLE IF NOT EXISTS `fba_settlement_report` (
  `settlement_report_id` int(10) NOT NULL AUTO_INCREMENT,
  `amazon_settlement_id` varchar(50) NOT NULL,
  `total_amount` double(16,2) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deposit_date` datetime NOT NULL,
  `settlement_report_file` varchar(50) NOT NULL,
  PRIMARY KEY (`settlement_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `poid` bigint(20) NOT NULL,
  `orderid` bigint(20) NOT NULL,
  `assigned` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator_items`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator_link`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` varchar(255) NOT NULL,
  `plan_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator_network`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator_network` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `network_sku` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator_respons`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator_respons` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso_id` bigint(20) NOT NULL,
  `shipment_id` varchar(50) NOT NULL,
  `destination_id` varchar(50) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  `ship_address` varchar(250) NOT NULL,
  `ship_address2` varchar(250) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_country` varchar(50) NOT NULL,
  `ship_zip` varchar(50) NOT NULL,
  `ship_label` varchar(50) NOT NULL,
  `created` tinyint(1) NOT NULL DEFAULT '0',
  `po_created` bigint(20) NOT NULL DEFAULT '0',
  `order_created` bigint(20) NOT NULL DEFAULT '0',
  `plan_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_shipment_creator_respons_items`
--

CREATE TABLE IF NOT EXISTS `fba_shipment_creator_respons_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sc_id` bigint(20) NOT NULL,
  `seller_sku` varchar(50) NOT NULL,
  `fba_sku` varchar(50) NOT NULL,
  `network_sku` varchar(50) NOT NULL,
  `qty` bigint(20) NOT NULL,
  `price` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_skus`
--

CREATE TABLE IF NOT EXISTS `fba_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku` varchar(32) DEFAULT NULL,
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `updateprice` int(11) NOT NULL DEFAULT '0',
  `fcsku` varchar(32) NOT NULL,
  KEY `id` (`id`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2407 ;

-- --------------------------------------------------------

--
-- Table structure for table `fba_sku_map`
--

CREATE TABLE IF NOT EXISTS `fba_sku_map` (
  `fba_sku` varchar(32) DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  KEY `fba_sku` (`fba_sku`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fws_auto_log`
--

CREATE TABLE IF NOT EXISTS `fws_auto_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `msg` text NOT NULL,
  `log_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `generic_shipper_options`
--

CREATE TABLE IF NOT EXISTS `generic_shipper_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipper_code` varchar(50) DEFAULT NULL,
  `manifest_copies` int(11) DEFAULT NULL,
  `email_manifest_to` varchar(255) DEFAULT NULL,
  `invoice_copies` int(11) DEFAULT NULL,
  `email_invoice_to` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `shipper_code` (`shipper_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `god_bless_the_little_birdies`
--

CREATE TABLE IF NOT EXISTS `god_bless_the_little_birdies` (
  `vpid` bigint(20) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `god_bless_the_little_birdies2`
--

CREATE TABLE IF NOT EXISTS `god_bless_the_little_birdies2` (
  `vpid` bigint(20) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `google_cancelled_feed`
--

CREATE TABLE IF NOT EXISTS `google_cancelled_feed` (
  `orderid` int(10) NOT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `google_debug`
--

CREATE TABLE IF NOT EXISTS `google_debug` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6099 ;

-- --------------------------------------------------------

--
-- Table structure for table `google_feed`
--

CREATE TABLE IF NOT EXISTS `google_feed` (
  `orderid` int(10) NOT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `google_status_shipped`
--

CREATE TABLE IF NOT EXISTS `google_status_shipped` (
  `orderid` bigint(20) DEFAULT NULL,
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `headernames`
--

CREATE TABLE IF NOT EXISTS `headernames` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `headernames_headerID` (`headerid`),
  KEY `headernames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `help_items`
--

CREATE TABLE IF NOT EXISTS `help_items` (
  `hid` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `public` int(11) NOT NULL DEFAULT '0',
  `data` text,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `hid` (`hid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Table structure for table `image_spider`
--

CREATE TABLE IF NOT EXISTS `image_spider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_addr` varchar(25) DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  `numhits` int(11) NOT NULL DEFAULT '1',
  KEY `id` (`id`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51743 ;

-- --------------------------------------------------------

--
-- Table structure for table `importlist`
--

CREATE TABLE IF NOT EXISTS `importlist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `importname` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `importlist_columns`
--

CREATE TABLE IF NOT EXISTS `importlist_columns` (
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `importid` bigint(20) NOT NULL DEFAULT '0',
  `tableid` bigint(20) NOT NULL DEFAULT '0',
  `colname` varchar(255) DEFAULT NULL,
  `colunique` int(11) DEFAULT '0',
  `carryover` int(11) DEFAULT '0',
  `carryname` varchar(255) DEFAULT NULL,
  `translate` longblob,
  KEY `lineid` (`lineid`),
  KEY `tableid` (`tableid`),
  KEY `importid` (`importid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `importlist_tables`
--

CREATE TABLE IF NOT EXISTS `importlist_tables` (
  `tableid` bigint(20) NOT NULL AUTO_INCREMENT,
  `importid` bigint(20) NOT NULL DEFAULT '0',
  `tablename` varchar(255) DEFAULT NULL,
  KEY `tableid` (`tableid`),
  KEY `importid` (`importid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ingram_alleta`
--

CREATE TABLE IF NOT EXISTS `ingram_alleta` (
  `imsku` varchar(12) DEFAULT NULL,
  `tstock` varchar(6) DEFAULT NULL,
  `i01_warehouse` char(2) DEFAULT NULL,
  `i01_eta` varchar(8) DEFAULT NULL,
  `i01_stock` varchar(5) DEFAULT NULL,
  `i02_warehouse` char(2) DEFAULT NULL,
  `i02_eta` varchar(8) DEFAULT NULL,
  `i02_stock` varchar(5) DEFAULT NULL,
  `i03_warehouse` char(2) DEFAULT NULL,
  `i03_eta` varchar(8) DEFAULT NULL,
  `i03_stock` varchar(5) DEFAULT NULL,
  `i04_warehouse` char(2) DEFAULT NULL,
  `i04_eta` varchar(8) DEFAULT NULL,
  `i04_stock` varchar(5) DEFAULT NULL,
  `i05_warehouse` char(2) DEFAULT NULL,
  `i05_eta` varchar(8) DEFAULT NULL,
  `i05_stock` varchar(5) DEFAULT NULL,
  `i06_warehouse` char(2) DEFAULT NULL,
  `i06_eta` varchar(8) DEFAULT NULL,
  `i06_stock` varchar(5) DEFAULT NULL,
  `i07_warehouse` char(2) DEFAULT NULL,
  `i07_eta` varchar(8) DEFAULT NULL,
  `i07_stock` varchar(5) DEFAULT NULL,
  `i08_warehouse` char(2) DEFAULT NULL,
  `i08_eta` varchar(8) DEFAULT NULL,
  `i08_stock` varchar(5) DEFAULT NULL,
  `i09_warehouse` char(2) DEFAULT NULL,
  `i09_eta` varchar(8) DEFAULT NULL,
  `i09_stock` varchar(5) DEFAULT NULL,
  `i10_warehouse` char(2) DEFAULT NULL,
  `i10_eta` varchar(8) DEFAULT NULL,
  `i10_stock` varchar(5) DEFAULT NULL,
  `i11_warehouse` char(2) DEFAULT NULL,
  `i11_eta` varchar(8) DEFAULT NULL,
  `i11_stock` varchar(5) DEFAULT NULL,
  `i12_warehouse` char(2) DEFAULT NULL,
  `i12_eta` varchar(8) DEFAULT NULL,
  `i12_stock` varchar(5) DEFAULT NULL,
  KEY `imsku` (`imsku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingram_cats`
--

CREATE TABLE IF NOT EXISTS `ingram_cats` (
  `category` varchar(255) DEFAULT NULL,
  `grp` varchar(4) DEFAULT NULL,
  `cat` varchar(4) DEFAULT NULL,
  KEY `grp` (`grp`,`cat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingram_cats_markup`
--

CREATE TABLE IF NOT EXISTS `ingram_cats_markup` (
  `grp` varchar(4) DEFAULT NULL,
  `cat` varchar(4) DEFAULT NULL,
  `formula` longblob,
  `minincrease` double(16,2) NOT NULL DEFAULT '0.00',
  `mindecrease` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `grp` (`grp`,`cat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingram_map`
--

CREATE TABLE IF NOT EXISTS `ingram_map` (
  `distsku` varchar(12) NOT NULL DEFAULT '',
  `etilizeid` int(11) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`distsku`),
  KEY `etilizeid` (`etilizeid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingram_products`
--

CREATE TABLE IF NOT EXISTS `ingram_products` (
  `im_action` char(2) DEFAULT NULL,
  `dist_sku` varchar(12) DEFAULT NULL,
  `vendorno` varchar(4) DEFAULT NULL,
  `vendor` varchar(35) DEFAULT NULL,
  `descr` varchar(50) DEFAULT NULL,
  `descr2` varchar(50) DEFAULT NULL,
  `price_msrp` double(16,2) DEFAULT NULL,
  `vendor_part` varchar(20) DEFAULT NULL,
  `weight` double(16,2) DEFAULT NULL,
  `upc` varchar(13) DEFAULT NULL,
  `price_change_flag` char(2) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `special` char(2) DEFAULT NULL,
  `available` char(2) DEFAULT NULL,
  `status` char(2) DEFAULT NULL,
  `cpu_code` varchar(10) DEFAULT NULL,
  `media_type` varchar(10) DEFAULT NULL,
  `catsub` varchar(4) DEFAULT NULL,
  `new_item` char(2) DEFAULT NULL,
  `has_rebate` char(2) DEFAULT NULL,
  `dist_sku2` varchar(12) DEFAULT NULL,
  KEY `dist_sku` (`dist_sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_log`
--

CREATE TABLE IF NOT EXISTS `inventory_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `whid` int(11) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `typeid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `whid` (`whid`),
  KEY `tstamp` (`tstamp`),
  KEY `type` (`type`,`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=822067 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_types`
--

CREATE TABLE IF NOT EXISTS `inventory_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `inv_type` enum('A','B') DEFAULT NULL,
  `wh_specific` int(11) NOT NULL DEFAULT '0',
  `valuetype` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`,`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_types_ext`
--

CREATE TABLE IF NOT EXISTS `inventory_types_ext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `itid` bigint(20) DEFAULT NULL,
  `whid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `itid` (`itid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ixtens_uploaded`
--

CREATE TABLE IF NOT EXISTS `ixtens_uploaded` (
  `orderid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jam_countries`
--

CREATE TABLE IF NOT EXISTS `jam_countries` (
  `country_id` int(10) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ship_to` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=242 ;

-- --------------------------------------------------------

--
-- Table structure for table `jam_members`
--

CREATE TABLE IF NOT EXISTS `jam_members` (
  `member_id` int(10) NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(10) NOT NULL DEFAULT '0',
  `original_sponsor_id` int(10) NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `billing_postal_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_postal_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `home_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `work_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_preference_amount` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `primary_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin_id` text COLLATE utf8_unicode_ci NOT NULL,
  `myspace_id` text COLLATE utf8_unicode_ci NOT NULL,
  `paypal_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `moneybookers_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payza_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bank_transfer` text COLLATE utf8_unicode_ci NOT NULL,
  `enable_custom_url` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `custom_url_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `program_custom_field_1` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_2` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_3` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_4` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_5` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_6` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_7` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_8` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_9` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_10` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_11` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_12` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_13` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_14` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_15` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_16` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_17` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_18` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_19` text COLLATE utf8_unicode_ci NOT NULL,
  `program_custom_field_20` text COLLATE utf8_unicode_ci NOT NULL,
  `alert_downline_signup` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `alert_new_commission` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `alert_payment_sent` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `allow_downline_view` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `allow_downline_email` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `view_hidden_programs` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `last_login_date` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_login_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `signup_date` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `updated_on` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `login_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `confirm_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `performance_paid` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `rawaffimported` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `JROX_MEMBERS_NAME` (`fname`,`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4017 ;

-- --------------------------------------------------------

--
-- Table structure for table `labels_return_log`
--

CREATE TABLE IF NOT EXISTS `labels_return_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderid` int(10) NOT NULL,
  `method` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `trackno` varchar(128) NOT NULL,
  `istracked` tinyint(3) NOT NULL,
  `isrefund` tinyint(3) NOT NULL,
  `tstamp` int(10) NOT NULL,
  `ord` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Table structure for table `latest_inventory`
--

CREATE TABLE IF NOT EXISTS `latest_inventory` (
  `sku` varchar(255) DEFAULT NULL,
  `instock` int(11) NOT NULL,
  `avgcost` double(16,2) DEFAULT NULL,
  KEY `sku` (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listeruption_extended`
--

CREATE TABLE IF NOT EXISTS `listeruption_extended` (
  `acctid` bigint(20) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `amount` double(16,2) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  KEY `acctid` (`acctid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_schedule`
--

CREATE TABLE IF NOT EXISTS `listing_schedule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `listid` smallint(5) NOT NULL DEFAULT '0',
  `belowcost` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `listing_schedule_extended`
--

CREATE TABLE IF NOT EXISTS `listing_schedule_extended` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `psid` int(10) NOT NULL,
  `startday` int(10) NOT NULL DEFAULT '-1',
  `starthour` int(10) NOT NULL DEFAULT '-1',
  `startmin` int(10) NOT NULL DEFAULT '-1',
  `endday` int(10) NOT NULL DEFAULT '-1',
  `endhour` int(10) NOT NULL DEFAULT '-1',
  `endmin` int(10) NOT NULL DEFAULT '-1',
  KEY `id` (`id`),
  KEY `psid` (`psid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `listing_schedule_values`
--

CREATE TABLE IF NOT EXISTS `listing_schedule_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `psid` int(10) NOT NULL,
  `pid` int(10) NOT NULL DEFAULT '0',
  `list_add` int(10) NOT NULL,
  `list_remove` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `psid` (`psid`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lists`
--

CREATE TABLE IF NOT EXISTS `lists` (
  `lid` bigint(20) NOT NULL AUTO_INCREMENT,
  `list_title` varchar(255) DEFAULT NULL,
  `list_descr` longblob,
  `bannerpos` int(11) NOT NULL DEFAULT '0',
  `exclusive` int(11) NOT NULL DEFAULT '0',
  `lcid` int(11) NOT NULL,
  KEY `lid` (`lid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

-- --------------------------------------------------------

--
-- Table structure for table `list_categories`
--

CREATE TABLE IF NOT EXISTS `list_categories` (
  `lcid` int(10) NOT NULL AUTO_INCREMENT,
  `lcname` varchar(255) NOT NULL,
  PRIMARY KEY (`lcid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `list_groups`
--

CREATE TABLE IF NOT EXISTS `list_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `list_groups_map`
--

CREATE TABLE IF NOT EXISTS `list_groups_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94 ;

-- --------------------------------------------------------

--
-- Table structure for table `list_products`
--

CREATE TABLE IF NOT EXISTS `list_products` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `lid` bigint(20) NOT NULL DEFAULT '0',
  `lorder` int(11) NOT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `list_products_atcost`
--

CREATE TABLE IF NOT EXISTS `list_products_atcost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `discount_type` enum('atcost','blowout1','blowout2') NOT NULL DEFAULT 'atcost',
  `blowout2_first` double(16,2) NOT NULL DEFAULT '0.00',
  `blowout2_second` double(16,2) NOT NULL DEFAULT '0.00',
  `blowout2_third` double(16,2) NOT NULL DEFAULT '0.00',
  `stock_qty` int(11) NOT NULL DEFAULT '0',
  `stock_qty_available` int(11) NOT NULL DEFAULT '0',
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2020 ;

-- --------------------------------------------------------

--
-- Table structure for table `livefeed_shipfees`
--

CREATE TABLE IF NOT EXISTS `livefeed_shipfees` (
  `shipfee` varchar(100) DEFAULT NULL,
  `id` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locales`
--

CREATE TABLE IF NOT EXISTS `locales` (
  `localeid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `languagecode` varchar(5) NOT NULL DEFAULT '',
  `countrycode` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`localeid`),
  KEY `locales_languageCode` (`languagecode`),
  KEY `locales_countryCode` (`countrycode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_log`
--

CREATE TABLE IF NOT EXISTS `login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `success` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `un` varchar(26) DEFAULT NULL,
  `ip_addr` varchar(26) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=162624 ;

-- --------------------------------------------------------

--
-- Table structure for table `lritems`
--

CREATE TABLE IF NOT EXISTS `lritems` (
  `jobid` bigint(20) DEFAULT NULL,
  `roimast` int(11) DEFAULT NULL,
  `peid` varchar(4) DEFAULT NULL,
  `complete` int(11) DEFAULT NULL,
  `itemid` bigint(20) NOT NULL AUTO_INCREMENT,
  `manpart` varchar(26) DEFAULT NULL,
  `bstamp` datetime DEFAULT NULL,
  `estamp` datetime DEFAULT NULL,
  `refurb` int(11) NOT NULL DEFAULT '0',
  `manname` varchar(128) DEFAULT NULL,
  `lpid` bigint(20) NOT NULL DEFAULT '0',
  `spidered` int(11) NOT NULL DEFAULT '0',
  `upccode` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  KEY `jobid` (`jobid`),
  KEY `jobid_2` (`jobid`,`roimast`),
  KEY `complete` (`complete`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=867 ;

-- --------------------------------------------------------

--
-- Table structure for table `lrqueue`
--

CREATE TABLE IF NOT EXISTS `lrqueue` (
  `jobid` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `numitems` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `company` varchar(128) DEFAULT NULL,
  KEY `jobid` (`jobid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturerid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) NOT NULL DEFAULT '',
  `address1` varchar(60) DEFAULT NULL,
  `address2` varchar(60) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`manufacturerid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mass_price_update`
--

CREATE TABLE IF NOT EXISTS `mass_price_update` (
  `parent_pid` int(10) NOT NULL,
  PRIMARY KEY (`parent_pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `memory_log`
--

CREATE TABLE IF NOT EXISTS `memory_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime NOT NULL,
  `memusage` double(16,2) NOT NULL DEFAULT '0.00',
  `command` text,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `memusage` (`memusage`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15300154 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `public` int(11) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  `colperm` varchar(128) NOT NULL DEFAULT '',
  KEY `mid` (`mid`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Table structure for table `metatag_data`
--

CREATE TABLE IF NOT EXISTS `metatag_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `subtype` varchar(26) DEFAULT 'metatag',
  `typeid` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  `divid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`subtype`,`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrate_ebay_listings`
--

CREATE TABLE IF NOT EXISTS `migrate_ebay_listings` (
  `listid` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `qtysold` int(11) DEFAULT NULL,
  KEY `listid` (`listid`),
  KEY `listid_2` (`listid`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mindreach_date_checked`
--

CREATE TABLE IF NOT EXISTS `mindreach_date_checked` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `date_checked` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `monitor_timestamp`
--

CREATE TABLE IF NOT EXISTS `monitor_timestamp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) DEFAULT NULL,
  `cur_timestamp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `monitor_timestamp_amazonblue`
--

CREATE TABLE IF NOT EXISTS `monitor_timestamp_amazonblue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) DEFAULT NULL,
  `cur_timestamp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=926 ;

-- --------------------------------------------------------

--
-- Table structure for table `msn_orders`
--

CREATE TABLE IF NOT EXISTS `msn_orders` (
  `odate` varchar(50) DEFAULT NULL,
  `ordernum` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `refund` varchar(50) DEFAULT NULL,
  `purchaseid` varchar(50) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `commission` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `multipack_upc_codes`
--

CREATE TABLE IF NOT EXISTS `multipack_upc_codes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5225 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_base_messages`
--

CREATE TABLE IF NOT EXISTS `newegg_base_messages` (
  `mid` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `brid` int(10) NOT NULL,
  `campid` smallint(5) NOT NULL,
  `asin` varchar(50) NOT NULL,
  `pid_lastupdate` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `deleteflag` tinyint(3) NOT NULL,
  `listid` smallint(5) NOT NULL,
  KEY `mid` (`mid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_base_requests`
--

CREATE TABLE IF NOT EXISTS `newegg_base_requests` (
  `brid` int(10) NOT NULL AUTO_INCREMENT,
  `newegg_submissionid` varchar(50) DEFAULT NULL,
  `newegg_requestid` varchar(255) NOT NULL DEFAULT '',
  `tstamp` datetime NOT NULL,
  `campid` int(10) NOT NULL,
  KEY `brid` (`brid`),
  KEY `newegg_submissionid` (`newegg_submissionid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_campaigns`
--

CREATE TABLE IF NOT EXISTS `newegg_campaigns` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `min_profit_pct` double(16,2) NOT NULL DEFAULT '0.00',
  `min_profit_cur` double(16,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '-1',
  `qty_control` int(11) NOT NULL DEFAULT '80',
  `lastupdate` bigint(20) DEFAULT NULL,
  `pricelevel` int(11) NOT NULL DEFAULT '0',
  `min_qty` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_campaign_lists`
--

CREATE TABLE IF NOT EXISTS `newegg_campaign_lists` (
  `campid` int(10) NOT NULL DEFAULT '0',
  `listid` int(10) NOT NULL DEFAULT '0',
  KEY `campid` (`campid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_categories`
--

CREATE TABLE IF NOT EXISTS `newegg_categories` (
  `catid` int(10) NOT NULL,
  `neweggcat` int(10) NOT NULL,
  KEY `catid` (`catid`),
  KEY `neweggcat` (`neweggcat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_categories_pid`
--

CREATE TABLE IF NOT EXISTS `newegg_categories_pid` (
  `pid` int(10) NOT NULL,
  `neweggcat` int(10) NOT NULL,
  KEY `pid` (`pid`),
  KEY `neweggcat` (`neweggcat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_cat_tree`
--

CREATE TABLE IF NOT EXISTS `newegg_cat_tree` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `newegg_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2226 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_duplicates`
--

CREATE TABLE IF NOT EXISTS `newegg_duplicates` (
  `did` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `did` (`did`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_errors`
--

CREATE TABLE IF NOT EXISTS `newegg_errors` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) NOT NULL,
  `campid` int(10) NOT NULL,
  `requestid` varchar(50) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`,`campid`,`requestid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_errors_other`
--

CREATE TABLE IF NOT EXISTS `newegg_errors_other` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `mid` int(10) NOT NULL DEFAULT '0',
  `campid` int(10) NOT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `type` enum('INVENTORY','PRICE','IMAGE','SHIPPING') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `mid` (`mid`),
  KEY `requestid` (`requestid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_feed_item_not_exist`
--

CREATE TABLE IF NOT EXISTS `newegg_feed_item_not_exist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestid` varchar(50) NOT NULL,
  `pid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_feed_report`
--

CREATE TABLE IF NOT EXISTS `newegg_feed_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` enum('NEW','BASE','INVENTORY','PRICE','IMAGE','REPORT','ORDERS','FULFILLMENT','SHIPPING','UNSHIPPED','LISTINGEXPORT') DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `reportid` varchar(50) DEFAULT NULL,
  `requestid` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `complete` tinyint(3) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `type` (`type`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_listing`
--

CREATE TABLE IF NOT EXISTS `newegg_listing` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `campid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `newegg_pid` int(10) NOT NULL,
  `refid` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_log`
--

CREATE TABLE IF NOT EXISTS `newegg_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `userid` int(10) NOT NULL DEFAULT '-1',
  `requestid` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_log_extended`
--

CREATE TABLE IF NOT EXISTS `newegg_log_extended` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `lid` int(10) NOT NULL,
  `type` enum('request','response') DEFAULT NULL,
  `data` longblob,
  KEY `id` (`id`),
  KEY `lid` (`lid`),
  KEY `type` (`type`),
  KEY `lid_2` (`lid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `newegg_log_extended_skus`
--

CREATE TABLE IF NOT EXISTS `newegg_log_extended_skus` (
  `lid` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  KEY `lid` (`lid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nxoi_monitor`
--

CREATE TABLE IF NOT EXISTS `nxoi_monitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `oi_batch`
--

CREATE TABLE IF NOT EXISTS `oi_batch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `oi_batch_ext`
--

CREATE TABLE IF NOT EXISTS `oi_batch_ext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `oibid` bigint(20) NOT NULL,
  `orderid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `oibid` (`oibid`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `oi_profiles`
--

CREATE TABLE IF NOT EXISTS `oi_profiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profilename` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `oi_profiles_ext`
--

CREATE TABLE IF NOT EXISTS `oi_profiles_ext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `oiid` bigint(20) NOT NULL,
  `colid` int(11) DEFAULT NULL,
  `colname` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `oiid` (`oiid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

-- --------------------------------------------------------

--
-- Table structure for table `oneyear`
--

CREATE TABLE IF NOT EXISTS `oneyear` (
  `emailaddr` varchar(255) DEFAULT NULL,
  KEY `emailaddr` (`emailaddr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_category_map`
--

CREATE TABLE IF NOT EXISTS `order_category_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=982827 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_shipped_globegistic`
--

CREATE TABLE IF NOT EXISTS `order_shipped_globegistic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` bigint(20) NOT NULL,
  `in_report` tinyint(5) NOT NULL DEFAULT '0',
  `reportid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_status_codes`
--

CREATE TABLE IF NOT EXISTS `order_status_codes` (
  `status` int(11) NOT NULL AUTO_INCREMENT,
  `internal_status` varchar(16) DEFAULT NULL,
  `client_status` varchar(255) DEFAULT NULL,
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pdps`
--

CREATE TABLE IF NOT EXISTS `pdps` (
  `pid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `pdp` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permalinks`
--

CREATE TABLE IF NOT EXISTS `permalinks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL DEFAULT '',
  `includefile` varchar(255) NOT NULL DEFAULT '',
  `evalcode` varchar(255) NOT NULL DEFAULT '',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `link` (`link`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7280 ;

-- --------------------------------------------------------

--
-- Table structure for table `permalinks2`
--

CREATE TABLE IF NOT EXISTS `permalinks2` (
  `postId` int(20) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL DEFAULT '',
  `sku` varchar(50) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`postId`),
  UNIQUE KEY `link` (`link`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pmw_categories`
--

CREATE TABLE IF NOT EXISTS `pmw_categories` (
  `catid` bigint(20) DEFAULT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pmw_manufacturers`
--

CREATE TABLE IF NOT EXISTS `pmw_manufacturers` (
  `manid` bigint(20) NOT NULL AUTO_INCREMENT,
  `manname` varchar(50) NOT NULL DEFAULT '',
  KEY `manid` (`manid`),
  KEY `manname` (`manname`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8041 ;

-- --------------------------------------------------------

--
-- Table structure for table `pmw_man_map`
--

CREATE TABLE IF NOT EXISTS `pmw_man_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vmanid` bigint(20) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vendorid_1` (`vmanid`),
  KEY `vendorid_2` (`manid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1038 ;

-- --------------------------------------------------------

--
-- Table structure for table `pmw_options`
--

CREATE TABLE IF NOT EXISTS `pmw_options` (
  `overwrite_title` int(11) NOT NULL DEFAULT '0',
  `overwrite_sdescr` int(11) NOT NULL DEFAULT '0',
  `overwrite_specs` int(11) NOT NULL DEFAULT '0',
  `overwrite_staticspecs` int(11) NOT NULL DEFAULT '0',
  `overwrite_images` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pmw_vendor_cat_map`
--

CREATE TABLE IF NOT EXISTS `pmw_vendor_cat_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vcatid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pmw2` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=237 ;

-- --------------------------------------------------------

--
-- Table structure for table `po_discounts`
--

CREATE TABLE IF NOT EXISTS `po_discounts` (
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  `poid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `rstamp` datetime DEFAULT NULL,
  `type` enum('STA','SPA','EOM','VR','HB','BD') DEFAULT NULL,
  `amount` double(16,2) DEFAULT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `pct` double(16,2) DEFAULT NULL,
  `cmemo` varchar(50) DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  KEY `vendorid` (`vendorid`),
  KEY `poid` (`poid`,`pid`),
  KEY `tstamp` (`tstamp`,`rstamp`),
  KEY `poid_2` (`poid`,`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pp`
--

CREATE TABLE IF NOT EXISTS `pp` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `price_msrp` double(16,2) DEFAULT NULL,
  `price_level1` double(16,2) DEFAULT NULL,
  `price_level2` double(16,2) DEFAULT NULL,
  `price_level3` double(16,2) DEFAULT NULL,
  `price_level4` double(16,2) DEFAULT NULL,
  `price_level5` double(16,2) DEFAULT NULL,
  `avgcost` double(16,2) DEFAULT NULL,
  `price_level6` double(16,2) DEFAULT NULL,
  `price_level7` double(16,2) DEFAULT NULL,
  `price_level8` double(16,2) DEFAULT NULL,
  `price_level9` double(16,2) DEFAULT NULL,
  `price_map` double(16,2) DEFAULT NULL,
  `instock` int(11) NOT NULL DEFAULT '0',
  `eta` datetime DEFAULT NULL,
  `onhold` int(11) NOT NULL DEFAULT '0',
  `committed` int(11) NOT NULL DEFAULT '0',
  `onpo` int(11) NOT NULL DEFAULT '0',
  `fco1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco3` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco4` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco5` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco6` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco7` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco8` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fco9` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price_change_log`
--

CREATE TABLE IF NOT EXISTS `price_change_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date_created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6297 ;

-- --------------------------------------------------------

--
-- Table structure for table `price_change_log_items`
--

CREATE TABLE IF NOT EXISTS `price_change_log_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `logid` int(10) NOT NULL,
  `price_old` double(16,2) NOT NULL,
  `price_new` double(16,2) NOT NULL,
  `pid` int(10) NOT NULL,
  `response` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `logid` (`logid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=372215 ;

-- --------------------------------------------------------

--
-- Table structure for table `pricing_schedule`
--

CREATE TABLE IF NOT EXISTS `pricing_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `listid` bigint(20) NOT NULL DEFAULT '0',
  `belowcost` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `pl1` int(11) NOT NULL DEFAULT '0',
  `pl2` int(11) NOT NULL DEFAULT '0',
  `pl3` int(11) NOT NULL DEFAULT '0',
  `pl4` int(11) NOT NULL DEFAULT '0',
  `pl5` int(11) NOT NULL DEFAULT '0',
  `pl6` int(11) NOT NULL DEFAULT '0',
  `pl7` int(11) NOT NULL DEFAULT '0',
  `pl8` int(11) NOT NULL DEFAULT '0',
  `pl9` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `pricing_schedule_extended`
--

CREATE TABLE IF NOT EXISTS `pricing_schedule_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psid` bigint(20) NOT NULL,
  `startday` bigint(20) NOT NULL DEFAULT '-1',
  `starthour` bigint(20) NOT NULL DEFAULT '-1',
  `startmin` bigint(20) NOT NULL DEFAULT '-1',
  `endday` bigint(20) NOT NULL DEFAULT '-1',
  `endhour` bigint(20) NOT NULL DEFAULT '-1',
  `endmin` bigint(20) NOT NULL DEFAULT '-1',
  KEY `id` (`id`),
  KEY `psid` (`psid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `pricing_schedule_prices`
--

CREATE TABLE IF NOT EXISTS `pricing_schedule_prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `pl1` double(16,2) DEFAULT '0.00',
  `pl2` double(16,2) DEFAULT '0.00',
  `pl3` double(16,2) DEFAULT '0.00',
  `pl4` double(16,2) DEFAULT '0.00',
  `pl5` double(16,2) DEFAULT '0.00',
  `pl6` double(16,2) DEFAULT '0.00',
  `pl7` double(16,2) DEFAULT '0.00',
  `pl8` double(16,2) DEFAULT '0.00',
  `pl9` double(16,2) DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `psid` (`psid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Table structure for table `pricing_schedule_prior`
--

CREATE TABLE IF NOT EXISTS `pricing_schedule_prior` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `pl1` double(16,2) DEFAULT '0.00',
  `pl2` double(16,2) DEFAULT '0.00',
  `pl3` double(16,2) DEFAULT '0.00',
  `pl4` double(16,2) DEFAULT '0.00',
  `pl5` double(16,2) DEFAULT '0.00',
  `pl6` double(16,2) DEFAULT '0.00',
  `pl7` double(16,2) DEFAULT '0.00',
  `pl8` double(16,2) DEFAULT '0.00',
  `pl9` double(16,2) DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `psid` (`psid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=140 ;

-- --------------------------------------------------------

--
-- Table structure for table `print_queue`
--

CREATE TABLE IF NOT EXISTS `print_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8247 ;

-- --------------------------------------------------------

--
-- Table structure for table `print_queue_extended`
--

CREATE TABLE IF NOT EXISTS `print_queue_extended` (
  `pqid` bigint(20) NOT NULL,
  `invoiceid` varchar(50) DEFAULT NULL,
  `dorder` int(10) NOT NULL,
  KEY `pqid` (`pqid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `manufacturerid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `mfgpartno` varchar(70) NOT NULL DEFAULT '',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `isaccessory` tinyint(1) NOT NULL DEFAULT '0',
  `equivalency` double NOT NULL DEFAULT '0',
  `creationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifieddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastupdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`productid`),
  KEY `product_isAccessory` (`isaccessory`),
  KEY `product_manufacturerID` (`manufacturerid`),
  KEY `product_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productaccessories`
--

CREATE TABLE IF NOT EXISTS `productaccessories` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `accessoryproductid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ispreferred` tinyint(1) NOT NULL DEFAULT '0',
  `isoption` tinyint(1) NOT NULL DEFAULT '0',
  `note` mediumtext NOT NULL,
  KEY `productaccessories_productID` (`productid`),
  KEY `productaccessories_isPreferred` (`ispreferred`),
  KEY `productacesories_acesoryPID` (`accessoryproductid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productattribute`
--

CREATE TABLE IF NOT EXISTS `productattribute` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `displayvalue` mediumtext,
  `absolutevalue` double NOT NULL DEFAULT '0',
  `unitid` int(11) NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productattribute_productID` (`productid`),
  KEY `productattribute_categoryID` (`categoryid`),
  KEY `productattribute_attributeID` (`attributeid`),
  KEY `productattribute_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productdescriptions`
--

CREATE TABLE IF NOT EXISTS `productdescriptions` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productdescriptions_productID` (`productid`),
  KEY `productdescriptions_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productimages`
--

CREATE TABLE IF NOT EXISTS `productimages` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(60) NOT NULL DEFAULT '',
  `status` varchar(60) NOT NULL DEFAULT '',
  KEY `productimages_productID` (`productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productkeywords`
--

CREATE TABLE IF NOT EXISTS `productkeywords` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `keywords` mediumtext NOT NULL,
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productkeywords_productID` (`productid`),
  KEY `productkeywords_keywords` (`keywords`(255)),
  KEY `productkeywords_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productlocales`
--

CREATE TABLE IF NOT EXISTS `productlocales` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(60) NOT NULL DEFAULT '',
  KEY `productlocales_productID` (`productid`),
  KEY `productlocales_localeID` (`localeid`),
  KEY `productlocales_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productsimilar`
--

CREATE TABLE IF NOT EXISTS `productsimilar` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `similarproductid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productsimilar_productID` (`productid`),
  KEY `productsimilar_spID` (`similarproductid`),
  KEY `productsimilar_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productskus`
--

CREATE TABLE IF NOT EXISTS `productskus` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) DEFAULT NULL,
  `sku` varchar(60) DEFAULT NULL,
  `localeid` int(11) NOT NULL DEFAULT '0',
  `addeddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `discontinueddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `productskus_productID` (`productid`),
  KEY `productskus_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_liveon`
--

CREATE TABLE IF NOT EXISTS `products_liveon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64519 ;

-- --------------------------------------------------------

--
-- Table structure for table `products_liveon_skus`
--

CREATE TABLE IF NOT EXISTS `products_liveon_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38891 ;

-- --------------------------------------------------------

--
-- Table structure for table `products_sourced_details`
--

CREATE TABLE IF NOT EXISTS `products_sourced_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `last_tstamp` datetime NOT NULL,
  `total_qty` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=270562 ;

-- --------------------------------------------------------

--
-- Table structure for table `products_test`
--

CREATE TABLE IF NOT EXISTS `products_test` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `info` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3349808 ;

-- --------------------------------------------------------

--
-- Table structure for table `productupsell`
--

CREATE TABLE IF NOT EXISTS `productupsell` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `upsellproductid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productupsell_productID` (`productid`),
  KEY `productupsell_upsellProductID` (`upsellproductid`),
  KEY `productupsell_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_accessories`
--

CREATE TABLE IF NOT EXISTS `product_accessories` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `accid` bigint(20) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_accessories2`
--

CREATE TABLE IF NOT EXISTS `product_accessories2` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `accid` bigint(20) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_alerts`
--

CREATE TABLE IF NOT EXISTS `product_alerts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `type` enum('STOCKALERT','priceALERT') DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `ip_addr` varchar(16) DEFAULT NULL,
  `time` varchar(100) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `accntid` bigint(20) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_bulk_options`
--

CREATE TABLE IF NOT EXISTS `product_bulk_options` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `bulk_qty_min` int(11) NOT NULL,
  `bulk_qty_max` int(11) NOT NULL,
  `bulk_price` double(9,2) NOT NULL DEFAULT '0.00',
  `bulk_unit` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1732 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_bulk_options_promo`
--

CREATE TABLE IF NOT EXISTS `product_bulk_options_promo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `bulk_qty_min` int(11) NOT NULL,
  `bulk_qty_max` int(11) NOT NULL,
  `bulk_price` double(9,2) NOT NULL DEFAULT '0.00',
  `bulk_unit` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2021 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `catid` bigint(20) DEFAULT NULL,
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visible` int(11) NOT NULL DEFAULT '1',
  `count` int(11) NOT NULL DEFAULT '0',
  `cat_banner` varchar(255) NOT NULL,
  `cat_thumb` varchar(255) NOT NULL,
  `amazonpct` double(16,2) NOT NULL DEFAULT '0.00',
  `component` int(11) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories_description`
--

CREATE TABLE IF NOT EXISTS `product_categories_description` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) NOT NULL,
  `description` blob NOT NULL,
  `divid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=596 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories_order`
--

CREATE TABLE IF NOT EXISTS `product_categories_order` (
  `catid` bigint(20) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories_title`
--

CREATE TABLE IF NOT EXISTS `product_categories_title` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `catid` int(20) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `divid` int(20) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=165 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_category_map`
--

CREATE TABLE IF NOT EXISTS `product_category_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `sort_order` int(10) NOT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=95444 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_commission`
--

CREATE TABLE IF NOT EXISTS `product_commission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `commission` double(16,4) DEFAULT NULL,
  `type` enum('FLAT','PERCENT') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_descr`
--

CREATE TABLE IF NOT EXISTS `product_descr` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` text,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_descr_amz`
--

CREATE TABLE IF NOT EXISTS `product_descr_amz` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` text,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_differentials`
--

CREATE TABLE IF NOT EXISTS `product_differentials` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_differentials_override`
--

CREATE TABLE IF NOT EXISTS `product_differentials_override` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `sid` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  `userid` bigint(20) NOT NULL DEFAULT '0',
  KEY `pid` (`pid`,`sid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_dimensions`
--

CREATE TABLE IF NOT EXISTS `product_dimensions` (
  `pid` int(10) NOT NULL,
  `width` double(16,2) NOT NULL,
  `height` double(16,2) NOT NULL,
  `length` double(16,2) NOT NULL,
  UNIQUE KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_docs`
--

CREATE TABLE IF NOT EXISTS `product_docs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `pid_2` (`pid`,`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17403 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_favorite`
--

CREATE TABLE IF NOT EXISTS `product_favorite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `accid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8375 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_featured`
--

CREATE TABLE IF NOT EXISTS `product_featured` (
  `id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_features`
--

CREATE TABLE IF NOT EXISTS `product_features` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `feature` varchar(255) DEFAULT NULL,
  `fid` bigint(20) NOT NULL AUTO_INCREMENT,
  KEY `id` (`id`),
  KEY `fid` (`fid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46969 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_groups`
--

CREATE TABLE IF NOT EXISTS `product_groups` (
  `grpid` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `subid` int(11) NOT NULL DEFAULT '0',
  `grpdescr` varchar(255) DEFAULT NULL,
  `catdescr` varchar(255) DEFAULT NULL,
  `subdescr` varchar(255) DEFAULT NULL,
  KEY `grpid` (`grpid`,`catid`,`subid`),
  KEY `grpdescr` (`grpdescr`,`catdescr`,`subdescr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_html`
--

CREATE TABLE IF NOT EXISTS `product_html` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` text,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `idata` longblob,
  `itype` int(11) DEFAULT NULL,
  `thumb` int(11) NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `imageid` bigint(20) NOT NULL AUTO_INCREMENT,
  `divid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `imageid` (`imageid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28170 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_images_md5`
--

CREATE TABLE IF NOT EXISTS `product_images_md5` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `itype` int(11) DEFAULT NULL,
  `thumb` int(11) DEFAULT NULL,
  `md5` varchar(32) DEFAULT NULL,
  KEY `id` (`id`,`itype`,`thumb`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_inthebox`
--

CREATE TABLE IF NOT EXISTS `product_inthebox` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory`
--

CREATE TABLE IF NOT EXISTS `product_inventory` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `distname` varchar(25) NOT NULL DEFAULT '',
  `warehouse` varchar(50) NOT NULL DEFAULT '',
  `wh_state` char(2) NOT NULL DEFAULT 'XX',
  `inv_available` bigint(20) NOT NULL DEFAULT '0',
  `inv_backorder` bigint(20) NOT NULL DEFAULT '0',
  `inv_onorder` bigint(20) NOT NULL DEFAULT '0',
  `eta` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cost` double(16,2) DEFAULT NULL,
  `reccost` double(16,2) DEFAULT NULL,
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `whid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `lineid` (`lineid`),
  KEY `warehouse` (`warehouse`),
  KEY `id_2` (`id`,`whid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1957824 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory_b`
--

CREATE TABLE IF NOT EXISTS `product_inventory_b` (
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `whid` bigint(20) NOT NULL DEFAULT '0',
  `inv_available` bigint(20) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  KEY `lineid` (`lineid`),
  KEY `pid` (`pid`,`whid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory_cache`
--

CREATE TABLE IF NOT EXISTS `product_inventory_cache` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_keywords`
--

CREATE TABLE IF NOT EXISTS `product_keywords` (
  `id` bigint(20) NOT NULL,
  `kw1` varchar(128) DEFAULT NULL,
  `kw2` varchar(128) DEFAULT NULL,
  `kw3` varchar(128) DEFAULT NULL,
  `kw4` varchar(128) DEFAULT NULL,
  `kw5` varchar(128) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kitskus`
--

CREATE TABLE IF NOT EXISTS `product_kitskus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `kid` bigint(20) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `kid` (`kid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28897 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_list`
--

CREATE TABLE IF NOT EXISTS `product_list` (
  `id` bigint(20) NOT NULL DEFAULT '-1',
  `manid` bigint(20) NOT NULL DEFAULT '-1',
  `manpart` varchar(50) NOT NULL DEFAULT '',
  `descr` varchar(255) NOT NULL DEFAULT 'NO DESCRIPTION',
  `added_dte` bigint(20) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `returnable` int(11) NOT NULL DEFAULT '0',
  `upccode` varchar(32) DEFAULT NULL,
  `platform` varchar(7) NOT NULL DEFAULT '',
  `grp` int(11) NOT NULL DEFAULT '0',
  `cat` int(11) NOT NULL DEFAULT '0',
  `sub` int(11) NOT NULL DEFAULT '0',
  `roimast` bigint(20) NOT NULL DEFAULT '-1',
  `dropship` int(11) NOT NULL DEFAULT '0',
  `shipweight` double(16,2) NOT NULL DEFAULT '0.00',
  `oem` int(11) NOT NULL DEFAULT '0',
  `refurb` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(50) NOT NULL DEFAULT '',
  `subsku` varchar(50) NOT NULL DEFAULT '',
  `visible` int(11) NOT NULL DEFAULT '0',
  `kit` int(11) NOT NULL DEFAULT '0',
  `demo` int(11) NOT NULL DEFAULT '0',
  `hidestock` int(11) NOT NULL DEFAULT '0',
  `shipping_flag` int(11) NOT NULL DEFAULT '0',
  `usemap` int(11) NOT NULL DEFAULT '0',
  `mappopup` int(11) NOT NULL DEFAULT '0',
  `mapcart` int(11) NOT NULL DEFAULT '0',
  `shipcode` varchar(26) NOT NULL DEFAULT '',
  `alwaysinstock` int(11) NOT NULL DEFAULT '0',
  `alwaysoutstock` int(11) NOT NULL DEFAULT '0',
  `invisibleonstock` int(11) NOT NULL DEFAULT '0',
  `shipsin` int(11) NOT NULL DEFAULT '1',
  `minqty` int(11) NOT NULL DEFAULT '0',
  `targetqty` int(11) NOT NULL DEFAULT '0',
  `pricenote` varchar(255) DEFAULT NULL,
  `asin` varchar(50) DEFAULT NULL,
  `am_qty` int(11) NOT NULL DEFAULT '0',
  `am_std_ship` double(16,2) DEFAULT NULL,
  `am_exp_ship` double(16,2) DEFAULT NULL,
  `kit_fixed` double(16,2) DEFAULT NULL,
  `kit_value` double(16,2) DEFAULT NULL,
  `amazon_realtime` int(11) NOT NULL DEFAULT '0',
  `modelno` varchar(30) NOT NULL DEFAULT '',
  `serialized` int(11) NOT NULL DEFAULT '0',
  `cmanpart` varchar(50) NOT NULL,
  `preorder` int(11) NOT NULL DEFAULT '0',
  `sysvis` int(11) NOT NULL DEFAULT '0',
  `rtdsinv` int(11) NOT NULL DEFAULT '0',
  `lastupdate` bigint(20) NOT NULL DEFAULT '0',
  `am_latency` int(11) NOT NULL DEFAULT '1',
  `use_asin` int(11) NOT NULL DEFAULT '0',
  `series` int(11) NOT NULL DEFAULT '0',
  `eb_std_ship` double(16,2) NOT NULL DEFAULT '-1.00',
  `eb_exp_ship` double(16,2) NOT NULL DEFAULT '-1.00',
  `am_update_override` int(11) NOT NULL DEFAULT '0',
  `am_condition` enum('New','UsedLikeNew','VeryGood','Good','Acceptable','Refurbished') DEFAULT NULL,
  `amazon_ca_asin` varchar(50) NOT NULL,
  `amazon_ca_am_qty` int(11) NOT NULL,
  `amazon_ca_am_std_ship` double(16,2) NOT NULL,
  `amazon_ca_am_exp_ship` double(16,2) NOT NULL,
  `amazon_ca_amazon_realtime` int(11) NOT NULL,
  `amazon_ca_am_latency` int(11) NOT NULL,
  `amazon_ca_use_asin` int(11) NOT NULL,
  `amazon_ca_am_update_override` int(11) NOT NULL,
  `amazon_ca_am_condition` enum('New','UsedLikeNew','VeryGood','Good','Acceptable','Refurbished') NOT NULL,
  `amazon_ca_realtime` int(11) NOT NULL,
  `amazonmax_asin` varchar(50) NOT NULL,
  `amazonmax_am_qty` int(11) NOT NULL,
  `amazonmax_am_std_ship` double(16,2) NOT NULL,
  `amazonmax_am_exp_ship` double(16,2) NOT NULL,
  `amazonmax_amazon_realtime` int(11) NOT NULL,
  `amazonmax_am_latency` int(11) NOT NULL,
  `amazonmax_use_asin` int(11) NOT NULL,
  `amazonmax_am_update_override` int(11) NOT NULL,
  `amazonmax_am_condition` enum('New','UsedLikeNew','VeryGood','Good','Acceptable','Refurbished') NOT NULL,
  `amazonmax_realtime` varchar(10) NOT NULL,
  `amazonblue_asin` varchar(50) NOT NULL,
  `amazonblue_am_qty` int(11) NOT NULL,
  `amazonblue_am_std_ship` double(16,2) NOT NULL,
  `amazonblue_am_exp_ship` double(16,2) NOT NULL,
  `amazonblue_amazon_realtime` int(11) NOT NULL,
  `amazonblue_am_latency` int(11) NOT NULL,
  `amazonblue_use_asin` int(11) NOT NULL,
  `amazonblue_am_update_override` int(11) NOT NULL,
  `amazonblue_am_condition` enum('New','UsedLikeNew','VeryGood','Good','Acceptable','Refurbished') NOT NULL,
  `amazonace_asin` varchar(50) NOT NULL,
  `amazonace_am_qty` int(11) NOT NULL,
  `amazonace_am_std_ship` double(16,2) NOT NULL,
  `amazonace_am_exp_ship` double(16,2) NOT NULL,
  `amazonace_amazon_realtime` int(11) NOT NULL,
  `amazonace_am_latency` int(11) NOT NULL,
  `amazonace_use_asin` int(11) NOT NULL,
  `amazonace_am_update_override` int(11) NOT NULL,
  `amazonace_am_condition` enum('New','UsedLikeNew','VeryGood','Good','Acceptable','Refurbished') NOT NULL,
  `amazon_alpha_realtime` int(11) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `manid` (`manid`,`manpart`),
  KEY `grp` (`grp`,`cat`,`sub`),
  KEY `descr` (`descr`),
  KEY `added_dte` (`added_dte`),
  KEY `status` (`status`,`returnable`),
  KEY `upccode` (`upccode`),
  KEY `manpart` (`manpart`),
  KEY `status_2` (`status`),
  KEY `cmanpart` (`cmanpart`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_manufacturers`
--

CREATE TABLE IF NOT EXISTS `product_manufacturers` (
  `manid` bigint(20) NOT NULL AUTO_INCREMENT,
  `manname` varchar(50) NOT NULL DEFAULT '',
  KEY `manid` (`manid`),
  KEY `manname` (`manname`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=912 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_manufacturers_image`
--

CREATE TABLE IF NOT EXISTS `product_manufacturers_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `manid` int(10) NOT NULL,
  `image` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_move_category`
--

CREATE TABLE IF NOT EXISTS `product_move_category` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `catid` bigint(20) NOT NULL DEFAULT '0',
  KEY `pid` (`pid`,`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_notify`
--

CREATE TABLE IF NOT EXISTS `product_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `accid` int(10) NOT NULL,
  `notify` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3529 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE IF NOT EXISTS `product_prices` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `price_msrp` double(16,2) DEFAULT NULL,
  `price_level1` double(16,2) DEFAULT NULL,
  `price_level2` double(16,2) DEFAULT NULL,
  `price_level3` double(16,2) DEFAULT NULL,
  `price_level4` double(16,2) DEFAULT NULL,
  `price_level5` double(16,2) DEFAULT NULL,
  `avgcost` double(16,2) DEFAULT NULL,
  `price_level6` double(16,2) DEFAULT NULL,
  `price_level7` double(16,2) DEFAULT NULL,
  `price_level8` double(16,2) DEFAULT NULL,
  `price_level9` double(16,2) DEFAULT NULL,
  `price_map` double(16,2) DEFAULT NULL,
  `instock` int(11) NOT NULL DEFAULT '0',
  `eta` datetime DEFAULT NULL,
  `onhold` int(11) NOT NULL DEFAULT '0',
  `committed` int(11) NOT NULL DEFAULT '0',
  `onpo` int(11) NOT NULL DEFAULT '0',
  `fco1` int(11) NOT NULL DEFAULT '0',
  `fco2` int(11) NOT NULL DEFAULT '0',
  `fco3` int(11) NOT NULL DEFAULT '0',
  `fco4` int(11) NOT NULL DEFAULT '0',
  `fco5` int(11) NOT NULL DEFAULT '0',
  `fco6` int(11) NOT NULL DEFAULT '0',
  `fco7` int(11) NOT NULL DEFAULT '0',
  `fco8` int(11) NOT NULL DEFAULT '0',
  `fco9` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `price_level1` (`price_level1`),
  KEY `id_2` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_profit_cached`
--

CREATE TABLE IF NOT EXISTS `product_profit_cached` (
  `pid` bigint(20) NOT NULL,
  `pl1pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl1cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl2pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl2cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl3pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl3cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl4pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl4cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl5pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl5cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl6pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl6cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl7pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl7cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl8pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl8cur` double(16,2) NOT NULL DEFAULT '0.00',
  `pl9pct` double(16,2) NOT NULL DEFAULT '0.00',
  `pl9cur` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_purchase_history`
--

CREATE TABLE IF NOT EXISTS `product_purchase_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `vendorid` bigint(20) DEFAULT NULL,
  `vendorname` varchar(128) DEFAULT NULL,
  `poid` bigint(20) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `tstamp` (`tstamp`),
  KEY `manid` (`manid`),
  KEY `vendorid` (`vendorid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172817 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

CREATE TABLE IF NOT EXISTS `product_related` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `relid` bigint(20) NOT NULL DEFAULT '0',
  `relationship` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_search_extended`
--

CREATE TABLE IF NOT EXISTS `product_search_extended` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `dsqty` int(11) NOT NULL DEFAULT '0',
  `scost` double(16,2) NOT NULL DEFAULT '0.00',
  `displayname` varchar(128) DEFAULT NULL,
  `roimast_count` int(11) DEFAULT NULL,
  `imagecount` int(11) DEFAULT NULL,
  `cattree` text,
  `sitelist` text,
  `inlist` int(11) NOT NULL DEFAULT '0',
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_shipping_limit`
--

CREATE TABLE IF NOT EXISTS `product_shipping_limit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `countries_list` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_sourcing`
--

CREATE TABLE IF NOT EXISTS `product_sourcing` (
  `sid` bigint(20) NOT NULL AUTO_INCREMENT,
  `distid` varchar(16) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `distsku` varchar(50) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `qty` bigint(20) NOT NULL DEFAULT '0',
  `tstamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `vpid` bigint(20) NOT NULL DEFAULT '0',
  `useinv` int(11) NOT NULL DEFAULT '0',
  `stealth` int(11) NOT NULL DEFAULT '0',
  `basecost` int(11) NOT NULL DEFAULT '0',
  `oprice` double(16,2) DEFAULT NULL,
  KEY `sid` (`sid`),
  KEY `distid` (`distid`,`distsku`),
  KEY `manid` (`manid`,`manpart`),
  KEY `sid_2` (`sid`),
  KEY `vpid` (`vpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2703533 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_specs`
--

CREATE TABLE IF NOT EXISTS `product_specs` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `section` varchar(255) DEFAULT NULL,
  `dkey` varchar(255) DEFAULT NULL,
  `dval` varchar(255) DEFAULT NULL,
  `specsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `divid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `specsid` (`specsid`),
  KEY `id_2` (`id`,`dkey`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1478495 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_static_order`
--

CREATE TABLE IF NOT EXISTS `product_static_order` (
  `pid` bigint(20) NOT NULL,
  `cid` bigint(20) NOT NULL,
  `lid` bigint(20) NOT NULL,
  `order_val` bigint(20) NOT NULL,
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9358 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_static_specs`
--

CREATE TABLE IF NOT EXISTS `product_static_specs` (
  `id` bigint(20) DEFAULT NULL,
  `tid` bigint(20) DEFAULT NULL,
  `sscid` bigint(20) DEFAULT NULL,
  `dvalue` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tid` (`tid`),
  KEY `sscid` (`sscid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_taxonomy`
--

CREATE TABLE IF NOT EXISTS `product_taxonomy` (
  `pid` bigint(20) NOT NULL,
  `taxonomy` text,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_translation`
--

CREATE TABLE IF NOT EXISTS `product_translation` (
  `divid` int(11) NOT NULL DEFAULT '0',
  `productid` varchar(50) NOT NULL DEFAULT '',
  `newproductid` bigint(20) DEFAULT NULL,
  KEY `divid` (`divid`,`productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_warranty`
--

CREATE TABLE IF NOT EXISTS `product_warranty` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `promoid` bigint(20) NOT NULL AUTO_INCREMENT,
  `promoname` varchar(255) NOT NULL DEFAULT '',
  `begin_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `type` varchar(25) DEFAULT NULL,
  `amount` double(16,2) DEFAULT NULL,
  KEY `promoid` (`promoid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `promotion_skus`
--

CREATE TABLE IF NOT EXISTS `promotion_skus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promoid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `manpart` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `amount` double(16,2) DEFAULT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  KEY `id` (`id`),
  KEY `promoid` (`promoid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4937 ;

-- --------------------------------------------------------

--
-- Table structure for table `pronto_categories`
--

CREATE TABLE IF NOT EXISTS `pronto_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `prontocat` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`prontocat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rawreform_listing`
--

CREATE TABLE IF NOT EXISTS `rawreform_listing` (
  `sku` varchar(32) DEFAULT NULL,
  `asin` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  KEY `sku` (`sku`),
  KEY `asin` (`asin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rebates`
--

CREATE TABLE IF NOT EXISTS `rebates` (
  `rid` bigint(20) NOT NULL AUTO_INCREMENT,
  `rebate_title` varchar(255) DEFAULT NULL,
  `rebate_descr` longblob,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `postmark_date` datetime DEFAULT NULL,
  `rebate_amount` double(16,2) DEFAULT NULL,
  `rebate_type` enum('MAIL-IN','INSTANT') DEFAULT NULL,
  `apply2price` int(11) NOT NULL DEFAULT '0',
  `promotion` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  KEY `rid` (`rid`),
  KEY `rebate_title` (`rebate_title`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `rebates_extended`
--

CREATE TABLE IF NOT EXISTS `rebates_extended` (
  `rxid` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  KEY `rxid` (`rxid`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `rebates_products`
--

CREATE TABLE IF NOT EXISTS `rebates_products` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `rid` bigint(20) NOT NULL DEFAULT '0',
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `manid` bigint(20) NOT NULL DEFAULT '0',
  `minval` double(16,2) DEFAULT NULL,
  `maxval` double(16,2) DEFAULT NULL,
  KEY `pid` (`pid`),
  KEY `rid` (`rid`),
  KEY `catid` (`catid`,`manid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rebates_site`
--

CREATE TABLE IF NOT EXISTS `rebates_site` (
  `rsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `rid` bigint(20) NOT NULL,
  `divid` bigint(20) NOT NULL,
  KEY `rsid` (`rsid`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=114 ;

-- --------------------------------------------------------

--
-- Table structure for table `recalc_altsku_queue`
--

CREATE TABLE IF NOT EXISTS `recalc_altsku_queue` (
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `whid` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recalc_inventory_queue`
--

CREATE TABLE IF NOT EXISTS `recalc_inventory_queue` (
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `session` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `time` date NOT NULL,
  `aprove` tinyint(1) NOT NULL,
  `rating` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

-- --------------------------------------------------------

--
-- Table structure for table `review_helpfull`
--

CREATE TABLE IF NOT EXISTS `review_helpfull` (
  `product_id` int(10) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `value` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rfw_featured_items`
--

CREATE TABLE IF NOT EXISTS `rfw_featured_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_title` varchar(255) NOT NULL,
  `item_desc` text NOT NULL,
  `item_link` varchar(255) NOT NULL,
  `item_img` varchar(255) NOT NULL,
  `ord` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

-- --------------------------------------------------------

--
-- Table structure for table `rma_management`
--

CREATE TABLE IF NOT EXISTS `rma_management` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `condition` varchar(50) NOT NULL,
  `whse_notes` varchar(100) NOT NULL,
  `mgr_notes` varchar(100) NOT NULL,
  `assigned` bigint(20) NOT NULL,
  `vendor` bigint(20) NOT NULL,
  `checked` int(5) NOT NULL,
  `qty` int(10) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `rma_management_wh`
--

CREATE TABLE IF NOT EXISTS `rma_management_wh` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rmam_id` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL,
  `whid` int(10) NOT NULL,
  `wh_qty` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_cost`
--

CREATE TABLE IF NOT EXISTS `salesman_cost` (
  `scid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `pid` bigint(20) DEFAULT NULL,
  `sdollar` double(16,2) DEFAULT NULL,
  `spercent` double(16,2) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  KEY `scid` (`scid`),
  KEY `pid` (`pid`,`type`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34041 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_order_search`
--

CREATE TABLE IF NOT EXISTS `saved_order_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sdata` longblob,
  `public` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=354 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_picking_search`
--

CREATE TABLE IF NOT EXISTS `saved_picking_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sdata` longblob,
  `public` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_po_search`
--

CREATE TABLE IF NOT EXISTS `saved_po_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sdata` longblob,
  `public` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `saved_sku_search`
--

CREATE TABLE IF NOT EXISTS `saved_sku_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sdata` longblob,
  `public` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `search_attribute`
--

CREATE TABLE IF NOT EXISTS `search_attribute` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `valueid` int(11) NOT NULL DEFAULT '0',
  `absolutevalue` double NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `search_attribute_productID` (`productid`),
  KEY `search_attribute_attributeID` (`attributeid`),
  KEY `search_attribute_valueID` (`valueid`),
  KEY `search_attribute_absoluteValue` (`absolutevalue`),
  KEY `search_attribute_isAbsolute` (`isabsolute`),
  KEY `search_attribute_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_attribute_values`
--

CREATE TABLE IF NOT EXISTS `search_attribute_values` (
  `valueid` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  `absolutevalue` double NOT NULL DEFAULT '0',
  `unitid` int(11) NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`valueid`),
  KEY `search_attrval_value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_cache_hint`
--

CREATE TABLE IF NOT EXISTS `search_cache_hint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `searchphrase` varchar(128) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `typeid` bigint(20) NOT NULL DEFAULT '0',
  `automated` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `searchphrase` (`searchphrase`),
  KEY `automated` (`automated`),
  KEY `searchphrase_2` (`searchphrase`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118531 ;

-- --------------------------------------------------------

--
-- Table structure for table `sears_categories`
--

CREATE TABLE IF NOT EXISTS `sears_categories` (
  `sears_catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `sears_parentid` bigint(20) NOT NULL,
  `sears_catname` varchar(255) NOT NULL,
  `sears_tag` varchar(255) NOT NULL,
  `sears_catfee` double(16,2) NOT NULL,
  PRIMARY KEY (`sears_catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3615 ;

-- --------------------------------------------------------

--
-- Table structure for table `sellery`
--

CREATE TABLE IF NOT EXISTS `sellery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2619 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(128) DEFAULT NULL,
  `trackid` varchar(128) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `ip_addr` varchar(20) DEFAULT NULL,
  `curpage` varchar(255) DEFAULT NULL,
  `pagedescr` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`),
  KEY `tstamp` (`tstamp`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40027688 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_archive`
--

CREATE TABLE IF NOT EXISTS `sessions_archive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(128) DEFAULT NULL,
  `trackid` varchar(128) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `ip_addr` varchar(20) DEFAULT NULL,
  `curpage` varchar(255) DEFAULT NULL,
  `pagedescr` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`),
  KEY `tstamp` (`tstamp`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_campaigns`
--

CREATE TABLE IF NOT EXISTS `sessions_campaigns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `campname` varchar(128) DEFAULT NULL,
  `reftag` varchar(128) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_filter`
--

CREATE TABLE IF NOT EXISTS `sessions_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_addr` varchar(20) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_log`
--

CREATE TABLE IF NOT EXISTS `sessions_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `trackid` varchar(128) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `trackid` (`trackid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1698163 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_traffic`
--

CREATE TABLE IF NOT EXISTS `sessions_traffic` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `sessionid` varchar(128) DEFAULT NULL,
  `trackid` varchar(128) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `pageurl` varchar(255) DEFAULT NULL,
  `referrer` text,
  `action` int(11) DEFAULT NULL,
  `pagedescr` varchar(128) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `cookie` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sessions_traffic_archive`
--

CREATE TABLE IF NOT EXISTS `sessions_traffic_archive` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `sessionid` varchar(128) DEFAULT NULL,
  `trackid` varchar(128) DEFAULT NULL,
  `tstamp` bigint(20) DEFAULT NULL,
  `pageurl` varchar(255) DEFAULT NULL,
  `referrer` text,
  `action` int(11) DEFAULT NULL,
  `pagedescr` varchar(128) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `cookie` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `sessionid` (`sessionid`,`trackid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_assignment`
--

CREATE TABLE IF NOT EXISTS `shipping_assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scid` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `rate` double(16,2) NOT NULL DEFAULT '-1.00',
  KEY `id` (`id`),
  KEY `scid` (`scid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4647 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_codes`
--

CREATE TABLE IF NOT EXISTS `shipping_codes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '0',
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `flatrate` int(11) NOT NULL DEFAULT '0',
  `method_name` varchar(255) DEFAULT NULL,
  `method_descr` varchar(255) DEFAULT NULL,
  `method_note` text,
  `formula` longblob,
  `method_html_descr` longblob,
  `tracklink` varchar(255) DEFAULT NULL,
  `isdefault` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `catid` (`catid`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=352 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_defaults`
--

CREATE TABLE IF NOT EXISTS `shipping_defaults` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `carrier` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `options` varchar(255) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `carrier` (`carrier`,`method`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_grid`
--

CREATE TABLE IF NOT EXISTS `shipping_grid` (
  `min` double(16,2) DEFAULT NULL,
  `max` double(16,2) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  KEY `min` (`min`,`max`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ship_method_mapper`
--

CREATE TABLE IF NOT EXISTS `ship_method_mapper` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `methodname` varchar(255) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `shop_categories`
--

CREATE TABLE IF NOT EXISTS `shop_categories` (
  `catid` bigint(20) NOT NULL DEFAULT '0',
  `shopcat` bigint(20) NOT NULL DEFAULT '0',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  KEY `catid` (`catid`),
  KEY `amazoncat` (`shopcat`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_orders_uploaded`
--

CREATE TABLE IF NOT EXISTS `shop_orders_uploaded` (
  `orderid` bigint(20) NOT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `orderid` (`orderid`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_list`
--

CREATE TABLE IF NOT EXISTS `site_list` (
  `divid` bigint(20) NOT NULL AUTO_INCREMENT,
  `sitename` varchar(255) DEFAULT NULL,
  `rootdir` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  KEY `divid` (`divid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_skus`
--

CREATE TABLE IF NOT EXISTS `site_skus` (
  `divid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  KEY `divid` (`divid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sku_assignment`
--

CREATE TABLE IF NOT EXISTS `sku_assignment` (
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  KEY `pid` (`pid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1008792 ;

-- --------------------------------------------------------

--
-- Table structure for table `spidercache`
--

CREATE TABLE IF NOT EXISTS `spidercache` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `engine` varchar(20) DEFAULT NULL,
  `manpart` varchar(20) DEFAULT NULL,
  `oursku` varchar(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `dkey` varchar(100) DEFAULT NULL,
  `dvalue` varchar(100) DEFAULT NULL,
  `descr` longblob,
  `specs` longblob,
  `image` longblob,
  PRIMARY KEY (`dataid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1328 ;

-- --------------------------------------------------------

--
-- Table structure for table `static_specs_columns`
--

CREATE TABLE IF NOT EXISTS `static_specs_columns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tid` bigint(20) DEFAULT NULL,
  `dkey` varchar(255) DEFAULT NULL,
  `dorder` int(11) NOT NULL DEFAULT '0',
  `dtype` varchar(50) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `tid` (`tid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2040 ;

-- --------------------------------------------------------

--
-- Table structure for table `static_specs_templates`
--

CREATE TABLE IF NOT EXISTS `static_specs_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `cat` int(11) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `template` (`template`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

-- --------------------------------------------------------

--
-- Table structure for table `store_cart`
--

CREATE TABLE IF NOT EXISTS `store_cart` (
  `cart_id` int(10) NOT NULL AUTO_INCREMENT,
  `cart_session` varchar(255) NOT NULL,
  `cart_email` varchar(255) NOT NULL,
  `cart_step1` tinyint(1) NOT NULL,
  `cart_step2` tinyint(1) NOT NULL,
  `cart_step3` tinyint(1) NOT NULL,
  `cart_step4` tinyint(1) NOT NULL,
  `cart_step5` tinyint(1) NOT NULL,
  `cart_date` date NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=203756 ;

-- --------------------------------------------------------

--
-- Table structure for table `store_cart_products`
--

CREATE TABLE IF NOT EXISTS `store_cart_products` (
  `store_cart_productid` int(10) NOT NULL,
  `store_cart_session` varchar(255) NOT NULL,
  `store_cart_quantity` int(5) NOT NULL,
  KEY `store_cart_session` (`store_cart_session`),
  KEY `store_cart_productid` (`store_cart_productid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `store_email`
--

CREATE TABLE IF NOT EXISTS `store_email` (
  `store_session` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subcat_order`
--

CREATE TABLE IF NOT EXISTS `subcat_order` (
  `grpid` varchar(25) DEFAULT NULL,
  `dorder` int(11) NOT NULL DEFAULT '0',
  KEY `grpid` (`grpid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `superpricer`
--

CREATE TABLE IF NOT EXISTS `superpricer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `min_price` double(16,2) NOT NULL,
  `max_price` double(16,2) NOT NULL,
  `map_price` double(16,2) NOT NULL,
  `sp_profile` varchar(255) NOT NULL,
  `sp_group` varchar(255) NOT NULL,
  `sp_managed` int(5) NOT NULL,
  `sp_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `superpricer_log`
--

CREATE TABLE IF NOT EXISTS `superpricer_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_pid` bigint(20) NOT NULL,
  `log_type` varchar(50) NOT NULL,
  `log_date` datetime NOT NULL,
  `log_sent` int(5) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=345 ;

-- --------------------------------------------------------

--
-- Table structure for table `synnex_cats`
--

CREATE TABLE IF NOT EXISTS `synnex_cats` (
  `grpid` int(11) DEFAULT NULL,
  `grpdescr` varchar(255) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `catdescr` varchar(255) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `subdescr` varchar(255) DEFAULT NULL,
  `synnex` int(11) NOT NULL DEFAULT '0',
  `dummy` varchar(255) DEFAULT NULL,
  KEY `synnex` (`synnex`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `targus_cache`
--

CREATE TABLE IF NOT EXISTS `targus_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoiceid` varchar(26) NOT NULL DEFAULT '',
  `ttype` int(11) NOT NULL DEFAULT '0',
  `cachedata` longblob,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=95 ;

-- --------------------------------------------------------

--
-- Table structure for table `taxonomyhistory`
--

CREATE TABLE IF NOT EXISTS `taxonomyhistory` (
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `revisiondate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_taxable`
--

CREATE TABLE IF NOT EXISTS `temp_taxable` (
  `pid` bigint(20) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_view1`
--

CREATE TABLE IF NOT EXISTS `test_view1` (
  `id` bigint(20) DEFAULT NULL,
  `manid` varchar(255) DEFAULT NULL,
  `manname` varchar(255) DEFAULT NULL,
  `manpart` varchar(255) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price_level4` double(16,2) DEFAULT NULL,
  `price_map` double(16,2) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `kit` tinyint(1) DEFAULT NULL,
  `usemap` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_address_book`
--

CREATE TABLE IF NOT EXISTS `tmp_address_book` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(64) DEFAULT NULL,
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `entry_street_address` varchar(64) NOT NULL DEFAULT '',
  `entry_suburb` varchar(32) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id_zen` (`customers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=834655 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_address_book_lstup`
--

CREATE TABLE IF NOT EXISTS `tmp_address_book_lstup` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(64) DEFAULT NULL,
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `entry_street_address` varchar(64) NOT NULL DEFAULT '',
  `entry_suburb` varchar(32) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id_zen` (`customers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=838503 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_address_book_lstup2`
--

CREATE TABLE IF NOT EXISTS `tmp_address_book_lstup2` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(64) DEFAULT NULL,
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `entry_street_address` varchar(64) NOT NULL DEFAULT '',
  `entry_suburb` varchar(32) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id_zen` (`customers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=838503 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_address_book_new2`
--

CREATE TABLE IF NOT EXISTS `tmp_address_book_new2` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(64) DEFAULT NULL,
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `entry_street_address` varchar(64) NOT NULL DEFAULT '',
  `entry_suburb` varchar(32) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id_zen` (`customers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=837468 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_categories`
--

CREATE TABLE IF NOT EXISTS `tmp_categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` varchar(64) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `categories_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`categories_id`),
  KEY `idx_parent_id_cat_id_zen` (`parent_id`,`categories_id`),
  KEY `idx_status_zen` (`categories_status`),
  KEY `idx_sort_order_zen` (`sort_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100576 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_categories_description`
--

CREATE TABLE IF NOT EXISTS `tmp_categories_description` (
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(32) NOT NULL DEFAULT '',
  `categories_description` text NOT NULL,
  PRIMARY KEY (`categories_id`,`language_id`),
  KEY `idx_categories_name_zen` (`categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_countries`
--

CREATE TABLE IF NOT EXISTS `tmp_countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(64) NOT NULL DEFAULT '',
  `countries_iso_code_2` char(2) NOT NULL DEFAULT '',
  `countries_iso_code_3` char(3) NOT NULL DEFAULT '',
  `address_format_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`countries_id`),
  KEY `idx_countries_name_zen` (`countries_name`),
  KEY `idx_address_format_id_zen` (`address_format_id`),
  KEY `idx_iso_2_zen` (`countries_iso_code_2`),
  KEY `idx_iso_3_zen` (`countries_iso_code_3`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=256 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_customers`
--

CREATE TABLE IF NOT EXISTS `tmp_customers` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_gender` char(1) NOT NULL DEFAULT '',
  `customers_firstname` varchar(32) NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) NOT NULL DEFAULT '',
  `customers_dob` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_nick` varchar(96) NOT NULL DEFAULT '',
  `customers_default_address_id` int(11) NOT NULL DEFAULT '0',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL DEFAULT '',
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_group_pricing` int(11) NOT NULL DEFAULT '0',
  `customers_email_format` varchar(4) NOT NULL DEFAULT 'TEXT',
  `customers_authorization` int(1) NOT NULL DEFAULT '0',
  `customers_referral` varchar(32) NOT NULL DEFAULT '',
  `customers_paypal_payerid` varchar(20) NOT NULL DEFAULT '',
  `customers_paypal_ec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_id`),
  KEY `idx_email_address_zen` (`customers_email_address`),
  KEY `idx_referral_zen` (`customers_referral`(10)),
  KEY `idx_grp_pricing_zen` (`customers_group_pricing`),
  KEY `idx_nick_zen` (`customers_nick`),
  KEY `idx_newsletter_zen` (`customers_newsletter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125967 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_customers_lstup`
--

CREATE TABLE IF NOT EXISTS `tmp_customers_lstup` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_gender` char(1) NOT NULL DEFAULT '',
  `customers_firstname` varchar(32) NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) NOT NULL DEFAULT '',
  `customers_dob` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_nick` varchar(96) NOT NULL DEFAULT '',
  `customers_default_address_id` int(11) NOT NULL DEFAULT '0',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL DEFAULT '',
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_group_pricing` int(11) NOT NULL DEFAULT '0',
  `customers_email_format` varchar(4) NOT NULL DEFAULT 'TEXT',
  `customers_authorization` int(1) NOT NULL DEFAULT '0',
  `customers_referral` varchar(32) NOT NULL DEFAULT '',
  `customers_paypal_payerid` varchar(20) NOT NULL DEFAULT '',
  `customers_paypal_ec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rfw_acctid` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_id`),
  KEY `idx_email_address_zen` (`customers_email_address`),
  KEY `idx_referral_zen` (`customers_referral`(10)),
  KEY `idx_grp_pricing_zen` (`customers_group_pricing`),
  KEY `idx_nick_zen` (`customers_nick`),
  KEY `idx_newsletter_zen` (`customers_newsletter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128431 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_customers_lstup2`
--

CREATE TABLE IF NOT EXISTS `tmp_customers_lstup2` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_gender` char(1) NOT NULL DEFAULT '',
  `customers_firstname` varchar(32) NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) NOT NULL DEFAULT '',
  `customers_dob` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_nick` varchar(96) NOT NULL DEFAULT '',
  `customers_default_address_id` int(11) NOT NULL DEFAULT '0',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL DEFAULT '',
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_group_pricing` int(11) NOT NULL DEFAULT '0',
  `customers_email_format` varchar(4) NOT NULL DEFAULT 'TEXT',
  `customers_authorization` int(1) NOT NULL DEFAULT '0',
  `customers_referral` varchar(32) NOT NULL DEFAULT '',
  `customers_paypal_payerid` varchar(20) NOT NULL DEFAULT '',
  `customers_paypal_ec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rfw_lstup2` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_id`),
  KEY `idx_email_address_zen` (`customers_email_address`),
  KEY `idx_referral_zen` (`customers_referral`(10)),
  KEY `idx_grp_pricing_zen` (`customers_group_pricing`),
  KEY `idx_nick_zen` (`customers_nick`),
  KEY `idx_newsletter_zen` (`customers_newsletter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128431 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_customers_new2`
--

CREATE TABLE IF NOT EXISTS `tmp_customers_new2` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_gender` char(1) NOT NULL DEFAULT '',
  `customers_firstname` varchar(32) NOT NULL DEFAULT '',
  `customers_lastname` varchar(32) NOT NULL DEFAULT '',
  `customers_dob` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_nick` varchar(96) NOT NULL DEFAULT '',
  `customers_default_address_id` int(11) NOT NULL DEFAULT '0',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_fax` varchar(32) DEFAULT NULL,
  `customers_password` varchar(40) NOT NULL DEFAULT '',
  `customers_newsletter` char(1) DEFAULT NULL,
  `customers_group_pricing` int(11) NOT NULL DEFAULT '0',
  `customers_email_format` varchar(4) NOT NULL DEFAULT 'TEXT',
  `customers_authorization` int(1) NOT NULL DEFAULT '0',
  `customers_referral` varchar(32) NOT NULL DEFAULT '',
  `customers_paypal_payerid` varchar(20) NOT NULL DEFAULT '',
  `customers_paypal_ec` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_id`),
  KEY `idx_email_address_zen` (`customers_email_address`),
  KEY `idx_referral_zen` (`customers_referral`(10)),
  KEY `idx_grp_pricing_zen` (`customers_group_pricing`),
  KEY `idx_nick_zen` (`customers_nick`),
  KEY `idx_newsletter_zen` (`customers_newsletter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127809 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders`
--

CREATE TABLE IF NOT EXISTS `tmp_orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  `rfw_orderid` bigint(20) NOT NULL,
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245182 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_lstup`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_lstup` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  `rfw_orderid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252639 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_lstup2`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_lstup2` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252639 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_merge`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_merge` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  `rfw_orderid` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252616 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_new`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_new` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  `rfw_orderid` bigint(20) NOT NULL,
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=247447 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_new2`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_new2` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(64) NOT NULL DEFAULT '',
  `customers_company` varchar(64) DEFAULT NULL,
  `customers_street_address` varchar(64) NOT NULL DEFAULT '',
  `customers_suburb` varchar(32) DEFAULT NULL,
  `customers_city` varchar(32) NOT NULL DEFAULT '',
  `customers_postcode` varchar(10) NOT NULL DEFAULT '',
  `customers_state` varchar(32) DEFAULT NULL,
  `customers_country` varchar(32) NOT NULL DEFAULT '',
  `customers_telephone` varchar(32) NOT NULL DEFAULT '',
  `customers_email_address` varchar(96) NOT NULL DEFAULT '',
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(64) NOT NULL DEFAULT '',
  `delivery_company` varchar(64) DEFAULT NULL,
  `delivery_street_address` varchar(64) NOT NULL DEFAULT '',
  `delivery_suburb` varchar(32) DEFAULT NULL,
  `delivery_city` varchar(32) NOT NULL DEFAULT '',
  `delivery_postcode` varchar(10) NOT NULL DEFAULT '',
  `delivery_state` varchar(32) DEFAULT NULL,
  `delivery_country` varchar(32) NOT NULL DEFAULT '',
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(64) NOT NULL DEFAULT '',
  `billing_company` varchar(64) DEFAULT NULL,
  `billing_street_address` varchar(64) NOT NULL DEFAULT '',
  `billing_suburb` varchar(32) DEFAULT NULL,
  `billing_city` varchar(32) NOT NULL DEFAULT '',
  `billing_postcode` varchar(10) NOT NULL DEFAULT '',
  `billing_state` varchar(32) DEFAULT NULL,
  `billing_country` varchar(32) NOT NULL DEFAULT '',
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `payment_module_code` varchar(32) NOT NULL DEFAULT '',
  `shipping_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_module_code` varchar(32) NOT NULL DEFAULT '',
  `coupon_code` varchar(32) NOT NULL DEFAULT '',
  `cc_type` varchar(20) DEFAULT NULL,
  `cc_owner` varchar(64) DEFAULT NULL,
  `cc_number` varchar(32) DEFAULT NULL,
  `cc_expires` varchar(4) DEFAULT NULL,
  `cc_cvv` blob,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `order_total` decimal(14,2) DEFAULT NULL,
  `order_tax` decimal(14,2) DEFAULT NULL,
  `paypal_ipn_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(96) NOT NULL DEFAULT '',
  `rfw_orderid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`orders_id`),
  KEY `idx_status_orders_cust_zen` (`orders_status`,`orders_id`,`customers_id`),
  KEY `idx_date_purchased_zen` (`date_purchased`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=250562 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_products`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_products` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(32) DEFAULT NULL,
  `products_name` varchar(96) DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` float NOT NULL DEFAULT '0',
  `onetime_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_priced_by_attribute` tinyint(1) NOT NULL DEFAULT '0',
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type_from` tinyint(1) NOT NULL DEFAULT '0',
  `products_prid` tinytext NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_id_prod_id_zen` (`orders_id`,`products_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=770831 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_products_lstup`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_products_lstup` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(32) DEFAULT NULL,
  `products_name` varchar(96) DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` float NOT NULL DEFAULT '0',
  `onetime_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_priced_by_attribute` tinyint(1) NOT NULL DEFAULT '0',
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type_from` tinyint(1) NOT NULL DEFAULT '0',
  `products_prid` tinytext NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_id_prod_id_zen` (`orders_id`,`products_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=798304 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_products_merge`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_products_merge` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(32) DEFAULT NULL,
  `products_name` varchar(96) DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` float NOT NULL DEFAULT '0',
  `onetime_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_priced_by_attribute` tinyint(1) NOT NULL DEFAULT '0',
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type_from` tinyint(1) NOT NULL DEFAULT '0',
  `products_prid` tinytext NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_id_prod_id_zen` (`orders_id`,`products_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=798304 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_products_new`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_products_new` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(32) DEFAULT NULL,
  `products_name` varchar(96) DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` float NOT NULL DEFAULT '0',
  `onetime_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_priced_by_attribute` tinyint(1) NOT NULL DEFAULT '0',
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type_from` tinyint(1) NOT NULL DEFAULT '0',
  `products_prid` tinytext NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_id_prod_id_zen` (`orders_id`,`products_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=778714 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_products_new2`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_products_new2` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(32) DEFAULT NULL,
  `products_name` varchar(96) DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` float NOT NULL DEFAULT '0',
  `onetime_charges` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_priced_by_attribute` tinyint(1) NOT NULL DEFAULT '0',
  `product_is_free` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type` tinyint(1) NOT NULL DEFAULT '0',
  `products_discount_type_from` tinyint(1) NOT NULL DEFAULT '0',
  `products_prid` tinytext NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_id_prod_id_zen` (`orders_id`,`products_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=791188 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_total_lstup`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_total_lstup` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '',
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_ot_orders_id_zen` (`orders_id`),
  KEY `idx_ot_class_zen` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=840745 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_total_merge`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_total_merge` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '',
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_ot_orders_id_zen` (`orders_id`),
  KEY `idx_ot_class_zen` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=840660 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_total_new`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_total_new` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '',
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_ot_orders_id_zen` (`orders_id`),
  KEY `idx_ot_class_zen` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=821640 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_orders_total_new2`
--

CREATE TABLE IF NOT EXISTS `tmp_orders_total_new2` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '',
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) NOT NULL DEFAULT '',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_ot_orders_id_zen` (`orders_id`),
  KEY `idx_ot_class_zen` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=833096 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_products`
--

CREATE TABLE IF NOT EXISTS `tmp_products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_date_added` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`products_id`),
  KEY `idx_products_date_added_zen` (`products_date_added`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1008042 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_products_description`
--

CREATE TABLE IF NOT EXISTS `tmp_products_description` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(64) NOT NULL DEFAULT '',
  `products_description` text,
  `products_url` varchar(255) DEFAULT NULL,
  `products_viewed` int(5) DEFAULT '0',
  PRIMARY KEY (`products_id`,`language_id`),
  KEY `idx_products_name_zen` (`products_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1007833 ;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_wholesalecost`
--

CREATE TABLE IF NOT EXISTS `tmp_wholesalecost` (
  `pid` bigint(20) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `wholesalecost` double(16,2) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_zones`
--

CREATE TABLE IF NOT EXISTS `tmp_zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL DEFAULT '0',
  `zone_code` varchar(32) NOT NULL DEFAULT '',
  `zone_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`zone_id`),
  KEY `idx_zone_country_id_zen` (`zone_country_id`),
  KEY `idx_zone_code_zen` (`zone_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=190 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_campaign_types`
--

CREATE TABLE IF NOT EXISTS `traffic_campaign_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reftag` varchar(50) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `cpc` double(16,2) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `tid` varchar(16) DEFAULT NULL,
  `abr` int(11) NOT NULL DEFAULT '0',
  `filter` int(11) NOT NULL DEFAULT '0',
  `price_level` int(11) NOT NULL DEFAULT '1',
  `loose` int(11) NOT NULL DEFAULT '0',
  `propername` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `reftag` (`reftag`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_paths`
--

CREATE TABLE IF NOT EXISTS `traffic_click_paths` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `followed` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`path`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63441 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_path_pages`
--

CREATE TABLE IF NOT EXISTS `traffic_click_path_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pagedescr` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pagedescr` (`pagedescr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_summary`
--

CREATE TABLE IF NOT EXISTS `traffic_click_summary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(255) NOT NULL DEFAULT '',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  `coa` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=309499 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_summary_byip`
--

CREATE TABLE IF NOT EXISTS `traffic_click_summary_byip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(16) NOT NULL DEFAULT '',
  `ip_addr` varchar(20) DEFAULT NULL,
  `network` varchar(20) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`pid`,`ip_addr`),
  KEY `tstamp_3` (`tstamp`,`clicktype`,`clickid`,`pid`,`network`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1277082 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_summary_bysku`
--

CREATE TABLE IF NOT EXISTS `traffic_click_summary_bysku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(255) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` text,
  `clicks` int(11) DEFAULT NULL,
  `sales` int(11) DEFAULT NULL,
  `salesvol` double(16,2) DEFAULT NULL,
  `coa` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `clicktype` (`clicktype`,`clickid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1613953 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_click_summary_extended`
--

CREATE TABLE IF NOT EXISTS `traffic_click_summary_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(16) NOT NULL DEFAULT '',
  `pid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  `coa` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=315561 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_exit_pages`
--

CREATE TABLE IF NOT EXISTS `traffic_exit_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`page`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14379500 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_product_performance`
--

CREATE TABLE IF NOT EXISTS `traffic_product_performance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '-1',
  `views` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `sales` int(11) DEFAULT '0',
  `salesvol` double(16,2) DEFAULT '0.00',
  `views_uk` int(11) DEFAULT '0',
  `clicks_uk` int(11) DEFAULT '0',
  `sales_uk` int(11) DEFAULT '0',
  `salesvol_uk` double(16,2) DEFAULT '0.00',
  `views_pe` int(11) DEFAULT '0',
  `clicks_pe` int(11) DEFAULT '0',
  `sales_pe` int(11) DEFAULT '0',
  `salesvol_pe` double(16,2) DEFAULT '0.00',
  `views_ca` int(11) DEFAULT '0',
  `clicks_ca` int(11) DEFAULT '0',
  `sales_ca` int(11) DEFAULT '0',
  `salesvol_ca` double(16,2) DEFAULT '0.00',
  `views_se` int(11) DEFAULT '0',
  `clicks_se` int(11) DEFAULT '0',
  `sales_se` int(11) DEFAULT '0',
  `salesvol_se` double(16,2) DEFAULT '0.00',
  `views_dr` int(11) DEFAULT '0',
  `clicks_dr` int(11) DEFAULT '0',
  `sales_dr` int(11) DEFAULT '0',
  `salesvol_dr` double(16,2) DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `pid` (`pid`),
  KEY `tstamp_2` (`tstamp`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1250079 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_referring_urls`
--

CREATE TABLE IF NOT EXISTS `traffic_referring_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`domain`,`path`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1348269 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_referring_urls_extended`
--
-- in use(#144 - Table './rawfoodworld/traffic_referring_urls_extended' is marked as crashed and last (automatic?) repair failed)

-- --------------------------------------------------------

--
-- Table structure for table `traffic_sales_summary`
--

CREATE TABLE IF NOT EXISTS `traffic_sales_summary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(255) DEFAULT NULL,
  `invoiceid` varchar(50) DEFAULT NULL,
  `ip_addr` varchar(26) DEFAULT NULL,
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `clicktype` (`clicktype`,`clickid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143696 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_search_terms_external`
--

CREATE TABLE IF NOT EXISTS `traffic_search_terms_external` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(16) NOT NULL DEFAULT '',
  `clicks` int(11) DEFAULT NULL,
  `phrase` varchar(128) DEFAULT NULL,
  `pviews` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`phrase`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=206902 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_search_terms_external_extended`
--

CREATE TABLE IF NOT EXISTS `traffic_search_terms_external_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `clickid` varchar(16) NOT NULL DEFAULT '',
  `phrase` varchar(128) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`clicktype`,`clickid`,`phrase`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=306510 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_search_terms_internal`
--

CREATE TABLE IF NOT EXISTS `traffic_search_terms_internal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `phrase` varchar(128) DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `pviews` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`phrase`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3897 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_search_terms_internal_extended`
--

CREATE TABLE IF NOT EXISTS `traffic_search_terms_internal_extended` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `phrase` varchar(128) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `tstamp_2` (`tstamp`,`phrase`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=340137 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_spiders`
--

CREATE TABLE IF NOT EXISTS `traffic_spiders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_addr` varchar(20) DEFAULT NULL,
  `tstamp` bigint(20) NOT NULL DEFAULT '0',
  `message` varchar(255) DEFAULT NULL,
  `repeated` int(11) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `ip_addr` (`ip_addr`),
  KEY `tstamp` (`tstamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_spider_notes`
--

CREATE TABLE IF NOT EXISTS `traffic_spider_notes` (
  `ip_addr` varchar(20) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  KEY `ip_addr` (`ip_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_summary_hourly_hits`
--

CREATE TABLE IF NOT EXISTS `traffic_summary_hourly_hits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `rtype` int(11) NOT NULL DEFAULT '0',
  `hour_00` double NOT NULL DEFAULT '0',
  `hour_01` double NOT NULL DEFAULT '0',
  `hour_02` double NOT NULL DEFAULT '0',
  `hour_03` double NOT NULL DEFAULT '0',
  `hour_04` double NOT NULL DEFAULT '0',
  `hour_05` double NOT NULL DEFAULT '0',
  `hour_06` double NOT NULL DEFAULT '0',
  `hour_07` double NOT NULL DEFAULT '0',
  `hour_08` double NOT NULL DEFAULT '0',
  `hour_09` double NOT NULL DEFAULT '0',
  `hour_10` double NOT NULL DEFAULT '0',
  `hour_11` double NOT NULL DEFAULT '0',
  `hour_12` double NOT NULL DEFAULT '0',
  `hour_13` double NOT NULL DEFAULT '0',
  `hour_14` double NOT NULL DEFAULT '0',
  `hour_15` double NOT NULL DEFAULT '0',
  `hour_16` double NOT NULL DEFAULT '0',
  `hour_17` double NOT NULL DEFAULT '0',
  `hour_18` double NOT NULL DEFAULT '0',
  `hour_19` double NOT NULL DEFAULT '0',
  `hour_20` double NOT NULL DEFAULT '0',
  `hour_21` double NOT NULL DEFAULT '0',
  `hour_22` double NOT NULL DEFAULT '0',
  `hour_23` double NOT NULL DEFAULT '0',
  `hour_24` double NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`rtype`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2848 ;

-- --------------------------------------------------------

--
-- Table structure for table `traffic_summary_location`
--

CREATE TABLE IF NOT EXISTS `traffic_summary_location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` bigint(20) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `ccode` varchar(4) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `sales` int(11) NOT NULL DEFAULT '0',
  `salesvol` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`,`country`),
  KEY `tstamp_2` (`tstamp`,`country`,`city`),
  KEY `tstamp_3` (`tstamp`,`country`,`city`,`state`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=933110 ;

-- --------------------------------------------------------

--
-- Table structure for table `unitnames`
--

CREATE TABLE IF NOT EXISTS `unitnames` (
  `unitid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `unitnames_unitID` (`unitid`),
  KEY `unitnames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `unitid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) NOT NULL DEFAULT '',
  `baseunitid` int(11) NOT NULL DEFAULT '0',
  `multiple` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`unitid`),
  KEY `units_baseUnitID` (`baseunitid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `update_pool`
--

CREATE TABLE IF NOT EXISTS `update_pool` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `updateid` varchar(50) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16165593 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `un` varchar(32) NOT NULL DEFAULT '',
  `pw_md5` varchar(32) NOT NULL DEFAULT '',
  `displayname` varchar(128) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `per_af` double(16,2) DEFAULT '0.01',
  `per_lm` double(16,2) DEFAULT '10.00',
  `per_cd` double(16,2) DEFAULT '0.00',
  `per_ecpc` double(16,2) DEFAULT '0.00',
  `per_ec` double(16,2) DEFAULT '100.00',
  `per_mr` int(11) NOT NULL DEFAULT '99',
  `per_us` int(11) NOT NULL DEFAULT '0',
  `per_sm` double(16,2) DEFAULT '10.00',
  `type` enum('MANAGER','SALESMAN','CUSTSERV','WAREHOUSE') DEFAULT NULL,
  `last_ip` varchar(16) NOT NULL DEFAULT '',
  `extension` varchar(16) NOT NULL DEFAULT '0',
  `pickerid` varchar(16) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `un` (`un`,`pw_md5`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=162 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_ip_restriction`
--

CREATE TABLE IF NOT EXISTS `user_ip_restriction` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `ipaddr` varchar(16) DEFAULT NULL,
  KEY `userid` (`userid`),
  KEY `userid_2` (`userid`,`ipaddr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE IF NOT EXISTS `user_permissions` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `perm_category_manager` int(11) NOT NULL DEFAULT '0',
  `perm_edit_menu` int(11) NOT NULL DEFAULT '0',
  `perm_email_templates` int(11) NOT NULL DEFAULT '0',
  `perm_ingram_markup` int(11) NOT NULL DEFAULT '0',
  `perm_order_flow` int(11) NOT NULL DEFAULT '0',
  `perm_shipping` int(11) NOT NULL DEFAULT '0',
  `perm_site_config` int(11) NOT NULL DEFAULT '0',
  `perm_featured_products` int(11) NOT NULL DEFAULT '0',
  `perm_pegrid` int(11) NOT NULL DEFAULT '0',
  `perm_categorize` int(11) NOT NULL DEFAULT '0',
  `perm_visible` int(11) NOT NULL DEFAULT '0',
  `perm_datafeed` int(11) NOT NULL DEFAULT '0',
  `perm_scolumns` int(11) NOT NULL DEFAULT '0',
  `perm_createskus` int(11) NOT NULL DEFAULT '0',
  `perm_accbycat` int(11) NOT NULL DEFAULT '0',
  `perm_scost` int(11) NOT NULL DEFAULT '0',
  `perm_editsku` int(11) NOT NULL DEFAULT '0',
  `perm_datafeed_manager` int(11) NOT NULL DEFAULT '0',
  `perm_email_marketing` int(11) NOT NULL DEFAULT '0',
  `perm_traffic_reports` int(11) NOT NULL DEFAULT '0',
  `perm_po_search` int(11) NOT NULL DEFAULT '0',
  `perm_vendor_search` int(11) NOT NULL DEFAULT '0',
  `perm_po_edit` int(11) NOT NULL DEFAULT '0',
  `perm_po_create` int(11) NOT NULL DEFAULT '0',
  `perm_vendor_edit` int(11) NOT NULL DEFAULT '0',
  `perm_vendor_create` int(11) NOT NULL DEFAULT '0',
  `perm_user_manager` int(11) NOT NULL DEFAULT '0',
  `perm_online_help` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_price` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_descr` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_hspecs` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_image` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_specs` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_access` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_sourcing` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_shipping` int(11) NOT NULL DEFAULT '0',
  `perm_skutabs_perf` int(11) NOT NULL DEFAULT '0',
  `perm_manual_billing` int(11) NOT NULL DEFAULT '0',
  `perm_add_cards` int(11) NOT NULL DEFAULT '0',
  `perm_decrypt_cards` int(11) NOT NULL DEFAULT '0',
  `perm_marketing_campaigns` int(11) NOT NULL DEFAULT '0',
  `perm_flow_move_anytime` int(11) NOT NULL DEFAULT '0',
  `perm_flow_move_onlymine` int(11) NOT NULL DEFAULT '0',
  `perm_flow_move_freemoveq` int(11) NOT NULL DEFAULT '0',
  `perm_manual_void` int(11) NOT NULL DEFAULT '0',
  `perm_manual_charge` int(11) NOT NULL DEFAULT '0',
  `perm_view_package_cost` int(11) NOT NULL DEFAULT '0',
  `perm_edit_ebay` int(11) NOT NULL DEFAULT '0',
  `perm_order_editprice` int(11) NOT NULL DEFAULT '0',
  `perm_iportal` int(11) NOT NULL DEFAULT '0',
  `perm_scanqty` int(11) NOT NULL DEFAULT '0',
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_search_columns`
--

CREATE TABLE IF NOT EXISTS `user_search_columns` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6032 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_search_filters`
--

CREATE TABLE IF NOT EXISTS `user_search_filters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_search_links`
--

CREATE TABLE IF NOT EXISTS `user_search_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_search_order_filters`
--

CREATE TABLE IF NOT EXISTS `user_search_order_filters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_search_tabs`
--

CREATE TABLE IF NOT EXISTS `user_search_tabs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) DEFAULT NULL,
  `colname` varchar(32) DEFAULT NULL,
  `dorder` int(11) DEFAULT NULL,
  `coltitle` varchar(128) NOT NULL DEFAULT 'N/A',
  `permission` varchar(50) NOT NULL,
  `db` varchar(50) NOT NULL,
  `tab_file` varchar(100) NOT NULL,
  KEY `id` (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_widget_permissions`
--

CREATE TABLE IF NOT EXISTS `user_widget_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `wid` bigint(20) NOT NULL DEFAULT '0',
  `userid` bigint(20) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `wid` (`wid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=620179 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `vendorid` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorname` varchar(128) DEFAULT NULL,
  `addrline1` varchar(50) DEFAULT NULL,
  `addrline2` varchar(50) DEFAULT NULL,
  `addrcity` varchar(50) DEFAULT NULL,
  `addrstate` char(2) DEFAULT NULL,
  `addrzip` varchar(10) DEFAULT NULL,
  `addrcountry` char(2) DEFAULT NULL,
  `phone1` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `primary_contact` varchar(255) DEFAULT NULL,
  `primary_phone1` varchar(50) DEFAULT NULL,
  `primary_phone2` varchar(50) DEFAULT NULL,
  `primary_email` varchar(255) DEFAULT NULL,
  `secondary_contact` varchar(255) DEFAULT NULL,
  `secondary_phone1` varchar(50) DEFAULT NULL,
  `secondary_phone2` varchar(50) DEFAULT NULL,
  `secondary_email` varchar(255) DEFAULT NULL,
  `account_type` int(11) NOT NULL DEFAULT '0',
  `account_terms` int(11) NOT NULL DEFAULT '0',
  `current_balance` double(16,2) NOT NULL DEFAULT '0.00',
  `credit_limit` double(16,2) NOT NULL DEFAULT '0.00',
  `acctno` varchar(128) DEFAULT NULL,
  `irs1099flag` int(11) NOT NULL DEFAULT '0',
  `edixml` int(11) NOT NULL DEFAULT '0',
  `nacha` int(11) NOT NULL DEFAULT '0',
  `distid` varchar(10) DEFAULT NULL,
  `managevisible` int(11) NOT NULL DEFAULT '0',
  `stockthreshold` int(11) NOT NULL DEFAULT '0',
  `etilizename` varchar(128) NOT NULL DEFAULT '',
  `dropshipvendor` int(11) NOT NULL DEFAULT '0',
  KEY `vendorid` (`vendorid`),
  KEY `acctno` (`acctno`),
  KEY `primary_contact` (`primary_contact`),
  KEY `current_balance` (`current_balance`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=462 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors_taxstate`
--

CREATE TABLE IF NOT EXISTS `vendors_taxstate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorid` bigint(20) NOT NULL,
  `states` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_asi_helper`
--

CREATE TABLE IF NOT EXISTS `vendor_asi_helper` (
  `id` varchar(20) DEFAULT NULL,
  `data` varchar(9999) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_cables2go_helper`
--

CREATE TABLE IF NOT EXISTS `vendor_cables2go_helper` (
  `pid` varchar(20) DEFAULT NULL,
  `csku` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_categories`
--

CREATE TABLE IF NOT EXISTS `vendor_categories` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(50) NOT NULL DEFAULT '',
  `code2` varchar(15) NOT NULL,
  `grp` varchar(50) NOT NULL,
  KEY `catid` (`catid`),
  KEY `vid` (`vid`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29863 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_categories_RENAMED`
--

CREATE TABLE IF NOT EXISTS `vendor_categories_RENAMED` (
  `catid` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `catname` varchar(255) DEFAULT NULL,
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(50) NOT NULL DEFAULT '',
  `code2` varchar(15) NOT NULL DEFAULT '',
  KEY `catid` (`catid`),
  KEY `vid` (`vid`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=651 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category_map`
--

CREATE TABLE IF NOT EXISTS `vendor_category_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `code` varchar(50) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `catid` (`catid`),
  KEY `vid` (`vid`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4655362 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category_map_RENAMED`
--

CREATE TABLE IF NOT EXISTS `vendor_category_map_RENAMED` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `code` varchar(50) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `pid` (`pid`),
  KEY `catid` (`catid`),
  KEY `vid` (`vid`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=819362 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_cat_map`
--

CREATE TABLE IF NOT EXISTS `vendor_cat_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorid` bigint(20) DEFAULT NULL,
  `vcatid` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vendorid` (`vendorid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=660 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_CRW_helper`
--

CREATE TABLE IF NOT EXISTS `vendor_CRW_helper` (
  `pid` varchar(20) DEFAULT NULL,
  `csku` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_doba`
--

CREATE TABLE IF NOT EXISTS `vendor_doba` (
  `helperid` int(11) NOT NULL AUTO_INCREMENT,
  `doba_pid` varchar(100) DEFAULT NULL,
  `product_sku` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `ship_fee` varchar(100) DEFAULT NULL,
  `drop_ship_fee` varchar(100) DEFAULT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `itemid` varchar(50) NOT NULL,
  PRIMARY KEY (`helperid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_doba_collection`
--

CREATE TABLE IF NOT EXISTS `vendor_doba_collection` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `doba_pid` varchar(100) DEFAULT NULL,
  `product_sku` longblob,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dataid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=438745 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_doba_count`
--

CREATE TABLE IF NOT EXISTS `vendor_doba_count` (
  `action` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_doba_tax`
--

CREATE TABLE IF NOT EXISTS `vendor_doba_tax` (
  `taxid` int(11) NOT NULL AUTO_INCREMENT,
  `doba_pid` varchar(100) DEFAULT NULL,
  `product_sku` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `ship_fee` varchar(100) DEFAULT NULL,
  `drop_ship_fee` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `taxstring` varchar(100) DEFAULT NULL,
  `itemid` varchar(50) NOT NULL,
  PRIMARY KEY (`taxid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_features`
--

CREATE TABLE IF NOT EXISTS `vendor_features` (
  `pid` bigint(20) DEFAULT NULL,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `feature` varchar(255) NOT NULL,
  KEY `pid` (`pid`),
  KEY `vid` (`vid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_ingram_debug`
--

CREATE TABLE IF NOT EXISTS `vendor_ingram_debug` (
  `sku` varchar(55) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` longblob,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_invoice`
--

CREATE TABLE IF NOT EXISTS `vendor_invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vinvoice` varchar(50) NOT NULL DEFAULT '0',
  `vendorid` bigint(20) DEFAULT NULL,
  `poid` bigint(20) DEFAULT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vinvoice` (`vinvoice`),
  KEY `vendorid` (`vendorid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6332 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_invoice_lines`
--

CREATE TABLE IF NOT EXISTS `vendor_invoice_lines` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vid` (`vid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21884 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_manufacturers`
--

CREATE TABLE IF NOT EXISTS `vendor_manufacturers` (
  `manid` bigint(20) NOT NULL AUTO_INCREMENT,
  `manname` varchar(50) NOT NULL DEFAULT '',
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `code` varchar(50) NOT NULL DEFAULT '',
  KEY `manid` (`manid`),
  KEY `manname` (`manname`),
  KEY `vid` (`vid`),
  KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20004 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_man_map`
--

CREATE TABLE IF NOT EXISTS `vendor_man_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorid` bigint(20) DEFAULT NULL,
  `vmanid` bigint(20) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vendorid` (`vendorid`,`vmanid`),
  KEY `vendorid_2` (`vendorid`,`manid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1908 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_markup_rules`
--

CREATE TABLE IF NOT EXISTS `vendor_markup_rules` (
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  KEY `vendorid` (`vendorid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_merge_profiles`
--

CREATE TABLE IF NOT EXISTS `vendor_merge_profiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_merge_profiles_extended`
--

CREATE TABLE IF NOT EXISTS `vendor_merge_profiles_extended` (
  `pfid` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  KEY `pfid` (`pfid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_metatag_data`
--

CREATE TABLE IF NOT EXISTS `vendor_metatag_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `subtype` varchar(26) DEFAULT 'metatag',
  `typeid` bigint(20) NOT NULL DEFAULT '0',
  `data` longblob,
  `divid` bigint(20) NOT NULL DEFAULT '0',
  `vid` varchar(4) DEFAULT NULL,
  `pid` varchar(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `type` (`type`,`subtype`,`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2763 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_notes`
--

CREATE TABLE IF NOT EXISTS `vendor_notes` (
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  `notes` longblob,
  `tstamp` datetime DEFAULT NULL,
  KEY `vendorid` (`vendorid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_pos`
--

CREATE TABLE IF NOT EXISTS `vendor_pos` (
  `poid` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorid` bigint(20) NOT NULL DEFAULT '0',
  `invoiceid` varchar(26) DEFAULT NULL,
  `poref` varchar(128) DEFAULT NULL,
  `vendorref` varchar(128) DEFAULT NULL,
  `accountname` varchar(50) DEFAULT NULL,
  `addrline1` varchar(50) DEFAULT NULL,
  `addrline2` varchar(50) DEFAULT NULL,
  `addrcity` varchar(50) DEFAULT NULL,
  `addrstate` char(2) DEFAULT NULL,
  `addrzip` varchar(10) DEFAULT NULL,
  `addrcountry` varchar(50) DEFAULT NULL,
  `saddrname` varchar(50) DEFAULT NULL,
  `saddrline1` varchar(50) DEFAULT NULL,
  `saddrline2` varchar(50) DEFAULT NULL,
  `saddrcity` varchar(50) DEFAULT NULL,
  `saddrstate` char(2) DEFAULT NULL,
  `saddrzip` varchar(10) DEFAULT NULL,
  `saddrcountry` varchar(50) DEFAULT NULL,
  `sub_stamp` bigint(20) DEFAULT NULL,
  `shp_stamp` bigint(20) DEFAULT NULL,
  `rec_stamp` bigint(20) DEFAULT NULL,
  `eta` datetime DEFAULT NULL,
  `status` bigint(20) NOT NULL DEFAULT '0',
  `status_rec` int(11) DEFAULT NULL,
  `status_inv` int(11) DEFAULT NULL,
  `edixml` int(11) DEFAULT NULL,
  `tax` double(16,2) NOT NULL DEFAULT '0.00',
  `total` double(16,2) NOT NULL DEFAULT '0.00',
  `repid` int(11) NOT NULL DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `sphone` varchar(50) DEFAULT NULL,
  `vendor_status` varchar(255) DEFAULT NULL,
  `edixml_status` int(11) NOT NULL DEFAULT '0',
  `shipping` double(16,2) NOT NULL DEFAULT '0.00',
  `divid` int(11) NOT NULL DEFAULT '0',
  `paid` int(5) NOT NULL DEFAULT '0',
  `paid_amount` double(16,2) NOT NULL,
  `mgr` int(5) NOT NULL DEFAULT '0',
  `assigned` bigint(20) NOT NULL,
  `fbaid` varchar(50) NOT NULL,
  `pend_cancel` tinyint(1) NOT NULL DEFAULT '0',
  KEY `poid` (`poid`),
  KEY `vendorid` (`vendorid`),
  KEY `poref` (`poref`),
  KEY `vendorref` (`vendorref`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12341 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_pos_extended`
--

CREATE TABLE IF NOT EXISTS `vendor_pos_extended` (
  `poid` bigint(20) NOT NULL,
  `invoiceid` varchar(26) DEFAULT NULL,
  KEY `poid` (`poid`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_po_files`
--

CREATE TABLE IF NOT EXISTS `vendor_po_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `poid` bigint(20) NOT NULL,
  `file` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_po_lines`
--

CREATE TABLE IF NOT EXISTS `vendor_po_lines` (
  `lineid` bigint(20) NOT NULL AUTO_INCREMENT,
  `poid` bigint(20) DEFAULT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `sku` varchar(25) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `price` double(16,2) NOT NULL DEFAULT '0.00',
  `qty_ordered` int(11) NOT NULL DEFAULT '0',
  `qty_shipped` int(11) NOT NULL DEFAULT '0',
  `weight` double(16,2) NOT NULL DEFAULT '0.00',
  `baseprice` double(16,2) NOT NULL DEFAULT '0.00',
  KEY `lineid` (`lineid`),
  KEY `poid` (`poid`),
  KEY `pid` (`pid`),
  KEY `sku` (`sku`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55826 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_po_notes`
--

CREATE TABLE IF NOT EXISTS `vendor_po_notes` (
  `poid` bigint(20) NOT NULL DEFAULT '0',
  `notes` longblob,
  KEY `poid` (`poid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_po_shipping`
--

CREATE TABLE IF NOT EXISTS `vendor_po_shipping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `street1` varchar(128) DEFAULT NULL,
  `street2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(16) DEFAULT NULL,
  `country` varchar(4) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_pricing`
--

CREATE TABLE IF NOT EXISTS `vendor_pricing` (
  `pid` bigint(20) NOT NULL,
  `instock` bigint(20) DEFAULT NULL,
  `cost` double(16,2) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `map` double(16,2) DEFAULT NULL,
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_accessories`
--

CREATE TABLE IF NOT EXISTS `vendor_product_accessories` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `accid` bigint(20) NOT NULL DEFAULT '0',
  `dorder` int(11) NOT NULL DEFAULT '0',
  `vid` int(11) NOT NULL DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_docs`
--

CREATE TABLE IF NOT EXISTS `vendor_product_docs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39124 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_html`
--

CREATE TABLE IF NOT EXISTS `vendor_product_html` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `data` text,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_images`
--

CREATE TABLE IF NOT EXISTS `vendor_product_images` (
  `imageid` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL DEFAULT '0',
  `idata` longblob,
  `itype` int(11) DEFAULT NULL,
  `thumb` int(11) NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `imageid` (`imageid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2311988 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_list`
--

CREATE TABLE IF NOT EXISTS `vendor_product_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `manid` bigint(20) NOT NULL DEFAULT '-1',
  `manpart` varchar(50) NOT NULL DEFAULT '',
  `descr` varchar(255) NOT NULL DEFAULT 'NO DESCRIPTION',
  `added_dte` bigint(20) NOT NULL DEFAULT '0',
  `status` varchar(4) DEFAULT NULL,
  `returnable` int(11) NOT NULL DEFAULT '0',
  `upccode` varchar(32) DEFAULT NULL,
  `platform` varchar(7) NOT NULL DEFAULT '',
  `shipweight` double(16,2) NOT NULL DEFAULT '0.00',
  `oem` int(11) NOT NULL DEFAULT '0',
  `refurb` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(50) NOT NULL DEFAULT '',
  `subsku` varchar(50) NOT NULL DEFAULT '',
  `etilieid` varchar(20) DEFAULT NULL,
  `etilizeid` varchar(20) DEFAULT NULL,
  `item_id` varchar(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vid` (`vid`),
  KEY `manid` (`manid`,`manpart`),
  KEY `descr` (`descr`),
  KEY `added_dte` (`added_dte`),
  KEY `upccode` (`upccode`),
  KEY `manpart` (`manpart`),
  KEY `status_2` (`status`),
  KEY `vid_2` (`vid`,`sku`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=526989 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_list_temp`
--

CREATE TABLE IF NOT EXISTS `vendor_product_list_temp` (
  `id` bigint(20) NOT NULL,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `manid` bigint(20) NOT NULL DEFAULT '-1',
  `manpart` varchar(50) NOT NULL DEFAULT '',
  `descr` varchar(255) NOT NULL DEFAULT 'NO DESCRIPTION',
  `added_dte` bigint(20) NOT NULL DEFAULT '0',
  `status` varchar(4) DEFAULT NULL,
  `returnable` int(11) NOT NULL DEFAULT '0',
  `upccode` varchar(32) DEFAULT NULL,
  `platform` varchar(7) NOT NULL DEFAULT '',
  `shipweight` double(16,2) NOT NULL DEFAULT '0.00',
  `oem` int(11) NOT NULL DEFAULT '0',
  `refurb` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(50) NOT NULL DEFAULT '',
  `subsku` varchar(50) NOT NULL DEFAULT '',
  `etilizeid` varchar(20) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vid` (`vid`),
  KEY `manid` (`manid`,`manpart`),
  KEY `descr` (`descr`),
  KEY `added_dte` (`added_dte`),
  KEY `upccode` (`upccode`),
  KEY `manpart` (`manpart`),
  KEY `status_2` (`status`),
  KEY `vid_2` (`vid`,`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_manufacturers`
--

CREATE TABLE IF NOT EXISTS `vendor_product_manufacturers` (
  `manid` bigint(20) NOT NULL AUTO_INCREMENT,
  `manname` varchar(50) NOT NULL DEFAULT '',
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  KEY `manid` (`manid`),
  KEY `manname` (`manname`),
  KEY `vid` (`vid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_specs`
--

CREATE TABLE IF NOT EXISTS `vendor_product_specs` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `section` varchar(255) DEFAULT NULL,
  `dkey` varchar(255) DEFAULT NULL,
  `dval` varchar(255) DEFAULT NULL,
  `specsid` bigint(20) NOT NULL AUTO_INCREMENT,
  KEY `id` (`id`),
  KEY `vid` (`vid`),
  KEY `specsid` (`specsid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33391 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_receipt`
--

CREATE TABLE IF NOT EXISTS `vendor_receipt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendorid` bigint(20) DEFAULT NULL,
  `poid` bigint(20) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `vinvoice` varchar(50) DEFAULT NULL,
  `invoiceid` varchar(26) NOT NULL DEFAULT '',
  KEY `id` (`id`),
  KEY `vendorid` (`vendorid`),
  KEY `poid` (`poid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16107 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_receipt_lines`
--

CREATE TABLE IF NOT EXISTS `vendor_receipt_lines` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `cost` double(16,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `manid` bigint(20) DEFAULT NULL,
  `manname` varchar(50) DEFAULT NULL,
  `manpart` varchar(50) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vid` (`vid`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35070 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_sales_descr`
--

CREATE TABLE IF NOT EXISTS `vendor_sales_descr` (
  `pid` bigint(20) DEFAULT NULL,
  `vid` bigint(20) NOT NULL DEFAULT '-1',
  `data` longblob,
  KEY `pid` (`pid`),
  KEY `vid` (`vid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_stampede_helper`
--

CREATE TABLE IF NOT EXISTS `vendor_stampede_helper` (
  `id` int(11) DEFAULT NULL,
  `manpart` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_stampede_helper_descr`
--

CREATE TABLE IF NOT EXISTS `vendor_stampede_helper_descr` (
  `id` int(11) DEFAULT NULL,
  `manpart` varchar(100) DEFAULT NULL,
  `descr` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_stampede_helper_image`
--

CREATE TABLE IF NOT EXISTS `vendor_stampede_helper_image` (
  `id` int(11) DEFAULT NULL,
  `manpart` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_status`
--

CREATE TABLE IF NOT EXISTS `vendor_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendorname` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_sync`
--

CREATE TABLE IF NOT EXISTS `vendor_sync` (
  `vendorname` varchar(50) DEFAULT NULL,
  `filedate` varchar(50) DEFAULT NULL,
  `lastsync` varchar(50) DEFAULT NULL,
  `updated` varchar(50) DEFAULT NULL,
  `created` varchar(50) DEFAULT NULL,
  KEY `vendorname` (`vendorname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_wynit_helper`
--

CREATE TABLE IF NOT EXISTS `vendor_wynit_helper` (
  `sku` varchar(50) NOT NULL,
  `pid` varchar(50) NOT NULL,
  `id` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verification_log`
--

CREATE TABLE IF NOT EXISTS `verification_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(26) DEFAULT NULL,
  `ordertext` text,
  `sendtext` text,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `verification_results`
--

CREATE TABLE IF NOT EXISTS `verification_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `orderid` varchar(26) NOT NULL DEFAULT '',
  `pares` char(2) DEFAULT NULL,
  `cavv` varchar(32) DEFAULT NULL,
  `eci` varchar(5) DEFAULT NULL,
  `xid` varchar(64) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `tstamp` (`tstamp`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `verification_results_pp`
--

CREATE TABLE IF NOT EXISTS `verification_results_pp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(25) DEFAULT NULL,
  `pmtstatus` varchar(25) DEFAULT NULL,
  `txid` varchar(50) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37148 ;

-- --------------------------------------------------------

--
-- Table structure for table `wdg_available_widgets`
--

CREATE TABLE IF NOT EXISTS `wdg_available_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_id` int(11) NOT NULL,
  `sort_no` int(11) NOT NULL,
  `collapsed` tinyint(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wdg_panels_column`
--

CREATE TABLE IF NOT EXISTS `wdg_panels_column` (
  `column_id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`column_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `wdg_widgets`
--

CREATE TABLE IF NOT EXISTS `wdg_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_id` int(11) NOT NULL,
  `sort_no` int(11) NOT NULL,
  `collapsed` tinyint(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `weborderreceived_email`
--

CREATE TABLE IF NOT EXISTS `weborderreceived_email` (
  `invoiceid` varchar(26) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `widget_list`
--

CREATE TABLE IF NOT EXISTS `widget_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `descr` text,
  `url` varchar(255) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `scroll` int(11) NOT NULL DEFAULT '0',
  `tstamp` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `yancey_broke`
--

CREATE TABLE IF NOT EXISTS `yancey_broke` (
  `id` bigint(20) NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zipcodes`
--

CREATE TABLE IF NOT EXISTS `zipcodes` (
  `zipcode` bigint(20) DEFAULT NULL,
  `latitude` double(16,2) DEFAULT NULL,
  `longitude` double(16,2) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(128) DEFAULT NULL,
  `abbr` varchar(4) DEFAULT NULL,
  KEY `zipcode` (`zipcode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zipcode_rates`
--

CREATE TABLE IF NOT EXISTS `zipcode_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zone` varchar(50) NOT NULL,
  `box_a` double(16,2) NOT NULL,
  `box_b` double(16,2) NOT NULL,
  `box_c` double(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `zipcode_zone`
--

CREATE TABLE IF NOT EXISTS `zipcode_zone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `zipcode` varchar(20) NOT NULL,
  `zone` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=931 ;

-- --------------------------------------------------------

--
-- Table structure for table `zip_code`
--

CREATE TABLE IF NOT EXISTS `zip_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_prefix` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `area_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time_zone` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zip_code` (`zip_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42625 ;
