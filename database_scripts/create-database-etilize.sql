-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 07:19 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `etilize`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributenames`
--

CREATE TABLE IF NOT EXISTS `attributenames` (
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(110) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `attributenames_attributeID` (`attributeid`),
  KEY `attributenames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `parentcategoryid` int(11) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ordernumber` int(11) NOT NULL DEFAULT '0',
  `catlevel` tinyint(4) NOT NULL DEFAULT '0',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `catery_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorydisplayattributes`
--

CREATE TABLE IF NOT EXISTS `categorydisplayattributes` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `templatetype` int(11) NOT NULL DEFAULT '0',
  `defaultdisplayorder` int(11) NOT NULL DEFAULT '1',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `caterydisplayattributes_hID` (`headerid`),
  KEY `caterydisplayattributes_cID` (`categoryid`),
  KEY `caterydisplayattributes_aID` (`attributeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categoryheader`
--

CREATE TABLE IF NOT EXISTS `categoryheader` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `templatetype` int(11) NOT NULL DEFAULT '0',
  `defaultdisplayorder` int(11) NOT NULL DEFAULT '1',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `cateryheader_headerID` (`headerid`),
  KEY `cateryheader_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorynames`
--

CREATE TABLE IF NOT EXISTS `categorynames` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `caterynames_categoryID` (`categoryid`),
  KEY `caterynames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorysearchattributes`
--

CREATE TABLE IF NOT EXISTS `categorysearchattributes` (
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ispreferred` tinyint(1) NOT NULL DEFAULT '0',
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `caterysearchattributes_aID` (`attributeid`),
  KEY `caterysearchattributes_cID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `headernames`
--

CREATE TABLE IF NOT EXISTS `headernames` (
  `headerid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `headernames_headerID` (`headerid`),
  KEY `headernames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locales`
--

CREATE TABLE IF NOT EXISTS `locales` (
  `localeid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `languagecode` varchar(5) NOT NULL DEFAULT '',
  `countrycode` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`localeid`),
  KEY `locales_languageCode` (`languagecode`),
  KEY `locales_countryCode` (`countrycode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturerid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) NOT NULL DEFAULT '',
  `address1` varchar(60) DEFAULT NULL,
  `address2` varchar(60) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`manufacturerid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `manufacturerid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `mfgpartno` varchar(70) NOT NULL DEFAULT '',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `isaccessory` tinyint(1) NOT NULL DEFAULT '0',
  `equivalency` double NOT NULL DEFAULT '0',
  `creationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifieddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastupdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`productid`),
  KEY `product_isAccessory` (`isaccessory`),
  KEY `product_manufacturerID` (`manufacturerid`),
  KEY `product_categoryID` (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productaccessories`
--

CREATE TABLE IF NOT EXISTS `productaccessories` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `accessoryproductid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `ispreferred` tinyint(1) NOT NULL DEFAULT '0',
  `isoption` tinyint(1) NOT NULL DEFAULT '0',
  `note` mediumtext NOT NULL,
  KEY `productaccessories_productID` (`productid`),
  KEY `productaccessories_isPreferred` (`ispreferred`),
  KEY `productacesories_acesoryPID` (`accessoryproductid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productattribute`
--

CREATE TABLE IF NOT EXISTS `productattribute` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `categoryid` int(11) NOT NULL DEFAULT '0',
  `displayvalue` mediumtext,
  `absolutevalue` double NOT NULL DEFAULT '0',
  `unitid` int(11) NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productattribute_productID` (`productid`),
  KEY `productattribute_categoryID` (`categoryid`),
  KEY `productattribute_attributeID` (`attributeid`),
  KEY `productattribute_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productdescriptions`
--

CREATE TABLE IF NOT EXISTS `productdescriptions` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productdescriptions_productID` (`productid`),
  KEY `productdescriptions_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productimages`
--

CREATE TABLE IF NOT EXISTS `productimages` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(60) NOT NULL DEFAULT '',
  `status` varchar(60) NOT NULL DEFAULT '',
  KEY `productimages_productID` (`productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productkeywords`
--

CREATE TABLE IF NOT EXISTS `productkeywords` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `keywords` mediumtext NOT NULL,
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productkeywords_productID` (`productid`),
  KEY `productkeywords_keywords` (`keywords`(255)),
  KEY `productkeywords_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productlocales`
--

CREATE TABLE IF NOT EXISTS `productlocales` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(60) NOT NULL DEFAULT '',
  KEY `productlocales_productID` (`productid`),
  KEY `productlocales_localeID` (`localeid`),
  KEY `productlocales_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productsimilar`
--

CREATE TABLE IF NOT EXISTS `productsimilar` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `similarproductid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productsimilar_productID` (`productid`),
  KEY `productsimilar_spID` (`similarproductid`),
  KEY `productsimilar_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productskus`
--

CREATE TABLE IF NOT EXISTS `productskus` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) DEFAULT NULL,
  `sku` varchar(60) DEFAULT NULL,
  `localeid` int(11) NOT NULL DEFAULT '0',
  `addeddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `discontinueddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `productskus_productID` (`productid`),
  KEY `productskus_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productupsell`
--

CREATE TABLE IF NOT EXISTS `productupsell` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `upsellproductid` int(11) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `productupsell_productID` (`productid`),
  KEY `productupsell_upsellProductID` (`upsellproductid`),
  KEY `productupsell_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_attribute`
--

CREATE TABLE IF NOT EXISTS `search_attribute` (
  `productid` int(11) NOT NULL DEFAULT '0',
  `attributeid` bigint(20) NOT NULL DEFAULT '0',
  `valueid` int(11) NOT NULL DEFAULT '0',
  `absolutevalue` double NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `search_attribute_productID` (`productid`),
  KEY `search_attribute_attributeID` (`attributeid`),
  KEY `search_attribute_valueID` (`valueid`),
  KEY `search_attribute_absoluteValue` (`absolutevalue`),
  KEY `search_attribute_isAbsolute` (`isabsolute`),
  KEY `search_attribute_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_attribute_values`
--

CREATE TABLE IF NOT EXISTS `search_attribute_values` (
  `valueid` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  `absolutevalue` double NOT NULL DEFAULT '0',
  `unitid` int(11) NOT NULL DEFAULT '0',
  `isabsolute` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`valueid`),
  KEY `search_attrval_value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taxonomyhistory`
--

CREATE TABLE IF NOT EXISTS `taxonomyhistory` (
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `revisiondate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unitnames`
--

CREATE TABLE IF NOT EXISTS `unitnames` (
  `unitid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `localeid` int(11) NOT NULL DEFAULT '0',
  KEY `unitnames_unitID` (`unitid`),
  KEY `unitnames_localeID` (`localeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `unitid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(60) NOT NULL DEFAULT '',
  `baseunitid` int(11) NOT NULL DEFAULT '0',
  `multiple` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`unitid`),
  KEY `units_baseUnitID` (`baseunitid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
