-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 07:30 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rfw_delete`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE IF NOT EXISTS `customer_orders` (
  `orderid` bigint(20) NOT NULL AUTO_INCREMENT,
  `smid` bigint(20) NOT NULL DEFAULT '0',
  `otype` varchar(26) DEFAULT NULL,
  `acctid` bigint(20) DEFAULT NULL,
  `smacctid` bigint(20) NOT NULL DEFAULT '0',
  `invoiceid` varchar(26) DEFAULT NULL,
  `orderdate` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `bill2name` varchar(128) DEFAULT NULL,
  `bill2street1` varchar(128) DEFAULT NULL,
  `bill2street2` varchar(128) DEFAULT NULL,
  `bill2city` varchar(128) DEFAULT NULL,
  `bill2state` varchar(16) DEFAULT NULL,
  `bill2zip` varchar(20) DEFAULT NULL,
  `ship2name` varchar(128) DEFAULT NULL,
  `ship2street1` varchar(128) DEFAULT NULL,
  `ship2street2` varchar(128) DEFAULT NULL,
  `ship2city` varchar(128) DEFAULT NULL,
  `ship2state` varchar(16) DEFAULT NULL,
  `ship2zip` varchar(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `altphone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `emailaddr` varchar(255) DEFAULT NULL,
  `weight` double(16,2) DEFAULT NULL,
  `subtotal` double(16,2) DEFAULT NULL,
  `tax` double(16,2) DEFAULT NULL,
  `shipping` double(16,2) DEFAULT NULL,
  `total` double(16,2) DEFAULT NULL,
  `ip_addr` varchar(26) DEFAULT NULL,
  `repid` int(11) DEFAULT NULL,
  `arepid` int(11) DEFAULT NULL,
  `cardnum` text,
  `exp1a` varchar(4) DEFAULT NULL,
  `exp1b` varchar(4) DEFAULT NULL,
  `pmttype` char(2) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  `bill2country` char(2) NOT NULL DEFAULT 'US',
  `ship2country` char(2) NOT NULL DEFAULT 'US',
  `package` int(11) NOT NULL DEFAULT '0',
  `csmid` int(11) NOT NULL DEFAULT '0',
  `csmethod` varchar(255) NOT NULL DEFAULT '',
  `divid` int(11) NOT NULL DEFAULT '0',
  `brand` bigint(20) DEFAULT NULL,
  `clickid` varchar(50) DEFAULT NULL,
  `clicktype` int(11) DEFAULT NULL,
  `category` varchar(128) DEFAULT NULL,
  `billing_id` bigint(20) DEFAULT NULL,
  `shipping_id` bigint(20) DEFAULT NULL,
  `envelope` text,
  `bid` int(11) NOT NULL DEFAULT '0',
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `houseorder` int(11) NOT NULL DEFAULT '0',
  `completedate` bigint(20) NOT NULL DEFAULT '0',
  `sessionid` varchar(128) DEFAULT NULL,
  `cost` double(16,2) NOT NULL DEFAULT '0.00',
  `wholesale` int(11) NOT NULL DEFAULT '0',
  `claim` int(11) NOT NULL DEFAULT '0',
  `ginvoiceid` varchar(26) DEFAULT NULL,
  `lockedup` int(11) NOT NULL DEFAULT '0',
  `allocated` int(11) NOT NULL DEFAULT '0',
  `pallocated` int(11) NOT NULL DEFAULT '0',
  `fbaid` varchar(50) NOT NULL,
  `assigned` bigint(20) NOT NULL,
  `solicited` int(5) NOT NULL,
  `reached` int(5) NOT NULL,
  `callback_later` int(5) NOT NULL,
  `credit_request` int(5) NOT NULL DEFAULT '0',
  `cancelled_reason` varchar(255) NOT NULL,
  `price_overwrite` int(5) NOT NULL,
  KEY `orderid` (`orderid`),
  KEY `smid` (`smid`),
  KEY `otype` (`otype`),
  KEY `sessionid` (`sessionid`),
  KEY `ginvoice` (`ginvoiceid`),
  KEY `invoiceid` (`invoiceid`),
  KEY `status` (`status`),
  KEY `completedate` (`completedate`),
  KEY `orderdate` (`orderdate`),
  KEY `acctid` (`acctid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=416349 ;
