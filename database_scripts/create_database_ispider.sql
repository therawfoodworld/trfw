-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 07:26 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ispider`
--

-- --------------------------------------------------------

--
-- Table structure for table `descr`
--

CREATE TABLE IF NOT EXISTS `descr` (
  `dataid` varchar(20) NOT NULL,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `data` varchar(2000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `manpart` varchar(40) NOT NULL,
  `sku` varchar(40) NOT NULL,
  `status` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `dataid` varchar(20) NOT NULL,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `data` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `index`
--

CREATE TABLE IF NOT EXISTS `index` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `upc` varchar(20) NOT NULL,
  PRIMARY KEY (`dataid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=94781 ;

-- --------------------------------------------------------

--
-- Table structure for table `specA`
--

CREATE TABLE IF NOT EXISTS `specA` (
  `dataid` varchar(20) NOT NULL,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `specB`
--

CREATE TABLE IF NOT EXISTS `specB` (
  `dataid` varchar(20) NOT NULL,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE IF NOT EXISTS `title` (
  `dataid` varchar(20) NOT NULL,
  `manpart` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `brand` varchar(40) NOT NULL,
  `data` varchar(2000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
