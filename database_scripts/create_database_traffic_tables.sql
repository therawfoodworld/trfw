-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 29, 2016 at 07:31 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `traffic_tables`
--

-- --------------------------------------------------------

--
-- Table structure for table `priceengines`
--

CREATE TABLE IF NOT EXISTS `priceengines` (
  `peid` char(2) NOT NULL DEFAULT '',
  `pename` varchar(128) DEFAULT NULL,
  `peurl` varchar(255) DEFAULT NULL,
  `peimg` varchar(255) DEFAULT NULL,
  `fulldomainmatch` int(11) NOT NULL DEFAULT '0',
  KEY `peid` (`peid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `searchengines`
--

CREATE TABLE IF NOT EXISTS `searchengines` (
  `engine` varchar(128) DEFAULT NULL,
  `engurl` varchar(255) DEFAULT NULL,
  `keyitem` varchar(128) DEFAULT NULL,
  `delimiter` varchar(128) DEFAULT NULL,
  `seid` int(11) NOT NULL AUTO_INCREMENT,
  KEY `seid` (`seid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `topleveldomains`
--

CREATE TABLE IF NOT EXISTS `topleveldomains` (
  `tld` char(2) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  KEY `tld` (`tld`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
